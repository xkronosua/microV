# About
**microV** - software for controlling hardware and handling experiments based on multidimensional scanning with multiphoton microscope (or other abstract type microscopes) and linear scanning based simple optical experiments.

**hyperspectralViewer** - visualization tool for post-processing of measured data [dev].

Hardware control based on isolated device servers managed by [python-microscope](https://www.micron.ox.ac.uk/software/microscope/) module.

Data format used for multidimensional data storage: [Exdir](https://exdir.readthedocs.io)

[Experimental Directory Structure (Exdir): An Alternative to HDF5 Without Introducing a New File Format](https://www.frontiersin.org/articles/10.3389/fninf.2018.00016/full)

# Install requirements:

`pip install -r requirements.txt`

## Linux:
```
pacaur -S picoscope libps3000a libps4000 libps5000a
```
or
```
yaourt -S picoscope libps3000a libps4000 libps5000a
```
Create links:
```
sudo ln -s /opt/picoscope/lib/libps3000a.so /usr/lib/libPS3000a.so
sudo ln -s /opt/picoscope/lib/libps4000.so /usr/lib/libPS4000.so
sudo ln -s /opt/picoscope/lib/libps5000a.so /usr/lib/libPS5000a.so
```

# Screenshots
![microV](./docs/images/microV.jpeg)
![microV_HRS](./docs/images/microV_HRS.jpeg)
![hyperspectralViewer](./docs/images/hyperspectralViewer.jpeg)
