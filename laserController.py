#!/usr/bin/python3
import sys
import os

try:
    from PySide2 import QtWidgets, QtCore
    from PySide2.QtCore import QThreadPool
    from PySide2.QtCore import Signal
    import resources_pyside2

except:
    from PyQt5 import QtWidgets, QtCore
    from PyQt5.QtCore import QThreadPool
    from PyQt5.QtCore import pyqtSignal as Signal

    QtCore.pyqtRemoveInputHook()
    import resources

# import pyvisa
# from hardware import pyvisa_almost as pyvisa
import time
import re
import threading
import traceback
from scipy.interpolate import interp1d
import numpy as np
import logging
import logging.handlers
from colorlog import ColoredFormatter
from utils.utils import WorkThread, Worker, qtLoggingSignalEmiter, loadUi
import datetime
# from hardware.laserServer import LaserServer


import faulthandler

faulthandler.enable()

LOGLEVEL = logging.DEBUG
LOG_FILENAME = "laser.log"
server_logger = logging.getLogger("hardware.laserServer")
server_logger.setLevel(LOGLEVEL)

LOGFORMAT_colored = "%(asctime)s - %(name)s - %(log_color)s[%(levelname)s]%(reset)s - %(log_color)s%(message)s%(reset)s (%(filename)s:%(lineno)d)"
LOGFORMAT = (
    "%(asctime)s - %(name)s - [%(levelname)s] - %(message)s (%(filename)s:%(lineno)d)"
)

formatter_colored = ColoredFormatter(LOGFORMAT_colored)
formatter = logging.Formatter(LOGFORMAT)

logging.root.setLevel(LOGLEVEL)
logger = logging.getLogger()
logger.setLevel(LOGLEVEL)
# logger.handlers.pop()

logging_console = logging.StreamHandler()  # logging.getLogger().handlers[0]
logging_console.setLevel(LOGLEVEL)
logging_console.setFormatter(formatter_colored)
logger.addHandler(logging_console)

logging_logfile = logging.handlers.RotatingFileHandler(
    LOG_FILENAME, mode='a', maxBytes=50*1024*1024,
                                 backupCount=1, encoding=None, delay=False
)  # logging.FileHandler("microV.log","w")
logging_logfile.setLevel(LOGLEVEL)
logging_logfile.setFormatter(formatter)
logger.addHandler(logging_logfile)
logging.getLogger("PyQt5.uic.uiparser").setLevel(logging.WARNING)
logging.getLogger("PyQt5.uic.properties").setLevel(logging.WARNING)
logging.getLogger("matplotlib.text").setLevel(logging.INFO)
logging.getLogger("matplotlib.font_manager").setLevel(logging.WARNING)
logging.getLogger("matplotlib.ticker").setLevel(logging.WARNING)
logging.getLogger("matplotlib").setLevel(logging.WARNING)
logging.getLogger("filelock").setLevel(logging.WARNING)
logging.getLogger("qdarkstyle").setLevel(logging.WARNING)

# logging.getLogger().setLevel(LOGLEVEL)

################################################################
################################################################

if "sim" in sys.argv:
    demo = True
else:
    demo = False
logger.info(f"microV demo mode: {demo}")

os.environ["LOGLEVEL"] = "DEBUG"

import Pyro4
import Pyro4.util

Pyro4.config.SERIALIZER = "pickle"

# laserServer = Pyro4.Proxy('PYRO:InSightX3Laser@127.0.0.1:8000')


import faulthandler

faulthandler.enable()


def time_msleep(msecs):
    loop = QtCore.QEventLoop()
    QtCore.QTimer.singleShot(msecs, loop.quit)
    loop.exec_()
    #QtCore.QThread.msleep(msecs)



class laserController(QtWidgets.QMainWindow):

    laserServer = None

    wavelength_ready = False
    prev_power = 0
    current_power = 0
    focusCounter = 0
    fineTuneFit = None
    MAX_POWER = 3  # W
    alive = True
    threadpool = QThreadPool()


    def __init__(self, parent=None):

        super(laserController, self).__init__(parent)
        self.ui = loadUi(
            "ui/laser.ui", Dir="ui", parent=parent
        )  # Loads all widgets of uifile.ui into self
        self.infoTimer = QtCore.QTimer()
        self.initUI()

        # self.infoTimer.timeout.connect(self.state_monitor)
        # self.infoTimer.start(1000)

        # You can format what is printed to text box
        handler = qtLoggingSignalEmiter()
        formatter = logging.Formatter(LOGFORMAT)
        handler.setFormatter(formatter)

        self.logTextBox = QtWidgets.QPlainTextEdit(self)
        self.ui.laserLog.addWidget(self.logTextBox)
        logger.addHandler(handler)

        handler.new_record.objSignal.connect(
            self.logTextBox_appendHtml
        )  # <---- connect QPlainTextEdit.appendPlainText slot

        # You can control the logging level
        # install_mp_handler()
        # self.laserServer.connect()
        self.ui.powerMeter.setMaximum(int(self.MAX_POWER * 1000))

    def logTextBox_appendHtml(self, msg):
        # import pdb; pdb.set_trace()
        search = re.search(r"(.*) - (.*) - \[(.*)\] - (.*) \((.*\.py):(\d+)\)", msg)
        # print(msg,search)
        if search:
            t, owner, level, msg, file, lineno = search.groups()
        else:
            return
        # self.widget.appendHtml(f"{msg}")

        if level == "WARNING":
            level = f'<font color="#e600e6">{level}</font>'
        elif level == "INFO":
            level = f'<font color="#00cc00">{level}</font>'
        elif level == "DEBUG":
            level = f'<font color="#85adad">{level}</font>'
        elif level == "ERROR":
            level = f'<font color="red">{level}</font>'
            msg = f'<b><font color="red">[{msg}]</font></b>'
        t = f'<font color="orange">{t}</font>'
        lineno = f'<font color="#4dc3ff">{lineno}</font>'
        func = f'<font color="orange">{file}</font>'
        msg = f"{t}[{level}]:{file}:{lineno}:\t {msg}"
        self.logTextBox.appendHtml(msg)

    def __del__(self):
        self.alive = False

    def initUI(self):

        self.ui.connectLaser.toggled[bool].connect(self.connectLaser)
        self.ui.onOff.clicked.connect(self.laserOnOff)
        self.ui.switchShutter.clicked.connect(self.setShutter)
        self.ui.switchIRShutter.clicked.connect(self.setIRShutter)
        # self.watchdogTimer.timeout.connect(self.onWatchdogTimerTimeout)
        self.ui.wavelength.valueChanged[int].connect(self.setWavelength)
        self.ui.LCD_brightness.valueChanged[int].connect(self.setLCDBrightness)
        self.ui.motorPos.valueChanged[float].connect(self.setMotorPos)
        self.ui.modeRUN.toggled.connect(self.setMODE)
        self.ui.watchdog_OFF.valueChanged[int].connect(self.setWatchdogForServer)

        self.ui.closeEvent = self.closeEvent
        self.ui.show()

    def closeEvent(self, evnt=None):
        print("closeEvent")
        self.alive = False
        # time.sleep(2)
        # self.laserServer.shutdown()

    def updateValues(self, info):
        self.ui.powerMeter.setValue(int(info["power"] * 1000))
        self.ui.power.setText(str(info["power"]) + " W")

        # self.ui.wavelength.setEnabled(not wavelength is None)
        if (
            not self.ui.wavelength.hasFocus()
            and info["wavelength"] >= 680
            and info["wavelength"] <= 1300
        ):
            # self.ui.wavelength.setStyleSheet('color:yellow;')
            self.ui.wavelength.blockSignals(True)
            self.ui.wavelength.setValue(info["wavelength"])
            self.ui.wavelength.blockSignals(False)

        # self.ui.LCD_brightness.setEnabled(not LCD is None)
        # if not info['LCD'] is None:
        # 	if not self.ui.LCD_brightness.hasFocus() and info['LCD']>=0 and info['LCD']<=255:
        # 		#self.ui.LCD_brightness.setStyleSheet('color:yellow;')
        # 		self.ui.LCD_brightness.blockSignals(True)
        # 		self.ui.LCD_brightness.setValue(info['LCD'])
        # 		self.ui.LCD_brightness.blockSignals(False)
        # else:
        # 	self.ui.LCD_brightness.setStyleSheet('color:#95a2b8;')

        # self.ui.motorPos.setEnabled(not motor is None)
        if not info["precompensation"] is None:
            if not self.ui.motorPos.hasFocus() and info["precompensation"] >= 0:
                self.ui.motorPos.blockSignals(True)
                self.ui.motorPos.setValue(info["precompensation"])
                self.ui.motorPos.blockSignals(False)

        if info["mode"] is None:
            pass
        elif info["mode"] == "RUN":
            self.ui.modeRUN.blockSignals(True)
            self.ui.modeRUN.setChecked(True)
            self.ui.modeRUN.blockSignals(False)

        elif "ALIGN" in info["mode"].upper():
            self.ui.modeALIGN.blockSignals(True)
            self.ui.modeALIGN.setChecked(True)
            self.ui.modeALIGN.blockSignals(False)

        # import pdb; pdb.set_trace()
        # self.ui.onOff.setEnabled(not ON_OFF is None)
        if not info["enabled"] is None:
            if info["enabled"]:
                self.ui.onOff.setStyleSheet("background-color:green;")
            else:
                self.ui.onOff.setStyleSheet("background-color:blue;")
        else:
            self.ui.onOff.setStyleSheet("background-color:#95a2b8;")

        # self.ui.switchShutter.setEnabled(not shutter is None)
        if not info["shutter"] is None:
            if info["shutter"]:
                self.ui.switchShutter.setStyleSheet(
                    "background-color:red;height:100px;"
                )
            else:
                self.ui.switchShutter.setStyleSheet(
                    "background-color:#595959;height:100px;"
                )
        else:
            self.ui.switchShutter.setStyleSheet(
                "background-color:#95a2b8;height:100px;"
            )

        # self.ui.switchIRShutter.setEnabled(not IRshutter is None)
        if not info["IRshutter"] is None:
            if info["IRshutter"]:
                self.ui.switchIRShutter.setStyleSheet(
                    "background-color:red;height:100px;"
                )
            else:
                self.ui.switchIRShutter.setStyleSheet(
                    "background-color:#595959;height:100px;"
                )
        else:
            self.ui.switchIRShutter.setStyleSheet(
                "background-color:#95a2b8;height:100px;"
            )

        self.ui.status.setText(info["status"]["State"])
        print(info["status"]["State"])
        # time.sleep(1)

        if not info["alive"]:
            self.alive = False
            self.ui.connectLaser.blockSignals(True)
            self.ui.connectLaser.setChecked(False)
            self.ui.connectLaser.blockSignals(False)

    def state_monitor(self, status_callback=None):
        # import pdb; pdb.set_trace()
        prev_info = {}
        while self.alive:
            alive = self.laserServer.is_alive()
            info = {"alive": alive}
            try:
                # print('state_monitor', alive)
                if alive:
                    # import pdb; pdb.set_trace()
                    laserInfo = self.laserServer.getInfo()
                    info["power"] = laserInfo["power"]["value"]
                    info["status"] = laserInfo["status"]["value"]

                    info["enabled"] = info["status"]["ON_OFF"]
                    info["wavelength"] = laserInfo["wavelength"]["value"]
                    # info['LCD'] = self.laserServer.lcd_brightness()
                    info["precompensation"] = laserInfo["precompensation"]["value"]
                    info["shutter"] = info["status"]["Main shutter"]
                    info["IRshutter"] = info["status"]["IR shutter"]
                    info["mode"] = info["status"]["MODE"]

                if info != prev_info:
                    prev_info = info.copy()
                    tmp = {
                        k: info[k]
                        for k in [
                            "alive",
                            "enabled",
                            "wavelength",
                            "precompensation",
                            "shutter",
                            "IRshutter",
                            "mode",
                            "power",
                        ]
                    }
                    logger.info(f"info: {tmp}")
                    if not status_callback is None:
                        status_callback.emit(info)
            except:
                traceback.print_exc()
                time_msleep(1000)#QtCore.QThread.msleep(1000)

            time_left = self.laserServer.getWatchdogTimeLeft_sec()
            self.ui.watchdog_OFF_label.setText(f"Watchdog OFF [{datetime.timedelta(seconds=time_left)}]:")

            time_msleep(1000)
            #QtCore.QThread.msleep(1000)
            # time.sleep(1)
        logger.info(f"state_monitor finished")
        print(f"state_monitor finished")

    def connectLaser(self, state):
        if state:
            # import pdb; pdb.set_trace()
            self.alive = True
            self.laserServer = Pyro4.Proxy("PYRO:InSightX3Laser@127.0.0.1:8000")
            self.laserServer._pyroRelease()

            self.laserServer.initialize()

            worker = Worker(
                self.state_monitor
            )  # Any other args, kwargs are passed to the run function
            worker.signals.status.connect(self.updateValues)
            # Execute
            self.threadpool.start(worker)

            # self.state_thread = threading.Thread(target=self.state_monitor)
            # self.state_thread.daemon = True
            # self.state_thread.start()
            # table = np.loadtxt('calibr/motorFineTune.txt')
            # self.fineTuneFit = interp1d(table[:,0],table[:,1],kind='quadratic',bounds_error=False,fill_value="extrapolate")

            # #self.watchdogTimer.start(2000)
            # self.readThreadpool = QThreadPool()
            # self.readWorker = Worker(self.state_monitor) # Any other args, kwargs are passed to the run function
            # #self.laserWorker.signals.result.connect(self.print_output)
            # #self.laserWorker.signals.finished.connect(self.thread_complete)
            # self.readWorker.signals.progress.connect(self.updateData)
            # # Execute
            # self.readThreadpool.start(self.readWorker)
        else:
            self.alive = False
            if not self.laserServer is None:
                self.laserServer.shutdown()

    def laserOnOff(self, state):
        self.ui.onOff.clearFocus()
        if not self.laserServer.get_is_on():
            self.ui.status.setText("Initialization...")
            self.laserServer.initialize()
            self.ui.onOff.setStyleSheet("background-color:#95a2b8;")
            self.setWatchdogForServer(self.ui.watchdog_OFF.value())
            t = threading.Thread(target=self.laserServer.enable)
            t.daemon = True
            t.start()
        else:
            t = threading.Thread(target=self.laserServer.disable)
            t.daemon = True
            t.start()

    def setWavelength(self, val=None):
        if val is None:
            val = self.ui.wavelength.value()
        res = self.laserServer.setWavelength(val)
        self.ui.wavelength.clearFocus()
        logger.info(f"Set wavelength: {val}")
        # res = self.laserServer.wavelength
        # if not res is None:
        # 	self.ui.wavelength.setValue(res)

    def setWatchdogForServer(self, value):
        self.laserServer.setWatchdogForServer(value)

    def setLCDBrightness(self, val=None):

        if val is None:
            val = self.ui.LCD_brightness.value()
        res = self.laserServer.set_lcd_brightness(val)

        self.ui.LCD_brightness.clearFocus()
        logger.info(f"Set LCD brightness: {val}")
        # res = self.laserServer.LCD_BRIGhtness()
        # if not res is None:
        # 	self.ui.LCD_brightness.setValue(res)

    def setMotorPos(self, val=None):
        if val is None:
            val = self.ui.motorPos.value()
        res = self.laserServer.setPrecompensation(val)
        self.ui.motorPos.clearFocus()

        logger.info(f"Set precompensation: {val}")
        # res = self.laserServer.CONTrol_MTRMOV()
        # if not res is None:
        # 	self.ui.motorPos.setValue(res)

    def setMODE(self):
        # mode = None
        # import pdb;pdb.set_trace()
        if self.ui.modeRUN.isChecked():
            mode = "RUN"
        else:
            mode = "ALIGN"

        self.laserServer.setMODE(mode)
        logger.info(f"Set mode: {mode}")
        # self.laserServer.MODE()

    def setShutter(self, state=None):
        state = self.laserServer.shutter()
        self.laserServer.setShutter(not state)
        self.ui.switchShutter.clearFocus()
        logger.info(f"Set main shutter: {state}")
        # self.laserServer.SHUTter()

    def setIRShutter(self, state=None):
        state = self.laserServer.IRshutter()
        self.laserServer.setIRshutter(not state)
        self.ui.switchIRShutter.clearFocus()
        logger.info(f"Set IR shutter: {state}")
        # self.laserServer.IRSHUTter()


from signal import signal as SYSTEM_signal, SIGINT


def handler_SIGINT(signal_received, frame):
    # Handle any cleanup here
    print("SIGINT or CTRL-C detected. Exiting gracefully")
    sys.exit(0)


if __name__ == "__main__":

    SYSTEM_signal(SIGINT, handler_SIGINT)
    # QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
    app = QtWidgets.QApplication(sys.argv)
    ex = laserController()

    Pyro4.util.getPyroTraceback()
    sys.exit(app.exec_())
