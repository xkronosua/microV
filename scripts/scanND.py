import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir
from pathlib import Path
import time
import traceback
import threading
from scipy.interpolate import interp1d
import numpy.lib.recfunctions as rfn
import pint
from collections import deque
import datetime

sys.path.append(str(Path("../utils")))

from .scriptUtils import scan_move_recursive, isRunning, process_progress_info

import importlib
import beepy as beep

from utils.units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES

ureg = pint.UnitRegistry()
Q_ = ureg.Quantity

# def time_sleep(secs):
#     QtCore.QThread.msleep(abs(int(secs*1000)))

class scanNDThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    moveToSignal = QtCore.Signal(float, str, float)
    running = False
    store = None
    fname = "tmp"
    header = ["index", "axis", "A", "B", "C"]

    def time_sleep(self, secs):
        time.sleep(secs)
        #self.msleep(abs(int(secs*1000)))

    def abstractMoveTo(self, target, axis="None", wait=True):
        self.moveToSignal.emit(target, axis, wait)

    def __init__(self, parent=None):
        super(scanNDThread, self).__init__(parent)

        self.fname = Path(self.parent().expData_filePath.text())
        if ".exdir" in str(self.fname):
            pass
        else:
            self.fname = str(self.fname).split(".exdir")[0] + ".exdir"

        self.start_time = datetime.datetime.now()
        self.cycle_time = deque([time.time()], maxlen=1000)

    def stop(self):
        if self.parent().ui.finalization_protocol_beep.isChecked():
            beep.beep(1)
        if self.parent().ui.finalization_protocol_closeShutters.isChecked():
            self.parent().laserSetShutter(False, wait=True)
            self.parent().laserSetIRShutter(False, wait=True)

        if self.parent().ui.finalization_protocol_laserOff.isChecked():
            self.parent().laserOnOff(False)

        if self.parent().ui.finalization_protocol_fullShutdown.isChecked():
            self.parent().laserSetShutter(False, wait=True)
            self.parent().laserSetIRShutter(False, wait=True)
            self.parent().laserOnOff(False)
            self.parent()._shutdownHardware()

        self.running = False

    # def checkOverflow(self):
    # 	if len(self.activeDataset) == self._write_to_row:
    # 		prev_name = self.activeDataset.object_name
    # 		l = len(self.activeDataset)
    # 		prev_dataset = self.activeDataset
    # 		new_len = np.prod([len(i) for i in self.scan_ranges])
    #
    # 		self.activeDataset = self.storeActiveGroup.create_dataset(prev_name+'_resized',shape=(new_len,),dtype=self.activeDataset.dtype)
    # 		self.activeDataset.data[:l] = prev_dataset.data[:]

    def process_script_line(self, index, script_line, iteration):
        print(script_line.to_dict())
        t = []
        t.append(time.time())

        if script_line["type"] == "move":
            self.parent().abstractMoveTo(
                script_line["target"], axis=script_line["axis"]
            )
            self._targets[int(script_line.level)] = script_line["target"]
            t.append(time.time())
            self.params.child("Config", "PlotAxis").setValue(script_line["axis"])

        elif script_line["type"] == "afterLoop":
            self._preview_start = self._write_to_row

        elif script_line["type"] == "wait":
            self.time_sleep(script_line["target"])

        elif script_line["type"] == "read":
            self.parent().laserClient.kickWatchdog()
            t.append(time.time())
            self.time_sleep(self.params.child("Config", "WaitBeforeReadout").value())
            lines = self.parent().getUnifiedData(
                save_metadata=self.save_all_metadata, axes_positions_skip=self.scan_axes
            )
            t.append(time.time())
            # self.checkOverflow()

            self.activeDataset[list(lines.dtype.names)][self._write_to_row] = lines

            t.append(time.time())
            self.activeDataset["iteration"][self._write_to_row][:] = iteration
            t.append(time.time())
            for i in range(len(self.scan_axes)):
                self.activeDataset["position"][self.scan_axes[i]][
                    self._write_to_row
                ] = self.parent().abstractGetPosition(axis=self.scan_axes[i])
            t.append(time.time())

            data_slice = slice(self._preview_start, self._write_to_row + 1)
            x = self.activeDataset["position"][
                self.params.child("Config", "PlotAxis").value()
            ][data_slice].copy()
            info_ = f"{[round(t,5) for t in self._targets[:-1]]}"
            items = []
            if len(x) > 1:
                for source in self.activeDataset["data"].dtype.names:
                    for name in self.activeDataset["data"][source]["data"].dtype.names:
                        items.append(
                            {
                                "type": "Scan",
                                "name": f"{source}_{name}",
                                "info": info_,
                                #'index': i,
                                "time": time.time(),
                                "data": [
                                    x,
                                    self.activeDataset["data"][source]["data"][name][
                                        data_slice
                                    ].copy(),
                                ],
                            }
                        )
                    if "raw" in self.activeDataset["data"][source].dtype.names:
                        raw_x_name = self.activeDataset["data"][source][
                            "raw"
                        ].dtype.names[0]
                        for name in self.activeDataset["data"][source][
                            "raw"
                        ].dtype.names[1:-1]:
                            if name in ["type", "VRange","time"]: continue
                            items.append(
                                {
                                    "type": "raw",
                                    "name": f"{source}_{name}",
                                    "info": info_,
                                    #'index': i,
                                    "time": time.time(),
                                    "data": [
                                        self.activeDataset["data"][source]["raw"][
                                            raw_x_name
                                        ][self._write_to_row].copy(),
                                        self.activeDataset["data"][source]["raw"][name][
                                            self._write_to_row
                                        ].copy(),
                                    ],
                                }
                            )
                t.append(time.time())
                self.dataReadySignal.emit(items)
            t.append(time.time())
            self._write_to_row += 1

            if self.parent().ui.actionPause.isChecked():
                while self.parent().ui.actionPause.isChecked():
                    self.time_sleep(0.1)

            try:
                progress = next(self.progress)
                info = process_progress_info(
                    progress, self.total_steps, self.start_time, self.cycle_time
                )
                self.progressSignal.emit(int(round(progress)))
                self.parent().progressBar.setToolTip(info)
            except Exception as ex:
                logging.error("progressError", exc_info=ex)
            t.append(time.time())

        elif script_line["type"] == "finish":

            self.running = False

        print(np.diff(t))

    @isRunning
    def run(self):
        with exdir.File(self.fname) as self.store:
            self.params = self.parent().scanND_tree.params

            if not "scanND" in self.store:
                self.storeActiveGroup = self.store.create_group("scanND").create_group(
                    time.strftime("data%Y%m%d-%H%M%S")
                )
            else:
                self.storeActiveGroup = self.store["scanND"].create_group(
                    time.strftime("data%Y%m%d-%H%M%S")
                )
            self.source = self.parent().readoutSources.currentText()

            self.save_all_metadata = (
                self.params.child("Config").child("Save all metadata").value()
            )

            self.script, self.iterations = self.parent().scanND_tree.generateScript()
            print(self.script["axis"].dropna().unique())
            self.scan_axes = self.script["axis"].dropna().unique()

            lines = self.parent().getUnifiedData(
                save_metadata=self.save_all_metadata, axes_positions_skip=self.scan_axes
            )
            pos_axis_val = []
            pos_axis_dtype = []

            for ax in self.scan_axes:
                pos_axis_val.append(self.parent().abstractGetPosition(axis=ax))
                pos_axis_dtype.append((ax, AXES_UNITS_DTYPES[ax][1]))
            if self.save_all_metadata:
                dtype = [
                    ("time", "f8"),
                    ("position", pos_axis_dtype),
                    ("data", lines.dtype["data"]),
                    ("metadata", lines.dtype["metadata"]),
                    ("iteration", "i4", (self.iterations.shape[1],)),
                ]
            else:
                dtype = [
                    ("time", "f8"),
                    ("position", pos_axis_dtype),
                    ("data", lines.dtype["data"]),
                    ("iteration", "i4", (self.iterations.shape[1],)),
                ]
            print(dtype)
            dtype = np.dtype(dtype)

            first_index = 0

            if self.source == "AndorCamera":
                lines["data"]["AndorCamera"]["raw"][:] = self.parent().andorCCDBaseline
                first_index = 1
            else:
                pass
            N_rows = (self.script["type"] == "read").sum()

            self.parent().scriptThread_lastStorePath = (
                self.store.directory.absolute(),
                self.storeActiveGroup.name,
            )
            self.storeActiveGroup.attrs[
                "README"
            ] = self.parent().ui.readme_textBrowser.toPlainText()
            self.activeDataset = self.storeActiveGroup.create_dataset(
                "data", shape=(N_rows + first_index,), dtype=dtype
            )

            self.total_steps = len(self.activeDataset)
            print(self.scan_axes.tolist(), type(self.scan_axes.tolist()))

            if (
                self.params.child("Config", "PlotAxis").opts["limits"]
                != self.scan_axes.tolist()
            ):
                self.params.child("Config", "PlotAxis").setLimits(
                    self.scan_axes.tolist()
                )
                self.params.child("Config", "PlotAxis").setValue(self.scan_axes[-1])

            self.progress = iter(np.linspace(0, 100, self.total_steps).tolist())

            self.activeDataset["time"][0] = time.time()
            self.activeDataset["data"][0] = lines["data"]
            if self.save_all_metadata:
                self.activeDataset["metadata"][0] = lines["metadata"]
            print(self.activeDataset["position"][0], pos_axis_val)
            for i in range(len(self.scan_axes)):
                self.activeDataset["position"][i][0] = pos_axis_val[i]
            self.activeDataset["iteration"][0] = -1

            self.activeDataset.attrs["axes_units_dtypes"] = AXES_UNITS_DTYPES
            self.activeDataset.attrs["sources_units_dtypes"] = AXES_UNITS_DTYPES

            self.activeDataset.attrs["readoutSource"] = self.source
            self.activeDataset.attrs["header"] = str(lines.dtype)
            self.activeDataset.attrs[
                "filters"
            ] = self.parent().filtersPiezoStage_Dict  # .to_dict()
            info, current = self.parent().MultiScan_getInfo()
            # print({k:info[k] for k in info.dtype.names})
            self.activeDataset.attrs["centerIndex_info"] = {
                k: info[k].tolist() for k in info.dtype.names
            }

            metadata = self.parent().abstractGetMetadata()
            metadata_ = {}
            for k in metadata.dtype.names:
                if hasattr(metadata[k], "dtype"):
                    if metadata[k].ndim == 0:
                        metadata_[k] = metadata[k].reshape(1)[0].tolist()
                    else:
                        metadata_[k] = metadata[k].tolist()
                elif type(metadata[k]) in (float, str, int, list):
                    metadata_[k] = metadata[k]

                else:
                    metadata_[k] = str(metadata[k])
                print("!!!!!!!", k, metadata_[k], type(metadata_[k]))
                self.activeDataset.attrs[k] = metadata_[k]

            self.lastUpdate = time.time()
            self._write_to_row = first_index
            self._preview_start = self._write_to_row
            self._targets = [-1] * int(self.script.level.max() + 1)

            self.cycle_time = deque([time.time()], maxlen=1000)

            for i, script_line in self.script.iterrows():
                if not self.running:
                    break
                try:
                    self.process_script_line(i, script_line, self.iterations[i])
                except Exception as ex:
                    logging.error("ScanND:ERROR", exc_info=ex)
                    break
            # print(self.activeDataset.dtype)
            del self.activeDataset
            del self.storeActiveGroup

        self.store.close()
        # self.stop()
