import sys
import exdir
import numpy as np
import matplotlib.pyplot as plt
import argparse
import numpy_indexed as npi
from scipy.signal import medfilt
import traceback
from matplotlib import cm
import matplotlib
import matplotlib.patheffects as path_effects


parser = argparse.ArgumentParser(description="Map polarization curves on scan3D data.")
parser.add_argument("-f", dest="file", help="Exdir file path.")
parser.add_argument("-b", dest="band",  nargs="+",
 type=str, help="Signal bands.", default="ALL")
parser.add_argument(
    "-t",
    dest="timestamps_polar",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in "MultiScan" folder.
					Integer index to process by index in sorted list of timestamps.
					Last measured "-1" by default.
					""",
    default=["-1"],
)
parser.add_argument(
    "-t3D",
    dest="timestamps3D",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in "scan3D" folder.
					Integer index to process by index in sorted list of timestamps.
					Last measured "-1" by default.
					""",
    default=["-1"],
)
parser.add_argument(
    "--log10",
    dest="log10",
    help="View as log10",
    action="store_true",
)

args = parser.parse_args()



def extractTimestamp(store, timestamps, dataType):
    timestamps_ = []
    keys = list(store[dataType])
    keys.sort()
    print(timestamps)

    if "ALL" in timestamps:
        timestamps_ = [k for k in store[dataType] if k[0] != "_"]
    else:
        for ts in timestamps:
            try:
                timestamps_.append(keys[int(ts)])
            except:
                traceback.print_exc()
                timestamps_.append(ts)
    print(timestamps_)
    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    dtype = store[dataType][timestamps_[-1]]["data"].dtype

    data = np.zeros(np.sum(sizes), dtype=dtype)
    last_row = 0
    data_dict = {}
    for i, k in enumerate(timestamps_):
        # if store[dataType][k]['data'].dtype==dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        if w.sum() > 0:
            data_dict[k] = d
        # data[last_row:last_row+w.sum()] = d[w]
        # last_row+=w.sum()
        # print(i,k)
    data_attrs = store[dataType][timestamps_[-1]]["data"].attrs.to_dict()
    # data = data[data['time']!=0]

    return data_dict, data_attrs

store = exdir.File(args.file, "r")
data3D_dict, data3D_attrs = extractTimestamp(store, args.timestamps3D, 'scan3D')
data_polar_dict, data_polar_attrs = extractTimestamp(store, args.timestamps_polar, 'MultiScan')

#data3D = data3D_dict[list(data3D_dict.keys())[0]]
#data_polar = data_polar_dict[list(data_polar_dict.keys())[0]]
#normalize item number values to colormap


if 'ALL' in args.band:
    bands = data_polar["data"]["AndorCamera"]["data"].dtype.names
else:
    bands = args.band
    bands = [b.replace(',','') for b in bands]

for timestamp3D, data3D in data3D_dict.items():
    for timestamp_polar, data_polar in data_polar_dict.items():
        for band in bands:
            img = data3D["data"]["AndorCamera"]["data"][band][0, ..., 0]  # .astype(float)
            X = data3D["metadata"]["X"][0, ..., 0]
            Y = data3D["metadata"]["Y"][0, ..., 0]
            scale = data3D_attrs["scale"]
            fig, axes = plt.subplots(1, 2, sharex=True, sharey=True)

            img1 = (img - img.min()).astype(float)
            img1 /= np.ptp(img1)
            if args.log10:
                img1 = np.log10(abs(img1))
                axes[0].set_title("*log10")

            axes[0].contourf(X, Y, img1, 100, cmap="turbo")

            axes[1].contourf(X, Y, img1, 100, cmap="turbo", alpha=0.1)

            # axes[0].set_aspect(1)

            info = data_polar_attrs["centerIndex_info"]#.to_dict()
            _index = data_polar["position"].dtype.names.index("CenterIndex")
            w = data_polar["time"] != 0
            norm_sig = np.ptp(data_polar[w]["data"]["AndorCamera"]["data"][band])
            norm_color = matplotlib.colors.Normalize(vmin=0, vmax=norm_sig)

            xlim = axes[1].get_xlim()
            view_size = xlim[1] - xlim[0]
            norm_sig = view_size / 10
            colors = "rgbcmky" * 2
            for d in npi.group_by(data_polar[w]["iteration"][:, _index]).split(data_polar[w]):
                centerIndex = int(d["position"]["CenterIndex"].mean())
                angle = d["position"]["HWP_Polarization"] * 2

                sig = d["data"]["AndorCamera"]["data"][band]
                # exposure = d['data']['AndorCamera']['exposure'][args.band]
                print(centerIndex)
                ptp_sig = np.ptp(sig)
                sig = medfilt(sig, 5) / ptp_sig * norm_sig
                x = sig * np.cos(np.deg2rad(angle)) + info["X"][int(centerIndex)]
                y = sig * np.sin(np.deg2rad(angle)) + info["Y"][int(centerIndex)]

                rgba_color = cm.jet(norm_color(ptp_sig))

                axes[1].fill(x, y, color=rgba_color, lw=1.5, label=f"NP{centerIndex}", fill=False, path_effects=[path_effects.SimpleLineShadow(offset=(1,-1), shadow_color='black'),
                       path_effects.Normal()])
                axes[1].text(
                    info["X"][int(centerIndex)] + 0.5,
                    info["Y"][int(centerIndex)] + 0.5,
                    s=f"NP{centerIndex}",
                )
            axes[0].set_aspect(1)
            axes[1].set_aspect(1)
            fig.suptitle(args.file.split(".exdir")[0]+f" [3D:{timestamp3D}, polar:{timestamp_polar}]\n{band}")
plt.show()
