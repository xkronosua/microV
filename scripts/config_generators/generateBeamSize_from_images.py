import exdir
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import image
from scipy.interpolate import interp1d
from scipy.signal import medfilt
import lmfit
from lmfit.lineshapes import gaussian, lorentzian
import time
import sys
import argparse
import pandas as pd
import yaml
from io import StringIO


def gaussianBeam2d(
    x,
    y,
    bg=0,
    amplitude=1.0,
    centerx=0.0,
    centery=0.0,
    wx=1.0,
    wy=1.0,
    rotation=0,
    profile=None,
):
    xp = (x - centerx) * np.cos(rotation) - (y - centery) * np.sin(rotation)
    yp = (x - centerx) * np.sin(rotation) + (y - centery) * np.cos(rotation)
    return bg + amplitude * np.exp(-2 * ((xp / wx) ** 2 + (yp / wy) ** 2))


parser = argparse.ArgumentParser(
    description="Fit beam from images captured by camera.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "-f",
    dest="files",
    nargs="+",
    help="""
					Images with beam profiles at different distances from beamsplitter
					and under different excitation wavelengths.
					Name format:
						TUN{Distance}mm_{LaserWavelength}nm
						FIX{Distance}mm
					Example:
						TUN340mm_1300nm.jpg
						FIX340mm.jpg
					""",
)
parser.add_argument(
    "-l",
    dest="laser_output",
    help="""
					Calibration for TUN, FIX or both outputs.
					Options: [TUN, FIX, ALL].
					""",
    default="ALL",
)
parser.add_argument(
    "-s", dest="pixel_size", type=float, help="""Pixel size in um""", default=6.0
)

parser.add_argument(
    "-D", dest="distance", type=float, help="Distance from beamsplitter to lens in mm"
)


parser.add_argument(
    "--vary_angle",
    dest="vary_angle",
    action="store_true",
    help="Vary angle (for elliptical shape).",
)

parser.add_argument("--lens", dest="lens", help="Focusing lens.", default="PAC04AR.16")
parser.add_argument(
    "--lens_config",
    dest="lens_config",
    help="Focusing lens parameters.",
    default='{"Focal length[mm]": 30, "Waist-sample distance[mm]": 0}',
)

parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)

args = parser.parse_args()
print(args)


fnames = list(args.files)

outputs = args.laser_output
if args.laser_output == "ALL":
    outputs = ["FIX", "TUN"]


def rebin(arr, new_shape):
    shape = (
        new_shape[0],
        arr.shape[0] // new_shape[0],
        new_shape[1],
        arr.shape[1] // new_shape[1],
    )
    return arr.reshape(shape).mean(-1).mean(1)


bin_size = 5

lens_config = yaml.safe_load(StringIO(args.lens_config))


for output_type in outputs:
    res = []
    fnames_ = [f for f in fnames if output_type in f]
    fnames_dict = {}
    for fname in fnames_:
        pos = int(fname.split(output_type)[-1].split("mm")[0])
        if not pos in fnames_dict:
            fnames_dict[pos] = []
        fnames_dict[pos].append(fname)

    for pos, fnames_ in fnames_dict.items():
        rows = 4
        fig, axes = plt.subplots(
            rows, int(np.ceil(len(fnames_) / rows)) + 1, sharex=True, sharey=True
        )
        fig1, axes1 = plt.subplots(
            rows, int(np.ceil(len(fnames_) / rows)) + 1, sharex=True, sharey=True
        )

        axes = axes.reshape(axes.shape[1] * axes.shape[0])
        axes1 = axes1.reshape(axes1.shape[1] * axes1.shape[0])
        fig.suptitle(f"{output_type}:{pos}")
        fig1.suptitle(f"{output_type}:{pos} (residuals)")

        if output_type == "TUN":
            fnames_.sort(key=lambda x: float(x.split("_")[-1].split("nm")[0]))

        for i, fname in enumerate(fnames_):
            data = image.imread(fname)[:, :, 0].T

            X, Y = (
                np.mgrid[0 : data.shape[0] : 1, 0 : data.shape[1] : 1]
                * args.pixel_size
                * 1e-3
            )

            X = rebin(X, np.array(X.shape) // bin_size)
            Y = rebin(Y, np.array(Y.shape) // bin_size)

            d = rebin(data, np.array(data.shape) // bin_size) - data.min()
            if output_type == "FIX":
                ex_wl = 1045
            else:
                ex_wl = int(fname.split("_")[-1].split("nm")[0])

            tmp = {"ex_wl": ex_wl, "distance": pos}
            print(fname, tmp)

            axes[i].contourf(X, Y, d, 50, vmin=d.min(), vmax=d.max())

            if d.sum() == 0:
                break
            model = lmfit.Model(gaussianBeam2d, independent_vars=["x", "y"])
            params = model.make_params()
            params["rotation"].set(
                value=0.001, min=-np.pi / 4, max=np.pi / 4, vary=args.vary_angle
            )
            params["wx"].set(value=np.ptp(X) / 5, min=0, max=np.ptp(X), vary=True)
            params["wy"].set(value=np.ptp(Y) / 5, min=0, max=np.ptp(Y), vary=True)
            params["centerx"].set(value=X.mean() + 1, min=X.min(), max=X.max())
            params["centery"].set(value=Y.mean() + 1, min=Y.min(), max=Y.max())
            params["amplitude"].set(value=d.max(), min=d.mean() / 2)
            params["bg"].set(d.min())

            error = 1  # /((d.flatten())**2+0.01)

            result = model.fit(
                d.flatten(),
                x=X.flatten(),
                y=Y.flatten(),
                params=params,
                weights=1 / error,
            )

            if args.vary_angle:
                axes[i].set_title(
                    f'{ex_wl}, ang:{np.rad2deg(result.params["rotation"].value):,.1f}',
                    fontsize=8,
                )
            else:
                axes[i].set_title(f"{ex_wl}")

            lmfit.report_fit(result, min_correl=1)
            # axes[i][1].contourf(X[i],Y[i],result.best_fit.reshape(X[i].shape),50,vmin=d.min(),vmax=d.max())
            resid = abs(d - result.best_fit.reshape(X.shape))
            axes1[i].contourf(X, Y, resid, 50, vmin=d.min(), vmax=d.max())

            resid_factor = resid.sum() / np.multiply(*d.shape)
            axes1[i].set_title(f"{ex_wl},erf={resid_factor:.1f}", fontsize=5)
            for k, v in result.params.items():
                tmp[k] = v.value
            res.append(tmp)

            for ax in axes:
                ax.set_xlim(X.min(), X.max())
                ax.set_ylim(Y.min(), Y.max())
                ax.set_aspect(1)
                ax.set_aspect(1)

    df = pd.DataFrame(res)
    df.sort_values(by=["ex_wl", "distance"], inplace=True)

    fig2, axes2 = plt.subplots(3, 1)

    tmp = []
    for i, g in df.groupby("ex_wl"):
        eq_x = np.poly1d(np.polyfit(g["distance"], g["wx"], 1))
        eq_y = np.poly1d(np.polyfit(g["distance"], g["wy"], 1))
        wx = eq_x(args.distance)
        wy = eq_y(args.distance)
        tmp.append(
            {"ex_wl": g.ex_wl.mean(), "wx": wx, "wy": wy, "w0_in": (wx + wy) / 2}
        )
    df1 = pd.DataFrame(tmp)

    axes2[0].plot(df1["ex_wl"], df1["wx"], ".-", label="wx")
    axes2[0].plot(df1["ex_wl"], df1["wy"], ".-", label="wy")
    axes2[0].plot(df1["ex_wl"], df1["w0_in"], "--", label="w0_in")
    axes2[0].legend()
    axes2[0].set_xlabel("Ex. wavelength, nm")
    axes2[0].set_ylabel(f"Radius at {args.distance}mm\n1/e$^2$, mm")
    axes20_ = axes2[0].twinx()
    axes20_.set_ylabel("FWHM, um")
    axes20_.set_ylim(
        axes2[0].get_ylim()[0] / np.sqrt(2 * np.log(2)),
        axes2[0].get_ylim()[1] / np.sqrt(2 * np.log(2)),
    )

    df2 = df.groupby(by="distance", as_index=False).mean()

    axes2[1].plot(df2["distance"], df2["centerx"], ".-", label="X")
    axes2[1].plot(df2["distance"], df2["centery"], ".-", label="Y")
    axes2[1].legend()
    axes2[1].set_xlabel("Distance, mm")
    axes2[1].set_ylabel("Center, mm")

    N = df.ex_wl.unique().shape[0]
    for i, g in list(df.groupby("ex_wl"))[:: (N // 5 + 1)]:
        axes2[2].plot(
            g["distance"],
            (g["wx"] + g["wy"]) / 2,
            "--o",
            label=f"{g.ex_wl.mean():.0f}nm",
        )

    axes2[2].set_xlabel("Distance, mm")
    axes2[2].set_ylabel(r"Radius 1/e$^2$, mm")
    axes2[2].legend()
    axes2[0].set_title(
        f"{output_type}. Divergence half-angle:\n {np.arctan((eq_x[1]+eq_y[1])/2)*1000:,.3f} (x:{np.arctan(eq_x[1])*1000:,.3f},y:{np.arctan(eq_y[1])*1000:,.3f}) mrad"
    )

    axes2[0].grid(1)
    axes2[1].grid(1)
    axes2[2].grid(1)

    dtype = [("ex_wl", "<f4"), ("w0_in", "<f8"), ("w0", "<f8")]
    out_data = np.zeros(len(df1), dtype=dtype)
    out_data["ex_wl"] = df1["ex_wl"]
    out_data["w0_in"] = df1["w0_in"] * 1e-3  # m
    with exdir.File(args.output) as store_out:
        if output_type in store_out:
            del store_out[output_type]

        timestamp = time.strftime("%Y%m%d-%H%M%S")
        path = f"HRS/Laser/BeamFocusing/{args.lens}/{output_type}"
        store_out.require_group(path)
        store_out[path].require_dataset(timestamp, data=out_data)

        store_out[path][timestamp].data[:] = out_data[:]
        path_lend = f"HRS/Laser/BeamFocusing/{args.lens}"
        for key, val in lens_config.items():
            store_out[path_lend].attrs[key] = val
plt.show()
