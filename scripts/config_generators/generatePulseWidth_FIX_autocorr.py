import exdir
import sys
import numpy as np
import matplotlib.pyplot as plt
import argparse
from scipy import signal
from scipy.integrate import quad, quad_vec
from scipy.interpolate import interp1d
from scipy.signal import medfilt, butter, filtfilt
from lmfit import Model
from lmfit.models import QuadraticModel
import itertools


sech = lambda x: 1 / np.cosh(x)


def I(t, t_p):
    return sech(1.7627 * t / t_p) ** 2


def E(t, t_p, wc):
    return I(t, t_p) ** 0.5 * np.exp(1j * wc * t)


# def E(t, t_p, wc):
# 	A = 0.5
# 	return np.exp(-(t/t_p)**2*(1+1j*A))*np.exp(1j*wc*t)


def func(t, tau, t_p=100, wc=1):
    return abs((E(t, t_p, wc) + E(t - tau, t_p, wc)) ** 2) ** 2


def func_high(t, tau, t_p=100, wc=0):
    return abs(E(t, t_p, wc).real + E(t - tau, t_p, wc).real) ** 4


def func_low(t, tau, t_p=100, wc=0):
    return abs(E(t, t_p, wc).real - E(t - tau, t_p, wc).real) ** 4


def func_norm(t, t_p=100, wc=1):
    return abs(E(t, t_p, wc)) ** 4


t = np.linspace(-1000, 1000, 1000)
almost_inf = 2000


def IAC(x, t0, A, A0, t_p, wc, a, b):
    t = x - t0
    f = lambda x: func(x, t, t_p, wc)
    f_norm = lambda x: func_norm(x, t_p, wc)
    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    r = np.nan_to_num(r, 0, neginf=0, posinf=0)
    res = A0 + A * r + a * t ** 2 + b * t

    print(t_p, wc, A, A0, t0)
    return res


def IAC_high(x, t0, A, A0, t_p, wc, a, b):
    t = x - t0
    f = lambda x: func_high(x, t, t_p, wc)
    f_norm = lambda x: func_norm(x, t_p, wc)
    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    r = np.nan_to_num(r, 0, neginf=0, posinf=0)
    res = A0 + A * r + a * t ** 2 + b * t

    print(t_p, wc, A, A0, t0)
    return res


def IAC_low(x, t0, A, A0, t_p, wc, a, b):
    t = x - t0
    f = lambda x: func_low(x, t, t_p, wc)
    f_norm = lambda x: func_norm(x, t_p, wc)
    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    r = np.nan_to_num(r, 0, neginf=0, posinf=0)
    res = A0 + A * r + a * t ** 2 + b * t

    print(t_p, wc, A, A0, t0)
    return res


def hl_envelopes(x, s, dmin=1, dmax=1, offset=0):
    # locals min
    lmin = (np.diff(np.sign(np.diff(s))) > 0).nonzero()[0] + 1
    # locals max
    lmax = (np.diff(np.sign(np.diff(s))) < 0).nonzero()[0] + 1
    # global max of dmax-chunks of locals max
    lmin = lmin[
        [i + np.argmin(s[lmin[i : i + dmin]]) for i in range(0, len(lmin), dmin)]
    ]
    # global min of dmin-chunks of locals min
    lmax = lmax[
        [i + np.argmax(s[lmax[i : i + dmax]]) for i in range(0, len(lmax), dmax)]
    ]

    iu_max = interp1d(x[lmax], s[lmax], bounds_error=False, fill_value="extrapolate")
    w = s >= (iu_max(x) - offset)
    s_max = s[w]
    x_max = x[w]

    iu_min = interp1d(x[lmin], s[lmin], bounds_error=False, fill_value="extrapolate")
    w = s <= (iu_min(x) + offset)
    s_min = s[w]
    x_min = x[w]

    return x_min, s_min, x_max, s_max


parser = argparse.ArgumentParser(
    description="Fit data from interfecometric autocorrelation measurements.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument("-f", dest="file", help="Exdir file path.")
parser.add_argument(
    "-b", dest="band", help="Band used as source of data.", default="2w1"
)


args = parser.parse_args()

print(args)

store = exdir.File(args.file, "r")
timestamps = list(store["scanND"].keys())
data_ = store["scanND"][timestamps[-2]]["data"]

data = data_[data_["position"]["Delay_line_position"] != 0]

delay_pos = (
    data["position"]["Delay_line_position"]
    - data["position"]["Delay_line_position"].mean()
)

delay = delay_pos * 2e-3 / 299792458 * 1e15  # fs

sig_ = data["data"]["AndorCamera"]["data"][args.band]

sig = sig_.copy()  # signal.detrend(sig_)#,bp=np.linspace(0,len(sig_),2,dtype=int))
# sig -= sig.min()
sig /= np.ptp(sig)
x = delay.astype(float)
y = sig.astype(float)
x -= np.mean(x[y > y.max() * 0.6])
# x -= abs(x*y).sum()/abs(y).sum()
w = abs(x) < 700
y = y[w]
x = x[w]
b, a = butter(3, 0.8)
y = filtfilt(b, a, y)
# eq = np.poly1d(np.polyfit(x,y,1))
# y /= eq(x)

y -= y.min()
y /= np.ptp(y)
iu = interp1d(x, y)

dt = 0.01  # sampling interval
Fs = 1 / dt  # sampling frequency
t = np.arange(x.min(), x.max(), dt)
s = iu(t)
plt.figure()
r = plt.magnitude_spectrum(s, Fs=Fs, color="C1")
ws_threshold_n = 10
wc = r[1][ws_threshold_n:][r[0][ws_threshold_n:] == r[0][ws_threshold_n:].max()][0]


ac_mod = Model(IAC)
params = ac_mod.make_params()

params["t_p"].set(150, min=100, max=250, vary=True)
params["t0"].set(0, min=-50, max=50, vary=True)
params["A"].set(0.046, min=0.00001, vary=True)
params["A0"].set(0.1, max=0.5, vary=True)
params["wc"].set(0.11, min=0.05, max=1, vary=True)

params["a"].set(0, vary=False)
params["b"].set(0, vary=True)

mod = ac_mod
init = mod.eval(params, x=x)
result = mod.fit(y, params, x=x)  # , weights=(y.mean()-y))#abs(x))
xi = np.linspace(x.min(), x.max(), 1000)
yi = mod.eval(result.params, x=xi)
print(result.fit_report())

plt.figure()
plt.plot(x, y, "o-", label="raw", alpha=0.7, markersize=5)
# plt.plot(x, init, '--', label='init')
plt.plot(xi, yi, "-g", label="fit")

x_min, y_min, x_max, y_max = hl_envelopes(x, y, offset=0.13)
# plt.plot(x_max, y_max, '+', label='raw_max')
# plt.plot(x_min, y_min, 'x', label='raw_min')


ac_high_mod = Model(IAC_high)
params_high = ac_high_mod.make_params()

params_high["t_p"].set(160, min=100, max=250, vary=True)
params_high["t0"].set(result.params["t0"].value, vary=False)
params_high["A"].set(0.046, min=0.00001, vary=True)
params_high["A0"].set(result.params["A0"].value, vary=False)
params_high["wc"].set(0, vary=False)


params_high["a"].set(result.params["a"].value, vary=False)
params_high["b"].set(result.params["b"].value, vary=False)

mod_high = ac_high_mod
init_high = mod_high.eval(params_high, x=x_max)
result_high = mod_high.fit(
    y_max, params_high, x=x_max
)  # , weights=(y.mean()-y))#abs(x))
xi_max = np.linspace(x_max.min(), x_max.max(), 1000)
yi_max = mod_high.eval(result_high.params, x=xi_max)
# plt.plot(x_max, init_high, 'x', label='init_max')

plt.plot(xi_max, yi_max, "-", label="fit_max")

print(result_high.fit_report())


#
# ac_low_mod = Model(IAC_low)
# params_low = ac_low_mod.make_params()
#
# params_low['t_p'].set(
# 	np.mean([result.params['t_p'].value,result_high.params['t_p'].value,]),
# 	min=100,max=200,vary=True)
# params_low['t0'].set(0,min=-50,max=50,vary=True)
# params_low['A'].set(
# 	np.mean([result.params['A'].value,result_high.params['A'].value,]),
# 	vary=True)
# params_low['A0'].set(np.mean([result.params['A0'].value,result_high.params['A0'].value,]),
# 	min=0,  vary=True)
# params_low['wc'].set(0,vary=False)
#
# params_low['a'].set(
# 	np.mean([result.params['a'].value,result_high.params['a'].value,]),
# 	vary=False)
# params_low['b'].set(
# 	np.mean([result.params['b'].value,result_high.params['b'].value,]),
# 	vary=False)
#
# mod_low = ac_low_mod
# init_low = mod_low.eval(params_low,x=x_min)
# result_low = mod_low.fit(y_min, params_low, x=x_min)#, weights=(y.mean()-y))#abs(x))
# xi_min = np.linspace(x_min.min(),x_min.max(),1000)
# yi_min = mod_low.eval(result_low.params,x=xi_min)
# #plt.plot(x_min, init_low, '-', label='init_min')
# plt.plot(xi_min, yi_min, '-', label='fit_min')
#
# print(result_low.fit_report())

plt.xlabel("Delay, fs")
plt.ylabel("Intensity, arb.un.")
plt.grid(1)
plt.title(
    f'FWHM:{result.params["t_p"].value:.3f} fs, FWHM(envelope):{result_high.params["t_p"].value:.3f} fs'
)
plt.legend()
plt.grid(1)
plt.tight_layout()
plt.show()
