import exdir
import numpy as np
from matplotlib import pyplot as plt
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.ptime as ptime
import pyqtgraph as pg
from scipy.ndimage import center_of_mass
from scipy.ndimage import gaussian_filter
from skimage.feature.peak import peak_local_max
from scipy.interpolate import griddata
from scipy.interpolate import LinearNDInterpolator, interp1d, splrep, splev, BSpline
from scipy.signal import medfilt, medfilt2d
import lmfit
from lmfit.lineshapes import gaussian, lorentzian
import time
import sys
import argparse
import pandas as pd
import scipy.special
import numpy_indexed as npi

parser = argparse.ArgumentParser(description="Fit single gaussian2d in scan3D.")
parser.add_argument(
    "-f", dest="file", help="Exdir with calibration for TUN, FIX or both outputs."
)
parser.add_argument(
    "-l",
    dest="laser_output",
    help="""
					Calibration for TUN, FIX or both outputs.
					Options: [TUN, FIX, ALL].
					*For "ALL" LaserShutter should be in scanning axes.
					""",
    default="ALL",
)

parser.add_argument(
    "-t",
    dest="timestamps",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in "scanND" folder.
					Integer index to process by index in sorted list of timestamps.
					Last measured "-1" by default.
					""",
    default=["-1"],
)
parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)
parser.add_argument(
    "--objective", dest="objective", help="Focusing objective.", default="LMM-40X-UUV"
)
parser.add_argument(
    "--smooth",
    dest="smooth",
    type=int,
    help="Extrapolate and smooth data.",
    default=None,
)

# parser.add_argument('-b', dest='band',
# 					help='Signal band.', default='2w')
#


def gaussian2d(
    x,
    y,
    bg=0,
    amplitude=1.0,
    centerx=0.0,
    centery=0.0,
    sigmax=1.0,
    sigmay=1.0,
    rotation=0,
):
    xp = (x - centerx) * np.cos(rotation) - (y - centery) * np.sin(rotation)
    yp = (x - centerx) * np.sin(rotation) + (y - centery) * np.cos(rotation)
    R = (xp / sigmax) ** 2 + (yp / sigmay) ** 2

    return bg + 2 * amplitude * gaussian(R) / (np.pi * sigmax * sigmay)
    # return amplitude*scipy.special.jv(0,alpha*R)**2/(np.pi*sigmax*sigmay)


args = parser.parse_args()

dataType = "scan3D"
store = exdir.File(args.file, "r")


def extractTimestamp(store, timestamps, dataType):
    timestamps_ = []
    keys = list(store[dataType])
    keys.sort()
    print(timestamps)

    if "ALL" in timestamps:
        timestamps_ = [k for k in store[dataType] if k[0] != "_"]
    else:
        for ts in timestamps:
            try:
                timestamps_.append(keys[int(ts)])
            except:
                traceback.print_exc()
                timestamps_.append(ts)
    print(timestamps_)
    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    dtype = store[dataType][timestamps_[-1]]["data"].dtype
    print(sizes, dtype, keys)
    data = np.zeros(np.sum(sizes), dtype=dtype)
    last_row = 0
    data_list = []
    for i, k in enumerate(timestamps_):
        print(i, k)
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        if w.sum() > 0:
            data_list.append(d[w])

    data_attrs = store[dataType][timestamps_[-1]]["data"].attrs.to_dict()

    # data = data[data['time']!=0]
    return data_list, data_attrs


data_list, data_attrs = extractTimestamp(store, args.timestamps, dataType)
rows = 4


if args.laser_output == "FIX":
    bands = ["2w1"]
elif args.laser_output == "TUN":
    bands = ["2w2"]
else:
    bands = ["2w2", "2w1"]

for band in bands:
    res = []
    for data in data_list:
        fig, axes = plt.subplots(
            rows, int(np.ceil(len(data) / rows)) + 1, sharex=True, sharey=True
        )
        fig1, axes1 = plt.subplots(
            rows, int(np.ceil(len(data) / rows)) + 1, sharex=True, sharey=True
        )

        if len(axes.shape) == 2:
            axes = axes.reshape(axes.shape[1] * axes.shape[0])
            axes1 = axes1.reshape(axes1.shape[1] * axes1.shape[0])
        if "w2" in band:
            output_type = "TUN"
        else:
            output_type = "FIX"

        fig.suptitle(output_type)

        for i, d in enumerate(data):
            if len(d) == 0:
                continue
            if "LaserShutter" in d["position"].dtype.names:
                if output_type == "FIX" and not d["position"][
                    "LaserShutter"
                ].mean() in [2, 3]:
                    continue
                if output_type == "TUN" and not d["position"][
                    "LaserShutter"
                ].mean() in [1, 3]:
                    continue

            tmp = {k: d["position"][k] for k in d["position"].dtype.names}
            ex_wl = tmp["LaserWavelength"]

            X = d["metadata"]["X"][..., -1].astype(float)
            Y = d["metadata"]["Y"][..., -1].astype(float)

            zero_mask = X == 0

            if zero_mask.sum() > 50:
                continue
            X[zero_mask] = X[~zero_mask].max()
            Y[zero_mask] = Y[~zero_mask].max()

            img_ = d["data"]["AndorCamera"]["data"][band][..., -1]
            img = img_.astype(float)
            img[zero_mask] = img_.mean()
            img[img == 0] = img.mean()
            # img[img>img.mean()*3]=img.mean()
            # img = gaussian_filter(img,(1,1))
            # if img.max()-img.mean()<img.std(): continue
            img = medfilt2d(img, 3)
            img[img == 0] = img_.mean()
            img = img - img.min()

            axes[i].contourf(X, Y, img, 50, vmin=img.min(), vmax=img.max())
            axes[i].set_title(f"{ex_wl}")
            if img.sum() == 0:
                continue
            model = lmfit.Model(gaussian2d, independent_vars=["x", "y"])
            params = model.make_params()
            params["rotation"].set(value=0.0, min=-np.pi / 4, max=np.pi / 4, vary=False)
            params["sigmax"].set(value=0.3, min=0.1, max=1.1, vary=True)
            params["sigmay"].set(value=0.3, min=0.1, max=1.1, vary=True)
            params["centerx"].set(
                value=(X * img).sum() / img.sum(), min=X.min(), max=X.max()
            )
            params["centery"].set(
                value=(Y * img).sum() / img.sum(), min=Y.min(), max=Y.max()
            )
            params["amplitude"].set(value=img.max(), min=img.min())
            params["bg"].set(value=img.min())
            error = 1  # /((d.flatten())**2+0.01)

            result = model.fit(
                img.flatten(),
                x=X.flatten(),
                y=Y.flatten(),
                params=params,
                weights=1 / error,
            )

            lmfit.report_fit(result)

            # axes[i][1].contourf(X[i],Y[i],result.best_fit.reshape(X[i].shape),50,vmin=d.min(),vmax=d.max())
            axes1[i].contourf(
                X,
                Y,
                img - result.best_fit.reshape(X.shape),
                50,
                vmin=img.min(),
                vmax=img.max(),
            )
            if "sigmay:     at boundary" in result.fit_report():
                continue
            if "sigmax:     at boundary" in result.fit_report():
                continue
            for k, v in result.params.items():
                tmp[k] = v.value
            res.append(tmp)

    for ax in axes:
        ax.set_aspect(1)
        ax.set_aspect(1)

    df = pd.DataFrame(res)

    fig2, axes2 = plt.subplots(2, 1, sharex=True)
    axes2[0].plot(df["LaserWavelength"], df["centerx"], ".-", label="Xc")
    axes2[0].plot(df["LaserWavelength"], df["centery"], ".-", label="Yc")
    axes2[0].legend()

    df["sigmax"] = df["sigmax"] * np.sqrt(2)
    df["sigmay"] = df["sigmay"] * np.sqrt(2)

    df["FWHMx"] = df["sigmax"] * 2 / 1.18
    df["FWHMy"] = df["sigmay"] * 2 / 1.18

    df["FWHM"] = (df["FWHMx"] + df["FWHMy"]) / 2
    df["sigma"] = (df["sigmax"] + df["sigmay"]) / 2

    df = df[df["sigma"] < 1]
    df = df.sort_values(by="LaserWavelength")
    weights = np.ones(len(df))
    weights[df.LaserWavelength < 900] = 0.8
    weights[df.LaserWavelength < 750] = 0.2
    weights[df.LaserWavelength > 1250] = 0.8

    axes2[1].plot(df["LaserWavelength"], df["sigmax"], ".-", label="sigmax")
    axes2[1].plot(df["LaserWavelength"], df["sigmay"], ".-", label="sigmay")
    axes2[1].plot(df["LaserWavelength"], df["sigma"], "--.", label="sigma")
    x = np.linspace(680, 1300, 1000)
    if not args.smooth is None:
        # eq = np.poly1d(np.polyfit(df['LaserWavelength'], df['sigma'], 3, w=weights))
        t, c, k = splrep(
            df["LaserWavelength"],
            df["sigma"],
            s=args.smooth,
            k=3,
        )  # w=weights)
        eq = BSpline(t, c, k, extrapolate=True)
        df["sigma"] = eq(df["LaserWavelength"])
        axes2[1].plot(x, eq(x), "--r", label="fit")
    axes2[1].legend()

    dtype = [("ex_wl", "<f4"), ("w0_in", "<f8"), ("w0", "<f8")]
    out_data = np.zeros(len(df), dtype=dtype)
    out_data["ex_wl"] = df["LaserWavelength"]
    out_data["w0"] = df["sigma"] * 1e-6  # m

    with exdir.File(args.output) as store_out:
        if output_type in store_out:
            del store_out[output_type]

        timestamp = time.strftime("%Y%m%d-%H%M%S")
        path = f"Microscope/Laser/BeamFocusing/{args.objective}/{output_type}"
        store_out.require_group(path)
        store_out[path].require_dataset(timestamp, data=out_data)

        store_out[path][timestamp].data[:] = out_data[:]


plt.show()
