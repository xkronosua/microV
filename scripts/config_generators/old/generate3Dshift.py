# -*- coding: utf-8 -*-
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl
import numpy as np
import time

import exdir
import sys
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter, medfilt
from pathlib import Path


def unique_dummy(array, return_index=False):
    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


path = sys.argv[1]

print(path)
store = exdir.File(path, "r")

dataType = "scanND"
if "MultiScan" in store:
    dataType = "MultiScan"

keys = [k for k in store[dataType] if k[0] != "_"]
sizes = [len(store[dataType][k]["data"]) for k in keys]

dtype = store[dataType][keys[-1]]["data"].dtype

data = np.zeros(np.sum(sizes), dtype=dtype)
last_row = 0
for i, k in enumerate(keys):
    if store[dataType][k]["data"].dtype == dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        data[last_row : last_row + w.sum()] = d[w]
        last_row += w.sum()
        print(i, k)
data = data[data["time"] != 0]

print("READY")


out_dtype = [("ex_wl", "<u2"), ("delay_0_pos", "<f8")]
out_data = np.zeros(0, dtype=out_dtype)


unique_ex_wls, unique_ex_wls_index = unique_dummy(
    data["position"]["LaserWavelength"], return_index=True
)

groups = np.split(data, unique_ex_wls_index)[1:]
ex_wls = np.unique(data["position"]["LaserWavelength"])[::-1]

Pos = np.zeros((len(ex_wls), 3))
for i, ex_wl in enumerate(ex_wls):
    print(f"{i}:\t{ex_wl}")
    d = groups[np.where(unique_ex_wls == ex_wl)[0][-1]]
    mask = d["position"]["CenterIndex"] == 0
    Pos[i, 0] = d["metadata"]["X_target"][mask][0]
    Pos[i, 1] = d["metadata"]["Y_target"][mask][0]
    Pos[i, 2] = d["metadata"]["Z_target"][mask][0]


out_data = out_data[out_data["ex_wl"] != 0]


app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.opts["distance"] = 20
w.show()
w.setWindowTitle("pyqtgraph example: GLScatterPlotItem")

g = gl.GLGridItem()
w.addItem(g)

Pos -= Pos[np.where(ex_wls == 1050)[0]]
Pos[11] = (Pos[10] + Pos[12]) / 2
Pos1 = Pos.copy()


Pos1[:, 0] = medfilt(Pos[:, 0], 3)
Pos1[:, 1] = medfilt(Pos[:, 1], 3)
Pos1[:, 2] = medfilt(Pos[:, 2], 3)
N = 2
eqx = np.poly1d(np.polyfit(ex_wls[:-3], Pos1[:, 0][:-3], N))
eqy = np.poly1d(np.polyfit(ex_wls[:-3], Pos1[:, 1][:-3], N))
eqz = np.poly1d(np.polyfit(ex_wls[:-3], Pos1[:, 2][:-3], N))
Pos1[:, 0] = eqx(ex_wls)
Pos1[:, 1] = eqy(ex_wls)
Pos1[:, 2] = eqz(ex_wls)

sp1 = gl.GLScatterPlotItem(pos=Pos, color=(0, 1, 0, 1), size=0.1, pxMode=False)
sp2 = gl.GLScatterPlotItem(pos=[[0, 0, 0]], color=(2, 0, 0, 1), size=1, pxMode=False)

l1 = gl.GLLinePlotItem(pos=Pos1, color=(1, 0, 0, 1))

w.addItem(sp1)
w.addItem(sp2)
w.addItem(l1)

## Start Qt event loop unless running in interactive mode.
if __name__ == "__main__":
    import sys

    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        QtGui.QApplication.instance().exec_()
