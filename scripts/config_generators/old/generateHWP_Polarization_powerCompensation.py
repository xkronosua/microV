import exdir
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
from lmfit import Model
import time

path = sys.argv[1]
store = exdir.File(path, "r")
data = store["scanND"][max(store["scanND"])]["data"]


def unique_dummy(array, return_index=False):
    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


unique_ex_wls, unique_ex_wls_index = unique_dummy(
    data["position"]["LaserWavelength"], return_index=True
)

groups = np.split(data, unique_ex_wls_index)[1:]


def func(x, x0, x01, y0, A, A1):
    return (
        y0 + A * np.sin(np.deg2rad((x - x0) * 4)) + A1 * np.sin(np.deg2rad((x - x01)))
    )


out_dtype = [
    ("ex_wl", "f4"),
    ("HWP_Polarization", "f4", 1000),
    ("correctionFactor", "f4", 1000),
]

out_data = []
zero_angle = 0
for i, d in enumerate(groups):

    ex_wl = d["position"]["LaserWavelength"].mean()
    x, y = (d["position"]["HWP_Polarization"], d["data"]["Powermeter"]["data"]["power"])
    mod = Model(func)
    pars = mod.make_params()
    pars["y0"].set(0.136, min=0, max=0.5)
    pars["x0"].set(61.35, vary=False, min=-90, max=90)
    pars["x01"].set(135, vary=True, min=-90, max=180)
    pars["A"].set(-0.004, min=-0.1, max=0.1)
    pars["A1"].set(-0.001, min=-0.1, max=0.1)
    out = mod.fit(y, pars, x=x)
    plt.figure()
    plt.title(f"{ex_wl}nm")
    angle = np.linspace(0, 360, 1000)
    y_new = mod.eval_components(params=out.params, x=angle)["func"]
    correctionFactor = y_new / y_new[0]
    plt.plot(x, y / y_new[0], "o")
    plt.plot(angle, correctionFactor, "-")
    tmp = np.zeros(1, dtype=out_dtype)
    tmp["ex_wl"] = ex_wl
    tmp["HWP_Polarization"] = angle
    tmp["correctionFactor"] = correctionFactor
    out_data.append(tmp)
    zero_angle = (zero_angle * i + out.params["x0"].value) / (i + 1)
    print(out.fit_report())
# zero_angle /= i+1

out_data = np.hstack(out_data)

output_type = "FIX" if "FIX" in path else "TUN"

with exdir.File("processed.exdir") as store_out:
    if "FIX_correctionFactor" in store_out and output_type == "FIX":
        del store_out["FIX_correctionFactor"]

    if "TUN_correctionFactor" in store_out and output_type == "TUN":
        del store_out["TUN_correctionFactor"]
    timestamp = time.strftime("%Y%m%d-%H%M%S")
    name = f"{output_type}_correctionFactor"
    store_out.require_group(name)
    store_out[name].require_dataset(timestamp, data=out_data)

    store_out[name][timestamp].attrs["zero_angle"] = float(zero_angle)
    store_out[name][timestamp].data[:] = out_data[:]


plt.show()
