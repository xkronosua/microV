import sys
import exdir
import numpy as np
from matplotlib import pyplot as plt
import argparse
from scipy.interpolate import interp1d, LinearNDInterpolator
from scipy.signal import medfilt
import pandas as pd

parser = argparse.ArgumentParser(
    description="Process AndorCamera sensitivity calibration curves."
)
parser.add_argument(
    "-s", dest="setup", help="Setup: HRS or Microscope", default="Microscope"
)
parser.add_argument(
    "-c",
    dest="configuration",
    help="Configuration of measuements [forward, backward, 90deg, ...]",
    default="forward",
)
parser.add_argument("-d", dest="data", help="Exdir folder with data")
parser.add_argument("-b", dest="background", help="Exdir folder with background")
parser.add_argument(
    "-g",
    dest="grating",
    type=int,
    help="Process data for specific grating [0,1]",
    default=None,
)
parser.add_argument(
    "-F",
    dest="filters",
    nargs="+",
    type=str,
    help="Filter datasheets",
    default=["FGB37x2.csv", "FESH0700.csv", "FESH0850.csv", "FESH1000.csv"],
)
parser.add_argument(
    "-L", dest="lamp", help="Lamp datasheet", default="USHIO_BRL_12V50W_1.csv"
)
parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)


args = parser.parse_args()
print(args)


lamp_data = np.loadtxt(args.lamp, delimiter=",")
lamp = interp1d(
    lamp_data[:, 0], lamp_data[:, 1], bounds_error=False, fill_value="extrapolate"
)


store = exdir.File(args.data, "r")
store_bg = exdir.File(args.background, "r")

data = store["scanND"][max(store["scanND"])]["data"]
data_bg = store_bg["scanND"][max(store_bg["scanND"])]["data"]

if not args.grating is None:
    mask = (data["position"]["shamrockGrating"] == args.grating) & (data["time"] != 0)
else:
    mask = data["time"] != 0

if "filtersPiezoStage" in data["position"].dtype.names:
    mask = mask & (data["position"]["filtersPiezoStage"] == 0)

mask = mask & (data["position"]["shamrockWavelength"] > 450)


filters = data.attrs["filters"].to_dict()

crop = 200
interps = []
lin_interps = []


wl = data["data"]["AndorCamera"]["raw"]["wavelength"][mask, crop:-crop].flatten()

intens = data["data"]["AndorCamera"]["raw"]["intensity"][mask, crop:-crop]
intens_bg = data_bg["data"]["AndorCamera"]["raw"]["intensity"][mask, crop:-crop]

exposure = data["data"]["AndorCamera"]["raw"]["exposure"][mask]
# exposure_bg = data_bg['data']['AndorCamera']['raw']['exposure'][mask]

intens_corrected = (((intens - intens_bg).T / exposure).T).flatten() / lamp(wl)
mask = (intens_corrected > 0) & (intens_corrected < 1e6) & (wl > 300)
wl = wl[mask]
intens_corrected = intens_corrected[mask]

plt.plot(wl, intens_corrected, "b.", label="raw")


intens_corrected = intens_corrected[wl.argsort()]
wl = wl[wl.argsort()]

wl = medfilt(wl, 151)
intens_corrected = medfilt(intens_corrected, 151)
plt.plot(wl, intens_corrected, "g.", label="filtered")
plt.legend()

plt.figure()
df = pd.DataFrame({"wavelength": wl, "None": intens_corrected})
plt.plot(df.wavelength, df["None"], label="None")

for name in args.filters:
    filter_path = name
    if not ".csv" in filter_path:
        filter_path = filter_path + ".csv"
    else:
        name = name.split(".csv")[0]
    try:
        datasheet_ = np.loadtxt(filter_path, skiprows=1, delimiter=",")
    except:
        datasheet_ = np.loadtxt(filter_path, skiprows=1, delimiter="\t")

    datasheet = interp1d(
        datasheet_[:, 0], datasheet_[:, 1], bounds_error=False, fill_value=0
    )

    df[name] = df["None"] * datasheet(df.wavelength)
    plt.plot(df.wavelength, df[name], label=name)

plt.legend()

output_name = f"AndorCameraCalibr_{args.setup}_{args.configuration}"
if not args.grating is None:
    output_name += f"_grating{args.grating}"
output_name += ".csv"

df.to_csv(output_name)

plt.show()
