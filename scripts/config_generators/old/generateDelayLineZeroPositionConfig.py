import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter
from pathlib import Path


def unique_dummy(array, return_index=False):
    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


def unique(array, return_index=False):
    _, idx = np.unique(array, return_index=True)
    idx = np.sort(idx)
    if return_index:
        return array[idx], idx
    else:
        return array[idx]


path = sys.argv[1]

print(path)
store = exdir.File(path, "r")

dataType = "scanND"
if "MultiScan" in store:
    dataType = "MultiScan"

keys = [k for k in store[dataType] if k[0] != "_"]
sizes = [len(store[dataType][k]["data"]) for k in keys]

dtype = store[dataType][keys[-1]]["data"].dtype

data = np.zeros(np.sum(sizes), dtype=dtype)
last_row = 0
for i, k in enumerate(keys):
    if store[dataType][k]["data"].dtype == dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        data[last_row : last_row + w.sum()] = d[w]
        last_row += w.sum()
        print(i, k)
data = data[data["time"] != 0]

print("READY")


out_dtype = [("ex_wl", "<u2"), ("delay_0_pos", "<f8")]
out_data = np.zeros(0, dtype=out_dtype)


unique_ex_wls, unique_ex_wls_index = unique_dummy(
    data["position"]["LaserWavelength"], return_index=True
)

groups = np.split(data, unique_ex_wls_index)[1:]
ex_wls = np.unique(data["position"]["LaserWavelength"])[::-1]

if len(ex_wls) > 10:
    fig, axes = plt.subplots(int(np.ceil(len(ex_wls) / 2)), 2)
    axes = axes.T.flatten()
else:
    fig, axes = plt.subplots(len(ex_wls), 1)
if len(ex_wls) == 1:
    axes = [axes]
ang_limits = []


if not "Delay_line_position" in data["metadata"].dtype.names:
    delay_zero_pos = np.load(Path("ZeroDelayPosition/data.npy"))
    delay_zero_pos_interp = interp1d(
        delay_zero_pos["ex_wl"], delay_zero_pos["delay_0_pos"]
    )

grid_N = 500
for i, ex_wl in enumerate(ex_wls):
    print(f"{i}:\t{ex_wl}")
    d = groups[np.where(unique_ex_wls == ex_wl)[0][-1]]

    out_data_tmp = np.zeros(1, dtype=out_dtype)
    # d = groups[i]
    # mask = indexes[i]
    if "Delay_line_position" in d["position"].dtype.names:
        x = d["position"][f"Delay_line_position"].astype(float)
    elif "Delay_line_position" in d["metadata"].dtype.names:
        x = d["metadata"][f"Delay_line_position"].astype(float)
    else:
        x = d["position"][f"Delay_line_position_zero_relative"].astype(float)
        x += delay_zero_pos_interp(ex_wl)
    y = d["data"]["AndorCamera"]["data"]["w1+w2"].astype(float)

    if "CenterIndex" in d["position"].dtype.names:
        mask = d["position"]["CenterIndex"] == 0
        x = x[mask]
        y = y[mask]

    k = unique(x[::-1], return_index=True)[1]
    x = x[::-1][k]
    y = y[::-1][k]

    # y = y[x.argsort()]
    # x = x[x.argsort()]
    try:
        b, a = butter(2, 0.925)
        y_filtered = filtfilt(b, a, y)
        out_data_tmp["ex_wl"] = ex_wl

        interp = interp1d(x, y_filtered, kind="cubic")
        xi = np.linspace(x.min(), x.max(), grid_N)
        y_new = interp(xi)

        out_data_tmp["delay_0_pos"] = x[y == y.max()]
        axes[i].plot(x, y, ".-", label=f"{ex_wl}")
        # axes[i].plot(xi,y_new,'-',label=f'{ex_wl}')
        axes[i].grid()
        axes[i].legend()
        out_data = np.hstack((out_data, out_data_tmp))
        print(out_data_tmp)
    except:
        traceback.print_exc()
        break


out_data = out_data[out_data["ex_wl"] != 0]
out_data = out_data[~np.isnan(out_data["delay_0_pos"])]
out_data.sort(order=["ex_wl", "delay_0_pos"])
out_data = out_data[::-1]

plt.figure()
plt.plot(out_data["ex_wl"], out_data["delay_0_pos"])

with exdir.File("processed.exdir") as store_out:
    if "ZeroDelayPosition" in store_out:
        del store_out["ZeroDelayPosition"]

    store_out.require_dataset(f"ZeroDelayPosition", data=out_data)

    store_out[f"ZeroDelayPosition"].data[:] = out_data[:]
    store_out[f"ZeroDelayPosition"].attrs["Callibration tested at wavelength"] = int(
        out_data["ex_wl"][0]
    )
    store_out[f"ZeroDelayPosition"].attrs["Zero delay was at position"] = float(
        out_data["delay_0_pos"][0]
    )


plt.show()
