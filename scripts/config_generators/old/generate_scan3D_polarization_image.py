import sys
import exdir
import numpy as np
import matplotlib.pyplot as plt
import argparse
import numpy_indexed as npi
from scipy.signal import medfilt

parser = argparse.ArgumentParser(description="Map polarization curves on scan3D data.")
parser.add_argument("-f", dest="file", help="Exdir file path.")
parser.add_argument("-b", dest="band", help="Signal band.", default="All")
parser.add_argument(
    "-s",
    dest="scan_index",
    help="Select scan by index.",
    nargs="+",
    type=int,
    default=[-1, -1],
)
parser.add_argument(
    "-l",
    dest="log10",
    help="View as log10",
    action="store_true",
)

args = parser.parse_args()

store = exdir.File(args.file, "r")

timestamps = [max(store["scan3D"]), max(store["MultiScan"])]
if args.scan_index[0] > 0:
    keys = list(store["scan3D"].keys())
    keys.sort()
    timestamps[0] = keys[args.scan_index[0]]
if args.scan_index[1] > 0:
    keys = list(store["MultiScan"].keys())
    keys.sort()
    timestamps[1] = keys[args.scan_index[1]]

data3D = store["scan3D"][timestamps[0]]["data"]
data_polar = store["MultiScan"][timestamps[1]]["data"]


img = data3D["data"]["AndorCamera"]["data"][args.band][0, ..., 0]  # .astype(float)
X = data3D["metadata"]["X"][0, ..., 0]
Y = data3D["metadata"]["Y"][0, ..., 0]
scale = data3D.attrs["scale"]
fig, axes = plt.subplots(1, 2, sharex=True, sharey=True)

img1 = (img - img.min()).astype(float)
img1 /= np.ptp(img1)
if args.log10:
    img1 = np.log10(abs(img1))
    axes[0].set_title("*log10")

axes[0].contourf(X, Y, img1, 100, cmap="turbo")

axes[1].contourf(X, Y, img1, 100, cmap="turbo", alpha=0.1)

# axes[0].set_aspect(1)

info = data_polar.attrs["centerIndex_info"].to_dict()
_index = data_polar["position"].dtype.names.index("CenterIndex")
w = data_polar["time"] != 0
norm_sig = np.ptp(data_polar[w]["data"]["AndorCamera"]["data"][args.band])
xlim = axes[1].get_xlim()
view_size = xlim[1] - xlim[0]
norm_sig = view_size / 10
colors = "rgbcmky" * 2
for d in npi.group_by(data_polar[w]["iteration"][:, _index]).split(data_polar[w]):
    centerIndex = int(d["position"]["CenterIndex"].mean())
    angle = d["position"]["HWP_Polarization"] * 2

    sig = d["data"]["AndorCamera"]["data"][args.band]
    # exposure = d['data']['AndorCamera']['exposure'][args.band]
    print(centerIndex)
    sig = medfilt(sig, 5) / np.ptp(sig) * norm_sig
    x = sig * np.cos(np.deg2rad(angle)) + info["X"][int(centerIndex)]
    y = sig * np.sin(np.deg2rad(angle)) + info["Y"][int(centerIndex)]
    axes[1].fill(x, y, color=colors[centerIndex], label=f"NP{centerIndex}", fill=False)
    axes[1].text(
        info["X"][int(centerIndex)] + 0.5,
        info["Y"][int(centerIndex)] + 0.5,
        s=f"NP{centerIndex}",
    )
axes[0].set_aspect(1)
axes[1].set_aspect(1)
fig.suptitle(args.file.split(".exdir")[0])
plt.show()
