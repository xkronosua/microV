import exdir
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import image
from scipy.interpolate import interp1d
from scipy.signal import medfilt
import lmfit
from lmfit.lineshapes import gaussian, lorentzian
import time
import sys
import argparse
import pandas as pd
from glob import glob

fig3, axes3 = plt.subplots(2, 1)
df_tun = pd.concat([pd.read_csv(f) for f in glob("TUN*.csv")])
df_fix = pd.concat([pd.read_csv(f) for f in glob("FIX*.csv")])
fig, axes = plt.subplots(2, 2)
df_tun = df_tun.sort_values(by=["distance", "LaserWavelength"])
df_fix = df_fix.sort_values(by=["distance", "delay"])

res = {"FIX": [], "TUN": []}
for p, g in df_fix.groupby(by="delay"):
    axes[0][0].plot(g["distance"], g["sigma"], "o-", label=f"{p}")
    eq = np.poly1d(np.polyfit(g["distance"], g["sigma"], 1))
    eq_1 = np.poly1d(np.polyfit(g["sigma"], g["distance"], 1))
    z0 = eq_1(0)
    print(p, z0)
    div = eq[1] * 2
    axes[0, 1].plot(p, div, "o")
    res["FIX"].append({"ex_wl": 1045, "w0_in": eq(840)})


for p, g in df_tun.groupby(by="LaserWavelength"):
    axes[1][0].plot(g["distance"], g["sigma"], "o-", label=f"{p}")

    eq = np.poly1d(np.polyfit(g["distance"], g["sigma"], 1))
    eq_1 = np.poly1d(np.polyfit(g["sigma"], g["distance"], 1))
    z0 = eq_1(0)
    print(p, z0)
    div = eq[1] * 2
    axes[1, 1].plot(p, div, "o")
    res["TUN"].append({"ex_wl": p, "w0_in": eq(1025)})


dtype = [("ex_wl", "<f4"), ("w0_in", "<f8"), ("w0", "<f8")]

with exdir.File("processed.exdir") as store_out:
    for laser_output in ["FIX", "TUN"]:
        if laser_output in store_out:
            del store_out[laser_output]
        df = pd.DataFrame(res[laser_output])
        out_data = np.zeros(len(df), dtype=dtype)
        out_data["ex_wl"] = df["ex_wl"]
        out_data["w0_in"] = df["w0_in"] * 1e-3  # m

        timestamp = time.strftime("%Y%m%d-%H%M%S")
        store_out.require_group(laser_output)
        store_out[laser_output].require_dataset(timestamp, data=out_data)

        store_out[laser_output][timestamp].data[:] = out_data[:]

axes[1][0].legend()
axes[0][0].legend()

plt.show()
