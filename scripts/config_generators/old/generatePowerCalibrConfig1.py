import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter
import time


def unique_dummy(array, return_index=False):
    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


path = sys.argv[1]

print(path)
store = exdir.File(path, "r")

dataType = "scanND"


keys = [k for k in store[dataType] if k[0] != "_"]
sizes = [len(store[dataType][k]["data"]) for k in keys]

dtype = store[dataType][keys[-1]]["data"].dtype

data = np.zeros(np.sum(sizes), dtype=dtype)
last_row = 0
for i, k in enumerate(keys):
    if store[dataType][k]["data"].dtype == dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        data[last_row : last_row + w.sum()] = d[w]
        last_row += w.sum()
        print(i, k)
data = data[data["time"] != 0]


out_dtype = [("Ex_wavelength", "u2"), ("HWP_angle", "f8"), ("power", "f8")]
out_data = np.zeros(0, dtype=out_dtype)

if "FIX" in path:
    output_type = "FIX"
else:
    output_type = "TUN"

unique_ex_wls, unique_ex_wls_index = unique_dummy(
    data["position"]["LaserWavelength"], return_index=True
)

groups = np.split(data, unique_ex_wls_index)[1:]
ex_wls = np.unique(data["position"]["LaserWavelength"])


if len(ex_wls) > 10:
    fig, axes = plt.subplots(int(np.ceil(len(ex_wls) / 2)), 2, sharex=True)
    axes = axes.T.flatten()
else:
    fig, axes = plt.subplots(len(ex_wls), 1, sharex=True)
if len(ex_wls) == 1:
    axes = [axes]
ang_limits = []


grid_N = 500
for i, ex_wl in enumerate(ex_wls):
    print(f"{i}:\t{ex_wl}")

    # mask = data['position']['LaserWavelength']==ex_wl

    d = groups[np.where(unique_ex_wls == ex_wl)[0][-1]]

    out_data_tmp = np.zeros(grid_N, dtype=out_dtype)
    # d = groups[i]
    # mask = indexes[i]
    x = d["position"][f"HWP_{output_type}_angle"].astype(float)
    y = d["data"]["Powermeter"]["data"]["power"].astype(float)
    k = np.unique(x, return_index=True)[1]
    x = x[k]
    y = y[k]

    # y = y[x.argsort()]
    # x = x[x.argsort()]
    try:
        b, a = butter(2, 0.325)
        y_filtered = filtfilt(b, a, y)
        out_data_tmp["Ex_wavelength"] = ex_wl

        interp = interp1d(x, y_filtered, kind="cubic")
        xi = np.linspace(x.min(), x.max(), grid_N)
        y_new = interp(xi)

        out_data_tmp["HWP_angle"] = xi
        out_data_tmp["power"] = y_new
        ang_limits.append([xi[y_new == y_new.min()][0], xi[y_new == y_new.max()][0]])
        axes[i].plot(x, y, ".", label=f"_{ex_wl}")
        axes[i].plot(xi, y_new, "-", label=f"{ex_wl}")
        axes[i].grid()
        axes[i].legend()
        out_data = np.hstack((out_data, out_data_tmp))
    except:
        traceback.print_exc()
        break


ang_limits = np.array(ang_limits).mean(axis=0)
out_data = out_data[out_data["Ex_wavelength"] != 0]
out_data = out_data[~np.isnan(out_data["HWP_angle"])]
out_data.sort(order=["Ex_wavelength", "HWP_angle"])


# for i,ex_wl in enumerate(ex_wls):
# 	print(f'{i}:\t{ex_wl}')
# 	mask = out_data['Ex_wavelength']==ex_wl
# 	x = out_data['HWP_angle'][mask]
# 	y = out_data['power'][mask]

# 	axes[i].plot(x,y,'--',label=f'_{ex_wl}')

with exdir.File("processed.exdir") as store_out:
    if "HWP_FIX" in store_out and output_type == "FIX":
        del store_out["HWP_FIX"]

    if "HWP_TUN" in store_out and output_type == "TUN":
        del store_out["HWP_TUN"]
    timestamp = time.strftime("%Y%m%d-%H%M%S")
    store_out.require_group(f"HWP_{output_type}")
    store_out[f"HWP_{output_type}"].require_dataset(timestamp, data=out_data)

    store_out[f"HWP_{output_type}"][timestamp].attrs[
        "power2angle_interpLimits"
    ] = ang_limits.tolist()
    store_out[f"HWP_{output_type}"][timestamp].data[:] = out_data[:]


plt.show()
