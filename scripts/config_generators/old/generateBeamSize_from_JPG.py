import exdir
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import image
from scipy.interpolate import interp1d
from scipy.signal import medfilt
import lmfit
from lmfit.lineshapes import gaussian, lorentzian
import time
import sys
import argparse
import pandas as pd


def gaussian2d(
    x, y, amplitude=1.0, centerx=0.0, centery=0.0, sigmax=1.0, sigmay=1.0, rotation=0
):
    xp = (x - centerx) * np.cos(rotation) - (y - centery) * np.sin(rotation)
    yp = (x - centerx) * np.sin(rotation) + (y - centery) * np.cos(rotation)
    R = (xp / sigmax) ** 2 + (yp / sigmay) ** 2

    return 2 * amplitude * gaussian(R) / (np.pi * sigmax * sigmay)


parser = argparse.ArgumentParser(description="Fit beam from images captured by camera.")
parser.add_argument("-f", dest="file", nargs="+", help="Files with beam profile.")
parser.add_argument("-l", dest="laser_output", help="Laser output.", default="FIX")
parser.add_argument("-o", dest="output", help="Save data to file.", default="res.csv")

args = parser.parse_args()


fnames = list(args.file)
if args.laser_output == "TUN":
    fnames.sort(key=lambda x: int(x.split("_")[1].split("nm")[0]))
else:
    fnames.sort(key=lambda x: int(x.split("delay")[1].split("mm")[0]))
data = [image.imread(f)[:, :, 0].T for f in fnames]
pixel_size = 6e-3  # mm
X, Y = np.mgrid[0 : data[0].shape[0] : 1, 0 : data[0].shape[1] : 1] * pixel_size


def rebin(arr, new_shape):
    shape = (
        new_shape[0],
        arr.shape[0] // new_shape[0],
        new_shape[1],
        arr.shape[1] // new_shape[1],
    )
    return arr.reshape(shape).mean(-1).mean(1)


bin = 5
X = rebin(X, np.array(X.shape) // bin)
Y = rebin(Y, np.array(Y.shape) // bin)

rows = 4

fig, axes = plt.subplots(
    rows, int(np.ceil(len(data) / rows)) + 1, sharex=True, sharey=True
)
fig1, axes1 = plt.subplots(
    rows, int(np.ceil(len(data) / rows)) + 1, sharex=True, sharey=True
)
res = []

axes = axes.reshape(axes.shape[1] * axes.shape[0])
axes1 = axes1.reshape(axes1.shape[1] * axes1.shape[0])
prev_center = [X[0].mean(), Y[0].mean()]

for i, dd in enumerate(data):
    fname = fnames[i]
    d = rebin(dd, np.array(dd.shape) // bin) - dd.min()
    if args.laser_output == "FIX":
        ex_wl = 1045
        delay = int(fname.split("delay")[-1].split("mm")[0])
    else:
        ex_wl = int(fname.split("_")[-1].split("nm")[0])
        delay = 0
    pos = int(fname.split(args.laser_output)[-1].split("mm")[0])

    tmp = {"LaserWavelength": ex_wl, "distance": pos, "delay": delay}
    print(i, tmp)
    axes[i].contourf(X, Y, d, 50, vmin=d.min(), vmax=d.max())
    axes[i].set_title(f"{ex_wl}:{pos}")
    if d.sum() == 0:
        break
    model = lmfit.Model(gaussian2d, independent_vars=["x", "y"])
    params = model.make_params()
    params["rotation"].set(value=0.0, min=-np.pi / 4, max=np.pi / 4, vary=False)
    params["sigmax"].set(value=1, min=0.5, max=3, vary=True)
    params["sigmay"].set(value=1, min=0.5, max=3, vary=True)
    params["centerx"].set(value=prev_center[0] + 1, min=X.min(), max=X.max())
    params["centery"].set(value=prev_center[1] + 1, min=Y.min(), max=Y.max())
    params["amplitude"].set(value=d.max() * 3, min=d.mean())
    error = 1  # /((d.flatten())**2+0.01)

    result = model.fit(
        d.flatten(), x=X.flatten(), y=Y.flatten(), params=params, weights=1 / error
    )

    lmfit.report_fit(result)
    # axes[i][1].contourf(X[i],Y[i],result.best_fit.reshape(X[i].shape),50,vmin=d.min(),vmax=d.max())
    axes1[i].contourf(
        X, Y, d - result.best_fit.reshape(X.shape), 50, vmin=d.min(), vmax=d.max()
    )
    for k, v in result.params.items():
        tmp[k] = v.value
    res.append(tmp)
    prev_center[0] = tmp["centerx"]
    prev_center[1] = tmp["centery"]


for ax in axes:
    ax.set_xlim(X.min(), X.max())
    ax.set_ylim(Y.min(), Y.max())
    ax.set_aspect(1)
    ax.set_aspect(1)

df = pd.DataFrame(res)


fig2, axes2 = plt.subplots(2, 1)
if args.laser_output == "FIX":
    ax = "delay"
else:
    ax = "LaserWavelength"

axes2[0].plot(df[ax], df["centerx"], ".-", label="Xc")
axes2[0].plot(df[ax], df["centery"], ".-", label="Yc")
axes2[0].legend()

df["sigma"] = (df["sigmax"] + df["sigmay"]) / 2
df["FWHMx"] = df["sigmax"] * 2 / 1.18
df["FWHMy"] = df["sigmay"] * 2 / 1.18
df["FWHM"] = (df["FWHMx"] + df["FWHMy"]) / 2

df["FWHM"] = medfilt(df["FWHM"], 3)
df["sigma"] = medfilt(df["sigma"], 3)

axes2[1].plot(df[ax], df["sigmax"], ".-", label="sigmax")
axes2[1].plot(df[ax], df["sigmay"], ".-", label="sigmay")
axes2[1].plot(df[ax], df["sigma"], "--.", label="sigma")


axes2[1].legend()

df.to_csv(args.output)

# dtype = [('ex_wl', '<f4'), ('w0_in', '<f8'), ('w0', '<f8')]
# out_data = np.zeros(len(df),dtype=dtype)
# out_data['ex_wl'] = df['LaserWavelength']
# out_data['w0_in'] = df['sigma']
#
# with exdir.File('processed.exdir') as store_out:
# 	if args.laser_output in store_out:
# 		del store_out[args.laser_output]
#
# 	timestamp = time.strftime("%Y%m%d-%H%M%S")
# 	store_out.require_group(args.laser_output)
# 	store_out[args.laser_output].require_dataset(timestamp, data=out_data)
#
# 	store_out[args.laser_output][timestamp].data[:] = out_data[:]


plt.show()
