import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter
import time
import numpy_indexed as npi
import argparse
import pandas as pd
from lmfit.models import GaussianModel, ConstantModel, ExponentialModel
from pathlib import Path

from scipy.integrate import quad, quad_vec
from lmfit import Model
from lmfit.models import QuadraticModel
from scipy.signal import medfilt, butter, filtfilt, find_peaks
import itertools
import traceback


sech = lambda x: 1 / np.cosh(x)


def I(t, t_p):
    return sech(1.7627 * t / t_p) ** 2


def func(t, tau, t_p1=100, t_p2=100):
    return I(t - tau, t_p1) * I(t, t_p2)


almost_inf = 2000


def IAC(x, t0, A0, A, t_p1=100, t_p2=100):
    t = x - t0
    # print(t)
    f = lambda x: func(x, t, t_p1, t_p2)
    f_norm = lambda x: func(x, 0, t_p1, t_p2)

    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    # r = np.nan_to_num(r,0, neginf=0, posinf=0)
    res = A0 + A * r
    # print(t0, A0, A, t_p1, t_p2)
    return res


parser = argparse.ArgumentParser(description="Process data for multiscan measurements.")
parser.add_argument("-f", dest="file", help="Exdir to process")

parser.add_argument(
    "-t",
    dest="timestamps",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in "MultiScan" folder.
					Integer index to process by index in sorted list of timestamps.
					Last measured "-1" by default.
					""",
    default=["-1"],
)

parser.add_argument(
    "--FIX",
    dest="FIX_pulse_width",
    type=float,
    help="Pulse duration for FIX output in fs",
    default=154,
)

parser.add_argument(
    "-s",
    dest="setup",
    help="Experimental setup. [Microscope, HRS]",
    default="Microscope",
)

parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)


args = parser.parse_args()
print(args)

dataType = "MultiScan"
store = exdir.File(args.file, "r")


def extractTimestamp(store, timestamps, dataType):
    timestamps_ = []
    keys = list(store[dataType])
    keys.sort()
    print(timestamps)

    if "ALL" in timestamps:
        timestamps_ = [k for k in store[dataType] if k[0] != "_"]
    else:
        for ts in timestamps:
            try:
                timestamps_.append(keys[int(ts)])
            except:
                traceback.print_exc()
                timestamps_.append(ts)
    print(timestamps_)
    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    dtype = store[dataType][timestamps_[-1]]["data"].dtype

    data = np.zeros(np.sum(sizes), dtype=dtype)
    last_row = 0
    data_dict = {}
    for i, k in enumerate(timestamps_):
        # if store[dataType][k]['data'].dtype==dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        if w.sum() > 0:
            data_dict[k] = d
        # data[last_row:last_row+w.sum()] = d[w]
        # last_row+=w.sum()
        # print(i,k)
    data_attrs = store[dataType][timestamps_[-1]]["data"].attrs.to_dict()
    # data = data[data['time']!=0]

    return data_dict, data_attrs


centers = lambda ex_wl: {
    "2w1": 1045 / 2,
    "2w2": ex_wl / 2,
    "3w1": 1045 / 3,
    "3w2": ex_wl / 3,
    "w1+w2": 1 / (1 / 1045 + 1 / ex_wl),
    # '2w1-w2': 1/(2/1045-1/ex_wl),
    "2w2-w1": 1 / (-1 / 1045 + 2 / ex_wl),
    "2w1+w2": 1 / (2 / 1045 + 1 / ex_wl),
    "2w2+w1": 1 / (1 / 1045 + 2 / ex_wl),
}


def fit_data(wl, intens, ex_wl, centers, filter_index, sync=False, plot=False):
    w = (~np.isnan(intens)) & (~np.isnan(wl))
    y = intens[w]
    x = wl[w].astype(float)
    intens_max = np.ptp(intens[w])
    y = (intens[w] / intens_max).astype(float)
    w = (x > 350) & (x < 650)
    x = x[w]
    y = y[w]

    if len(x) == 0:
        raise Exception("aaaaa")

    iu = interp1d(x, y, bounds_error=False, fill_value=0)
    model = ConstantModel(prefix="bg_")
    params = model.make_params()
    # if ex_wl<=725:
    # 	params['bg_c'].set(y.min(), min=-0.1, max=0.1, vary=False)
    # else:
    params["bg_c"].set(0, min=-0.01, max=0.01, vary=False)

    for k in centers:
        y_init = abs(iu(np.linspace(centers[k] - 10, centers[k] + 10, 100)).max())
        # if centers[k]>x.min() and centers[k]<x.max():
        pref = f"g{k}_".replace("+", "p").replace("-", "m")
        g_model = GaussianModel(prefix=pref)
        model += g_model
        params += g_model.make_params()
        vary = True
        if y_init <= 1e-8:
            vary = False
            params[pref + "center"].set(
                centers[k], min=centers[k] - 3, max=centers[k] + 3, vary=vary
            )
            params[pref + "sigma"].set(1.6, min=1, max=3.5, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=y_init / 20, max=10, vary=vary
            )

        elif (k in ["w1+w2", "2w2+w1", "2w1+w2", "2w2-w1"]) and not sync:
            vary = False
            y_init = 0
            params[pref + "center"].set(
                centers[k], min=centers[k] - 3, max=centers[k] + 3, vary=vary
            )
            params[pref + "sigma"].set(1.6, min=1, max=3.5, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=y_init / 20, max=10, vary=vary
            )

        elif k in ["3w1", "3w2", "2w2-w1"]:
            vary = True
            params[pref + "center"].set(
                centers[k] - 3, min=centers[k] - 5, max=centers[k] + 2, vary=vary
            )
            params[pref + "sigma"].set(3.6, min=1, max=5, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=y_init / 20, max=y.max() * 10, vary=vary
            )

        else:
            params[pref + "center"].set(
                centers[k], min=centers[k] - 3, max=centers[k] + 3, vary=vary
            )
            params[pref + "sigma"].set(1.6, min=1, max=3.5, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=y_init / 20, max=10, vary=vary
            )

    out = model.fit(y, x=x, params=params)
    print(out.fit_report(min_correl=0.99))
    if plot:
        plt.plot(x, y, ".")
        plt.plot(x, out.best_fit, "-")
    res = {}
    for k in centers:
        res[k] = (
            out.params["g" + k.replace("+", "p").replace("-", "m") + "_amplitude"].value
            * intens_max
        )
    return res, (x, out.best_fit * intens_max, model, out)


data_dict, data_attrs = extractTimestamp(store, args.timestamps, dataType)


filters = data_attrs["filters"]
currentFilter = filters[data_attrs["filtersPiezoStage"]]
currentFilter_index = 0
info = data_attrs["centerIndex_info"]

# lamp_data = pd.read_csv('AndorCameraCalibr_Microscope_forward.csv')
# # dichroicMirror_data = pd.read_csv('DMSP650R.csv')
#
# lamp_correction = {}
# for f in filters.values():
# 	# tmp = interp1d(dichroicMirror_data.wavelength, dichroicMirror_data['T'],
# 	# 		bounds_error=False, fill_value=np.nan)
# 	wl = lamp_data.wavelength
# 	correction = lamp_data[f]#*tmp(wl)
# 	w = correction>1000
#
# 	lamp_correction[f] = interp1d(wl[w], correction[w],
# 			bounds_error=False, fill_value=np.nan)

res = []

ac_mod = Model(IAC)
params = ac_mod.make_params()
params["t_p1"].set(args.FIX_pulse_width, min=100, max=250, vary=False)
params["t_p2"].set(100, min=70, max=300, vary=True)
params["t0"].set(0, min=-200, max=200, vary=True)
params["A"].set(1, min=0.8, max=1.1, vary=True)
params["A0"].set(0, min=-0.05, max=0.05, vary=True)
mod = ac_mod

for timestamp, data in data_dict.items():
    for g in npi.group_by(data["position"]["LaserWavelength"]).split(data):
        ex_wl = g["position"]["LaserWavelength"].mean()
        if ex_wl == 0:
            continue

        mask = g["position"]["CenterIndex"] == 0
        if mask.sum() < 4:
            continue
        delay = (
            g["position"]["Delay_line_position_zero_relative"][mask]
            * 2
            / 299792458
            * 1e12
        )

        sig = g["data"]["AndorCamera"]["data"]["w1+w2"][mask]
        fig, axes = plt.subplots(1, 1)
        t = delay.astype(float)
        y = (sig - sig.min()) / np.ptp(sig)
        y = y.astype(float)
        y = y[t.argsort()]
        t = t[t.argsort()]

        if abs(t[y == y.max()]) > 100:
            continue
        if t.min() > 500 or t.max() < 500:
            continue
        if len(t) < 35:
            continue

        axes.plot(t, y, "o")
        result = mod.fit(y, params, x=t)
        print(ex_wl)
        print(result.fit_report(min_correl=0.99))

        # xi = np.linspace(-0.1,0.1)
        axes.plot(t, result.best_fit, "-r")
        axes.set_title(f"@{ex_wl}")
        res.append({"Ex_wl": ex_wl, "pulsewidth": result.params["t_p2"].value})

df = pd.DataFrame(res)
df = df[df.pulsewidth < 290]
df.sort_values(by="Ex_wl", inplace=True)
plt.figure()
df1 = df.groupby("Ex_wl", as_index=False).min()
plt.plot(df.Ex_wl, df.pulsewidth, "o-")
plt.plot(df1.Ex_wl, df1.pulsewidth, "o-")

datasheet_data = np.load("data.npy")
datasheet = interp1d(
    datasheet_data["Ex_wl"][1:-1],
    datasheet_data["pulsewidth"][1:-1] * 1e15,
    bounds_error=False,
    fill_value="extrapolate",
)


plt.plot(datasheet_data["Ex_wl"][1:-1], datasheet_data["pulsewidth"][1:-1] * 1e15, "-")
df1["pulsewidth_correction"] = df1.pulsewidth / datasheet(df1.Ex_wl)
df1.to_csv("pulsewidth_correction.csv", index=False)

df2 = df1[["Ex_wl", "pulsewidth"]]
df2["pulsewidth"] *= 1e-15
out_data = df2.to_records(index=False)

with exdir.File(args.output) as store_out:

    timestamp = time.strftime("%Y%m%d-%H%M%S")
    path = args.setup + "/Laser/pulsewidth/"
    store_out.require_group(path)
    store_out[path].require_dataset(timestamp, data=out_data)
    store_out[path][timestamp].attrs["pulsewidth_IR"] = args.FIX_pulse_width * 1e-15


plt.show()
