import exdir
import numpy as np
from matplotlib import pyplot as plt
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.ptime as ptime
import pyqtgraph as pg
from scipy.ndimage import center_of_mass
from scipy.ndimage import gaussian_filter
from skimage.feature.peak import peak_local_max
from scipy.interpolate import griddata
from scipy.interpolate import LinearNDInterpolator, interp1d, splrep, splev, BSpline
from scipy.signal import medfilt, medfilt2d
import lmfit
from lmfit.lineshapes import gaussian, lorentzian
import time
import sys
import argparse
import pandas as pd
import scipy.special
import numpy_indexed as npi
import yaml
from io import StringIO
import traceback

parser = argparse.ArgumentParser(
    description="Fit single gaussian2d in scan3D.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "-f", dest="file", help="Exdir with calibration for TUN, FIX or both outputs."
)
parser.add_argument(
    "-l",
    dest="laser_output",
    help="""
					Calibration for TUN, FIX or both outputs.
					Options: [TUN, FIX, ALL].
					*For "ALL" LaserShutter should be in scanning axes.
					""",
    default="ALL",
)

parser.add_argument(
    "-t",
    dest="timestamps",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in "scanND" folder.
					Integer index to process by index in sorted list of timestamps.
					""",
    default=["-1"],
)
parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)

parser.add_argument(
    "--smooth",
    dest="smooth",
    type=int,
    help="Smooth factor. If defined data will be smoothed by b-spline and extrapolated if needed. Optimal value: 2",
    default=None,
)

parser.add_argument(
    "--vary_angle",
    dest="vary_angle",
    action="store_true",
    help="Vary angle (for elliptical shape).",
)

parser.add_argument(
    "--objective", dest="objective", help="Focusing objective.", default="LMM-40X-UUV"
)
parser.add_argument(
    "--objective_config",
    dest="objective_config",
    help="Focusing objective parameters.",
    default='{"Numerical aperture": 0.50, "Working distance[mm]": 7.8, "Focal length[mm]": 5.0, "Waist-sample distance[mm]": 0}',
)

parser.add_argument(
    "--final_size",
    dest="final_size",
    help="Select how to process X and Y sizes of 2D image in order to obtaine single value. Options: [mean, min, max, X, Y]",
    default="mean",
)


parser.add_argument(
    "--indexing",
    dest="indexing",
    help="Defalt indexing of laser outputs [for processing data older than ~Sep1012 should be \"0213\"]. 0 - both closed, 1 - FIX, 2 - TUN, 3 - both opened",
    default="0123",
)

# parser.add_argument('-b', dest='band',
# 					help='Signal band.', default='2w')
#

args = parser.parse_args()
print(args)


LaserShutterIndexes = {key:int(index) for key,index in zip(["CLOSED","FIX", "TUN", "OPENED"], args.indexing)}

def gaussianBeam2d(
    x,
    y,
    bg=0,
    amplitude=1.0,
    centerx=0.0,
    centery=0.0,
    wx=1.0,
    wy=1.0,
    rotation=0,
    profile=None,
):
    xp = (x - centerx) * np.cos(rotation) - (y - centery) * np.sin(rotation)
    yp = (x - centerx) * np.sin(rotation) + (y - centery) * np.cos(rotation)
    return bg + amplitude * np.exp(-2 * ((xp / wx) ** 2 + (yp / wy) ** 2))


dataType = "scan3D"
store = exdir.File(args.file, "r")


def extractTimestamp(store, timestamps, dataType):
    timestamps_ = []
    keys = list(store[dataType])
    keys.sort()
    print(timestamps)

    if "ALL" in timestamps:
        timestamps_ = [k for k in store[dataType] if k[0] != "_"]
    else:
        for ts in timestamps:
            try:
                timestamps_.append(keys[int(ts)])
            except:
                traceback.print_exc()
                timestamps_.append(ts)
    print(timestamps_)
    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    dtype = store[dataType][timestamps_[-1]]["data"].dtype
    print(sizes, dtype, keys)
    data = np.zeros(np.sum(sizes), dtype=dtype)
    last_row = 0
    data_dict = {}
    for i, k in enumerate(timestamps_):
        print(i, k)
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        if w.sum() > 0:
            data_dict[k] = d[w]

    data_attrs = store[dataType][timestamps_[-1]]["data"].attrs.to_dict()

    # data = data[data['time']!=0]
    return data_dict, data_attrs


data_dict, data_attrs = extractTimestamp(store, args.timestamps, dataType)
rows = 4


if args.laser_output == "FIX":
    bands = ["2w1"]
elif args.laser_output == "TUN":
    bands = ["2w2"]
else:
    bands = ["2w2", "2w1"]

objective_config = yaml.safe_load(StringIO(args.objective_config))


for band in bands:
    res = []
    for timestamp, data in data_dict.items():
        fig, axes = plt.subplots(
            rows, int(np.ceil(len(data) / rows)) + 1, sharex=True, sharey=True
        )
        fig1, axes1 = plt.subplots(
            rows, int(np.ceil(len(data) / rows)) + 1, sharex=True, sharey=True
        )

        if len(axes.shape) == 2:
            axes = axes.reshape(axes.shape[1] * axes.shape[0])
            axes1 = axes1.reshape(axes1.shape[1] * axes1.shape[0])
        if "w2" in band:
            output_type = "TUN"
        else:
            output_type = "FIX"

        fig.suptitle(f"{timestamp}:{output_type}")
        fig1.suptitle(f"{timestamp}:{output_type} (residuals)")

        for i, d in enumerate(data):
            if len(d) == 0:
                continue
            if "LaserShutter" in d["position"].dtype.names:
                if output_type == "FIX" and not d["position"][
                    "LaserShutter"
                ].mean() in [LaserShutterIndexes['OPENED'], LaserShutterIndexes['FIX']]:
                    continue
                if output_type == "TUN" and not d["position"][
                    "LaserShutter"
                ].mean() in [LaserShutterIndexes['OPENED'], LaserShutterIndexes['TUN']]:
                    continue

            tmp = {k: d["position"][k] for k in d["position"].dtype.names}
            ex_wl = tmp["LaserWavelength"]

            X = d["metadata"]["X"][..., -1].astype(float)
            Y = d["metadata"]["Y"][..., -1].astype(float)

            zero_mask = X == 0

            if zero_mask.sum() > 50:
                continue
            X[zero_mask] = X[~zero_mask].max()
            Y[zero_mask] = Y[~zero_mask].max()

            img_ = d["data"]["AndorCamera"]["data"][band][..., -1]
            img = img_.astype(float)
            img[zero_mask] = img_.mean()
            img[img == 0] = img.mean()
            # img[img>img.mean()*3]=img.mean()
            # img = gaussian_filter(img,(1,1))
            # if img.max()-img.mean()<img.std(): continue
            img = medfilt2d(img, 3)
            img[img == 0] = img_.mean()
            img = img - img.min()

            axes[i].contourf(X, Y, img, 50, vmin=img.min(), vmax=img.max())
            if img.sum() == 0:
                continue
            model = lmfit.Model(gaussianBeam2d, independent_vars=["x", "y"])
            params = model.make_params()
            params["rotation"].set(
                value=0.0, min=0, max=np.pi / 4, vary=args.vary_angle
            )
            params["wx"].set(value=0.3, min=0.1, max=1.5, vary=True)
            params["wy"].set(value=0.3, min=0.1, max=1.5, vary=True)
            params["centerx"].set(
                value=(X * img).sum() / img.sum(), min=X.min(), max=X.max()
            )
            params["centery"].set(
                value=(Y * img).sum() / img.sum(), min=Y.min(), max=Y.max()
            )
            params["amplitude"].set(value=img.max(), min=img.min())
            params["bg"].set(value=img.min())
            error = 1  # /((d.flatten())**2+0.01)

            result = model.fit(
                img.flatten(),
                x=X.flatten(),
                y=Y.flatten(),
                params=params,
                weights=1 / error,
            )

            lmfit.report_fit(result, min_correl=1)
            if args.vary_angle:
                axes[i].set_title(
                    f'{ex_wl}, ang:{np.rad2deg(result.params["rotation"].value):,.1f}',
                    fontsize=8,
                )
            else:
                axes[i].set_title(f"{ex_wl}")

            # axes[i][1].contourf(X[i],Y[i],result.best_fit.reshape(X[i].shape),50,vmin=d.min(),vmax=d.max())
            resid = abs(img - result.best_fit.reshape(X.shape))
            axes1[i].contourf(X, Y, resid, 50, vmin=img.min(), vmax=img.max())
            resid_factor = resid.sum() / np.multiply(*img.shape)
            axes1[i].set_title(f"{ex_wl}, erf={resid_factor:.1f}", fontsize=5)

            if "wy:     at boundary" in result.fit_report():
                continue
            if "wx:     at boundary" in result.fit_report():
                continue
            for k, v in result.params.items():
                tmp[k] = v.value
            res.append(tmp)

    for ax in axes:
        ax.set_aspect(1)
    for ax in axes1:
        ax.set_aspect(1)

    df = pd.DataFrame(res)

    fig2, axes2 = plt.subplots(2, 1, sharex=True)
    fig2.suptitle(f"{output_type}")
    axes20_ = axes2[0].twinx()
    xc_line = axes2[0].plot(df["LaserWavelength"], df["centerx"], ".-r", label="Xc")
    yc_line = axes20_.plot(df["LaserWavelength"], df["centery"], ".-b", label="Yc")
    axes20_.tick_params(axis="y", colors=yc_line[0].get_color())
    axes2[0].tick_params(axis="y", colors=xc_line[0].get_color())
    axes20_.set_ylabel("Center:Y, um", color=yc_line[0].get_color())
    axes2[0].set_ylabel("Center:X, um", color=xc_line[0].get_color())

    # axes2[0].legend()

    # for SHG:
    df["wx"] = df["wx"] * np.sqrt(2)
    df["wy"] = df["wy"] * np.sqrt(2)

    df["FWHMx"] = df["wx"] * np.sqrt(2 * np.log(2))
    df["FWHMy"] = df["wy"] * np.sqrt(2 * np.log(2))

    if args.final_size == "mean":
        df["FWHM"] = np.mean([df["FWHMx"], df["FWHMy"]], axis=0)
        df["w"] = np.mean([df["wx"], df["wy"]], axis=0)
    elif args.final_size == "min":
        df["FWHM"] = np.min([df["FWHMx"], df["FWHMy"]], axis=0)
        df["w"] = np.min([df["wx"], df["wy"]], axis=0)
    elif args.final_size == "max":
        df["FWHM"] = np.max([df["FWHMx"], df["FWHMy"]], axis=0)
        df["w"] = np.max([df["wx"], df["wy"]], axis=0)
    elif args.final_size == "X":
        df["FWHM"] = df["FWHMx"]
        df["w"] = df["wx"]
    elif args.final_size == "Y":
        df["FWHM"] = df["FWHMy"]
        df["w"] = df["wy"]
    df = df[df["w"] < 2]
    df = df.sort_values(by="LaserWavelength")
    weights = np.ones(len(df))
    weights[df.LaserWavelength < 900] = 0.8
    weights[df.LaserWavelength < 750] = 0.2
    weights[df.LaserWavelength > 1250] = 0.8

    axes2[0].set_title("Beam center")
    axes2[1].set_ylabel("Beam radius at 1/e$^2$, um")

    axes21_ = axes2[1].twinx()
    axes21_.set_ylabel("FWHM, um")

    axes2[1].plot(df["LaserWavelength"], df["wx"], ".-", label="x")
    axes2[1].plot(df["LaserWavelength"], df["wy"], ".-", label="y")
    axes2[1].plot(df["LaserWavelength"], df["w"], "--.", label=args.final_size)
    axes21_.set_ylim(
        axes2[1].get_ylim()[0] / np.sqrt(2 * np.log(2)),
        axes2[1].get_ylim()[1] / np.sqrt(2 * np.log(2)),
    )
    x = np.linspace(680, 1320, 1000)
    if not args.smooth is None:
        # eq = np.poly1d(np.polyfit(df['LaserWavelength'], df['sigma'], 3, w=weights))
        t, c, k = splrep(
            df["LaserWavelength"],
            df["w"],
            s=args.smooth,
            k=3,
        )  # w=weights)
        eq = BSpline(t, c, k, extrapolate=True)
        df["w"] = eq(df["LaserWavelength"])
        axes2[1].plot(x, eq(x), "--r", label="smooth")
    axes2[1].legend()
    axes2[1].set_xlabel("Ex. wavelength, nm")

    dtype = [("ex_wl", "<f4"), ("w0_in", "<f8"), ("w0", "<f8")]
    if not args.smooth is None:
        out_data = np.zeros(len(x), dtype=dtype)
        out_data["ex_wl"] = x
        out_data["w0"] = eq(x) * 1e-6  # m

    else:
        out_data = np.zeros(len(df)+1, dtype=dtype)
        out_data["ex_wl"][1:] = df["LaserWavelength"]
        out_data["w0"][1:] = df["w"] * 1e-6  # m
        out_data["ex_wl"][0] = 680
        out_data["w0"][0] = df["w"][df.LaserWavelength==df.LaserWavelength.min()].mean()* 1e-6  # m


    with exdir.File(args.output) as store_out:
        if output_type in store_out:
            del store_out[output_type]

        timestamp = time.strftime("%Y%m%d-%H%M%S")
        path = f"Microscope/Laser/BeamFocusing/{args.objective}/{output_type}"
        store_out.require_group(path)
        store_out[path].require_dataset(timestamp, data=out_data)

        store_out[path][timestamp].data[:] = out_data[:]
        path_objective = f"Microscope/Laser/BeamFocusing/{args.objective}"
        for key, val in objective_config.items():
            store_out[path_objective].attrs[key] = val
fig2.tight_layout()

plt.show()
