import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter
import time
import numpy_indexed as npi
import argparse
import pandas as pd

parser = argparse.ArgumentParser(
    description="Generate config from power calibration data.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "-f", dest="file", help="Exdir with calibration for TUN, FIX or both outputs."
)
parser.add_argument(
    "-l",
    dest="laser_output",
    help="""
					Calibration for TUN, FIX or both outputs.
					Options: [TUN, FIX, ALL].
					*For "ALL" LaserShutter should be in scanning axes.
					""",
    default="ALL",
)

parser.add_argument(
    "-t",
    dest="timestamps",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - process each timestamp in "scanND" folder.
					Integer index - process by index in sorted list of timestamps.
					Last measured - "-1" [by default].
					""",
    default=["-1"],
)
parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)

parser.add_argument(
    "--indexing",
    dest="indexing",
    help="Defalt indexing of laser outputs [for processing data older than ~Sep1012 should be \"0213\"]. 0 - both closed, 1 - FIX, 2 - TUN, 3 - both opened",
    default="0123",
)


args = parser.parse_args()
print(args)


store = exdir.File(args.file, "r")

dataType = "scanND"

timestamps = []
keys = list(store[dataType])
keys.sort()
if "ALL" in args.timestamps:
    timestamps = [k for k in store[dataType] if k[0] != "_"]
else:
    for ts in args.timestamps:
        try:
            timestamps.append(keys[int(ts)])
        except:
            timestamps.append(ts)

sizes = [len(store[dataType][k]["data"]) for k in timestamps]

dtype = store[dataType][keys[-1]]["data"].dtype

data = np.zeros(np.sum(sizes), dtype=dtype)
last_row = 0

for i, k in enumerate(timestamps):
    if store[dataType][k]["data"].dtype == dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        data[last_row : last_row + w.sum()] = d[w]
        last_row += w.sum()
        print(i, k)
data = data[data["time"] != 0]


out_dtype = [("Ex_wavelength", "u2"), ("HWP_angle", "f8"), ("power", "f8")]

if args.laser_output == "ALL":
    groups = npi.group_by(data["position"]["LaserShutter"]).split(data)
else:
    groups = [data]

fig0, axes0 = plt.subplots(1, 1)

LaserShutterIndexes = {key:int(index) for key,index in zip(["CLOSED","FIX", "TUN", "OPENED"], args.indexing)}


for group in groups:
    out_data = np.zeros(0, dtype=out_dtype)

    if args.laser_output == "ALL":
        if np.all(group["position"]["LaserShutter"] == LaserShutterIndexes["TUN"]):
            output_type = "TUN"
        elif np.all(group["position"]["LaserShutter"] == LaserShutterIndexes["FIX"]):
            output_type = "FIX"
        else:
            raise Exception("UNKNOWN LASER INDEXING")
    else:
        output_type = args.laser_output

    groups1 = npi.group_by(group["position"]["LaserWavelength"]).split(group)

    if len(groups1) > 10:
        fig, axes = plt.subplots(
            int(np.ceil(len(groups1) / 4)),
            4,
            sharex=True,
            sharey=True,
        )
        axes = axes.T.flatten()
    else:
        fig, axes = plt.subplots(
            len(groups1),
            1,
            sharex=True,
            sharey=True,
        )

    fig.suptitle(output_type)

    if len(groups1) == 1:
        axes = [axes]
    ang_limits = []

    grid_N = 500
    for i, d in enumerate(groups1):
        ex_wl = d["position"]["LaserWavelength"][0]
        print(f"{i}:\t{ex_wl}")

        out_data_tmp = np.zeros(grid_N, dtype=out_dtype)
        x = d["position"][f"HWP_{output_type}_angle"].astype(float)
        y = d["data"]["Powermeter"]["data"]["power"].astype(float)
        w = abs(x - 36) > 1
        x = x[w]
        y = y[w]

        k = np.unique(x, return_index=True)[1]
        x = x[k][1:-1]
        y = y[k][1:-1]

        # y = y[x.argsort()]
        # x = x[x.argsort()]
        try:
            b, a = butter(2, 0.325)

            x_filtered = filtfilt(b, a, x)
            y_filtered = filtfilt(b, a, y)
            out_data_tmp["Ex_wavelength"] = ex_wl

            interp = interp1d(
                x_filtered,
                y_filtered,
                kind="cubic",
                bounds_error=False,
                fill_value="extrapolate",
            )
            xi = np.linspace(x.min(), x.max(), grid_N)
            y_new = interp(xi)

            out_data_tmp["HWP_angle"] = xi
            out_data_tmp["power"] = y_new
            ang_limits.append(
                [xi[y_new == y_new.min()][0], xi[y_new == y_new.max()][0]]
            )
            axes[i].plot(x, y, ".", label=f"_{ex_wl}")
            axes[i].plot(xi, y_new, "-", label=f"{ex_wl}")
            axes[i].grid()
            axes[i].legend()
            out_data = np.hstack((out_data, out_data_tmp))
        except:
            traceback.print_exc()
            break

    ang_limits = np.array(ang_limits).mean(axis=0)
    out_data = out_data[out_data["Ex_wavelength"] != 0]
    out_data = out_data[~np.isnan(out_data["HWP_angle"])]
    out_data.sort(order=["Ex_wavelength", "HWP_angle"])

    df = pd.DataFrame(out_data)
    df1 = df.groupby("Ex_wavelength", as_index=False).max()
    print(df1)
    if len(df1) > 1:
        axes0.plot(df1.Ex_wavelength, df1.power, label=output_type)
    else:
        axes0.plot(df1.Ex_wavelength, df1.power, "o", label=output_type)
    # for i,ex_wl in enumerate(ex_wls):
    # 	print(f'{i}:\t{ex_wl}')
    # 	mask = out_data['Ex_wavelength']==ex_wl
    # 	x = out_data['HWP_angle'][mask]
    # 	y = out_data['power'][mask]

    # 	axes[i].plot(x,y,'--',label=f'_{ex_wl}')

    with exdir.File(args.output) as store_out:
        if "HWP_FIX" in store_out and output_type == "FIX":
            del store_out["HWP_FIX"]

        if "HWP_TUN" in store_out and output_type == "TUN":
            del store_out["HWP_TUN"]
        timestamp = time.strftime("%Y%m%d-%H%M%S")
        store_out.require_group(f"HWP_{output_type}")
        store_out[f"HWP_{output_type}"].require_dataset(timestamp, data=out_data)

        store_out[f"HWP_{output_type}"][timestamp].attrs[
            "power2angle_interpLimits"
        ] = ang_limits.tolist()
        store_out[f"HWP_{output_type}"][timestamp].data[:] = out_data[:]

axes0.legend()
axes0.set_ylabel("Max Power, W")
axes0.set_xlabel("Ex. wavelength, nm")
axes0.grid(1)


plt.show()
