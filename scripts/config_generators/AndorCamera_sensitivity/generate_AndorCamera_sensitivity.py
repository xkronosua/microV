import sys
import time
import exdir
import numpy as np
from matplotlib import pyplot as plt
import argparse
from scipy.interpolate import interp1d, LinearNDInterpolator
from scipy.signal import medfilt
import pandas as pd

parser = argparse.ArgumentParser(
    description="Process AndorCamera sensitivity calibration curves."
)
parser.add_argument(
    "-s", dest="setup", help="Setup: HRS or Microscope", default="Microscope"
)
parser.add_argument(
    "-c",
    dest="configuration",
    help="Configuration of measurements [forward, backward, 90deg, ...]",
    default="forward",
)
parser.add_argument("-d", dest="data", help="Exdir folder with data")
parser.add_argument("-b", dest="background", help="Exdir folder with background")
parser.add_argument(
    "-g",
    dest="grating",
    type=int,
    help="Process data for specific grating [0,1]",
    default=1,
)
parser.add_argument(
    "-F",
    dest="filters",
    nargs="+",
    type=str,
    help="""
					Filter datasheets.
					Two columns separated by tabulation:
						wavelength	transmittance
					Note:
						wavelength - nm
						transmittance - norm from 0 to 1
					""",
    default=["FGB37x2.csv", "FESH0700.csv", "FESH0850.csv", "FESH1000.csv"],
)
parser.add_argument(
    "-L", dest="lamp", help="Lamp datasheet", default="USHIO_BRL_12V50W_1.csv"
)
parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)


args = parser.parse_args()
print(args)


lamp_data = np.loadtxt(args.lamp, delimiter=",")
lamp = interp1d(
    lamp_data[:, 0], lamp_data[:, 1], bounds_error=False, fill_value="extrapolate"
)


store = exdir.File(args.data, "r")
store_bg = exdir.File(args.background, "r")

data = store["scanND"][max(store["scanND"])]["data"]
data_bg = store_bg["scanND"][max(store_bg["scanND"])]["data"]

if "shamrockGrating" in data["position"].dtype.names:
    mask = (data["position"]["shamrockGrating"] == args.grating) & (data["time"] != 0)
else:
    mask = data["time"] != 0

if "filtersPiezoStage" in data["position"].dtype.names:
    mask = mask & (data["position"]["filtersPiezoStage"] == 0)

mask = mask & (data["position"]["shamrockWavelength"] > 450)


filters = data.attrs["filters"].to_dict()

crop = 200
interps = []
lin_interps = []


wl = data["data"]["AndorCamera"]["raw"]["wavelength"][mask, crop:-crop].flatten()

intens = data["data"]["AndorCamera"]["raw"]["intensity"][mask, crop:-crop]
intens_bg = data_bg["data"]["AndorCamera"]["raw"]["intensity"][mask, crop:-crop]

exposure = data["data"]["AndorCamera"]["raw"]["exposure"][mask]
# exposure_bg = data_bg['data']['AndorCamera']['raw']['exposure'][mask]

intens_corrected = (((intens - intens_bg).T / exposure).T).flatten() / lamp(wl)
mask = (intens_corrected > 0) & (intens_corrected < 1e6) & (wl > 300)
wl = wl[mask]
intens_corrected = intens_corrected[mask]

plt.plot(wl, intens_corrected, "b.", label="raw")


intens_corrected = intens_corrected[wl.argsort()]
wl = wl[wl.argsort()]

wl = medfilt(wl, 151)
intens_corrected = medfilt(intens_corrected, 151)
plt.plot(wl, intens_corrected, "g.", label="filtered")
plt.legend()

plt.figure()
df = pd.DataFrame({"wavelength": wl, "None": intens_corrected})
plt.plot(df.wavelength, df["None"], label="None", lw=3)

# fgb = np.loadtxt('FGB37.csv', skiprows=1, delimiter='\t')
# eo = np.loadtxt('EO84-712.csv', skiprows=1, delimiter='\t')


def totalT(t):
    T = 1 / (np.sum(np.vstack([1 / tt for tt in t]), axis=0) - 1)
    return T


# iu = interp1d(eo[:,0], eo[:,1], bounds_error=False, fill_value=np.nan)
# T = totalT([fgb[:,1]]*2+[iu(fgb[:,0])])
# T = totalT([fgb[:,1]]*2)#*iu(fgb[:,0])
# #T = fgb[:,1]**2
# #T = fgb[:,1]**2*iu(fgb[:,0])
#
# eo_fgb = interp1d(fgb[:,0], T, bounds_error=False, fill_value=np.nan)

datasheets = {}
with exdir.File(args.output) as store_out:

    timestamp = time.strftime("%Y%m%d-%H%M%S")
    path_andor = f"{args.setup}/AndorCamera/Sensitivity/{args.configuration}/grating{args.grating}"
    path_filters = f"{args.setup}/filtersPiezoStage/Filters"
    store_out.require_group(path_andor)
    store_out.require_group(path_filters)
    df1 = df.rename(columns={"None": "Sensitivity"})

    store_out[path_andor].require_dataset(timestamp, data=df1.to_records(index=False))

    for name in args.filters:
        filter_path_ = name.replace(",", "")
        T_tmp = []
        res_name = []
        for filter_path in filter_path_.split("*"):
            if not ".csv" in filter_path:
                filter_path = filter_path + ".csv"
            else:
                name = name.split(".csv")[0]
            try:
                datasheet_ = np.loadtxt(filter_path, skiprows=1, delimiter=",")
            except:
                datasheet_ = np.loadtxt(filter_path, skiprows=1, delimiter="\t")
            datasheet_[:, 1][datasheet_[:, 1] <= 0] = np.nan
            interp = interp1d(
                datasheet_[:, 0],
                datasheet_[:, 1],
                bounds_error=False,
                fill_value=np.nan,
            )
            #plt.plot(datasheet_[:, 0],datasheet_[:, 1],'-')
            print(filter_path)
            if "EO84-712" in filter_path:
                def interp(x):
                    # extrapolation is not working properly so we can do it manually
                    y = np.zeros(len(x))
                    ip = interp1d(
                        datasheet_[:, 0],
                        datasheet_[:, 1],
                        bounds_error=False,
                        fill_value=np.nan#"extrapolate",assume_sorted=True
                    )
                    y = ip(x)
                    w = (x <= datasheet_[:, 0].min()) & (x > 349)
                    #w1 = (datasheet_[:, 0] < datasheet_[:, 0].min()+0.5)
                    #ip = np.poly1d(np.polyfit(
                    #    datasheet_[w1, 0],
                    #    datasheet_[w1, 1],2
                    #))
                    y[w] = datasheet_[datasheet_[:, 0]==datasheet_[:, 0].min(), 1]#ip(x[w])

                    return y
            datasheets[name] = interp
            T_tmp.append(
                interp(df.wavelength)
            )
            res_name.append(filter_path.split(".csv")[0])
        name = "_".join(res_name)
        data_tmp = np.zeros(
            len(df), dtype=[("wavelength", "f4"), ("transmittance", "f8")]
        )
        data_tmp["wavelength"] = df.wavelength
        if len(T_tmp) == 1:
            data_tmp["transmittance"] = T_tmp[0]
        else:
            data_tmp["transmittance"] = totalT(T_tmp)
        data_tmp["transmittance"][data_tmp["transmittance"] < 0] = 0
        store_out[path_filters].require_group(name)
        store_out[path_filters][name].require_dataset(timestamp, data=data_tmp)

        df[name] = df["None"] * data_tmp["transmittance"]
        plt.plot(df.wavelength, df[name], label=name)

# df['EO84-712_FGB37x2'] = df['None']*datasheet(df.wavelength)
# plt.plot(df.wavelength, df['None']*eo_fgb(df.wavelength),'--', label='eo_fgb')

plt.legend()

output_name = f"AndorCameraCalibr_{args.setup}_{args.configuration}"
output_name += f"_grating{args.grating}"
output_name += ".csv"

df.to_csv(output_name)


plt.show()
