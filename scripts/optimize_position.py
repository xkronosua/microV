import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir
from pathlib import Path
import time
import traceback
import threading
from scipy.interpolate import interp1d
from scipy.optimize import minimize_scalar

# from noisyopt import minimizeCompass, minimizeSPSA
from skopt import gp_minimize, callbacks


import numpy.lib.recfunctions as rfn
from utils.units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES

from .scriptUtils import isRunning, process_progress_info


def time_sleep(secs):
    time.sleep(secs)
    #QtCore.QThread.msleep(int(secs*1000))

# def generate_random_sphere(r, N):
#     phi = np.random.uniform(0, np.pi * 2, N)
#     theta = np.random.uniform(0, np.pi, N)
#     r = np.random.uniform(0, r, N)
#     x = r * np.sin(theta) * np.cos(phi)
#     y = r * np.sin(theta) * np.sin(phi)
#     z = r * np.cos(theta)
#     return x, y, z


def func_minimize(
    x,
    moveTo,
    readData,
    readPosition,
    axes,
    source,
    chan,
    averageN,
    dataReadySignal,
    maximize,
    log=[],
    _isRunning=lambda: True,
    raw_dset=None,
    _write_to_row=None,
    wait=0,
    thread=None
):
    if not _isRunning():
        raise Exception("Optimization interrupted")
    # if len(axes)==1:
    # 	moveTo(target=x, axis=axes[0], wait=True)
    # else:
    for i, ax in enumerate(axes):
        moveTo(target=x[i], axis=ax, wait=True)
    if thread is None:
        time_sleep(wait)
    else:
        thread.time_sleep(wait)

    pos = np.array([readPosition(ax) for ax in axes])
    sig = 0
    for i in range(averageN):
        lines = readData(source=source)
        sig += lines["data"][source]["data"][chan]

    sig /= i + 1
    # if len(axes)==1:
    # 	log.append([pos[0], sig[0]])

    if not raw_dset is None:
        raw_dset[_write_to_row[0]]["data"] = lines
        raw_dset[_write_to_row[0]]["data"][source][data][chan] = sig
        raw_dset[_write_to_row[0]]["position"] = pos
        _write_to_row[0] += 1

    items = []
    if not dataReadySignal is None:
        if "raw" in lines["data"][source].dtype.names:
            raw_x_name = lines["data"][source]["raw"].dtype.names[0]
            for name in lines["data"][source]["raw"].dtype.names[1:-1]:
                if name in ["type", "VRange", "time"]: continue
                items.append(
                    {
                        "type": "raw",
                        "name": f"{source}_{name}*",
                        "info": f"",
                        #'index': i,
                        "time": time.time(),
                        "data": [
                            lines["data"][source]["raw"][raw_x_name][-1],
                            lines["data"][source]["raw"][name][-1],
                        ],
                    }
                )
        dataReadySignal.emit(items)

    if maximize:
        return -sig[0]
    else:
        return sig[0]


def optimize_position(
    axes=None,
    x0=None,
    bounds=None,
    source=None,
    chan=None,
    maxiter=None,
    averageN=1,
    maximize=True,
    wait=0,
    moveTo=None,
    readData=None,
    readPosition=None,
    dataReadySignal=None,
    xatol=0.01,
    _isRunning=lambda: True,
    thread=None
):

    lines = readData(source=source)
    _log = []
    pos_init = np.array([readPosition(ax) for ax in axes])
    kwargs = {
        "moveTo": moveTo,
        "readData": readData,
        "readPosition": readPosition,
        "axes": axes,
        "source": source,
        "chan": chan,
        "averageN": averageN,
        "dataReadySignal": dataReadySignal,
        "maximize": maximize,
        "log": _log,
        "_isRunning": _isRunning,
        "wait": wait,
        "thread": thread,
    }

    if type(x0) == np.ndarray:
        x0 = x0.tolist()

    res = None
    try:
        # if len(axes)!=1:
        print(x0, type(x0), bounds)
        _func = lambda x: func_minimize(x, **kwargs)
        res = gp_minimize(
            _func,  # the function to minimize
            dimensions=bounds,  # the bounds on each dimension of x
            x0=x0,  # the starting point
            y0=None,
            acq_func="LCB",  # the acquisition function (optional)
            n_calls=maxiter,  # number of evaluations of f including at x0
            n_random_starts=maxiter // 3,  # the number of random initial points
            # callback=[checkpoint_saver],
            # a list of callbacks including the checkpoint saver
            verbose=True,
            initial_point_generator="sobol",
            # random_state=777,
            noise="gaussian",
        )
        # else:
        # 	res = minimize_scalar(func_minimize, args=args,
        # 				bounds=bounds[0],
        # 				method='bounded',
        # 				options={'xatol':xatol, 'maxiter':maxiter})
        print(res)
    except Exception as ex:
        traceback.print_exc()
        logging.error("optimize", exc_info=ex)

    log = {}
    if res is None:
        pos = pos_init
        log = None
    else:
        pos = res.x
        # if len(axes)==1:
        # 	pos = [res.x]
        # 	print(_log)
        # 	_log = np.array(_log).T
        # 	log = {'position': {axes[0]:_log[0]}, 'data':_log[1]}
        # else:
        _log = np.array(res.x_iters).T
        log["position"] = {}
        for i, ax in enumerate(axes):
            log["position"][ax] = _log[i]
        log["data"] = res.func_vals

    for i, ax in enumerate(axes):
        moveTo(target=pos[i], axis=ax, wait=True)

    lines = readData(source=source)
    sign = -1 if maximize else 1
    if not dataReadySignal is None and not res is None:
        items = []
        name = chan
        for data_x_name in log["position"].keys():
            items.append(
                {
                    "type": "Scan",
                    "name": f"{source}_{name}*",
                    "info": f"{data_x_name}",
                    #'index': i,
                    "time": time.time(),
                    "data": [log["position"][data_x_name], sign * log["data"]],
                }
            )
        if "raw" in lines["data"][source].dtype.names:
            raw_x_name = lines["data"][source]["raw"].dtype.names[0]
            for name in lines["data"][source]["raw"].dtype.names[1:-1]:
                if name in ["type", "VRange", "time"]: continue
                items.append(
                    {
                        "type": "raw",
                        "name": f"{source}_{name}*",
                        "info": f"{[round(p, 4) for p in pos]}",
                        #'index': i,
                        "time": time.time(),
                        "data": [
                            lines["data"][source]["raw"][raw_x_name][-1],
                            lines["data"][source]["raw"][name][-1],
                        ],
                    }
                )
        dataReadySignal.emit(items)

    return pos, log


class optimize_positionThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    running = False

    def __init__(self, parent=None):
        super(optimize_positionThread, self).__init__(parent)

    def stop(self):
        self.running = False

    def time_sleep(self, secs):
        time.sleep(secs)
        #self.msleep(abs(int(secs*1000)))

    @isRunning
    def run(self):
        axes = []
        bounds = []
        x0 = []
        scaling = []
        source, _, chan = self.parent().ui.optimize_chan.currentText().split("/")
        relative = self.parent().ui.optimize_relative.isChecked()
        # xtol = self.parent().ui.optimize_tolerance.value()
        maxiter = self.parent().ui.optimize_maxIterations.value()
        averageN = self.parent().ui.optimize_averageN.value()
        maximize = self.parent().ui.optimize_maximize.isChecked()
        wait = self.parent().ui.optimize_wait.value()

        for i in range(self.parent().ui.optimize_config.rowCount()):
            ax = self.parent().ui.optimize_config.cellWidget(i, 0).currentText()
            if ax == "-":
                continue
            axes.append(ax)
            p = float(self.parent().ui.optimize_config.item(i, 1).text())
            x0.append(p)
            b = [
                float(self.parent().ui.optimize_config.item(i, 2).text()),
                float(self.parent().ui.optimize_config.item(i, 3).text()),
            ]
            # scale = float(self.parent().ui.optimize_config.item(i,4).text())
            # scaling.append(scale)
            if relative:
                b = [p + b[0], p + b[1]]
            bounds.append(b)

        def _isRunning():
            return self.running

        pos, log = optimize_position(
            axes=axes,
            x0=x0,
            bounds=bounds,
            source=source,
            chan=chan,
            maxiter=maxiter,
            averageN=averageN,
            maximize=maximize,
            moveTo=self.parent().abstractMoveTo,
            readData=self.parent().getUnifiedData,
            readPosition=self.parent().abstractGetPosition,
            dataReadySignal=self.dataReadySignal,
            xatol=0.01,
            _isRunning=_isRunning,
            wait=wait,
            thread=self
        )
        print(pos, log)
        self.running = False
