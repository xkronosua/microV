import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir
from pathlib import Path
import time
import traceback
import threading
from scipy.interpolate import interp1d
import numpy.lib.recfunctions as rfn
import pint
from collections import deque
import datetime

sys.path.append(str(Path("../utils")))
from .optimize_position import optimize_position
from .scriptUtils import scan_move_recursive, isRunning, process_progress_info

import importlib

import beepy as beep

from utils.units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES

ureg = pint.UnitRegistry()
Q_ = ureg.Quantity


# def time_sleep(secs):
#     QtCore.QThread.msleep(abs(int(secs*1000)))


class MultiScanThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    moveToSignal = QtCore.Signal(float, str, float)

    running = False
    store = None
    fname = "tmp"
    header = ["index", "axis", "A", "B", "C"]
    skip_counter_general = 0
    skip_counter_shift_delay = 0

    def time_sleep(self, secs):
        #with self.parent().sleep_lock:
        time.sleep(secs)
        #self.msleep(abs(int(secs*1000)))

    def abstractMoveTo(self, target, axis="None", wait=True):
        self.moveToSignal.emit(target, axis, wait)

    def __init__(self, parent=None):
        super(MultiScanThread, self).__init__(parent)

        self.fname = Path(self.parent().expData_filePath.text())
        if ".exdir" in str(self.fname):
            pass
        else:
            self.fname = str(self.fname).split(".exdir")[0] + ".exdir"

        self.start_time = datetime.datetime.now()
        self.cycle_time = deque([time.time()], maxlen=1000)

    def stop(self):
        if self.parent().ui.finalization_protocol_beep.isChecked():
            beep.beep(1)
        if self.parent().ui.finalization_protocol_closeShutters.isChecked():
            self.parent().laserSetShutter(False, wait=True)
            self.parent().laserSetIRShutter(False, wait=True)

        if self.parent().ui.finalization_protocol_laserOff.isChecked():
            self.parent().laserOnOff(False)

        if self.parent().ui.finalization_protocol_fullShutdown.isChecked():
            self.parent().closeEvent()

        self.running = False

    def save_img(self):
        img_item = self.parent().IMG_VIEW["cam"].getImageItem()
        scale = np.array([self.parent().ui.ThorCamera_scale.value()] * 2)
        roi = self.parent().rectROI["cam"]
        img_raw = img_item.image
        img_roi = roi.getArrayRegion(img_raw, img_item)

        info, currentRow = self.parent().MultiScan_getInfo()

        dtype = [
            ("image", np.int16, img_raw.shape),
            ("image_roi", np.int16, img_roi.shape),
            ("roi_pos", "f4", (2,)),
            ("shift", "f8", (2,)),
            ("XYZ", "f8", (3,)),
            ("scale", "f4", (2,)),
            ("exposure", "f4"),
            ("currentRow", "u4"),
            ("info", info.dtype, (len(info),)),
            ]

        dset = self.storeActiveGroup.create_dataset(
            "img", shape=(1,), dtype=dtype
        )
        print(dset.dtype)
        dset[0]['image'] = img_raw
        dset[0]['image_roi'] = img_roi
        dset[0]["roi_pos"] = np.array(roi.pos())
        dset[0]["scale"] = scale
        dset[0]["exposure"] = self.parent().ui.ThorCamera_exposure.value()
        dset[0]["shift"] = img_item.pos()
        dset[0]["XYZ"] = self.parent().piStage.getPosition()
        dset[0]["currentRow"] = currentRow
        dset[0]["info"] = info
        del dset

    def _optimize_delay_overlap(self, info, current):
        bounds = []
        pos_init = []
        axes = []
        protocol = self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Protocol").value()
        try:
            source = self.params.child(
                "Config", "Optimize on anchor", "BeamShift+Delay", "By"
            ).value()
            source, _, chan = source.split("/")
        except:
            source = "AndorCamera"
            chan = "w1+w2"
        exposure = self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Exposure"
        ).value()
        init_exposure = -1
        if source == "AndorCamera" and exposure > 0:
            default_exposure_config = self.params.child(
                "Config", "Default exposure"
            ).value()
            if default_exposure_config > 0:
                init_exposure = default_exposure_config
            else:
                init_exposure = self.parent().andorCameraGetExposure()
            self.parent().andorCameraSetExposure(exposure)

        bounds_ = [
            [float(bb) for bb in b.split(",")]
            for b in self.params.child(
                "Config",
                "Optimize on anchor",
                "BeamShift+Delay",
                "Bounds[shiftV,shiftH,delay]",
            )
            .value()
            .split(";")
        ]
        bounds_ = {
            "shiftV": bounds_[0],
            "shiftH": bounds_[1],
            "delay": bounds_[2],
        }
        if self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Optimize shiftV"
        ).value():
            axes.append("laserBeamShift_V")
            pos = self.parent().abstractGetPosition(axes[-1])
            pos_init.append(pos)
            bounds.append((pos + bounds_["shiftV"][0], pos + bounds_["shiftV"][1]))

        if self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Optimize shiftH"
        ).value():
            axes.append("laserBeamShift_H")

            pos = self.parent().abstractGetPosition(axes[-1])
            pos_init.append(pos)
            bounds.append((pos + bounds_["shiftH"][0], pos + bounds_["shiftH"][1]))

        if self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Optimize delay"
        ).value():
            axes.append("Delay_line_position_zero_relative")
            pos_init.append(0)
            bounds.append((bounds_["delay"][0], bounds_["delay"][1]))

        default_shutter_config = self.params.child("Config", "Default shutters").value()
        if default_shutter_config == "None":
            shutter_init = (
                self.parent().ui.actionShutter.isChecked(),
                self.parent().ui.actionIRShutter.isChecked(),
            )
        elif default_shutter_config == "FIX":
            shutter_init = (False, True)
        elif default_shutter_config == "TUN":
            shutter_init = (True, False)
        elif default_shutter_config == "OPENED":
            shutter_init = (True, True)
        else:
            shutter_init = (False, False)



        maxiter = self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Maxiter"
        ).value()
        # tol = self.params.child('Config','Optimize on anchor','Tolerance').value()
        averageN = self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Average"
        ).value()
        maximize = self.parent().ui.optimize_maximize.isChecked()
        wait = self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Wait"
        ).value()
        optim_save_raw = (
            False  # self.params.child('Config','Optimize on anchor','Save raw').value()
        )

        def _isRunning():
            return self.running

        print(pos_init, bounds)
        if protocol ==  "BeamShift and delay (w1+w2)":
            self.parent().laserSetShutter(True, wait=True)
            self.parent().laserSetIRShutter(True, wait=True)
            pos, log = optimize_position(
                axes=axes,
                x0=pos_init,
                bounds=bounds,
                source=source,
                chan=chan,
                maxiter=maxiter,
                averageN=averageN,
                maximize=maximize,
                moveTo=self.parent().abstractMoveTo,
                readData=self.parent().getUnifiedData,
                readPosition=self.parent().abstractGetPosition,
                dataReadySignal=self.dataReadySignal,
                # xatol=tol,
                _isRunning=_isRunning,
                wait=wait,
                #thread=self,
            )
        elif protocol == "BeamShift(2w2), delay(w1+w2)":
            ex_wl = self.parent().abstractGetPosition(axis="LaserWavelength")
            if source == "AndorCamera":
                chan0 = "2w2"
            else:
                # write for your source if you need it
                self.stop()
                return

            self.parent().laserSetShutter(True, wait=True)
            self.parent().laserSetIRShutter(False, wait=True)
            if "laserBeamShift_H" in axes or "laserBeamShift_V" in axes:
                pos, log = optimize_position(
                    axes=axes[:-1],
                    x0=pos_init[:-1],
                    bounds=bounds[:-1],
                    source=source,
                    chan=chan0,
                    maxiter=maxiter,
                    averageN=averageN,
                    maximize=maximize,
                    moveTo=self.parent().abstractMoveTo,
                    readData=self.parent().getUnifiedData,
                    readPosition=self.parent().abstractGetPosition,
                    dataReadySignal=self.dataReadySignal,
                    # xatol=tol,
                    _isRunning=_isRunning,
                    wait=wait,
                    #thread=self,
                )
                if self.params.child(
                    "Config", "Optimize on anchor", "BeamShift+Delay", "Save optimized"
                ).value():
                    self.parent().laserBeamShift_add_to_calibr(yes=True)
                    self.time_sleep(0.5)

            tmp_line = self.parent().getUnifiedData()
            ref = 0
            if source == "AndorCamera":
                ref = tmp_line["data"][source]['data']['2w2']
                self.parent().laserSetShutter(False, wait=True)
                self.parent().laserSetIRShutter(True, wait=True)
                self.time_sleep(0.5)
                tmp_line = self.parent().getUnifiedData()
                if ex_wl == 700:
                    ref = min(ref, tmp_line["data"][source]['data']['2w1'])
                elif abs(ex_wl-1045)<15:
                    ref += tmp_line["data"][source]['data']['2w1']
                    ref*=0.7
                else:
                    ref = tmp_line["data"][source]['data']['2w1']

            self.parent().laserSetShutter(True, wait=True)
            self.parent().laserSetIRShutter(True, wait=True)

            self.parent().abstractMoveTo(axis='Delay_line_position_zero_relative', target=0)
            delay_init = self.parent().abstractGetPosition(axes[-1])
            bounds_tmp = np.array([delay_init + bounds_["delay"][0], delay_init + bounds_["delay"][1]])

            HDR = self.parent().ui.andorCamera_HDR.isChecked()
            if HDR:
                self.parent().ui.andorCamera_HDR.setChecked(False)
            for it in range(20):
                bounds_tmp += (bounds_tmp[1]-bounds_tmp[0])*it*(1 if it%2==0 else -1)
                #if np.any(bounds_tmp>=100) or np.any(bounds_tmp<=5): break


                pos, log = optimize_position(
                    axes=['Delay_line_position_zero_relative'],
                    x0=[bounds_tmp.mean()],
                    bounds=[bounds_tmp],
                    source=source,
                    chan=chan,
                    maxiter=maxiter,
                    averageN=averageN,
                    maximize=maximize,
                    moveTo=self.parent().abstractMoveTo,
                    readData=self.parent().getUnifiedData,
                    readPosition=self.parent().abstractGetPosition,
                    dataReadySignal=self.dataReadySignal,
                    # xatol=tol,
                    _isRunning=_isRunning,
                    wait=0,
                    #thread=self,
                )
                tmp_line = self.parent().getUnifiedData()

                if source == "AndorCamera":
                    if tmp_line["data"][source]['data'][chan] > ref:
                        break

                else:
                    break
            self.parent().ui.andorCamera_HDR.setChecked(HDR)

        if source == "AndorCamera" and init_exposure > 0:
            self.parent().andorCameraSetExposure(init_exposure)

        self.parent().laserSetShutter(shutter_init[0], wait=True)
        self.parent().laserSetIRShutter(shutter_init[1], wait=True)

        error = self.parent().abstractGetPosition("Delay_line_position_zero_relative")
        if self.parent().ui.MultiScan_delay_error_max.isChecked():
            if abs(error) > self.parent().ui.MultiScan_delay_error_maxValue.value():
                return False
        else:
            self.parent().ui.MultiScan_delay_error.setValue(error)

        if self.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "Save optimized"
        ).value():
            #self.parent().laserBeamShift_add_to_calibr(yes=True)
            #self.time_sleep(0.5)
            self.parent().mocoZeroDelay_add_to_calibr(yes=True)
        return True

    def _optimize_position(self, info, current, pos):
        pos_init = pos
        self.delta = np.array(
            [float(i) for i in self.parent().ui.MultiScan_delta.text().split(",")]
        )

        axes = self.params.child(
            "Config", "Optimize on anchor", "General optimization", "Axes"
        ).value()

        if axes == "None":
            return True
        elif axes == "Custom axes":
            axes = self.params.child(
                "Config", "Optimize on anchor", "General optimization", "Custom axes"
            ).value()
        axes = axes.split(",")
        print("AXES:", axes)
        bounds = []
        source = self.params.child(
            "Config", "Optimize on anchor", "General optimization", "By"
        ).value()
        source, _, chan = source.split("/")

        exposure = self.params.child(
            "Config", "Optimize on anchor", "General optimization", "Exposure"
        ).value()
        init_exposure = -1
        if source == "AndorCamera" and exposure > 0:
            default_exposure_config = self.params.child(
                "Config", "Default exposure"
            ).value()
            if default_exposure_config > 0:
                init_exposure = default_exposure_config
            else:
                init_exposure = self.parent().andorCameraGetExposure()
            self.parent().andorCameraSetExposure(exposure)

        _shutter = self.params.child(
            "Config", "Optimize on anchor", "General optimization", "Shutter"
        ).value()
        shutter = (None, None)
        if _shutter == "None":
            shutter = (None, None)
        elif _shutter == "FIX":
            shutter = (False, True)
        elif _shutter == "TUN":
            shutter = (True, False)
        elif _shutter == "CLOSED":
            shutter = (False, False)
        elif _shutter == "OPENED":
            shutter = (True, True)

        default_shutter_config = self.params.child("Config", "Default shutters").value()
        if default_shutter_config == "None":
            shutter_init = (
                self.parent().ui.actionShutter.isChecked(),
                self.parent().ui.actionIRShutter.isChecked(),
            )
        elif default_shutter_config == "FIX":
            shutter_init = (False, True)
        elif default_shutter_config == "TUN":
            shutter_init = (True, False)
        elif default_shutter_config == "OPENED":
            shutter_init = (True, True)
        else:
            shutter_init = (False, False)

        if not shutter[0] is None and not shutter[1] is None:
            self.parent().laserSetShutter(shutter[0], wait=True)
            self.parent().laserSetIRShutter(shutter[1], wait=True)

        relative_bounds = self.params.child(
            "Config", "Optimize on anchor", "General optimization", "Relative"
        ).value()
        maxiter = self.params.child(
            "Config", "Optimize on anchor", "General optimization", "Maxiter"
        ).value()
        # tol = self.params.child('Config','Optimize on anchor','Tolerance').value()
        averageN = self.params.child(
            "Config", "Optimize on anchor", "General optimization", "Average"
        ).value()
        maximize = self.parent().ui.optimize_maximize.isChecked()
        optim_save_raw = (
            False  # self.params.child('Config','Optimize on anchor','Save raw').value()
        )

        bounds_ = [
            [float(bb) for bb in b.split(",")]
            for b in self.params.child(
                "Config", "Optimize on anchor", "General optimization", "Bounds"
            )
            .value()
            .split(";")
        ]
        bounds = []
        pos_init = []

        for i, ax in enumerate(axes):
            p = self.parent().abstractGetPosition(ax)
            # if ax in 'XYZ':
            # 	p += delta[ax]
            pos_init.append(p)

            if relative_bounds:
                bounds.append((p + bounds_[i][0], p + bounds_[i][1]))
            else:
                bounds = bounds_
        # for i,bounds_ in enumerate(self.params.child('Config','Optimize on anchor','Bounds').value().split(';')):
        # 	b = [float(bb) for bb in bounds_.split(',')]
        # 	if relative_bounds:
        # 		b = [pos[i]+b[0],pos[i]+b[1]]
        # 	bounds.append(b)

        def _isRunning():
            return self.running

        pos, log = optimize_position(
            axes=axes,
            x0=pos_init,
            bounds=bounds,
            source=source,
            chan=chan,
            maxiter=maxiter,
            averageN=averageN,
            maximize=maximize,
            moveTo=self.parent().abstractMoveTo,
            readData=self.parent().getUnifiedData,
            readPosition=self.parent().abstractGetPosition,
            dataReadySignal=self.dataReadySignal,
            # xatol=tol,
            _isRunning=_isRunning,
            #thread=self,
        )

        if not shutter[0] is None and not shutter[1] is None:
            self.parent().laserSetShutter(shutter_init[0], wait=True)
            self.parent().laserSetIRShutter(shutter_init[1], wait=True)
            self.time_sleep(0.5)

        if source == "AndorCamera" and init_exposure > 0:
            self.parent().andorCameraSetExposure(init_exposure)



        pos = self.parent().abstractGetPosition("XYZ")
        polarization = self.parent().abstractGetPosition("HWP_Polarization")

        print(pos, self.delta)
        if self.params.child(
            "Config", "Optimize on anchor", "General optimization", "Rewrite optimized"
        ).value():

            if "Laser_precompensation" in axes:
                self.parent().laserSavePrecompensation(yes=True)

            for i,ax in enumerate(["X","Y","Z","HWP_Polarization"]):
                if ax in axes:
                    p = self.parent().abstractGetPosition(ax)
                    self.parent().ui.MultiScan_probe.item(current, i).setText(
                            f"{p:.4f}"
                        )

            self.delta = np.array([0, 0, 0])
            self.parent().ui.MultiScan_delta.setText(
                    ",".join([f"{i:.4f}" for i in self.delta])
                )
        else:
            self.delta = pos - np.array(info[current][["X", "Y", "Z"]].tolist())

            if self.parent().ui.MultiScan_delta_max.isChecked():
                if ((self.delta**2).sum())**0.5 > self.parent().ui.MultiScan_delta_maxValue.value():
                    self.delta = np.array([0, 0, 0])
                    return False

            self.parent().ui.MultiScan_delta.setText(
                ",".join([f"{i:.4f}" for i in self.delta])
            )
        self.parent().MultiScan_update()
        return True

    def center_preparation_protocol(self):
        TRACKING = True
        info, current = self.parent().MultiScan_getInfo()

        if current >= len(info):
            return
        pos = np.array(
            [info[current]["X"], info[current]["Y"], info[current]["Z"]]
        )  # + self.delta
        # if info[current]['Type']!=b'Anchor':
        # 	pos +=self.delta

        if TRACKING:
            pos += self.delta

        self.parent().piStage_moveTo(axis=[0, 1, 2], target=pos, wait=True)
        if self.params.child("Config", "Polarization control").value():
            if not np.isnan(info[current]["Polarization"]):
                self.parent().HWP_Polarization_moveTo(
                    info[current]["Polarization"], wait=False
                )

        info, current = self.parent().MultiScan_getInfo()
        if info[current]["Type"] == b"Anchor":
            if (
                self.skip_counter_general
                == self.params.child(
                    "Config", "Optimize on anchor", "General optimization", "Skip"
                ).value()
            ):
                if not self._optimize_position(info, current, pos):
                    self._optimize_position(info, current, pos)

                self.skip_counter_general = 0
            else:
                self.skip_counter_general += 1

            if (
                self.params.child(
                    "Config",
                    "Optimize on anchor",
                    "BeamShift+Delay",
                    "Optimize delay",
                ).value()
                or self.params.child(
                    "Config",
                    "Optimize on anchor",
                    "BeamShift+Delay",
                    "Optimize shiftV",
                ).value()
                or self.params.child(
                    "Config",
                    "Optimize on anchor",
                    "BeamShift+Delay",
                    "Optimize shiftH",
                ).value()
            ):
                if (
                    self.skip_counter_shift_delay
                    == self.params.child(
                        "Config", "Optimize on anchor", "BeamShift+Delay", "Skip"
                    ).value()
                ):
                    if not self._optimize_delay_overlap(info, current):
                        self._optimize_delay_overlap(info, current)
                    self.skip_counter_shift_delay = 0
                else:
                    self.skip_counter_shift_delay += 1

    def process_script_line(self, index, script_line, iteration):
        print(script_line.to_dict())
        t = []
        t.append(time.time())

        if script_line["type"] == "move":
            target = script_line["target"]
            if "Delay_line_position" in script_line["axis"]:
                if self.params.child(
                    "Config", "Optimize on anchor", "BeamShift+Delay", "Optimize delay"
                ).value():
                    if not self.params.child(
                        "Config",
                        "Optimize on anchor",
                        "BeamShift+Delay",
                        "Save optimized",
                    ).value():
                        target += self.parent().ui.MultiScan_delay_error.value()

            #self.parent().ui.MultiScan_probe.viewport().update()
            self.parent().abstractMoveTo(target, axis=script_line["axis"])
            #self.parent().ui.MultiScan_probe.viewport().update()
            # if script_line["axis"] == "CenterIndex":
            #     for i in range(100):
            #         self.parent().ui.MultiScan_probe.setCurrentCell(int(target),0)
            #         self.parent().ui.MultiScan_probe.viewport().update()
            #         pos = self.parent().abstractGetPosition(axis="CenterIndex")
            #         if pos == target: break
            #         time_sleep(0.1)

            self._targets[int(script_line.level)] = target
            t.append(time.time())
            self.params.child("Config", "PlotAxis").setValue(script_line["axis"])
            if script_line["axis"] == "CenterIndex":
                self.center_preparation_protocol()

        elif script_line["type"] == "afterLoop":
            # self.delta_tmp = []
            self.parent().ui.MultiScan_delta.setText(
                ",".join([f"{i:.4f}" for i in self.delta])
            )
            self._preview_start = self._write_to_row

        elif script_line["type"] == "wait":
            self.time_sleep(script_line["target"])

        elif script_line["type"] == "read":
            self.parent().laserClient.kickWatchdog()
            t.append(time.time())
            self.time_sleep(self.params.child("Config", "WaitBeforeReadout").value())

            VIBRATE = False

            if VIBRATE:

                def vibrate():
                    mean = self.parent().abstractGetPosition("XY")
                    print(mean)
                    cov = np.eye(2)*0.05
                    test_points = np.random.multivariate_normal(mean,cov,1000)
                    exposure = self.parent().andorCameraGetExposure()
                    t0 = time.time()
                    for row in test_points:
                        self.parent().abstractMoveTo(row, axis="XY")
                        if (time.time()-t0) > exposure:
                            break
                    self.parent().abstractMoveTo(mean, axis="XY")

                lines = self.parent().getUnifiedData(
                    save_metadata=self.save_all_metadata, axes_positions_skip=self.scan_axes,
                    _inject_func=vibrate
                )

            else:
                lines = self.parent().getUnifiedData(
                    save_metadata=self.save_all_metadata, axes_positions_skip=self.scan_axes
                )
            t.append(time.time())
            # self.checkOverflow()

            self.activeDataset[list(lines.dtype.names)][self._write_to_row] = lines

            t.append(time.time())
            self.activeDataset["iteration"][self._write_to_row][:] = iteration
            t.append(time.time())
            for i in range(len(self.scan_axes)):
                pos = self.parent().abstractGetPosition(axis=self.scan_axes[i])
                if self.scan_axes[i] == "Delay_line_position_zero_relative":
                    if self.params.child(
                        "Config",
                        "Optimize on anchor",
                        "BeamShift+Delay",
                        "Optimize delay",
                    ).value():
                        if not self.params.child(
                            "Config",
                            "Optimize on anchor",
                            "BeamShift+Delay",
                            "Save optimized",
                        ).value():
                            pos -= self.parent().ui.MultiScan_delay_error.value()
                self.activeDataset["position"][self.scan_axes[i]][
                    self._write_to_row
                ] = pos
            t.append(time.time())

            data_slice = slice(self._preview_start, self._write_to_row + 1)
            x = self.activeDataset["position"][
                self.params.child("Config", "PlotAxis").value()
            ][data_slice].copy()
            info_ = f"{[round(t,5) for t in self._targets[:-1]]}"
            items = []
            if len(x) > 1:
                for source in self.activeDataset["data"].dtype.names:
                    for name in self.activeDataset["data"][source]["data"].dtype.names:
                        items.append(
                            {
                                "type": "Scan",
                                "name": f"{source}_{name}",
                                "info": info_,
                                #'index': i,
                                "time": time.time(),
                                "data": [
                                    x,
                                    self.activeDataset["data"][source]["data"][name][
                                        data_slice
                                    ].copy(),
                                ],
                            }
                        )
                    if "raw" in self.activeDataset["data"][source].dtype.names:
                        raw_x_name = self.activeDataset["data"][source][
                            "raw"
                        ].dtype.names[0]
                        for name in self.activeDataset["data"][source][
                            "raw"
                        ].dtype.names[1:-1]:
                            if name in ["type", "VRange","time"]: continue
                            items.append(
                                {
                                    "type": "raw",
                                    "name": f"{source}_{name}",
                                    "info": info_,
                                    #'index': i,
                                    "time": time.time(),
                                    "data": [
                                        self.activeDataset["data"][source]["raw"][
                                            raw_x_name
                                        ][self._write_to_row].copy(),
                                        self.activeDataset["data"][source]["raw"][name][
                                            self._write_to_row
                                        ].copy(),
                                    ],
                                }
                            )
                t.append(time.time())
                self.dataReadySignal.emit(items)
            t.append(time.time())
            self._write_to_row += 1

            if self.parent().ui.actionPause.isChecked():
                while self.parent().ui.actionPause.isChecked():
                    self.time_sleep(0.1)

            try:
                progress = next(self.progress)
                info = process_progress_info(
                    progress, self.total_steps, self.start_time, self.cycle_time
                )
                self.progressSignal.emit(int(round(progress)))
                self.parent().progressBar.setToolTip(info)
            except Exception as ex:
                logging.error("progressError", exc_info=ex)
            t.append(time.time())

        elif script_line["type"] == "finish":

            self.running = False

        print(np.diff(t))

    @isRunning
    def run(self):

        self.delta = np.zeros(
            3
        )  # ([float(i) for i in self.parent().ui.MultiScan_delta.text().split(',')])
        self.delta = np.array(
            [float(i) for i in self.parent().ui.MultiScan_delta.text().split(",")]
        )
        # self.delta_tmp = []
        self.parent().ui.MultiScan_delta.setText(
            ",".join([f"{i:.4f}" for i in self.delta])
        )
        self.parent().ui.MultiScan_delay_error.setValue(0)

        with exdir.File(self.fname) as self.store:
            self.params = self.parent().MultiScan_tree.params

            if not "MultiScan" in self.store:
                self.storeActiveGroup = self.store.create_group(
                    "MultiScan"
                ).create_group(time.strftime("data%Y%m%d-%H%M%S"))
            else:
                self.storeActiveGroup = self.store["MultiScan"].create_group(
                    time.strftime("data%Y%m%d-%H%M%S")
                )
            self.source = self.parent().readoutSources.currentText()

            self.save_img()

            self.save_all_metadata = self.params.child(
                "Config", "Save all metadata"
            ).value()

            self.script, self.iterations = self.parent().MultiScan_tree.generateScript()
            print(self.script["axis"].dropna().unique())
            self.scan_axes = self.script["axis"].dropna().unique()

            lines = self.parent().getUnifiedData(
                save_metadata=self.save_all_metadata, axes_positions_skip=self.scan_axes
            )
            pos_axis_val = []
            pos_axis_dtype = []

            for ax in self.scan_axes:
                pos_axis_val.append(self.parent().abstractGetPosition(axis=ax))
                pos_axis_dtype.append((ax, AXES_UNITS_DTYPES[ax][1]))
            if self.save_all_metadata:
                dtype = [
                    ("time", "f8"),
                    ("position", pos_axis_dtype),
                    ("data", lines.dtype["data"]),
                    ("metadata", lines.dtype["metadata"]),
                    ("iteration", "i4", (self.iterations.shape[1],)),
                ]
            else:
                dtype = [
                    ("time", "f8"),
                    ("position", pos_axis_dtype),
                    ("data", lines.dtype["data"]),
                    ("iteration", "i4", (self.iterations.shape[1],)),
                ]
            print(dtype)
            dtype = np.dtype(dtype)

            first_index = 0
            if self.source == "AndorCamera":
                lines["data"]["AndorCamera"]["raw"][:] = self.parent().andorCCDBaseline
                first_index = 1
            else:
                pass
            N_rows = (self.script["type"] == "read").sum()

            self.parent().scriptThread_lastStorePath = (
                self.store.directory.absolute(),
                self.storeActiveGroup.name,
            )
            self.storeActiveGroup.attrs[
                "README"
            ] = self.parent().ui.readme_textBrowser.toPlainText()
            self.activeDataset = self.storeActiveGroup.create_dataset(
                "data", shape=(N_rows + first_index,), dtype=dtype
            )

            self.total_steps = len(self.activeDataset)
            print(self.scan_axes.tolist(), type(self.scan_axes.tolist()))

            if (
                self.params.child("Config", "PlotAxis").opts["limits"]
                != self.scan_axes.tolist()
            ):
                self.params.child("Config", "PlotAxis").setLimits(
                    self.scan_axes.tolist()
                )
                self.params.child("Config", "PlotAxis").setValue(self.scan_axes[-1])

            self.progress = iter(np.linspace(0, 100, self.total_steps).tolist())

            self.activeDataset["time"][0] = time.time()
            self.activeDataset["data"][0] = lines["data"]
            if self.save_all_metadata:
                self.activeDataset["metadata"][0] = lines["metadata"]
            print(self.activeDataset["position"][0], pos_axis_val)
            for i in range(len(self.scan_axes)):
                self.activeDataset["position"][i][0] = pos_axis_val[i]
            self.activeDataset["iteration"][0] = -1

            self.activeDataset.attrs["axes_units_dtypes"] = AXES_UNITS_DTYPES
            self.activeDataset.attrs["sources_units_dtypes"] = AXES_UNITS_DTYPES

            self.activeDataset.attrs["readoutSource"] = self.source
            self.activeDataset.attrs["header"] = str(lines.dtype)
            self.activeDataset.attrs[
                "filters"
            ] = self.parent().filtersPiezoStage_Dict  # .to_dict()
            info, current = self.parent().MultiScan_getInfo()
            # print({k:info[k] for k in info.dtype.names})
            self.activeDataset.attrs["centerIndex_info"] = {
                k: info[k].tolist() for k in info.dtype.names
            }
            axes_positions = [
                "HWP_TUN_angle",
                "HWP_FIX_angle",
                "HWP_TUN_power",
                "HWP_FIX_power",
                "HWP_TUN_intens",
                "HWP_FIX_intens",
                "Delay_line_position",
                "laserBeamShift_H",
                "laserBeamShift_V",
                "Knife_position",
                "Laser_precompensation",
                "LaserWavelength",
                "filtersPiezoStage",
                "shamrockWavelength",
                "shamrockGrating",
            ]
            metadata = self.parent().abstractGetMetadata(axes_positions=axes_positions)
            metadata_ = {}
            for k in metadata.dtype.names:
                if hasattr(metadata[k], "dtype"):
                    if metadata[k].ndim == 0:
                        metadata_[k] = metadata[k].reshape(1)[0].tolist()
                    else:
                        metadata_[k] = metadata[k].tolist()
                elif type(metadata[k]) in (float, str, int, list):
                    metadata_[k] = metadata[k]

                else:
                    metadata_[k] = str(metadata[k])
                print("!!!!!!!", k, metadata_[k], type(metadata_[k]))
                self.activeDataset.attrs[k] = metadata_[k]

            self.lastUpdate = time.time()
            self._write_to_row = first_index
            self._preview_start = self._write_to_row
            self._targets = [-1] * int(self.script.level.max() + 1)

            self.cycle_time = deque([time.time()], maxlen=1000)

            for i, script_line in self.script.iterrows():
                if not self.running:
                    break
                try:
                    self.process_script_line(i, script_line, self.iterations[i])
                except Exception as ex:
                    logging.error("MultiScan:ERROR", exc_info=ex)
                    break
            # print(self.activeDataset.dtype)
            del self.activeDataset
            del self.storeActiveGroup

        self.store.close()
        # self.stop()
