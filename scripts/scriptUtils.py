import sys
from pathlib import Path
import numpy as np
import pandas as pd


import logging
import time
import datetime

import traceback


import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

# QtCore.pyqtRemoveInputHook()
# import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import (
    Parameter,
    ParameterTree,
    ParameterItem,
    registerParameterType,
)

import json

sys.path.append(str(Path("utils")))
from units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES


def gen_default_axis(
    name,
    config=dict(
        axis="X",
        active=False,
        start=0,
        end=100,
        step=1,
        grid=None,
    ),
):
    bygrid = False
    if "grid" in config:
        if not config["grid"] is None:
            grid = config["grid"]
            bygrid = True
        else:
            grid = "0,50,100"
    else:
        grid = "0,50,100"

    if not "step" in config:
        config["step"] = 1
    if not "start" in config:
        config["start"] = 0
    if not "end" in config:
        config["end"] = 100
    if not "wait" in config:
        config["wait"] = 0
    if not "removable" in config:
        config["removable"] = True
    if not "Active" in config:
        config["Active"] = True

    ax = {
        "name": name,
        "type": "group",
        "removable": config["removable"],
        "children": [
            {
                "name": "Axis",
                "type": "list",
                "values": list(AXES_UNITS_DTYPES.keys()),
                "value": config["axis"],
            },
            {"name": "Active", "type": "bool", "value": config["active"]},
            {
                "name": "Start",
                "type": "float",
                "value": config["start"],
                "decimals": 4,
                "visible": not bygrid,
            },
            {
                "name": "End",
                "type": "float",
                "value": config["end"],
                "decimals": 4,
                "visible": not bygrid,
            },
            {
                "name": "Step",
                "type": "float",
                "value": config["step"],
                "decimals": 4,
                "visible": not bygrid,
            },
            {"name": "Wait", "type": "float", "value": config["wait"], "decimals": 4},
            {
                "name": "ScanByGrid",
                "type": "bool",
                "value": bygrid,
                "children": [
                    {"name": "Grid", "type": "str", "value": grid, "visible": bygrid}
                ],
            },
        ],
    }
    return ax


def isRunning(f):
    def wrapper(*args, **kw):
        r = None
        try:
            self = args[0]
            self.running = True
            r = f(*args, **kw)
            self.running = False
            self.finishedSignal.emit(time.time())
        except Exception as ex:
            logging.error("Script error:", exc_info=ex)
            self.finishedSignal.emit(time.time())
            self.running = False
        return r

    return wrapper


def scan_move_recursive(
    number_of_loops,
    move_function,
    readout_function,
    after_last_loop_func,
    iter_index=0,
    range_list=[],
    _iter_list=[],
    _iter_list_counter=[],
):

    if _iter_list == []:
        _iter_list = [0] * number_of_loops
        _iter_list_counter = [0] * number_of_loops

    if iter_index == number_of_loops - 1:
        for i, _iter_list[iter_index] in enumerate(range_list[iter_index]):
            _iter_list_counter[iter_index] = i
            state = move_function(_iter_list[iter_index], axis_index=iter_index)
            if not state:
                break
            readout_function(_iter_list, _iter_list_counter)
        if not after_last_loop_func is None:
            after_last_loop_func(_iter_list, _iter_list_counter, iter_index)

    else:
        for i, _iter_list[iter_index] in enumerate(range_list[iter_index]):
            _iter_list_counter[iter_index] = i
            state = move_function(_iter_list[iter_index], axis_index=iter_index)
            if not state:
                break
            scan_move_recursive(
                number_of_loops,
                _iter_list=_iter_list,
                _iter_list_counter=_iter_list_counter,
                range_list=range_list,
                move_function=move_function,
                readout_function=readout_function,
                after_last_loop_func=after_last_loop_func,
                iter_index=iter_index + 1,
            )


def process_progress_info(progress, total_steps, start_time, cycle_time):
    cycle_time.append(time.time())
    info = {"progress": progress}
    dt = float(np.diff(cycle_time).mean())
    time_to_end = datetime.timedelta(seconds=int((1 - progress / 100) * total_steps * dt))
    now = datetime.datetime.now()
    time_from_start = now - start_time
    expected_finish = now + time_to_end
    info = f"%:\t{progress:.3f}%\n"
    info += f"dt:\t{dt:.3f}s\n"
    info += f'start:\t{start_time.strftime("%d.%m-%H:%M:%S")}\t-[ {time_from_start} ]\n'
    info += f'finish:\t{expected_finish.strftime("%d.%m-%H:%M:%S")}\t+[ {time_to_end} ]'
    return info


class ScriptTree(QtGui.QWidget):
    depth = 0
    default_state = None
    default_axis = dict(axis="X", active=False, start=0, end=100, step=1, grid=None)
    cloned_axis = None
    def __init__(
        self,
        ax_config=[dict(axis="Z", active=True, start=0, end=100, step=1, grid=None)],
        config=[{"name": "WaitBeforeReadout", "type": "float", "value": 0}],
        default_axis=None,
    ):
        super().__init__()
        layout = QtGui.QGridLayout()
        self.setLayout(layout)
        self.default_axis = default_axis
        params = [
            {"name": "Config", "type": "group", "children": config},
            {
                "name": "Script",
                "type": "group",
                "children": [
                    # 	gen_default_axis('Axis#', 'X'),
                ],
            },
        ]

        ## Create tree of Parameter objects
        self.params = Parameter.create(name="params", type="group", children=params)

        self.params.sigTreeStateChanged.connect(self.on_change)

        ## Create ParameterTree widget
        self.tree = ParameterTree()
        self.tree.setParameters(self.params, showTop=False)

        self.cloneButton = QtGui.QPushButton("Clone")
        self.cloneButton.setCheckable(True)
        self.cloneButton.toggled[bool].connect(self.cloneScript)
        layout.addWidget(self.cloneButton, 2, 3, 1, 1)


        if len(ax_config) >= 1:
            names = ["Script", "Axis#0"]
            child = self.addAxis(name="Axis#0", config=ax_config[0])

            for i, conf in enumerate(ax_config[1:]):
                name = "Axis#0" + ".0" * (i + 1)
                child = self.addSubAxis(name=name, parent=child, config=conf)

        layout.addWidget(self.tree, 0, 0, 1, 4)

        addButton = QtGui.QPushButton("Add")
        addButton.clicked.connect(self.addAxis)
        layout.addWidget(addButton, 1, 0, 1, 1)

        addSubButton = QtGui.QPushButton("Sublevel")
        addSubButton.clicked.connect(self.addSubAxis)
        layout.addWidget(addSubButton, 1, 1, 1, 1)

        removeButton = QtGui.QPushButton("Remove")
        removeButton.clicked.connect(self.removeAxis)
        layout.addWidget(removeButton, 1, 2, 1, 1)

        generateButton = QtGui.QPushButton("Generate")
        generateButton.clicked.connect(self.generateScript)
        layout.addWidget(generateButton, 1, 3, 1, 1)

        saveButton = QtGui.QPushButton("Save")
        saveButton.clicked.connect(self.saveScript)
        layout.addWidget(saveButton, 2, 0, 1, 1)

        loadButton = QtGui.QPushButton("Load")
        loadButton.clicked.connect(self.loadScript)
        layout.addWidget(loadButton, 2, 1, 1, 1)

        resetButton = QtGui.QPushButton("Reset")
        resetButton.clicked.connect(self.resetScript)
        layout.addWidget(resetButton, 2, 2, 1, 1)



        # self.show()

        self.default_state = self.params.saveState()

    def saveScript(self):
        state = self.params.saveState()
        path = QtGui.QFileDialog.getSaveFileName(self, "Save File")[0]
        if path:
            with open(path, "w") as file:
                json.dump(state, file)
        print(state)

    def loadScript(self):
        path = QtGui.QFileDialog.getOpenFileName(self, "Load sctipt")[0]
        if path:
            with open(path, "r") as file:
                state = json.load(file)

            self.params.restoreState(
                state,
            )

    def resetScript(self):
        if not self.default_state is None:
            self.params.restoreState(
                self.default_state,
            )

    def release_cloneButton(self):
        self.cloned_axis = None
        self.cloneButton.blockSignals(True)
        self.cloneButton.setChecked(False)
        self.cloneButton.blockSignals(False)

    def cloneScript(self, state):
        if state:
            try:
                child = self.tree.itemFromIndex(self.tree.currentIndex())[0].param
            except IndexError:
                self.release_cloneButton()

                return
            if "Axis#" in child.name():
                self.cloned_axis = child.saveState()
            else:
                self.release_cloneButton()

    def on_change(self, param, changes):
        # print("tree changes:")
        # self.get_depth()
        for param, change, data in changes:
            path = self.params.childPath(param)
            # print(path)
            if path is not None:
                childName = ".".join(path)
            else:
                childName = param.name()

            # print('  parameter: %s'% childName)
            # print('  change:	%s'% change)
            # print('  data:	  %s'% str(data))
            # print('  ----------')
            if path is None:
                continue
            if path[-1] == "ScanByGrid" and change == "value":
                parent = self.params.child(*path[:-1])
                checkbox = self.params.child(*path)
                if checkbox.value():
                    parent.child("Start").hide()
                    parent.child("End").hide()
                    parent.child("Step").hide()
                    checkbox.child("Grid").show()
                else:
                    parent.child("Start").show()
                    parent.child("End").show()
                    parent.child("Step").show()
                    checkbox.child("Grid").hide()
            elif path[-1] == "Active" and change == "value":
                pass
                """
				parent = self.params.child(*path[:-1])
				checkbox = self.params.child(*path)
				for child in parent.childs[2:]:
					print(path, child.name())
					if checkbox.value():
						child.show()
					else:
						child.hide()
				dir(child)

				"""

    def removeAxis(self):
        if len(self.tree.selectedIndexes()) == 0:
            return
        child = self.tree.itemFromIndex(self.tree.currentIndex())[0].param
        if "Axis#" in child.name():
            if child.opts["removable"]:
                child.parent().removeChild(child)

    def addAxis(
        self,
        status=None,
        name="Axis#0",
        config=None,
    ):
        # print(self.tree.itemFromIndex(self.tree.currentIndex())[0])
        # print(dir(self.tree.itemFromIndex(self.tree.currentIndex())[0]))
        if config is None:
            config = self.default_axis

        try:
            child = self.tree.itemFromIndex(self.tree.currentIndex())[0].param
        except IndexError:
            child = self.params.child("Script")
        indexOfChild = 0
        if "Axis#" in child.name():

            for i, child_ in enumerate(child.parent().childs):
                if child_ == child:
                    indexOfChild = i
                    break
            if self.cloneButton.isChecked():
                ax = child.parent().insertChild(
                    indexOfChild + 1,
                    self.cloned_axis,
                    autoIncrementName=True,
                )
                self.release_cloneButton()
                return ax
            else:
                return child.parent().insertChild(
                    indexOfChild + 1,
                    gen_default_axis(child.name(), config=config),
                    autoIncrementName=True,
                )
        elif child.name() == "Script":
            ax = child.insertChild(
                0, gen_default_axis(name, config=config), autoIncrementName=True
            )
            self.release_cloneButton()
            return ax


        else:
            raise NameError('Axis name should start with "Axis#"')

    def addSubAxis(
        self,
        status=None,
        parent=None,
        name=None,
        config=None,
    ):
        if config is None:
            config = self.default_axis
        # print(self.tree.itemFromIndex(self.tree.currentIndex())[0])
        # print(dir(self.tree.itemFromIndex(self.tree.currentIndex())[0]))
        if parent is None:
            parent = self.tree.itemFromIndex(self.tree.currentIndex())[0].param
            name = parent.name() + ".0"
        if "Axis#" in name:

            if self.cloneButton.isChecked():
                ax = parent.addChild(
                    self.cloned_axis, autoIncrementName=True
                    )
                self.release_cloneButton()
                return ax
            else:
                return parent.addChild(
                    gen_default_axis(name, config=config), autoIncrementName=True
                    )

        else:
            raise NameError('Axis name should start with "Axis#"')

    def generateScript(
        self,
        state=None,
        scriptGen_readFunc=None,
        scriptGen_moveFunc=None,
        scriptGen_afterLoopFunc=None,
        scriptGen_waitFunc=None,
        scriptGen_readFunc3D=None,
    ):
        script = []
        if scriptGen_readFunc is None:
            scriptGen_readFunc = self.scriptGen_readFunc
        if scriptGen_moveFunc is None:
            scriptGen_moveFunc = self.scriptGen_moveFunc
        if scriptGen_afterLoopFunc is None:
            scriptGen_afterLoopFunc = self.scriptGen_afterLoopFunc
        if scriptGen_waitFunc is None:
            scriptGen_waitFunc = self.scriptGen_waitFunc

        Scan3D = False
        if not scriptGen_readFunc3D is None:
            Scan3D = True
        script = []

        def recGenScript(level, script, parent, Scan3D=Scan3D):
            sub_levels = 0
            sub_lev = 0
            for child in parent:

                if "Axis#" in child.name():
                    # print(child.name(),'-'*100)
                    if not child.child("Active").value():
                        continue
                    if child.child("ScanByGrid").value():
                        targets = np.array(
                            [
                                float(s)
                                for s in child.child("ScanByGrid")
                                .child("Grid")
                                .value()
                                .split(",")
                            ]
                        )
                    else:
                        if child.child("Axis").value() in ['dX', 'dY', 'dZ']:
                            targets = np.arange(
                                child.child("Start").value(),
                                child.child("End").value(),
                                child.child("Step").value(),
                            )
                            if not child.child("End").value() in targets:
                                targets = np.append(targets, child.child("End").value())

                            targets = np.insert(np.diff(targets),[0,len(targets)-1],[targets[0],-targets[-1]])
                        else:
                            targets = np.arange(
                                child.child("Start").value(),
                                child.child("End").value(),
                                child.child("Step").value(),
                            )
                            if not child.child("End").value() in targets:
                                targets = np.append(targets, child.child("End").value())

                    for i, target in enumerate(targets):
                        if child.child("Axis").value() == 'XYZ':
                            scriptGen_readFunc3D(level, script, child, i)
                        else:
                            scriptGen_moveFunc(
                                level, script, child, i, child.child("Axis").value(), target
                            )
                        if child.child("Wait").value() > 0:
                            scriptGen_waitFunc(
                                level,
                                script,
                                child,
                                i,
                                wait=child.child("Wait").value(),
                            )

                        sub_lev = recGenScript(level + 1, script, child)
                        if sub_lev == 0:
                            if not Scan3D:
                                scriptGen_readFunc(level, script, child, i)

                    if sub_lev == 0:
                        if not Scan3D:
                            scriptGen_afterLoopFunc(level, script, child, i)

                    sub_levels += 1
            # print(sub_levels)
            return sub_levels

        recGenScript(level=0, script=script, parent=self.params.child("Script"))
        script.append(
            {
                "type": "finish",
            }
        )
        df = pd.DataFrame(script)
        # df.to_csv('/tmp/script.dat')
        axes = df["axis"].dropna().unique()

        iterations = np.full((len(df), len(axes)), np.nan)
        for i, ax in enumerate(axes):
            w = df["axis"] == ax
            iterations[:, i][w] = df.iteration[w]
        iterations = (
            pd.DataFrame(iterations)
            .fillna(
                method="ffill",
            )
            .fillna(0)
            .values
        )

        # def get_max_depth(arr, depth=[0]):
        # 	print(arr)
        # 	depth[0] = max(len(arr),depth[0])
        # 	return depth[0]

        # res=df.iteration.apply(get_max_depth)
        # print(df)
        # for i,line in enumerate(script):
        # 	if line['type']=='move':
        # 		print(f"Move: ax[{line['axis']}]->{line['target']}")
        # 	elif line['type']=='read':
        # 		print(f"Read")
        # 	elif line['type']=='afterLoop':
        # 		print('ActionAfterLast')
        # 	elif line['type']=='finish':
        # 		print('FINISH')

        print(df)

        return df, iterations

    def scriptGen_readFunc(
        self, level, script, child, _iter=0, axis=np.nan, target=np.nan
    ):
        script.append(
            {
                "iteration": _iter,
                "level": level,
                "axis": axis,
                "target": target,
                "type": "read",
            }
        )

    def scriptGen_afterLoopFunc(
        self, level, script, child, _iter=0, axis=np.nan, target=np.nan
    ):
        script.append(
            {
                "iteration": _iter,
                "level": level,
                "axis": axis,
                "target": target,
                "type": "afterLoop",
            }
        )

    def scriptGen_waitFunc(
        self, level, script, child, _iter=0, axis=np.nan, wait=np.nan
    ):
        script.append(
            {
                "iteration": _iter,
                "level": level,
                "axis": axis,
                "target": wait,
                "type": "wait",
            }
        )

    def scriptGen_moveFunc(self, level, script, child, _iter, axis, target):
        script.append(
            {
                "iteration": _iter,
                "level": level,
                "axis": axis,
                "target": target,
                "type": "move",
            }
        )


#
# ## test save/restore
# s = p.saveState()
# p.restoreState(s)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == "__main__":
    app = QtGui.QApplication([])

    win = QtGui.QMainWindow()
    params = ScriptTree()
    win.setCentralWidget(params)
    win.show()

    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        QtGui.QApplication.instance().exec_()
