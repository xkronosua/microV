import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir
from pathlib import Path
import time
import datetime
import traceback
import threading
from scipy.interpolate import interp1d
import numpy.lib.recfunctions as rfn
import pint
from collections import deque

sys.path.append(str(Path("../utils")))
# sys.path.append(str(Path('../scripts')))

from .scriptUtils import scan_move_recursive, isRunning, process_progress_info

from utils.units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES

ureg = pint.UnitRegistry()
Q_ = ureg.Quantity

params = [
    {
        "name": "Additional parameters",
        "type": "group",
        "children": [
            {"name": "Save all metadata", "type": "bool", "value": True},
            {"name": "Save raw", "type": "bool", "value": True},
            {
                "name": "AndorCamera A",
                "type": "list",
                "value": "2w1",
                "values": [
                    "w1",
                    "2w1",
                    "3w1",
                    "w2",
                    "2w2",
                    "3w2",
                    "w1+w2",
                    "2w1-w2",
                    "2w2-w1",
                    "2w1+w2",
                    "2w2+w1",
                    "All",
                ],
            },
            {
                "name": "AndorCamera B",
                "type": "list",
                "value": "2w2",
                "values": [
                    "w1",
                    "2w1",
                    "3w1",
                    "w2",
                    "2w2",
                    "3w2",
                    "w1+w2",
                    "2w1-w2",
                    "2w2-w1",
                    "2w1+w2",
                    "2w2+w1",
                    "All",
                ],
            },
        ],
    },
    {
        "name": "MetaAxes",
        "type": "group",
        "children": [
            {
                "name": "Axis0",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "LaserWavelength",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1, "decimals": 4},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                ],
            },
            {
                "name": "Axis1",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "Delay_line_position",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                ],
            },
        ],
    },
]


class scan3DThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    running = False
    store = None
    fname = "tmp"
    header = ["index", "axis", "A", "B", "C"]

    def __init__(self, parent=None):
        super(scan3DThread, self).__init__(parent)

        self.fname = Path(self.parent().expData_filePath.text())
        if ".exdir" in str(self.fname):
            pass
        else:
            self.fname = str(self.fname).split(".exdir")[0] + ".exdir"
        self.store = exdir.File(self.fname)
        self.start_time = datetime.datetime.now()
        self.cycle_time = deque([time.time()], maxlen=1000)

    def stop(self):
        self.running = False

    @isRunning
    def run(self):

        params = self.parent().scan3D_params

        if not "scan3D" in self.store:
            self.storeActiveGroup = self.store.create_group("scan3D").create_group(
                "data" + str(time.time())
            )
        else:
            self.storeActiveGroup = self.store["scan3D"].create_group(
                "data" + str(time.time())
            )

        self.source = self.parent().readoutSources.currentText()

        # self.header = ['time',]+[child['Axis'] for child in params.childs] + ['A','B','C']
        # self.storeActiveGroup.attrs['readoutSource'] = self.source
        # self.storeActiveGroup.attrs['header'] = self.header
        self.save_all_metadata = (
            params.child("Additional parameters").child("Save all metadata").value()
        )
        self.save_raw = params.child("Additional parameters").child("Save raw").value()

        # TODO:
        self.scan_axes = []
        self.meta_axes = []
        self.scan_ranges = []

        for i in range(len(params.child("MetaAxes").childs)):
            child = params.child("MetaAxes").childs[i]
            if child["Active"]:
                if child["ScanByGrid"]:
                    if ";" in child["Grid"]:
                        tmp = child["Grid"].split(";")
                        scan_range_tmp = []
                        for t in tmp:
                            start, stop, step = [float(i) for i in t.split(",")]

                            scan_range_tmp.append(np.arange(start, stop, step))
                            # scan_range_tmp.append([stop])
                        print(scan_range_tmp)
                        scan_range_tmp = np.hstack(scan_range_tmp)
                    else:
                        scan_range_tmp = np.array(
                            [float(i) for i in child["Grid"].split(",")]
                        )

                else:
                    scan_range_tmp = np.arange(
                        child["Start"], child["End"], child["Step"]
                    )
                    if not child["End"] in scan_range_tmp:
                        scan_range_tmp = np.append(scan_range_tmp, child["End"])
                self.scan_axes.append(child["Axis"])
                self.scan_ranges.append(scan_range_tmp)
                self.meta_axes.append(child["Axis"])

        meta_axes_N_scans = int(
            np.prod([len(self.scan_ranges[i]) for i in range(len(self.scan_ranges))])
        )

        positions_dtype = []
        for ax in self.scan_axes:
            positions_dtype.append((ax, AXES_UNITS_DTYPES[ax][1]))

        for ax in self.scan_axes:
            pos_axis = self.parent().abstractGetPosition(axis=ax)

        axes_tmp = list(self.parent().scan3D_config["scanOrder"].currentText())

        scale = {}

        for i, ax in enumerate(axes_tmp):
            child = self.parent().scan3D_config[ax]

            self.scan_axes.append(ax)

            if child["scanByGrid"].isChecked():
                scan_range_tmp = np.array(
                    [float(i) for i in child["Grid"].text().split(",")]
                )
                s = np.diff(scan_range_tmp).mean()
                if np.isnan(s):
                    scale[ax] = 0
                else:
                    scale[ax] = s
            else:
                scan_range_tmp = np.arange(
                    child["start"].value(), child["end"].value(), child["step"].value()
                )
                scale[ax] = child["step"].value()
                if not child["end"].value() in scan_range_tmp:
                    scan_range_tmp = np.append(scan_range_tmp, child["end"].value())

            print(scan_range_tmp)
            self.scan_ranges.append(scan_range_tmp)

            # self.scan_axes.append(child['Axis'])

        lines = self.parent().getUnifiedData(
            save_raw=self.save_raw,
            save_metadata=self.save_all_metadata,
            axes_positions=["X", "Y", "Z", "X_target", "Y_target", "Z_target"],
            parameters=["laserBultInPower"],
        )

        print(lines.dtype["data"])
        print(lines.dtype["metadata"])
        ax_x = self.scan_axes.index("X")
        ax_y = self.scan_axes.index("Y")
        ax_z = self.scan_axes.index("Z")
        t = [
            ("time", "f8"),
            ("position", positions_dtype),
            (
                "data",
                lines.dtype["data"],
                (
                    len(self.scan_ranges[ax_x]),
                    len(self.scan_ranges[ax_y]),
                    len(self.scan_ranges[ax_z]),
                ),
            ),
            (
                "metadata",
                lines.dtype["metadata"],
                (
                    len(self.scan_ranges[ax_x]),
                    len(self.scan_ranges[ax_y]),
                    len(self.scan_ranges[ax_z]),
                ),
            ),
            ("iteration", "i4", len(self.scan_axes)),
        ]
        print(t)
        dtype = np.dtype(t)

        self.first_index = 0
        self.activeDataset = self.storeActiveGroup.create_dataset(
            "data", shape=(meta_axes_N_scans + 1,), dtype=dtype
        )
        N = meta_axes_N_scans * np.prod(
            [
                len(self.scan_ranges[ax_x]),
                len(self.scan_ranges[ax_y]),
                len(self.scan_ranges[ax_z]),
            ]
        )
        print(f"progress={N}")

        # self.progress = iter(np.linspace(0,100,int(N),dtype=int).tolist())
        self.progress = iter(np.linspace(0, 100, int(N)).tolist())
        self.total_steps = N
        # self.activeDataset[0] = lines

        self.header = str(dtype)
        self.activeDataset.attrs["axes_units_dtypes"] = AXES_UNITS_DTYPES
        self.activeDataset.attrs["sources_units_dtypes"] = AXES_UNITS_DTYPES

        self.activeDataset.attrs["readoutSource"] = self.source
        self.activeDataset.attrs["header"] = self.header
        self.activeDataset.attrs[
            "filters"
        ] = self.parent().filtersPiezoStage_Dict  # .to_dict()
        self.activeDataset.attrs["scale"] = scale

        metadata = self.parent().abstractGetMetadata()
        metadata_ = {}
        for k in metadata.dtype.names:
            if hasattr(metadata[k], "dtype"):
                if metadata[k].ndim == 0:
                    metadata_[k] = metadata[k].reshape(1)[0].tolist()
                else:
                    metadata_[k] = metadata[k].tolist()
            elif type(metadata[k]) in (float, str, int, list):
                metadata_[k] = metadata[k]

            else:
                metadata_[k] = str(metadata[k])
            print("!!!!!!!", k, metadata_[k], type(metadata_[k]))
            self.activeDataset.attrs[k] = metadata_[k]

        self.lastUpdate = time.time()
        self._write_to_row = self.first_index
        self._prev_axis_index = 0
        self._preview_start = 0
        self._scan_index = 0

        def move_to_target(target, axis_index):
            if self.running:
                if (
                    self._prev_axis_index != axis_index
                    and self._prev_axis_index == len(self.scan_axes) - 1
                ):
                    self._preview_start = self._write_to_row
                    self._scan_index += 1
                    if self._scan_index > 15:
                        self._scan_index = 0

                self._prev_axis_index = axis_index
                ax = self.scan_axes[axis_index]
                self.parent().abstractMoveTo(target, axis=ax, wait=True)
                if ax not in ["X", "Y", "Z"]:
                    pos = self.parent().abstractGetPosition(axis=ax)
                    self.activeDataset["position"][self._write_to_row][ax] = pos

            return self.running

        def after_last_loop_func(iter_list_, _iter_list_counter, iter_index):
            pass

        ax_x = self.scan_axes.index("X")
        ax_y = self.scan_axes.index("Y")
        ax_z = self.scan_axes.index("Z")

        self.data_A = np.zeros(
            (
                len(self.scan_ranges[ax_z]),
                len(self.scan_ranges[ax_x]),
                len(self.scan_ranges[ax_y]),
            )
        )
        self.data_B = np.zeros(
            (
                len(self.scan_ranges[ax_z]),
                len(self.scan_ranges[ax_x]),
                len(self.scan_ranges[ax_y]),
            )
        )

        self._write_to_row = 0

        def readData(iter_list_, _iter_list_counter):
            if self.running:
                print(
                    "iter_list_:",
                    iter_list_,
                    _iter_list_counter,
                    "row",
                    self._write_to_row,
                )
                iter_list = iter_list_[:-1]
                preview_start = self._preview_start
                iter_index = self._scan_index
                lines = self.parent().getUnifiedData(
                    save_raw=self.save_raw,
                    save_metadata=self.save_all_metadata,
                    axes_positions=["X", "Y", "Z", "X_target", "Y_target", "Z_target"],
                    parameters=["laserBultInPower"],
                )

                xi = _iter_list_counter[self.scan_axes.index("X")]
                yi = _iter_list_counter[self.scan_axes.index("Y")]
                zi = _iter_list_counter[self.scan_axes.index("Z")]

                self.activeDataset["time"][self._write_to_row] = time.time()
                self.activeDataset["data"][self._write_to_row][xi, yi, zi] = lines[
                    "data"
                ]
                self.activeDataset["metadata"][self._write_to_row][xi, yi, zi] = lines[
                    "metadata"
                ]
                self.activeDataset["iteration"][self._write_to_row] = np.array(
                    _iter_list_counter
                )

                if self.scan_axes[-1] == "X":
                    lin_scan = self.activeDataset["data"][self._write_to_row][:, yi, zi]
                    x = self.activeDataset["metadata"][self._write_to_row][:, yi, zi][
                        "X"
                    ]
                elif self.scan_axes[-1] == "Y":
                    lin_scan = self.activeDataset["data"][self._write_to_row][xi, :, zi]
                    x = self.activeDataset["metadata"][self._write_to_row][xi, :, zi][
                        "Y"
                    ]
                elif self.scan_axes[-1] == "Z":
                    lin_scan = self.activeDataset["data"][self._write_to_row][xi, yi, :]
                    x = self.activeDataset["metadata"][self._write_to_row][xi, yi, :][
                        "Z"
                    ]

                if "PicoScope" in lines["data"].dtype.names:
                    if "ChA" in lines["data"]["PicoScope"]["data"].dtype.names:
                        self.data_A[zi, xi, yi] = lines["data"]["PicoScope"]["data"][
                            "ChA"
                        ]
                    if "ChB" in lines["data"]["PicoScope"]["data"].dtype.names:
                        self.data_B[zi, xi, yi] = lines["data"]["PicoScope"]["data"][
                            "ChB"
                        ]

                if "AndorCamera" in lines["data"].dtype.names:
                    self.data_A[zi, xi, yi] = lines["data"]["AndorCamera"]["data"][
                        params.child("Additional parameters")
                        .child("AndorCamera A")
                        .value()
                    ]
                    self.data_B[zi, xi, yi] = lines["data"]["AndorCamera"]["data"][
                        params.child("Additional parameters")
                        .child("AndorCamera B")
                        .value()
                    ]

                if "Powermeter" in lines["data"].dtype.names:
                    self.data_A[zi, xi, yi] = lines["data"]["Powermeter"]["data"][
                        "power"
                    ]

                if time.time() - self.lastUpdate > 0.3:
                    w = x != 0
                    items = []
                    # print('a'*100)
                    for source in lin_scan.dtype.names:
                        for name in lin_scan[source]["data"].dtype.names:
                            items.append(
                                {
                                    "type": "scan",
                                    "name": f"{source}_{name}",
                                    "info": f"{iter_list}",
                                    #'index': i,
                                    "time": time.time(),
                                    "data": [x[w], lin_scan[source]["data"][name][w]],
                                }
                            )
                        if "raw" in lines["data"][source].dtype.names and self.save_raw:
                            raw_x_name = lines["data"][source]["raw"].dtype.names[0]
                            for name in lines["data"][source]["raw"].dtype.names[1:-1]:
                                items.append(
                                    {
                                        "type": "raw",
                                        "name": f"{source}_{name}",
                                        "info": "",
                                        #'index': i,
                                        "time": time.time(),
                                        "data": [
                                            lines["data"][source]["raw"][raw_x_name][0],
                                            lines["data"][source]["raw"][name][0],
                                        ],
                                    }
                                )

                    self.dataReadySignal.emit(items)

                    childs = (
                        self.parent().scan3D_config
                    )  # params.child('3DScanAxes').childs

                    zvals = self.scan_ranges[ax_z]
                    zindex = _iter_list_counter[ax_z]

                    self.dataReadySignal.emit(
                        [
                            {
                                "type": "img",
                                "name": "img",
                                "time": time.time(),
                                "data": [
                                    self.data_A,
                                    (
                                        self.scan_ranges[ax_x].min()
                                        - childs["X"]["step"].value() / 2,
                                        self.scan_ranges[ax_y].min()
                                        - childs["Y"]["step"].value() / 2,
                                    ),
                                    (
                                        childs["X"]["step"].value(),
                                        childs["Y"]["step"].value(),
                                    ),
                                    zvals,
                                    zindex,
                                ],
                            },
                            {
                                "type": "img",
                                "name": "img1",
                                "time": time.time(),
                                "data": [
                                    self.data_B,
                                    (
                                        self.scan_ranges[ax_x].min()
                                        - childs["X"]["step"].value() / 2,
                                        self.scan_ranges[ax_y].min()
                                        - childs["Y"]["step"].value() / 2,
                                    ),
                                    (
                                        childs["X"]["step"].value(),
                                        childs["Y"]["step"].value(),
                                    ),
                                    zvals,
                                    zindex,
                                ],
                            },
                        ]
                    )
                    self.lastUpdate = time.time()

                print(
                    "??",
                    _iter_list_counter[-3],
                    len(self.scan_ranges[-3]),
                    _iter_list_counter[-2],
                    len(self.scan_ranges[-2]),
                    _iter_list_counter[-1],
                    len(self.scan_ranges[-1]),
                )
                if (
                    _iter_list_counter[-3] == len(self.scan_ranges[-3]) - 1
                    and _iter_list_counter[-2] == len(self.scan_ranges[-2]) - 1
                    and _iter_list_counter[-1] == len(self.scan_ranges[-1]) - 1
                ):
                    self._write_to_row += 1

                if self.parent().ui.actionPause.isChecked():
                    while self.parent().ui.actionPause.isChecked():
                        time.sleep(0.1)

                try:
                    progress = next(self.progress)
                    info = process_progress_info(
                        progress, self.total_steps, self.start_time, self.cycle_time
                    )
                    self.progressSignal.emit(int(round(progress)))
                    self.parent().progressBar.setToolTip(info)
                except Exception as ex:
                    logging.error("progressError", exc_info=ex)

        self.cycle_time = deque([time.time()], maxlen=1000)

        scan_move_recursive(
            len(self.scan_axes),
            move_to_target,
            readData,
            after_last_loop_func,
            range_list=self.scan_ranges,
        )

        print(self.activeDataset.dtype)
        del self.activeDataset
        self.running = False


class fastScan3DThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    running = False

    def __init__(self, parent):
        super(fastScan3DThread, self).__init__(parent)
        self.fname = fname = self.parent().expData_filePath.text()
        if ".exdir" in self.fname:
            pass
        else:
            self.fname = self.fname.split(".exdir")[0] + ".exdir"

    def __isRunning(f):
        def wrapper(*args, **kw):
            r = None
            try:
                self = args[0]
                self.running = True
                r = f(*args, **kw)
                self.running = False
                self.finishedSignal.emit(time.time())
            except Exception as ex:
                logging.error("fastScan3D:ERR", exc_info=ex)
                self.finishedSignal.emit(time.time())
                self.running = False
            return r

        return wrapper

    @__isRunning
    def run(self):

        z_start = float(self.parent().ui.scan3D_config.item(0, 1).text())
        z_end = float(self.parent().ui.scan3D_config.item(0, 2).text())
        z_step = float(self.parent().ui.scan3D_config.item(0, 3).text())

        if z_start == z_end:
            z_end += z_step
        if z_start > z_end:
            z_step *= -1

        if z_step == 0:
            Range_z = np.array([z_start] * 100)
        else:
            Range_z = np.arange(z_start, z_end, z_step)

        Range_zi = np.arange(len(Range_z))
        y_start = float(self.parent().ui.scan3D_config.item(1, 1).text())
        y_end = float(self.parent().ui.scan3D_config.item(1, 2).text())
        y_step = float(self.parent().ui.scan3D_config.item(1, 3).text())
        if y_start == y_end:
            y_end += y_step
        if y_start > y_end:
            y_step *= -1

        Range_y = np.arange(y_start, y_end, y_step)
        Range_yi = np.arange(len(Range_y))

        x_start = float(self.parent().ui.scan3D_config.item(2, 1).text())
        x_end = float(self.parent().ui.scan3D_config.item(2, 2).text())
        x_step = float(self.parent().ui.scan3D_config.item(2, 3).text())
        if x_start == x_end:
            x_end += x_step
        if x_start > x_end:
            x_step *= -1

        Range_x = np.arange(x_start, x_end, x_step)
        Range_xi = np.arange(len(Range_x))

        data_pmtA = np.zeros((len(Range_z), len(Range_x), len(Range_y)))
        # data_pmtA.fill(np.nan)
        data_pmtB = np.zeros((len(Range_z), len(Range_x), len(Range_y)))

        def stop_callback():
            print("External readout stop_callback")

        scanParam = self.parent().piStage.initGridScanXY(
            rangeX=(x_start, x_end),
            rangeY=(y_start, y_end),
            scan_axis=0,
            N=len(Range_y),
            rate=2,
        )
        print(scanParam)
        waveTable_dt = scanParam["waveTable_dt"]

        self.averageNpulses = self.parent().ui.fastScan3D_averageNpulses.value()
        self.PicoScope_config = {}
        self.PicoScope_config["n_captures"] = 2 ** 15
        self.PicoScope_config["frame_duration"] = waveTable_dt
        self.PicoScope_config["sampleInterval"] = 2e-9
        self.PicoScope_config["pulseFreq"] = float(
            self.parent().ui.pulseFreq.text()
        )  # Hz
        self.PicoScope_config["samplingDuration"] = (
            1 / self.PicoScope_config["pulseFreq"] * self.averageNpulses
        )
        # import pdb; pdb.set_trace()
        self.PicoScope_config = self.parent().PicoScope_setConfig(self.PicoScope_config)
        print(self.PicoScope_config)

        PicoScope_dataA = np.zeros(
            (self.PicoScope_config["n_captures"], self.PicoScope_config["noSamples"]),
            dtype=np.int16,
        )
        PicoScope_dataB = np.zeros(
            (self.PicoScope_config["n_captures"], self.PicoScope_config["noSamples"]),
            dtype=np.int16,
        )
        # self.PicoScope_read_done = True

        self.parent().piStage.gridScanXY(stop_callback=stop_callback, timeout=10)

        if self.parent().ui.fastScan3D_trigSource.currentText() == "Software":
            t0 = time.time()
            self.parent().piStage.softStartEvent.set()
            self.parent().PicoScope.runBlock(pretrig=self.PicoScope_config["pretrig"])
            self.parent().PicoScope.waitReady()
            t1 = time.time()

        print("wait gridScanXY_done")
        while not self.parent().piStage.scanParam["gridScanXY_done"]:
            time.sleep(0.1)
            # print('wait gridScanXY_done',len(self.parent().piStage.pi_device.path['1']))

        last_gridScanXY = self.parent().piStage.scanParam["last_gridScanXY"]

        self.parent().PicoScope.getDataRawBulk(channel="A", data=PicoScope_dataA)
        self.parent().PicoScope.getDataRawBulk(channel="B", data=PicoScope_dataB)
        # self.parent().PicoScope_setConfig()

        dataA_p2p = PicoScope_dataA.max(axis=1) - PicoScope_dataA.min(axis=1)
        dataB_p2p = PicoScope_dataB.max(axis=1) - PicoScope_dataB.min(axis=1)
        # import pdb; pdb.set_trace()

        signals = np.array([self.parent().PicoScope_dataT, dataA_p2p, dataB_p2p]).T
        print(signals, signals.shape)
        dataA_ = np.hstack(self.parent().PicoScope.rawToV("A", PicoScope_dataA))
        dataB_ = np.hstack(self.parent().PicoScope.rawToV("B", PicoScope_dataB))
        dataT_ = self.parent().PicoScope_time_grid
        # self.parent().dataReadySignal.emit({'name':'line_pico_ChA','time':time.time(),'data':[dataT_,dataA_]})
        # self.parent().dataReadySignal.emit({'name':'line_pico_ChB','time':time.time(),'data':[dataT_,dataB_]})

        # import pdb; pdb.set_trace()
        X, Y, Z_A, Z_B, pathSignal = process_gridScanXY(
            signals,
            last_gridScanXY["data"],
            x_step=x_step,
            y_step=y_step,
            read_start_delay=0,
        )
        # x_step = X[0,1]-X[0,0]
        # y_step = Y[1,0]-Y[0,0]
        # import pdb; pdb.set_trace()
        dataA = np.array([Z_A.T])
        dataB = np.array([Z_B.T])

        self.dataReadySignal.emit(
            {
                "name": "img",
                "time": time.time(),
                "data": [dataA, (X.min(), Y.min()), (x_step, y_step), np.array([1]), 0],
            }
        )
        self.dataReadySignal.emit(
            {
                "name": "img1",
                "time": time.time(),
                "data": [dataB, (X.min(), Y.min()), (x_step, y_step), np.array([1]), 0],
            }
        )
        # import pdb; pdb.set_trace()
        self.dataReadySignal.emit(
            {
                "name": "line_pmtA",
                "time": time.time(),
                "data": [pathSignal[:, 3], pathSignal[:, 4]],
            }
        )
        self.dataReadySignal.emit(
            {
                "name": "line_pmtB",
                "time": time.time(),
                "data": [scanParam["path"][:, 0], scanParam["path"][:, 1]],
            }
        )
