import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir
from pathlib import Path
import time
import traceback
import threading
from scipy.interpolate import interp1d
import numpy.lib.recfunctions as rfn
import pint
import datetime
from collections import deque

sys.path.append(str(Path("../utils")))

from .optimize_position import optimize_position
from .scriptUtils import scan_move_recursive, isRunning, process_progress_info


from utils.units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES

ureg = pint.UnitRegistry()
Q_ = ureg.Quantity


params = [
    {
        "name": "Additional parameters",
        "type": "group",
        "children": [
            {"name": "Save all metadata", "type": "bool", "value": True},
            {"name": "Save raw", "type": "bool", "value": True},
            # {'name': 'Grid optimize', 'type': 'bool', 'value': False},
            {"name": "Zero delay", "type": "bool", "value": False},
            {"name": "Intensity calibr", "type": "bool", "value": False},
            {"name": "Shift compensation", "type": "bool", "value": False},
            {"name": "Polarization control", "type": "bool", "value": True},
            {
                "name": "Optimize on anchor",
                "type": "group",
                "children": [
                    {
                        "name": "By",
                        "type": "list",
                        "value": "AndorCamera/data/2w1",
                        "values": [],
                    },
                    {"name": "Axes", "type": "str", "value": "X,Y,Z"},
                    {"name": "Relative", "type": "bool", "value": True},
                    {"name": "Bounds", "type": "str", "value": "-2,2;-2,2;-3,3"},
                    {"name": "Average", "type": "int", "value": 1},
                    {"name": "Maxiter", "type": "int", "value": 20},
                    {"name": "Tolerance", "type": "float", "value": 0.1},
                    {"name": "Exposure", "type": "float", "value": -1},
                    {"name": "Save raw", "type": "bool", "value": False},
                    {
                        "name": "Shutter",
                        "type": "list",
                        "value": "None",
                        "values": ["None", "Close TUN", "Close FIX", "Close both"],
                    },
                ],
            },
        ],
    },
    {
        "name": "MetaAxes",
        "type": "group",
        "children": [
            {
                "name": "Axis0",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "LaserWavelength",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1, "decimals": 4},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                ],
            },
            {
                "name": "Axis1",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "Delay_line_position",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                ],
            },
            {
                "name": "Axis2",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "CenterIndex",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                ],
            },
            {
                "name": "Axis3",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "CenterIndex",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                ],
            },
        ],
    },
]


class MultiScanThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    running = False
    store = None
    fname = "tmp"
    header = ["index", "axis", "A", "B", "C"]

    def __init__(self, parent=None):
        super(MultiScanThread, self).__init__(parent)

        self.fname = Path(self.parent().expData_filePath.text())
        if ".exdir" in str(self.fname):
            pass
        else:
            self.fname = str(self.fname).split(".exdir")[0] + ".exdir"
        self.store = exdir.File(self.fname)
        self.start_time = datetime.datetime.now()
        self.cycle_time = deque([time.time()], maxlen=1000)

    def stop(self):
        self.running = False

    def intensity_calibr_protocol(self):
        if not "tmp" in self.store:
            self.store.create_group("tmp")

        ex_wl = self.parent().ui.laserWavelength.value()
        self.store["tmp"].require_group(f"{ex_wl}")

        t0 = time.time()

        info, current = self.parent().MultiScan_getInfo()
        target_fix = np.array(
            [info[current]["X"], info[current]["Y"], info[current]["Z"]]
        )

        if (
            self.params.child("Additional parameters")
            .child("Polarization control")
            .value()
        ):
            if not np.isnan(info[current]["Polarization"]):
                self.parent().HWP_Polarization_moveTo(
                    info[current]["Polarization"], wait=True
                )

        # source = self.params.child('Additional parameters').child('Optimize by').value()
        # source, _, chan = source.split('/')
        source = "AndorCamera"
        bounds = (
            self.params.child("Additional parameters")
            .child("Optimize on anchor")
            .child("Bounds")
            .value()
        )
        tol = (
            self.params.child("Additional parameters")
            .child("Optimize on anchor")
            .child("Tolerance")
            .value()
        )
        maxiter = (
            self.params.child("Additional parameters")
            .child("Optimize on anchor")
            .child("Maxiter")
            .value()
        )

        self.parent().laserSetShutter(True, wait=True)
        self.parent().laserSetIRShutter(False, wait=True)
        self.parent().piStage_moveTo_corrected(axis=[0, 1, 2], target=target_fix)
        pos, rec = optimize_position(
            self.parent(),
            axis="XYZ",
            source=source,
            chan="2w2",
            optimize_bounds_str=bounds,
            xtol=tol,
            maxiter=maxiter,
        )
        lines = self.parent().getUnifiedData(save_raw=True, save_metadata=True)
        target_2w2_intensity = lines["data"][source]["data"]["2w2"]

        self.parent().laserSetIRShutter(True, wait=True)
        lines1 = self.parent().getUnifiedData(save_raw=True, save_metadata=True)
        lines = np.hstack((lines, lines1))
        self.store["tmp"][f"{ex_wl}"].create_dataset(f"TUN{t0}", data=lines)

        self.parent().laserSetShutter(False, wait=True)

        self.parent().piStage_moveTo(axis=[0, 1, 2], target=target_fix)
        pos, rec = optimize_position(
            self.parent(),
            axis="XYZ",
            source=source,
            chan="2w1",
            radius=radius,
            xtol=tol,
            maxiter=maxiter,
        )

        lines = self.parent().getUnifiedData(save_raw=True, save_metadata=True)
        self.store["tmp"][f"{ex_wl}"].create_dataset(f"FIX{t0}", data=lines)

        self.parent().laserSetShutter(True, wait=True)
        self.parent().laserSetIRShutter(False, wait=True)
        pos, rec = optimize_position(
            self.parent(),
            axis="HWP_TUN_angle",
            source=source,
            chan="2w2",
            radius=20,
            xtol=0.1,
            reference_val=target_2w2_intensity,
            maxiter=maxiter,
        )
        self.store["tmp"][f"{ex_wl}"].create_dataset(f"TUN_rec{t0}", data=rec)

        lines0 = self.parent().getUnifiedData(save_raw=True, save_metadata=True)

        self.parent().laserSetShutter(False, wait=True)
        self.parent().laserSetIRShutter(True, wait=True)
        lines1 = self.parent().getUnifiedData(save_raw=True, save_metadata=True)

        self.parent().laserSetShutter(True, wait=True)
        self.parent().laserSetIRShutter(True, wait=True)
        lines2 = self.parent().getUnifiedData(save_raw=True, save_metadata=True)

        lines = np.hstack([lines0, lines1, lines2])
        self.store["tmp"][f"{ex_wl}"].create_dataset(f"TUN_optim{t0}", data=lines)

    def center_preparation_protocol(self):
        info, current = self.parent().MultiScan_getInfo()
        if self.params.child("Additional parameters").child("Zero delay").value():
            self.parent().abstractMoveTo(
                0, axis="Delay_line_position_zero_relative", wait=True
            )
        if current >= len(info):
            return
        pos = np.array(
            [info[current]["X"], info[current]["Y"], info[current]["Z"]]
        )  # + self.delta
        # if info[current]['Type']!=b'Anchor':
        # 	pos +=self.delta
        pos += self.delta

        if (
            self.params.child("Additional parameters")
            .child("Shift compensation")
            .value()
        ):
            self.parent().piStage_moveTo_corrected(axis=[0, 1, 2], target=pos)
        else:
            self.parent().piStage.move([0, 1, 2], pos)
        if (
            self.params.child("Additional parameters")
            .child("Polarization control")
            .value()
        ):
            if not np.isnan(info[current]["Polarization"]):
                self.parent().HWP_Polarization_moveTo(
                    info[current]["Polarization"], wait=True
                )

        info, current = self.parent().MultiScan_getInfo()
        if info[current]["Type"] == b"Anchor":

            axes = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Axes")
                .value()
                .split(",")
            )
            bounds = []
            source = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("By")
                .value()
            )
            source, _, chan = source.split("/")
            exposure = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Exposure")
                .value()
            )

            _shutter = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Shutter")
                .value()
            )
            shutter = (None, None)
            if _shutter == "None":
                shutter = (None, None)
            elif _shutter == "Close TUN":
                shutter = (False, True)
            elif _shutter == "Close FIX":
                shutter = (True, False)
            elif _shutter == "Close both":
                shutter = (False, False)

            if not shutter[0] is None and not shutter[1] is None:
                self.parent().laserSetShutter(shutter[0], wait=True)
                self.parent().laserSetIRShutter(shutter[1], wait=True)

            if source == "AndorCamera" and exposure > 0:
                init_exposure = self.parent().andorCameraGetExposure()
                self.parent().andorCameraSetExposure(exposure)

            relative_bounds = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Relative")
                .value()
            )
            maxiter = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Maxiter")
                .value()
            )
            tol = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Tolerance")
                .value()
            )
            averageN = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Average")
                .value()
            )
            maximize = self.parent().ui.optimize_maximize.isChecked()
            optim_save_raw = (
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Save raw")
                .value()
            )

            for i, bounds_ in enumerate(
                self.params.child("Additional parameters")
                .child("Optimize on anchor")
                .child("Bounds")
                .value()
                .split(";")
            ):
                b = [float(bb) for bb in bounds_.split(",")]
                if relative_bounds:
                    b = [pos[i] + b[0], pos[i] + b[1]]
                bounds.append(b)

            def _isRunning():
                return self.running

            pos, log = optimize_position(
                axes=axes,
                x0=pos,
                bounds=bounds,
                source=source,
                chan=chan,
                maxiter=maxiter,
                averageN=averageN,
                maximize=maximize,
                moveTo=self.parent().abstractMoveTo,
                readData=self.parent().getUnifiedData,
                readPosition=self.parent().abstractGetPosition,
                dataReadySignal=self.dataReadySignal,
                xatol=tol,
                _isRunning=_isRunning,
            )

            if not shutter[0] is None and not shutter[1] is None:
                self.parent().laserSetShutter(True, wait=True)
                self.parent().laserSetIRShutter(True, wait=True)

            if source == "AndorCamera" and exposure > 0:
                self.parent().andorCameraSetExposure(init_exposure)

            # self.delta = np.zeros(3)
            # pos, rec = optimize_position(self.parent(),axis='XYZ',source=source,chan=chan, radius=radius, xtol=tol,maxiter=maxiter)

            if (
                self.params.child("Additional parameters")
                .child("Shift compensation")
                .value()
            ):
                shift = self.parent().estimate_piStageWaistShift_shift()
                if chan == "w1+w2":
                    shift /= 2
                print(pos, shift, self.delta)
                self.delta_tmp.append(
                    pos - shift - np.array(info[current][["X", "Y", "Z"]].tolist())
                )
            else:
                self.delta_tmp.append(
                    pos - np.array(info[current][["X", "Y", "Z"]].tolist())
                )
                print(pos, self.delta)

            self.delta = np.mean(self.delta_tmp, axis=0)
            print("delta_after", self.delta)
            self.parent().ui.MultiScan_delta.setText(
                ",".join([str(i) for i in self.delta])
            )

    @isRunning
    def run(self):

        self.delta = np.zeros(
            3
        )  # ([float(i) for i in self.parent().ui.MultiScan_delta.text().split(',')])
        self.delta = np.array(
            [float(i) for i in self.parent().ui.MultiScan_delta.text().split(",")]
        )
        self.delta_tmp = []
        self.parent().ui.MultiScan_delta.setText(",".join([str(i) for i in self.delta]))
        params = self.parent().MultiScan_params
        self.params = params
        if not "MultiScan" in self.store:
            self.storeActiveGroup = self.store.create_group("MultiScan").create_group(
                "data" + str(time.time())
            )
        else:
            self.storeActiveGroup = self.store["MultiScan"].create_group(
                "data" + str(time.time())
            )

        self.source = self.parent().readoutSources.currentText()

        # self.header = ['time',]+[child['Axis'] for child in params.childs] + ['A','B','C']
        # self.storeActiveGroup.attrs['readoutSource'] = self.source
        # self.storeActiveGroup.attrs['header'] = self.header

        self.save_all_metadata = (
            params.child("Additional parameters").child("Save all metadata").value()
        )
        self.save_raw = params.child("Additional parameters").child("Save raw").value()

        # TODO:
        self.scan_axes = []
        self.meta_axes = []
        self.scan_ranges = []

        for i in range(len(params.child("MetaAxes").childs)):
            child = params.child("MetaAxes").childs[i]
            if child["Active"]:
                if child["ScanByGrid"]:
                    if ";" in child["Grid"]:
                        tmp = child["Grid"].split(";")
                        scan_range_tmp = []
                        for t in tmp:
                            start, stop, step = [float(i) for i in t.split(",")]

                            scan_range_tmp.append(np.arange(start, stop, step))
                            # scan_range_tmp.append([stop])
                        print(scan_range_tmp)
                        scan_range_tmp = np.hstack(scan_range_tmp)
                    else:
                        scan_range_tmp = np.array(
                            [float(i) for i in child["Grid"].split(",")]
                        )

                else:
                    scan_range_tmp = np.arange(
                        child["Start"], child["End"], child["Step"]
                    )
                    scan_range_tmp = np.append(scan_range_tmp, child["End"])
                self.scan_axes.append(child["Axis"])
                self.scan_ranges.append(scan_range_tmp)
                self.meta_axes.append(child["Axis"])

        meta_axes_N_scans = int(
            np.prod([len(self.scan_ranges[i]) for i in range(len(self.scan_ranges))])
        )

        positions_dtype = []
        for ax in self.scan_axes:
            positions_dtype.append((ax, AXES_UNITS_DTYPES[ax][1]))

        for ax in self.scan_axes:
            pos_axis = self.parent().abstractGetPosition(axis=ax)

        lines = self.parent().getUnifiedData(
            save_raw=self.save_raw,
            save_metadata=self.save_all_metadata,
            # axes_positions=['X','Y','Z','X_target','Y_target','Z_target','Delay_line_position'],
            parameters=["laserBultInPower"],
        )

        print(lines.dtype["data"])
        print(lines.dtype["metadata"])

        t = [
            ("time", "f8"),
            ("position", positions_dtype),
            ("data", lines.dtype["data"]),
            ("metadata", lines.dtype["metadata"]),
            ("iteration", "i4", len(self.scan_axes)),
        ]
        print(t)
        dtype = np.dtype(t)

        self.first_index = 0

        self.activeDataset = self.storeActiveGroup.create_dataset(
            "data", shape=(meta_axes_N_scans + 1,), dtype=dtype
        )
        N = meta_axes_N_scans

        print(f"progress={N}")

        self.progress = iter(np.linspace(0, 100, N, dtype=int).tolist())
        self.total_steps = N
        # self.activeDataset[0] = lines

        self.header = str(dtype)
        self.activeDataset.attrs["axes_units_dtypes"] = AXES_UNITS_DTYPES
        self.activeDataset.attrs["sources_units_dtypes"] = AXES_UNITS_DTYPES

        self.activeDataset.attrs["readoutSource"] = self.source
        self.activeDataset.attrs["header"] = self.header
        self.activeDataset.attrs[
            "filters"
        ] = self.parent().filtersPiezoStage_Dict  # .to_dict()
        info, current = self.parent().MultiScan_getInfo()
        # print({k:info[k] for k in info.dtype.names})
        self.activeDataset.attrs["centerIndex_info"] = {
            k: info[k].tolist() for k in info.dtype.names
        }

        metadata = self.parent().abstractGetMetadata()
        metadata_ = {}
        for k in metadata.dtype.names:
            if hasattr(metadata[k], "dtype"):
                if metadata[k].ndim == 0:
                    metadata_[k] = metadata[k].reshape(1)[0].tolist()
                else:
                    metadata_[k] = metadata[k].tolist()
            elif type(metadata[k]) in (float, str, int, list):
                metadata_[k] = metadata[k]

            else:
                metadata_[k] = str(metadata[k])
            print("!!!!!!!", k, metadata_[k], type(metadata_[k]))
            self.activeDataset.attrs[k] = metadata_[k]

        self.lastUpdate = time.time()
        self._write_to_row = self.first_index
        self._prev_axis_index = 0
        self._preview_start = 0
        self._scan_index = 0

        def move_to_target(target, axis_index):
            if self.running:
                if (
                    self._prev_axis_index != axis_index
                    and self._prev_axis_index == len(self.scan_axes) - 1
                ):
                    self._preview_start = self._write_to_row
                    self._scan_index += 1
                    if self._scan_index > 15:
                        self._scan_index = 0

                self._prev_axis_index = axis_index
                ax = self.scan_axes[axis_index]
                info, current = self.parent().MultiScan_getInfo()
                if ax == "CenterIndex":
                    self.parent().abstractMoveTo(target, axis=ax, wait=True)

                    if (
                        self.params.child("Additional parameters")
                        .child("Intensity calibr")
                        .value()
                    ):
                        self.intensity_calibr_protocol()
                    else:
                        self.center_preparation_protocol()

                else:
                    self.parent().abstractMoveTo(target, axis=ax, wait=True)

                for ax in self.scan_axes:
                    pos = self.parent().abstractGetPosition(axis=ax)
                    self.activeDataset["position"][self._write_to_row][ax] = pos

            return self.running

        def after_last_loop_func(iter_list_, _iter_list_counter, iter_index):
            # if len(self.delta_tmp)>0:
            # self.delta = np.zeros(3)#np.mean(self.delta_tmp, axis=0)

            self.delta_tmp = []
            self.parent().ui.MultiScan_delta.setText(
                ",".join([str(i) for i in self.delta])
            )
            self._preview_start = self._write_to_row

        self._write_to_row = 0

        def readData(iter_list_, _iter_list_counter):
            if self.running:
                print(
                    "iter_list_:",
                    iter_list_,
                    _iter_list_counter,
                    "row",
                    self._write_to_row,
                )
                iter_list = iter_list_[:-1]
                preview_start = self._preview_start
                iter_index = self._scan_index

                lines = self.parent().getUnifiedData(
                    save_raw=self.save_raw,
                    save_metadata=self.save_all_metadata,
                    # axes_positions=['X','Y','Z','X_target','Y_target','Z_target'],
                    parameters=["laserBultInPower"],
                )

                self.activeDataset["time"][self._write_to_row] = time.time()
                self.activeDataset["data"][self._write_to_row] = lines["data"]
                # print(lines.dtype)
                self.activeDataset["metadata"][self._write_to_row] = lines["metadata"]
                self.activeDataset["iteration"][self._write_to_row] = np.array(
                    _iter_list_counter
                )

                lin_scan = self.activeDataset["data"][
                    self._preview_start : self._write_to_row
                ]
                x = self.activeDataset["position"][
                    self._preview_start : self._write_to_row
                ][self.scan_axes[-1]]

                if time.time() - self.lastUpdate > 0.3:
                    w = x != 0
                    items = []
                    # print('a'*100)
                    for source in lin_scan.dtype.names:
                        for name in lin_scan[source]["data"].dtype.names:
                            items.append(
                                {
                                    "type": "scan",
                                    "name": f"{source}_{name}",
                                    "info": f"{iter_list}",
                                    #'index': i,
                                    "time": time.time(),
                                    "data": [x[w], lin_scan[source]["data"][name][w]],
                                }
                            )
                        if "raw" in lines["data"][source].dtype.names and self.save_raw:
                            raw_x_name = lines["data"][source]["raw"].dtype.names[0]
                            for name in lines["data"][source]["raw"].dtype.names[1:-1]:
                                items.append(
                                    {
                                        "type": "raw",
                                        "name": f"{source}_{name}",
                                        "info": "",
                                        #'index': i,
                                        "time": time.time(),
                                        "data": [
                                            lines["data"][source]["raw"][raw_x_name][0],
                                            lines["data"][source]["raw"][name][0],
                                        ],
                                    }
                                )

                    self.dataReadySignal.emit(items)

                    self.lastUpdate = time.time()

                self._write_to_row += 1

                if self.parent().ui.actionPause.isChecked():
                    while self.parent().ui.actionPause.isChecked():
                        time.sleep(0.1)

                try:
                    progress = next(self.progress)
                    info = process_progress_info(
                        progress, self.total_steps, self.start_time, self.cycle_time
                    )
                    self.progressSignal.emit(int(round(progress)))
                    self.parent().progressBar.setToolTip(info)
                    print(info)
                except Exception as ex:
                    logging.error("progressError", exc_info=ex)

        self.cycle_time = deque([time.time()], maxlen=1000)

        scan_move_recursive(
            len(self.scan_axes),
            move_to_target,
            readData,
            after_last_loop_func,
            range_list=self.scan_ranges,
        )

        # print(self.activeDataset.dtype)
        del self.activeDataset
        self.running = False
