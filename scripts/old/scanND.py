import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir
from pathlib import Path
import time
import traceback
import threading
from scipy.interpolate import interp1d
import numpy.lib.recfunctions as rfn
import pint
from collections import deque
import datetime

sys.path.append(str(Path("../utils")))

from .scriptUtils import scan_move_recursive, isRunning, process_progress_info

import importlib

from utils.units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES

ureg = pint.UnitRegistry()
Q_ = ureg.Quantity

params = [
    {
        "name": "ScanAxes",
        "type": "group",
        "children": [
            {
                "name": "Axis0",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "X",
                    },
                    {"name": "Active", "type": "bool", "value": True},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1, "decimals": 4},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                    # {'name': 'Run with previous', 'type': 'bool', 'value': False},
                ],
            },
            {
                "name": "Axis1",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "Y",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                    # {'name': 'Run with previous', 'type': 'bool', 'value': False},
                ],
            },
            {
                "name": "Axis2",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "Z",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                    # {'name': 'Run with previous', 'type': 'bool', 'value': False},
                ],
            },
            {
                "name": "Axis3",
                "type": "group",
                "children": [
                    {
                        "name": "Axis",
                        "type": "list",
                        "values": list(AXES_UNITS_DTYPES.keys()),
                        "value": "Z",
                    },
                    {"name": "Active", "type": "bool", "value": False},
                    {"name": "Start", "type": "float", "value": 0, "decimals": 4},
                    {"name": "End", "type": "float", "value": 100, "decimals": 4},
                    {"name": "Step", "type": "float", "value": 1},
                    {"name": "ScanByGrid", "type": "bool", "value": False},
                    {"name": "Grid", "type": "str", "value": "0,50,100"},
                    {"name": "Wait", "type": "float", "value": 0},
                    # {'name': 'Run with previous', 'type': 'bool', 'value': False},
                ],
            },
        ],
    },
    {
        "name": "Additional parameters",
        "type": "group",
        "children": [
            {"name": "Save all metadata", "type": "bool", "value": True},
        ],
    },
]


class scanNDThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    running = False
    store = None
    fname = "tmp"
    header = ["index", "axis", "A", "B", "C"]

    def __init__(self, parent=None):
        super(scanNDThread, self).__init__(parent)

        self.fname = Path(self.parent().expData_filePath.text())
        if ".exdir" in str(self.fname):
            pass
        else:
            self.fname = str(self.fname).split(".exdir")[0] + ".exdir"
        self.store = exdir.File(self.fname)
        self.start_time = datetime.datetime.now()
        self.cycle_time = deque([time.time()], maxlen=1000)

    def stop(self):
        self.running = False

    @isRunning
    def run(self):

        params = self.parent().scanND_params

        if not "scanND" in self.store:
            self.storeActiveGroup = self.store.create_group("scanND").create_group(
                "data" + str(time.time())
            )
        else:
            self.storeActiveGroup = self.store["scanND"].create_group(
                "data" + str(time.time())
            )
        self.source = self.parent().readoutSources.currentText()

        # self.header = ['time',]+[child['Axis'] for child in params.childs] + ['A','B','C']
        # self.storeActiveGroup.attrs['readoutSource'] = self.source
        # self.storeActiveGroup.attrs['header'] = self.header
        self.save_all_metadata = (
            params.child("Additional parameters").child("Save all metadata").value()
            == True
        )

        self.scan_axes = []
        self.scan_ranges = []
        self.scan_limits = []
        self.scan_wait = []

        self.params = params

        N_rows = 1
        for i in range(len(params.child("ScanAxes").childs)):
            child = params.child("ScanAxes").childs[i]
            print(
                child,
                child["Active"],
                child["ScanByGrid"],
                child["Start"],
                child["End"],
                child["Step"],
            )
            if child["Active"]:

                if child["ScanByGrid"]:
                    if ";" in child["Grid"]:
                        tmp = child["Grid"].split(";")
                        scan_range_tmp = []
                        for t in tmp:
                            start, stop, step = [float(i) for i in t.split(",")]

                            scan_range_tmp.append(np.arange(start, stop, step))
                            # scan_range_tmp.append([stop])
                        print(scan_range_tmp)
                        scan_range_tmp = np.hstack(scan_range_tmp)
                    else:
                        scan_range_tmp = np.array(
                            [float(i) for i in child["Grid"].split(",")]
                        )

                else:
                    scan_range_tmp = np.arange(
                        child["Start"], child["End"], child["Step"]
                    )
                    scan_range_tmp = np.append(scan_range_tmp, child["End"])

                limits = (scan_range_tmp.min(), scan_range_tmp.max())
                if child["Axis"] == "Laser_precompensation":
                    limits = self.parent().laserClient.precompensationLimits()
                    mask = (scan_range_tmp >= limits[0]) & (scan_range_tmp <= limits[1])
                    scan_range_tmp = scan_range_tmp[mask]
                self.scan_limits.append(limits)

                print(scan_range_tmp)
                self.scan_ranges.append(scan_range_tmp)
                N_rows *= len(scan_range_tmp)

                self.scan_axes.append(child["Axis"])
                self.scan_wait.append(child["Wait"])

        lines = self.parent().getUnifiedData(
            save_metadata=self.save_all_metadata, axes_positions_skip=self.scan_axes
        )
        pos_axis_val = []
        pos_axis_dtype = []

        for ax in self.scan_axes:
            pos_axis_val.append(self.parent().abstractGetPosition(axis=ax))
            pos_axis_dtype.append((ax, AXES_UNITS_DTYPES[ax][1]))

        dtype = [
            ("time", "f8"),
            ("position", pos_axis_dtype),
            ("data", lines.dtype["data"]),
            ("metadata", lines.dtype["metadata"]),
            ("iteration", "i4", len(self.scan_axes)),
        ]
        print(dtype)
        dtype = np.dtype(dtype)

        # lines = rfn.append_fields(
        # 			lines, 'position',np.array([tuple(pos_axis_val)],dtype=pos_axis_dtype),usemask=False)
        # lines = rfn.append_fields(
        # 				lines, 'iteration',np.array([tuple([-1]*len(self.scan_axes))],dtype='i4'),usemask=False)
        # print(self.scan_ranges)
        # self.running = False
        # return

        first_index = 0

        if self.source == "AndorCamera":
            lines["data"]["AndorCamera"]["raw"][
                "intensity"
            ] = self.parent().andorCCDBaseline["intensity"]
            lines["data"]["AndorCamera"]["raw"][
                "wavelength"
            ] = self.parent().andorCCDBaseline["wavelength"]
            lines["data"]["AndorCamera"]["raw"][
                "exposure"
            ] = self.parent().andorCCDBaseline["exposure"]
            first_index = 1
        else:
            pass
        self.first_index = first_index
        self.activeDataset = self.storeActiveGroup.create_dataset(
            "data", shape=(N_rows + first_index,), dtype=dtype
        )

        self.total_steps = len(self.activeDataset)
        self.progress = iter(np.linspace(0, 100, self.total_steps).tolist())

        self.activeDataset["time"][0] = time.time()
        self.activeDataset["data"][0] = lines["data"]
        self.activeDataset["metadata"][0] = lines["metadata"]
        print(self.activeDataset["position"][0], pos_axis_val)
        for i in range(len(self.scan_axes)):
            self.activeDataset["position"][i][0] = pos_axis_val[i]
        self.activeDataset["iteration"][0] = np.array([-1] * len(self.scan_axes))

        self.header = str(lines.dtype)
        self.activeDataset.attrs["axes_units_dtypes"] = AXES_UNITS_DTYPES
        self.activeDataset.attrs["sources_units_dtypes"] = AXES_UNITS_DTYPES

        self.activeDataset.attrs["readoutSource"] = self.source
        self.activeDataset.attrs["header"] = self.header
        self.activeDataset.attrs[
            "filters"
        ] = self.parent().filtersPiezoStage_Dict  # .to_dict()

        metadata = self.parent().abstractGetMetadata()
        metadata_ = {}
        for k in metadata.dtype.names:
            if hasattr(metadata[k], "dtype"):
                if metadata[k].ndim == 0:
                    metadata_[k] = metadata[k].reshape(1)[0].tolist()
                else:
                    metadata_[k] = metadata[k].tolist()
            elif type(metadata[k]) in (float, str, int, list):
                metadata_[k] = metadata[k]

            else:
                metadata_[k] = str(metadata[k])
            print("!!!!!!!", k, metadata_[k], type(metadata_[k]))
            self.activeDataset.attrs[k] = metadata_[k]

        self.lastUpdate = time.time()
        self._write_to_row = first_index
        self._prev_axis_index = 0
        self._preview_start = 0
        self._scan_index = 0

        def move_to_target(target, axis_index):
            if self.running:
                if (
                    self._prev_axis_index != axis_index
                    and self._prev_axis_index == len(self.scan_axes) - 1
                ):
                    self._preview_start = self._write_to_row
                    self._scan_index += 1
                    if self._scan_index > 15:
                        self._scan_index = 0

                self._prev_axis_index = axis_index

                if (
                    target >= self.scan_limits[axis_index][0]
                    or target <= self.scan_limits[axis_index][1]
                ):
                    if self.scan_axes[axis_index] == "HWP_Polarization":
                        self.parent().abstractMoveTo(
                            target, axis=self.scan_axes[axis_index], wait=False
                        )
                    else:
                        self.parent().abstractMoveTo(
                            target, axis=self.scan_axes[axis_index], wait=True
                        )

                    time.sleep(self.scan_wait[axis_index])

                    ####################### tmp ####################
                    # TODO: comutation
                    # if self.scan_axes[axis_index] == 'Knife_position':
                    # 	self.parent().shamrockSetWavelength()
            return self.running

        def after_last_loop_func(iter_list_, _iter_list_counter, iter_index):
            child = params.child("ScanAxes").childs[len(self.scan_ranges) - 1]

            if child["ScanByGrid"]:
                if ";" in child["Grid"]:
                    tmp = child["Grid"].split(";")
                    scan_range_tmp = []
                    for t in tmp:
                        start, stop, step = [float(i) for i in t.split(",")]

                        scan_range_tmp.append(np.arange(start, stop, step))
                        # scan_range_tmp.append([stop])
                    # print(scan_range_tmp)
                    scan_range_tmp = np.hstack(scan_range_tmp)
                else:
                    scan_range_tmp = np.array(
                        [float(i) for i in child["Grid"].split(",")]
                    )
            else:
                scan_range_tmp = np.arange(child["Start"], child["End"], child["Step"])
                scan_range_tmp = np.append(scan_range_tmp, child["End"])

            self.scan_ranges[-1] = scan_range_tmp

        def readData(iter_list_, _iter_list_counter):
            if self.running:
                print("iter_list_:", iter_list_, _iter_list_counter)
                iter_list = iter_list_[:-1]
                preview_start = self._preview_start
                iter_index = self._scan_index
                lines = self.parent().getUnifiedData(
                    save_metadata=self.save_all_metadata,
                    axes_positions_skip=self.scan_axes,
                )

                if len(self.activeDataset) == self._write_to_row:
                    prev_name = self.activeDataset.object_name
                    l = len(self.activeDataset)
                    prev_dataset = self.activeDataset
                    new_len = np.prod([len(i) for i in self.scan_ranges])

                    self.activeDataset = self.storeActiveGroup.create_dataset(
                        prev_name + "_resized",
                        shape=(new_len,),
                        dtype=self.activeDataset.dtype,
                    )
                    self.activeDataset.data[:l] = prev_dataset.data[:]

                self.activeDataset[list(lines.dtype.names)][self._write_to_row] = lines

                self.activeDataset["iteration"][self._write_to_row] = np.array(
                    _iter_list_counter
                )

                for i in range(len(self.scan_axes)):
                    self.activeDataset["position"][self.scan_axes[i]][
                        self._write_to_row
                    ] = self.parent().abstractGetPosition(axis=self.scan_axes[i])

                x = self.activeDataset["position"][self.scan_axes[-1]][
                    preview_start : self._write_to_row
                ]
                items = []
                # print('a'*100)
                data_slice = slice(preview_start, self._write_to_row)
                for source in self.activeDataset["data"].dtype.names:
                    for name in self.activeDataset["data"][source]["data"].dtype.names:
                        items.append(
                            {
                                "type": "scan",
                                "name": f"{source}_{name}",
                                "info": f"{iter_list}",
                                #'index': i,
                                "time": time.time(),
                                "data": [
                                    x,
                                    self.activeDataset["data"][source]["data"][name][
                                        data_slice
                                    ],
                                ],
                            }
                        )
                    if "raw" in self.activeDataset["data"][source].dtype.names:
                        raw_x_name = self.activeDataset["data"][source][
                            "raw"
                        ].dtype.names[0]
                        for name in self.activeDataset["data"][source][
                            "raw"
                        ].dtype.names[1:-1]:
                            items.append(
                                {
                                    "type": "raw",
                                    "name": f"{source}_{name}",
                                    "info": f"{iter_list}",
                                    #'index': i,
                                    "time": time.time(),
                                    "data": [
                                        self.activeDataset["data"][source]["raw"][
                                            raw_x_name
                                        ][self._write_to_row],
                                        self.activeDataset["data"][source]["raw"][name][
                                            self._write_to_row
                                        ],
                                    ],
                                }
                            )

                self.dataReadySignal.emit(items)
                # print('b'*100)
                self._write_to_row += 1

                if self.parent().ui.actionPause.isChecked():
                    while self.parent().ui.actionPause.isChecked():
                        time.sleep(0.1)

                try:
                    progress = next(self.progress)
                    info = process_progress_info(
                        progress, self.total_steps, self.start_time, self.cycle_time
                    )
                    self.progressSignal.emit(int(round(progress)))
                    self.parent().progressBar.setToolTip(info)
                except Exception as ex:
                    logging.error("progressError", exc_info=ex)

        def readData_onFly(iter_list_):
            # self.parent().ui.scan1D_onFly.isChecked():
            print(iter_list_)
            iter_list = iter_list_[:-1]

            self._write_to_row = self._preview_start
            preview_start = self._preview_start
            iter_index = self._scan_index

            lines = self.parent().getUnifiedData(
                save_metadata=self.save_all_metadata, axes_positions_skip=self.scan_axes
            )
            self.activeDataset[list(lines.dtype.names)][self._write_to_row - 1] = lines

            for i in range(len(self.scan_axes)):
                self.activeDataset[self.scan_axes[i]][
                    self._write_to_row - 1
                ] = self.parent().abstractGetPosition(axis=self.scan_axes[i])[0]

            self.averageNpulses = 3
            self.PicoScope_config = self.parent().PicoScope_setConfig()
            self.PicoScope_config["n_captures"] = len(self.scan_ranges[-1])
            vel = 10  # mm/s
            self.PicoScope_config["frame_duration"] = (
                abs(self.scan_ranges[-1][1] - self.scan_ranges[-1][0]) / vel
            )
            self.PicoScope_config["sampleInterval"] = 2e-9
            self.PicoScope_config["pulseFreq"] = float(
                self.parent().ui.pulseFreq.text()
            )  # Hz
            self.PicoScope_config["samplingDuration"] = (
                1 / self.PicoScope_config["pulseFreq"] * self.averageNpulses
            )
            # import pdb; pdb.set_trace()
            self.PicoScope_config = self.parent().PicoScope_setConfig(
                self.PicoScope_config
            )
            if not self.PicoScope_config["ready_to_read"]:
                logging.error(
                    f"Picoscope is not ready to read with this config:{self.PicoScope_config}"
                )
                return
            print(self.PicoScope_config)
            PicoScope_dataA = np.zeros(
                (
                    self.PicoScope_config["n_captures"],
                    self.PicoScope_config["noSamples"],
                ),
                dtype=np.int16,
            )
            PicoScope_dataB = np.zeros(
                (
                    self.PicoScope_config["n_captures"],
                    self.PicoScope_config["noSamples"],
                ),
                dtype=np.int16,
            )
            move_to_target(
                self.scan_ranges[-1][0], axis_index=len(self.scan_ranges) - 1
            )

            time.sleep(3)
            print("START")
            pos_time = np.array(
                [time.time(), self.parent().moco.axes["delay"].position]
            )
            with self.parent().PicoScope_lock:
                self.parent().PicoScope.runBlock(
                    pretrig=self.PicoScope_config["pretrig"]
                )
                pos_time = np.vstack(
                    [
                        pos_time,
                        np.array(
                            [time.time(), self.parent().moco.axes["delay"].position]
                        ),
                    ]
                )
                self.parent().moco.axes["delay"].move_to(self.scan_ranges[-1][-1])
                # time.sleep(2)
                while self.parent().moco.axes["delay"].is_moving():
                    pos_time = np.vstack(
                        [
                            pos_time,
                            np.array(
                                [time.time(), self.parent().moco.axes["delay"].position]
                            ),
                        ]
                    )
                print("ON TARGET", pos_time[-1, 0] - pos_time[0, 0])
                self.parent().PicoScope.waitReady()
            pos_time = np.vstack(
                [
                    pos_time,
                    np.array([time.time(), self.parent().moco.axes["delay"].position]),
                ]
            )
            print("READOUT DONE", pos_time[-1, 0] - pos_time[0, 0])

            self.parent().PicoScope.getDataRawBulk(channel="A", data=PicoScope_dataA)
            self.parent().PicoScope.getDataRawBulk(channel="B", data=PicoScope_dataB)
            # self.parent().PicoScope_setConfig()

            PicoScope_dataA_ = self.parent().PicoScope.rawToV("A", PicoScope_dataA)
            PicoScope_dataB_ = self.parent().PicoScope.rawToV("B", PicoScope_dataB)

            PicoScope_dataA = np.array_split(
                PicoScope_dataA_,
                self.parent().PicoScope_config["pulses_in_frame"],
                axis=1,
            )
            PicoScope_dataA = np.array(
                [np.ptp(sub, axis=1) for sub in PicoScope_dataA]
            ).T

            PicoScope_dataB = np.array_split(
                PicoScope_dataB_,
                self.parent().PicoScope_config["pulses_in_frame"],
                axis=1,
            )
            PicoScope_dataB = np.array(
                [np.ptp(sub, axis=1) for sub in PicoScope_dataB]
            ).T

            dataA_p2p = PicoScope_dataA.mean(
                axis=1
            )  # dataA_.max(axis=1) - dataA_.min(axis=1)
            dataB_p2p = PicoScope_dataB.mean(
                axis=1
            )  # dataB_.max(axis=1) - dataB_.min(axis=1)

            dataT_ = self.parent().PicoScope_dataT + pos_time[0, 0]
            pos_interp = interp1d(
                pos_time[:, 0],
                pos_time[:, 1],
                bounds_error=False,
                fill_value="extrapolate",
            )

            print(len(self.scan_ranges[-1]), len(dataA_p2p), len(dataB_p2p))
            x = pos_interp(dataT_)
            # print(x,dataA_p2p,dataB_p2p)
            self.parent().dataReadySignal.emit(
                [
                    {"name": "line_pmtA", "time": time.time(), "data": [x, dataA_p2p]},
                    {"name": "line_pmtB", "time": time.time(), "data": [x, dataB_p2p]},
                ]
            )

            print("DONE", time.time() - pos_time[0, 0])

            self.activeDataset[self.scan_axes[-1]][
                preview_start : preview_start + len(x)
            ] = x
            self.activeDataset["PicoScope"]["ChA"][
                preview_start : preview_start + len(x)
            ] = dataA_p2p
            self.activeDataset["PicoScope"]["ChB"][
                preview_start : preview_start + len(x)
            ] = dataB_p2p
            self._write_to_row = preview_start + len(x)

            self.dataReadySignal.emit(
                [
                    {
                        "name": f"PicoScope_ChA{iter_list}",
                        "index": iter_index * 2,
                        "time": time.time(),
                        "data": [
                            self.activeDataset[self.scan_axes[-1]][
                                preview_start : self._write_to_row
                            ],
                            self.activeDataset["PicoScope"]["ChA"][
                                preview_start : self._write_to_row
                            ],
                        ],
                    },
                    {
                        "name": f"PicoScope_ChB{iter_list}",
                        "index": iter_index * 2 + 1,
                        "time": time.time(),
                        "data": [
                            self.activeDataset[self.scan_axes[-1]][
                                preview_start : self._write_to_row
                            ],
                            self.activeDataset["PicoScope"]["ChB"][
                                preview_start : self._write_to_row
                            ],
                        ],
                    },
                ]
            )
            self._write_to_row += 1
            self._preview_start = self._write_to_row

        self.cycle_time = deque([time.time()], maxlen=1000)

        if self.parent().ui.scan1D_onFly.isChecked():
            ranges = self.scan_ranges.copy()
            ranges[-1] = [self.scan_ranges[-1][0]]
            scan_move_recursive(
                len(self.scan_axes),
                range_list=ranges,
                move_function=move_to_target,
                readout_function=readData_onFly,
                after_last_loop_func=after_last_loop_func,
            )
        else:
            scan_move_recursive(
                len(self.scan_axes),
                move_to_target,
                readData,
                after_last_loop_func,
                range_list=self.scan_ranges,
            )

        print(self.activeDataset.dtype)
        del self.activeDataset
        self.running = False
