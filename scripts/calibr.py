import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir
from pathlib import Path
import time
import queue

sys.path.append(str(Path("../utils")))

from .scriptUtils import scan_move_recursive, isRunning

import numpy.lib.recfunctions as rfn


class calibrThread(QtCore.QThread):
    dataReadySignal = QtCore.Signal(object)
    finishedSignal = QtCore.Signal(object)
    running = False

    def __init__(self, parent):
        super(calibrThread, self).__init__(parent)

    def stop(self):
        self.running = False

    @isRunning
    def run(self):

        t0 = time.time()

        i = 0
        source = self.parent().readoutSources.currentText()

        data = self.parent().getUnifiedData(save_raw=False)

        lines_ = np.zeros(1000, dtype=data.dtype)
        lines_[0] = data
        if source == "PicoScope":
            conf = self.parent().PicoScope_setConfig()
            if conf is None:
                return

        names = lines_.dtype.names
        i = 0
        updatePeriod = 1/20  # s
        lastUpdateTime = 0
        while self.running:
            if time.time() - lastUpdateTime > updatePeriod:
                t0_ = time.time()
                data = self.parent().getUnifiedData()
                # print('dt=',time.time()-t0_)
                mask = lines_["time"] != 0
                index = (mask).sum()
                #print(index)
                dt = time.time() - t0_
                print("dt0=", dt)
                if index == len(lines_):
                    lines_[:-1] = lines_[1:]
                    if "ThorCam" in source:
                        lines_[-1] = rfn.drop_fields(data, "img")
                    else:
                        lines_[-1] = rfn.drop_fields(data, "raw")
                else:
                    if "ThorCam" in source:
                        lines_[index] = rfn.drop_fields(data, "img")
                    else:
                        lines_[index] = rfn.drop_fields(data, "raw")

                lines = lines_[mask]
                names = lines["data"].dtype.names
                _lines = []
                x = lines["time"] - t0
                # for name in names:
                # print(lines[name])
                items = []
                # print('dt=',time.time()-t0_)
                dt = time.time() - t0_
                #print("dt0=", dt)
                for source in lines["data"].dtype.names:
                    for name in lines["data"][source]["data"].dtype.names:
                        items.append(
                            {
                                "type": "Scan",
                                "name": f"{source}_{name}",
                                "info": "calibr",  # f'{iter_list}',
                                #'index': i,
                                "time": time.time(),
                                "data": (x, lines["data"][source]["data"][name]),
                            }
                        )
                    if "raw" in data["data"][source].dtype.names:
                        raw_x_name = data["data"][source]["raw"].dtype.names[0]
                        for name in data["data"][source]["raw"].dtype.names[1:-1]:
                            if name in ["type", "VRange", "time"]: continue
                            items.append(
                                {
                                    "type": "raw",
                                    "name": f"{source}_{name}",
                                    "info": "calibr",  # f'{iter_list}',
                                    #'index': i,
                                    "time": time.time(),
                                    "data": (
                                        data["data"][source]["raw"][raw_x_name][0],
                                        data["data"][source]["raw"][name][0],
                                    ),
                                }
                            )
                # print('dt=',time.time()-t0_)
                self.dataReadySignal.emit(items)
                # print(x,lines['data']['Powermeter']['data']['power'])
                dt = time.time() - t0_
                #print("dt=", dt)
                lastUpdateTime = time.time()

                if self.parent().ui.actionPause.isChecked():
                    while self.parent().ui.actionPause.isChecked() and self.running:
                        QtCore.QThread.msleep(100)
                        #time.sleep(0.1)
            else:
                #time.sleep(0.01)
                QtCore.QThread.msleep(100)
        t1 = time.time()
