import sys
import numpy as np
import exdir
from scipy.interpolate import interp1d, CloughTocher2DInterpolator, LinearNDInterpolator
from scipy import integrate
from pathlib import Path
import pint
import time
import datetime
import pandas as pd
import shutil

sys.path.append(str(Path("../utils")))
try:
    from utils.utils import setExdirData
except:
    from utils import setExdirData
ureg = pint.UnitRegistry()
Q_ = ureg.Quantity

# 0.88 for sech2-shaped pulses https://www.rp-photonics.com/peak_power.html
PULSE_SHAPE_CORRECTION_FACTOR = 0.88


def n_UVFS(wavelength):
    wl = wavelength / 1000  # nm -> um
    n = np.sqrt(
        1
        + 0.6961663 * wl ** 2 / (wl ** 2 - 0.0684043 ** 2)
        + 0.4079426 * wl ** 2 / (wl ** 2 - 0.1162414 ** 2)
        + 0.8974794 * wl ** 2 / (wl ** 2 - 9.896161 ** 2)
    )
    return n


def calcShiftDelay(T, ang, n):
    """
    Params:
    T - plate thickness
    ang - angle of incidence [deg]
    n - refractive index
    Output:
    delay_delta - induced delay
    T_ - beam path in plate
    d - beam displacement
    """
    theta = np.deg2rad(ang)
    theta_ = np.arcsin(np.sin(theta) / n)
    T_ = T / np.cos(theta_)
    in_out_distance = T_ * np.cos(theta - theta_)
    # print(in_out_distance)
    delay_delta = in_out_distance - T_ * n
    d = T * np.sin(theta) * (1 - np.cos(theta) / np.sqrt(n ** 2 - np.sin(theta) ** 2))
    return np.array([delay_delta, T_, d])


def calcSpotSize(z, f, w_in, wavelength=1064e-6, M2=1.2, return_allInfo=False):
    """
    z - distance from lens [mm]
    f - focal length [mm]
    w_in - input beam radius, 1/e**2 [mm]
    wavelength - excitation wavelength [mm]
    M2 - beam quality factor. Should be 1 for perfect beam,
        but we are living in real world so the maximal value from documentation
        for laser is used.
    output: beam spot radius at z, [mm]
    """

    w_ = lambda w0, z, z0, f: np.sqrt(w0 ** 2 * (1 + ((z - f) / z0) ** 2))
    w0_ = lambda f, wavelength, w_in: f * wavelength / np.pi / w_in
    k_ = lambda wavelength: 2 * np.pi / wavelength
    z0_ = lambda k, w0: k * w0 ** 2 / M2 / 2  # Rayleigh distance or Rayleigh range z_R
    # np.pi*w0**2/out.params['M2'].value/(ex_wl*1e-6)

    k = k_(wavelength)
    w0 = w0_(f, wavelength, w_in)
    z0 = z0_(k, w0)
    w = w_(w0, z, z0, f)
    if return_allInfo:
        return w, w0, z0
    else:
        return w


def latestTimeStamp(group, before=None):
    """
    before='20210323-114108'
    before='20210323-1141'
    before='20210323'
    """
    if len(group) == 0:
        root_path = group.root_directory
        path = group.relative_path
        timestamp = time.strftime("data%Y%m%d-%H%M%S")
        with exdir.File(root_path) as store:
            new_timestamp = store[path].create_dataset(timestamp,data=np.zeros(1))
            del new_timestamp

    if not before is None:
        stamps = list(group)
        stamps = [si for si in stamps if si[: len(before)] <= before]
        timestamp = max(stamps)
    else:
        timestamp = max(group)
    return group[timestamp]


class CalibrToolkit(object):
    # interp = CloughTocher2DInterpolator(list(zip(x, y)), z)
    filters_transmittance = {}
    andorCamera_sensitivity = {}

    def __init__(
        self, storePath="configStore.exdir", setup="HRS", config=None, before=None
    ):

        self.store = exdir.File(storePath, "r")
        self.setup = setup
        if config is None:
            active = self.store[f"{setup}/Laser/BeamFocusing"].attrs["Active"]
            config = self.store[f"{setup}/Laser/BeamFocusing"][active].attrs.to_dict()
            config["focusing_by"] = active
        self.config = config
        self.getLatestBefore = before
        self._initIR_laser_params_interp()
        self._init_laser_params_interp()
        self._init_delayLineCalibr()
        self._init_laserBeamShift_delay_delta()
        self._init_laserPrecompensation()

        if setup == "Microscope":
            self._init_piStageWaistShift()
            self._init_HWP_Polarization_power_correction()
            self._init_HWP_Polarization_laserBeamShift_correction()

        self.andorCamera_sensitivity = {}
        for readout_conf in self.store[f"{self.setup}/AndorCamera/Sensitivity"]:
            self.andorCamera_sensitivity[readout_conf] = {}
            for grating in self.store[
                f"{self.setup}/AndorCamera/Sensitivity/{readout_conf}"
            ]:
                self.andorCamera_sensitivity[readout_conf][grating] = {}
                dset = latestTimeStamp(
                    self.store[
                        f"{self.setup}/AndorCamera/Sensitivity/{readout_conf}/{grating}"
                    ],
                    before=self.getLatestBefore,
                )
                self.andorCamera_sensitivity[readout_conf][grating]["None"] = interp1d(
                    dset["wavelength"],
                    dset["Sensitivity"],
                    bounds_error=False,
                    fill_value=np.nan,
                )
                for filter_name in self.store[f"{self.setup}/filtersPiezoStage/Filters"]:
                    dset = latestTimeStamp(
                        self.store[f"{self.setup}/filtersPiezoStage/Filters/{filter_name}"],
                        before=self.getLatestBefore,
                    )
                    T = dset["transmittance"].copy()
                    wl = dset["wavelength"].copy()
                    T[T < 1e-11] = np.nan

                    isnan = np.isnan(T)
                    #print(isnan.sum(), len(T))
                    if (~isnan).sum() == 0:
                        continue
                    sens = interp1d(
                        wl[~isnan],
                        T[~isnan]
                        * self.andorCamera_sensitivity[readout_conf][grating]["None"](
                            wl[~isnan]
                        ),
                        bounds_error=False,
                        fill_value=np.nan,
                    )
                    self.andorCamera_sensitivity[readout_conf][grating][filter_name] = sens
                    #print(self.andorCamera_sensitivity)

    def _init_laserPrecompensation(self):
        name = f"{self.setup}/Laser/Precompensation"
        name = str(
            latestTimeStamp(self.store[name], before=self.getLatestBefore).relative_path
        )

        self.laserPrecompensation = interp1d(
            self.store[name]["Ex_wl"],
            self.store[name]["precompensation"],
            bounds_error=False,
            fill_value=np.nan,
        )
        self.laserPrecompensation_min = interp1d(
            self.store[name]["Ex_wl"],
            self.store[name]["min"],
            bounds_error=False,
            fill_value=np.nan,
        )
        self.laserPrecompensation_max = interp1d(
            self.store[name]["Ex_wl"],
            self.store[name]["max"],
            bounds_error=False,
            fill_value=np.nan,
        )
        self.laserPrecompensation_dset_name = name

    def remove_from_laserPrecompensation_calibr(self, ex_wl):

        calibr = self.store[self.laserPrecompensation_dset_name].data.copy()
        dist = abs(calibr["Ex_wl"]-ex_wl)

        calibr = calibr[dist>1]
        print(calibr)
        # np.save(self.ZeroDelay_position_calibr.data_filename, calibr)

        setExdirData(self.store[self.laserPrecompensation_dset_name], calibr)

        self._init_laserPrecompensation()
        return calibr

    def insert_laserPrecompensation_calibr(self, ex_wl, value):
        calibr = np.array(self.store[self.laserPrecompensation_dset_name])
        if (calibr["Ex_wl"] == ex_wl).sum() > 0:
            index = calibr["Ex_wl"] == ex_wl
            calibr["precompensation"][index] = value

        else:
            calibr.resize(len(calibr) + 1, refcheck=False)
            calibr[-1]["Ex_wl"] = ex_wl
            calibr[-1]["precompensation"] = value
            calibr[-1]["min"] = self.laserPrecompensation_min(ex_wl)
            calibr[-1]["max"] = self.laserPrecompensation_max(ex_wl)


        calibr = calibr[calibr["Ex_wl"].argsort()[::-1]]
        print(calibr)
        # np.save(self.ZeroDelay_position_calibr.data_filename, calibr)

        setExdirData(self.store[self.laserPrecompensation_dset_name], calibr)

        self._init_laserPrecompensation()
        return calibr

    def _initIR_laser_params_interp(self):
        # import pdb; pdb.set_trace()
        calibr = latestTimeStamp(
            self.store[f"{self.setup}/HWP_FIX"], before=self.getLatestBefore
        ).data.copy()
        self.power2angle_interpLimits_IR = latestTimeStamp(
            self.store[f"{self.setup}/HWP_FIX"], before=self.getLatestBefore
        ).attrs["power2angle_interpLimits"]
        w = (calibr["HWP_angle"] >= self.power2angle_interpLimits_IR[0]) & (
            calibr["HWP_angle"] <= self.power2angle_interpLimits_IR[1]
        )
        #print("_initIR_laser_params_interp:", w.sum())
        if len(np.unique(calibr["Ex_wavelength"])) == 1:
            self._power2angle_IR_interp_ = interp1d(
                calibr[w]["power"],
                calibr[w]["HWP_angle"],
                bounds_error=False,
                fill_value=np.nan,
            )
            self._angle2power_IR_interp_ = interp1d(
                calibr["HWP_angle"],
                calibr["power"],
                bounds_error=False,
                fill_value=np.nan,
            )
            self._power2angle_IR_interp = (
                lambda ex_wl, power: self._power2angle_IR_interp_(power)
            )
            self._angle2power_IR_interp = (
                lambda ex_wl, angle: self._angle2power_IR_interp_(angle)
            )
        else:

            self._angle2power_IR_interp = LinearNDInterpolator(
                # CloughTocher2DInterpolator(
                np.vstack([calibr["Ex_wavelength"], calibr["HWP_angle"]]).T,
                calibr["power"],
            )
            self._power2angle_IR_interp = LinearNDInterpolator(
                # CloughTocher2DInterpolator(
                np.vstack([calibr[w]["Ex_wavelength"], calibr[w]["power"]]).T,
                calibr[w]["HWP_angle"],
            )

        name = self.config["focusing_by"]
        self.beam_size_info_ir = latestTimeStamp(
            self.store[f"{self.setup}/Laser/BeamFocusing/{name}/FIX"],
            before=self.getLatestBefore,
        ).data.copy()
        self.beam_size_info_ir_w0_in = (
            latestTimeStamp(
                self.store[f"{self.setup}/Laser/BeamFocusing/{name}/FIX"],
                before=self.getLatestBefore,
            )
            .data["w0_in"]
            .mean()
        )
        self.beam_size_info_ir_w0 = (
            latestTimeStamp(
                self.store[f"{self.setup}/Laser/BeamFocusing/{name}/FIX"],
                before=self.getLatestBefore,
            )
            .data["w0"]
            .mean()
        )

        self.pulsewidth_ir = latestTimeStamp(
            self.store[f"{self.setup}/Laser/pulsewidth"], before=self.getLatestBefore
        ).attrs["pulsewidth_IR"]

        return self._power2angle_IR_interp, self._angle2power_IR_interp

    def _init_laser_params_interp(self):
        calibr = latestTimeStamp(
            self.store[f"{self.setup}/HWP_TUN"], before=self.getLatestBefore
        ).data.copy()
        self.power2angle_interpLimits = latestTimeStamp(
            self.store[f"{self.setup}/HWP_TUN"], before=self.getLatestBefore
        ).attrs["power2angle_interpLimits"]
        w = (calibr["HWP_angle"] >= self.power2angle_interpLimits[0]) & (
            calibr["HWP_angle"] <= self.power2angle_interpLimits[1]
        )

        self._angle2power_interp = LinearNDInterpolator(
            # CloughTocher2DInterpolator(
            np.vstack([calibr["Ex_wavelength"], calibr["HWP_angle"]]).T,
            calibr["power"],
        )
        self._power2angle_interp = LinearNDInterpolator(
            # CloughTocher2DInterpolator(
            np.vstack([calibr[w]["Ex_wavelength"], calibr[w]["power"]]).T,
            calibr[w]["HWP_angle"],
        )
        name = self.config["focusing_by"]
        self.beam_size_info = latestTimeStamp(
            self.store[f"{self.setup}/Laser/BeamFocusing/{name}/TUN"],
            before=self.getLatestBefore,
        ).data.copy()
        self.beam_size_info_w0_in = interp1d(
            self.beam_size_info["ex_wl"],
            self.beam_size_info["w0_in"],
            bounds_error=False,
            fill_value="extrapolate",
        )
        self.beam_size_info_w0 = interp1d(
            self.beam_size_info["ex_wl"],
            self.beam_size_info["w0"],
            bounds_error=False,
            fill_value="extrapolate",
        )
        #print(self.beam_size_info,self.beam_size_info_w0(np.linspace(700,1300,10)))
        self.pulsewidth = latestTimeStamp(
            self.store[f"{self.setup}/Laser/pulsewidth"], before=self.getLatestBefore
        ).data.copy()
        self.ex_wl2pulsewidth = interp1d(
            self.pulsewidth["Ex_wl"],
            self.pulsewidth["pulsewidth"],
            bounds_error=False,
            fill_value="extrapolate",
        )

        # self._ex_wl_beam_radius = interp1d()
        return self._power2angle_interp, self._angle2power_interp

    def _init_delayLineCalibr(self):

        # self.ZeroDelay_position_calibr = self.store[f'{self.setup}/DelayLine/ZeroDelayPosition']
        name = f"{self.setup}/DelayLine/ZeroDelayPosition"
        name = str(
            latestTimeStamp(self.store[name], before=self.getLatestBefore).relative_path
        )
        self.ZeroDelay_position_calibr_interp = interp1d(
            self.store[name]["ex_wl"],
            self.store[name]["delay_0_pos"],
            bounds_error=False,
            fill_value="extrapolate",
        )
        self.ZeroDelay_dset_name = name

    def _init_HWP_Polarization_power_correction(self):
        if not self.setup == "Microscope":
            return
        # self.ZeroDelay_position_calibr = self.store[f'{self.setup}/DelayLine/ZeroDelayPosition']
        name_FIX = f"{self.setup}/HWP_Polarization/FIX_correctionFactor"
        name_FIX = str(
            latestTimeStamp(
                self.store[name_FIX], before=self.getLatestBefore
            ).relative_path
        )

        name_TUN = f"{self.setup}/HWP_Polarization/TUN_correctionFactor"
        name_TUN = str(
            latestTimeStamp(
                self.store[name_TUN], before=self.getLatestBefore
            ).relative_path
        )

        self.HWP_Polarization_power_correction_interp_FIX = interp1d(
            self.store[name_FIX]["HWP_Polarization"][0],
            self.store[name_FIX]["correctionFactor"][0],
            bounds_error=False,
            fill_value="extrapolate",
        )
        self.HWP_Polarization_power_correction_dset_FIX = name_FIX

        ex_wl = np.hstack(
            [
                np.repeat(t["ex_wl"], len(t["HWP_Polarization"]))
                for t in self.store[name_TUN]
            ]
        )

        self.HWP_Polarization_power_correction_interp_TUN = LinearNDInterpolator(
            np.vstack([ex_wl, self.store[name_TUN]["HWP_Polarization"].flatten()]).T,
            self.store[name_TUN]["correctionFactor"].flatten(),
        )
        self.HWP_Polarization_power_correction_dset_TUN = name_TUN

    def estimate_HWP_Polarization_correctionFactor(
        self, HWP_Polarization_angle, wavelength
    ):
        correctionFactor_FIX = self.HWP_Polarization_power_correction_interp_FIX(
            HWP_Polarization_angle
        )
        correctionFactor_TUN = self.HWP_Polarization_power_correction_interp_TUN(
            wavelength, HWP_Polarization_angle
        )
        return correctionFactor_TUN, correctionFactor_FIX

    def _init_HWP_Polarization_laserBeamShift_correction(self):
        if not self.setup == "Microscope":
            return
        # self.ZeroDelay_position_calibr = self.store[f'{self.setup}/DelayLine/ZeroDelayPosition']
        name = f"{self.setup}/HWP_Polarization/BeamShift"
        name = str(
            latestTimeStamp(self.store[name], before=self.getLatestBefore).relative_path
        )

        self.HWP_Polarization_laserBeamShift_X_interp = interp1d(
            self.store[name]["HWP_angle"],
            self.store[name]["shift_X"],
            bounds_error=False,
            fill_value="extrapolate",
        )

        self.HWP_Polarization_laserBeamShift_Y_interp = interp1d(
            self.store[name]["HWP_angle"],
            self.store[name]["shift_Y"],
            bounds_error=False,
            fill_value="extrapolate",
        )

        self.HWP_Polarization_laserBeamShift_X_diff_interp = interp1d(
            self.store[name]["HWP_angle"],
            self.store[name]["shift_X_diff"],
            bounds_error=False,
            fill_value="extrapolate",
        )

        self.HWP_Polarization_laserBeamShift_Y_diff_interp = interp1d(
            self.store[name]["HWP_angle"],
            self.store[name]["shift_Y_diff"],
            bounds_error=False,
            fill_value="extrapolate",
        )

        self.HWP_Polarization_laserBeamShift_correction_dset = name

    def estimate_HWP_Polarization_laserBeamShift_diff(self, HWP_Polarization_angle):
        shiftX_diff = self.HWP_Polarization_laserBeamShift_X_diff_interp(
            HWP_Polarization_angle
        )
        shiftY_diff = self.HWP_Polarization_laserBeamShift_Y_diff_interp(
            HWP_Polarization_angle
        )
        return shiftX_diff, shiftY_diff

    def estimate_HWP_Polarization_laserBeamShift(self, HWP_Polarization_angle):
        shiftX = self.HWP_Polarization_laserBeamShift_X_interp(HWP_Polarization_angle)
        shiftY = self.HWP_Polarization_laserBeamShift_Y_interp(HWP_Polarization_angle)
        return shiftX, shiftY

    def _init_laserBeamShift_delay_delta(self):
        ang = np.arange(0, 91, 1)
        ex_wl = np.arange(680, 1301, 1).reshape(1301 - 680, 1)
        Ang, Ex_wl = np.meshgrid(ang, ex_wl)
        delay_shift, T_, d = calcShiftDelay(5, ang, n_UVFS(ex_wl))  # 5 mm window
        interp = LinearNDInterpolator(
            np.transpose([d.flatten(), Ex_wl.flatten()]), Ang.flatten()
        )
        self.laserBeamShift_shift2angle = interp
        name = f"{self.setup}/Laser/BeamShift"
        name = str(
            latestTimeStamp(self.store[name], before=self.getLatestBefore).relative_path
        )
        self.laserBeamShift_V_interp = interp1d(
            self.store[name]["ex_wl"],
            self.store[name]["shift_V"],
            bounds_error=False,
            fill_value="extrapolate",
        )

        self.laserBeamShift_H_interp = interp1d(
            self.store[name]["ex_wl"],
            self.store[name]["shift_H"],
            bounds_error=False,
            fill_value="extrapolate",
        )
        self.laserBeamShift_dset_name = name

    def estimate_laserBeamShift_compensation(self, ex_wl):
        shift_V = self.laserBeamShift_V_interp(ex_wl)
        shift_H = self.laserBeamShift_H_interp(ex_wl)
        return shift_V, shift_H

    def insert_laserBeamShift_calibr(self, ex_wl, shift_V=None, shift_H=None):

        calibr = self.store[self.laserBeamShift_dset_name].data.copy()
        if (calibr["ex_wl"] == ex_wl).sum() > 0:
            index = calibr["ex_wl"] == ex_wl
            if not shift_V is None:
                calibr["shift_V"][index] = shift_V

            if not shift_H is None:
                calibr["shift_H"][index] = shift_H

        else:
            calibr.resize(len(calibr) + 1, refcheck=False)
            calibr[-1]["ex_wl"] = ex_wl

            if not shift_V is None:
                calibr[-1]["shift_V"] = shift_V

            if not shift_H is None:
                calibr[-1]["shift_H"] = shift_H

        calibr = calibr[calibr["ex_wl"].argsort()[::-1]]
        print(calibr)
        # np.save(self.ZeroDelay_position_calibr.data_filename, calibr)

        setExdirData(self.store[self.laserBeamShift_dset_name], calibr)

        self._init_laserBeamShift_delay_delta()
        return calibr

    def remove_from_laserBeamShift_calibr(self, ex_wl):

        calibr = self.store[self.laserBeamShift_dset_name].data.copy()
        dist = abs(calibr["ex_wl"]-ex_wl)

        calibr = calibr[dist>1]
        print(calibr)
        # np.save(self.ZeroDelay_position_calibr.data_filename, calibr)

        setExdirData(self.store[self.laserBeamShift_dset_name], calibr)

        self._init_laserBeamShift_delay_delta()
        return calibr

    def save_laserBeamShift_calibr(self, ex_wl, shift_V=None, shift_H=None):
        calibr = np.zeros(
            len(ex_wl), dtype=self.store[self.laserBeamShift_dset_name].dtype
        )
        calibr["ex_wl"] = ex_wl
        if not shift_V is None:
            calibr["shift_V"] = shift_V
        if not shift_H is None:
            calibr["shift_H"] = shift_H

        calibr = calibr[calibr["ex_wl"].argsort()[::-1]]
        setExdirData(self.store[self.laserBeamShift_dset_name], calibr)
        self._init_laserBeamShift_delay_delta()

    def reset_laserBeamShift_calibr(self):
        calibr = latestTimeStamp(
            self.store[f"{self.setup}/Laser/BeamShift_backup/"],
            before=self.getLatestBefore,
        ).data.copy()
        setExdirData(self.store[self.laserBeamShift_dset_name], calibr)
        self._init_laserBeamShift_delay_delta()

    def backup_laserBeamShift_calibr(self):
        timestamp = time.strftime("%Y%m%d-%H%M%S")
        calibr = self.store[self.laserBeamShift_dset_name].data.copy()
        name = f"{self.setup}/Laser/BeamShift"

        path = self.store[self.laserBeamShift_dset_name].root_directory
        with exdir.File(path) as store:
            store[name].create_dataset(timestamp, data=calibr)
            store[name+"_backup"].create_dataset(timestamp, data=calibr)
        self._init_laserBeamShift_delay_delta()

    def estimate_ZeroDelay_position(self, ex_wl):
        delay_zero_pos = self.ZeroDelay_position_calibr_interp(ex_wl)
        return delay_zero_pos

    def correct_ZeroDelay_calibr(self, ex_wl, true_pos):
        delta = true_pos - self.ZeroDelay_position_calibr_interp(ex_wl)
        calibr = np.array(self.store[self.ZeroDelay_dset_name])
        calibr["delay_0_pos"] += delta
        setExdirData(self.store[self.ZeroDelay_dset_name], calibr)
        self._init_delayLineCalibr()
        return calibr

    def insert_ZeroDelay_calibr(self, ex_wl, new_position):

        calibr = np.array(self.store[self.ZeroDelay_dset_name])
        if (calibr["ex_wl"] == ex_wl).sum() > 0:
            index = calibr["ex_wl"] == ex_wl
            calibr["delay_0_pos"][index] = new_position

        else:
            calibr.resize(len(calibr) + 1, refcheck=False)
            calibr[-1]["ex_wl"] = ex_wl
            calibr[-1]["delay_0_pos"] = new_position
        calibr = calibr[calibr["ex_wl"].argsort()[::-1]]
        print(calibr)
        # np.save(self.ZeroDelay_position_calibr.data_filename, calibr)

        setExdirData(self.store[self.ZeroDelay_dset_name], calibr)

        self._init_delayLineCalibr()
        return calibr

    def remove_from_ZeroDelay_calibr(self, ex_wl):

        calibr = np.array(self.store[self.ZeroDelay_dset_name])
        dist = abs(calibr["ex_wl"]-ex_wl)

        calibr = calibr[dist>1]
        print(calibr)
        # np.save(self.ZeroDelay_position_calibr.data_filename, calibr)

        setExdirData(self.store[self.ZeroDelay_dset_name], calibr)

        self._init_delayLineCalibr()
        return calibr


    def save_ZeroDelay_calibr(self, ex_wl, delay_0_pos):
        calibr = np.zeros(len(ex_wl), dtype=self.store[self.ZeroDelay_dset_name].dtype)
        calibr["ex_wl"] = ex_wl
        calibr["delay_0_pos"] = delay_0_pos
        calibr = calibr[calibr["ex_wl"].argsort()[::-1]]
        setExdirData(self.store[self.ZeroDelay_dset_name], calibr)
        self._init_delayLineCalibr()

    def reset_ZeroDelay_calibr(self):
        calibr = latestTimeStamp(
            self.store[f"{self.setup}/DelayLine/ZeroDelayPosition_backup"],
            before=self.getLatestBefore,
        ).data.copy()
        setExdirData(self.store[self.ZeroDelay_dset_name], calibr)
        self._init_delayLineCalibr()

    def backup_ZeroDelay_calibr(self):
        timestamp = time.strftime("%Y%m%d-%H%M%S")
        calibr = self.store[self.ZeroDelay_dset_name].data.copy()
        name = f"{self.setup}/DelayLine/ZeroDelayPosition"

        path = self.store[self.ZeroDelay_dset_name].root_directory
        with exdir.File(path) as store:
            store[name].create_dataset(timestamp, data=calibr)
            store[name+"_backup"].create_dataset(timestamp, data=calibr)
        self._init_laserBeamShift_delay_delta()
        # new_dset_name = self.ZeroDelay_dset_name.replace("ZeroDelayPosition", "ZeroDelayPosition_backup")
        # root_path = self.store.root_directory
        # shutil.copytree(Path(root_path)/self.ZeroDelay_dset_name, Path(root_path)/new_dset_name, dirs_exist_ok=True)

    # $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    def _init_piStageWaistShift(self):

        name = f"{self.setup}/piStage/waistShiftCompensation/TUN"
        name = str(
            latestTimeStamp(self.store[name], before=self.getLatestBefore).relative_path
        )
        xi = interp1d(
            self.store[name]["ex_wl"],
            self.store[name]["X"],
            bounds_error=False,
            fill_value=np.nan,
        )
        yi = interp1d(
            self.store[name]["ex_wl"],
            self.store[name]["Y"],
            bounds_error=False,
            fill_value=np.nan,
        )
        zi = interp1d(
            self.store[name]["ex_wl"],
            self.store[name]["Z"],
            bounds_error=False,
            fill_value=np.nan,
        )

        self.piStageWaistShift_interp = [xi, yi, zi]
        self.piStageWaistShift_dset_name = name

    def estimate_piStageWaistShift_shift(self, ex_wl):
        res = []
        for i in range(3):
            res.append(self.piStageWaistShift_interp[i](ex_wl))
        return np.array(res)

    def correct_piStageWaistShift_calibr(self, ex_wl, true_pos):
        pass
        # delta = true_pos - self.estimate_piStageWaistShift_shift(ex_wl)
        # calibr = np.array(self.store[self.piStageWaistShiftCompensation_dset_name])
        # calibr['delay_0_pos'] += delta
        # setExdirData(self.store[self.ZeroDelay_dset_name], calibr)
        # self._init_delayLineCalibr()
        # return calibr

    def insert_piStageWaistShift_calibr(self, ex_wl, new_position):
        pass
        # calibr = np.array(self.store[self.ZeroDelay_dset_name])
        # if (calibr['ex_wl']==ex_wl).sum()>0:
        # 	index = calibr['ex_wl']==ex_wl
        # 	calibr['delay_0_pos'][index] = new_position
        # else:
        # 	calibr.resize(len(calibr)+1,refcheck=False)
        # 	calibr[-1]['ex_wl'] = ex_wl
        # 	calibr[-1]['delay_0_pos'] = new_position

        # calibr = calibr[calibr['ex_wl'].argsort()[::-1]]
        # print(calibr)
        # #np.save(self.ZeroDelay_position_calibr.data_filename, calibr)

        # setExdirData(self.store[self.ZeroDelay_dset_name], calibr)

        # self._init_delayLineCalibr()
        # return calibr

    def save_piStageWaistShift_calibr(self, ex_wl, delay_0_pos):
        pass
        # calibr = np.zeros(len(ex_wl),dtype=self.store[self.ZeroDelay_dset_name].dtype)
        # calibr['ex_wl'] = ex_wl
        # calibr['delay_0_pos'] = delay_0_pos
        # calibr = calibr[calibr['ex_wl'].argsort()[::-1]]
        # setExdirData(self.store[self.ZeroDelay_dset_name], calibr)
        # self._init_delayLineCalibr()

    def reset_piStageWaistShift_calibr(self):
        pass
        # calibr = self.store[f'{self.setup}/DelayLine/ZeroDelayPosition_backup'].data
        # setExdirData(self.store[self.ZeroDelay_dset_name], calibr)
        # self._init_delayLineCalibr()

    ############################################################################
    #########################  intens  #########################################
    def calcBeam_params(self, Ex_wavelength, config=None, IR=False):
        if config is None:
            focusing_by = self.config["focusing_by"]
            config = self.store[
                f"{self.setup}/Laser/BeamFocusing/{focusing_by}"
            ].attrs.to_dict()
            # print(config)
        Ex_wavelength = Q_(Ex_wavelength, "nm")
        freq = Q_(80e6, "Hz")  # Hz
        if IR:
            pulse_dt = Q_(self.pulsewidth_ir, "s")  # s   #self.pulsewidth(1045)
            w_in = Q_(
                self.beam_size_info_ir_w0_in, "m"
            )  # self.beam_size_info_w0_in(Ex_wavelength)

        else:
            pulse_dt = Q_(self.ex_wl2pulsewidth(Ex_wavelength.m), "s")
            w_in = Q_(self.beam_size_info_w0_in(Ex_wavelength.m), "m")
        # print(w_in)
        duty_cycle = pulse_dt * freq / PULSE_SHAPE_CORRECTION_FACTOR
        duty_cycle = duty_cycle.to("dimensionless")

        if self.setup == "HRS":
            f = Q_(config["Focal length[mm]"], "mm")  # mm
            z = Q_(config["Waist-sample distance[mm]"], "mm") + f
            w, w0, z_R = calcSpotSize(
                z, f, w_in, Ex_wavelength.to("mm"), return_allInfo=True
            )
        else:
            NA = Q_(config["Numerical aperture"], "dimensionless")
            f = Q_(config["Focal length[mm]"], "mm")
            z = Q_(config["Waist-sample distance[mm]"], "mm") + f
            # w, w0, z_R = calcSpotSize(z,f,w_in,Ex_wavelength,return_allInfo=True)
            if IR:
                w0 = Q_(self.beam_size_info_ir_w0, "m").to(
                    "mm"
                )  # self.beam_size_info_w0_in(Ex_wavelength)
            else:
                w0 = Q_(self.beam_size_info_w0(Ex_wavelength.m), "m").to("mm")
            w = w0
            z_R = (2 * np.pi / Ex_wavelength * w0 ** 2 / 2).to("mm")
            # w = w_in
            # w0 = Ex_wavelength/np.pi/NA
            # z_R = 2*Ex_wavelength/np.pi/NA**2

        params = {
            "Ex_wavelength": Ex_wavelength,
            "f": f,
            "z": z,
            "freq": freq,
            "pulse_dt": pulse_dt,
            "duty_cycle": duty_cycle,
            "w_in": w_in,
            "w": w,
            "w0": w0,
            "z_R": z_R,
        }
        return (
            params  # Ex_wavelength, f, z, freq, pulse_dt, duty_cycle, w_in, w, w0, z_R
        )

    def power2angle_IR(self, power, Ex_wavelength=1045):
        # TODO: write some out of range warning
        angle = self._power2angle_IR_interp(Ex_wavelength, power)
        if angle.shape == ():
            return angle.reshape(1)[0]
        else:
            return angle

    def angle2power_IR(self, angle, Ex_wavelength=1045):
        # TODO: write some out of range shift
        power = self._angle2power_IR_interp(Ex_wavelength, angle)

        if power.shape == ():
            return power.reshape(1)[0]
        else:
            return power

    def power2angle(self, power, Ex_wavelength):
        # TODO: write some out of range warning
        angle = self._power2angle_interp(Ex_wavelength, power)
        if angle.shape == ():
            return angle.reshape(1)[0]
        else:
            return angle

    def angle2power(self, angle, Ex_wavelength):
        # TODO: write some out of range shift
        power = self._angle2power_interp(Ex_wavelength, angle)
        if power.shape == ():
            return power.reshape(1)[0]
        else:
            return power

    def intens2angle_IR(self, intens, Ex_wavelength=1045, config=None):
        """
        Convert peak laser intensity to angle of HWP
        """
        bp = self.calcBeam_params(Ex_wavelength, config=config, IR=True)
        bp["I_peak"] = Q_(intens, "W/cm**2")

        bp["I_avg"] = bp["I_peak"] * bp["duty_cycle"] #/ PULSE_SHAPE_CORRECTION_FACTOR
        bp["P_peak"] = (bp["I_peak"] * np.pi * bp["w"] ** 2 / 2).to("W")
        bp["P_avg"] = (bp["I_avg"] * np.pi * bp["w"] ** 2 / 2).to("W")
        bp["angle"] = Q_(
            self.power2angle_IR(bp["P_avg"].m, Ex_wavelength=Ex_wavelength), "deg"
        )
        bp["VPower_peak"] = self.Intens2VPower(bp["I_peak"], bp)
        bp["VPower_avg"] = self.Intens2VPower(bp["I_avg"], bp)
        return bp

    def angle2intens_IR(self, angle, Ex_wavelength=1045, config=None):
        bp = self.calcBeam_params(Ex_wavelength, config=config, IR=True)
        bp["P_avg"] = Q_(self.angle2power_IR(angle, Ex_wavelength=Ex_wavelength), "W")

        bp["P_peak"] = bp["P_avg"] / bp["duty_cycle"] #* PULSE_SHAPE_CORRECTION_FACTOR
        bp["angle"] = Q_(angle, "deg")
        bp["I_peak"] = 2 * bp["P_peak"] / np.pi / bp["w"] ** 2
        bp["I_avg"] = 2 * bp["P_avg"] / np.pi / bp["w"] ** 2
        bp["VPower_peak"] = self.Intens2VPower(bp["I_peak"], bp)
        bp["VPower_avg"] = self.Intens2VPower(bp["I_avg"], bp)
        return bp

    def intens2angle(self, intens, Ex_wavelength=1045, config=None):
        """
        Convert peak laser intensity to angle of HWP
        """
        bp = self.calcBeam_params(Ex_wavelength, config=config, IR=False)
        bp["I_peak"] = Q_(intens, "W/cm**2")

        bp["I_avg"] = bp["I_peak"] * bp["duty_cycle"] #/ PULSE_SHAPE_CORRECTION_FACTOR
        bp["P_peak"] = (bp["I_peak"] * np.pi * bp["w"] ** 2 / 2).to("W")
        bp["P_avg"] = (bp["I_avg"] * np.pi * bp["w"] ** 2 / 2).to("W")
        bp["angle"] = Q_(
            self.power2angle(bp["P_avg"].m, Ex_wavelength=Ex_wavelength), "deg"
        )
        bp["VPower_peak"] = self.Intens2VPower(bp["I_peak"], bp)
        bp["VPower_avg"] = self.Intens2VPower(bp["I_avg"], bp)
        return bp

    def angle2intens(self, angle, Ex_wavelength=1045, config=None):
        bp = self.calcBeam_params(Ex_wavelength, config=config, IR=False)
        bp["P_avg"] = Q_(self.angle2power(angle, Ex_wavelength=Ex_wavelength), "W")

        bp["P_peak"] = bp["P_avg"] / bp["duty_cycle"] #* PULSE_SHAPE_CORRECTION_FACTOR
        bp["angle"] = Q_(angle, "deg")
        bp["I_peak"] = 2 * bp["P_peak"] / np.pi / bp["w"] ** 2
        bp["I_avg"] = 2 * bp["P_avg"] / np.pi / bp["w"] ** 2
        bp["VPower_peak"] = self.Intens2VPower(bp["I_peak"], bp)
        bp["VPower_avg"] = self.Intens2VPower(bp["I_avg"], bp)

        return bp

    def Intens2VPower(self, intens, bp):
        return 3 / 4 * intens / bp["z_R"]

    def VPower2Intens(self, vpower, bp):
        return 4 / 3 * vpower * bp["z_R"]

    def vol_power2angle_IR(self, vpower, Ex_wavelength=1045, config=None, peak=False):
        """
        Convert volumetric power  to angle of HWP
        """
        bp = self.calcBeam_params(Ex_wavelength, config=config, IR=True)
        if peak:
            bp["VPower_peak"] = Q_(vpower, "W/cm**3")
            bp["VPower_avg"] = (
                bp["VPower_peak"] * bp["duty_cycle"] #/ PULSE_SHAPE_CORRECTION_FACTOR
            )
        else:
            bp["VPower_avg"] = Q_(vpower, "W/cm**3")
            bp["VPower_peak"] = (
                bp["VPower_avg"] / bp["duty_cycle"] #* PULSE_SHAPE_CORRECTION_FACTOR
            )

        bp["I_peak"] = self.VPower2Intens(bp["VPower_peak"], bp)
        bp["I_avg"] = self.VPower2Intens(bp["VPower_avg"], bp)
        bp["P_peak"] = (bp["I_peak"] * np.pi * bp["w"] ** 2 / 2).to("W")
        bp["P_avg"] = (bp["I_avg"] * np.pi * bp["w"] ** 2 / 2).to("W")
        bp["angle"] = Q_(
            self.power2angle_IR(bp["P_avg"].m, Ex_wavelength=Ex_wavelength), "deg"
        )

        return bp

    def angle2vol_power_IR(self, angle, Ex_wavelength=1045, config=None):
        # TODO: write some out of range shift
        bp = self.angle2intens_IR(angle, Ex_wavelength=Ex_wavelength, config=config)
        return bp

    def vol_power2angle(self, vpower, Ex_wavelength=1045, config=None, peak=False):
        """
        Convert peak laser intensity to angle of HWP
        """
        bp = self.calcBeam_params(Ex_wavelength, config=config, IR=False)
        if peak:
            bp["VPower_peak"] = Q_(vpower, "W/cm**3")
            bp["VPower_avg"] = (
                bp["VPower_peak"] * bp["duty_cycle"] #/ PULSE_SHAPE_CORRECTION_FACTOR
            )
        else:
            bp["VPower_avg"] = Q_(vpower, "W/cm**3")
            bp["VPower_peak"] = (
                bp["VPower_avg"] / bp["duty_cycle"] #* PULSE_SHAPE_CORRECTION_FACTOR
            )

        bp["I_peak"] = self.VPower2Intens(bp["VPower_peak"], bp)
        bp["I_avg"] = self.VPower2Intens(bp["VPower_avg"], bp)
        bp["P_peak"] = (bp["I_peak"] * np.pi * bp["w"] ** 2 / 2).to("W")
        bp["P_avg"] = (bp["I_avg"] * np.pi * bp["w"] ** 2 / 2).to("W")
        bp["angle"] = Q_(
            self.power2angle(bp["P_avg"].m, Ex_wavelength=Ex_wavelength), "deg"
        )

        return bp

    def angle2vol_power(self, angle, Ex_wavelength=1045, config=None):
        # TODO: write some out of range shift
        bp = self.angle2intens(angle, Ex_wavelength=Ex_wavelength, config=config)
        return bp

    def value2angle(
        self,
        power_avg=None,
        intens_peak=None,
        vol_power_avg=None,
        vol_power_peak=None,
        Ex_wavelength=1045,
        config=None,
        IR=False,
    ):
        # import pdb; pdb.set_trace()
        bp = None
        if not power_avg is None:
            if IR:
                angle = self.power2angle_IR(power_avg, Ex_wavelength=Ex_wavelength)
                bp = self.angle2intens_IR(
                    angle,
                    Ex_wavelength=Ex_wavelength,
                    config=config,
                )
            else:
                angle = self.power2angle(power_avg, Ex_wavelength=Ex_wavelength)
                bp = self.angle2intens(
                    angle, Ex_wavelength=Ex_wavelength, config=config
                )

        if not intens_peak is None:
            if IR:
                bp = self.intens2angle_IR(
                    intens_peak,
                    Ex_wavelength=Ex_wavelength,
                    config=config,
                )
            else:
                bp = self.intens2angle(
                    intens_peak, Ex_wavelength=Ex_wavelength, config=config
                )

        if not vol_power_avg is None:
            if IR:
                bp = self.vol_power2angle_IR(
                    vol_power_avg,
                    config=config,
                    Ex_wavelength=Ex_wavelength,
                    peak=False,
                )
            else:
                bp = self.vol_power2angle(
                    vol_power_avg,
                    Ex_wavelength=Ex_wavelength,
                    config=config,
                    peak=False,
                )

        if not vol_power_peak is None:
            if IR:
                bp = self.vol_power2angle_IR(
                    vol_power_peak,
                    config=config,
                    Ex_wavelength=Ex_wavelength,
                    peak=True,
                )
            else:
                bp = self.vol_power2angle(
                    vol_power_peak,
                    Ex_wavelength=Ex_wavelength,
                    config=config,
                    peak=True,
                )

        return bp

    def angle2value(self, angle, Ex_wavelength=1045, config=None, IR=False):
        if IR:
            bp = self.angle2intens_IR(
                angle,
                Ex_wavelength=Ex_wavelength,
                config=config,
            )
        else:
            bp = self.angle2intens(angle, Ex_wavelength=Ex_wavelength, config=config)

        return bp


if __name__ == "__main__":
    ct = CalibrToolkit(
        storePath="D:\\microV\configStore.exdir",
        setup="HRS",
        config={
            "focusing_by": "PAC04AR.16",
            "Focal length[mm]": 30,
            "Waist-sample distance[mm]": 30,
        },
        before="20210311",
    )
    ct = CalibrToolkit(
        storePath="D:\\microV\configStore.exdir",
        setup="HRS",
        config={
            "focusing_by": "PAC04AR.16",
            "Focal length[mm]": 30,
            "Waist-sample distance[mm]": 30,
        },
    )
    ct = CalibrToolkit(storePath="D:\\microV\configStore.exdir", setup="HRS")

    val = ct.angle2value(
        np.array([70, 95]),
        Ex_wavelength=1200,
        config={"Focal length[mm]": 30, "Waist-sample distance[mm]": 35},
    )
    print(val)

    val = ct.angle2value(
        70,
        Ex_wavelength=1200,
        config={"Focal length[mm]": 30, "Waist-sample distance[mm]": 30},
    )
    print(val)

    val = ct.angle2value(
        np.array([30, 65]),
        config={"Focal length[mm]": 30, "Waist-sample distance[mm]": 30},
        Ex_wavelength=700,
        IR=True,
    )
    print(val)

    val = ct.angle2value(np.array([30, 65]), Ex_wavelength=1300, IR=True)
    print(val)

    print(val["P_avg"])
