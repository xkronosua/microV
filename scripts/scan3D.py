import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir
from pathlib import Path
import time
import traceback
import threading
from scipy.interpolate import interp1d
import numpy.lib.recfunctions as rfn
import pint
from collections import deque
import datetime
import pandas as pd

sys.path.append(str(Path("../utils")))

from .scriptUtils import scan_move_recursive, isRunning, process_progress_info

import importlib
import beepy as beep

from utils.units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES

ureg = pint.UnitRegistry()
Q_ = ureg.Quantity


# def time_sleep(secs):
#     QtCore.QThread.msleep(abs(int(secs*1000)))


class scan3DThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    moveToSignal = QtCore.Signal(float, str, float)
    running = False
    store = None
    fname = "tmp"
    header = ["index", "axis", "A", "B", "C"]
    pixelIndex = {"X": 0, "Y": 0, "Z": 0}
    scan3D_direction = False

    def time_sleep(self, secs):
        time.sleep(secs)
        #self.msleep(abs(int(secs*1000)))

    def abstractMoveTo(self, target, axis="None", wait=True):
        self.moveToSignal.emit(target, axis, wait)

    def __init__(self, parent=None):
        super(scan3DThread, self).__init__(parent)

        self.fname = Path(self.parent().expData_filePath.text())
        if ".exdir" in str(self.fname):
            pass
        else:
            self.fname = str(self.fname).split(".exdir")[0] + ".exdir"

        self.start_time = datetime.datetime.now()
        self.cycle_time = deque([time.time()], maxlen=1000)

    def stop(self):
        if self.parent().ui.finalization_protocol_beep.isChecked():
            beep.beep(1)
        if self.parent().ui.finalization_protocol_closeShutters.isChecked():
            self.parent().laserSetShutter(False, wait=True)
            self.parent().laserSetIRShutter(False, wait=True)

        if self.parent().ui.finalization_protocol_laserOff.isChecked():
            self.parent().laserOnOff(False)

        if self.parent().ui.finalization_protocol_fullShutdown.isChecked():
            self.parent().closeEvent()

        self.running = False

    # def checkOverflow(self):
    # 	if len(self.activeDataset) == self._write_to_row:
    # 		prev_name = self.activeDataset.object_name
    # 		l = len(self.activeDataset)
    # 		prev_dataset = self.activeDataset
    # 		new_len = np.prod([len(i) for i in self.scan_ranges])
    #
    # 		self.activeDataset = self.storeActiveGroup.create_dataset(prev_name+'_resized',shape=(new_len,),dtype=self.activeDataset.dtype)
    # 		self.activeDataset.data[:l] = prev_dataset.data[:]

    def process_script_line(self, index, script_line, iteration):
        print(script_line.to_dict())
        t = []
        t.append(time.time())

        if script_line["type"] == "move":
            self.parent().abstractMoveTo(
                script_line["target"], axis=script_line["axis"]
            )
            self._targets[int(script_line.level)] = script_line["target"]
            t.append(time.time())
            self.params.child("Config", "PlotAxis").setValue(script_line["axis"])
            self.pixelIndex[script_line["axis"]] = int(script_line["iteration"])

            self._prev_axis = script_line["axis"]

            if not self._prev_axis in "XYZ":
                t.append(time.time())
                self.activeDataset["iteration"][self._write_to_row][:] = iteration
                self.activeDataset["time"][self._write_to_row] = time.time()
                t.append(time.time())
                for i in range(len(self.scan_axes)):
                    self.activeDataset["position"][self.scan_axes[i]][
                        self._write_to_row
                    ] = self.parent().abstractGetPosition(axis=self.scan_axes[i])
                t.append(time.time())

        elif script_line["type"] == "afterLoop":
            if self._prev_axis == list(self.scan_ranges3D)[-1]:
                self._preview_start = self._write_to_row

        elif script_line["type"] == "finish3D":
            for i in range(len(self.scan_axes)):
                self.activeDataset["position"][self.scan_axes[i]][
                    self._write_to_row
                ] = self.parent().abstractGetPosition(axis=self.scan_axes[i])

            self._write_to_row += 1
            self._preview_start = self._write_to_row

        elif script_line["type"] == "wait":
            self.time_sleep(script_line["target"])

        elif script_line["type"] == "read":
            self.parent().laserClient.kickWatchdog()
            t.append(time.time())
            self.time_sleep(self.params.child("Config", "WaitBeforeReadout").value())
            lines = self.parent().getUnifiedData(
                save_raw=self.save_raw,
                save_metadata=self.save_all_metadata,
                axes_positions=["X", "Y", "Z", "X_target", "Y_target", "Z_target"],
                parameters=["laserBultInPower"],
            )
            t.append(time.time())
            # self.checkOverflow()
            zi = self.pixelIndex["Z"]
            xi = self.pixelIndex["X"]
            yi = self.pixelIndex["Y"]

            self.activeDataset["time"][self._write_to_row] = time.time()
            self.activeDataset["data"][self._write_to_row][xi, yi, zi] = lines["data"]
            self.activeDataset["metadata"][self._write_to_row][xi, yi, zi] = lines[
                "metadata"
            ]

            x = []
            lin_scan = []
            if list(self.scan_ranges3D)[-1] == "X":
                lin_scan = self.activeDataset["data"][self._write_to_row][
                    :, yi, zi
                ].copy()
                if self.save_all_metadata:
                    x = self.activeDataset["metadata"][self._write_to_row][:, yi, zi][
                        "X"
                    ].copy()
                else:
                    x = np.arange(len(lin_scan))
            elif list(self.scan_ranges3D)[-1] == "Y":
                lin_scan = self.activeDataset["data"][self._write_to_row][
                    xi, :, zi
                ].copy()
                if self.save_all_metadata:
                    x = self.activeDataset["metadata"][self._write_to_row][xi, :, zi][
                        "Y"
                    ].copy()
                else:
                    x = np.arange(len(lin_scan))
            elif list(self.scan_ranges3D)[-1] == "Z":
                lin_scan = self.activeDataset["data"][self._write_to_row][
                    xi, yi, :
                ].copy()
                if self.save_all_metadata:
                    x = self.activeDataset["metadata"][self._write_to_row][xi, yi, :][
                        "Z"
                    ].copy()
                else:
                    x = np.arange(len(lin_scan))

            if "PicoScope" in lines["data"].dtype.names:
                if "ChA" in lines["data"]["PicoScope"]["data"].dtype.names:
                    self.data_A[zi, xi, yi] = lines["data"]["PicoScope"]["data"]["ChA"]
                if "ChB" in lines["data"]["PicoScope"]["data"].dtype.names:
                    self.data_B[zi, xi, yi] = lines["data"]["PicoScope"]["data"]["ChB"]

            if "AndorCamera" in lines["data"].dtype.names:
                self.data_A[zi, xi, yi] = lines["data"]["AndorCamera"]["data"][
                    self.params.child("Config").child("AndorCamera A").value()
                ]
                self.data_B[zi, xi, yi] = lines["data"]["AndorCamera"]["data"][
                    self.params.child("Config").child("AndorCamera B").value()
                ]

            if "Powermeter" in lines["data"].dtype.names:
                self.data_A[zi, xi, yi] = lines["data"]["Powermeter"]["data"]["power"]

            #print(x)

            info_ = f"{self._targets[:-1]}"
            items = []
            if len(x) > 1:
                if time.time() - self.lastUpdate > 0.3:
                    w = x != 0
                    #items = []
                    # print('a'*100)
                    for source in lin_scan.dtype.names:
                        for name in lin_scan[source]["data"].dtype.names:
                            items.append(
                                {
                                    "type": "Scan",
                                    "name": f"{source}_{name}",
                                    "info": f"",
                                    #'index': i,
                                    "time": time.time(),
                                    "data": [x[w], lin_scan[source]["data"][name][w]],
                                }
                            )
                        if "raw" in lines["data"][source].dtype.names and self.save_raw:
                            raw_x_name = lines["data"][source]["raw"].dtype.names[0]
                            for name in lines["data"][source]["raw"].dtype.names[1:-1]:
                                if name in ["type", "VRange","time"]: continue
                                items.append(
                                    {
                                        "type": "raw",
                                        "name": f"{source}_{name}",
                                        "info": f"",
                                        #'index': i,
                                        "time": time.time(),
                                        "data": [
                                            lines["data"][source]["raw"][raw_x_name][0],
                                            lines["data"][source]["raw"][name][0],
                                        ],
                                    }
                                )
                t.append(time.time())
                childs = (
                    self.parent().scan3D_config
                )  # params.child('3DScanAxes').childs

                zvals = self.scan_ranges3D["Z"]
                if len(np.unique(self.scan_ranges3D["Z"])) == 1:
                    zvals = np.arange(len(self.scan_ranges3D["Z"]))

                zindex = self.pixelIndex["Z"]

                #items = []
                if self.data_A.sum()!=0:
                    items.append(
                        {
                            "type": "img",
                            "name": "A",
                            "time": time.time(),
                            "data": [
                                self.data_A,
                                (
                                    self.scan_ranges3D["X"].min()
                                    - childs["X"]["step"].value() / 2,
                                    self.scan_ranges3D["Y"].min()
                                    - childs["Y"]["step"].value() / 2,
                                ),
                                (
                                    childs["X"]["step"].value(),
                                    childs["Y"]["step"].value(),
                                ),
                                zvals,
                                zindex,
                            ],
                        })
                if self.data_B.sum()!=0:
                    items.append(
                        {
                            "type": "img",
                            "name": "B",
                            "time": time.time(),
                            "data": [
                                self.data_B,
                                (
                                    self.scan_ranges3D["X"].min()
                                    - childs["X"]["step"].value() / 2,
                                    self.scan_ranges3D["Y"].min()
                                    - childs["Y"]["step"].value() / 2,
                                ),
                                (
                                    childs["X"]["step"].value(),
                                    childs["Y"]["step"].value(),
                                ),
                                zvals,
                                zindex,
                            ],
                        }

                )
                self.dataReadySignal.emit(items)
                t.append(time.time())
                #self.dataReadySignal.emit(items)
            t.append(time.time())

            if not self._prev_axis in "XYZ":
                t.append(time.time())
                self.activeDataset["iteration"][self._write_to_row][:] = iteration
                self.activeDataset["time"][self._write_to_row] = time.time()
                t.append(time.time())
                for i in range(len(self.scan_axes)):
                    self.activeDataset["position"][self.scan_axes[i]][
                        self._write_to_row
                    ] = self.parent().abstractGetPosition(axis=self.scan_axes[i])
                t.append(time.time())

            if self.parent().ui.actionPause.isChecked():
                while self.parent().ui.actionPause.isChecked():
                    self.time_sleep(0.1)

            try:
                progress = next(self.progress)
                info = process_progress_info(
                    progress, self.total_steps, self.start_time, self.cycle_time
                )
                self.progressSignal.emit(int(round(progress)))
                self.parent().progressBar.setToolTip(info)
            except Exception as ex:
                logging.error("progressError", exc_info=ex)
            t.append(time.time())

        elif script_line["type"] == "finish":

            self.running = False
        else:
            pass

        print(np.diff(t))

    def _recGenScript(self, level, script, parent):
        sub_levels = 0
        sub_lev = 0
        # print(level,parent)
        if level == 3:
            return 0
        axis = parent[level]
        child = self.parent().scan3D_config[axis]
        axes_3D = list(self.parent().scan3D_config["scanOrder"].currentText())

        if child["scanByGrid"].isChecked():
            targets = np.array([float(i) for i in child["Grid"].text().split(",")])
        else:
            targets = np.arange(
                child["start"].value(), child["end"].value(), child["step"].value()
            )
            if not child["end"].value() in targets:
                targets = np.append(targets, child["end"].value())

        Range = list(enumerate(targets))

        if (
            axis == axes_3D[-1]
            and self.parent().ui.scan3D_scanPath.currentText()=='ZigZag'
            ):
            self.scan3D_direction = not self.scan3D_direction
            if not self.scan3D_direction:
                Range = Range[::-1]

        for i, target in Range:
            script.append(
                {
                    "iteration": i,
                    "level": level,
                    "axis": axis,
                    "target": target,
                    "type": "move",
                }
            )
            sub_lev = self._recGenScript(level + 1, script, parent)
            if sub_lev == 0 and script[-1]["type"]!="afterLoop":
                script.append(
                    {
                        "iteration": i,
                        "level": level,
                        "type": "read",
                    }
                )

        if sub_lev == 0 and script[-1]["type"]=="read":
            script.append(
                {
                    "iteration": i,
                    "level": level,
                    "type": "afterLoop",
                }
            )

        sub_levels += 1
        if level == 2:
            sub_levels = 0
        # print(sub_levels)
        return sub_levels

    def scriptGen_readFunc(
        self, level, script, child, _iter=0, axis=np.nan, target=np.nan
    ):
        # print('scriptGen_readFunc '*100)
        axes_3D = list(self.parent().scan3D_config["scanOrder"].currentText())
        self._recGenScript(level=0, script=script, parent=axes_3D)

        script.append(
            {
                "iteration": 0,
                "level": level,
                "type": "finish3D",
            }
        )

    @isRunning
    def run(self):
        with exdir.File(self.fname) as self.store:
            self.params = self.parent().scan3D_tree.params

            if not "scan3D" in self.store:
                self.storeActiveGroup = self.store.create_group("scan3D").create_group(
                    time.strftime("data%Y%m%d-%H%M%S")
                )
            else:
                self.storeActiveGroup = self.store["scan3D"].create_group(
                    time.strftime("data%Y%m%d-%H%M%S")
                )
            self.source = self.parent().readoutSources.currentText()

            self.save_all_metadata = (
                self.params.child("Config").child("Save all metadata").value()
            )
            self.save_raw = self.params.child("Config").child("Save raw").value()

            self.script, self.iterations = self.parent().scan3D_tree.generateScript(
                scriptGen_readFunc3D=self.scriptGen_readFunc
            )

            print("*",self.script,self.iterations)
            print("**",self.script['axis'].dropna().unique())

            self.scan_ranges3D = {}
            self.scan_scale3D = {}
            for axis in list(self.parent().scan3D_config["scanOrder"].currentText()):
                child = self.parent().scan3D_config[axis]
                if child["scanByGrid"].isChecked():
                    targets = np.array(
                        [float(i) for i in child["Grid"].text().split(",")]
                    )
                else:
                    targets = np.arange(
                        child["start"].value(),
                        child["end"].value(),
                        child["step"].value(),
                    )
                    if not child["end"].value() in targets:
                        targets = np.append(targets, child["end"].value())
                self.scan_ranges3D[axis] = targets
                self.scan_scale3D[axis] = child["step"].value()

            self.data_A = np.zeros(
                (
                    len(self.scan_ranges3D["Z"]),
                    len(self.scan_ranges3D["X"]),
                    len(self.scan_ranges3D["Y"]),
                )
            )
            self.data_B = self.data_A.copy()

            self.scan_axes = [
                ax
                for ax in self.script["axis"].dropna().unique()
                if not ax in ["X", "Y", "Z", "XYZ"]
            ]
            if len(self.scan_axes)==0:
                self.scan_axes.append("XYZ")
            print(
                self.scan_axes,
                type(self.scan_axes),
                type(self.scan_axes[0]),
            )

            lines = self.parent().getUnifiedData(
                save_raw=self.save_raw,
                save_metadata=self.save_all_metadata,
                axes_positions=["X", "Y", "Z", "X_target", "Y_target", "Z_target"],
                parameters=["laserBultInPower"],
            )

            pos_axis_val = []
            pos_axis_dtype = []

            for ax in self.scan_axes:
                pos_axis_val.append(self.parent().abstractGetPosition(axis=ax))
                pos_axis_dtype.append((ax, AXES_UNITS_DTYPES[ax][1]))

            if self.save_all_metadata:
                dtype = [
                    ("time", "f8"),
                    ("position", pos_axis_dtype),
                    (
                        "data",
                        lines.dtype["data"],
                        (
                            len(self.scan_ranges3D["X"]),
                            len(self.scan_ranges3D["Y"]),
                            len(self.scan_ranges3D["Z"]),
                        ),
                    ),
                    (
                        "metadata",
                        lines.dtype["metadata"],
                        (
                            len(self.scan_ranges3D["X"]),
                            len(self.scan_ranges3D["Y"]),
                            len(self.scan_ranges3D["Z"]),
                        ),
                    ),
                    ("iteration", "i4", (self.iterations.shape[1],)),
                ]
            else:
                dtype = [
                    ("time", "f8"),
                    ("position", pos_axis_dtype),
                    (
                        "data",
                        lines.dtype["data"],
                        (
                            len(self.scan_ranges3D["X"]),
                            len(self.scan_ranges3D["Y"]),
                            len(self.scan_ranges3D["Z"]),
                        ),
                    ),
                    ("iteration", "i4", (self.iterations.shape[1],)),
                ]
            print(dtype)
            dtype = np.dtype(dtype)

            first_index = 0
            #
            # if self.source == "AndorCamera":
            #     lines["data"]["AndorCamera"]["raw"][:] = self.parent().andorCCDBaseline
            #     first_index = 0
            # else:
            #     pass
            N_rows = (self.script["type"] == "finish3D").sum()
            print(N_rows, self.script["type"].unique())

            self.parent().scriptThread_lastStorePath = (
                self.store.directory.absolute(),
                self.storeActiveGroup.name,
            )
            self.storeActiveGroup.attrs[
                "README"
            ] = self.parent().ui.readme_textBrowser.toPlainText()
            self.activeDataset = self.storeActiveGroup.create_dataset(
                "data", shape=(N_rows + first_index,), dtype=dtype
            )

            self.total_steps = (self.script["type"] == "read").sum()
            print(self.scan_axes)

            if self.params.child("Config", "PlotAxis").opts["limits"] != self.scan_axes:
                self.params.child("Config", "PlotAxis").setLimits(self.scan_axes)
                self.params.child("Config", "PlotAxis").setValue(self.scan_axes[-1])

            self.progress = iter(np.linspace(0, 100, self.total_steps).tolist())

            # self.activeDataset['time'][0] = time.time()
            # self.activeDataset['data'][0] = lines['data']
            # if self.save_all_metadata:
            # 	self.activeDataset['metadata'][0] = lines['metadata']
            # print(self.activeDataset['position'][0],pos_axis_val)
            # for i in range(len(self.scan_axes)):
            # 	self.activeDataset['position'][i][0] = pos_axis_val[i]
            # self.activeDataset['iteration'][0] = -1

            self.activeDataset.attrs["axes_units_dtypes"] = AXES_UNITS_DTYPES
            self.activeDataset.attrs["sources_units_dtypes"] = AXES_UNITS_DTYPES

            self.activeDataset.attrs["readoutSource"] = self.source
            self.activeDataset.attrs["header"] = str(lines.dtype)
            self.activeDataset.attrs[
                "filters"
            ] = self.parent().filtersPiezoStage_Dict  # .to_dict()
            info, current = self.parent().MultiScan_getInfo()
            # print({k:info[k] for k in info.dtype.names})
            self.activeDataset.attrs["centerIndex_info"] = {
                k: info[k].tolist() for k in info.dtype.names
            }
            self.activeDataset.attrs["scale"] = self.scan_scale3D
            self.activeDataset.attrs["ranges"] = str(self.scan_ranges3D)

            metadata = self.parent().abstractGetMetadata()
            metadata_ = {}
            for k in metadata.dtype.names:
                if hasattr(metadata[k], "dtype"):
                    if metadata[k].ndim == 0:
                        metadata_[k] = metadata[k].reshape(1)[0].tolist()
                    else:
                        metadata_[k] = metadata[k].tolist()
                elif type(metadata[k]) in (float, str, int, list):
                    metadata_[k] = metadata[k]

                else:
                    metadata_[k] = str(metadata[k])
                print("!!!!!!!", k, metadata_[k], type(metadata_[k]))
                self.activeDataset.attrs[k] = metadata_[k]

            self.lastUpdate = time.time()
            self._write_to_row = 0
            self._preview_start = self._write_to_row
            self._prev_axis = None

            self._targets = [-1] * int(self.script.level.max() + 1)

            self.cycle_time = deque([time.time()], maxlen=1000)

            for i, script_line in self.script.iterrows():
                if not self.running:
                    break
                try:
                    self.process_script_line(i, script_line, self.iterations[i])
                except Exception as ex:
                    logging.error("scan3D:ERROR", exc_info=ex)
                    break
            # print(self.activeDataset.dtype)
            del self.activeDataset
            del self.storeActiveGroup

        self.store.close()
        # self.stop()


class fastScan3DThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    running = False

    def __init__(self, parent):
        super(fastScan3DThread, self).__init__(parent)
        self.fname = fname = self.parent().expData_filePath.text()
        if ".exdir" in self.fname:
            pass
        else:
            self.fname = self.fname.split(".exdir")[0] + ".exdir"

    def __isRunning(f):
        def wrapper(*args, **kw):
            r = None
            try:
                self = args[0]
                self.running = True
                r = f(*args, **kw)
                self.running = False
                self.finishedSignal.emit(time.time())
            except Exception as ex:
                logging.error("fastScan3D:ERR", exc_info=ex)
                self.finishedSignal.emit(time.time())
                self.running = False
            return r

        return wrapper

    @__isRunning
    def run(self):

        z_start = float(self.parent().ui.scan3D_config.item(0, 1).text())
        z_end = float(self.parent().ui.scan3D_config.item(0, 2).text())
        z_step = float(self.parent().ui.scan3D_config.item(0, 3).text())

        if z_start == z_end:
            z_end += z_step
        if z_start > z_end:
            z_step *= -1

        if z_step == 0:
            Range_z = np.array([z_start] * 100)
        else:
            Range_z = np.arange(z_start, z_end, z_step)

        Range_zi = np.arange(len(Range_z))
        y_start = float(self.parent().ui.scan3D_config.item(1, 1).text())
        y_end = float(self.parent().ui.scan3D_config.item(1, 2).text())
        y_step = float(self.parent().ui.scan3D_config.item(1, 3).text())
        if y_start == y_end:
            y_end += y_step
        if y_start > y_end:
            y_step *= -1

        Range_y = np.arange(y_start, y_end, y_step)
        Range_yi = np.arange(len(Range_y))

        x_start = float(self.parent().ui.scan3D_config.item(2, 1).text())
        x_end = float(self.parent().ui.scan3D_config.item(2, 2).text())
        x_step = float(self.parent().ui.scan3D_config.item(2, 3).text())
        if x_start == x_end:
            x_end += x_step
        if x_start > x_end:
            x_step *= -1

        Range_x = np.arange(x_start, x_end, x_step)
        Range_xi = np.arange(len(Range_x))

        data_pmtA = np.zeros((len(Range_z), len(Range_x), len(Range_y)))
        # data_pmtA.fill(np.nan)
        data_pmtB = np.zeros((len(Range_z), len(Range_x), len(Range_y)))

        def stop_callback():
            print("External readout stop_callback")

        scanParam = self.parent().piStage.initGridScanXY(
            rangeX=(x_start, x_end),
            rangeY=(y_start, y_end),
            scan_axis=0,
            N=len(Range_y),
            rate=2,
        )
        print(scanParam)
        waveTable_dt = scanParam["waveTable_dt"]

        self.averageNpulses = self.parent().ui.fastScan3D_averageNpulses.value()
        self.PicoScope_config = {}
        self.PicoScope_config["n_captures"] = 2 ** 15
        self.PicoScope_config["frame_duration"] = waveTable_dt
        self.PicoScope_config["sampleInterval"] = 2e-9
        self.PicoScope_config["pulseFreq"] = float(
            self.parent().ui.pulseFreq.text()
        )  # Hz
        self.PicoScope_config["samplingDuration"] = (
            1 / self.PicoScope_config["pulseFreq"] * self.averageNpulses
        )
        # import pdb; pdb.set_trace()
        self.PicoScope_config = self.parent().PicoScope_setConfig(self.PicoScope_config)
        print(self.PicoScope_config)

        PicoScope_dataA = np.zeros(
            (self.PicoScope_config["n_captures"], self.PicoScope_config["noSamples"]),
            dtype=np.int16,
        )
        PicoScope_dataB = np.zeros(
            (self.PicoScope_config["n_captures"], self.PicoScope_config["noSamples"]),
            dtype=np.int16,
        )
        # self.PicoScope_read_done = True

        self.parent().piStage.gridScanXY(stop_callback=stop_callback, timeout=10)

        if self.parent().ui.fastScan3D_trigSource.currentText() == "Software":
            t0 = time.time()
            self.parent().piStage.softStartEvent.set()
            self.parent().PicoScope.runBlock(pretrig=self.PicoScope_config["pretrig"])
            self.parent().PicoScope.waitReady()
            t1 = time.time()

        print("wait gridScanXY_done")
        while not self.parent().piStage.scanParam["gridScanXY_done"]:
            self.time_sleep(0.1)
            # print('wait gridScanXY_done',len(self.parent().piStage.pi_device.path['1']))

        last_gridScanXY = self.parent().piStage.scanParam["last_gridScanXY"]

        self.parent().PicoScope.getDataRawBulk(channel="A", data=PicoScope_dataA)
        self.parent().PicoScope.getDataRawBulk(channel="B", data=PicoScope_dataB)
        # self.parent().PicoScope_setConfig()

        dataA_p2p = PicoScope_dataA.max(axis=1) - PicoScope_dataA.min(axis=1)
        dataB_p2p = PicoScope_dataB.max(axis=1) - PicoScope_dataB.min(axis=1)
        # import pdb; pdb.set_trace()

        signals = np.array([self.parent().PicoScope_dataT, dataA_p2p, dataB_p2p]).T
        print(signals, signals.shape)
        dataA_ = np.hstack(self.parent().PicoScope.rawToV("A", PicoScope_dataA))
        dataB_ = np.hstack(self.parent().PicoScope.rawToV("B", PicoScope_dataB))
        dataT_ = self.parent().PicoScope_time_grid
        # self.parent().dataReadySignal.emit({'name':'line_pico_ChA','time':time.time(),'data':[dataT_,dataA_]})
        # self.parent().dataReadySignal.emit({'name':'line_pico_ChB','time':time.time(),'data':[dataT_,dataB_]})

        # import pdb; pdb.set_trace()
        X, Y, Z_A, Z_B, pathSignal = process_gridScanXY(
            signals,
            last_gridScanXY["data"],
            x_step=x_step,
            y_step=y_step,
            read_start_delay=0,
        )
        # x_step = X[0,1]-X[0,0]
        # y_step = Y[1,0]-Y[0,0]
        # import pdb; pdb.set_trace()
        dataA = np.array([Z_A.T])
        dataB = np.array([Z_B.T])
        if dataA.sum()!=0:
            self.dataReadySignal.emit(
                {
                    "name": "img",
                    "time": time.time(),
                    "data": [dataA, (X.min(), Y.min()), (x_step, y_step), np.array([1]), 0],
                }
            )
        if dataB.sum()!=0:
            self.dataReadySignal.emit(
                {
                    "name": "img1",
                    "time": time.time(),
                    "data": [dataB, (X.min(), Y.min()), (x_step, y_step), np.array([1]), 0],
                }
            )
        # import pdb; pdb.set_trace()
        self.dataReadySignal.emit(
            {
                "name": "line_pmtA",
                "time": time.time(),
                "data": [pathSignal[:, 3], pathSignal[:, 4]],
            }
        )
        self.dataReadySignal.emit(
            {
                "name": "line_pmtB",
                "time": time.time(),
                "data": [scanParam["path"][:, 0], scanParam["path"][:, 1]],
            }
        )
