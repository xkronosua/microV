#!/usr/bin/python
import sys
import time
import datetime
import os

# os.environ['PYQTGRAPH_QT_LIB'] = 'PyQt5'
# import qdarkstyle

if "PySide2" in sys.argv:
    # try:
    from PySide2 import QtWidgets, QtCore, QtGui
    from PySide2.QtCore import QThread, QThreadPool, QMutex, QMutexLocker
    from PySide2.QtCore import Signal
    import resources_pyside2
else:
    # except:

    from PyQt5 import QtWidgets, QtCore, QtGui
    from PyQt5.QtCore import QThread, QThreadPool, QMutex, QMutexLocker
    from PyQt5.QtCore import pyqtSignal as Signal

    QtCore.pyqtRemoveInputHook()
    import resources

import pyqtgraph as pg
from pyqtgraph.dockarea import DockArea, Dock
import numpy as np

from scipy.signal import medfilt, argrelextrema
from scipy.interpolate import interp1d
from scipy.ndimage import gaussian_filter
import scipy.ndimage
import scipy.optimize
import numpy.lib.recfunctions as rfn

import pandas as pd

# from skimage.io import imsave
from skimage.feature.peak import peak_local_max

# import matplotlib

# from multiprocessing import Process, Queue, Array
from queue import Queue
import traceback
import threading


import exdir
from pathlib import Path

import psutil
from utils.utils import loadUi, Worker, qtLoggingSignalEmiter
from utils.tableOfLines import TableOfLines

from utils.units_dtypes import (
    AXES_UNITS_DTYPES,
    READOUT_SOURCES_UNITS_DTYPES,
    PARAMETERS_UNITS_DTYPES,
)

# from scripts.scan1D import scan1DThread
from scripts import (
    scan3D,
    scanND,
    MultiScan,
    optimize_position,
    calibr as Calibr,
)
from scripts.scan3D import fastScan3DThread
from scripts.calibr import calibrThread

# from scripts.MultistepScan import MultistepScanThread
from scripts.scriptUtils import ScriptTree

from scripts.calibrTools import CalibrToolkit, calcShiftDelay, n_UVFS

import importlib

import faulthandler

faulthandler.enable()

from pyqtgraph.parametertree import Parameter, ParameterTree
from pyqtgraph.graphicsItems import TargetItem
import pint

ureg = pint.UnitRegistry()
Q_ = ureg.Quantity

import pyvisa

# import ipdb

demo = False

if "sim" in sys.argv:
    demo = True
    import nidaqmx
    from nidaqmx.constants import AcquisitionType, TaskMode

    # from picoscope import ps3000a
    from hardware.sim.picoscope import ps3000a

else:
    import nidaqmx
    from nidaqmx.constants import AcquisitionType, TaskMode
    from picoscope import ps3000a

CONNECT_ALL = False
if "connect_all" in sys.argv:
    CONNECT_ALL = True


################################################################################
################################################################################
BEAMSHIFT_BY_MIRRORS = True  # True for new mirrors based system
################################################################################
################################################################################

from hardware.cameras import thorcam as ThorCamera
from hardware.ThorlabsPM100 import ThorlabsPM100
from hardware.stages.Newport_rotationStage_AG_UC2 import AG_UC2
from hardware.spectrometers.Thorlabs_spectrometer_CCS200 import CCS200
from hardware.stages.PI_NanoCube_E727 import E727

# from hardware.stages.Thorlabs_rotationStage_TDC001 import TDC001

# from hardware.MoCo_linearStage import MOCO
# from hardware.Thorlabs_piezoStage_ELLx import ELLx as ELL14
# from hardware.Thorlabs_piezoStage_ELLx_bus import ELLx as ELLx_bus
# from hardware.laserClient import LaserClient

import Pyro4

Pyro4.config.SERIALIZER = "pickle"

# from hardware.cameras.atmcd import AndorAtmcd
# from hardware.AndorCamera import AndorCamera
# from hardware.spectrometers.Shamrock import ShamRock_spectrometer

import microscope

import logging
import logging.handlers

# if os.name != 'posix':
# 	from colorama import init
# 	init()
# 	logging.debug('colorama init')
from colorlog import ColoredFormatter
from signal import signal as SYSTEM_signal, SIGINT


def handler_SIGINT(signal_received, frame):
    # Handle any cleanup here
    print("SIGINT or CTRL-C detected. Exiting gracefully")
    sys.exit(0)


LOGLEVEL = logging.DEBUG
LOGLEVEL = logging.INFO

logging.getLogger("PyQt5.uic.uiparser").setLevel(logging.WARNING)
logging.getLogger("PyQt5.uic.properties").setLevel(logging.WARNING)
logging.getLogger("matplotlib.text").setLevel(logging.INFO)
logging.getLogger("matplotlib.font_manager").setLevel(logging.WARNING)
logging.getLogger("matplotlib.ticker").setLevel(logging.WARNING)
logging.getLogger("matplotlib").setLevel(logging.WARNING)
logging.getLogger("filelock").setLevel(logging.WARNING)
logging.getLogger("qdarkstyle").setLevel(logging.WARNING)

logging.getLogger("GCSDll").setLevel(logging.WARNING)
logging.getLogger("gcsdll").setLevel(logging.WARNING)
logging.getLogger("GCSCommands.qPOS").setLevel(logging.WARNING)
logging.getLogger("GCSDll.read").setLevel(logging.WARNING)


LOG_FILENAME = "microV.log"
LOGFORMAT_colored = "%(asctime)s - %(name)s - %(log_color)s[%(levelname)s]%(reset)s - %(log_color)s%(message)s%(reset)s (%(filename)s:%(lineno)d)"
LOGFORMAT = (
    "%(asctime)s - %(name)s - [%(levelname)s] - %(message)s (%(filename)s:%(lineno)d)"
)

microV_logger = logging.getLogger("")
microV_logger.setLevel(LOGLEVEL)
fh = logging.handlers.RotatingFileHandler(
    "microV.log",
    mode="a",
    maxBytes=50 * 1024 * 1024,
    backupCount=1,
    encoding=None,
    delay=False,
)
sh = logging.StreamHandler(sys.stdout)
formatter_colored = ColoredFormatter(LOGFORMAT_colored)
formatter = logging.Formatter(LOGFORMAT)
fh.setFormatter(formatter)
sh.setFormatter(formatter_colored)
microV_logger.addHandler(fh)
microV_logger.addHandler(sh)


microV_logger.info(f"microV demo mode: {demo}")
logging.info(f"microV demo mode: {demo}")
print("handlers:", microV_logger.handlers)

################################################################
################################################################
# QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)
microV_logger.debug(f"Qt lib:{pg.Qt.QT_LIB}")


def calcVisibleBands(lambda1=1045, lambda2=1300):
    res = {"w1": lambda1, "w2": lambda2}
    res["2w1"] = res["w1"] / 2
    res["3w1"] = res["w1"] / 3

    res["2w2"] = res["w2"] / 2
    res["3w2"] = res["w2"] / 3

    res["2w1-w2"] = 1 / (2 / res["w1"] - 1 / res["w2"])
    res["2w2-w1"] = 1 / (2 / res["w2"] - 1 / res["w1"])

    res["2w1+w2"] = 1 / (2 / res["w1"] + 1 / res["w2"])
    res["2w2+w1"] = 1 / (2 / res["w2"] + 1 / res["w1"])

    res["w1+w2"] = 1 / (1 / res["w1"] + 1 / res["w2"])

    return res


sleep_lock = threading.RLock()
def time_sleep(secs):
    with sleep_lock:
        time.sleep(secs)
    #loop = QtCore.QEventLoop()
    #QtCore.QTimer.singleShot(abs(int(secs*1000)), loop.quit)
    #loop.exec_()
    #QtCore.QThread.msleep(abs(int(secs*1000)))

def time_msleep(secs):
    with sleep_lock:
        time.sleep(secs/1000)
    #loop = QtCore.QEventLoop()
    #QtCore.QTimer.singleShot(secs, loop.quit)
    #loop.exec_()
    #QtCore.QThread.msleep(abs(int(secs*1000)))



class microV(QtWidgets.QMainWindow):
    _mutex = QMutex(QMutex.Recursive)
    _monitor_lock = False
    ############ configs #################
    settings = QtCore.QSettings(f"gui_{pg.Qt.QT_LIB}.ini", QtCore.QSettings.IniFormat)
    configStore = exdir.File("configStore.exdir", "r")

    scriptThread_lastStorePath = None

    ########### signals/slots ########################
    dataReadySignal = Signal(object)

    ############ hardware ################
    piStage = E727(demo=demo)

    piStage_position_beamSpot_position = np.zeros(2)
    ThorCamera_live = [None, None]
    piStage_position_cam_static_pos = np.zeros(2)


    _rotationStage_TDC001 = Pyro4.Proxy("PYRO:TDC001_rotationStage@127.0.0.1:8005")
    _rotationStage_TDC001._pyroRelease()
    rotationStage_TDC001 = _rotationStage_TDC001.axes["HWP"]
    microV_logger.info("PYRO:TDC001_rotationStage@127.0.0.1:8005")

    # rotPiezoStage = AG_UC2()
    # print('AG_UC2')
    moco = Pyro4.Proxy("PYRO:MoCo_linearStage@127.0.0.1:8001")
    moco._pyroRelease()
    mocoZeroDelay_position_calibr = None
    microV_logger.info("PYRO:MoCo_linearStage@127.0.0.1:8001")

    _ArduinoStage = Pyro4.Proxy("PYRO:ArduinoStage@127.0.0.1:8006")
    _ArduinoStage._pyroRelease()
    arduinoStage = _ArduinoStage.axes["lens_stage"]
    arduinoStage1 = _ArduinoStage.axes["knife_stage"]


    microV_logger.info("PYRO:PYRO:ArduinoStage@127.0.0.1:8006")

    HWP_FIX = Pyro4.Proxy("PYRO:ELLx_bus@127.0.0.1:8003")
    HWP_FIX._pyroRelease()
    microV_logger.info("PYRO:ELLx_bus@127.0.0.1:8003")

    piezoDev_bus = Pyro4.Proxy("PYRO:ELLx_bus@127.0.0.1:8002")
    piezoDev_bus._pyroRelease()
    microV_logger.info("PYRO:ELLx_bus@127.0.0.1:8003")

    # piezoDev_bus = Pyro4.Proxy('PYRO:ELLx_bus@127.0.0.1:8002')
    # piezoDev_bus._pyroRelease()
    # microV_logger.info('PYRO:ELLx_bus@127.0.0.1:8003')
    # commutation_mirror0_correction = -16.547739000000007

    ccs200_spectrometer = Pyro4.Proxy("PYRO:CCS200@127.0.0.1:8007")
    ccs200_spectrometer._pyroRelease()
    microV_logger.info("PYRO:CCS200@127.0.0.1:8007")

    if not BEAMSHIFT_BY_MIRRORS:
        beamShift_V = Pyro4.Proxy("PYRO:ELLx_bus@127.0.0.1:8008")
        beamShift_V.initialize()
        beamShift_V._pyroRelease()
        beamShift_H = _ArduinoStage.axes["beamShift_H"]
    else:
        beamShift = Pyro4.Proxy("PYRO:KDC101_motorizedActuator@127.0.0.1:8009")
        beamShift.initialize()
        beamShift._pyroRelease()
        beamShift_H = beamShift.axes["beamShift_H"]
        beamShift_V = beamShift.axes["beamShift_V"]



    microV_logger.info("PYRO:ELLx_bus@127.0.0.1:8008")

    PicoScope_config = {}
    PicoScope_config["PicoScope_SerialNumber"] = b"CW694/010\x00"
    PicoScope = ps3000a.PS3000a(
        serialNumber=PicoScope_config["PicoScope_SerialNumber"], connect=False
    )
    PicoScope_config["PicoScope_VRange_dict"] = {
        "Auto": 20.0,
        "20mV": 0.02,
        "50mV": 0.05,
        "100mV": 0.1,
        "200mV": 0.2,
        "500mV": 0.5,
        "1V": 1.0,
        "2V": 2.0,
        "5V": 5.0,
        "10V": 10.0,
        "20V": 20.0,
    }
    PicoScope_lock = threading.Lock()

    # shamrock = Pyro4.Proxy('PYRO:ShamRock_spectrometer@127.0.0.1:8005')
    # shamrock._pyroRelease()
    # shamrock = ShamRock_spectrometer()
    # shamrock = ShamRockController()
    # andorCCD = AndorCamera()
    # andorCCD = AndorAtmcd()
    # andorCCD = Pyro4.Proxy('PYRO:AndorAtmcd@127.0.0.1:8004')
    # andorCCD._pyroRelease()
    andorShamrock = Pyro4.Proxy("PYRO:AndorShamrock_spectrometer@127.0.0.1:8004")
    andorShamrock._pyroRelease()
    andorCCD = andorShamrock.devices["andor"]
    shamrock = andorShamrock.devices["shamrock"]
    print("PYRO:AndorShamrock_spectrometer@127.0.0.1:8004")
    shamrockWavelengthSet_lock = threading.RLock()
    andorCCD_prev_center_wavelength = 0
    andorCamera_temperature_lastUpdate = time.time()
    andorCamera_correctionCurve = np.ones(1024)

    # laserClient = LaserClient(demo=demo)
    laserClient = Pyro4.Proxy("PYRO:InSightX3Laser@127.0.0.1:8000")
    laserClient._pyroRelease()
    print("PYRO:InSightX3Laser@127.0.0.1:8000")
    laserPower_params = {"TUN": None, "FIX": None}

    thorcam = None  # ThorCamera.CameraDummy()
    ThorCamera_img_isSaved = True

    power_meter = None

    ########### Threads dict ################
    actionThreads = {}
    scriptThread = None

    threadpool = QThreadPool()
    sleep_lock = threading.Lock()

    ############### Tools ####################################
    scanND_tree = None
    mouse_last_pos = {
        "Preview": (0, 0),
        "Scan": (0, 0),
        "Spectra": (0, 0),
    }


    def _gen_splash(self):
        splash_pix = QtGui.QPixmap(str(Path("./ui/logo.jpg"))).scaledToWidth(400)
        self.splash = QtWidgets.QSplashScreen(splash_pix, QtCore.Qt.WindowStaysOnTopHint)
        self.splash.show()

    def time_sleep(self, secs):
        with self.sleep_lock:
            time.sleep(secs)
        #loop = QtCore.QEventLoop()
        #QtCore.QTimer.singleShot(abs(int(secs*1000)), loop.quit)
        #loop.exec_()
        #QtCore.QThread.msleep(abs(int(secs*1000)))

    def time_msleep(secs):
        with self.sleep_lock:
            time.sleep(self, secs/1000)
        #loop = QtCore.QEventLoop()
        #QtCore.QTimer.singleShot(secs, loop.quit)
        #loop.exec_()
        #QtCore.QThread.msleep(abs(int(secs*1000)))

    def __init__(self):
        self._gen_splash()
        self.demo = demo

        pid_list = []
        if os.path.exists("microV.pid"):
            with open("microV.pid") as f:
                pid_list = f.readlines()
            open("microV.pid", "w").close()
            print(pid_list)
            for pid in pid_list:
                pid = int(pid.split("\t")[0])
                if psutil.pid_exists(pid):
                    print("microV process with pid %d exists" % pid)
                    p = psutil.Process(pid)
                    p.terminate()  # or p.kill()
                    print("microV process with pid %d killed" % pid)
                else:
                    pass  # print("a process with pid %d does not exist" % pid)
        self.pid = os.getpid()
        with open("microV.pid", "a") as f:
            f.write(f"{self.pid}\n")

        super(microV, self).__init__()

        # from mainwindow import Ui_mw
        self.ui = loadUi(Path("ui/microV.ui"), Dir="ui")  # Ui_mw()
        self.ui.closeEvent = self.closeEvent
        self._want_to_close = False

        ################ QTimers #####################

        self.piStageLiveTimer = QtCore.QTimer()
        self.statusBar_message_cleanupTimer = -1# = QtCore.QTimer()

        self.initUI()
        self.ui.actionStop.setChecked(True)
        if Path("/tmp").exists():
            self.expData_filePath.setText("/tmp/tmp")

        self.initPiStage()
        if self.demo:
            if "XYZ" in self.settings.allKeys():
                pos = self.settings.value('XYZ')
                if not np.any(np.isnan(pos)):
                    self.piStage_moveTo(axis=[0, 1, 2], target=pos)
        self.generate_scriptTrees()
        # self.scanND_loadScript(reset=False)

        self.ui.setLocale(QtCore.QLocale(QtCore.QLocale.C))

        self.state_monitor_alive = True
        self.lastUpdateTime = time.time()
        worker = Worker(self.state_monitor)  # Any other args, kwargs are passed to
        # Execute
        self.threadpool.start(worker)

        self.log_handler = qtLoggingSignalEmiter()
        formatter = logging.Formatter(LOGFORMAT)
        self.log_handler.setFormatter(formatter)
        microV_logger.addHandler(self.log_handler)
        self.log_handler.new_record.objSignal.connect(self.process_log_events)

        #self.updateHWP_TUN()
        #self.updateHWP_FIX()
        if "piStage_position_cam_static_pos" in self.settings.allKeys():
            self.piStage_position_cam_static_pos = self.settings.value("piStage_position_cam_static_pos")
        if "piStage_position_beamSpot_position" in self.settings.allKeys():
            self.piStage_position_beamSpot_position = self.settings.value("piStage_position_beamSpot_position")


        self.ThorCamera_imgLoad()
        if not demo:
            self.ThorCamera_light(False)
        self.MultiScan_update()
        self._on_laserWavelength_valueChanged()

        if BEAMSHIFT_BY_MIRRORS:
            self.ui.laserBeamShift_V_angle.setVisible(False)
            self.ui.laserBeamShift_V_controlAngle.setVisible(False)
            self.ui.laserBeamShift_V_controlShift.setVisible(False)
            self.ui.laserBeamShift_V_delay_delta.setVisible(False)
            self.ui.laserBeamShift_H_delay_delta.setVisible(False)

        #microV_logger.error("READY")
        microV_logger.info("READY")

        # loggers = [logging.getLogger()]  # get the root logger
        # loggers = loggers + [logging.getLogger(name) for name in logging.root.manager.loggerDict]
        # logging.getLogger('GCS2Commands.qPOS').setLevel(logging.WARNING)
        # print(loggers)
        # self.closeEvent()
        self.ui.show()

        self.splash.hide()

    def generate_scriptTrees(self):
        self.scanND_tree = ScriptTree(
            config=[
                {"name": "Save all metadata", "type": "bool", "value": True},
                {"name": "PlotAxis", "type": "list", "values": []},
                {"name": "WaitBeforeReadout", "type": "float", "value": 0},
            ],
            ax_config=[
                {
                    "axis": "Z",
                    "active": True,
                    "start": 0,
                    "end": 100,
                    "step": 1,
                    "grid": None,
                },
                {
                    "axis": "Y",
                    "active": False,
                    "start": 0,
                    "end": 100,
                    "step": 1,
                    "grid": None,
                },
                {
                    "axis": "X",
                    "active": False,
                    "start": 0,
                    "end": 100,
                    "step": 1,
                    "grid": None,
                },
            ],
            default_axis={
                "axis": "X",
                "active": True,
                "start": 0,
                "end": 100,
                "step": 1,
                "grid": None,
            },
        )
        self.ui.scanND_container.addWidget(self.scanND_tree)

        if "ScanND_params" in self.settings.allKeys():
            state = self.settings.value("ScanND_params")
            self.scanND_tree.params.restoreState(state)

        self.scan3D_tree = ScriptTree(
            config=[
                {"name": "Save all metadata", "type": "bool", "value": True},
                {"name": "Save raw", "type": "bool", "value": True},
                {
                    "name": "AndorCamera A",
                    "type": "list",
                    "value": "2w1",
                    "values": [
                        "w1",
                        "2w1",
                        "3w1",
                        "w2",
                        "2w2",
                        "3w2",
                        "w1+w2",
                        "2w1-w2",
                        "2w2-w1",
                        "2w1+w2",
                        "2w2+w1",
                        "All",
                    ],
                },
                {
                    "name": "AndorCamera B",
                    "type": "list",
                    "value": "2w2",
                    "values": [
                        "w1",
                        "2w1",
                        "3w1",
                        "w2",
                        "2w2",
                        "3w2",
                        "w1+w2",
                        "2w1-w2",
                        "2w2-w1",
                        "2w1+w2",
                        "2w2+w1",
                        "All",
                    ],
                },
                {"name": "PlotAxis", "type": "list", "values": []},
                {"name": "WaitBeforeReadout", "type": "float", "value": 0},
            ],
            ax_config=[
                {
                    "axis": "XYZ",
                    "active": True,
                    "start": 0,
                    "end": 1,
                    "step": 1,
                    "grid": "0",
                },
            ],
            default_axis={
                "axis": "XYZ",
                "active": True,
                "start": 0,
                "end": 1,
                "step": 1,
                "grid": "0",
            },
        )

        if "Scan3D_params" in self.settings.allKeys():
            state = self.settings.value("Scan3D_params")
            self.scan3D_tree.params.restoreState(state)

        # Axis_Z = self.scan3D_tree.addAxis(name='Axis#Z', config={'axis': 'Z', 'active': True, 'start': 0,'end':100,'step':1,'grid':'50','removable': False})
        # Axis_Y = self.scan3D_tree.addSubAxis(name='Axis#Y', parent=Axis_Z, config={'axis': 'Y', 'active': True, 'start': 0,'end':50,'step':1,'grid':None,'removable': False})
        # Axis_X = self.scan3D_tree.addSubAxis(name='Axis#X', parent=Axis_Y, config={'axis': 'X', 'active': True, 'start': 0,'end':50,'step':1,'grid':None,'removable': False})
        # self.scan3D_tree_XYZ = {"Z": Axis_Z, 'Y': Axis_Y, 'X': Axis_X}

        self.ui.scan3D_container.addWidget(self.scan3D_tree)

        self.MultiScan_tree = ScriptTree(
            config=[
                {"name": "Save all metadata", "type": "bool", "value": True},
                {"name": "Save raw", "type": "bool", "value": True},
                {"name": "Polarization control", "type": "bool", "value": True},
                {"name": "Default exposure", "type": "float", "value": -1},
                {
                    "name": "Default shutters",
                    "type": "list",
                    "value": "None",
                    "values": [
                        "None",
                        "TUN",
                        "FIX",
                        "OPENED",
                        "CLOSED",
                    ],
                },
                {
                    "name": "Optimize on anchor",
                    "type": "group",
                    "children": [
                        {
                            "name": "General optimization",
                            "type": "group",
                            "children": [
                                {
                                    "name": "By",
                                    "type": "list",
                                    "value": "AndorCamera/data/2w1",
                                    "values": [],
                                },
                                {
                                    "name": "Axes",
                                    "type": "list",
                                    "value": "X,Y,Z",
                                    "values": [
                                        "None",
                                        "X,Y",
                                        "X,Y,Z",
                                        "X,Y,HWP_Polarization",
                                        "X,Y,Z,HWP_Polarization",
                                        "Custom axes",
                                    ],
                                },
                                {"name": "Custom axes", "type": "str", "value": ""},
                                {"name": "Relative", "type": "bool", "value": True},
                                {
                                    "name": "Bounds",
                                    "type": "str",
                                    "value": "-0.5,0.5;-0.5,0.5;-5,5;-45,50",
                                },
                                {"name": "Skip", "type": "int", "value": 0},
                                {"name": "Average", "type": "int", "value": 1},
                                {"name": "Maxiter", "type": "int", "value": 20},
                                # {'name': 'Tolerance', 'type': 'float','value': 0.001},
                                {"name": "Exposure", "type": "float", "value": -1},
                                {"name": "Wait", "type": "float", "value": 0},
                                # {'name': 'Save raw', 'type': 'bool','value': False},
                                {
                                    "name": "Shutter",
                                    "type": "list",
                                    "value": "None",
                                    "values": [
                                        "None",
                                        "TUN",
                                        "FIX",
                                        "OPENED",
                                        "CLOSED",
                                    ],
                                },
                                {
                                    "name": "Rewrite optimized",
                                    "type": "bool",
                                    "value": False,
                                },
                            ],
                        },
                        {
                            "name": "BeamShift+Delay",
                            "type": "group",
                            "children": [
                                {
                                    "name": "Optimize delay",
                                    "type": "bool",
                                    "value": False,
                                },
                                {
                                    "name": "Optimize shiftV",
                                    "type": "bool",
                                    "value": False,
                                },
                                {
                                    "name": "Optimize shiftH",
                                    "type": "bool",
                                    "value": False,
                                },
                                {
                                    "name": "Protocol",
                                    "type": "list",
                                    "value": "BeamShift(2w2), delay(w1+w2)",
                                    "values": [
                                        "BeamShift(2w2), delay(w1+w2)",
                                        "BeamShift and delay (w1+w2)",
                                        ],
                                },
                                {
                                    "name": "By",
                                    "type": "list",
                                    "value": "AndorCamera/data/w1+w2",
                                    "values": [],
                                },
                                {"name": "Skip", "type": "int", "value": 0},
                                {"name": "Average", "type": "int", "value": 1},
                                {"name": "Maxiter", "type": "int", "value": 20},
                                # {'name': 'Tolerance', 'type': 'float','value': 0.001},
                                {"name": "Exposure", "type": "float", "value": -1},
                                {"name": "Wait", "type": "float", "value": 0},
                                {
                                    "name": "Bounds[shiftV,shiftH,delay]",
                                    "type": "str",
                                    "value": "-0.5,0.5;-0.5,0.5;-0.1,0.1",
                                },
                                {
                                    "name": "Save optimized",
                                    "type": "bool",
                                    "value": False,
                                },
                            ],
                        },
                    ],
                },
                {"name": "PlotAxis", "type": "list", "values": []},
                {"name": "WaitBeforeReadout", "type": "float", "value": 0},
            ],
            ax_config=[
                {
                    "axis": "CenterIndex",
                    "active": True,
                    "start": 0,
                    "end": 100,
                    "step": 1,
                    "grid": "0",
                },
            ],
            default_axis={
                "axis": "CenterIndex",
                "active": True,
                "start": 0,
                "end": 1,
                "step": 1,
                "grid": "0",
            },
        )

        self.ui.MultiScan_container.addWidget(self.MultiScan_tree)

        if "MultiScan_params" in self.settings.allKeys():
            state = self.settings.value("MultiScan_params")
            self.MultiScan_tree.params.restoreState(state)

        optimize_chan = [
            self.ui.optimize_chan.itemText(i)
            for i in range(self.ui.optimize_chan.count())
        ]
        self.MultiScan_tree.params.child(
            "Config", "Optimize on anchor", "General optimization", "By"
        ).setLimits(optimize_chan)
        self.MultiScan_tree.params.child(
            "Config", "Optimize on anchor", "BeamShift+Delay", "By"
        ).setLimits(optimize_chan)
        # self.ui.powerCalibr_go.clicked.connect(self.laserPowerCalibr)


    def process_log_events(self, msg):

        if "[ERROR]" in msg:
            self.statusBar_setMessage(msg.split("\n")[0].split("[ERROR] - ")[1])
            self.statusBar_message.setToolTip(msg)

    def statusBar_setMessage(self, msg):
        self.statusBar_message.setText(msg)

        self.statusBar_message_cleanupTimer = time.time()#.start(5000)

    def statusBar_cleanMessage(self):
        self.statusBar_message.setText("")
        self.statusBar_message.setToolTip("")
        self.statusBar_message_cleanupTimer = -1

    def stopActiveThreads(self):
        self.ui.actionPause.setChecked(False)
        # keys = self.actionThreads.keys()
        # active = []
        timeout = 1000
        qm = QtWidgets.QMessageBox
        if "AndorCamera" in self.readoutSources.currentText():
            self.andorCCD.abort()
            # timeout += int(self.ui.andorCameraExposure.value()*1000*0.1)
        success = True
        if not self.scriptThread is None:
            self.scriptThread.stop()
            time_msleep(timeout)
            #QThread.msleep(timeout)
            if not self.scriptThread is None:
                if self.scriptThread.isRunning():
                    self.scriptThread.quit()
                    self.scriptThread.wait()

            if not self.scriptThread is None:
                if not self.scriptThread.isRunning():
                    self.scriptThread = None
                else:
                    success = False

        self.ui.actionStop.setEnabled(not success)
        self.ui.actionPause.setEnabled(not success)

        return success

    ###############################   Laser	####################################
    ############################################################################
    def laserClient_connect(self, state):
        if state:
            self.laserClient.initialize()
            alive = self.laserClient.is_alive()
            if alive:
                # self.moco.positionChanged.objSignal.connect(self.mocoPositionChanged)
                self.ui.actionShutter.setEnabled(True)
                self.ui.actionIRShutter.setEnabled(True)
            else:
                self.ui.laserClient_connect.setChecked(False)

        else:
            pass
            # self.laserClient.shutdown()

    def laserOnOff(self, state):
        self.ui.laserOnOff.clearFocus()
        if not self.laserClient.get_is_on():

            self.ui.laserOnOff.setStyleSheet("background-color:#95a2b8;")

            t = threading.Thread(target=self.laserClient.enable)
            t.daemon = True
            t.start()
        else:
            t = threading.Thread(target=self.laserClient.disable)
            t.daemon = True
            t.start()

    def laserSetShutter(self, state=None, wait=True):
        if state is None:
            state = self.ui.actionShutter.isChecked() != 0
        else:
            state = state != 0
        res = self.laserClient.setShutter(state)
        if wait:
            for i in range(100):
                if self.laserClient.shutter() == state:
                    return state
                else:
                    self.time_sleep(0.1)
        return res

    def laserSetIRShutter(self, state=None, wait=True):
        if state is None:
            state = self.ui.actionIRShutter.isChecked() != 0
        else:
            state = state != 0
        res = self.laserClient.setIRshutter(state)
        if wait:
            for i in range(100):
                if self.laserClient.IRshutter() == state:
                    return state
                else:
                    self.time_sleep(0.1)
        return res

    def laserSetWavelength(self, wl=None, closeShutter=False, wait=False):
        # locker = QMutexLocker(self._mutex)
        # self._mutex.lock()
        if wait:
            self.ui.laserWavelength.blockSignals(True)
        shutterState = self.laserClient.shutter()
        if wl is None:
            wl = self.ui.laserWavelength_target.value()
        if closeShutter:
            self.laserClient.setShutter(state=False, wait=True)

        if wl is None:
            return
        self.laserClient.setWavelength(int(round(wl)), wait=wait)
        wl = self.laserClient.wavelength()
        if closeShutter:
            self.laserClient.setShutter(state=shutterState, wait=True)

        if wait:
            # self.ui.laserWavelength.blockSignals(True)
            self.ui.laserWavelength.setValue(wl)
            self._on_laserWavelength_valueChanged(wl)
            self.ui.laserWavelength.blockSignals(False)
        # print(f'SetExWavelength:[{wl}]:Done, {wait}')
        microV_logger.info(f"SetExWavelength:[{wl}]:Done, {wait}")
        # self._mutex.unlock()

    def laserGetWavelength(self):
        # locker = QMutexLocker(self._mutex)
        # self._mutex.lock()
        ex_wl = self.ui.laserWavelength.value()
        # self._mutex.unlock()
        return ex_wl

    def laserGetPrecompensation(self):
        value = self.ui.laserPrecompensation.value()
        # self._mutex.unlock()
        return value

    def laserSetPrecompensation(self, value=None, wait=True):
        if value is None:
            value = self.ui.laserPrecompensation_target.value()
        ex_wl = self.laserGetWavelength()

        Min = self.calibrTools.laserPrecompensation_min(ex_wl)*1.001
        Max = self.calibrTools.laserPrecompensation_max(ex_wl)*0.999
        if value < Min:
            value = Min
        elif value > Max:
            value = Max

        self.laserClient.setPrecompensation(value)
        if wait:
            for i in range(100):
                self.time_sleep(0.1)
                tmp = self.laserClient.precompensation()
                if abs(tmp-value) <= 0.02: break
                self.laserClient.setPrecompensation(value)


            self.ui.laserPrecompensation.setValue(value)

        value = self.ui.laserPrecompensation.value()
        return value

    def laserPrecompensation_moveToCalibr(self, ex_wl=None):
        if ex_wl is None:
            ex_wl = self.laserGetWavelength()

        target = self.calibrTools.laserPrecompensation(ex_wl)
        self.laserSetPrecompensation(target)
        return target

    def laserSavePrecompensation(self, ex_wl=None, value=None, yes=False):
        qm = QtWidgets.QMessageBox
        if not yes:
            ret = qm.question(
                self, "", f"Save this position to calibration curve?", qm.Yes | qm.No
            )
            if ret == qm.No:
                return

        if value is None:
            value = self.laserClient.precompensation()

        if ex_wl is None:
            ex_wl = self.laserGetWavelength()

        calibr = self.calibrTools.insert_laserPrecompensation_calibr(ex_wl, value)
        self.laserPrecompensation_plot_calibr(calibr)

    def laserRemovePrecompensation(self, ex_wl=None):
        if ex_wl is None:
            ex_wl = self.laserGetWavelength()

        calibr = self.calibrTools.remove_from_laserPrecompensation_calibr(
            ex_wl
        )
        self.laserPrecompensation_plot_calibr(calibr)

    def laserPrecompensation_plot_calibr(self, calibr=None):
        if calibr is None:
            calibr = self.calibrTools.store[self.calibrTools.laserPrecompensation_dset_name].data

        self.dataReadySignal.emit(
            [
                {
                    "type": "grid_points",
                    "name": "Laser_precompensation",
                    "info": "",
                    "time": time.time(),
                    "pen": 'r',
                    "symbol": 'o',
                    "symbolBrush": 'm',
                    "symbolSize": 3,


                    "data": [calibr["Ex_wl"], calibr["precompensation"]],
                },
                {
                    "type": "grid_points",
                    "name": "Laser_precompensation_min",
                    "info": "",
                    "time": time.time(),
                    "pen": 'b',
                    "symbol": None,
                    "data": [calibr["Ex_wl"], calibr["min"]],
                },
                {
                    "type": "grid_points",
                    "name": "Laser_precompensation_max",
                    "info": "",
                    "time": time.time(),
                    "pen": 'r',
                    "symbol": None,
                    "data": [calibr["Ex_wl"], calibr["max"]],
                }
            ]
        )

    def on_laserWavelength_valueChanged(self, wl):
        # print('thread')
        # worker = Worker(lambda status_callback=None: self._on_laserWavelength_valueChanged(wl=wl)) # Any other args, kwargs are passed to
        # self.threadpool.start(worker)
        # self._mutex.lock()
        self._on_laserWavelength_valueChanged(wl)
        # self._mutex.unlock()

    def _on_laserWavelength_valueChanged(self, wl=None):
        if wl is None:
            if self.laserClient.is_alive():
                wl = self.laserClient.wavelength()
            else:
                return
        # locker = QMutexLocker(self._mutex)

        print(f"on_laserWavelength_valueChanged:{wl}")
        self.statusBar_ExWavelength.setText(f"Ex:{wl}")
        if self.ui.laserPowerIntens_fixed.isChecked():
            self.laserPowerIntens_set(ex_wl=wl)
        if self.ui.shamrockWavelength_adaptive_centering.isChecked():
            if self.ui.andorCamera_Shamrock_connect.isChecked():
                self.shamrockSetWavelength(ex_wl=wl)
        if self.ui.laserBeamShift_compensate_vs_ex_wavelength.isChecked():
            shift_V, shift_H = self.calibrTools.estimate_laserBeamShift_compensation(wl)
            self.laserBeamShift_V_moveTo(shift_V, ex_wl=wl)
            self.laserBeamShift_H_moveTo(shift_H, ex_wl=wl)
        # self.time_sleep(10)
        if self.ui.laserPrecompensation_use_calibration.isChecked():
            self.laserPrecompensation_moveToCalibr(ex_wl=wl)

        print("on_laserWavelength_valueChanged:DONE")

    def laserPowerIntens_set(self, target=None, mode=None, ex_wl=None):
        if target is None:
            target = self.ui.laserPowerIntens_TUN_target.value()
        if mode is None:
            mode = self.ui.laserControlPower_or_Intens.currentText()
        target_ = target
        if self.calibrTools.setup == "Microscope":
            if self.ui.HWP_Polarization_compensate_power.isChecked():
                compensation = self.ui.HWP_Polarization_TUN_compensationFactor.value()
                target *= compensation

        conf = self.laserPowerInfo_params.childs[-1]
        if ex_wl is None:
            ex_wl = self.laserGetWavelength()
        # print(conf)
        ct = self.calibrTools  # [conf['Setup']]
        config = {}
        for child in self.Setup_params.child(
            "Setup parameters", self.calibrTools.setup
        ).childs:
            config[child.name()] = child.value()

        k = 1
        if (
            self.laserPowerInfo_params.child("TUN/FIX")
            .child("Ratio correction")
            .value()
            == True
        ):
            k = (
                self.laserPowerInfo_params.child("TUN/FIX")
                .child("Ratio correction factor")
                .value()
            )

        if mode == "Average power":
            out = ct.value2angle(
                power_avg=target * k, config=config, Ex_wavelength=ex_wl, IR=False
            )
        elif mode == "Peak intensity":
            out = ct.value2angle(
                intens_peak=target * k, config=config, Ex_wavelength=ex_wl, IR=False
            )
        elif mode == "Average power density":
            out = ct.value2angle(
                vol_power_avg=target * k, config=config, Ex_wavelength=ex_wl, IR=False
            )
        elif mode == "Peak power density":
            out = ct.value2angle(
                vol_power_peak=target * k, config=config, Ex_wavelength=ex_wl, IR=False
            )
        else:
            return

        # print(out)
        angle = out["angle"].m
        shift = self.ui.HWP_TUN_min_power_angle.value() - ct.power2angle_interpLimits[0]
        angle = angle + shift
        out["angle"] = Q_(angle, "deg")

        if not np.isnan(angle):
            self.HWP_TUN_angle_moveTo(angle, wait=True)
        else:
            self.statusBar_setMessage("Can't set intensity")

        if self.ui.laserPowerIntens_linkedOut.isChecked():
            self.laserPowerIntensIR_set(target=target_)

    def laserPowerIntensIR_set(self, target=None, mode=None):

        if target is None:
            target = self.ui.laserPowerIntens_FIX_target.value()
        if mode is None:
            mode = self.ui.laserControlPower_or_Intens.currentText()

        if self.calibrTools.setup == "Microscope":
            if self.ui.HWP_Polarization_compensate_power.isChecked():
                compensation = self.ui.HWP_Polarization_FIX_compensationFactor.value()
                target *= compensation

        params = self.laserPowerInfo_params.childs[-1]
        ex_wl = self.laserGetWavelength()
        # print(params)
        ct = self.calibrTools  # [params['Setup']]

        config = {}
        for child in self.Setup_params.child(
            "Setup parameters", self.calibrTools.setup
        ).childs:
            config[child.name()] = child.value()

        if mode == "Average power":
            out = ct.value2angle(
                power_avg=target, config=config, Ex_wavelength=ex_wl, IR=True
            )
        elif mode == "Peak intensity":
            out = ct.value2angle(
                intens_peak=target, config=config, Ex_wavelength=ex_wl, IR=True
            )
        elif mode == "Average power density":
            out = ct.value2angle(
                vol_power_avg=target, config=config, Ex_wavelength=ex_wl, IR=True
            )
        elif mode == "Peak power density":
            out = ct.value2angle(
                vol_power_peak=target, config=config, Ex_wavelength=ex_wl, IR=True
            )
        else:
            return

        # print(out)
        angle = out["angle"].m
        shift = (
            self.ui.HWP_FIX_min_power_angle.value() - ct.power2angle_interpLimits_IR[0]
        )
        angle = angle + shift
        out["angle"] = Q_(angle, "deg")

        if not np.isnan(angle):
            self.HWP_FIX_angle_moveTo(angle, wait=True)
        else:
            self.statusBar_setMessage("Can't set intensity")

    def on_laserControlPower_or_Intens(self, index):
        mode = self.ui.laserControlPower_or_Intens.currentText()

        if mode == "Average power":
            self.ui.laserPowerIntens_TUN_target.setSuffix("W")
            self.ui.laserPowerIntens_FIX_target.setSuffix("W")
        elif mode == "Peak intensity":
            self.ui.laserPowerIntens_TUN_target.setSuffix("W/cm**2")
            self.ui.laserPowerIntens_FIX_target.setSuffix("W/cm**2")

        elif mode == "Average power density":
            self.ui.laserPowerIntens_TUN_target.setSuffix("W/cm**3")
            self.ui.laserPowerIntens_FIX_target.setSuffix("W/cm**3")

        elif mode == "Peak power density":
            self.ui.laserPowerIntens_TUN_target.setSuffix("W/cm**3")
            self.ui.laserPowerIntens_FIX_target.setSuffix("W/cm**3")

        elif mode == "Average power ratio":
            self.ui.laserPowerIntens_TUN_target.setSuffix("W")
            self.ui.laserPowerIntens_FIX_target.setSuffix("W")

        val = self.ui.laserPowerIntens_TUN_target.value()
        val_IR = self.ui.laserPowerIntens_FIX_target.value()
        # print(val,val_IR)

    def laserCalculate_FIX_Power(self):
        angle = float(self.ui.HWP_FIX_angle.text())
        # import pdb;pdb.set_trace()
        params = self.laserPowerInfo_params.childs[-1]

        ex_wl = 1045  # self.laserGetWavelength()
        # print(params)
        # import pdb;pdb.set_trace()
        ct = self.calibrTools  # [params['Setup']]
        shift = (
            self.ui.HWP_FIX_min_power_angle.value() - ct.power2angle_interpLimits_IR[0]
        )
        angle = angle - shift

        config = {}
        for child in self.Setup_params.child(
            "Setup parameters", self.calibrTools.setup
        ).childs:
            config[child.name()] = child.value()
        # power = ct.angle2power_IR(angle,ex_wl)
        ex_wl = self.laserGetWavelength()
        out = ct.angle2value(angle, ex_wl, config=config, IR=True)
        # print(out)
        self.laserPower_params["FIX"] = out
        self.laserPowerInfo_params.child("Laser parameters", "Fixed output")[
            "Average power"
        ] = (out["P_avg"].to("W").m)
        self.laserPowerInfo_params.child("Laser parameters", "Fixed output")[
            "Peak intensity"
        ] = (out["I_peak"].to("W/cm**2").m)
        self.laserPowerInfo_params.child("Laser parameters", "Fixed output")[
            "Average power density"
        ] = (out["VPower_avg"].to("W/cm**3").m)
        self.laserPowerInfo_params.child("Laser parameters", "Fixed output")[
            "Peak power density"
        ] = (out["VPower_peak"].to("W/cm**3").m)
        self.laserPowerInfo_params.child("Laser parameters", "Fixed output")[
            "Waist radius 1/e**2"
        ] = (out["w0"].to("m").m)
        self.laserPowerInfo_params.child("Laser parameters", "Fixed output")[
            "Confocal parameter"
        ] = (out["z_R"].to("m").m)
        self.laserPowerInfo_params.child("Laser parameters", "Fixed output")[
            "Pulsewidth"
        ] = (out["pulse_dt"].to("s").m)
        self.laserPowerInfo_params.child("Laser parameters", "Fixed output")[
            "Waist diameter FWHM"
        ] = (out["w0"].to("m").m * 1.18)

        state = self.laserPowerInfo_params.saveState()
        self.settings.setValue("laserPowerInfo_params", state)

    def laserCalculate_TUN_Power(self):
        angle = float(self.ui.HWP_TUN_angle.text())
        # import pdb;pdb.set_trace()
        params = self.laserPowerInfo_params.childs[-1]

        ex_wl = self.laserGetWavelength()
        # print(params)
        # import pdb;pdb.set_trace()
        ct = self.calibrTools  # [params['Setup']]
        shift = self.ui.HWP_TUN_min_power_angle.value() - ct.power2angle_interpLimits[0]
        angle = angle - shift

        config = {}
        for child in self.Setup_params.child(
            "Setup parameters", self.calibrTools.setup
        ).childs:
            config[child.name()] = child.value()
        # power = ct.angle2power_IR(angle,ex_wl)
        out = ct.angle2value(angle, ex_wl, config=config, IR=False)
        # print(out)
        self.laserPower_params["TUN"] = out
        self.laserPowerInfo_params.child("Laser parameters", "Tunable output")[
            "Average power"
        ] = (out["P_avg"].to("W").m)
        self.laserPowerInfo_params.child("Laser parameters", "Tunable output")[
            "Peak intensity"
        ] = (out["I_peak"].to("W/cm**2").m)
        self.laserPowerInfo_params.child("Laser parameters", "Tunable output")[
            "Average power density"
        ] = (out["VPower_avg"].to("W/cm**3").m)
        self.laserPowerInfo_params.child("Laser parameters", "Tunable output")[
            "Peak power density"
        ] = (out["VPower_peak"].to("W/cm**3").m)
        self.laserPowerInfo_params.child("Laser parameters", "Tunable output")[
            "Waist radius 1/e**2"
        ] = (out["w0"].to("m").m)
        self.laserPowerInfo_params.child("Laser parameters", "Tunable output")[
            "Confocal parameter"
        ] = (out["z_R"].to("m").m)
        self.laserPowerInfo_params.child("Laser parameters", "Tunable output")[
            "Pulsewidth"
        ] = (out["pulse_dt"].to("s").m)
        self.laserPowerInfo_params.child("Laser parameters", "Tunable output")[
            "Waist diameter FWHM"
        ] = (out["w0"].to("m").m * 1.18)

        state = self.laserPowerInfo_params.saveState()
        self.settings.setValue("laserPowerInfo_params", state)

    ############################################################################
    ###############################   DAQmx	#################################
    def connect_DAQmx(self, state):
        if state:
            self.initDAQmx()
        else:
            try:
                self.DAQmx.close()
            except:
                traceback.print_exc()

    def initDAQmx(self):
        self.DAQmx.ai_channels.add_ai_voltage_chan(
            "Dev1/ai0,Dev1/ai2,Dev1/ai3", max_val=10, min_val=-10
        )
        self.DAQmx.timing.cfg_samp_clk_timing(
            10000, sample_mode=AcquisitionType.CONTINUOUS
        )
        self.DAQmx.control(TaskMode.TASK_COMMIT)
        self.DAQmx.triggers.start_trigger.cfg_anlg_edge_start_trig(
            "Dev1/ai0", trigger_level=1.5
        )

        # self.DAQmx.configure()

    def readDAQmx(self, preview=False, print_dt=False):
        start = time.time()
        with nidaqmx.Task() as master_task:
            # master_task = nidaqmx.Task()
            master_task.ai_channels.add_ai_voltage_chan(
                "Dev1/ai0,Dev1/ai2,Dev1/ai3", max_val=10, min_val=-10
            )
            master_task.timing.cfg_samp_clk_timing(
                100000, sample_mode=AcquisitionType.FINITE
            )
            master_task.control(TaskMode.TASK_COMMIT)
            master_task.triggers.start_trigger.cfg_dig_edge_start_trig("PFI0")
            master_task.start()
            # start = time.time()
            for i in range(100):
                master_data = master_task.read(number_of_samples_per_channel=1000)
                if master_task.is_task_done():
                    break
            r, d, d1 = master_data
            # pp.pprint(master_data)
            # print(time.time()-start,master_task.is_task_done())
        r, d, d1 = master_data
        r = np.array(r)
        d = np.array(d)
        d1 = np.array(d1)

        d = d[int(self.ui.DAQmx_shift.value()) :]
        d1 = d1[int(self.ui.DAQmx_shift.value()) :]
        r = r[0 : int(-self.ui.DAQmx_shift.value())]
        if self.ui.DAQmx_preview.isChecked():
            self.line_DAQmx_sig.setData(d)
            self.line_DAQmx_sig1.setData(d1)
            self.line_DAQmx_ref.setData(r)

        # pp.pprint(master_data)
        if print_dt:
            print("readDAQmx(tdiff):\n", time.time() - start)

        w = r > r.mean()
        out = abs(d[w].mean() - d[~w].mean())
        out1 = abs(d1[w].mean() - d1[~w].mean())
        print(time.time() - start, i)

        return out, out1

    def optimizeDAQmx(self):
        start = time.time()
        with nidaqmx.Task() as master_task:
            # master_task = nidaqmx.Task()
            master_task.ai_channels.add_ai_voltage_chan(
                "Dev1/ai0,Dev1/ai2,Dev1/ai3", max_val=10, min_val=-10
            )

            master_task.timing.cfg_samp_clk_timing(
                100000, sample_mode=AcquisitionType.FINITE
            )

            master_task.control(TaskMode.TASK_COMMIT)

            master_task.triggers.start_trigger.cfg_dig_edge_start_trig("PFI0")

            master_task.start()
            # start = time.time()
            for i in range(100):
                master_data = master_task.read(number_of_samples_per_channel=1000)
                if master_task.is_task_done():
                    break
            r, d, d1 = master_data
            # pp.pprint(master_data)
            # print(time.time()-start,master_task.is_task_done())
        r = np.array(r)
        d = np.array(d)
        data_shift = 0
        shift_array = []
        for data_shift in range(len(r) // 2):
            d_ = d[int(data_shift) :]
            r_ = r[0 : int(-data_shift)]
            w = r_ > r_.mean()
            out = abs(d_[w].mean() - d_[~w].mean())
            shift_array.append(out)

        shift_array = np.array(shift_array)
        self.line_DAQmx_ref.setData(shift_array)
        m = argrelextrema(shift_array, np.greater)[0]
        # pp.pprint(master_data)
        print("optimizeDAQmx(tdiff):\n", time.time() - start, m)
        self.ui.DAQmx_shift.setValue(m[1])

        return m

    ############################################################################
    ###############################   Picoscope	#############################

    def PicoScope_connect(self, state):

        if state:
            self.PicoScope_config["ready_to_read"] = False
            self.initPico()

            # self.ui.PicoScope_connect.setChecked(True)
        else:
            self.PicoScope.close()

    def PicoScope_setConfig(self, config=None):
        if not self.ui.PicoScope_connect.isChecked():
            self.PicoScope_config["ready_to_read"] = False
            self.statusBar_setMessage("Picoscope not connected")
            return
        with self.PicoScope_lock:
            self.PicoScope_config["ready_to_read"] = False
            if not self.ui.PicoScope_connect.isChecked():
                self.ui.PicoScope_connect.setChecked(True)
            self.PicoScope_Vrange_2float = {
                "20mV": 0.02,
                "50mV": 0.05,
                "100mV": 0.1,
                "200mV": 0.2,
                "500mV": 0.5,
                "1V": 1,
                "2V": 2,
                "5V": 5,
                "10V": 10,
                "20V": 20,
            }
            self.PicoScope_config = {}
            self.PicoScope_config[
                "ChA_active"
            ] = self.ui.PicoScope_ChA_active.isChecked()
            self.PicoScope_config[
                "ChB_active"
            ] = self.ui.PicoScope_ChB_active.isChecked()

            self.PicoScope_config["ChA_VRange"] = self.PicoScope_Vrange_2float[
                self.ui.PicoScope_ChA_VRange.currentText()
            ]
            self.PicoScope_config["ChA_Offset"] = self.ui.PicoScope_ChA_offset.value()

            self.PicoScope_config["ChB_VRange"] = self.PicoScope_Vrange_2float[
                self.ui.PicoScope_ChB_VRange.currentText()
            ]
            self.PicoScope_config["ChB_Offset"] = self.ui.PicoScope_ChB_offset.value()
            self.PicoScope_config["pulseFreq"] = float(self.ui.pulseFreq.text())  # Hz

            self.PicoScope_config["sampleInterval"] = float(
                self.ui.PicoScope_sampleInterval.text()
            )
            if (
                self.PicoScope_config["ChA_active"]
                and self.PicoScope_config["ChB_active"]
            ):
                if self.PicoScope_config["sampleInterval"] < 2e-9:
                    self.ui.PicoScope_sampleInterval.setText("2e-9")
            elif self.PicoScope_config["sampleInterval"] < 1e-9:
                self.ui.PicoScope_sampleInterval.setText("1e-9")
            self.PicoScope_config["sampleInterval"] = float(
                self.ui.PicoScope_sampleInterval.text()
            )
            self.PicoScope_config["samplingDuration"] = float(
                self.ui.PicoScope_samplingDuration.text()
            )
            self.PicoScope_config[
                "trigSrc"
            ] = self.ui.PicoScope_TrigSource.currentText()
            self.PicoScope_config["pretrig_s"] = float(self.ui.PicoScope_pretrig.text())
            self.PicoScope_config[
                "trigDirection"
            ] = self.ui.PicoScope_Trig_mode.currentText()  # .upper()
            self.PicoScope_config["trigDelay_s"] = float(self.ui.PicoScope_delay.text())
            self.PicoScope_config[
                "trigThreshold_V"
            ] = self.ui.PicoScope_TrigThreshold.value()
            self.PicoScope_config["n_captures"] = self.ui.PicoScope_n_captures.value()

            if not config is None:
                for key in config:
                    self.PicoScope_config[key] = config[key]
            self.PicoScope.setChannel(
                "A",
                coupling="DC",
                enabled=self.PicoScope_config["ChA_active"],
                VRange=self.PicoScope_config["ChA_VRange"],
                VOffset=self.PicoScope_config["ChA_Offset"],
            )
            self.PicoScope.setChannel(
                "B",
                coupling="DC",
                enabled=self.PicoScope_config["ChB_active"],
                VRange=self.PicoScope_config["ChB_VRange"],
                VOffset=self.PicoScope_config["ChB_Offset"],
            )

            _noSamples = round(
                self.PicoScope_config["samplingDuration"]
                / self.PicoScope_config["sampleInterval"]
                * self.PicoScope_config["n_captures"]
                * 2
            )

            _samples_ok = True
            if _noSamples > 512e6:
                self.PicoScope_config["ready_to_read"] = False
                self.statusBar_setMessage("Not enought memory... try less frames")
                return

            (
                sampleInterval,
                noSamples,
                maxSamples,
            ) = self.PicoScope.setSamplingInterval(
                self.PicoScope_config["sampleInterval"],
                self.PicoScope_config["samplingDuration"],
            )
            self.PicoScope_config["sampleInterval"] = sampleInterval
            self.PicoScope_config["noSamples"] = noSamples

            self.PicoScope_config["pulses_in_frame"] = (
                self.PicoScope_config["samplingDuration"]
                * self.PicoScope_config["pulseFreq"]
            )

            self.PicoScope_config["maxSamples"] = maxSamples

            self.PicoScope_config["trigEnabled"] = True
            if self.PicoScope_config["trigSrc"] == "OFF":
                self.PicoScope_config["trigEnabled"] = False
                self.PicoScope_config["trigSrc"] = 0
            if "frame_duration" in self.PicoScope_config:
                self.PicoScope_config["trigDelay_s"] = (
                    self.PicoScope_config["frame_duration"]
                    - self.PicoScope_config["samplingDuration"]
                    + self.PicoScope_config["pretrig_s"]
                )
                print(
                    "frame_duration",
                    self.PicoScope_config["frame_duration"],
                    "trigDelay_s",
                    self.PicoScope_config["trigDelay_s"],
                )
            self.PicoScope_config["pretrig"] = (
                self.PicoScope_config["pretrig_s"]
                / self.PicoScope_config["sampleInterval"]
                / self.PicoScope_config["noSamples"]
            )  # %
            self.PicoScope_config["trigDelay"] = round(
                self.PicoScope_config["trigDelay_s"]
                / self.PicoScope_config["sampleInterval"]
            )

            self.PicoScope.setSimpleTrigger(
                trigSrc=self.PicoScope_config["trigSrc"],
                threshold_V=self.PicoScope_config["trigThreshold_V"],
                direction=self.PicoScope_config["trigDirection"],
                timeout_ms=5,
                enabled=self.PicoScope_config["trigEnabled"],
                delay=self.PicoScope_config["trigDelay"],
            )

            self.PicoScope_config[
                "samples_per_segment"
            ] = self.PicoScope.memorySegments(self.PicoScope_config["n_captures"])
            self.PicoScope.setNoOfCaptures(self.PicoScope_config["n_captures"])

            print(self.PicoScope_config)
            dt = 1 / self.PicoScope_config["pulseFreq"]

            # ______trigDelay_s____pretrig_s__________samplingDuration___________frame_pulse_delta
            # ==============================
            # ____________________[---------]____________________________________(**)
            # ____________________[++++++++++++++++++++++++++++++++++++++++++++++]
            # |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   | ... pulses

            t_frame = np.linspace(
                0,
                self.PicoScope_config["samplingDuration"],
                self.PicoScope_config["noSamples"],
            )

            t_frame += self.PicoScope_config["trigDelay_s"]
            t_frame_max = t_frame.max()
            if (
                self.PicoScope_config["trigDelay_s"]
                >= self.PicoScope_config["pretrig_s"]
            ):
                t_frame_max -= self.PicoScope_config["pretrig_s"]
            t_frame -= self.PicoScope_config["pretrig_s"]
            frame_pulse_delta = (int(t_frame_max / dt) + 1) * dt - t_frame_max
            print("frame_pulse_delta", frame_pulse_delta)
            if frame_pulse_delta >= dt:
                frame_pulse_delta = 0
            if not "frame_duration" in self.PicoScope_config:
                self.PicoScope_config["frame_duration"] = (
                    t_frame_max + frame_pulse_delta
                )
            print("frame_pulse_delta", frame_pulse_delta)
            print("frame_duration", self.PicoScope_config["frame_duration"])

            if self.PicoScope_config["n_captures"] == 1:
                self.PicoScope_time_grid = t_frame
                self.PicoScope_dataT = t_frame.mean()
            else:
                self.PicoScope_time_grid = np.hstack(
                    [
                        t_frame + self.PicoScope_config["frame_duration"] * i
                        for i in range(self.PicoScope_config["n_captures"])
                    ]
                )
                self.PicoScope_dataT = np.hstack(
                    [
                        t_frame.mean() + self.PicoScope_config["frame_duration"] * i
                        for i in range(self.PicoScope_config["n_captures"])
                    ]
                )

            self.PicoScope_data_raw = np.zeros(
                (
                    2,
                    self.PicoScope_config["n_captures"],
                    self.PicoScope_config["noSamples"],
                ),
                dtype=np.int16,
            )
            if _samples_ok:
                self.PicoScope_config["ready_to_read"] = True
            else:
                self.PicoScope_config["ready_to_read"] = False

            return self.PicoScope_config.copy()

    def initPico(self):

        self.PicoScope.open()
        if self.PicoScope_lock.locked():
            self.PicoScope_lock.release()
        # self.PicoScope.open(self.PicoScope_config['PicoScope_SerialNumber'])
        self.PicoScope_setConfig()

    def readPico(self, save_raw=True):

        if not self.ui.PicoScope_connect.isChecked():
            self.ui.PicoScope_connect.setChecked(True)
            #
            self.time_sleep(5)
        # dataA = np.zeros((self.n_captures, self.samples_per_segment), dtype=np.int16)
        # dataB = np.zeros((self.n_captures, self.samples_per_segment), dtype=np.int16)
        # t1 = time.time()

        # self.PicoScope.runBlock()
        out = None

        try:
            # r = self.PicoScope.capture_prep_block(return_scaled_array=1)
            # self.PicoScope.stop()
            if not self.PicoScope_config["ready_to_read"]:
                # self.PicoScope.close()
                # self.initPico()
                return np.nan, np.nan

            with self.PicoScope_lock:
                t0 = time.time()
                self.PicoScope.runBlock(pretrig=self.PicoScope_config["pretrig"])
                self.PicoScope.waitReady()
                t1 = time.time()
                print("Time to get sweep: " + str(t1 - t0))

            data = []
            data_p2p = []
            vrange = []
            data_format = []
            raw_format = []
            for i, ch in enumerate(["A", "B"]):
                if self.PicoScope_config[f"Ch{ch}_active"]:
                    self.PicoScope.getDataRawBulk(
                        channel=ch, data=self.PicoScope_data_raw[i]
                    )

                    PicoScope_dataV_ = self.PicoScope.rawToV(
                        ch, self.PicoScope_data_raw[i]
                    )
                    threshold_type = getattr(
                        self.ui, f"PicoScope_Ch{ch}_thresholdType"
                    ).currentText()
                    threshold = getattr(self.ui, f"PicoScope_Ch{ch}_threshold").value()

                    if self.ui.PicoScope_byFrames.isChecked():
                        PicoScope_dataV = -PicoScope_dataV_#.mean(axis=0)
                        if threshold_type != "None":
                            if threshold_type[-1] == ">":
                                mask = PicoScope_dataV > threshold
                            else:
                                mask = PicoScope_dataV < threshold

                            data_p2p.append(PicoScope_dataV[mask].mean())
                        else:
                            data_p2p.append(PicoScope_dataV.mean())
                    else:

                        PicoScope_dataV = np.array_split(
                            PicoScope_dataV_,
                            self.PicoScope_config["pulses_in_frame"],
                            axis=1,
                        )
                        PicoScope_dataV = np.array(
                            [np.ptp(sub, axis=1) for sub in PicoScope_dataV]
                        ).T

                        if threshold_type != "None":

                            if threshold_type[-1] == ">":
                                mask = PicoScope_dataV > threshold
                            else:
                                mask = PicoScope_dataV < threshold
                            data_p2p.append(PicoScope_dataV[mask].mean())
                        else:
                            data_p2p.append(PicoScope_dataV.mean())
                    if save_raw:
                        data.append(np.hstack(PicoScope_dataV_))
                    vrange.append(self.PicoScope_config[f"Ch{ch}_VRange"])
                    data_format.append((f"Ch{ch}", "f4"))
                    raw_format.append((f"Ch{ch}", "f4", len(self.PicoScope_time_grid)))

            self.PicoScope_read_done = True
            t2 = time.time()

            dataT = (self.PicoScope_dataT + t0).mean()
            if save_raw:

                out = np.rec.array(
                    [
                        (
                            dataT,
                            tuple(data_p2p),
                            (self.PicoScope_time_grid, *data, tuple(vrange)),
                        )
                    ],
                    dtype=[
                        ("time", "f8"),
                        ("data", data_format),
                        (
                            "raw",
                            [
                                ("time", "f4", len(self.PicoScope_time_grid)),
                                *raw_format,
                                ("VRange", data_format),
                            ],
                        ),
                    ],
                )
            else:
                out = np.rec.array(
                    [(dataT, tuple(data_p2p))],
                    dtype=[("time", "f8"), ("data", data_format)],
                )
            # out = out.reshape(1)
            # if time.time() - self.uiUpdate_t0>0.1:#self.ui.ViewTabs.currentIndex()==1:
            # 	self.uiUpdate_t0 = time.time()

            if self.ui.PicoScope_AutoRange.isChecked():

                indexChA = self.ui.PicoScope_ChA_VRange.currentIndex()
                ChA_VRange = self.PicoScope_Vrange_2float[
                    self.ui.PicoScope_ChA_VRange.currentText()
                ]
                ptpA = abs(np.ptp(self.PicoScope_data_raw[0]))
                if ptpA > 18000 and indexChA < 8:
                    indexChA += 1
                elif ptpA < 100 and indexChA >= 1:
                    indexChA -= 1

                self.ui.PicoScope_ChA_VRange.setCurrentIndex(indexChA)
                ChA_VRange = self.PicoScope_Vrange_2float[
                    self.ui.PicoScope_ChA_VRange.currentText()
                ]
                ChA_Offset = self.ui.PicoScope_ChA_offset.value()

                indexChB = self.ui.PicoScope_ChB_VRange.currentIndex()
                ptpB = abs(np.ptp(self.PicoScope_data_raw[1]))

                if ptpB > 18000 and indexChB < 8:
                    indexChB += 1
                elif ptpB < 100 and indexChB >= 1:
                    indexChB -= 1
                self.ui.PicoScope_ChB_VRange.setCurrentIndex(indexChB)

                ChB_VRange = self.PicoScope_Vrange_2float[
                    self.ui.PicoScope_ChB_VRange.currentText()
                ]
                # ChB_VRange = self.PicoScope_VRange_dict[ChB_VRange]
                ChB_Offset = self.ui.PicoScope_ChB_offset.value()
                with self.PicoScope_lock:
                    self.PicoScope.setChannel(
                        "A",
                        coupling="DC",
                        enabled=self.PicoScope_config["ChA_active"],
                        VRange=ChA_VRange,
                        VOffset=ChA_Offset,
                    )
                    self.PicoScope.setChannel(
                        "B",
                        coupling="DC",
                        enabled=self.PicoScope_config["ChB_active"],
                        VRange=ChB_VRange,
                        VOffset=ChB_Offset,
                    )
                # print(ptpA,ptpB)

            # dataA = self.PicoScope.rawToV(channel="A", dataRaw=dataA)
            # dataB = self.PicoScope.rawToV(channel="B", dataRaw=dataB)

        except Exception as ex:
            microV_logger.error("PicoScope ERROR:", exc_info=ex)
            self.stopActiveThreads()
            return None

        return out

    ############################################################################
    ###############################   Powermeter	 ###########################
    ###############################   Thorlabs PM100 ###########################

    def pm100Connect(self, state):
        if state:
            # rm = pyvisa.ResourceManager()
            # self.power_meter_instr = rm.open_resource(
            # 	'USB0::0x1313::0x8078::P0011470::INSTR',timeout=1)
            # self.power_meter = ThorlabsPM100(inst=self.power_meter_instr)
            try:
                self.power_meter = ThorlabsPM100(
                    ID="USB0::0x1313::0x8078::P0011470::INSTR", timeout=1
                )
                self.power_meter.initialize()
                self.power_meter_correction_wavelength = self.laserGetWavelength()
                # self.power_meter.sense.correction.wavelength = self.power_meter_correction_wavelength
                self.power_meter.correction_wavelength = (
                    self.power_meter_correction_wavelength
                )

            except:
                # self.readPower()
                state = False
        else:
            # self.power_meter_instr.close()
            self.power_meter.shutdown()

        self.ui.pm100Connect.blockSignals(True)
        self.ui.pm100Connect.setChecked(state)
        self.ui.pm100Connect.blockSignals(False)

        if state:
            self.pm100Average()

    def readPower(self):
        if not self.ui.pm100Connect.isChecked():
            self.pm100Connect(state=True)

        if self.demo:
            return np.random.rand(1)[0]

        ex_wl = self.ui.pm100_wavelength.value()
        if self.ui.pm100_auto_wavelength.isChecked():

            if (
                self.ui.actionShutter.isChecked()
                and self.ui.actionIRShutter.isChecked()
            ):
                ex_wl = self.laserGetWavelength()
                ex_wl = (ex_wl + 1045) // 2
            elif self.ui.actionShutter.isChecked():
                ex_wl = self.laserGetWavelength()
            elif self.ui.actionIRShutter.isChecked():
                ex_wl = 1045
            else:
                pass  # self.ui.pm100_wavelength.setValue(1045)
            self.ui.pm100_wavelength.setValue(ex_wl)

        if ex_wl != self.power_meter_correction_wavelength:

            self.power_meter_correction_wavelength = ex_wl
            # self.power_meter.sense.correction.wavelength = self.power_meter_correction_wavelength
            self.power_meter.correction_wavelength = (
                self.power_meter_correction_wavelength
            )
        val = np.nan
        for i in range(10):
            retry = True
            for i in range(100):
                try:
                    # val = self.power_meter.read
                    val = self.power_meter.power
                    self.ui.pm100Power.setText(str(round(val, 6)))
                    retry = False
                    break
                    if self.ui.actionStop.isEnabled():
                        return
                except pyvisa.errors.VisaIOError:
                    self.time_sleep(0.1)
                    # traceback.print_exc()
            if retry:
                self.pm100Connect(state=False)
                self.time_sleep(1)
                self.pm100Connect(state=True)
            else:
                break

        return val

    def pm100Average(self, val=None):
        if not self.ui.pm100Connect.isChecked():
            self.pm100Connect(state=True)
        if val is None:
            val = self.ui.pm100Average.value()
        if not self.ui.pm100Connect.isChecked():
            # self.pm100Connect(state=True)
            self.ui.pm100Connect.setChecked(True)
        else:
            # self.power_meter.sense.average.count = val
            self.power_meter.average_count = val

    ############################################################################
    ###############################   HWP_TUN	################################
    def HWP_TUN_connect(self, state):

        if state:
            if not self.rotationStage_TDC001.is_alive():
                self.rotationStage_TDC001.initialize()
            state = self.rotationStage_TDC001.is_alive()

        else:
            self.rotationStage_TDC001.shutdown()

        self.ui.HWP_TUN_connect.blockSignals(True)
        self.ui.HWP_TUN_connect.setChecked(state)
        self.ui.HWP_TUN_connect.blockSignals(False)
        if state:
            self.updateHWP_TUN()
        return state

    def updateHWP_TUN(self):
        if self.ui.HWP_TUN_connect.isChecked():
            pos = self.rotationStage_TDC001.get_position()
            self.ui.HWP_TUN_angle.setText(str(round(pos, 6)))
            self.statusBar_HWP_TUN_angle.setText(f"TUN:{pos:.2f}{chr(176)}")
            self.laserCalculate_TUN_Power()
            return pos

    def HWP_TUN_go(self):
        if not self.ui.HWP_TUN_connect.isChecked():
            self.HWP_TUN_connect(state=True)

        to_angle = self.ui.HWP_TUN_move_to_angle.value()
        self.rotationStage_TDC001.move_to(to_angle)
        self.updateHWP_TUN()

    def HWP_TUN_go_min_power_angle(self):
        if not self.ui.HWP_TUN_connect.isChecked():
            self.HWP_TUN_connect(state=True)
        to_angle = self.ui.HWP_TUN_min_power_angle.value()
        self.rotationStage_TDC001.move_to(to_angle)
        self.updateHWP_TUN()

    def HWP_TUN_angle_moveTo(self, pos, wait=False):
        if not self.ui.HWP_TUN_connect.isChecked():
            self.HWP_TUN_connect(state=True)
        # print('HWP_TUN_angle_moveTo',pos)
        # if np.isnan(pos): return

        self.rotationStage_TDC001.move_to(pos, wait=wait)
        pos = self.rotationStage_TDC001.get_position()

        self.ui.HWP_TUN_angle.setText(str(round(pos, 6)))

        self.statusBar_HWP_TUN_angle.setText(f"TUN:{pos:.2f}{chr(176)}")
        self.laserCalculate_TUN_Power()
        # print(pos)

    def HWP_TUN_go_home(self):
        if not self.ui.HWP_TUN_connect.isChecked():
            self.HWP_TUN_connect(state=True)
        self.rotationStage_TDC001.move_to()
        self.updateHWP_TUN()

    def HWP_TUN_negative_step(self):
        if not self.ui.HWP_TUN_connect.isChecked():
            self.HWP_TUN_connect(state=True)
        to_angle = -self.ui.HWP_TUN_rel_step.value()
        self.rotationStage_TDC001.move_by(to_angle)
        self.updateHWP_TUN()

    def HWP_TUN_positive_step(self):
        if not self.ui.HWP_TUN_connect.isChecked():
            self.HWP_TUN_connect(state=True)
        to_angle = self.ui.HWP_TUN_rel_step.value()
        self.rotationStage_TDC001.move_by(to_angle)
        self.updateHWP_TUN()

    ############################################################################
    ############################## HWP_FIX  ####################################

    def HWP_FIX_connect(self, state):

        if state:
            if not self.HWP_FIX.is_alive():
                self.HWP_FIX.initialize()
            state = self.HWP_FIX.is_alive()

        else:
            self.HWP_FIX.shutdown()

        self.ui.HWP_FIX_connect.blockSignals(True)
        self.ui.HWP_FIX_connect.setChecked(state)
        self.ui.HWP_FIX_connect.blockSignals(False)
        if state:
            self.updateHWP_FIX()

        return state

    def updateHWP_FIX(self):
        if self.ui.HWP_FIX_connect.isChecked():
            pos = self.HWP_FIX.axes["HWP"].position
            self.ui.HWP_FIX_angle.setText(str(round(pos, 6)))
            self.statusBar_HWP_FIX_angle.setText(f"FIX:{pos:.2f}{chr(176)}")
            self.laserCalculate_FIX_Power()
            return pos

    def HWP_FIX_go(self):
        if not self.ui.HWP_FIX_connect.isChecked():
            self.HWP_FIX_connect(state=True)
        to_angle = self.ui.HWP_FIX_move_to_angle.value()
        self.HWP_FIX.axes["HWP"].move_to(to_angle)
        self.updateHWP_FIX()

    def HWP_FIX_go_min_power_angle(self):
        if not self.ui.HWP_FIX_connect.isChecked():
            self.HWP_FIX_connect(state=True)
        to_angle = self.ui.HWP_FIX_min_power_angle.value()
        self.HWP_FIX.axes["HWP"].move_to(to_angle)
        self.updateHWP_FIX()

    def HWP_FIX_angle_moveTo(self, pos, wait=False):
        if not self.ui.HWP_FIX_connect.isChecked():
            self.HWP_FIX_connect(state=True)
        self.HWP_FIX.axes["HWP"].move_to(pos, wait=wait)
        self.updateHWP_FIX()

    def HWP_FIX_go_home(self):
        if not self.ui.HWP_FIX_connect.isChecked():
            self.HWP_FIX_connect(state=True)
        self.HWP_FIX.axes["HWP"].move_to()
        self.updateHWP_FIX()

    def HWP_FIX_negative_step(self):
        if not self.ui.HWP_FIX_connect.isChecked():
            self.HWP_FIX_connect(state=True)
        step = -self.ui.HWP_FIX_rel_step.value()
        self.HWP_FIX.axes["HWP"].move_by(step)
        self.updateHWP_FIX()

    def HWP_FIX_positive_step(self):
        if not self.ui.HWP_FIX_connect.isChecked():
            self.HWP_FIX_connect(state=True)
        step = self.ui.HWP_FIX_rel_step.value()
        self.HWP_FIX.axes["HWP"].move_by(step)
        self.updateHWP_FIX()

    ############################################################################
    ######################### Thorlabs HWP Polarization ########################

    def HWP_Polarization_connect(self, state):

        if state:

            self.piezoDev_bus.initialize()
            alive = self.piezoDev_bus.is_alive()
            if alive:
                self.updateHWP_Polarization()
                self.commutation_mirror0_updatePosition()
            else:
                self.ui.HWP_Polarization_connect.setChecked(False)
            state = alive
        else:
            self.piezoDev_bus.shutdown()

        self.ui.HWP_Polarization_connect.blockSignals(True)
        self.ui.HWP_Polarization_connect.setChecked(state)
        self.ui.HWP_Polarization_connect.blockSignals(False)
        return state

    def updateHWP_Polarization(self):
        correction = self.ui.HWP_Polarization_angle_correction.value()
        pos = self.piezoDev_bus.axes["HWP"].position - correction
        self.ui.HWP_Polarization_angle.display(float(pos))
        self.statusBar_HWP_Polarization.setText(f"HWP:{pos:.2f}{chr(176)}")

        return pos

    def HWP_Polarization_go(self):
        if not self.ui.HWP_Polarization_connect.isChecked():
            self.HWP_Polarization_connect(state=True)
        # correction = self.ui.HWP_Polarization_angle_correction.value()
        to_angle = self.ui.HWP_Polarization_move_to_angle.value()  # -correction
        self.HWP_Polarization_moveTo(to_angle, wait=True)
        self.updateHWP_Polarization()

    # def HWP_Polarization_go_min_power_angle(self):
    # 	if not self.ui.HWP_Polarization_connect.isChecked():
    # 		self.HWP_Polarization_connect(state=True)
    # 	to_angle = self.ui.HWP_Polarization_min_power_angle.value()
    # 	self.piezoDev_bus.axes['HWP'].move_to(to_angle)
    # 	self.updateHWP_Polarization()

    def HWP_Polarization_moveTo(self, pos=None, wait=False):
        if not self.ui.HWP_Polarization_connect.isChecked():
            self.HWP_Polarization_connect(state=True)

        correction = self.ui.HWP_Polarization_angle_correction.value()
        # pos_prev = self.updateHWP_Polarization()

        wavelength = self.laserGetWavelength()
        if not pos is None:
            pos + correction
        self.piezoDev_bus.axes["HWP"].move_to(pos, wait=wait)
        pos_new = self.updateHWP_Polarization()

        if self.ui.HWP_Polarization_compensate_power.isChecked():
            # prev_correction_TUN, prev_correction_FIX = self.calibrTools.estimate_HWP_Polarization_correctionFactor(pos_prev, wavelength)

            (
                new_correction_TUN,
                new_correction_FIX,
            ) = self.calibrTools.estimate_HWP_Polarization_correctionFactor(
                pos_new, wavelength
            )

            # correction_TUN = new_correction_TUN/prev_correction_TUN
            # correction_FIX = new_correction_FIX/prev_correction_FIX

            power_TUN = (
                self.ui.laserPowerIntens_TUN_target.value()
            )  # self.laserPowerInfo_params.child('Laser parameters').child('Tunable output')['Average power']
            if (
                self.laserPowerInfo_params.child("TUN/FIX")
                .child("Ratio correction")
                .value()
                == True
            ):
                power_TUN /= (
                    self.laserPowerInfo_params.child("TUN/FIX")
                    .child("Ratio correction factor")
                    .value()
                )
            power_FIX = (
                self.ui.laserPowerIntens_FIX_target.value()
            )  # self.laserPowerInfo_params.child('Laser parameters').child('Fixed output')['Average power']

            # new_power_TUN = prev_power_TUN / correction_TUN
            # new_power_FIX = prev_power_FIX / correction_FIX

            self.ui.HWP_Polarization_TUN_compensationFactor.setValue(
                1 / new_correction_TUN
            )
            self.ui.HWP_Polarization_FIX_compensationFactor.setValue(
                1 / new_correction_FIX
            )

            self.abstractMoveTo(power_TUN, axis="HWP_TUN_power")
            if not self.ui.laserPowerIntens_linkedOut.isChecked():
                self.abstractMoveTo(power_FIX, axis="HWP_FIX_power")

        if self.ui.HWP_Polarization_compensateBeamShift.isChecked():
            self._HWP_Polarization_compensate_shift(pos_new)

    def _HWP_Polarization_compensate_shift(self, pos_new):
        if self.ui.HWP_Polarization_compensateBeamShift.isChecked():
            shiftX, shiftY = self.calibrTools.estimate_HWP_Polarization_laserBeamShift(
                pos_new
            )
            print(shiftX, shiftY)
            shiftX_prev = self.ui.HWP_Polarization_Pi_offsetX.value()
            shiftY_prev = self.ui.HWP_Polarization_Pi_offsetY.value()
            self.ui.HWP_Polarization_Pi_offsetX.setValue(shiftX)
            self.ui.HWP_Polarization_Pi_offsetY.setValue(shiftY)
            stage_pos0 = self.piStage.getPosition()
            stage_pos0[0] += shiftX - shiftX_prev
            stage_pos0[1] += shiftY - shiftY_prev
            self.piStage_moveTo([0, 1, 2], target=stage_pos0)

    def HWP_Polarization_go_home(self):
        if not self.ui.HWP_Polarization_connect.isChecked():
            self.HWP_Polarization_connect(state=True)
        self.HWP_Polarization_moveTo()
        self.updateHWP_Polarization()
        # self._HWP_Polarization_compensate_shift()

    def HWP_Polarization_negative_step(self):
        if not self.ui.HWP_Polarization_connect.isChecked():
            self.HWP_Polarization_connect(state=True)
        step = -self.ui.HWP_Polarization_rel_step.value()
        # pos_prev = self.updateHWP_Polarization()
        self.piezoDev_bus.axes["HWP"].move_by(step)
        wavelength = self.laserGetWavelength()
        pos_new = self.updateHWP_Polarization()

        if self.ui.HWP_Polarization_compensate_power.isChecked():
            # prev_correction_TUN, prev_correction_FIX = self.calibrTools.estimate_HWP_Polarization_correctionFactor(pos_prev, wavelength)

            (
                new_correction_TUN,
                new_correction_FIX,
            ) = self.calibrTools.estimate_HWP_Polarization_correctionFactor(
                pos_new, wavelength
            )

            # correction_TUN = new_correction_TUN/prev_correction_TUN
            # correction_FIX = new_correction_FIX/prev_correction_FIX

            power_TUN = (
                self.ui.laserPowerIntens_TUN_target.value()
            )  # self.laserPowerInfo_params.child('Laser parameters').child('Tunable output')['Average power']
            if (
                self.laserPowerInfo_params.child("TUN/FIX")
                .child("Ratio correction")
                .value()
                == True
            ):
                power_TUN /= (
                    self.laserPowerInfo_params.child("TUN/FIX")
                    .child("Ratio correction factor")
                    .value()
                )
            power_FIX = (
                self.ui.laserPowerIntens_FIX_target.value()
            )  # self.laserPowerInfo_params.child('Laser parameters').child('Fixed output')['Average power']

            # new_power_TUN = prev_power_TUN / correction_TUN
            # new_power_FIX = prev_power_FIX / correction_FIX

            self.ui.HWP_Polarization_TUN_compensationFactor.setValue(
                1 / new_correction_TUN
            )
            self.ui.HWP_Polarization_FIX_compensationFactor.setValue(
                1 / new_correction_FIX
            )

            self.abstractMoveTo(power_TUN, axis="HWP_TUN_power")
            if not self.ui.laserPowerIntens_linkedOut.isChecked():
                self.abstractMoveTo(power_FIX, axis="HWP_FIX_power")
        self._HWP_Polarization_compensate_shift(pos_new)

    def HWP_Polarization_positive_step(self):
        if not self.ui.HWP_Polarization_connect.isChecked():
            self.HWP_Polarization_connect(state=True)
        step = self.ui.HWP_Polarization_rel_step.value()
        # pos_prev = self.updateHWP_Polarization()
        self.piezoDev_bus.axes["HWP"].move_by(step)
        wavelength = self.laserGetWavelength()
        pos_new = self.updateHWP_Polarization()

        if self.ui.HWP_Polarization_compensate_power.isChecked():
            # prev_correction_TUN, prev_correction_FIX = self.calibrTools.estimate_HWP_Polarization_correctionFactor(pos_prev, wavelength)

            (
                new_correction_TUN,
                new_correction_FIX,
            ) = self.calibrTools.estimate_HWP_Polarization_correctionFactor(
                pos_new, wavelength
            )

            # correction_TUN = new_correction_TUN/prev_correction_TUN
            # correction_FIX = new_correction_FIX/prev_correction_FIX

            power_TUN = (
                self.ui.laserPowerIntens_TUN_target.value()
            )  # self.laserPowerInfo_params.child('Laser parameters').child('Tunable output')['Average power']
            if (
                self.laserPowerInfo_params.child("TUN/FIX")
                .child("Ratio correction")
                .value()
                == True
            ):
                power_TUN /= (
                    self.laserPowerInfo_params.child("TUN/FIX")
                    .child("Ratio correction factor")
                    .value()
                )
            power_FIX = (
                self.ui.laserPowerIntens_FIX_target.value()
            )  # self.laserPowerInfo_params.child('Laser parameters').child('Fixed output')['Average power']

            # new_power_TUN = prev_power_TUN / correction_TUN
            # new_power_FIX = prev_power_FIX / correction_FIX

            self.ui.HWP_Polarization_TUN_compensationFactor.setValue(
                1 / new_correction_TUN
            )
            self.ui.HWP_Polarization_FIX_compensationFactor.setValue(
                1 / new_correction_FIX
            )

            self.abstractMoveTo(power_TUN, axis="HWP_TUN_power")
            if not self.ui.laserPowerIntens_linkedOut.isChecked():
                self.abstractMoveTo(power_FIX, axis="HWP_FIX_power")

        self._HWP_Polarization_compensate_shift(pos_new)

    ############################################################################
    ######################### arduino linear stage  ############################
    def arduinoStage_init(self):
        #import pdb; pdb.set_trace()
        self._ArduinoStage.initialize()
        success = True
        for ax in self._ArduinoStage.axes:
            self._ArduinoStage.axes[ax].initialize()
            success = success & self._ArduinoStage.axes[ax].is_alive()

        self.ui.arduinoStage_connect.blockSignals(True)
        self.ui.arduinoStage_connect.setChecked(success)
        self.ui.arduinoStage_connect.blockSignals(False)
        if success:
            self.update_arduinoStage()
            self.update_arduinoStage1()
            self.laserBeamShift_H_position_update()
        return success

    def arduinoStage_connect(self, state):

        if state:
            success = self.arduinoStage_init()
        else:
            success = False
            self._ArduinoStage.shutdown()
        return success

    def update_arduinoStage(self):
        # self.time_sleep(5)
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        pos = self.arduinoStage.position
        self.ui.arduinoStage_position.setText(str(round(pos, 5)))
        self.statusBar_arduinoStage.setText(f"Lens:{pos:.2f}")

    def arduinoStage_lock(self, state):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        print(state)
        self.arduinoStage.lock(state == 1)

    def arduinoStage_go(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        state = self.ui.arduinoStage_lock.isChecked()
        if not state:
            self.ui.arduinoStage_lock.setChecked(1)
        target = self.ui.arduinoStage_move_to_position.value()
        self.arduinoStage.move_to(target)
        self.ui.arduinoStage_lock.setChecked(state)
        self.update_arduinoStage()

    def arduinoStage_rewrite_position(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        target = self.ui.arduinoStage_rewrite_position_value.value()
        self.arduinoStage.set_position(target)
        self.update_arduinoStage()

    def arduinoStage_moveTo(self, pos, wait=False):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        state = self.ui.arduinoStage_lock.isChecked()
        if not state:
            self.ui.arduinoStage_lock.setChecked(1)
        self.arduinoStage.move_to(pos, wait=wait)
        self.ui.arduinoStage_lock.setChecked(state)
        # self.time_sleep(5)
        self.update_arduinoStage()

    def arduinoStage_go_home(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        self.arduinoStage.move_to()
        self.update_arduinoStage()

    def arduinoStage_negative_step(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        step = -self.ui.arduinoStage_rel_step.value()
        self.arduinoStage.move_by(step)
        self.update_arduinoStage()

    def arduinoStage_positive_step(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        step = self.ui.arduinoStage_rel_step.value()
        self.arduinoStage.move_by(step)
        self.update_arduinoStage()

    def update_arduinoStage1(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        pos = self.arduinoStage1.position
        self.ui.arduinoStage1_position.setText(str(round(pos, 5)))

    def arduinoStage1_lock(self, state):
        print(state)
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        self.arduinoStage1.lock(state)

    def arduinoStage1_go(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        state = self.ui.arduinoStage1_lock.isChecked()
        if not state:
            self.ui.arduinoStage1_lock.setChecked(1)
        target = self.ui.arduinoStage1_move_to_position.value()
        self.arduinoStage1.move_to(target)
        self.ui.arduinoStage1_lock.setChecked(state)
        self.update_arduinoStage1()

    def arduinoStage1_rewrite_position(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        target = self.ui.arduinoStage1_rewrite_position_value.value()
        self.arduinoStage1.set_position(target)
        self.update_arduinoStage1()

    def arduinoStage1_moveTo(self, pos, wait=False):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        state = self.ui.arduinoStage1_lock.isChecked()
        if not state:
            self.ui.arduinoStage1_lock.setChecked(1)
        self.arduinoStage1.move_to(pos, wait=wait)
        self.ui.arduinoStage1_lock.setChecked(state)
        self.update_arduinoStage1()

    def arduinoStage1_go_home(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        self.arduinoStage1.move_to()
        self.update_arduinoStage1()

    def arduinoStage1_negative_step(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        step = -self.ui.arduinoStage1_rel_step.value()
        self.arduinoStage1.move_by(step)
        self.update_arduinoStage1()

    def arduinoStage1_positive_step(self):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        step = self.ui.arduinoStage1_rel_step.value()
        self.arduinoStage1.move_by(step)
        self.update_arduinoStage1()

    ############################################################################
    ################### Beam position ##########################################
    def laserBeamShift_V_position_update(self):
        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_V.is_alive():
                self.beamShift_V.initialize()
            zero = self.ui.laserBeamShift_V_zero_angle.value()
            ang = self.beamShift_V.axes[
                "beamShift_V"
            ].position  # self.arduinoStage1.position
            print(zero, ang)
            ang -= zero
            wavelength = self.laserGetWavelength()
            delay_delta, T_, pos = calcShiftDelay(5, ang, n_UVFS(wavelength))
            delay_delta0, _, _ = calcShiftDelay(
                5, 0, n_UVFS(self.ui.mocoZeroDelay_correct_ex_wl.value())
            )
            delay_delta01, _, _ = calcShiftDelay(5, 0, n_UVFS(wavelength))
            print(delay_delta, T_, pos, wavelength, ang)
            self.ui.laserBeamShift_V_angle.display(ang)
            self.ui.laserBeamShift_V_position.display(pos)
            self.ui.laserBeamShift_V_delay_delta.setValue(delay_delta)
            self.ui.laserBeamShift_V_delay_delta0.setValue((delay_delta - delay_delta01))

        else:
            if not self.beamShift.is_alive():
                self.beamShift.initialize()
            pos = self.beamShift.axes[
                "beamShift_V"
            ].position  # self.arduinoStage1.position
            print(pos)
            pos = float(pos)
            pos *= 2
            zero = self.ui.laserBeamShift_V_zero_angle.value()

            pos -= zero
            self.ui.laserBeamShift_V_position.display(pos)
            self.ui.laserBeamShift_V_delay_delta.setValue(0)
            self.ui.laserBeamShift_V_delay_delta0.setValue(0)

    def laserBeamShift_V_moveTo_target(self, ex_wl=None):
        # state = self.ui.arduinoStage1_lock.isChecked()
        # if not state:
        # 	self.ui.arduinoStage1_lock.setChecked(1)
        target = self.ui.laserBeamShift_V_target.value()
        by_angle = self.ui.laserBeamShift_V_controlAngle.isChecked()
        self.laserBeamShift_V_moveTo(
            target, wait=True, compensate_delay=False, by_angle=by_angle, ex_wl=ex_wl
        )
        self.laserBeamShift_V_position_update()

    def laserBeamShift_V_moveTo(
        self, pos, wait=True, compensate_delay=False, by_angle=False, ex_wl=None
    ):
        # state = self.ui.arduinoStage1_lock.isChecked()
        # if not state:
        # 	self.ui.arduinoStage1_lock.setChecked(1)
        if ex_wl is None:
            wavelength = self.laserGetWavelength()
        else:
            wavelength = ex_wl
        zero = self.ui.laserBeamShift_V_zero_angle.value()
        print(pos)
        if not BEAMSHIFT_BY_MIRRORS:
            # import pdb; pdb.set_trace()
            if pos is None:
                ang = pos
            elif pos < 0:
                ang = -self.calibrTools.laserBeamShift_shift2angle(-pos, wavelength)
            else:
                ang = self.calibrTools.laserBeamShift_shift2angle(pos, wavelength)
            print(ang)

            if by_angle:
                # self.arduinoStage1.move_to(pos, wait=wait)
                if not pos is None:
                    pos += zero
                self.beamShift_V.axes["beamShift_V"].move_to(pos)
            else:
                # self.arduinoStage1.move_to(ang, wait=wait)
                # self.ui.arduinoStage1_lock.setChecked(state)
                if not ang is None:
                    ang += zero
                print(ang)
                self.beamShift_V.axes["beamShift_V"].move_to(ang)

        else:
            self.beamShift.axes["beamShift_V"].move_to(pos/2)

        self.laserBeamShift_V_position_update()
        if (
            self.ui.laserBeamShift_compensate_delay.isChecked()
            and compensate_delay
            or self.ui.laserBeamShift_V_moveWithZeroDelay.isChecked()
        ):
            self.mocoZeroDelay_move_to_zero()
            # self.abstractMoveTo(self.ui.laserBeamShift_V_delay_delta0.value(),axis='Delay_line_position_zero_relative')

    def laserBeamShift_V_reset(self):
        target = self.ui.laserBeamShift_V_target.value()
        # self.arduinoStage1.set_position(target)
        # self.beamShift_V.axes['beamShift_V'].set_position(target)
        self.laserBeamShift_V_position_update()

    def laserBeamShift_V_moveHome(self):

        self.laserBeamShift_V_moveTo(pos=None, wait=True)
        zero = self.ui.laserBeamShift_V_zero_angle.value()
        # self.laserBeamShift_V_moveTo(pos=zero, wait=True, compensate_delay=True, by_angle=True)

    def laserBeamShift_moveToZero(self):
        shift_V, shift_H = self.estimate_laserBeamShift_compensation(visible=True)
        self.laserBeamShift_V_moveTo(pos=shift_V, wait=True, compensate_delay=True)
        self.laserBeamShift_H_moveTo(pos=shift_H, wait=True, compensate_delay=True)

    def laserBeamShift_V_negativeStep(self):
        step = -self.ui.laserBeamShift_V_step.value()
        by_angle = self.ui.laserBeamShift_V_controlAngle.isChecked()
        if by_angle:
            target = self.ui.laserBeamShift_V_angle.value()
        else:
            target = self.ui.laserBeamShift_V_position.value()
        self.laserBeamShift_V_moveTo(
            pos=target + step, wait=True, compensate_delay=False, by_angle=by_angle
        )

    def laserBeamShift_V_positiveStep(self):
        step = self.ui.laserBeamShift_V_step.value()
        by_angle = self.ui.laserBeamShift_V_controlAngle.isChecked()
        if by_angle:
            target = self.ui.laserBeamShift_V_angle.value()
        else:
            target = self.ui.laserBeamShift_V_position.value()
        self.laserBeamShift_V_moveTo(
            pos=target + step, wait=True, compensate_delay=False, by_angle=by_angle
        )

    def estimate_laserBeamShift_compensation(self, ex_wl=None, visible=False):
        if ex_wl is None:
            ex_wl = self.laserGetWavelength()

        shift_V, shift_H = self.calibrTools.estimate_laserBeamShift_compensation(ex_wl)
        print(f"shift: {shift_V}, {shift_H}")
        if visible:
            self.ui.laserBeamShift_V_position.display(str(shift_V))
            self.ui.laserBeamShift_H_position.display(str(shift_H))

        return shift_V, shift_H

    def laserBeamShift_plot_calibr(self, calibr=None):
        if calibr is None:
            calibr = self.calibrTools.store[
                self.calibrTools.laserBeamShift_dset_name
            ].data

        self.dataReadySignal.emit(
            [
                {
                    "type": "grid_points",
                    "name": "beamShift",
                    "info": "V",
                    "time": time.time(),
                    "pen": 'g',
                    "symbol": "t",
                    "symbolSize": 3,
                    "data": [calibr["ex_wl"], calibr["shift_V"]],
                },
                {
                    "type": "grid_points",
                    "name": "beamShift",
                    "info": "H",
                    "time": time.time(),
                    "pen": 'y',
                    "symbol": "d",
                    "symbolSize": 3,
                    "data": [calibr["ex_wl"], calibr["shift_H"]],
                },
            ]
        )

    def laserBeamShift_save_calibr(self):
        qm = QtWidgets.QMessageBox
        ret = qm.question(self, "", f"Save modified calibration curve?", qm.Yes | qm.No)
        if ret == qm.No:
            return

        ex_wl_V, shift_V = self.linesManager.getLine("Preview","beamShift","V").getData()
        ex_wl_H, shift_H = self.linesManager.getLine("Preview","beamShift","H").getData()

        if len(ex_wl_V) > 2 and len(ex_wl_H) > 2:
            if not np.all(ex_wl_V == ex_wl_H):
                ex_wl = np.union1d(ex_wl_V, ex_wl_H)
                iu_H = interp1d(
                    ex_wl_H, shift_H, bounds_error=False, fill_value="extrapolate"
                )
                iu_V = interp1d(
                    ex_wl_V, shift_V, bounds_error=False, fill_value="extrapolate"
                )
                shift_H = iu_H(ex_wl)
                shift_V = iu_V(ex_wl)
            else:
                ex_wl = ex_wl_H

            # ex_wl = self.laserGetWavelength()
            # shift_V, shift_H = self.calibrTools.estimate_laserBeamShift_compensation(data[:,0])
            calibr = self.calibrTools.save_laserBeamShift_calibr(
                ex_wl=ex_wl, shift_V=shift_V, shift_H=shift_H
            )
            # print(calibr)
            self.laserBeamShift_plot_calibr(calibr)
            # print(calibr)

    def laserBeamShift_add_to_calibr(
        self, shift_V=None, shift_H=None, wavelength=None, yes=False
    ):
        qm = QtWidgets.QMessageBox
        if not yes:
            ret = qm.question(
                self, "", f"Save this position to calibration curve?", qm.Yes | qm.No
            )
            if ret == qm.No:
                return

        if shift_V is None:
            shift_V = self.ui.laserBeamShift_V_position.value()

        if shift_H is None:
            shift_H = self.ui.laserBeamShift_H_position.value()

        if wavelength is None:
            wavelength = self.laserGetWavelength()

        calibr = self.calibrTools.insert_laserBeamShift_calibr(
            wavelength, shift_V, shift_H
        )
        self.laserBeamShift_plot_calibr(calibr)

    def laserBeamShift_remove_from_calibr(self, wavelength=None):
        if wavelength is None:
            wavelength = self.laserGetWavelength()

        calibr = self.calibrTools.remove_from_laserBeamShift_calibr(
            wavelength
        )
        self.laserBeamShift_plot_calibr(calibr)

    def laserBeamShift_calibr_reset(self):
        calibr = self.calibrTools.reset_laserBeamShift_calibr()
        self.laserBeamShift_plot_calibr(calibr)

    def laserBeamShift_calibr_backup(self):
        calibr = self.calibrTools.backup_laserBeamShift_calibr()
        self.laserBeamShift_plot_calibr(calibr)

    ###############################
    def laserBeamShift_H_position_update(self, pos=None):
        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_H.is_alive():
                if not self.arduinoStage_init():
                    return
        else:
            if not self.beamShift.is_alive():
                if not self.beamShift.initialize():
                    return
        zero = self.ui.laserBeamShift_H_zero_position.value()
        if pos is None:
            pos = self.beamShift_H.position  # self.arduinoStage1.position
        pos -= zero
        pos = float(pos)
        # wavelength = self.laserGetWavelength()
        # delay_delta, T_, pos = calcShiftDelay(5, ang, n_UVFS(wavelength))
        # delay_delta0, _, _ = calcShiftDelay(5, 0, n_UVFS(self.ui.mocoZeroDelay_correct_ex_wl.value()))
        # delay_delta01, _, _ = calcShiftDelay(5, 0, n_UVFS(wavelength))
        # print(delay_delta, T_, pos, wavelength, ang)
        self.ui.laserBeamShift_H_position.display(pos)
        self.ui.laserBeamShift_H_delay_delta.setValue(pos)
        self.ui.laserBeamShift_H_delay_delta0.setValue(pos)

        # self.statusBar_arduinoStage1.setText(f'[Knife:{pos:.2f}]')

    # def arduinoStage1_lock(self, state):
    # 	print(state)
    # 	self.arduinoStage1.lock(state)

    def laserBeamShift_H_moveTo_target(self, ex_wl=None):
        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_H.is_alive():
                if not self.arduinoStage_init():
                    return
        else:
            if not self.beamShift.is_alive():
                if not self.beamShift.initialize():
                    return
        target = self.ui.laserBeamShift_H_target.value()
        self.laserBeamShift_H_moveTo(
            target, wait=True, compensate_delay=True, ex_wl=ex_wl
        )

    def laserBeamShift_H_moveTo(
        self, pos, wait=True, compensate_delay=False, ex_wl=None
    ):

        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_H.is_alive():
                if not self.arduinoStage_init():
                    return
        else:
            if not self.beamShift.is_alive():
                if not self.beamShift.initialize():
                    return
        if ex_wl is None:
            wavelength = self.laserGetWavelength()
        else:
            wavelength = ex_wl
        zero = self.ui.laserBeamShift_H_zero_position.value()
        if not pos is None:
            pos += zero
        self.beamShift_H.move_to(pos, wait=wait)
        self.laserBeamShift_H_position_update()
        if (
            self.ui.laserBeamShift_compensate_delay.isChecked()
            and compensate_delay
            or self.ui.laserBeamShift_H_moveWithZeroDelay.isChecked()
        ):
            self.mocoZeroDelay_move_to_zero()
            # self.abstractMoveTo(self.ui.laserBeamShift_V_delay_delta0.value(),axis='Delay_line_position_zero_relative')
        self.laserBeamShift_H_position_update()

    def laserBeamShift_H_reset(self):
        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_H.is_alive():
                if not self.arduinoStage_init():
                    return
        else:
            if not self.beamShift.is_alive():
                if not self.beamShift.initialize():
                    return
        target = self.ui.laserBeamShift_H_target.value()
        self.beamShift_H.set_position(target)
        self.laserBeamShift_H_position_update()

    def laserBeamShift_H_moveHome(self):
        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_H.is_alive():
                if not self.arduinoStage_init():
                    return
        else:
            if not self.beamShift.is_alive():
                if not self.beamShift.initialize():
                    return
        self.laserBeamShift_H_moveTo(pos=None, wait=True)
        zero = self.ui.laserBeamShift_H_zero_position.value()
        # self.laserBeamShift_V_moveTo(pos=zero, wait=True, compensate_delay=True, by_angle=True)

    def laserBeamShift_H_moveToZero(self):
        pass
        # shift_V, shift_H =self.estimate_laserBeamShift_compensation(visible=True)
        # self.laserBeamShift_H_moveTo(pos=shift_H, wait=True, compensate_delay=True)

    def laserBeamShift_H_negativeStep(self):
        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_H.is_alive():
                if not self.arduinoStage_init():
                    return
        else:
            if not self.beamShift.is_alive():
                if not self.beamShift.initialize():
                    return
        step = -self.ui.laserBeamShift_H_step.value()
        self.beamShift_H.move_by(step, wait=True)
        self.laserBeamShift_H_position_update()
        if self.ui.laserBeamShift_H_moveWithZeroDelay.isChecked():
            self.mocoZeroDelay_move_to_zero()

    def laserBeamShift_H_positiveStep(self):
        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_H.is_alive():
                if not self.arduinoStage_init():
                    return
        else:
            if not self.beamShift.is_alive():
                if not self.beamShift.initialize():
                    return
        step = self.ui.laserBeamShift_H_step.value()
        self.beamShift_H.move_by(step, wait=True)
        self.laserBeamShift_H_position_update()
        if self.ui.laserBeamShift_H_moveWithZeroDelay.isChecked():
            self.mocoZeroDelay_move_to_zero()

    def laserBeamShift_H_lock(self, state):
        if not BEAMSHIFT_BY_MIRRORS:
            if not self.beamShift_H.is_alive():
                if not self.arduinoStage_init():
                    return
        else:
            if not self.beamShift.is_alive():
                if not self.beamShift.initialize():
                    return
        print(state)
        self.beamShift_H.lock(state == 1)

    ############################################################################
    ######################### DAQmx linear stage  ############################

    # def arduinoStage1_connect(self,state):
    # 	if state:
    # 		self.ui.arduinoStage_connect.blockSignals(True)
    # 			#self.ui.arduinoStage_connect.setChecked(True)
    # 			#self.ui.arduinoStage_connect.blockSignals(False)
    # 		#self._arduinoStage1.initialize()
    # 		#self.arduinoStage1.initialize()
    # 		self._ArduinoStage.initialize()
    # 		self.arduinoStage.initialize()

    # 		alive = self.arduinoStage1.is_alive()

    # 		if alive:
    # 			self.initarduinoStage1()
    # 			#self.ui.arduinoStage_connect.blockSignals(True)
    # 			#self.ui.arduinoStage_connect.setChecked(True)
    # 			#self.ui.arduinoStage_connect.blockSignals(False)
    # 			self.initArduinoStage()

    # 		else:
    # 			self.ui.arduinoStage1_connect.setChecked(False)
    # 			#self.ui.arduinoStage_connect.blockSignals(True)
    # 			#self.ui.arduinoStage_connect.setChecked(False)
    # 			#self.ui.arduinoStage_connect.blockSignals(False)

    # 	else:
    # 		self._arduinoStage1.shutdown()
    # 		self._ArduinoStage.shutdown()
    # 		#self.ui.arduinoStage_connect.blockSignals(True)
    # 		#self.ui.arduinoStage_connect.setChecked(False)
    # 		#self.ui.arduinoStage_connect.blockSignals(False)

    ############################################################################
    ###############################   Commutation mirros	####################
    def commutation_mirror0_set(self, state=None, index=None):
        if index is None:
            index = self.ui.commutation_mirror0_out.currentIndex()
        angles = np.fromstring(self.ui.commutation_mirror0_positions.text(), sep=",")
        self.commutation_mirror0_moveTo(angles[index])
        self.commutation_mirror0_updatePosition()

    def commutation_mirror0_reset(self):
        self.commutation_mirror0_home()

    def commutation_mirror0_moveTo(self, target=None, wait=False):
        if target is None:
            target = 0
        self.piezoDev_bus.axes["mirror0"].move_to(target)
        self.commutation_mirror0_updatePosition()

    def commutation_mirror0_home(self):
        self.commutation_mirror0_moveTo()
        self.commutation_mirror0_updatePosition()

    def commutation_mirror0_updatePosition(self):

        pos = np.nan
        try:

            if not self.piezoDev_bus.axes["mirror0"].is_alive():
                return
            pos = self.piezoDev_bus.axes["mirror0"].position
            pos_set = np.fromstring(
                self.ui.commutation_mirror0_positions.text(), sep=","
            )
            index = abs(pos_set - pos).argmin()
            self.ui.commutation_mirror0_out.setCurrentIndex(index)
            self.ui.commutation_mirror0_position.setValue(pos)
            self.toolBar_commutation_mirror0.blockSignals(True)
            self.toolBar_commutation_mirror0.setCurrentIndex(index)
            self.toolBar_commutation_mirror0.blockSignals(False)

            if self.ui.ThorCamera_lightAutoOnOff.isChecked():
                self.ThorCamera_light(self.toolBar_commutation_mirror0.currentText() == 'Camera')

        except Exception as ex:
            microV_logger.error("Mirror0", exc_info=ex)
        return pos

    # def commutation_mirror1_set(self,state=None, index=None):
    # 	if index is None:
    # 		index = self.ui.commutation_mirror1_out.currentIndex()
    # 	#self.arduinoStage1_moveTo(index*11.25, wait=True)
    # 	angles = np.fromstring(self.ui.commutation_mirror1_positions.text(),sep=',')
    #
    # 	self.commutation_mirror1_moveTo(angles[index], wait=True)
    # 	self.commutation_mirror1_updatePosition()
    #
    # def commutation_mirror1_reset(self):
    # 	pos = self.ui.commutation_mirror1_position.value()
    # 	self._ArduinoStage.axes['commutation_mirror1'].set_position(pos)
    # 	self.commutation_mirror1_updatePosition()
    #
    # def commutation_mirror1_moveTo(self,target=None,wait=False):
    # 	#if target is None:
    # 	#	target = 0
    # 	self._ArduinoStage.axes['commutation_mirror1'].move_to(target, wait=True)
    # 	self.commutation_mirror1_updatePosition()
    #
    # def commutation_mirror1_home(self):
    # 	#self.arduinoStage1_moveTo(pos=None,wait=True)
    # 	self.commutation_mirror1_moveTo(wait=True)
    # 	self.commutation_mirror1_updatePosition()
    #
    # def commutation_mirror1_updatePosition(self):
    # 	pos = np.nan
    # 	try:
    # 		#if not self.arduinoStage1.is_alive(): return
    # 		#pos = self.arduinoStage1.position
    # 		if not self._ArduinoStage.is_alive(): return
    # 		pos = self._ArduinoStage.axes['commutation_mirror1'].position
    # 		pos_set =  np.fromstring(self.ui.commutation_mirror1_positions.text(),sep=',')
    # 		index = abs(pos_set - pos).argmin()
    # 		self.ui.commutation_mirror1_out.setCurrentIndex(index)
    # 		self.ui.commutation_mirror1_position.setValue(pos)
    # 	except Exception as ex:
    # 		microV_logger.error('Mirror1',exc_info=ex)
    # 	return pos
    #
    # def commutation_mirror2_set(self,state=None, index=None):
    # 	if index is None:
    # 		index = self.ui.commutation_mirror2_out.currentIndex()
    # 	#self.arduinoStage_lock(True)
    # 	#self.arduinoStage_moveTo(index*12, wait=True)
    # 	#self.arduinoStage_lock(False)
    # 	self.commutation_mirror2_moveTo(index*10, wait=True)
    # 	self.commutation_mirror2_updatePosition()
    #
    # def commutation_mirror2_reset(self):
    # 	pos = self.ui.commutation_mirror2_position.value()
    # 	self._ArduinoStage.axes['commutation_mirror2'].set_position(pos)
    # 	self.commutation_mirror2_updatePosition()
    #
    # def commutation_mirror2_moveTo(self,target=None,wait=False):
    # 	if target is None:
    # 		target = 0
    # 	self._ArduinoStage.axes['commutation_mirror2'].move_to(target, wait=True)
    # 	self.commutation_mirror1_updatePosition()
    #
    #
    # def commutation_mirror2_home(self):
    # 	#self.arduinoStage_lock(True)
    # 	#self.arduinoStage_moveTo(pos=None,wait=True)
    # 	#self.arduinoStage_lock(False)
    # 	self.commutation_mirror2_moveTo(wait=True)
    # 	self.commutation_mirror2_updatePosition()
    #
    # def commutation_mirror2_updatePosition(self):
    # 	pos = np.nan
    # 	try:
    # 		#if not self.arduinoStage.is_alive(): return
    # 		#pos = self.arduinoStage.position
    # 		if not self._ArduinoStage.is_alive(): return
    # 		pos = self._ArduinoStage.axes['commutation_mirror2'].position
    # 		pos_set = np.array([0,12])
    # 		index = abs(pos_set - pos).argmin()
    # 		self.ui.commutation_mirror2_out.setCurrentIndex(index)
    # 		self.ui.commutation_mirror2_position.setValue(pos)
    # 	except Exception as ex:
    # 		microV_logger.error('Mirror2',exc_info=ex)
    # 	return pos

    ############################################################################
    ###############################   PiNanoCube	############################
    def piStage_connect(self, state):
        if state:
            self.initPiStage()
        else:
            self.piStageLiveTimer.stop()
            self.piStage.close()

    def initPiStage(self):
        try:
            res = self.piStage.connect()
            if not res:
                return False
            self.piStage.getAxesNames()
            self.piStage.setServoControl()
            vel = self.ui.Pi_Velocity.value()
            self.piStage.setVelocity(values=[vel] * 3)
            # self.piStage.getVelocity()
            # print(self.piStage.DCO([1,1,1],b'1 2 3'))
            # print(self.piStage.MOV(self.ui.Pi_X_move_to.value(),axis=1,waitUntilReady=True))
            # self.time_sleep(0.2)
            # print(self.piStage.MOV(self.ui.Pi_X_move_to.value(),axis=2,waitUntilReady=True))
            # self.time_sleep(0.2)
            # print(self.piStage.MOV(self.ui.Pi_X_move_to.value(),axis=3,waitUntilReady=True))
            pos = self.piStage.getPosition()
            if pos is None:
                microV_logger.error("Can't get position from piStage")
                return
            if sum(pos < -100) or sum(pos > 130):
                logging.info(f"PiStage:autoZero: {self.piStage.autoZero()}")
            pos = self.piStage.getPosition()
            self.setUiPiPos(pos=pos)
            self.time_sleep(1)
            self.piStageLiveTimer.start(300)
            self.ui.piStage_connect.blockSignals(True)
            self.ui.piStage_connect.setChecked(True)
            self.ui.piStage_connect.blockSignals(False)
            self.Pi_set_tiltCompensation()
            return True
        except:
            self.ui.piStage_connect.blockSignals(True)
            self.ui.piStage_connect.setChecked(False)
            self.ui.piStage_connect.blockSignals(False)
            return False

    def setUiPiPos(self, pos):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        self.ui.Pi_XPos.setText(f"{pos[0]:+3.3f}")
        self.ui.Pi_YPos.setText(f"{pos[1]:+3.3f}")
        self.ui.Pi_ZPos.setText(f"{pos[2]:+3.3f}")
        self.statusBar_Position_X.setText(f"{pos[0]:+3.3f}")
        self.statusBar_Position_Y.setText(f"{pos[1]:+3.3f}")
        self.statusBar_Position_Z.setText(f"{pos[2]:+3.3f}")

        self.dataReadySignal.emit(
            [
                {
                    "type": "piStage_position",
                    "name": "A",
                    "time": time.time(),
                    "data": [[pos[0]], [pos[1]]],
                },
                {
                    "type": "piStage_position",
                    "name": "B",
                    "time": time.time(),
                    "data": [[pos[0]], [pos[1]]],
                },
                {
                    "type": "piStage_position",
                    "name": "cam",
                    "time": time.time(),
                    "data": [[pos[0]], [pos[1]]],
                }
                # [[pos[0]+self.piStage_position_cam_shift[0]],[pos[1]+self.piStage_position_cam_shift[1]]]}
            ]
        )
        # self.piStage_position.setData(x=[pos[0]],y=[pos[1]])
        # self.piStage_POSITION['B'].setData(x=[pos[0]],y=[pos[1]])
        self.ui.zSlider.blockSignals(True)
        self.ui.zSlider.setValue(int(pos[2] * 1000))
        self.ui.zSlider.blockSignals(False)

    def setPiStageZPosition(self, index):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        print(self.piStage_moveTo(2, target=[index / 1000], wait=True))
        pos = self.piStage.getPosition()
        self.setUiPiPos(pos=pos)

    def Pi_X_go(self):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        pos = self.ui.Pi_X_move_to.value()
        print(self.piStage_moveTo(0, target=[pos], wait=True))
        pos = self.piStage.getPosition()
        self.setUiPiPos(pos=pos)

    def Pi_Y_go(self):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        pos = self.ui.Pi_Y_move_to.value()
        print(self.piStage_moveTo(1, target=[pos], wait=True))
        pos = self.piStage.getPosition()
        self.setUiPiPos(pos=pos)

    def Pi_Z_go(self):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        pos = self.ui.Pi_Z_move_to.value()
        print(self.piStage_moveTo(2, target=[pos], wait=True))
        pos = self.piStage.getPosition()
        self.setUiPiPos(pos=pos)

    def Pi_XYZ_50mkm(self):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        print(self.piStage_moveTo(axes=[0, 1, 2], target=[50, 50, 50], wait=True))
        pos = self.piStage.getPosition()
        self.setUiPiPos(pos=pos)

    def piStage_moveTo(self, axis=None, target=None, wait=True, corrected=False):

        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        if target is None:
            pos = self.piStage.getPosition()

        # offset = np.zeros(3)
        # if self.ui.HWP_Polarization_compensateBeamShift.isChecked():
        # 	offset += np.array([getattr(self.ui, 'HWP_Polarization_Pi_offset'+ax).value() for ax in 'XYZ'])
        # target = np.array(target) + offset[axis]

        # print(target,type(target),target.dtype)
        self.piStage.move(axis, values=target, wait=wait)
        pos = self.piStage.getPosition()
        return pos

    def piStage_moveBy(self, axis=None, target=None, wait=True, corrected=False):

        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        if target is None:
            pos = self.piStage.getPosition()

        self.piStage.move_by(axis, values=target, wait=wait)
        pos = self.piStage.getPosition()
        return pos

    # def piStage_getPosition(self, corrected=False):
    # 	if not self.piStage.isAlive():
    # 		self.initPiStage()
    # 		if not self.piStage.isAlive(): return
    # 	pos = self.piStage.getPosition()
    # 	self.setUiPiPos(pos=pos)
    # 	if corrected:
    # 		offset = np.zeros(3)
    # 		if self.ui.HWP_Polarization_compensateBeamShift.isChecked():
    # 			offset += np.array([getattr(self.ui,'Pi_offset'+ax).value() for ax in 'XYZ'])
    # 		pos -= offset
    #
    # 	return pos

    # def piStage_moveTo_corrected(self,state=1, axis=None, target=None):
    # 	if not self.piStage.isAlive():
    # 		self.initPiStage()
    # 		if not self.piStage.isAlive(): return
    # 	if axis is None and target is None:
    # 		axis=[0,1,2]
    # 		target = self.piStage.getPosition()
    #
    # 	shift = self.estimate_piStageWaistShift_shift()
    #
    # 	self.piStage.move(axis,values=target+shift,wait=True)
    # 	pos = self.piStage.getPosition()
    # 	self.setUiPiPos(pos=pos)

    def Pi_autoZero(self):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        print(self.piStage.autoZero(wait=True))
        pos = self.piStage.getPosition()
        self.setUiPiPos(pos=pos)

    def Pi_Set(self):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        vel = self.ui.Pi_Velocity.value()
        self.piStage.setVelocity(values=[vel] * 3)
        print("VEL:", self.piStage.getVelocity())

    def Pi_set_tiltCompensation(self):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        xa = self.ui.Pi_X_angle.value()
        ya = self.ui.Pi_Y_angle.value()
        za = self.ui.Pi_Z_angle.value()

        self.piStage.set_tiltCompensation(xa, ya, za)

    def onPiStageLiveTimer(self):
        if not self.piStage.isAlive():
            self.initPiStage()
            if not self.piStage.isAlive():
                return
        pos = self.piStage.getPosition()
        # print(pos)
        self.setUiPiPos(pos=pos)
        self.ui.zSlider.setToolTip(f"Z: {pos[2]:+.4f}")

    def estimate_piStageWaistShift_shift(self, ex_wl=None, visible=False):
        if ex_wl is None:
            ex_wl = self.laserGetWavelength()

        shift = self.calibrTools.estimate_piStageWaistShift_shift(ex_wl)
        if visible:
            self.ui.piStageWaistShift_correct_shift.setText(
                ",".join([str(i) for i in shift])
            )
        return shift

    def piStageWaistShift_plot_calibr(self, calibr=None):
        if calibr is None:
            calibr = self.calibrTools.store[self.piStageWaistShift_dset_name].data
        pass
        # self.mouse_last_pos_arr = np.array([
        # 		calibr['ex_wl'],
        # 		calibr['delay_0_pos']
        # 		]).T

        # self.dataReadySignal.emit([
        # 			{'type':'grid_points', 'name':f'test', 'index':0, 'time': time.time(),
        # 			'data': [self.mouse_last_pos_arr[:,0],self.mouse_last_pos_arr[:,1]]}])

    def piStageWaistShift_save_calibr(self):
        pass
        # qm = QtWidgets.QMessageBox
        # ret = qm.question(self,'', f"Save modified calibration curve?", qm.Yes | qm.No)
        # if ret == qm.No:
        # 	return
        # data = self.mouse_last_pos_arr
        # if len(data)>2:
        # 	calibr = self.calibrTools.save_ZeroDelay_calibr(ex_wl=data[:,0],delay_0_pos=data[:,1])
        # 	self.mocoZeroDelay_plot_calibr(calibr)

    def piStageWaistShift_correct_calibr(self):
        ex_wl = self.ui.mocoZeroDelay_correct_ex_wl.value()
        pos = self.ui.mocoZeroDelay_correct_pos.value()

        calibr = self.calibrTools.correct_piStageWaistShift_calibr(
            ex_wl=ex_wl, true_pos=pos
        )
        self.piStageWaistShift_plot_calibr(calibr)

    def piStageWaistShift_reset_calibr(self):
        calibr = self.calibrTools.reset_piStageWaistShift_calibr()
        self.piStageWaistShift_plot_calibr(calibr)

    def piStageWaistShift_add_to_calibr(self, pos=None, wavelength=None):
        pass
        # qm = QtWidgets.QMessageBox
        # ret = qm.question(self,'', f"Save this position to calibration curve?", qm.Yes | qm.No)
        # if ret == qm.No:
        # 	return

        # if pos is None:
        # 	pos = self.moco.axes['delay'].position

        # if wavelength  is None:
        # 	ex_wl = self.laserGetWavelength()

        # calibr = self.calibrTools.insert_ZeroDelay_calibr(ex_wl,pos)
        # self.mocoZeroDelay_plot_calibr(calibr)

    ############################################################################
    ###############################   ThorCamera	############################
    def ThorCamera_connect(self, state):

        if state:
            self.ThorCamera_bg = np.zeros((1024, 1024))
            exposure = self.ui.ThorCamera_exposure.value()
            if self.demo:
                self.thorcam = ThorCamera.CameraDummy()
            elif self.ui.ThorCamera_source.currentText() == "ThorCamera":
                self.thorcam = ThorCamera.Camera()
            else:
                self.thorcam = ThorCamera.CameraOpenCV()

            self.thorcam.open(
                bit_depth=8,
                roi_shape=(1024, 1024),
                roi_pos=(0, 0),
                camera="ThorCam FS",
                exposure=exposure,
                frametime=10.0,
            )

            self.thorcam.start_continuous_capture()
            exposure = self.thorcam.exposure
            self.statusBar_exposure.setText(f"Exp:{exposure:0.4f}")
            self.ui.ThorCamera_source.setEnabled(False)
            self.ui.ThorCamera_connect.blockSignals(True)
            self.ui.ThorCamera_connect.setChecked(True)
            self.ui.ThorCamera_connect.blockSignals(False)

        else:
            self.thorcam.stop_live_capture()
            self.thorcam.close()
            self.ui.ThorCamera_source.setEnabled(True)

    def ThorCamera_exposure(self, exposure):
        if not self.ui.ThorCamera_connect.isChecked() or self.thorcam is None:
            self.ThorCamera_connect(state=True)
        if exposure:
            self.thorcam.set_exposure(exposure)
        self.ui.ThorCamera_exposure.blockSignals(True)
        exposure = self.thorcam.exposure
        self.ui.ThorCamera_exposure.setValue(exposure)
        self.ui.ThorCamera_exposure.blockSignals(False)
        self.statusBar_exposure.setText(f"Exp:{exposure:0.4f}")
        return exposure

    def ThorCamera_setLightLevel(self, value):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        if self.ui.ThorCamera_light.isChecked():
            self._ArduinoStage.setLight(value)

    def ThorCamera_light(self, state):
        if not self._ArduinoStage.is_alive():
            if not self.arduinoStage_init():
                return
        if state:
            value = self.ui.ThorCamera_lightLevel.value()
            self._ArduinoStage.setLight(value)

        else:
            self._ArduinoStage.setLight(0)
        self.toolBar_light.blockSignals(True)
        self.toolBar_light.setChecked(state)
        self.toolBar_light.setText("ON" if state else "OFF")
        self.toolBar_light.blockSignals(False)

    def ThorCamera_resetBG(self):
        self.ThorCamera_bg[:] = 0

    def ThorCamera_getBG(self):
        if not self.ui.ThorCamera_connect.isChecked() or self.thorcam is None:
            self.ThorCamera_connect(state=True)
        # self.ThorCamera_bg = 0
        # self.thorcam.start_continuous_capture()
        self.ThorCamera_bg = self.ThorCamera_getImage()

    def ThorCamera_getImage(self):

        if not self.ui.ThorCamera_connect.isChecked() or self.thorcam is None:
            self.ThorCamera_connect(state=True)



        img = self.thorcam.get_image().astype(int)
        if self.ui.ThorCamera_average.value()>1:
            for i in range(self.ui.ThorCamera_average.value()-1):
                img += self.thorcam.get_image().astype(int)
        img = (img.T/self.ui.ThorCamera_average.value()).astype(np.int16)

        reflectV = 1
        if self.ui.ThorCamera_reflectV.isChecked():
            reflectV = -1
        reflectH = 1
        if self.ui.ThorCamera_reflectH.isChecked():
            reflectH = -1
        img = img[::reflectV, ::reflectH]
        if self.ui.ThorCamera_transpose.isChecked():
            img = img.T
        img = scipy.ndimage.rotate(img, self.ui.ThorCamera_angle.value(), reshape=False)
        # img -= self.ThorCamera_bg
        return img

    def ThorCamera_getData(self, save_raw=True):

        if save_raw:
            dtype = [
                ("time", "f8"),
                (
                    "data",
                    [
                        ("ptp", "f4"),
                        ("mean", "f4"),
                        ("std", "f4"),
                        ("ptpROI", "f4"),
                        ("meanROI", "f4"),
                        ("stdROI", "f4"),
                        # ("contrastROI", "f4"),
                        # ("sharpnessROI", "f4"),
                    ],
                ),
                (
                    "img",
                    [
                        ("image", np.int16, (1024, 1024)),
                        ("shift", "f8", (2,)),
                        ("XYZ", "f8", (3,)),
                        ("scale", "f4", (2,)),
                        ("exposure", "f4"),
                    ],
                ),
            ]
        else:
            dtype = [
                ("time", "f8"),
                (
                    "data",
                    [
                        ("ptp", "f4"),
                        ("mean", "f4"),
                        ("std", "f4"),
                        ("ptpROI", "f4"),
                        ("meanROI", "f4"),
                        ("stdROI", "f4"),
                        # ("contrastROI", "f4"),
                        # ("sharpnessROI", "f4"),
                    ],
                ),
            ]
        data = np.zeros(1, dtype=dtype)
        position = self.piStage.getPosition()
        scale = self.ui.ThorCamera_scale.value()
        # if self.thorcam.live:
        img_ = self.ThorCamera_getImage()


        if self.demo:
            pos = self.piStage.getPosition()
            X, Y, Z = self.andorCCD.demo_getPosition()
            X = (X - pos[0]) / scale
            Y = (Y - pos[1]) / scale
            mask = (X >= 0) & (X <= 1024) & (Y >= 0) & (Y <= 1024)
            shiftX, shiftY = self.calibrTools.estimate_HWP_Polarization_laserBeamShift(
                self.ui.HWP_Polarization_angle.value()
            )
            X = X[mask]
            Y = Y[mask]
            img_[X.astype(int), Y.astype(int)] = 255
            img_ = gaussian_filter(img_, (3, 3))

        img = img_.astype(np.int16) - self.ThorCamera_bg.astype(np.int16)

        pos = position[:2] - self.piStage_position_beamSpot_position

        if np.isnan(pos).sum() >= 1:
            pos = (0, 0)
        self.piStage_position_cam_static_pos = pos
        self.settings.setValue("piStage_position_cam_static_pos", pos)
        self.settings.setValue("piStage_position_beamSpot_position", self.piStage_position_beamSpot_position)

        conf = {
            "type": "img",
            "name": "cam",
            "time": time.time(),
            "data": [img, pos, (scale, scale), [0], 0],
        }
        self.dataReadySignal.emit(conf)

        data["data"]["ptp"] = np.ptp(img)
        data["data"]["mean"] = np.mean(img)
        data["data"]["std"] = np.std(img)

        img_item = self.IMG_VIEW["cam"].getImageItem()
        img_roi = self.rectROI["cam"].getArrayRegion(img_item.image, img_item)

        data["data"]["ptpROI"] = np.ptp(img_roi)
        data["data"]["meanROI"] = np.mean(img_roi)
        data["data"]["stdROI"] = np.std(img_roi)
        # if data["data"]["ptpROI"] != 0:
        #     data["data"]["contrastROI"] = data["data"]["ptpROI"]/(np.max(img_roi) + np.min(img_roi))
        # else:
        #     data["data"]["contrastROI"] = np.nan
        # gy, gx = np.gradient(img_roi)
        # gnorm = np.sqrt(gx**2 + gy**2)
        # data["data"]["sharpnessROI"] = np.average(gnorm)

        if save_raw:
            data["img"]["image"] = img
            data["img"]["XYZ"] = position
            data["img"]["scale"] = np.array([scale]*2)
            data["img"]["shift"] = np.array(pos)
            data["img"]["exposure"] = self.ui.ThorCamera_exposure.value()

        # else:
        # 	self.ui.ThorCamera_live.setChecked(False)
        data["time"] = time.time()
        self.ThorCamera_img_isSaved = False

        return data

    def ThorCamera_imgSave(self):
        img_item = self.IMG_VIEW["cam"].getImageItem()
        scale = np.array([self.ui.ThorCamera_scale.value()] * 2)
        roi = self.rectROI["cam"]
        img_raw = img_item.image
        img_roi = roi.getArrayRegion(img_raw, img_item)

        info, currentRow = self.MultiScan_getInfo()

        dtype = [
            ("image", np.int16, img_raw.shape),
            ("image_roi", np.int16, img_roi.shape),
            ("roi_pos", "f4", (2,)),
            ("shift", "f8", (2,)),
            ("XYZ", "f8", (3,)),
            ("scale", "f4", (2,)),
            ("exposure", "f4"),
            ("currentRow", "u4"),
            ("info", info.dtype, (len(info),)),
            ]

        dset = np.zeros((1,), dtype=dtype)
        print(dset.dtype)
        dset[0]['image'] = img_raw
        dset[0]['image_roi'] = img_roi
        dset[0]["roi_pos"] = np.array(roi.pos())
        dset[0]["scale"] = scale
        dset[0]["exposure"] = self.ui.ThorCamera_exposure.value()
        dset[0]["shift"] = img_item.pos()
        dset[0]["XYZ"] = self.piStage.getPosition()
        dset[0]["currentRow"] = currentRow
        dset[0]["info"] = info
        np.save(self.ui.ThorCamera_imgPath.text(), dset)
        self.ThorCamera_img_isSaved = True

    def ThorCamera_imgLoad(self, path=None):
        if path is None:
            path = self.ui.ThorCamera_imgPath.text()
        if os.path.exists(path):
            data = np.load(path)

            pos = self.piStage_position_cam_static_pos
            conf = {
                "type": "img",
                "name": "cam",
                "time": time.time(),
                "data": [data[0]['image'], pos, data[0]['scale'], [0], 0],
            }
            self.dataReadySignal.emit(conf)

    def ThorCamera_imgPathSelect(self):
        fname = QtWidgets.QFileDialog.getSaveFileName(
            self, "Seve data to", self.ui.ThorCamera_imgPath.text()
        )
        if type(fname) == tuple:
            fname = fname[0]
        if fname:
            self.ui.ThorCamera_imgPath.setText(fname)


    ############################################################################
    ###############################   Shamrock	################################

    def shamrockSetFocusingMirror(self, pos=None):
        with self.shamrockWavelengthSet_lock:
            if not self.ui.andorCamera_Shamrock_connect.isChecked():
                self.andorCamera_Shamrock_connect(state=True)

            if pos is None:
                pos = self.ui.shamrockFocusingMirror_position.value()
            if pos >= 0 and pos <= 600:
                self.shamrock.set_focusingMirrorPosition(pos)
            pos = self.shamrock.get_focusingMirrorPosition()
            self.ui.shamrockFocusingMirror_position.setValue(pos)
        return pos

    def shamrockSetWavelength(self, wl=None, ex_wl=None):
        with self.shamrockWavelengthSet_lock:
            if not self.ui.andorCamera_Shamrock_connect.isChecked():
                self.andorCamera_Shamrock_connect(state=True)
                # self.statusBar_setMessage('Spectrometer not connected')
                # return

            if wl is None or wl == False:
                wl = self.shamrockWavelength_center(ex_wl=ex_wl)
            print(wl)
            self.ui.shamrockWavelength_target.setValue(wl)
            # wavelength = self.shamrock.get_wavelength()
            for i in range(100):
                try:
                    self.shamrock.set_wavelength(wl)
                    wavelength = self.shamrock.get_wavelength()
                    break
                except:
                    traceback.print_exc()
                    return
                self.time_sleep(1)
            wavelength = self.shamrock.get_wavelength()
            self.ui.shamrockWavelength.setValue(wavelength)

            if self.ui.shamrockWavelength_adaptive_centering_getBaseline.isChecked():
                shutterState = self.laserClient.shutter()
                IRshutterState = self.laserClient.IRshutter()
                res = self.laserSetShutter(state=False, wait=True)
                microV_logger.info(f"Shutter closed:{res}")
                res = self.laserSetIRShutter(state=False, wait=True)
                microV_logger.info(f"IRShutter closed:{res}")
                self.time_sleep(1)
                self.andorCameraGetBaseline()
                self.time_sleep(1)
                res = self.laserSetShutter(state=shutterState, wait=True)
                microV_logger.info(f"Shutter restored:{res}")
                res = self.laserSetIRShutter(state=IRshutterState, wait=True)
                microV_logger.info(f"IRShutter restored:{res}")

    def shamrockSetPort(self, val):
        with self.shamrockWavelengthSet_lock:
            if not self.ui.andorCamera_Shamrock_connect.isChecked():
                self.andorCamera_Shamrock_connect(state=True)
                # self.statusBar_setMessage('Spectrometer not connected')
                # return

            port = val
            self.shamrock.set_port(port)
            port = self.shamrock.get_port()
            # self.ui.shamrockPort.blockSignals(True)
            self.ui.shamrockPort.setCurrentIndex(port)

    def shamrockSetWavelength_offset(self, val):
        with self.shamrockWavelengthSet_lock:
            if not self.ui.andorCamera_Shamrock_connect.isChecked():
                self.andorCamera_Shamrock_connect(state=True)
                # self.statusBar_setMessage('Spectrometer not connected')
                # return
            grating = self.shamrock.get_grating()
            self.shamrock.set_gratingOffset(grating, val)
            offset = self.shamrock.get_gratingOffset(grating)
            self.ui.shamrockWavelength_offset.setValue(offset)

    def shamrockSetGrating(self, val=None):
        with self.shamrockWavelengthSet_lock:
            if not self.ui.andorCamera_Shamrock_connect.isChecked():
                self.andorCamera_Shamrock_connect(state=True)
                # self.statusBar_setMessage('Spectrometer not connected')
                # return

            grating = val

            self.shamrock.set_grating(grating)
            grating = self.shamrock.get_grating()
            grating_offset = self.shamrock.get_gratingOffset(grating)
            # grating = self.shamrock.get_grating()
            # new_offset = self.ui.shamrockWavelength_offset.value()
            # if grating_offset != new_offset:
            # 	self.shamrock.set_gratingOffset(grating,new_offset)

            # self.ui.shamrockGrating.blockSignals(True)
            self.ui.shamrockGrating.setCurrentIndex(grating)
            # self.ui.shamrockGrating.blockSignals(False)
            # grating_offset = self.shamrock.get_gratingOffset(grating)
            self.ui.shamrockWavelength_offset.setValue(grating_offset)

    def on_shamrockWavelength_adaptive_centering_by_currentIndexChanged(self, index):
        wavelength = self.laserGetWavelength()
        if wavelength is None:
            return

        if index == 0:
            self.ui.shamrockWavelength_adaptive_centering_coef.setEnabled(True)
            center = (
                wavelength * self.ui.shamrockWavelength_adaptive_centering_coef.value()
            )
            self.ui.shamrockWavelength_target.setValue(center)
            return
        else:
            self.ui.shamrockWavelength_adaptive_centering_coef.setEnabled(False)

        center_by = self.ui.shamrockWavelength_adaptive_centering_by.currentText()
        ranges = calcVisibleBands(1045, wavelength)
        if center_by in ranges:
            center = ranges[center_by]
        else:
            center = self.shamrockWavelength_center()
        self.ui.shamrockWavelength_target.setValue(center)

    def shamrockWavelength_center(self, ex_wl=None):
        if ex_wl is None:
            wavelength = self.laserGetWavelength()
        else:
            wavelength = ex_wl
        if wavelength is None:
            return

        if self.ui.shamrockWavelength_adaptive_centering.isChecked():
            center_by = self.ui.shamrockWavelength_adaptive_centering_by.currentText()
            if center_by == "w2*coef":
                center = (
                    wavelength
                    * self.ui.shamrockWavelength_adaptive_centering_coef.value()
                )
            elif center_by == "2w1,2w2,3w1,3w2":
                center = np.mean([wavelength / 2, 1045 / 2, wavelength / 3, 1045 / 3])
                if center < 405:
                    center = 405
            else:

                ranges = calcVisibleBands(1045, wavelength)
                if center_by in ranges:
                    center = ranges[center_by]
                else:
                    return
            microV_logger.info(f"shamrockWavelength_center({center_by}) = {center}")

        else:
            center = self.ui.shamrockWavelength_target.value()

        return center

    def shamrockGetWavelengthArray(self):
        center = self.shamrock.get_wavelength()
        if self.andorCCD_prev_center_wavelength != center:
            size = self.andorCCD.get_pixel_size()
            shape = self.andorCCD.get_sensor_shape()
            self.shamrock.set_pixel_width(size[0])
            self.shamrock.set_number_pixels(shape[0])
            self.andorCCD_wavelength = self.shamrock.get_calibration()

            self.andorCCD_prev_center_wavelength = center

        return self.andorCCD_wavelength

    ############################################################################
    ###############################   AndorCamera	############################

    def andorCamera_Shamrock_connect(self, state):

        if state:

            try:

                if not self.shamrock.is_alive():
                    self.shamrock.initialize()
                    self.shamrock.enable()

                wavelength = self.shamrock.get_wavelength()
                if wavelength == 0:
                    self.shamrockSetWavelength()
                else:
                    self.ui.shamrockWavelength.setValue(wavelength)

                port = self.shamrock.get_port()
                self.ui.shamrockPort.blockSignals(True)
                self.ui.shamrockPort.setCurrentIndex(port)
                self.ui.shamrockPort.blockSignals(False)

                grating = self.shamrock.get_grating()
                self.ui.shamrockGrating.blockSignals(True)
                self.ui.shamrockGrating.setCurrentIndex(grating)
                self.ui.shamrockGrating.blockSignals(False)

                grating_offset = self.shamrock.get_gratingOffset(grating)
                self.ui.shamrockWavelength_offset.setValue(grating_offset)

                focusingMirror_pos = self.shamrock.get_focusingMirrorPosition()
                self.ui.shamrockFocusingMirror_position.setValue(focusingMirror_pos)

                wavelength = self.shamrock.get_wavelength()
                self.andorCCD_prev_center_wavelength = (
                    self.ui.shamrockWavelength.value()
                )
                print(wavelength)

                if not self.andorCCD.is_alive():
                    print("initialize", self.andorCCD.initialize())
                    print(self.andorCCD.enable())
                # print('id',self.andorCCD.get_id())
                print("exposure", self.andorCCD.get_exposure_time())
                print("temperature", self.andorCCD.get_sensor_temperature())
                print("sensor shape", self.andorCCD.get_sensor_shape())
                print("pixel size", self.andorCCD.get_pixel_size())

                # self.andorCCD.set_sensor_temperature(-85)
                # self.andorCCD.set_read_mode('FVB')

                size = self.andorCCD.get_pixel_size()
                shape = self.andorCCD.get_sensor_shape()

                self.shamrock.set_pixel_width(size[0])
                self.shamrock.set_number_pixels(shape[0])
                # self.andorCCD_wavelength_shift = self.ui.shamrockWavelengthShift.value()
                self.andorCCD_wavelength = (
                    self.shamrock.get_calibration()
                )  # - self.andorCCD_wavelength_shift

                self.andorCCDBaseline_dtype = np.dtype(
                    [
                        ("wavelength", "f4", len(self.andorCCD_wavelength)),
                        ("intensity", "l", len(self.andorCCD_wavelength)),
                        ("type", "i1"),  # -1 - baseline, 0 - spectrum
                        ("exposure", "f4"),
                    ]
                )
                data = np.zeros(len(self.andorCCD_wavelength))
                exposure = self.ui.andorCameraExposure.value()
                self.andorCCDBaseline = np.array(
                    (self.andorCCD_wavelength, data, -1, exposure),
                    dtype=self.andorCCDBaseline_dtype,
                )

                print(
                    self.andorCCD.set_trigger(
                        microscope.TriggerType.SOFTWARE, microscope.TriggerMode.ONCE
                    )
                )
                exposure = self.ui.andorCameraExposure.value()
                if exposure != 0:
                    self.andorCCD.set_exposure_time(exposure)

                # data = self.andorCCD.grab_next_data()
                # print(data,data[0].shape)
                self.ui.andorCamera_Shamrock_connect.blockSignals(True)
                self.ui.andorCamera_Shamrock_connect.setChecked(True)
                self.ui.andorCamera_Shamrock_connect.blockSignals(False)
                return True

            except Exception as ex:
                microV_logger.error("andorCamera_Shamrock connect error:", exc_info=ex)
                self.ui.andorCamera_Shamrock_connect.setChecked(False)
                return False
        else:
            try:
                self.andorCCD.shutdown()
                self.shamrock.shutdown()
                return False

            except Exception as ex:
                microV_logger.error(
                    "andorCamera_Shamrock disconnect error:", exc_info=ex
                )
                return False

    def andorCameraSetExposure(self, val):
        if not self.ui.andorCamera_Shamrock_connect.isChecked():
            self.andorCamera_Shamrock_connect(state=True)
            # self.statusBar_setMessage('Spectrometer not connected')
            # return
        if val == 0:
            return -1
        # self.andorCCD.SetExposureTime(val)
        self.andorCCD.set_exposure_time(val)
        val_new = self.andorCameraGetExposure()
        return val_new

    def andorCameraGetExposure(self):
        if not self.ui.andorCamera_Shamrock_connect.isChecked():
            self.andorCamera_Shamrock_connect(state=True)
            # self.statusBar_setMessage('Spectrometer not connected')
            # return

        val = self.andorCCD.get_exposure_time()
        self.ui.andorCameraExposure.blockSignals(True)
        self.ui.andorCameraExposure.setValue(val)
        self.statusBar_exposure.setText(f"Exp:{val:0.4f}")
        self.ui.andorCameraExposure.blockSignals(False)
        return val

    # def andorCameraSetExposure_mode(self,index=0,mode=None,forced=False):
    #
    # 	if not self.ui.andorCamera_Shamrock_connect.isChecked():
    # 		self.andorCamera_Shamrock_connect(state=True)
    # 		#self.statusBar_setMessage('Spectrometer not connected')
    # 		#return
    # 		#
    # 	modes = {'fast':1, 'long':0}
    # 	if mode is None:
    # 		pass
    # 	else:
    # 		index = modes[mode]
    # 	if index == 0:
    # 		val = self.ui.andorCameraExposure_long.value()
    # 	else:
    # 		val = self.ui.andorCameraExposure_fast.value()
    # 	self.ui.andorCameraExposure.setValue(val)
    # 	if forced:
    # 		#self.andorCCD.SetExposureTime(val)
    # 		self.andorCCD.set_exposure_time(val)

    def andorCameraSetReadoutMode(self, val):
        if not self.ui.andorCamera_Shamrock_connect.isChecked():
            self.andorCamera_Shamrock_connect(state=True)
            # self.statusBar_setMessage('Spectrometer not connected')
            # return
            #
        mode = self.ui.andorCameraReadoutMode.currentText()
        self.andorCCD.set_read_mode(mode)

    def getData_oneShot(self):
        source = self.readoutSources.currentText()
        if "AndorCamera" in source:
            worker = Worker(
                lambda status_callback: self.andorCameraGetData(plot=True)
            )  # Any other args, kwargs are passed to
            self.threadpool.start(worker)

        elif "ThorCamera" in source:
            self.ThorCamera_getData()

    def andorCameraGetData(self, plot=False, line_index=0, save_raw=True, _inject_func=None):
        tmp = None
        t00_ = time.time()

        with self.shamrockWavelengthSet_lock:
            if not self.ui.andorCamera_Shamrock_connect.isChecked():
                self.andorCamera_Shamrock_connect(state=True)
                # self.statusBar_setMessage('Spectrometer not connected')
                # return
                #
            t = time.time()
            data = np.zeros(1024)
            exposure = self.andorCameraGetExposure()

            if time.time() - self.andorCamera_temperature_lastUpdate > 10:
                temperature = self.andorCCD.get_sensor_temperature()
                self.ui.andorCamera_temperature.display(temperature)
                self.andorCamera_temperature_lastUpdate = time.time()

            try:
                self.ui.actionOneShot.setEnabled(False)
                if self.ui.andorCamera_HDR.isChecked():
                    MAX = self.ui.andorCamera_HDR_max.value()
                    init_exposure = self.andorCameraGetExposure()
                    mask = None
                    data = None
                    coef = 1
                    factor = self.ui.andorCamera_HDR_factor.value()
                    prev_exposure = self.andorCameraGetExposure()
                    for i in range(5):
                        t0_ = time.time()

                        soft_trigger = (_inject_func is None)
                        if not soft_trigger:
                            self.andorCCD.soft_trigger()
                            _inject_func()
                        d = np.array(
                            self.andorCCD.grab_next_data(soft_trigger=soft_trigger)[0]
                        )  # .mean(axis=0)[::-1]
                        print(time.time() - t0_, time.time()-t00_)
                        if data is None:
                            data = d.copy()
                            mask = np.full(len(data), True)
                        data[mask] = d[mask] * coef
                        mask = d >= MAX
                        if mask.sum() == 0:

                            break
                        else:
                            print("OVERLOAD:", mask.sum() / len(mask))
                            if prev_exposure is None:
                                break
                            if prev_exposure < 0:
                                break
                            if prev_exposure / factor < 0.02:
                                break
                            prev_exposure = self.andorCameraSetExposure(
                                prev_exposure / factor
                            )
                            if prev_exposure < 0.02:
                                break
                            coef *= factor
                    self.andorCameraSetExposure(init_exposure)
                else:
                    t0_ = time.time()
                    soft_trigger = (_inject_func is None)
                    if not soft_trigger:
                        self.andorCCD.soft_trigger()
                        _inject_func()
                    data = np.array(
                        self.andorCCD.grab_next_data(soft_trigger=soft_trigger)[0]
                    )  # .mean(axis=0)[::-1]

                    print(time.time() - t0_)
                self.ui.actionOneShot.setEnabled(True)
            except Exception as ex:
                microV_logger.error("andorCameraGetData", exc_info=ex)
                self.ui.actionOneShot.setEnabled(True)
                return

            data_max = data.max()

            data -= self.andorCCDBaseline["intensity"]

            kernel = self.ui.andorCamera_filteringKernel.value()
            if kernel > 2:
                if kernel % 2 == 0:
                    kernel += 1
                    self.ui.andorCamera_filteringKernel.setValue(kernel)
                data_preview = medfilt(data, kernel)
            else:
                data_preview = data.copy()

            self.shamrockGetWavelengthArray()

            andorCamera_correctionCurve = np.ones(1024)

            if self.ui.andorCamera_previewNormTransmittance.isChecked():
                readout_conf = self.Setup_params.child(
                    "Setup parameters", self.calibrTools.setup, "Readout configuration"
                ).value()
                Filter = self.filtersPiezoStage_ComboBox.currentText()
                Grating = "grating%d" % (self.ui.shamrockGrating.currentIndex())
                # import pdb; pdb.set_trace()
                andorCamera_correctionCurve = (
                    1
                    / self.calibrTools.andorCamera_sensitivity[readout_conf][Grating][
                        Filter
                    ](self.andorCCD_wavelength)
                )
                andorCamera_correctionCurve[andorCamera_correctionCurve > 4] = np.nan
                # data_preview = data_preview*andorCamera_correctionCurve

            if self.ui.andorCamera_previewNormExposure.isChecked():
                andorCamera_correctionCurve /= exposure

            data_preview = data_preview * andorCamera_correctionCurve

            self.andorCamera_correctionCurve = andorCamera_correctionCurve

            if plot:
                now = datetime.datetime.now().strftime("%H:%M:%S")

                self.dataReadySignal.emit(
                    {
                        "type": "raw",
                        "name": "AndorCamera_spectrum",
                        "info": f"{now}",
                        "time": time.time(),
                        "data": [self.andorCCD_wavelength, data],
                    }
                )

                self.dataReadySignal.emit(
                    {
                        "type": "raw",
                        "name": "AndorCamera_correctionCurve",
                        "info": f"{now}",
                        "time": time.time(),
                        "data": [
                            self.andorCCD_wavelength,
                            self.andorCamera_correctionCurve,
                        ],
                    }
                )

            intensity = data
            wavelength_arr = self.andorCCD_wavelength

            Ex_wl = self.laserGetWavelength()
            window = self.ui.andorCamera_integrWindow.value()

            lines = calcVisibleBands(lambda1=1045, lambda2=Ex_wl)

            for l in lines:
                self.andorCamera_integrRanges_params.childs[0][l] = lines[l]

            scan_ranges = {
                child.name(): [
                    lines[child.name()] - window / 2,
                    lines[child.name()] + window / 2,
                ]
                for child in self.andorCamera_integrRanges_params.childs[0]
            }

            for child in self.andorCamera_integrRanges_params.childs[1]:
                scan_ranges[child.name()] = [child["start"], child["end"]]

            # dw = np.diff(wavelength_arr).mean()

            dtype = [
                ("time", "f8"),
                (
                    "raw",
                    [
                        ("wavelength", "f4", len(intensity)),
                        ("intensity", "l", len(intensity)),
                        ("type", "i1"),  # -1 - baseline, 0 - spectrum
                        ("exposure", "f4"),
                    ],
                ),
                ("data", [(key, "f4") for key in scan_ranges.keys()]),
            ]
            tmp = np.empty(1, dtype=dtype)
            tmp["time"] = t
            tmp["raw"]["exposure"] = exposure
            tmp["raw"]["intensity"] = intensity
            tmp["raw"]["wavelength"] = wavelength_arr
            tmp["raw"]["type"] = 0

            for i, r in enumerate(scan_ranges):
                w = (
                    (wavelength_arr > scan_ranges[r][0])
                    & (wavelength_arr < scan_ranges[r][1])
                    & (~np.isnan(data_preview))
                )
                if w.sum() == 0:
                    tmp["data"][r] = np.nan
                else:
                    tmp["data"][r] = np.trapz(data_preview[w], x=wavelength_arr[w])

            keys = list(tmp.dtype.names)
            if not save_raw:
                keys.remove("raw")

            tmp = tmp[keys]
            if self.ui.andorCameraExposure_decreaseOnSaturation.isChecked():
                for i in range(4):
                    if data_max > self.ui.andorCamera_HDR_max.value():
                        self.andorCameraSetExposure(
                            exposure / self.ui.andorCamera_HDR_factor.value()
                        )
        return tmp

    def getBaseline_oneShot(self):
        source = self.readoutSources.currentText()
        if "AndorCamera" in source:
            worker = Worker(
                lambda status_callback: self.andorCameraGetBaseline()
            )  # Any other args, kwargs are passed to
            self.threadpool.start(worker)
        else:
            self.ThorCamera_getBG()

    def andorCameraGetBaseline(self):
        with self.shamrockWavelengthSet_lock:
            if not self.ui.andorCamera_Shamrock_connect.isChecked():
                self.andorCamera_Shamrock_connect(state=True)
                # self.statusBar_setMessage('Spectrometer not connected')
                # return

            # print('andorCameraGetBaseline',self.andorCCD.get_sensor_temperature())
            if time.time() - self.andorCamera_temperature_lastUpdate > 10:
                temperature = self.andorCCD.get_sensor_temperature()
                self.ui.andorCamera_temperature.display(temperature)
                self.andorCamera_temperature_lastUpdate = time.time()
            # self.andorCCD.StartAcquisition()
            # self.andorCCD.WaitForAcquisition()
            # try:
            # 	data = self.andorCCD.GetMostRecentImage()
            # except RuntimeError:
            # 	return
            try:
                # data = self.andorCCD.GetMostRecentImage()
                # self.andorCCD.trigger()
                self.ui.actionBaseline.setEnabled(False)
                data = np.array(
                    self.andorCCD.grab_next_data(soft_trigger=True)[0]
                )  # .mean(axis=0)[::-1]
                self.ui.actionBaseline.setEnabled(True)
            except Exception as ex:
                microV_logger.error("andorCameraGetBaseline", exc_info=ex)
                self.ui.actionBaseline.setEnabled(True)
                return

            exposure = self.andorCameraGetExposure()
            # print(data.mean(),exposure)
            # if not self.ui.shamrockConnect.isChecked():
            # 	self.ui.shamrockConnect.setChecked(True)
            # shift = self.ui.shamrockWavelengthShift.value()
            if (
                self.andorCCD_prev_center_wavelength
                != self.ui.shamrockWavelength.value()
            ):  # or shift != self.andorCCD_wavelength_shift:

                # shape=self.andorCCD.GetDetector()
                # size=self.andorCCD.GetPixelSize()

                size = self.andorCCD.get_pixel_size()
                shape = self.andorCCD.get_sensor_shape()
                self.shamrock.set_pixel_width(size[0])
                self.shamrock.set_number_pixels(shape[0])

                # self.andorCCD_wavelength_shift = shift
                self.andorCCD_wavelength = self.shamrock.get_calibration()  # - shift
                self.andorCCD_prev_center_wavelength = (
                    self.ui.shamrockWavelength.value()
                )
            else:
                wavelength = self.andorCCD_wavelength

            self.andorCCDBaseline = np.array(
                (self.andorCCD_wavelength, data, -1, exposure),
                dtype=[
                    ("wavelength", "f4", len(data)),
                    ("intensity", "l", len(data)),
                    ("type", "i1"),  # -1 - baseline, 0 - spectrum
                    ("exposure", "f4"),
                ],
            )

            self.dataReadySignal.emit(
                {
                    "type": "raw",
                    "name": "AndorCamera_baseline",
                    "info": "",
                    "time": time.time(),
                    "data": [self.andorCCD_wavelength, data],
                }
            )

    ############################################################################
    ###############################   rotPiezoStage	#########################
    #
    # def connect_rotPiezoStage(self, state):
    #     if state:
    #         self.rotPiezoStage.connect()
    #     else:
    #         self.rotPiezoStage.close()
    #
    # def rotPiezoStage_Go(self):
    #     toAngle = self.ui.rotPiezoStage_Step.value()
    #     wait = self.ui.rotPiezoStage_wait.isChecked()
    #     self.rotPiezoStage.move(toAngle, waitUntilReady=wait)
    #     self.ui.rotPiezoStage_Angle.setText(str(self.rotPiezoStage.getAngle()))

    ############################################################################
    ###############################   Spectrometer	##########################
    def ccs200Connect(self, state):
        if state:

            if not self.ccs200_spectrometer.is_alive():
                self.ccs200_spectrometer.initialize()

            # if not self.ccs200_spectrometer.get_is_enabled():
            # 	self.ccs200_spectrometer.enable()
            if self.ui.ccs200ExtTrig.isChecked():
                self.ccs200_spectrometer.set_trigger(
                    microscope.TriggerType.RISING_EDGE, microscope.TriggerMode.BULB
                )
            else:
                self.ccs200_spectrometer.set_trigger(
                    microscope.TriggerType.SOFTWARE, microscope.TriggerMode.ONCE
                )
            self.ui.ccs200Exposure.setValue(
                self.ccs200_spectrometer.get_exposure_time()
            )

        else:
            self.ccs200_spectrometer.disable()
            self.ccs200_spectrometer.shutdown()

    def ccs200SetExposure(self, val=None):
        if not self.ui.ccs200Connect.isChecked():
            self.ui.ccs200Connect.setChecked(True)
        if not val is None:
            val = self.ui.ccs200Exposure.value()

        self.ccs200_spectrometer.set_exposure_time(val)

        self.ui.ccs200Exposure.setValue(self.ccs200_spectrometer.get_exposure_time())

    def ccs200GetBaseline(self, plot=False):
        if not self.ui.ccs200Connect.isChecked():
            self.ui.ccs200Connect.setChecked(True)

        # data,t = self.ccs200_spectrometer.grab_next_data()
        # self.ccs200_spectrometer_baseline =  data[0]
        # wavelength_arr = self.ccs200_spectrometer.get_wavelength()
        # exposure = self.ccs200_spectrometer.get_exposure_time()
        wavelength_arr, intensity, exposure = self.ccs200_spectrometer.getAllData()
        t = time.time()
        self.ccs200_spectrometer_baseline = intensity
        if plot:
            self.dataReadySignal.emit(
                {
                    "type": "raw",
                    "name": "CCS200_baseline",
                    "info": "",
                    "time": t,
                    "data": [wavelength_arr, intensity / exposure],
                }
            )

    def ccs200GetSpectra(self, plot=False):

        if not self.ui.ccs200Connect.isChecked():
            self.ui.ccs200Connect.setChecked(True)
        if not self.ccs200_spectrometer.get_is_enabled():
            self.ccs200_spectrometer.enable()
        wavelength_arr, intensity_, exposure = self.ccs200_spectrometer.getAllData()

        t = time.time()
        intensity = intensity_ - self.ccs200_spectrometer_baseline
        # data,t = self.ccs200_spectrometer.grab_next_data()
        # intensity = data[0] - self.ccs200_spectrometer_baseline
        # wavelength_arr = self.ccs200_spectrometer.get_wavelength()
        # exposure = self.ccs200_spectrometer.get_exposure_time()

        dtype = [
            ("time", "f8"),
            (
                "raw",
                [
                    ("wavelength", "f4", len(intensity)),
                    ("intensity", "f4", len(intensity)),
                    ("type", "i1"),
                    ("exposure", "f4"),
                ],
            ),
            ("data", [("max", "f4"), ("all", "f4")]),
        ]
        tmp = np.empty(1, dtype=dtype)
        tmp["time"] = t
        tmp["raw"]["exposure"] = exposure
        tmp["raw"]["intensity"] = intensity
        tmp["raw"]["type"] = 0
        tmp["raw"]["wavelength"] = wavelength_arr
        tmp["data"]["max"] = intensity.max()
        tmp["data"]["all"] = intensity.sum() / len(intensity)

        if plot:
            self.dataReadySignal.emit(
                {
                    "type": "raw",
                    "name": "CCS200_spectrum",
                    "info": "",
                    "time": t,
                    "data": [wavelength_arr, intensity / exposure],
                }
            )
        return tmp

    ############################################################################
    ###############################   MOCO	##########################

    def mocoConnect(self, state):
        if state:
            # self.moco = MOCO()
            self.moco.initialize()
            alive = self.moco.is_alive()
            if alive:
                # self.moco.positionChanged.objSignal.connect(self.mocoPositionChanged)
                self.ui.mocoPosition.setValue(self.moco.axes["delay"].position)
                self.moco_calcDelay()
            else:
                self.ui.mocoConnect.blockSignals(True)
                self.ui.mocoConnect.setChecked(False)
                self.ui.mocoConnect.blockSignals(False)

            state = alive
        else:
            if not self.moco is None:
                self.moco.shutdown()
                # self.moco = None
        self.ui.mocoConnect.blockSignals(True)
        self.ui.mocoConnect.setChecked(state)
        self.ui.mocoConnect.blockSignals(False)
        return state

    def estimate_mocoZeroDelay_position(self, ex_wl=None, visible=False):
        if ex_wl is None:
            ex_wl = self.laserGetWavelength()

        delay_zero_pos = self.calibrTools.estimate_ZeroDelay_position(ex_wl)
        if visible:
            self.ui.mocoZeroDelay_position.setValue(delay_zero_pos)
        return delay_zero_pos

    def mocoZeroDelay_plot_calibr(self, calibr=None):
        if calibr is None:
            calibr = self.calibrTools.store[self.calibrTools.ZeroDelay_dset_name].data

        self.dataReadySignal.emit(
            [
                {
                    "type": "grid_points",
                    "name": "delay_0_pos",
                    "info": "",
                    "time": time.time(),
                    "pen": 'm',
                    "symbol": "s",
                    "symbolSize": 3,
                    "data": [calibr["ex_wl"], calibr["delay_0_pos"]],
                }
            ]
        )

    def mocoZeroDelay_save_calibr(self):
        qm = QtWidgets.QMessageBox
        ret = qm.question(self, "", f"Save modified calibration curve?", qm.Yes | qm.No)
        if ret == qm.No:
            return
        x, y = self.linesManager.getLine("Preview","delay_0_pos","").getData()
        if len(x) > 2:
            calibr = self.calibrTools.save_ZeroDelay_calibr(ex_wl=x, delay_0_pos=y)
            self.mocoZeroDelay_plot_calibr(calibr)

    def mocoZeroDelay_correct_calibr(self):
        ex_wl = self.ui.mocoZeroDelay_correct_ex_wl.value()
        pos = self.ui.mocoZeroDelay_correct_pos.value()

        if self.ui.laserBeamShift_compensate_delay.isChecked():
            delay_delta = (
                self.ui.laserBeamShift_V_delay_delta0.value()
                + self.ui.laserBeamShift_H_delay_delta0.value()
            )
            pos -= delay_delta / 2

        calibr = self.calibrTools.correct_ZeroDelay_calibr(ex_wl=ex_wl, true_pos=pos)
        self.mocoZeroDelay_plot_calibr(calibr)

    def mocoZeroDelay_reset_calibr(self):
        calibr = self.calibrTools.reset_ZeroDelay_calibr()
        self.mocoZeroDelay_plot_calibr(calibr)

    def mocoZeroDelay_backup_calibr(self):
        self.calibrTools.backup_ZeroDelay_calibr()
        self.mocoZeroDelay_plot_calibr()

    def mocoZeroDelay_add_to_calibr(self, pos=None, wavelength=None, yes=False):
        qm = QtWidgets.QMessageBox
        if not yes:
            ret = qm.question(
                self, "", f"Save this position to calibration curve?", qm.Yes | qm.No
            )
            if ret == qm.No:
                return

        if pos is None:
            pos = self.moco.axes["delay"].position

        if wavelength is None:
            wavelength = self.laserGetWavelength()

        if self.ui.laserBeamShift_compensate_delay.isChecked():
            delay_delta = (
                self.ui.laserBeamShift_V_delay_delta0.value()
                + self.ui.laserBeamShift_H_delay_delta0.value()
            )
            pos -= delay_delta / 2

        calibr = self.calibrTools.insert_ZeroDelay_calibr(wavelength, pos)
        self.mocoZeroDelay_plot_calibr(calibr)

    def mocoZeroDelay_remove_from_calibr(self, wavelength=None):

        if wavelength is None:
            wavelength = self.laserGetWavelength()


        calibr = self.calibrTools.remove_from_ZeroDelay_calibr(wavelength)
        self.mocoZeroDelay_plot_calibr(calibr)

    def mocoMoveRel_forw(self):
        if not self.ui.mocoConnect.isChecked():
            self.mocoConnect(state=True)

        step = self.ui.mocoMoveRel_step.value()
        self.moco.axes["delay"].move_by(step)
        pos = self.moco.axes["delay"].position
        self.ui.mocoPosition.setValue(pos)
        self.moco_calcDelay()

    def mocoMoveRel_backw(self):
        if not self.ui.mocoConnect.isChecked():
            self.mocoConnect(state=True)

        step = self.ui.mocoMoveRel_step.value()
        self.moco.axes["delay"].move_by(-step)
        pos = self.moco.axes["delay"].position
        self.ui.mocoPosition.setValue(pos)
        self.moco_calcDelay()

    def mocoMoveAbs(self, pos=None, wait=False):
        if not self.ui.mocoConnect.isChecked():
            self.mocoConnect(state=True)

        if pos is None:
            pos = self.ui.mocoMoveAbs_target.value()
        pos = self.moco.axes["delay"].move_to(target=pos, wait=wait)
        pos = self.moco.axes["delay"].position
        self.ui.mocoPosition.setValue(pos)
        self.moco_calcDelay()

    def moco_calcDelay(self, pos=None):
        if pos is None:
            pos = self.ui.mocoPosition.value()
        zero = self.ui.mocoZeroDelay_position.value()
        c = 299792458  # m/s
        m = 2  # self.ui.mocoLightPathMultiplier.value()
        delay = (pos - zero) * m / 1000 / c  # s
        self.ui.mocoDelay.setValue(delay)  # s

    def mocoHome(self):
        if not self.ui.mocoConnect.isChecked():
            self.mocoConnect(state=True)

        pos = self.moco.axes["delay"].move_to(target=None)
        pos = self.moco.axes["delay"].position
        self.ui.mocoPosition.setValue(pos)
        self.moco_calcDelay()

    def mocoZeroDelay_move_to_zero(self):
        self.abstractMoveTo(0, "Delay_line_position_zero_relative")

    ############################################################################
    ###############################   filtersPiezoStage	########################
    def filtersPiezoStage_connect(self, state):
        if state:
            self.piezoDev_bus.initialize()
            alive = self.piezoDev_bus.is_alive()
            if alive:
                index = self.piezoDev_bus.axes["filters"].position
                self.filtersPiezoStage_ComboBox.setCurrentIndex(int(index))
                self.commutation_mirror0_updatePosition()
            else:
                self.ui.filtersPiezoStage_connect.setChecked(False)
            state = alive

        else:
            self.piezoDev_bus.shutdown()

        self.ui.filtersPiezoStage_connect.blockSignals(True)
        self.ui.filtersPiezoStage_connect.setChecked(state)
        self.ui.filtersPiezoStage_connect.blockSignals(False)
        return state

    def filtersPiezoStage_update(self):
        index = self.piezoDev_bus.axes["filters"].position
        self.filtersPiezoStage_ComboBox.blockSignals(True)
        self.filtersPiezoStage_ComboBox.setCurrentIndex(int(index))
        self.filtersPiezoStage_ComboBox.blockSignals(False)

    def filtersPiezoStage_next(self):
        if not self.ui.filtersPiezoStage_connect.isChecked():
            self.filtersPiezoStage_connect(state=True)

        index = self.piezoDev_bus.axes["filters"].position + 1
        self.piezoDev_bus.axes["filters"].move_to(index)
        self.filtersPiezoStage_update()

    def filtersPiezoStage_prev(self):
        if not self.ui.filtersPiezoStage_connect.isChecked():
            self.filtersPiezoStage_connect(state=True)

        index = self.piezoDev_bus.axes["filters"].position - 1
        self.piezoDev_bus.axes["filters"].move_to(index)
        self.filtersPiezoStage_update()

    def filtersPiezoStage_home(self):
        if not self.ui.filtersPiezoStage_connect.isChecked():
            self.filtersPiezoStage_connect(state=True)
        self.piezoDev_bus.axes["filters"].move_to()
        self.filtersPiezoStage_update()

    def filtersPiezoStage_filters_set(self):

        filters = self.ui.filtersPiezoStage_filters.text().replace(" ", "").split(",")
        for i, f in enumerate(filters):
            self.filtersPiezoStage_Dict[i] = f
        store = exdir.File("configStore.exdir")
        store["HRS"]["filtersPiezoStage"].attrs["Filters"] = self.filtersPiezoStage_Dict
        store["Microscope"]["filtersPiezoStage"].attrs[
            "Filters"
        ] = self.filtersPiezoStage_Dict
        store.close()
        self.filtersPiezoStage_ComboBox.clear()
        self.filtersPiezoStage_ComboBox.insertItems(1, filters)
        self.filtersPiezoStage_ComboBox.adjustSize()
        self.filtersPiezoStage_update()

    def filtersPiezoStage_setCurrentIndex(self, index):
        if not self.ui.filtersPiezoStage_connect.isChecked():
            self.filtersPiezoStage_connect(state=True)

        self.piezoDev_bus.axes["filters"].move_to(index)
        self.filtersPiezoStage_update()

    ############################################################################
    #######################  abstract methods  #################################
    def getUnifiedData(
        self,
        source=None,
        save_raw=True,
        save_metadata=False,
        axes_positions_skip=None,
        axes_positions=None,
        parameters=None,
        _inject_func=None,
    ):

        if source is None:
            source = self.readoutSources.currentText()
        # out = {'source':source}
        # data_dtype = {}

        # data_dtype[('time','f8')] = time.time()
        t0 = time.time()
        data = {}
        start = np.array([time.time()], dtype="f8")

        # np.rec.array(
        if "PicoScope" in source:
            data["PicoScope"] = self.readPico(save_raw=save_raw)

        if "AndorCamera" in source:
            data["AndorCamera"] = self.andorCameraGetData(save_raw=save_raw, _inject_func=_inject_func)

        if "CCS200" in source:
            data["CCS200"] = self.ccs200GetSpectra()

        if "Powermeter" in source:
            dtype = [
                (
                    "data",
                    [("power", READOUT_SOURCES_UNITS_DTYPES["Powermeter"]["Power"][1])],
                )
            ]

            data["Powermeter"] = np.zeros(1, dtype=dtype)

            data["Powermeter"]["data"]["power"] = self.readPower()

        if "DAQmx" in source:
            pass
            """
			with nidaqmx.Task() as task:
				task.ai_channels.add_ai_voltage_chan("Dev1/ai0,Dev1/ai1")
				dataA, dataB= task.read(number_of_samples_per_channel=self.ui.DAQmx_number_of_samples_per_channel.value())
				dataA = -np.array(dataA)
				dataB = -np.array(dataB)

				A = np.mean(dataA)
				B = np.mean(dataB)

				if self.ui.ViewTabs.currentIndex()==1:
					t = np.arange(len(dataA))
					self.dataReadySignal.emit([
						{'time':time.time(),'name':'line_PicoScope_ChA','data':[t,dataA]},
						{'time':time.time(),'name':'line_PicoScope_ChB','data':[t,dataB]}
						])
				out['DAQmx_ai0'] = A
				out['DAQmx_ai1'] = B
			"""
        if "ThorCamera" in source:
            data["ThorCamera"] = self.ThorCamera_getData(save_raw=save_raw)
        # out['data'] = np.rec.array(list(data_dtype.values()),dtype=list(data_dtype.keys()))
        # out['data'].resize((1,))
        tmp = np.rec.fromarrays(list(data.values()), names=list(data.keys()))
        data = np.rec.fromarrays([start, tmp], names=["time", "data"])
        if save_metadata:

            metadata = self.abstractGetMetadata(
                axes_positions=axes_positions,
                axes_positions_skip=axes_positions_skip,
                parameters=parameters,
            )
            # print('metadata',metadata)
            data = rfn.append_fields(data, "metadata", [metadata], usemask=False)
            # print('data',data)

        return data

    def moveToDialog(self):
        sender = self.sender()
        axis = sender.objectName()
        modifiers = QtWidgets.QApplication.keyboardModifiers()

        if axis == "statusBar_Position_X":
            axis = "X"
        elif axis == "statusBar_Position_Y":
            axis = "Y"
        elif axis == "statusBar_Position_Z":
            axis = "Z"
        elif axis == "statusBar_ExWavelength":
            axis = "LaserWavelength"
        elif axis == "statusBar_HWP_FIX_angle":
            axis = "HWP_FIX_angle"
            if modifiers == QtCore.Qt.ControlModifier:
                self.HWP_FIX_go_home()
                return
            elif modifiers == QtCore.Qt.ShiftModifier:
                self.HWP_FIX_go_min_power_angle()
                return

        elif axis == "statusBar_HWP_TUN_angle":
            axis = "HWP_TUN_angle"
            if modifiers == QtCore.Qt.ControlModifier:
                self.HWP_TUN_go_home()
                return
            elif modifiers == QtCore.Qt.ShiftModifier:
                self.HWP_TUN_go_min_power_angle()
                return
        elif axis == "statusBar_HWP_Polarization":
            axis = "HWP_Polarization"
            if modifiers == QtCore.Qt.ControlModifier:
                self.HWP_Polarization_go_home()
                return
            elif modifiers == QtCore.Qt.ShiftModifier:
                self.HWP_Polarization_moveTo(90)
                return
        elif axis == "statusBar_arduinoStage":
            axis = "Lens_position"
        elif axis == "statusBar_arduinoStage1":
            axis = "Knife_position"
        elif axis == "statusBar_exposure":
            source = self.readoutSources.currentText()
            if source == "ThorCamera":
                axis = "ThorCameraExposure"
                if modifiers == QtCore.Qt.ControlModifier:
                    val = self.ThorCamera_exposure(99)
                    self.statusBar_exposure.setText(f"Exp:{val:0.4f}")
                    return
                elif modifiers == QtCore.Qt.ShiftModifier:
                    val = self.ThorCamera_exposure(0.1)
                    self.statusBar_exposure.setText(f"Exp:{val:0.4f}")
                    return

            elif "AndorCamera" in source:

                axis = "andorCameraExposure"
                if modifiers == QtCore.Qt.ControlModifier:
                    self.andorCameraSetExposure(1)
                    return
                elif modifiers == QtCore.Qt.ShiftModifier:
                    self.andorCameraSetExposure(0.1)
                    return
            else:
                pass
        # elif axis == 'statusBar_laserCalcPower': axis = "LaserPower"
        elif axis == "statusBar_moco":
            axis = "Delay_line_position"
            if modifiers == QtCore.Qt.ShiftModifier:
                self.abstractMoveTo(0, axis="Delay_line_position_zero_relative")
                return
            elif modifiers == QtCore.Qt.ControlModifier:
                self.mocoHome()
                return
        else:
            return

        print(axis)
        dlg = QtWidgets.QInputDialog(self)
        dlg.setLocale(QtCore.QLocale(QtCore.QLocale.C))
        dlg.setInputMode(QtWidgets.QInputDialog.DoubleInput)
        dlg.setLabelText("Target [" + axis + "]:")
        dlg.setDoubleDecimals(6)
        dlg.setDoubleRange(-999999, 999999)

        dlg.setDoubleValue(self.abstractGetPosition(axis))
        ok = dlg.exec_()
        target = dlg.doubleValue()
        if ok:
            t = threading.Thread(target=self.abstractMoveTo, args=(target, axis))
            t.daemon = True
            t.start()

    def abstractMoveTo(self, target, axis="None", wait=True):
        self._monitor_lock = True
        try:
            move_function = None
            print(axis, target)
            if target is None:
                return
            if axis == "X":
                if 100 < target or target < 0:
                    self._monitor_lock = False
                    return
                self.piStage_moveTo(0, [target], wait=wait)
            elif axis == "Y":
                if 100 < target or target < 0:
                    self._monitor_lock = False
                    return
                self.piStage_moveTo(1, [target], wait=wait)
            elif axis == "Z":
                if 100 < target or target < 0:
                    self._monitor_lock = False
                    return
                self.piStage_moveTo(2, [target], wait=wait)
            elif axis == "dX":
                self.piStage_moveBy(0, [target], wait=wait)
            elif axis == "dY":
                self.piStage_moveBy(1, [target], wait=wait)
            elif axis == "dZ":
                self.piStage_moveBy(2, [target], wait=wait)
            elif axis == "XY":
                # if 100<target<0: return
                self.piStage_moveTo([0, 1], target, wait=wait)
            elif axis == "XYZ":
                # if 100<target<0: return
                self.piStage_moveTo([0, 1, 2], target, wait=wait)

            elif axis == "HWP_TUN_angle":
                self.HWP_TUN_angle_moveTo(target, wait=wait)
            elif axis == "HWP_FIX_angle":
                self.HWP_FIX_angle_moveTo(target, wait=wait)
            elif axis == "HWP_TUN_power":
                self.laserPowerIntens_set(target, mode="Average power")
            elif axis == "HWP_FIX_power":
                self.laserPowerIntensIR_set(target, mode="Average power")
            elif axis == "HWP_TUN_intens":
                self.laserPowerIntens_set(target, mode="Peak intensity")
            elif axis == "HWP_FIX_intens":
                self.laserPowerIntensIR_set(target, mode="Peak intensity")
            elif axis == "HWP_Polarization":
                self.HWP_Polarization_moveTo(target, wait=wait)

            elif axis == "Delay_line_position":
                self.mocoMoveAbs(pos=target, wait=wait)
            elif axis == "Delay_line_position_zero_relative":
                zero = self.estimate_mocoZeroDelay_position()
                if self.ui.laserBeamShift_compensate_delay.isChecked():
                    delay_delta = (
                        self.ui.laserBeamShift_V_delay_delta0.value()
                        + self.ui.laserBeamShift_H_delay_delta0.value()
                    )
                    zero += delay_delta / 2
                self.mocoMoveAbs(pos=target + zero, wait=wait)

            elif axis == "laserBeamShift_V":
                self.laserBeamShift_V_moveTo(target, wait=wait)
            elif axis == "laserBeamShift_V_angle":
                self.laserBeamShift_V_moveTo(target, wait=wait, by_angle=True)
            elif axis == "laserBeamShift_H":
                self.laserBeamShift_H_moveTo(target, wait=wait)
            elif axis == "Lens_position":
                self.arduinoStage_moveTo(target, wait=wait)
            elif axis == "Knife_position":
                self.arduinoStage1_moveTo(target, wait=wait)

            elif axis == "commutation_mirror0":
                self.commutation_mirror0_moveTo(target, wait=wait)
            # elif axis == 'commutation_mirror1':
            # 	self.commutation_mirror1_moveTo(target, wait=wait)
            # elif axis == 'commutation_mirror2':
            # 	self.commutation_mirror2_moveTo(target, wait=wait)

            elif axis == "Laser_precompensation":
                # print('Delay_line_position::::')
                self.laserSetPrecompensation(target, wait=True)
            elif axis == "LaserShutter":
                if target == 0:
                    self.laserSetShutter(False, wait=True)
                    self.laserSetIRShutter(False, wait=True)
                elif target == 1:
                    self.laserSetShutter(False, wait=True)
                    self.laserSetIRShutter(True, wait=True)
                elif target == 2:
                    self.laserSetShutter(True, wait=True)
                    self.laserSetIRShutter(False, wait=True)
                elif target == 3:
                    self.laserSetShutter(True, wait=True)
                    self.laserSetIRShutter(True, wait=True)
                else:

                    self._monitor_lock = False
                    return
            elif axis == "filtersPiezoStage":
                self.filtersPiezoStage_setCurrentIndex(int(target))
                self.time_sleep(0.3)
            elif axis == "shamrockWavelength":
                self.shamrockSetWavelength(target)
            elif axis == "shamrockGrating":
                self.shamrockSetGrating(int(target))

            elif axis == "shamrockFocusingMirror":
                self.shamrockSetFocusingMirror(int(target))

            elif axis == "andorCameraExposure":
                self.andorCameraSetExposure(target)

            elif axis == "ThorCameraExposure":
                self.ThorCamera_exposure(target)

            # elif axis == 'HWP_Polarization':
            # 	self.HWP_stepper_moveTo(float(target),wait=True)
            elif axis == "LaserWavelength":
                wl = target
                if target < 680:

                    self._monitor_lock = False
                    return
                if target > 1300:
                    self._monitor_lock = False
                    return
                # self.laserClient.setWavelength(int(target))
                self.laserSetWavelength(wl=target, wait=wait)
                if wait:
                    for i in range(10):
                        wl = self.laserClient.wavelength()
                        if wl == int(target):
                            break
                        self.time_sleep(1)
                        self.laserClient.setWavelength(int(target))

            elif axis == "CenterIndex":
                rowCount = self.ui.MultiScan_probe.rowCount()
                if target < rowCount:
                    for i in range(10):
                        self.ui.MultiScan_probe.viewport().update()
                        if self.ui.MultiScan_probe.currentRow() == target:
                            break
                        else:
                            self.ui.MultiScan_probe.selectRow(int(target))
                            self.time_sleep(0.2)

            else:
                self._monitor_lock = False
                return

            self._monitor_lock = False
        except Exception as ex:
            microV_logger.error("abstractMoveTo ERROR", exc_info=ex)
            self._monitor_lock = False

    def abstractGetPosition(self, axis):
        try:

            self._monitor_lock = True
            pos = None
            if axis in ["X", "Y", "Z", "XY", "XYZ"]:
                pos_ = self.piStage.getPosition()
                if axis == "X":
                    pos = pos_[0]
                elif axis == "Y":
                    pos = pos_[1]
                elif axis == "Z":
                    pos = pos_[2]
                elif axis == "XY":
                    pos = pos_[:2]
                else:
                    self._monitor_lock = False
                    return pos_
            elif axis in ["X_target", "Y_target", "Z_target"]:
                pos_ = self.piStage.getTargetPosition()
                if axis == "X_target":
                    pos = pos_[0]
                elif axis == "Y_target":
                    pos = pos_[1]
                elif axis == "Z_target":
                    pos = pos_[2]
                elif axis == "XY_target":
                    pos = pos_[:2]
                else:
                    self._monitor_lock = False
                    return pos_

            elif axis in ["dX", "dY", "dZ"]:
                pos_ = self.piStage.getTargetPosition()
                if axis == "dX":
                    pos = pos_[0]
                elif axis == "dY":
                    pos = pos_[1]
                elif axis == "dZ":
                    pos = pos_[2]
                else:
                    self._monitor_lock = False
                    return pos_
            elif axis == "HWP_Polarization":
                pos = self.ui.HWP_Polarization_angle.value()

            elif axis == "HWP_TUN_angle":
                pos = float(self.ui.HWP_TUN_angle.text())
            elif axis == "HWP_FIX_angle":
                pos = float(self.ui.HWP_FIX_angle.text())

            elif axis == "HWP_TUN_power":
                pos = self.laserPowerInfo_params.child("Laser parameters").child(
                    "Tunable output"
                )["Average power"]
            elif axis == "HWP_FIX_power":
                pos = self.laserPowerInfo_params.child("Laser parameters").child(
                    "Fixed output"
                )["Average power"]

            elif axis == "HWP_TUN_intens":
                pos = self.laserPowerInfo_params.child("Laser parameters").child(
                    "Tunable output"
                )["Peak intensity"]
            elif axis == "HWP_FIX_intens":
                pos = self.laserPowerInfo_params.child("Laser parameters").child(
                    "Fixed output"
                )["Peak intensity"]

            elif axis == "Delay_line_position":
                pos = self.moco.axes["delay"].position

            elif axis == "Delay_line_position_zero_relative":
                zero = self.estimate_mocoZeroDelay_position()
                if self.ui.laserBeamShift_compensate_delay.isChecked():
                    delay_delta = (
                        self.ui.laserBeamShift_V_delay_delta0.value()
                        + self.ui.laserBeamShift_H_delay_delta0.value()
                    )
                    zero += delay_delta / 2
                pos = self.moco.axes["delay"].position - zero
            elif axis == "laserBeamShift_V":
                pos = self.ui.laserBeamShift_V_position.value()
            elif axis == "laserBeamShift_V_angle":
                pos = self.ui.laserBeamShift_V_angle.value()
            elif axis == "laserBeamShift_H":
                pos = self.ui.laserBeamShift_H_position.value()
            elif axis == "Lens_position":
                pos = float(self.ui.arduinoStage_position.text())
            elif axis == "Knife_position":
                pos = float(self.ui.arduinoStage1_position.text())

            elif axis == "commutation_mirror0":
                pos = self.ui.commutation_mirror0_position.value()
            # elif axis == 'commutation_mirror1':
            # 	pos = self.ui.commutation_mirror1_position.value()
            # elif axis == 'commutation_mirror2':
            # 	pos = self.ui.commutation_mirror2_position.value()

            elif axis == "Laser_precompensation":
                pos = self.laserGetPrecompensation()#ui.laserPrecompensation.value()
            elif axis == "LaserWavelength":
                pos = self.laserGetWavelength()
            elif axis == "LaserShutter":
                s = self.ui.actionShutter.isChecked()
                sIR = self.ui.actionIRShutter.isChecked()
                if s and sIR:
                    pos = 3
                elif s:
                    pos = 2
                elif sIR:
                    pos = 1
                else:
                    pos = 0
            elif axis == "filtersPiezoStage":
                pos = self.filtersPiezoStage_ComboBox.currentIndex()
            elif axis == "shamrockWavelength":
                pos = self.ui.shamrockWavelength.value()
            elif axis == "shamrockGrating":
                pos = self.ui.shamrockGrating.currentIndex()
            elif axis == "shamrockFocusingMirror":
                pos = self.ui.shamrockFocusingMirror_position.value()

            elif axis == "andorCameraExposure":
                pos = self.ui.andorCameraExposure.value()

            elif axis == "ThorCameraExposure":
                pos = self.thorcam.exposure

            elif axis == "CenterIndex":
                self.ui.MultiScan_probe.viewport().update()
                pos = self.ui.MultiScan_probe.currentRow()

            else:
                pos = time.time()

        except Exception as ex:
            microV_logger.error("abstractGetPosition:", exc_info=ex)
            self._monitor_lock = False
            return np.nan

        if type(pos) == np.ndarray:
            if pos.ndim == 0:
                pos = pos.reshape(1)[0]

        self._monitor_lock = False
        return pos

    def abstractGetPositions(self, axes):
        pos_axis_val = []
        pos_axis_dtype = []
        for ax in axes:
            pos_axis_val.append(self.abstractGetPosition(axis=ax))
            pos_axis_dtype.append((ax, AXES_UNITS_DTYPES[ax][1]))
        return pos_axis_val, pos_axis_dtype

    def abstractGetMetadata(
        self, axes_positions=None, axes_positions_skip=None, parameters=None
    ):

        metadata = {}
        if axes_positions is None:
            axes_positions = [
                "HWP_TUN_angle",
                "HWP_FIX_angle",
                "HWP_TUN_power",
                "HWP_FIX_power",
                "HWP_TUN_intens",
                "HWP_FIX_intens",
                "Delay_line_position",
                "Delay_line_position_zero_relative",
                "laserBeamShift_H",
                "laserBeamShift_V",
                "Knife_position",
                "Laser_precompensation",
                "LaserWavelength",
                "LaserShutter",
                "filtersPiezoStage",
                "shamrockWavelength",
                "shamrockGrating",
            ]
            if self.calibrTools.setup == "Microscope":
                axes_positions += [
                    "X",
                    "Y",
                    "Z",
                    "X_target",
                    "Y_target",
                    "Z_target",
                    "commutation_mirror0",
                    #'commutation_mirror1',
                    #'commutation_mirror2',
                    "HWP_Polarization",
                ]

            else:
                axes_positions += [
                    "Lens_position",
                ]
        if not axes_positions_skip is None:
            for k in axes_positions_skip:
                if k in axes_positions:
                    axes_positions.remove(k)

        for p in axes_positions:
            metadata[p] = self.abstractGetPosition(p)

        keys = list(metadata.keys())
        formats = [AXES_UNITS_DTYPES[k][1] for k in keys]

        if parameters is None:
            parameters = [
                "laserBultInPower",
                "AndorCamera_exposure",
                "HWP_TUN_min_power_angle",
                "HWP_FIX_min_power_angle",
            ]

        if "laserBultInPower" in parameters:
            metadata["laserBultInPower"] = self.ui.laserPower_internal.value()
            keys.append("laserBultInPower")
            formats.append("f4")
        if "AndorCamera_exposure" in parameters:
            metadata["AndorCamera_exposure"] = self.ui.andorCameraExposure.value()
            keys.append("AndorCamera_exposure")
            formats.append("f4")

            metadata[
                "andorCamera_previewNormExposure"
            ] = self.ui.andorCamera_previewNormExposure.isChecked()
            keys.append("andorCamera_previewNormExposure")
            formats.append("b1")

            metadata[
                "andorCamera_previewNormTransmittance"
            ] = self.ui.andorCamera_previewNormTransmittance.isChecked()
            keys.append("andorCamera_previewNormTransmittance")
            formats.append("b1")

        if "HWP_FIX_min_power_angle" in parameters:
            metadata[
                "HWP_FIX_min_power_angle"
            ] = self.ui.HWP_FIX_min_power_angle.value()
            keys.append("HWP_FIX_min_power_angle")
            formats.append("f4")

        if "HWP_TUN_min_power_angle" in parameters:
            metadata[
                "HWP_TUN_min_power_angle"
            ] = self.ui.HWP_TUN_min_power_angle.value()
            keys.append("HWP_TUN_min_power_angle")
            formats.append("f4")

        for k in keys:
            if type(metadata[k]) == np.ndarray:
                if metadata[k].ndim == 0:
                    metadata[k] = metadata[k].reshape(1)[0]

        # print(metadata,keys,formats)
        # print(tuple(metadata.values()))
        metadata = np.core.records.fromrecords(
            tuple(metadata.values()), names=keys, formats=formats
        )
        return metadata

    ############################################################################
    ###############################   scan	################################

    def expData_filePath_find_dialog(self):
        fname = QtWidgets.QFileDialog.getExistingDirectory(
            self, "Set experimental directory", self.expData_filePath.text()
        )
        if type(fname) == tuple:
            fname = fname[0]
        if not os.listdir(fname):
            os.rmdir(fname)
        try:
            if not ".exdir" in fname:
                if fname[-1] == "/" or fname[-1] == "\\":
                    fname = fname[:-1]
                new_name = fname + ".exdir"
                fname = new_name
            self.expData_filePath.setText(fname)

            microV_logger.info("expData_filePath: " + fname)
        except:
            traceback.print_exc()

    def centerDetect_find(self):
        num_peaks = self.ui.centerDetect_num_peaks.value()
        threshold_rel = self.ui.centerDetect_threshold_rel.value()
        min_distance = self.ui.centerDetect_min_distance.value()
        source = self.ui.centerDetect_source.currentText()
        roi = self.rectROI["A"]

        if source == "A":
            img_item = self.IMG_VIEW["A"].getImageItem()
            scale = np.array(
                [self.ui.scan3D_Axis0_step.value(), self.ui.scan3D_Axis1_step.value()]
            )
            roi = self.rectROI["A"]

        elif source == "B":
            img_item = self.IMG_VIEW["B"].getImageItem()
            scale = np.array(
                [self.ui.scan3D_Axis0_step.value(), self.ui.scan3D_Axis1_step.value()]
            )
            roi = self.rectROI["B"]

        elif source == "ThorCamera":
            img_item = self.IMG_VIEW["cam"].getImageItem()
            scale = np.array([self.ui.ThorCamera_scale.value()] * 2)
            roi = self.rectROI["cam"]

        img = roi.getArrayRegion(img_item.image, img_item)
        # print(img, img.shape)
        if self.ui.centerDetect_negative.isChecked():
            img = img.max() - img
        pos = self.piStage.getPosition()
        offset = np.array(roi.pos())

        print(pos, scale)

        # img = gaussian_filter(img,sigma=2)

        angle = self.abstractGetPosition(axis="HWP_Polarization")
        pos = np.append(pos, angle)

        centers_index = peak_local_max(
            img,
            min_distance=min_distance,
            threshold_rel=threshold_rel,
            num_peaks=num_peaks,
        )
        centers = centers_index * scale + offset + scale / 2
        centers = np.append(centers, np.full((len(centers), 1), pos[2]), axis=1)
        centers = np.append(centers, np.full((len(centers), 1), angle), axis=1)

        print(centers_index)
        print(centers)
        for row in range(len(centers)):
            for i in range(4):
                self.ui.MultiScan_probe.item(row, i).setText(
                    str(centers[row][i].round(4))
                )
            if self.ui.MultiScan_probe.cellWidget(row, 4).currentText() == "None":
                self.ui.MultiScan_probe.cellWidget(row, 4).setCurrentIndex(1)

        # self.MultiScan_update()
        self.MultiScan_update()

    def scanND_Scan(self, state):
        self.progressBar.setValue(0)
        self.ui.actionStop.setEnabled(True)
        self.ui.actionPause.setEnabled(True)

        if self.scriptThread is None:
            microV_logger.info("scanND: start")
            try:
                importlib.reload(scanND)
                self.scriptThread = scanND.scanNDThread(self)
                self.scriptThread.finishedSignal.connect(self.progressBar_finished)
                self.scriptThread.progressSignal.connect(self.progressBar_progress)
                self.scriptThread.dataReadySignal.connect(self.plotter)

                state = self.scanND_tree.params.saveState()
                self.settings.setValue("ScanND_params", state)

                self.scriptThread.start()
                if hasattr(self.scriptThread, "storeActiveGroup"):
                    self.scriptThread_lastStorePath = (
                        self.scriptThread.store.directory.absolute(),
                        self.scriptThread.storeActiveGroup.name,
                    )
                else:
                    self.scriptThread_lastStorePath = None

            except Exception as ex:
                microV_logger.error("scanND: ERROR", exc_info=ex)
                self.stopActiveThreads()
        else:
            self.stopActiveThreads()

    def scan3D_Scan(self, state):
        self.progressBar.setValue(0)
        self.ui.actionStop.setEnabled(True)
        self.ui.actionPause.setEnabled(True)

        if self.scriptThread is None:
            microV_logger.info("scan3D: start")
            self.saveUiValues()
            try:
                importlib.reload(scan3D)
                self.scriptThread_lastStorePath = None
                self.scriptThread = scan3D.scan3DThread(self)
                self.scriptThread.finishedSignal.connect(self.progressBar_finished)
                self.scriptThread.progressSignal.connect(self.progressBar_progress)
                self.scriptThread.dataReadySignal.connect(self.plotter)

                state = self.scan3D_tree.params.saveState()
                self.settings.setValue('Scan3D_params',state)

                self.scriptThread.start()

            except Exception as ex:
                microV_logger.error("scan3D: ERROR", exc_info=ex)
                self.stopActiveThreads()
        else:
            self.stopActiveThreads()

    def readme_save(self):
        print(self.scriptThread_lastStorePath)
        if not self.scriptThread_lastStorePath is None:
            with exdir.File(self.scriptThread_lastStorePath[0]) as store:
                store[self.scriptThread_lastStorePath[1]].attrs[
                    "README"
                ] = self.ui.readme_textBrowser.toPlainText()
                self.ui.readme_status.setText(
                    f"Saved to: {self.scriptThread_lastStorePath[0]}{self.scriptThread_lastStorePath[1]}"
                )
        else:
            ## TODO: add custom path
            self.ui.readme_status.setText(f"No active store for saving")
            pass

    def script_open(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(
            self,
            "Open script",
        )[0]
        print(fname)
        if fname:
            self.ui.script_path.setText(fname)

    def script_run(self):
        try:
            importlib.reload(scan3D_new)
            self.scriptThread_lastStorePath = None
            self.scriptThread = scan3D_new.scan3DThread(self)
            self.scriptThread.finishedSignal.connect(self.progressBar_finished)
            self.scriptThread.progressSignal.connect(self.progressBar_progress)
            self.scriptThread.dataReadySignal.connect(self.plotter)

            #state = self.scan3D_params.saveState()
            #self.settings.setValue("scan3D_params", state)

            self.scriptThread.start()

        except Exception as ex:
            microV_logger.error("scan3D: ERROR", exc_info=ex)
            self.stopActiveThreads()

    def setCalibrSetup(self, setup="HRS", config=None, show_hide=True):
        if config is None:
            config = {
                child.name(): child.value()
                for child in self.Setup_params.child("Setup parameters")
                .child(setup)
                .childs
            }

        self.ui.actionMicroscope.blockSignals(True)
        self.ui.actionHRS.blockSignals(True)
        if show_hide:
            if setup == "HRS":
                self.ui.actionHRS.setChecked(True)
                self.ui.actionMicroscope.setChecked(False)
                # self.DOCK['A'].hide()
                # self.DOCK['B'].hide()
                # self.DOCK['cam'].hide()
                self.ui.actionADock.setChecked(False)
                self.ui.actionADock.triggered.emit(False)

                self.ui.actionBDock.setChecked(False)
                self.ui.actionBDock.triggered.emit(False)

                self.ui.actionCameraDock.setChecked(False)
                self.ui.actionCameraDock.triggered.emit(False)

                self.statusBar_Position_X.hide()
                self.statusBar_Position_Y.hide()
                self.statusBar_Position_Z.hide()
                self.statusBar_arduinoStage.show()

                self.ui.zSlider.hide()
                self.ui.toolBar.actions()[self._toolBar_light_actionIndex].setVisible(False)
                self.ui.toolBar.actions()[self._toolBar_commutation_mirror0_actionIndex].setVisible(False)
                self.statusBar_HWP_Polarization.hide()
                # self.DOCK['A'].hide()
                # self.DOCK['B'].hide()
                self.ui.laserPrecompensation_use_calibration.setChecked(False)

            else:
                self.ui.actionHRS.setChecked(False)
                self.ui.actionMicroscope.setChecked(True)
                self.statusBar_Position_X.show()
                self.statusBar_Position_Y.show()
                self.statusBar_Position_Z.show()
                self.statusBar_arduinoStage.hide()

                self.ui.zSlider.show()

                self.ui.toolBar.actions()[self._toolBar_light_actionIndex].setVisible(True)
                self.ui.toolBar.actions()[self._toolBar_commutation_mirror0_actionIndex].setVisible(True)
                self.statusBar_HWP_Polarization.show()

                self.ui.actionADock.setChecked(True)
                self.ui.actionADock.triggered.emit(True)

                self.ui.actionBDock.setChecked(True)
                self.ui.actionBDock.triggered.emit(True)

                self.ui.actionCameraDock.setChecked(True)
                self.ui.actionCameraDock.triggered.emit(True)
                self.ui.laserPrecompensation_use_calibration.setChecked(True)
                #self.laserPrecompensation_moveToCalibr()

        self.calibrTools = CalibrToolkit(setup=setup, config=config)
        self.settings.setValue("calibrSetup", setup)

        self.ui.actionMicroscope.blockSignals(False)
        self.ui.actionHRS.blockSignals(False)
        tmp = self.configStore[f"{self.calibrTools.setup}/HWP_TUN"]
        timestamp = max(tmp)
        self.ui.HWP_TUN_min_power_angle.setValue(
            tmp[timestamp].attrs["power2angle_interpLimits"][0]
        )
        del tmp

        tmp = self.configStore[f"{self.calibrTools.setup}/HWP_FIX"]
        timestamp = max(tmp)
        self.ui.HWP_FIX_min_power_angle.setValue(
            tmp[timestamp].attrs["power2angle_interpLimits"][0]
        )
        del tmp

    def startCalibr(self):
        self.progressBar.setValue(0)
        self.ui.actionStop.setEnabled(True)
        self.ui.actionPause.setEnabled(True)

        # if not 'startCalibr' in self.actionThreads:
        # 	self.actionThreads['startCalibr'] = None
        if self.scriptThread is None:
            microV_logger.info("startCalibr: start")
            try:
                importlib.reload(Calibr)
                self.scriptThread = Calibr.calibrThread(self)
                self.scriptThread.finishedSignal.connect(self.progressBar_finished)
                self.scriptThread.dataReadySignal.connect(self.plotter)
                self.scriptThread.start()

            except Exception as ex:
                microV_logger.error("startCalibr: ERROR", exc_info=ex)
                self.stopActiveThreads()
        else:
            self.stopActiveThreads()

    def optimize_start(self):
        self.progressBar.setValue(0)
        self.ui.actionStop.setEnabled(True)
        self.ui.actionPause.setEnabled(True)

        if self.scriptThread is None:
            microV_logger.info("optimize_position: start")
            try:
                importlib.reload(optimize_position)
                self.scriptThread = optimize_position.optimize_positionThread(self)
                self.scriptThread.finishedSignal.connect(self.progressBar_finished)
                self.scriptThread.dataReadySignal.connect(self.plotter)
                self.scriptThread.start()

            except Exception as ex:
                microV_logger.error("optimize_position: ERROR", exc_info=ex)
                self.stopActiveThreads()
        else:
            self.stopActiveThreads()

    def optimize_getCurrentPositions(self):
        for i in range(self.ui.optimize_config.rowCount()):
            ax = self.ui.optimize_config.cellWidget(i, 0).currentText()
            pos = self.abstractGetPosition(ax)
            self.ui.optimize_config.item(i, 1).setText(str(pos))

    def MultiScan_setPoint(self):
        pos = self.piStage.getPosition()
        angle = self.abstractGetPosition(axis="HWP_Polarization")
        pos = np.append(pos, angle)
        currentRow = self.ui.MultiScan_probe.currentRow()
        for i in range(4):
            self.ui.MultiScan_probe.item(currentRow, i).setText(str(pos[i].round(4)))
        if self.ui.MultiScan_probe.cellWidget(currentRow, 4).currentText() == "None":
            self.ui.MultiScan_probe.cellWidget(currentRow, 4).setCurrentIndex(1)
        self.MultiScan_update()

    def MultiScan_setPolarization(self):
        pos = self.piStage.getPosition()
        angle = self.abstractGetPosition(axis="HWP_Polarization")
        currentRow = self.ui.MultiScan_probe.currentRow()
        self.ui.MultiScan_probe.item(currentRow, 3).setText(f"{angle:.3f}")
        if self.ui.MultiScan_probe.cellWidget(currentRow, 4).currentText() == "None":
            self.ui.MultiScan_probe.cellWidget(currentRow, 4).setCurrentIndex(1)
        self.MultiScan_update()

    def MultiScan_moveRowUp(self):
        currentRow = self.ui.MultiScan_probe.currentRow()
        if currentRow == 0:
            return

        for i in [0, 1, 2, 3, 5]:
            current = self.ui.MultiScan_probe.item(currentRow, i).text()
            tmp = self.ui.MultiScan_probe.item(currentRow - 1, i).text()

            self.ui.MultiScan_probe.item(currentRow, i).setText(tmp)
            self.ui.MultiScan_probe.item(currentRow - 1, i).setText(current)

        current = self.ui.MultiScan_probe.cellWidget(currentRow, 4).currentIndex()
        tmp = self.ui.MultiScan_probe.cellWidget(currentRow - 1, 4).currentIndex()

        self.ui.MultiScan_probe.cellWidget(currentRow, 4).setCurrentIndex(tmp)
        self.ui.MultiScan_probe.cellWidget(currentRow - 1, 4).setCurrentIndex(current)

        self.ui.MultiScan_probe.selectRow(currentRow - 1)
        self.MultiScan_update()

    def MultiScan_moveRowDown(self):
        currentRow = self.ui.MultiScan_probe.currentRow()
        if currentRow >= self.ui.MultiScan_probe.rowCount() - 1:
            return

        for i in [0, 1, 2, 3, 5]:
            current = self.ui.MultiScan_probe.item(currentRow, i).text()
            tmp = self.ui.MultiScan_probe.item(currentRow + 1, i).text()

            self.ui.MultiScan_probe.item(currentRow, i).setText(tmp)
            self.ui.MultiScan_probe.item(currentRow + 1, i).setText(current)

        current = self.ui.MultiScan_probe.cellWidget(currentRow, 4).currentIndex()
        tmp = self.ui.MultiScan_probe.cellWidget(currentRow + 1, 4).currentIndex()

        self.ui.MultiScan_probe.cellWidget(currentRow, 4).setCurrentIndex(tmp)
        self.ui.MultiScan_probe.cellWidget(currentRow + 1, 4).setCurrentIndex(current)

        self.ui.MultiScan_probe.selectRow(currentRow + 1)
        self.MultiScan_update()

    def MultiScan_removeRow(self):
        currentRow = self.ui.MultiScan_probe.currentRow()
        if currentRow == 0:
            return

        for i in [0, 1, 2, 3]:
            self.ui.MultiScan_probe.item(currentRow, i).setText("nan")
        self.ui.MultiScan_probe.item(currentRow, 5).setText("")

        self.ui.MultiScan_probe.cellWidget(currentRow, 4).setCurrentIndex(0)

        self.ui.MultiScan_probe.selectRow(currentRow)

        self.MultiScan_update()

    def MultiScan_resetPoint(self):
        self.NP_centers = None
        for j in range(self.ui.MultiScan_probe.rowCount()):
            for i in range(4):
                self.ui.MultiScan_probe.item(j, i).setText("nan")
            self.ui.MultiScan_probe.cellWidget(j, 4).setCurrentIndex(0)
            self.ui.MultiScan_probe.item(j, 5).setText("")
        for Type in self.PROBE_CENTERS:
            for view in self.PROBE_CENTERS[Type]:
                self.PROBE_CENTERS[Type][view].setData(x=[], y=[])
        for view in self.centerLabels:
            for name in self.centerLabels[view]:
                self.centerLabels[view][name].hide()
        self.MultiScan_anchor = 0
        # rows = self.ui.MultiScan_probe.rowCount()
        # for j in range(rows):
        # 	for i in range(4):
        # 		if j == (self.MultiScan_anchor+1):
        # 			self.ui.MultiScan_probe.item(self.MultiScan_anchor+1,i).setBackground(QtWidgets.QColor(49,0,0))
        # 		else:
        # 			self.ui.MultiScan_probe.item(j,i).setBackground(self.ui.MultiScan_probe.item(0,0).background())
        self.MultiScan_update()

    def MultiScan_moveToPoint(self, row=None):
        try:
            if row is None:
                row = self.ui.MultiScan_probe.currentRow()
            pos = [float(self.ui.MultiScan_probe.item(row, i).text()) for i in range(4)]
            self.piStage_moveTo(axis=[0, 1, 2], target=pos[:-1])
            self.abstractMoveTo(axis="HWP_Polarization", target=pos[-1])

        except Exception as ex:
            microV_logger.error("MultiScan_moveToPoint:ERROR", exc_info=ex)

    def MultiScan_getInfo(self):
        rows = self.ui.MultiScan_probe.rowCount()

        dtype = [
            ("X", "f4"),
            ("Y", "f4"),
            ("Z", "f4"),
            ("Polarization", "f4"),
            ("Type", "S8"),
            ("Comment", "S16"),
        ]
        info = np.zeros(0, dtype=dtype)
        for r in range(rows):
            tmp = [float(self.ui.MultiScan_probe.item(r, i).text()) for i in range(4)]
            tmp.append(self.ui.MultiScan_probe.cellWidget(r, 4).currentText())
            tmp.append(self.ui.MultiScan_probe.item(r, 5).text())
            if tmp[4] != "None":
                info = np.hstack((info, np.array(tuple(tmp), dtype=dtype)))
        currentRow = self.ui.MultiScan_probe.currentRow()
        return info, currentRow

    def MultiScan_update(self):
        info, currentIndex = self.MultiScan_getInfo()

        self.settings.setValue("probeCoord", info)
        self.probe_anchor_centers = info
        for Type in self.PROBE_CENTERS:
            w = info["Type"] == Type.encode()
            for view in self.PROBE_CENTERS[Type]:
                self.PROBE_CENTERS[Type][view].setData(x=info[w]["X"], y=info[w]["Y"])

        for i in range(len(info["X"])):
            name = f"{i}"
            Type = info["Type"][i].decode()
            for view in self.centerLabels:

                if name in self.centerLabels[view]:
                    self.centerLabels[view][name].show()
                    self.centerLabels[view][name].setPos(info[i]["X"], info[i]["Y"])

                else:
                    self.centerLabels[view][name] = pg.TextItem(text=name, color="g")
                    effect = QtWidgets.QGraphicsDropShadowEffect()
                    effect.setOffset(1,1)
                    self.centerLabels[view][name].setGraphicsEffect(effect)
                    self.centerLabels[view][name].setHtml(f"<span style='font-size: 12pt'>{name}</span>")
                    self.IMG_VIEW[view].addItem(self.centerLabels[view][name])
                    self.centerLabels[view][name].setPos(info[i]["X"], info[i]["Y"])
        header = self.ui.MultiScan_probe.horizontalHeader()
        header.setSectionResizeMode(5, QtWidgets.QHeaderView.Stretch)
        for i in range(5):
            header.setSectionResizeMode(i, QtWidgets.QHeaderView.ResizeToContents)

    def MultiScan_Scan(self, state):
        self.progressBar.setValue(0)
        self.ui.actionStop.setEnabled(True)
        self.ui.actionPause.setEnabled(True)

        if self.scriptThread is None:
            microV_logger.info("MultiScan: start")
            self.saveUiValues()
            try:
                importlib.reload(MultiScan)
                self.scriptThread_lastStorePath = None
                self.scriptThread = MultiScan.MultiScanThread(self)
                self.scriptThread.finishedSignal.connect(self.progressBar_finished)
                self.scriptThread.progressSignal.connect(self.progressBar_progress)
                self.scriptThread.dataReadySignal.connect(self.plotter)

                state = self.MultiScan_tree.params.saveState()
                self.settings.setValue('MultiScan_params',state)

                self.scriptThread.start()

            except Exception as ex:
                microV_logger.error("MultiScan: ERROR", exc_info=ex)
                self.stopActiveThreads()
        else:
            self.stopActiveThreads()

    def startFast3DScan(self):

        self.progressBar.setValue(0)
        self.ui.actionStop.setEnabled(True)
        self.ui.actionPause.setEnabled(True)

        if self.scriptThread is None:
            print("fastScan3D: start")
            try:
                self.scriptThread = fastScan3DThread(self)
                self.scriptThread.finishedSignal.connect(self.progressBar_finished)
                self.scriptThread.progressSignal.connect(self.progressBar_progress)
                self.scriptThread.dataReadySignal.connect(self.plotter)

                self.scriptThread.start()

            except Exception as ex:
                microV_logger.error("fastScan3D: ERROR", exc_info=ex)
                self.stopActiveThreads()
        else:
            self.stopActiveThreads()

    ############################################################################
    ##########################   Ui   ##########################################
    def initUI(self):

        self.ui.PeakFind_container.setVisible(False)
        self.ui.PeakFind.toggled[bool].connect(self.ui.PeakFind_container.setVisible)
        self.ui.actionMicroscope.toggled[bool].connect(
            lambda x: self.setCalibrSetup("Microscope")
        )
        self.ui.actionHRS.toggled[bool].connect(lambda x: self.setCalibrSetup("HRS"))

        self.dataReadySignal.connect(self.plotter)
        ########################################################################
        #  laser
        self.ui.laserClient_connect.toggled[bool].connect(self.laserClient_connect)
        self.ui.laserOnOff.clicked.connect(self.laserOnOff)
        self.ui.laserWavelength_set.clicked.connect(
            lambda x: self.laserSetWavelength(wl=None, closeShutter=False)
        )
        params = [
            {
                "name": "Laser parameters",
                "type": "group",
                "children": [
                    {
                        "name": "Tunable output",
                        "type": "group",
                        "children": [
                            {
                                "name": "Confocal parameter",
                                "type": "float",
                                "value": 0,
                                "decimals": 4,
                                "siPrefix": True,
                                "suffix": "m",
                                "readonly": True,
                            },
                            {
                                "name": "Waist radius 1/e**2",
                                "type": "float",
                                "value": 0,
                                "decimals": 4,
                                "siPrefix": True,
                                "suffix": "m",
                                "readonly": True,
                            },
                            {
                                "name": "Waist diameter FWHM",
                                "type": "float",
                                "value": 0,
                                "decimals": 4,
                                "siPrefix": True,
                                "suffix": "m",
                                "readonly": True,
                            },
                            {
                                "name": "Average power",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "W",
                                "readonly": True,
                            },
                            {
                                "name": "Peak intensity",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "W/cm**2",
                                "readonly": True,
                            },
                            {
                                "name": "Average power density",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "W/cm**3",
                                "readonly": True,
                            },
                            {
                                "name": "Peak power density",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "W/cm**3",
                                "readonly": True,
                            },
                            {
                                "name": "Pulsewidth",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "s",
                                "readonly": True,
                            },
                        ],
                    },
                    {
                        "name": "Fixed output",
                        "type": "group",
                        "children": [
                            {
                                "name": "Confocal parameter",
                                "type": "float",
                                "value": 0,
                                "decimals": 4,
                                "siPrefix": True,
                                "suffix": "m",
                                "readonly": True,
                            },
                            {
                                "name": "Waist radius 1/e**2",
                                "type": "float",
                                "value": 0,
                                "decimals": 4,
                                "siPrefix": True,
                                "suffix": "m",
                                "readonly": True,
                            },
                            {
                                "name": "Waist diameter FWHM",
                                "type": "float",
                                "value": 0,
                                "decimals": 4,
                                "siPrefix": True,
                                "suffix": "m",
                                "readonly": True,
                            },
                            {
                                "name": "Average power",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "W",
                                "readonly": True,
                            },
                            {
                                "name": "Peak intensity",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "W/cm**2",
                                "readonly": True,
                            },
                            {
                                "name": "Average power density",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "W/cm**3",
                                "readonly": True,
                            },
                            {
                                "name": "Peak power density",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "W/cm**3",
                                "readonly": True,
                            },
                            {
                                "name": "Pulsewidth",
                                "type": "float",
                                "value": 0,
                                "decimals": 3,
                                "siPrefix": True,
                                "suffix": "s",
                                "readonly": True,
                            },
                        ],
                    },
                ],
            },
            {
                "name": "TUN/FIX",
                "type": "group",
                "children": [
                    {"name": "Ratio correction", "type": "bool", "value": False},
                    {"name": "Ratio correction factor", "type": "float", "value": 1},
                ],
            },
        ]

        self.laserPowerInfo_params = Parameter.create(
            name="params", type="group", children=params
        )
        self.laserPowerInfo_tree = ParameterTree()
        self.laserPowerInfo_tree.setParameters(
            self.laserPowerInfo_params, showTop=False
        )

        self.ui.laserPowerInfo_container.addWidget(self.laserPowerInfo_tree)
        try:
            if "laserPowerInfo_params" in self.settings.allKeys():
                state = self.laserPowerInfo_params.saveState()
                try:
                    self.laserPowerInfo_params.restoreState(
                        self.settings.value("laserPowerInfo_params")
                    )
                except:
                    # state = self.laserPowerInfo_params.saveState()
                    self.settings.setValue("laserPowerInfo_params", state)
            else:
                state = self.laserPowerInfo_params.saveState()
                self.settings.setValue("laserPowerInfo_params", state)

        except Exception as ex:
            microV_logger.error("laserPowerInfo_params restore error", exc_info=ex)

        objectives = list(self.configStore["Microscope/Laser/BeamFocusing"])
        lenses = list(self.configStore["HRS/Laser/BeamFocusing"])
        objective_name = self.configStore["Microscope/Laser/BeamFocusing"].attrs[
            "Active"
        ]
        lens_name = self.configStore["HRS/Laser/BeamFocusing"].attrs["Active"]

        lens0 = self.configStore["HRS/Laser/BeamFocusing"][lens_name]
        lens0_timestamp = max(lens0)

        objective0 = self.configStore["Microscope/Laser/BeamFocusing"][objective_name]
        objective0_timestamp = max(objective0)

        params = [
            {
                "name": "Setup parameters",
                "type": "group",
                "children": [
                    {
                        "name": "HRS",
                        "type": "group",
                        "children": [
                            {
                                "name": "Readout configuration",
                                "type": "list",
                                "values": ["90deg", "forward"],
                                "value": "90deg",
                            },
                            {
                                "name": "focusing_by",
                                "type": "list",
                                "values": lenses,
                                "value": lens_name,
                            },
                            {
                                "name": "Focal length[mm]",
                                "type": "float",
                                "value": lens0.attrs["Focal length[mm]"],
                            },
                            {
                                "name": "Waist-sample distance[mm]",
                                "type": "float",
                                "value": lens0.attrs["Waist-sample distance[mm]"],
                            },
                        ],
                    },
                    {
                        "name": "Microscope",
                        "type": "group",
                        "children": [
                            {
                                "name": "Readout configuration",
                                "type": "list",
                                "values": ["forward", "backward"],
                                "value": "forward",
                            },
                            {
                                "name": "focusing_by",
                                "type": "list",
                                "values": objectives,
                                "value": objective_name,
                            },
                            {
                                "name": "Numerical aperture",
                                "type": "float",
                                "value": objective0.attrs["Numerical aperture"],
                            },
                            {
                                "name": "Working distance[mm]",
                                "type": "float",
                                "value": objective0.attrs["Working distance[mm]"],
                            },
                            {
                                "name": "Focal length[mm]",
                                "type": "float",
                                "value": objective0.attrs["Focal length[mm]"],
                            },
                            {
                                "name": "Waist-sample distance[mm]",
                                "type": "float",
                                "value": objective0.attrs["Waist-sample distance[mm]"],
                            },
                        ],
                    },
                ],
            }
        ]
        del lens0
        del objective0

        self.Setup_params = Parameter.create(
            name="params", type="group", children=params
        )
        self.Setup_tree = ParameterTree()
        self.Setup_tree.setParameters(self.Setup_params, showTop=False)

        self.ui.Setup_container.addWidget(self.Setup_tree)
        state = self.Setup_params.saveState()
        self.settings.setValue("Setup_params", state)
        try:
            if "Setup_params" in self.settings.allKeys():
                self.Setup_params.restoreState(self.settings.value("Setup_params"))
            else:
                state = self.Setup_params.saveState()
                self.settings.setValue("Setup_params", state)

        except Exception as ex:
            microV_logger.error("Setup_params restore error", exc_info=ex)

        # self.laserPowerInfo_tree.setStyleSheet("""QTreeView {
        # 	border: red;
        # 	alternate-background-color: #170D9A;
        # 	background: #00235D;
        # }""")







        setup = "HRS"
        if "calibrSetup" in self.settings.allKeys():
            setup = self.settings.value("calibrSetup")
        print(f"{setup=}")
        self.setCalibrSetup(setup, show_hide=False)

        self._generate_toolBar_statusBar()
        #self._generate_statusbar()
        self.setCalibrSetup(setup, show_hide=True)

        powerIntens_TUN_target = pg.SpinBox(
            value=0.1, suffix="W", siPrefix=True, dec=True, step=1.0, minStep=0.001
        )
        powerIntens_TUN_target.setObjectName("laserPowerIntens_TUN_target")
        self.ui.laserPowerIntens_TUN_target_container.addWidget(powerIntens_TUN_target)
        setattr(self.ui, "laserPowerIntens_TUN_target", powerIntens_TUN_target)

        powerIntens_FIX_target = pg.SpinBox(
            value=0.1, suffix="W", siPrefix=True, dec=True, step=1.0, minStep=0.001
        )
        powerIntens_FIX_target.setObjectName("laserPowerIntens_FIX_target")
        self.ui.laserPowerIntens_FIX_target_container.addWidget(powerIntens_FIX_target)
        setattr(self.ui, "laserPowerIntens_FIX_target", powerIntens_FIX_target)

        self.ui.laserPowerIntens_set.clicked.connect(
            lambda x: self.laserPowerIntens_set()
        )
        self.ui.laserPowerIntensIR_set.clicked.connect(
            lambda x: self.laserPowerIntensIR_set()
        )
        self.ui.laserControlPower_or_Intens.currentIndexChanged[int].connect(
            self.on_laserControlPower_or_Intens
        )
        self.ui.actionShutter.toggled[bool].connect(self.laserSetShutter)
        self.ui.actionIRShutter.toggled[bool].connect(self.laserSetIRShutter)



        self.ui.actionExit.toggled.connect(self.closeEvent)
        self.ui.actionStop.triggered.connect(self.actionStop)
        # self.ui.optim1step.clicked.connect(self.optim1step)

        spin = pg.SpinBox(
            value=1e-15, suffix="s", siPrefix=True, dec=True, step=1.0, minStep=0.001
        )
        spin.setObjectName("mocoDelay")
        self.ui.mocoDelay_container.addWidget(spin)
        setattr(self.ui, "mocoDelay", spin)

        self.ui.mocoConnect.toggled[bool].connect(self.mocoConnect)
        self.ui.mocoMoveAbs.clicked.connect(lambda x: self.mocoMoveAbs())
        self.ui.mocoMoveRel_forw.clicked.connect(self.mocoMoveRel_forw)
        self.ui.mocoMoveRel_backw.clicked.connect(self.mocoMoveRel_backw)
        self.ui.mocoHome.clicked.connect(self.mocoHome)
        self.ui.mocoZeroDelay_estimate.clicked.connect(
            lambda x: self.estimate_mocoZeroDelay_position(visible=True)
        )
        self.ui.mocoZeroDelay_move_to_zero.clicked.connect(
            self.mocoZeroDelay_move_to_zero
        )
        self.ui.mocoZeroDelay_add_to_calibr.clicked.connect(
            lambda x: self.mocoZeroDelay_add_to_calibr()
        )
        self.ui.mocoZeroDelay_remove_from_calibr.clicked.connect(
            lambda x: self.mocoZeroDelay_remove_from_calibr()
        )

        self.ui.mocoZeroDelay_plot_calibr.clicked.connect(
            lambda x: self.mocoZeroDelay_plot_calibr()
        )
        self.ui.mocoZeroDelay_save_calibr.clicked.connect(
            self.mocoZeroDelay_save_calibr
        )
        self.ui.mocoZeroDelay_correct_calibr.clicked.connect(
            self.mocoZeroDelay_correct_calibr
        )
        self.ui.mocoZeroDelay_reset_calibr.clicked.connect(
            self.mocoZeroDelay_reset_calibr
        )
        self.ui.mocoZeroDelay_backup_calibr.clicked.connect(self.mocoZeroDelay_backup_calibr)

        self.ui.filtersPiezoStage_connect.toggled[bool].connect(
            self.filtersPiezoStage_connect
        )
        self.ui.filtersPiezoStage_next.clicked.connect(self.filtersPiezoStage_next)
        self.ui.filtersPiezoStage_prev.clicked.connect(self.filtersPiezoStage_prev)
        self.ui.filtersPiezoStage_home.clicked.connect(self.filtersPiezoStage_home)
        self.ui.filtersPiezoStage_filters_set.clicked.connect(
            self.filtersPiezoStage_filters_set
        )

        self.expData_filePath_find.clicked.connect(self.expData_filePath_find_dialog)
        # self.expData_filePath_find.clicked.connect(self.MultiScan_filePath_find)

        # self.expData_filePath_find.clicked.connect(self.scan3D_path_dialog)

        self.ui.centerDetect_find.clicked.connect(self.centerDetect_find)

        # self.ui.scan3D_generateMask.clicked.connect(self.generate2Dmask)

        self.ui.ccs200Connect.toggled[bool].connect(self.ccs200Connect)
        self.ui.ccs200Exposure.valueChanged[float].connect(self.ccs200SetExposure)
        self.ui.ccs200GetSpectra.clicked.connect(
            lambda x: self.ccs200GetSpectra(plot=True)
        )
        self.ui.ccs200GetBaseline.clicked.connect(
            lambda x: self.ccs200GetBaseline(plot=True)
        )

        self.ui.connect_DAQmx.toggled[bool].connect(self.connect_DAQmx)
        self.ui.DAQmx_find_shift.clicked.connect(self.optimizeDAQmx)

        self.ui.HWP_TUN_connect.toggled[bool].connect(self.HWP_TUN_connect)
        self.ui.HWP_TUN_go.clicked.connect(self.HWP_TUN_go)
        self.ui.HWP_TUN_go_min_power_angle.clicked.connect(
            self.HWP_TUN_go_min_power_angle
        )
        self.ui.HWP_TUN_go_home.clicked.connect(self.HWP_TUN_go_home)
        self.ui.HWP_TUN_negative_step.clicked.connect(self.HWP_TUN_negative_step)
        self.ui.HWP_TUN_positive_step.clicked.connect(self.HWP_TUN_positive_step)

        self.ui.HWP_FIX_connect.toggled[bool].connect(self.HWP_FIX_connect)
        self.ui.HWP_FIX_go.clicked.connect(self.HWP_FIX_go)
        self.ui.HWP_FIX_go_min_power_angle.clicked.connect(
            self.HWP_FIX_go_min_power_angle
        )
        self.ui.HWP_FIX_go_home.clicked.connect(self.HWP_FIX_go_home)
        self.ui.HWP_FIX_negative_step.clicked.connect(self.HWP_FIX_negative_step)
        self.ui.HWP_FIX_positive_step.clicked.connect(self.HWP_FIX_positive_step)

        tmp = self.configStore[f"{self.calibrTools.setup}/HWP_TUN"]
        timestamp = max(tmp)
        self.ui.HWP_TUN_min_power_angle.setValue(
            tmp[timestamp].attrs["power2angle_interpLimits"][0]
        )
        del tmp

        tmp = self.configStore[f"{self.calibrTools.setup}/HWP_FIX"]
        timestamp = max(tmp)
        self.ui.HWP_FIX_min_power_angle.setValue(
            tmp[timestamp].attrs["power2angle_interpLimits"][0]
        )
        del tmp

        self.ui.HWP_Polarization_connect.toggled[bool].connect(
            self.HWP_Polarization_connect
        )
        self.ui.HWP_Polarization_go.clicked.connect(self.HWP_Polarization_go)
        # self.ui.HWP_Polarization_go_min_power_angle.clicked.connect(
        # 	self.HWP_Polarization_go_min_power_angle)
        self.ui.HWP_Polarization_go_home.clicked.connect(self.HWP_Polarization_go_home)
        self.ui.HWP_Polarization_negative_step.clicked.connect(
            self.HWP_Polarization_negative_step
        )
        self.ui.HWP_Polarization_positive_step.clicked.connect(
            self.HWP_Polarization_positive_step
        )

        self.ui.arduinoStage_connect.toggled[bool].connect(self.arduinoStage_connect)

        self.ui.arduinoStage_lock.toggled[bool].connect(self.arduinoStage_lock)
        self.ui.arduinoStage1_lock.toggled[bool].connect(self.arduinoStage1_lock)

        self.ui.arduinoStage_go.clicked.connect(self.arduinoStage_go)
        self.ui.arduinoStage_rewrite_position.clicked.connect(
            self.arduinoStage_rewrite_position
        )
        self.ui.arduinoStage_go_home.clicked.connect(self.arduinoStage_go_home)
        self.ui.arduinoStage_negative_step.clicked.connect(
            self.arduinoStage_negative_step
        )
        self.ui.arduinoStage_positive_step.clicked.connect(
            self.arduinoStage_positive_step
        )

        # self.ui.arduinoStage1_connect.toggled[bool].connect(
        # 	self.arduinoStage1_connect)
        self.ui.arduinoStage1_go.clicked.connect(self.arduinoStage1_go)
        self.ui.arduinoStage1_rewrite_position.clicked.connect(
            self.arduinoStage1_rewrite_position
        )
        self.ui.arduinoStage1_go_home.clicked.connect(self.arduinoStage1_go_home)
        self.ui.arduinoStage1_negative_step.clicked.connect(
            self.arduinoStage1_negative_step
        )
        self.ui.arduinoStage1_positive_step.clicked.connect(
            self.arduinoStage1_positive_step
        )

        ###############################################
        ##### commutation
        self.ui.commutation_mirror0_set.clicked.connect(self.commutation_mirror0_set)
        self.ui.commutation_mirror0_reset.clicked.connect(
            self.commutation_mirror0_reset
        )
        self.ui.commutation_mirror0_go.clicked.connect(
            lambda x: self.commutation_mirror0_moveTo(
                target=self.ui.commutation_mirror0_position.value(), wait=True
            )
        )
        self.ui.commutation_mirror0_home.clicked.connect(self.commutation_mirror0_home)

        # self.ui.commutation_mirror1_set.clicked.connect(self.commutation_mirror1_set)
        # self.ui.commutation_mirror1_reset.clicked.connect(self.commutation_mirror1_reset)
        # self.ui.commutation_mirror1_go.clicked.connect(lambda x: self.commutation_mirror1_moveTo(target=self.ui.commutation_mirror1_position.value(), wait=True))
        # self.ui.commutation_mirror1_home.clicked.connect(self.commutation_mirror1_home)
        #
        #
        # self.ui.commutation_mirror2_set.clicked.connect(self.commutation_mirror2_set)
        # self.ui.commutation_mirror2_reset.clicked.connect(self.commutation_mirror2_reset)
        # self.ui.commutation_mirror2_go.clicked.connect(lambda x: self.commutation_mirror2_moveTo(target=self.ui.commutation_mirror2_position.value(), wait=True))
        # self.ui.commutation_mirror2_home.clicked.connect(self.commutation_mirror1_home)
        #

        ################################################
        ###### laserBeamShift

        self.ui.laserBeamShift_V_moveTo_target.clicked.connect(
            lambda x: self.laserBeamShift_V_moveTo_target()
        )
        self.ui.laserBeamShift_V_reset.clicked.connect(self.laserBeamShift_V_reset)
        self.ui.laserBeamShift_V_negativeStep.clicked.connect(
            self.laserBeamShift_V_negativeStep
        )
        self.ui.laserBeamShift_V_positiveStep.clicked.connect(
            self.laserBeamShift_V_positiveStep
        )
        self.ui.laserBeamShift_V_moveHome.clicked.connect(
            self.laserBeamShift_V_moveHome
        )
        self.ui.laserBeamShift_moveToZero.clicked.connect(
            self.laserBeamShift_moveToZero
        )

        self.ui.laserBeamShift_calibr_add.clicked.connect(
            lambda x: self.laserBeamShift_add_to_calibr()
        )
        self.ui.laserBeamShift_calibr_remove.clicked.connect(
            lambda x: self.laserBeamShift_remove_from_calibr()
        )
        self.ui.laserBeamShift_calibr_preview.clicked.connect(
            lambda x: self.laserBeamShift_plot_calibr()
        )
        self.ui.laserBeamShift_calibr_save_curve.clicked.connect(
            self.laserBeamShift_save_calibr
        )
        self.ui.laserBeamShift_calibr_reset.clicked.connect(
            self.laserBeamShift_calibr_reset
        )
        self.ui.laserBeamShift_calibr_backup.clicked.connect(
            self.laserBeamShift_calibr_backup
        )
        # self.ui.mocoZeroDelay_correct_calibr.clicked.connect(self.mocoZeroDelay_correct_calibr)
        # self.ui.mocoZeroDelay_reset_calibr.clicked.connect(self.mocoZeroDelay_reset_calibr)

        self.ui.laserBeamShift_H_moveTo_target.clicked.connect(
            lambda x: self.laserBeamShift_H_moveTo_target()
        )
        self.ui.laserBeamShift_H_reset.clicked.connect(self.laserBeamShift_H_reset)
        self.ui.laserBeamShift_H_negativeStep.clicked.connect(
            self.laserBeamShift_H_negativeStep
        )
        self.ui.laserBeamShift_H_positiveStep.clicked.connect(
            self.laserBeamShift_H_positiveStep
        )
        self.ui.laserBeamShift_H_moveHome.clicked.connect(
            self.laserBeamShift_H_moveHome
        )
        # self.ui.laserBeamShift_H_moveToZero.clicked.connect(self.laserBeamShift_H_moveToZero)
        self.ui.laserBeamShift_H_lock.toggled[bool].connect(self.laserBeamShift_H_lock)

        self.ui.Pi_X_go.clicked.connect(self.Pi_X_go)
        self.ui.Pi_Y_go.clicked.connect(self.Pi_Y_go)
        self.ui.Pi_Z_go.clicked.connect(self.Pi_Z_go)
        self.ui.Pi_XYZ_50mkm.clicked.connect(self.Pi_XYZ_50mkm)
        self.ui.Pi_Set.clicked.connect(self.Pi_Set)
        self.ui.Pi_set_tiltCompensation.clicked.connect(self.Pi_set_tiltCompensation)
        self.ui.Pi_autoZero.clicked.connect(self.Pi_autoZero)
        self.piStageLiveTimer.timeout.connect(self.onPiStageLiveTimer)
        self.ui.zSlider.valueChanged[int].connect(self.setPiStageZPosition)
        self.ui.piStage_connect.toggled[bool].connect(self.piStage_connect)

        self.ui.piStageWaistShift_estimate.clicked.connect(
            lambda x: self.estimate_piStageWaistShift_shift(visible=True)
        )
        # self.ui.piStageWaistShift_move_to_zero.clicked.connect(self.piStage_moveTo_corrected)#lambda x: self.piStage_moveTo(target=self.ui.piStageWaistShift_position.value()))
        self.ui.piStageWaistShift_add_to_calibr.clicked.connect(
            lambda x: self.piStageWaistShift_add_to_calibr()
        )
        self.ui.piStageWaistShift_plot_calibr.clicked.connect(
            lambda x: self.piStageWaistShift_plot_calibr()
        )
        self.ui.piStageWaistShift_save_calibr.clicked.connect(
            self.piStageWaistShift_save_calibr
        )
        self.ui.piStageWaistShift_correct_calibr.clicked.connect(
            self.piStageWaistShift_correct_calibr
        )
        self.ui.piStageWaistShift_reset_calibr.clicked.connect(
            self.piStageWaistShift_reset_calibr
        )

        # self.ui.connect_rotPiezoStage.toggled[bool].connect(self.connect_rotPiezoStage)

        # self.ui.rotPiezoStage_Go.clicked.connect(self.rotPiezoStage_Go)

        self.ui.PicoScope_connect.toggled[bool].connect(self.PicoScope_connect)
        self.ui.PicoScope_set.clicked.connect(lambda x: self.PicoScope_setConfig())

        self.ui.ThorCamera_connect.toggled[bool].connect(self.ThorCamera_connect)
        # self.ui.ThorCamera_live.toggled[bool].connect(self.ThorCamera_live)
        self.ui.ThorCamera_exposure.valueChanged[float].connect(
            self.ThorCamera_exposure
        )
        self.ui.ThorCamera_resetBG.clicked.connect(self.ThorCamera_resetBG)
        # self.ui.ThorCamera_rec.toggled[bool].connect(self.ThorCamera_rec)
        self.ui.ThorCamera_light.toggled[bool].connect(self.ThorCamera_light)
        self.ui.ThorCamera_lightLevel.valueChanged[int].connect(
            self.ThorCamera_setLightLevel
        )

        self.ui.ThorCamera_imgPathSelect.clicked.connect(self.ThorCamera_imgPathSelect)
        self.ui.ThorCamera_imgSave.clicked.connect(self.ThorCamera_imgSave)



        # self.ui.shamrockConnect.toggled[bool].connect(self.shamrockConnect)
        self.ui.shamrockSetWavelength.clicked.connect(
            lambda x: self.shamrockSetWavelength()
        )
        # self.ui.shamrockPort.currentIndexChanged[int].connect(self.shamrockSetPort)
        self.ui.shamrockPort_set.clicked.connect(
            lambda x: self.shamrockSetPort(self.ui.shamrockPort.currentIndex())
        )
        # self.ui.shamrockGrating.currentIndexChanged[int].connect(self.shamrockSetGrating)
        self.ui.shamrockGrating_set.clicked.connect(
            lambda x: self.shamrockSetGrating(self.ui.shamrockGrating.currentIndex())
        )
        self.ui.shamrockWavelength_offset_set.clicked.connect(
            lambda x: self.shamrockSetWavelength_offset(
                self.ui.shamrockWavelength_offset.value()
            )
        )
        self.ui.shamrockWavelength_adaptive_centering_by.currentIndexChanged[
            int
        ].connect(self.on_shamrockWavelength_adaptive_centering_by_currentIndexChanged)

        self.ui.andorCamera_Shamrock_connect.toggled[bool].connect(
            self.andorCamera_Shamrock_connect
        )
        self.ui.shamrockFocusingMirror_set.clicked.connect(
            lambda x: self.shamrockSetFocusingMirror()
        )

        wls = (
            calcVisibleBands()
        )  # ['w1','2w1','3w1','w2','2w2','3w2','w1+w2','2w1-w2','2w2-w1','2w1+w2','2w2+w1']

        params = [
            {
                "name": "Default",
                "type": "group",
                "children": [
                    {"name": name, "type": "float", "value": wls[name], "decimals": 4}
                    for name in wls
                ],
            },
            {
                "name": "Custom",
                "type": "group",
                "children": [
                    {
                        "name": "All",
                        "type": "group",
                        "children": [
                            {
                                "name": "start",
                                "type": "float",
                                "value": 0,
                                "decimals": 4,
                            },
                            {
                                "name": "end",
                                "type": "float",
                                "value": 1200,
                                "decimals": 4,
                            },
                        ],
                    },
                ],
            },
        ]

        self.andorCamera_integrRanges_params = Parameter.create(
            name="params", type="group", children=params
        )
        self.andorCamera_integrRanges_tree = ParameterTree()
        self.andorCamera_integrRanges_tree.setParameters(
            self.andorCamera_integrRanges_params, showTop=False
        )

        self.andorCamera_integrRanges_tree.setStyleSheet(
            """QTreeView {
			border: red;
			alternate-background-color: #170D9A;
			background: #00235D;
		}"""
        )

        self.ui.andorCamera_integrRanges_container.addWidget(
            self.andorCamera_integrRanges_tree
        )

        adaptive_centering_by = []
        for child in self.andorCamera_integrRanges_params.childs[0]:
            adaptive_centering_by.append(child.name())
        # adaptive_centering_by.append('factor')

        self.ui.shamrockWavelength_adaptive_centering_by.insertItems(
            1, adaptive_centering_by
        )
        SFG_index = adaptive_centering_by.index("w1+w2") + 1
        self.ui.shamrockWavelength_adaptive_centering_by.setCurrentIndex(SFG_index)

        # self.ui.andorCameraConnect.toggled[bool].connect(self.andorCameraConnect)
        self.ui.andorCameraExposure.valueChanged[float].connect(
            self.andorCameraSetExposure
        )
        # self.ui.andorCameraGetData.toggled[bool].connect(self.andorCameraGetData)
        self.ui.andorCameraReadoutMode.currentIndexChanged[int].connect(
            self.andorCameraSetReadoutMode
        )
        # self.ui.andorCameraGetBaseline.clicked.connect(self.andorCameraGetBaseline)
        self.ui.actionBaseline.triggered.connect(self.getBaseline_oneShot)
        self.ui.actionOneShot.triggered.connect(self.getData_oneShot)
        # self.ui.andorCameraCleanLines.clicked.connect(self.andorCameraCleanLines)

        self.ui.readme_save.clicked.connect(self.readme_save)

        self.ui.optimize_start.clicked.connect(self.optimize_start)
        self.ui.optimize_getCurrentPositions.clicked.connect(
            self.optimize_getCurrentPositions
        )
        optim_chan = ["PicoScope/data/ChA", "PicoScope/data/ChB"]

        for child in self.andorCamera_integrRanges_params.childs[0]:
            optim_chan.append("AndorCamera/data/" + child.name())
        optim_chan.append("AndorCamera/data/All")

        self.ui.optimize_chan.insertItems(1, optim_chan)

        # optim_axes = [self.ui.optimize_axis.itemText(i) for i in range(self.ui.optimize_axis.count())]
        # axes = list(AXES_UNITS_DTYPES.keys())
        # axes.append('XY')
        # axes.append('XYZ')
        # self.ui.optimize_axis.clear()
        # axes.sort()
        # self.ui.optimize_axis.addItems(axes)
        # self.ui.HWP_stepper_Connect.toggled[bool].connect(self.HWP_stepper_Connect)
        # self.ui.HWP_stepper_MoveTo_Go.clicked.connect(self.HWP_stepper_MoveTo_Go)
        # self.ui.HWP_stepper_CW.clicked.connect(self.HWP_stepper_CW)
        # self.ui.HWP_stepper_CCW.clicked.connect(self.HWP_stepper_CCW)
        # self.ui.HWP_stepper_Reset.clicked.connect(self.HWP_stepper_Reset)
        ############### optimize
        # self.ui.optimize_config.setRowCount(4)
        values = [
            ("X", 25, -0.5, 0.5, 0.05),
            ("Y", 25, -0.5, 0.5, 0.05),
            ("Z", 25, -5.0, 5.0, 0.05),
            ("-", 25, -50, 50, 0.05),
            ("-", 25, -0.1, 0.1, 0.05),

        ]
        self.ui.optimize_config.setRowCount(len(values))
        axes = ["-"] + list(AXES_UNITS_DTYPES.keys())
        for i in range(self.ui.optimize_config.rowCount()):
            combo = QtWidgets.QComboBox()
            combo.insertItems(1, axes)
            combo.setCurrentIndex(axes.index(values[i][0]))
            combo.adjustSize()
            self.ui.optimize_config.setCellWidget(i, 0, combo)
            # print(values[i])
            for j in range(4):
                # print(values[i][j+1])
                item = QtWidgets.QTableWidgetItem(str(values[i][j + 1]))
                self.ui.optimize_config.setItem(i, j + 1, item)

        self.ui.optimize_config.setVerticalHeaderLabels(
            [str(i) for i in range(self.ui.optimize_config.rowCount())]
        )
        self.ui.optimize_config.resizeColumnsToContents()
        self.ui.optimize_config.resizeRowsToContents()

        self.ui.pm100Connect.toggled[bool].connect(self.pm100Connect)
        self.ui.pm100Average.valueChanged[int].connect(self.pm100Average)

        self.ui.MultiScan_setPoint.clicked.connect(self.MultiScan_setPoint)
        self.ui.MultiScan_setPolarization.clicked.connect(
            self.MultiScan_setPolarization
        )

        self.ui.MultiScan_resetPoint.clicked.connect(self.MultiScan_resetPoint)
        self.ui.MultiScan_moveToPoint.clicked.connect(
            lambda x: self.MultiScan_moveToPoint()
        )
        self.ui.MultiScan_update.clicked.connect(self.MultiScan_update)

        self.ui.MultiScan_moveRowDown.clicked.connect(self.MultiScan_moveRowDown)
        self.ui.MultiScan_moveRowUp.clicked.connect(self.MultiScan_moveRowUp)
        self.ui.MultiScan_removeRow.clicked.connect(self.MultiScan_removeRow)

        # self.ui.scan1D_Scan.clicked.connect(self.scan1D_Scan)

        # self.ui.scanND_loadScript.clicked.connect(self.scanND_loadScript)
        self.ui.scanND_Scan.clicked.connect(self.scanND_Scan)

        # self.ui.start3DScan.clicked.connect(self.start3DScan)

        self.scan3D_config = {}
        for i, k in enumerate(["X", "Y", "Z"]):
            tmp = {
                #'name': getattr(self.ui,f'scan3D_Axis{i}_name'),
                #'zigzag': getattr(self.ui,f'scan3D_Axis{i}_zigzag'),
                "start": getattr(self.ui, f"scan3D_Axis{i}_start"),
                "end": getattr(self.ui, f"scan3D_Axis{i}_end"),
                "step": getattr(self.ui, f"scan3D_Axis{i}_step"),
                "scanByGrid": getattr(self.ui, f"scan3D_Axis{i}_scanByGrid"),
                "Grid": getattr(self.ui, f"scan3D_Axis{i}_Grid"),
            }
            tmp["start"].valueChanged[float].connect(self.syncRectROI_table)
            tmp["end"].valueChanged[float].connect(self.syncRectROI_table)

            self.scan3D_config[k] = tmp

        self.scan3D_config["scanOrder"] = getattr(self.ui, f"scan3D_scanOrder")

        self.ui.scan3D_Scan.clicked.connect(self.scan3D_Scan)

        self.ui.fast3DScan.clicked.connect(self.startFast3DScan)

        self.ui.actionCalibr.triggered.connect(self.startCalibr)

        N_rows = 100
        self.ui.MultiScan_probe.setRowCount(N_rows)

        for i in range(N_rows):
            for j in range(4):
                item = QtWidgets.QTableWidgetItem("nan")
                self.ui.MultiScan_probe.setItem(i, j, item)
            combo = QtWidgets.QComboBox()
            combo.insertItems(1, ["None", "Read", "Anchor", "BG"])
            combo.adjustSize()
            self.ui.MultiScan_probe.setCellWidget(i, 4, combo)
            item = QtWidgets.QTableWidgetItem("")
            self.ui.MultiScan_probe.setItem(i, 5, item)
        self.ui.MultiScan_probe.setVerticalHeaderLabels([str(i) for i in range(N_rows)])
        self.ui.MultiScan_probe.resizeColumnsToContents()
        self.ui.MultiScan_probe.resizeRowsToContents()

        self.ui.MultiScan_Scan.clicked.connect(self.MultiScan_Scan)


        self.linesManager = TableOfLines(max_lines_N=50)

        self.ui.tab_Plot_container.addWidget(self.linesManager)

        self.ui.actionClean.triggered.connect(self.viewCleanLines)
        self.ui.actionClean.triggered.connect(self.linesManager.removeAll)

        # self.ui.saveGuiConfig.clicked.connect(self.saveGuiConfig)








        # self.activeThreadsCounterTimer.timeout.connect(self.countActiveThreads)
        # self.activeThreadsCounterTimer.start(1000)

        self.ui.laserWavelength.valueChanged[int].connect(
            self.on_laserWavelength_valueChanged
        )
        self.ui.laserPrecompensation_set.clicked.connect(lambda x: self.laserSetPrecompensation())
        self.ui.laserPrecompensation_moveToCalibr.clicked.connect(lambda x: self.laserPrecompensation_moveToCalibr())
        self.ui.laserPrecompensation_preview.clicked.connect(lambda x: self.laserPrecompensation_plot_calibr())

        self.ui.laserPrecompensation_save.clicked.connect(lambda x: self.laserSavePrecompensation())
        self.ui.laserPrecompensation_remove.clicked.connect(lambda x: self.laserRemovePrecompensation())

        # self.ui.MultiScan_optim3D.toggled.connect(lambda: self.toggleGroup(self.ui.MultiScan_optim3D))
        # self.ui.MultiScan_rescan2D.toggled.connect(lambda: self.toggleGroup(self.ui.MultiScan_rescan2D))
        # self.ui.fastScan_groupBox.setVisible(False)

        ########################################################################
        ########################################################################
        ########################################################################

        self.tabColors = {0: "rgba(165, 42, 42, 100)",
            1: "rgba(95, 158, 160, 100)",
            2: "rgba(220, 20, 60, 100)",
            3: "rgba(85, 107, 47, 100)",
            4: "rgba(210, 105, 30, 100)",
            5: "rgba(72, 61, 139, 100)",
            6: "rgba(178, 34, 34, 100)",
            7: "rgba(251, 0, 130, 100)",
            8: "rgba(245, 107, 47, 100)",
            9: "rgba(242, 105, 30, 100)",
            10: "rgba(248, 61, 139, 100)",
            11: "rgba(242, 34, 34, 100)",
            12: "rgba(251, 0, 130, 100)",
            }
        self.ui.configTabWidget.tabBar().currentChanged.connect(self.styleTabs)



        #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        self.DOCK = {}

        self.IMG_VIEW = {}
        self.PLOT_WIDGET = {}
        self.IMG_GRID = {}
        self.rectROI = {}
        self.PROBE_CENTERS = {"Anchor": {}, "Read": {}, "BG": {}}
        self.piStage_POSITION = {}
        self.RULER_LINES = {}
        self._ruler_points = {}
        self.RULER_LINES_DISTANCE = {}

        self.centerLabels = {}
        self.piStage_position_cam_limits = {}
        self.piStage_position_cam_limits1 = {}


        self.colors = [  # range(0,255)
            (255, 215, 0),
            (10, 255, 10),
            (81, 99, 242),
            (230, 162, 0),
            (81, 255, 242),
            (218, 255, 0),
            (218, 122, 0),
            (47, 187, 172),
            (0, 255, 0),
            (131, 170, 255),
            (224, 170, 10),
            (120, 223, 230),
            (255, 215, 0),
            (0, 255, 255)[::-1],
            (10, 255, 10)[::-1],
            (81, 99, 242)[::-1],
            (230, 162, 0)[::-1],
            (81, 255, 242)[::-1],
            (218, 255, 0)[::-1],
            (218, 122, 0)[::-1],
            (47, 187, 172)[::-1],
            (131, 170, 255)[::-1],
            (120, 223, 230)[::-1],
            (224, 170, 10)[::-1],
        ] * 2


        self.dockarea = DockArea()
        self.ui.ViewerContainer.addWidget(self.dockarea)
        self.dockarea.setStyleSheet("margin:0px;padding:0px;")

        for widget in ['Spectra', 'Scan', 'Preview']:
            self.DOCK[widget] = Dock(widget)  # , size=(500,400))
            self.PLOT_WIDGET[widget] = pg.PlotWidget(name=widget)
            self.DOCK[widget].addWidget(self.PLOT_WIDGET[widget])
            self.PLOT_WIDGET[widget].addLegend()

            self.linesManager.addPage(widget, self.PLOT_WIDGET[widget])


        self.line_markers = {}
        self.defined_line_styles = {
            "w1": pg.mkPen(color=self.colors[0]),  # , style=QtCore.Qt.DashLine),
            "2w1": pg.mkPen(color=self.colors[1]),  # , style=QtCore.Qt.DashLine),
            "3w1": pg.mkPen(color=self.colors[2]),  # , style=QtCore.Qt.DashLine),
            "w2": pg.mkPen(color=self.colors[3]),  # , style=QtCore.Qt.DashDotLine),
            "2w2": pg.mkPen(color=self.colors[4]),  # , style=QtCore.Qt.DashDotLine),
            "3w2": pg.mkPen(color=self.colors[5]),  # , style=QtCore.Qt.DashDotLine),
            "w1+w2": pg.mkPen(color=self.colors[6]),  # , style=QtCore.Qt.DotLine),
            "2w1-w2": pg.mkPen(color=self.colors[7]),  # , style=QtCore.Qt.DotLine),
            "2w2-w1": pg.mkPen(color=self.colors[8]),  # , style=QtCore.Qt.DotLine),
            "2w1+w2": pg.mkPen(color=self.colors[9]),  # , style=QtCore.Qt.DotLine),
            "2w2+w1": pg.mkPen(color=self.colors[10]),  # , style=QtCore.Qt.DotLine),
            "All": pg.mkPen(color=self.colors[11]),  # , style=QtCore.Qt.DotLine),
            "Custom": pg.mkPen(color=self.colors[12]),  # , style=QtCore.Qt.DotLine),
        }
        label_positions = np.linspace(0.5, 0.9, len(self.defined_line_styles))
        for i, key in enumerate(self.defined_line_styles):

            self.line_markers[key] = pg.InfiniteLine(
                angle=90,
                label=key,
                labelOpts={"position": label_positions[i]},
                pen=self.defined_line_styles[key],
            )

        self.defined_line_styles["ChA"] = pg.mkPen(color=self.colors[13])
        self.defined_line_styles["ChB"] = pg.mkPen(color=self.colors[14])
        self.defined_line_styles["power"] = pg.mkPen(color=self.colors[15])
        # self.defined_line_styles['spectrum'] = pg.mkPen(color=self.colors[16])
        self.defined_line_styles["baseline"] = pg.mkPen(color=self.colors[17])
        self.defined_line_styles["max"] = pg.mkPen(color=self.colors[18])

        sig_wl = calcVisibleBands(lambda2=680)
        for k in sig_wl:
            self.line_markers[k].setPos(sig_wl[k])
            self.PLOT_WIDGET["Spectra"].addItem(self.line_markers[k])


        data = np.zeros((100, 100, 10))

        color_gradients = [
            [(0, 0, 0), (204, 255, 255)],
            [(0, 0, 0), (255, 214, 112)],
            [(0, 0, 0), (255, 255, 255)],
        ]*10

        for i, view in enumerate(["A", "B", "cam"]):
            self.DOCK[view] = Dock(view)  # , size=(500,400))
            self.IMG_VIEW[view] = pg.ImageView()
            self.IMG_VIEW[view].setToolTip(view)
            self.DOCK[view].addWidget(self.IMG_VIEW[view])

            self.IMG_VIEW[view].setImage(data)
            self.IMG_VIEW[view].view.invertY(True)
            # arrowX = pg.ArrowItem(angle=-180,  brush='r')
            # arrowY = pg.ArrowItem(angle=-90,  brush='g')
            # arrowX.setPos(100,0)
            # arrowY.setPos(0,100)
            # self.IMG_VIEW['A'].addItem(arrowX)
            # self.IMG_VIEW['A'].addItem(arrowY)

            self.IMG_GRID[view] = pg.GridItem('g', 'g')
            self.IMG_VIEW[view].addItem(self.IMG_GRID[view])

            self.rectROI[view] = pg.RectROI([0, 0], [100, 100], pen=(0, 9), rotatable=False, movable=False)
            self.rectROI[view].addTranslateHandle(pos=[0,0])
            self.IMG_VIEW[view].addItem(self.rectROI[view])

            self.PROBE_CENTERS["Read"][view] = pg.ScatterPlotItem(
                size=15,
                pen=pg.mkPen(None),
                brush=pg.mkBrush(255, 0, 100),
                symbol="x",
            )
            self.PROBE_CENTERS["Anchor"][view] = pg.ScatterPlotItem(
                size=15,
                pen=pg.mkPen(255,0,0),
                brush=pg.mkBrush(0, 255, 153),
                symbol="star",
            )
            self.PROBE_CENTERS["BG"][view] = pg.ScatterPlotItem(
                size=10, pen=pg.mkPen(120, 0, 25, width=3), brush=None, symbol="h"
            )

            self.IMG_VIEW[view].addItem(self.PROBE_CENTERS["Read"][view])
            self.IMG_VIEW[view].addItem(self.PROBE_CENTERS["Anchor"][view])
            self.IMG_VIEW[view].addItem(self.PROBE_CENTERS["BG"][view])

            self.RULER_LINES[view] = pg.PlotDataItem(
                [],
                pen=pg.mkPen(color="g", style=QtCore.Qt.DashLine),
                symbolBrush=(0, 0, 200),
                symbolPen="w",
                symbol="o",
                symbolSize=5,
            )
            self.IMG_VIEW[view].addItem(self.RULER_LINES[view])
            self.RULER_LINES_DISTANCE[view] = pg.TextItem(color=(255, 255, 236), fill=(50,50,50,100))
            self.IMG_VIEW[view].addItem(self.RULER_LINES_DISTANCE[view])
            self._ruler_points[view] = []

            cmap = pg.ColorMap(pos=[0.0, 1.0], color=color_gradients[i])
            self.IMG_VIEW[view].setColorMap(cmap)
            self.rectROI[view].sigRegionChanged.connect(self.syncRectROI)

            self.piStage_POSITION[view] = TargetItem.TargetItem(
                pen=pg.mkPen(255, 0, 68, width=2), movable=False
            )
            effect = QtWidgets.QGraphicsDropShadowEffect()
            self.piStage_POSITION[view].setGraphicsEffect(effect)
            effect.setColor(QtGui.QColor('white'))
            effect.setOffset(1,1)


            self.IMG_VIEW[view].addItem(self.piStage_POSITION[view])

            self.centerLabels[view] = {}

            self.piStage_position_cam_limits[view] = pg.PlotCurveItem(
                pen=pg.mkPen(255, 255, 0)
            )
            self.IMG_VIEW[view].addItem(self.piStage_position_cam_limits[view])

            self.piStage_position_cam_limits1[view] = pg.PlotCurveItem(
                pen=pg.mkPen(255, 255, 0, style=QtCore.Qt.DashLine)
            )
            self.IMG_VIEW[view].addItem(self.piStage_position_cam_limits1[view])
            # self.piStage_position_cam_limits[view].setData(
            # 	x=np.array([0,100,100,0,0]),#+self.piStage_position_cam_shift[0],
            # 	y=np.array([0,0,100,100,0])#+self.piStage_position_cam_shift[1]
            # 	)

        # if "piStage_position_cam_shift" in self.settings.allKeys():
        #     self.piStage_position_cam_shift = self.settings.value(
        #         "piStage_position_cam_shift"
        #     )
        #     if np.any(np.isnan(self.piStage_position_cam_shift)):
        #         real_position = self.piStage.getPosition()
        #         self.piStage_position_cam_shift = -real_position[[0, 1]]
        #         self.settings.setValue(
        #             "piStage_position_cam_shift", self.piStage_position_cam_shift
        #         )
        #         # self.piStage_position_cam_prev_pos = (0, 0)
        # else:
        #     real_position = self.piStage.getPosition()
        #     self.piStage_position_cam_shift = -real_position[[0, 1]]
        #     # self.piStage_position_cam_shift_prevPos = (0, 0)
        self.DOCK['cam'].label.setToolTip(
        """Viewport: ThorCamera
        Shift+Click: move to point
        Alt+Click: calibrate target -> real beam position
        Crtl+Click: measure distance.
        """)
        self.DOCK['A'].label.setToolTip(
        """Viewport: A
        Shift+Click: move to point
        Crtl+Click: measure distance.
        """)
        self.DOCK['B'].label.setToolTip(
        """Viewport: B
        Shift+Click: move to point
        Crtl+Click: measure distance.
        """)

        self.toolpath = pg.PlotCurveItem(
            pen=(255, 0, 0, 150), symbolBrush=(255, 0, 0, 150), symbolPen="r"
        )
        self.IMG_VIEW["A"].addItem(self.toolpath)
        self.toolpath.setZValue(100)

        self.IMG_GRID["A"].scene().sigMouseClicked.connect(
            lambda event: self.onImgPos(event, sender="A")
        )
        self.IMG_GRID["B"].scene().sigMouseClicked.connect(
            lambda event: self.onImgPos(event, sender="B")
        )
        self.IMG_GRID["cam"].scene().sigMouseClicked.connect(
            lambda event: self.onImgPos(event, sender="cam")
        )

        pw_preview_proxy = pg.SignalProxy(
            self.PLOT_WIDGET["Preview"].scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved
        )
        self.PLOT_WIDGET["Preview"].scene().sigMouseMoved.connect(
            lambda x: self.mouseMoved(x, view_name="Preview")
        )
        self.PLOT_WIDGET["Preview"].scene().sigMouseClicked.connect(
            lambda x: self.mouseClicked(x, view_name="Preview")
        )

        pw_scan_proxy = pg.SignalProxy(
            self.PLOT_WIDGET["Scan"].scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved
        )
        self.PLOT_WIDGET["Scan"].scene().sigMouseMoved.connect(
            lambda x: self.mouseMoved(x, view_name="Scan")
        )
        self.PLOT_WIDGET["Scan"].scene().sigMouseClicked.connect(
            lambda x: self.mouseClicked(x, view_name="Scan")
        )

        pw_spectra_proxy = pg.SignalProxy(
            self.PLOT_WIDGET["Spectra"].scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved
        )
        self.PLOT_WIDGET["Spectra"].scene().sigMouseMoved.connect(
            lambda x: self.mouseMoved(x, view_name="Spectra")
        )
        self.PLOT_WIDGET["Spectra"].scene().sigMouseClicked.connect(
            lambda x: self.mouseClicked(x, view_name="Spectra")
        )

        self.dockarea.addDock(
            self.DOCK["Preview"], "bottom"
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
        self.dockarea.addDock(
            self.DOCK["Scan"], "bottom"
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
        self.dockarea.addDock(
            self.DOCK["Spectra"], "right", self.DOCK["Scan"]
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
        self.dockarea.addDock(
            self.DOCK["A"], "bottom"
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
        self.dockarea.addDock(
            self.DOCK["B"], "right", self.DOCK["A"]
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
        self.dockarea.addDock(
            self.DOCK["cam"], "below", self.DOCK["B"]
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)

        self.ui.actionOscilloscopeDock.triggered[bool].connect(
            lambda x: self.DOCK["Preview"].setVisible(x)
        )
        self.ui.actionSpectraDock.triggered[bool].connect(
            lambda x: self.DOCK["Spectra"].setVisible(x)
        )
        self.ui.actionScanDock.triggered[bool].connect(
            lambda x: self.DOCK["Scan"].setVisible(x)
        )
        self.ui.actionADock.triggered[bool].connect(
            lambda x: self.DOCK["A"].setVisible(x)
        )
        self.ui.actionBDock.triggered[bool].connect(
            lambda x: self.DOCK["B"].setVisible(x)
        )
        self.ui.actionCameraDock.triggered[bool].connect(
            lambda x: self.DOCK["cam"].setVisible(x)
        )





        if "dockareaState" in self.settings.allKeys():
            state = self.settings.value("dockareaState")
            try:
                self.dockarea.restoreState(state)
            except Exception as ex:
                logging.error("Can't load DockArea", exc_info=ex)
        if "probeCoord" in self.settings.allKeys():
            data = self.settings.value("probeCoord")
            print("probeCoord:", data)
            if len(data) > 0:
                rows = len(data)
                columns = len(data.dtype.names)
                # self.ui.MultiScan_probe.setRowCount(rows)
                # self.ui.MultiScan_probe.setColumnCount(columns)
                combo_items = [
                    self.ui.MultiScan_probe.cellWidget(0, 4).itemText(i)
                    for i in range(self.ui.MultiScan_probe.cellWidget(0, 4).count())
                ]
                for r in range(rows):
                    self.ui.MultiScan_probe.item(r, 0).setText(f"{data['X'][r]:.4f}")
                    self.ui.MultiScan_probe.item(r, 1).setText(f"{data['Y'][r]:.4f}")
                    self.ui.MultiScan_probe.item(r, 2).setText(f"{data['Z'][r]:.4f}")
                    self.ui.MultiScan_probe.item(r, 3).setText(
                        f"{data['Polarization'][r]:.4f}"
                    )

                    self.ui.MultiScan_probe.cellWidget(r, 4).setCurrentIndex(
                        combo_items.index(data[r]["Type"].decode())
                    )
                    self.ui.MultiScan_probe.item(r, 5).setText(
                        data["Comment"][r].decode()
                    )

        for s in self.settings.allKeys():
            if s == "savedParams":
                savedParams = self.settings.value("savedParams")
                for key in savedParams.keys():
                    if not hasattr(self.ui, key):
                        continue
                    obj = getattr(self.ui, key)
                    # print(type(obj))
                    str_type = str(type(obj))
                    if "SpinBox" in str_type:
                        obj.setValue(savedParams[key])
                    elif "CheckBox" in str_type:
                        obj.setChecked(savedParams[key])
                    elif "Edit" in str_type:
                        obj.setText(savedParams[key])
                    elif "QComboBox" in str_type:
                        obj.setCurrentIndex(savedParams[key])
            elif s == "main_dir":
                self.main_dir = self.settings.value(s)
            else:
                if hasattr(self.ui, s):
                    obj = getattr(self.ui, s)
                    # print(type(obj))
                    str_type = str(type(obj))
                    if "SpinBox" in str_type:
                        obj.setValue(self.settings.value(s))
                    elif "CheckBox" in str_type:
                        obj.setChecked(self.settings.value(s) == "true")
                    elif "Edit" in str_type:
                        obj.setText(self.settings.value(s))



        if CONNECT_ALL:
            self.HWP_FIX_connect(state=True)
            self.HWP_TUN_connect(state=True)
            self.mocoConnect(state=True)
            self.arduinoStage_connect(state=True)
            self.HWP_Polarization_connect(state=True)
            self.filtersPiezoStage_connect(state=True)
            # self.ui.arduinoStage1_connect.setChecked(True)

    def on_readoutSorces_currentIndexChanged(self, index):
        currentText = self.readoutSources.currentText()
        if "AndorCamera" in currentText:
            self.statusBar_exposure.setToolTip(
                "Exposure\nCtrl+click - 1 s\nShift+click - 0.1 s"
            )
            exposure = self.ui.andorCameraExposure.value()
            self.statusBar_exposure.setText(f"Exp:{exposure:0.4f}")
        elif "ThorCamera" in currentText:
            self.statusBar_exposure.setToolTip(
                "Exposure\nCtrl+click - 60 ms\nShift+click - 0.1 ms"
            )
            exposure = self.ui.ThorCamera_exposure.value()
            self.statusBar_exposure.setText(f"Exp:{exposure:0.4f}")

    def _generate_toolBar_statusBar(self):
        ###########################################################!!!!!!!!!!!!!
        self.readoutSources = QtWidgets.QComboBox()
        self.readoutSources.setToolTip("Source of data")
        self.readoutSources.setObjectName("readoutSources")
        self.ui.toolBar.setSizePolicy(
            QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred
        )
        self.ui.toolBar.addWidget(self.readoutSources)
        sources = [
            "ThorCamera",
            "AndorCamera",
            "Powermeter",
            "Powermeter & AndorCamera",
            "PicoScope",
            "PicoScope & AndorCamera",
            "DAQmx",
            "CCS200",
        ]  # [self.readoutSources.itemText(i) for i in range(self.readoutSources.count())]
        self.readoutSources.insertItems(1, sources)
        self.readoutSources.adjustSize()
        setattr(self.ui, "readoutSources", self.readoutSources)

        self.toolBar_commutation_mirror0 = QtWidgets.QComboBox()
        ports = [
         self.ui.commutation_mirror0_out.itemText(i)
         for i in range(self.ui.commutation_mirror0_out.count())
        ]
        self.toolBar_commutation_mirror0.insertItems(1, ports)
        self.toolBar_commutation_mirror0.setToolTip("Commutation mirror output")
        self.toolBar_commutation_mirror0.setObjectName(
         "toolBar_commutation_mirror0"
        )

        self._toolBar_commutation_mirror0_actionIndex = len(self.ui.toolBar.actions())
        self.ui.toolBar.addWidget(self.toolBar_commutation_mirror0)

        self.toolBar_commutation_mirror0.currentIndexChanged[int].connect(
         lambda x: self.commutation_mirror0_set(index=x)
        )

        self.toolBar_light = QtWidgets.QPushButton("OFF")
        self.toolBar_light.setToolTip("Lamp")
        self.toolBar_light.setObjectName("toolBar_light")

        self._toolBar_light_actionIndex = len(self.ui.toolBar.actions())
        self.ui.toolBar.addWidget(self.toolBar_light)

        self.toolBar_light.setCheckable(True)
        self.toolBar_light.toggled["bool"].connect(
         self.ui.ThorCamera_light.setChecked
        )


        self.filtersPiezoStage_ComboBox = QtWidgets.QComboBox()
        self.filtersPiezoStage_ComboBox.setToolTip("Filters piezostage")

        self.ui.toolBar.addWidget(self.filtersPiezoStage_ComboBox)
        self.filtersPiezoStage_Dict = (
            self.configStore[self.calibrTools.setup]["filtersPiezoStage"]
            .attrs["Filters"]
            .to_dict()
        )
        filters = []
        for k in range(len(self.filtersPiezoStage_Dict)):
            filters.append(self.filtersPiezoStage_Dict[k])

        self.filtersPiezoStage_ComboBox.insertItems(1, filters)
        self.ui.filtersPiezoStage_filters.setText(",".join(filters))
        self.filtersPiezoStage_ComboBox.adjustSize()
        self.filtersPiezoStage_ComboBox.currentIndexChanged[int].connect(
            self.filtersPiezoStage_setCurrentIndex
        )

        self.expData_filePath = QtWidgets.QLineEdit("../data/experiment")
        self.expData_filePath_find = QtWidgets.QToolButton()
        self.expData_filePath_find.setText("...")

        self.ui.toolBar.addWidget(self.expData_filePath)
        self.ui.toolBar.addWidget(self.expData_filePath_find)


        self.statusBar = QtWidgets.QStatusBar()
        self.ui.setStatusBar(self.statusBar)

        self.statusBar_Position_X = QtWidgets.QPushButton("X")
        self.statusBar_Position_X.setObjectName("statusBar_Position_X")
        self.statusBar_Position_X.setToolTip('X')

        self.statusBar.addWidget(self.statusBar_Position_X)
        self.statusBar_Position_X.setStyleSheet(
            "background-color:#222222;color:orange; font-family:monospace;"
        )
        self.statusBar_Position_X.clicked.connect(self.moveToDialog)
        self.statusBar_Position_X.setSizePolicy(
            QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum
        )

        self.statusBar_Position_Y = QtWidgets.QPushButton("Y")
        self.statusBar_Position_Y.setToolTip('Y')
        self.statusBar_Position_Y.setObjectName("statusBar_Position_Y")
        self.statusBar.addWidget(self.statusBar_Position_Y)
        self.statusBar_Position_Y.setStyleSheet(
            "background-color:#222222;color:pink; font-family:monospace;"
        )
        self.statusBar_Position_Y.clicked.connect(self.moveToDialog)

        self.statusBar_Position_Z = QtWidgets.QPushButton("Z")
        self.statusBar_Position_Z.setObjectName("statusBar_Position_Z")
        self.statusBar_Position_Z.setToolTip('Z')

        self.statusBar.addWidget(self.statusBar_Position_Z)
        self.statusBar_Position_Z.setStyleSheet(
            "background-color:#222222;color:cyan; font-family:monospace;"
        )
        self.statusBar_Position_Z.clicked.connect(self.moveToDialog)

        self.statusBar_ExWavelength = QtWidgets.QPushButton("[Ex: ? nm]")
        self.statusBar_ExWavelength.setObjectName("statusBar_ExWavelength")
        self.statusBar.addWidget(self.statusBar_ExWavelength)
        self.statusBar_ExWavelength.setStyleSheet(
            "background:#222222; font-family:monospace;"
        )
        self.statusBar_ExWavelength.clicked.connect(self.moveToDialog)

        # self.statusBar_laserStatus = QtWidgets.QLabel('[Laser_Status]')
        # self.ui.statusbar.addWidget(self.statusBar_laserStatus)

        # self.statusBar_HWP_stepper = QtWidgets.QPushButton('[HWP (stepper)]')
        # self.statusBar_HWP_stepper.setObjectName('statusBar_HWP_stepper')
        # self.statusBar_HWP_stepper.setStyleSheet('color:cyan;background:#222222;')
        # self.statusBar_HWP_stepper.setToolTip('HWP (Polarization)')
        # self.statusBar.addWidget(self.statusBar_HWP_stepper)
        # self.statusBar_HWP_stepper.clicked.connect(self.moveToDialog)

        self.statusBar_HWP_TUN_angle = QtWidgets.QPushButton("[TUN]")
        self.statusBar_HWP_TUN_angle.setObjectName("statusBar_HWP_TUN_angle")
        self.statusBar_HWP_TUN_angle.setToolTip(
            "HWP (TUN)\nCtrl+click - Home\nShift+click - angle with min power"
        )
        self.statusBar.addWidget(self.statusBar_HWP_TUN_angle)
        self.statusBar_HWP_TUN_angle.setStyleSheet(
            "color:#54ecff;background:#222222; font-family:monospace;"
        )
        self.statusBar_HWP_TUN_angle.clicked.connect(self.moveToDialog)

        self.statusBar_HWP_FIX_angle = QtWidgets.QPushButton("[FIX]")
        self.statusBar_HWP_FIX_angle.setObjectName("statusBar_HWP_FIX_angle")
        self.statusBar_HWP_FIX_angle.setToolTip(
            "HWP (FIX)\nCtrl+click - Home\nShift+click - angle with min power"
        )
        self.statusBar.addWidget(self.statusBar_HWP_FIX_angle)
        self.statusBar_HWP_FIX_angle.setStyleSheet(
            "color:#8aff54;background:#222222; font-family:monospace;"
        )
        self.statusBar_HWP_FIX_angle.clicked.connect(self.moveToDialog)

        self.statusBar_HWP_Polarization = QtWidgets.QPushButton("[HWP]")
        self.statusBar_HWP_Polarization.setObjectName("statusBar_HWP_Polarization")
        self.statusBar_HWP_Polarization.setToolTip(
            "HWP (Polarization)\nCtrl+click - Home"
        )
        self.statusBar.addWidget(self.statusBar_HWP_Polarization)
        self.statusBar_HWP_Polarization.setStyleSheet(
            "color:#dfff54;background:#222222; font-family:monospace;"
        )
        self.statusBar_HWP_Polarization.clicked.connect(self.moveToDialog)

        # self.statusBar_laserCalcPower = QtWidgets.QPushButton('[Laser power]')
        # self.statusBar_laserCalcPower.setObjectName('statusBar_laserCalcPower')
        # self.statusBar_laserCalcPower.setToolTip('Laser input power')
        # self.statusBar_laserCalcPower.setStyleSheet('color:yellow;background:#222222;')
        # self.statusBar.addWidget(self.statusBar_laserCalcPower)
        # self.statusBar_laserCalcPower.clicked.connect(self.moveToDialog)

        self.statusBar_moco = QtWidgets.QPushButton("[MoCo]")
        self.statusBar_moco.setObjectName("statusBar_moco")
        self.statusBar_moco.setToolTip(
            "Delay line position\nCtrl+click - Home\nShift+click - Zero delay"
        )
        self.statusBar.addWidget(self.statusBar_moco)
        self.statusBar_moco.setStyleSheet(
            "color:orange;background:#222222; font-family:monospace;"
        )
        self.statusBar_moco.clicked.connect(self.moveToDialog)

        self.statusBar_arduinoStage = QtWidgets.QPushButton("[lens]")
        self.statusBar_arduinoStage.setObjectName("statusBar_arduinoStage")
        self.statusBar_arduinoStage.setToolTip("Linear stage (lens)")
        self.statusBar.addWidget(self.statusBar_arduinoStage)
        self.statusBar_arduinoStage.setStyleSheet(
            "color:cyan;background:#222222; font-family:monospace;"
        )
        self.statusBar_arduinoStage.clicked.connect(self.moveToDialog)

        # self.statusBar_arduinoStage1 = QtWidgets.QPushButton('[knife]')
        # self.statusBar_arduinoStage1.setObjectName('statusBar_arduinoStage1')
        # self.statusBar_arduinoStage1.setToolTip('Linear stage (knife)')
        # self.statusBar.addWidget(self.statusBar_arduinoStage1)
        # self.statusBar_arduinoStage1.setStyleSheet('color:cyan;background:#222222;')
        # self.statusBar_arduinoStage1.clicked.connect(self.moveToDialog)

        self.statusBar_exposure = QtWidgets.QPushButton("[Exposure]")
        self.statusBar_exposure.setObjectName("statusBar_exposure")
        self.statusBar_exposure.setToolTip(
            "Exposure\nCtrl+click - 1 s\nShift+click - 0.1 s"
        )
        self.statusBar.addWidget(self.statusBar_exposure)
        self.statusBar_exposure.setStyleSheet(
            "color:yellow;background:#222222; font-family:monospace;"
        )
        self.statusBar_exposure.clicked.connect(self.moveToDialog)

        # self.statusBar_ShamrockPort = QtWidgets.QLabel('[Shamrock Port]')
        # self.statusBar_ShamrockPort.setStyleSheet('color:orange;')
        # self.statusBar.addWidget(self.statusBar_ShamrockPort)

        self.readoutSources.currentIndexChanged[int].connect(self.on_readoutSorces_currentIndexChanged)




        self.progressBar = QtWidgets.QProgressBar()
        self.progressBar.setValue(42)
        #self.progressBar.setFixedWidth(200)

        self.statusBar.addWidget(self.progressBar)


        self.mouse_position_viewer = QtWidgets.QLabel()
        self.mouse_position_viewer.setFixedWidth(150)
        self.statusBar.addWidget(self.mouse_position_viewer)

        self.statusBar_message = QtWidgets.QLabel("satus")
        self.statusBar.addWidget(self.statusBar_message)
        #self.statusBar.setSizePolicy(
        #    QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Preferred
        #)
        #self.statusBar_message_cleanupTimer.timeout.connect(self.statusBar_cleanMessage)





    def toggleGroup(self, ctrl):
        state = ctrl.isChecked()
        if state:
            ctrl.setFixedHeight(ctrl.sizeHint().height())
        else:
            ctrl.setFixedHeight(30)

    def onImgPos(self, event, sender="A"):
        # print(sender)
        if sender == "A":
            pos = self.IMG_GRID["A"].mapFromScene(event.scenePos())
            x = pos.x()
            y = pos.y()

        elif sender == "B":
            pos = self.IMG_GRID["B"].mapFromScene(event.scenePos())
            x = pos.x()
            y = pos.y()
        else:
            pos = self.IMG_GRID["cam"].mapFromScene(event.scenePos())
            x = pos.x()  # -self.piStage_position_cam_shift[0]
            y = pos.y()  # -self.piStage_position_cam_shift[1]

        self.mouse_position_viewer.setText(" [X: %.4f, Y: %.4f] " % (x, y))


        if event.modifiers() & QtCore.Qt.ControlModifier:
            self._ruler_points[sender].append([x, y])
            v = np.array(self._ruler_points[sender])
            if len(self._ruler_points[sender]) == 1:
                self.RULER_LINES[sender].setData(x=v[:, 0], y=v[:, 1])
            elif len(self._ruler_points[sender]) == 2:
                self.RULER_LINES[sender].setData(x=v[:, 0], y=v[:, 1])
                dist = ((v[1] - v[0]) ** 2).sum() ** 0.5
                self.RULER_LINES_DISTANCE[sender].setText(f"{dist:.4f}um")
                self.RULER_LINES_DISTANCE[sender].setPos(x, y)
                self.RULER_LINES_DISTANCE[sender].show()
                self._ruler_points[sender] = []

            else:
                self._ruler_points[sender] = []
                self.RULER_LINES[sender].setData(self._ruler_points[sender])
        else:
            self.RULER_LINES[sender].setData([])
            self._ruler_points[sender] = []
            self.RULER_LINES_DISTANCE[sender].hide()

        if event.modifiers() & QtCore.Qt.ShiftModifier:
            if x > 0 and x < 100 and y > 0 and y < 100:
                self.piStage_moveTo([0, 1], [x, y], wait=1)
                real_position = self.piStage.getPosition()
                self.setUiPiPos(real_position)



        if event.modifiers() & QtCore.Qt.AltModifier:
            self.ThorCamera_live[0] = self.ThorCamera_live[1]
            self.ThorCamera_live[1] = (not self.scriptThread is None and self.readoutSources.currentText()=="ThorCamera")

            cursor_pos = np.array((pos.x(), pos.y()))
            if sender == "cam":
                real_position = self.piStage.getPosition()
                if self.ThorCamera_live[1] is None:
                    self.piStage_position_beamSpot_position = 0#+= cursor_pos - real_position[:2]
                elif self.ThorCamera_live[1]:
                    self.piStage_position_beamSpot_position += cursor_pos - real_position[:2]
                else:
                    if self.ThorCamera_live[0]:
                        self.piStage_position_beamSpot_position = cursor_pos - real_position[:2]
                    else:
                        self.piStage_position_beamSpot_position += cursor_pos - real_position[:2]

                if not self.ThorCamera_live[1]:
                    new_pos = self.piStage_position_cam_static_pos - self.piStage_position_beamSpot_position
                    self.settings.setValue("piStage_position_cam_static_pos", new_pos)
                    self.settings.setValue("piStage_position_beamSpot_position", self.piStage_position_beamSpot_position)

                    scale = self.ui.ThorCamera_scale.value()
                    img = self.IMG_VIEW["cam"].getImageItem().image
                    conf = {
                        "type": "img",
                        "name": "cam",
                        "time": time.time(),
                        "data": [img, new_pos, (scale, scale), [0], 0],
                    }
                    self.dataReadySignal.emit(conf)
                    #self.IMG_VIEW["cam"].getImageItem().setPos(*new_pos)


    def mouseMoved(self, evt, view_name=None):
        pos = evt  # [0]\
        if self.sender() is None:
            return
        if self.sender().parent() is None:
            return
        vb = self.sender().parent().plotItem.vb

        if self.sender().parent().sceneBoundingRect().contains(pos):
            try:
                mousePoint = vb.mapSceneToView(pos)
            except:
                return

            self.mouse_position_viewer.setText(
                f"<span style='font-size: 12pt'>{mousePoint.x():.4f}</span>, <span style='font-size: 12pt;color: cyan'>{mousePoint.y():.4f}</span>"

            )
            self.mouse_last_pos[view_name] = (mousePoint.x(), mousePoint.y())

    def mouseClicked(self, evt, view_name=None):
        pos = evt  # [0]

        # print(self.mouse_last_pos[view_name])
        if not hasattr(self, "mouse_last_pos_arr"):
            self.mouse_last_pos_arr = np.zeros((0, 2))

        modifiers = QtWidgets.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ControlModifier and view_name == "Preview":
            row = self.linesManager.TABLES["Preview"].currentRow()
            if self.linesManager.TABLES["Preview"].item(row, 0) is None:
                row = 0
            lineClass = self.linesManager.TABLES["Preview"].item(row, 0).text()
            lineInfo = self.linesManager.TABLES["Preview"].item(row, 1).text()

            x, y = self.linesManager.getLine("Preview",lineClass,lineInfo).getData()

            index = (
                (x - self.mouse_last_pos[view_name][0]) ** 2
                + (y - self.mouse_last_pos[view_name][1]) ** 2
            ) ** 0.5
            if len(index) > 0:
                x = np.append(x, self.mouse_last_pos[view_name][0])
                y = np.append(y, self.mouse_last_pos[view_name][1])
                y = y[x.argsort()]
                x = x[x.argsort()]

                # index = index.argmin()
                # x = np.insert(x,index,self.mouse_last_pos[view_name][0])
                # y = np.insert(y,index,self.mouse_last_pos[view_name][1])
                self.linesManager.getLine("Preview", lineClass,lineInfo).setData(x=x, y=y)

        if modifiers == QtCore.Qt.AltModifier and view_name == "Preview":
            row = self.linesManager.TABLES["Preview"].currentRow()
            if self.linesManager.TABLES["Preview"].item(row, 0) is None:
                row = 0
            lineClass = self.linesManager.TABLES["Preview"].item(row, 0).text()
            lineInfo = self.linesManager.TABLES["Preview"].item(row, 1).text()

            x, y = self.linesManager.getLine("Preview",lineClass,lineInfo).getData()

            index = (
                (x - self.mouse_last_pos[view_name][0]) ** 2
                + (y - self.mouse_last_pos[view_name][1]) ** 2
            ) ** 0.5
            if len(index) > 0:
                index = index.argmin()
                x = np.delete(x, index, axis=0)
                y = np.delete(y, index, axis=0)
                self.linesManager.getLine("Preview",lineClass,lineInfo).setData(x=x, y=y)

        else:
            pass

    def MultiScan_probe_sync(self, row, col):
        try:
            self.MultiScan_update()
        except:
            traceback.print_exc()

    def actionStop(self, state):
        self.stopActiveThreads()

    def progressBar_finished(self, resp=None):
        self.progressBar.setValue(100)
        self.stopActiveThreads()

    def progressBar_progress(self, value=None):
        self.progressBar.setValue(value)

    def plotter(self, item):
        try:

            if type(item) != list and type(item) != tuple:
                item = [item]

            for Index, item_ in enumerate(item):

                ###############################################################

                itemType = item_["type"]

                if itemType == "Scan":
                    name = item_["name"] + item_["info"]
                    ch = item_["name"].split("_")[1]
                    line_class = name.split("[")[0]
                    x, y = item_["data"]
                    if y.sum() == 0:
                        continue
                    if (~np.isnan(x*y)).sum() == 0:
                        continue

                    if not ch in self.defined_line_styles:
                        self.defined_line_styles[ch] = pg.mkPen(
                            color=pg.intColor(np.random.randint(20), maxHue=200))

                    pen = self.defined_line_styles[ch]

                    self.linesManager.addUpdateLine("Scan", item_["name"], item_["info"], x=x, y=y, pen=pen, symbol="o", symbolSize=3, symbolPen=pen, symbolBrush=None)


                elif itemType == "grid_points":
                    x, y = item_["data"]
                    if (~np.isnan(x*y)).sum() == 0:
                        continue
                    kwargs = {}
                    for kw in ["pen", "symbol", "symbolPen",  "symbolSize", "symbolBrush"]:
                        if kw in item_:
                            kwargs[kw] = item_[kw]

                    self.linesManager.addUpdateLine("Preview", item_["name"], item_["info"], x=x, y=y,
                            **kwargs
                        )

                elif itemType == "raw":
                    name = item_["name"] + item_["info"]
                    source, ch = item_["name"].split("_")
                    x, y = item_["data"]
                    try:
                        x[0]
                        y[0]
                    except:
                        continue
                    if (~np.isnan(x*y)).sum() == 0:
                        continue
                    isRaw = True
                    view_name = "Spectra"
                    if source == "PicoScope":
                        view_name = "Preview"
                    elif source == "AndorCamera" or source == "CCS200":
                        view_name = "Spectra"
                        sig_wl = calcVisibleBands(lambda2=self.laserGetWavelength())
                        if (
                            source == "AndorCamera"
                            and not self.ui.andorCamera_previewNormButShowRaw.isChecked()
                        ):
                            y = y * self.andorCamera_correctionCurve
                        w = (~np.isnan(x)) & (~np.isnan(y))
                        x = x[w]
                        y = y[w]
                        for k in sig_wl:
                            self.line_markers[k].setPos(sig_wl[k])
                        for k in self.line_markers:
                            pos = self.line_markers[k].x()
                            if pos >= x.min() and pos <= x.max():
                                self.line_markers[k].show()
                            else:
                                self.line_markers[k].hide()
                    else:
                        microV_logger.error("Unknown type of data for raw")
                        isRaw = False
                    if isRaw:
                        if not ch in self.defined_line_styles:
                            self.defined_line_styles[ch] = pg.mkPen(
                                color=pg.intColor(np.random.randint(20), maxHue=200))

                        pen = self.defined_line_styles[ch]
                        self.linesManager.addUpdateLine(view_name, item_["name"], item_["info"], x=x, y=y, pen=pen)


                elif itemType == "piStage_position":
                    name = item_["name"]
                    x, y = item_["data"]
                    self.piStage_POSITION[name].setPos(x[0], y[0])  # .setData(x=x,y=y)
                    if name == "cam":

                        self.piStage_position_cam_limits["cam"].setData(
                            x=np.array(
                                [0, 100, 100, 0, 0]
                            ),  # +self.piStage_position_cam_shift[0],
                            y=np.array(
                                [0, 0, 100, 100, 0]
                            ),  # +self.piStage_position_cam_shift[1]
                        )
                        self.piStage_position_cam_limits1["cam"].setData(
                            x=np.array(
                                [0, 50, 50, 50, 50, 100]
                            ),  # +self.piStage_position_cam_shift[0],
                            y=np.array(
                                [50, 50, 0, 100, 50, 50]
                            ),  # +self.piStage_position_cam_shift[1]
                        )
                if itemType == "img":

                    data, pos, scale, xvals, zi = item_["data"]
                    # if len(xvals)<=1:
                    # 		xvals=None
                    img = self.IMG_VIEW[item_["name"]]
                    if item_["name"] in ["A", "B"]:
                        if len(xvals) > 1:
                            img.setImage(
                                data,
                                pos=pos,
                                scale=scale,
                                xvals=xvals,
                                axes={"t": 0, "x": 1, "y": 2},
                                autoRange=False,
                            )
                            if not zi is None:
                                img.setCurrentIndex(zi)
                        else:
                            img.setImage(
                                data[0],
                                pos=pos,
                                scale=scale,
                                axes={"x": 0, "y": 1},
                                autoRange=False,
                            )
                    elif item_["name"] == "cam":
                        print('cam_pos:',pos)
                        img.setImage(
                            data,
                            pos=pos,
                            scale=scale,
                            autoHistogramRange=self.ui.ThorCamera_autoRange.isChecked(),
                            autoRange=False,
                            autoLevels=self.ui.ThorCamera_autoRange.isChecked(),
                        )

        except Exception as ex:
            microV_logger.error("PlotterError", exc_info=ex)




    def state_monitor(self, status_callback=None):
        print("state_monitor: start")
        while self.state_monitor_alive:
            try:
                self
            except:
                microV_logger.info("state_monitor_alive==False")
                return
            if not self._monitor_lock:
                self.lazyUpdateUi()
            self.time_sleep(0.45)
            if self.statusBar_message_cleanupTimer>0:
                if (time.time() - self.statusBar_message_cleanupTimer)>10:
                    self.statusBar_cleanMessage()

        microV_logger.info("state_monitor_alive==False")

        return

    def lazyUpdateUi(self):

        try:
            # self.ThorCamera_updateImage()

            if self.arduinoStage.is_alive():
                if not self._ArduinoStage.is_alive():
                    self.arduinoStage_init()
                else:

                    self.update_arduinoStage()
                    self.update_arduinoStage1()
                    self.laserBeamShift_H_position_update()

            else:
                self.ui.arduinoStage_connect.blockSignals(True)
                self.ui.arduinoStage_connect.setChecked(False)
                self.ui.arduinoStage_connect.blockSignals(False)

            if self.rotationStage_TDC001.is_alive():
                # if self.ui.actionStop.isChecked():
                pos = self.rotationStage_TDC001.position
                self.ui.HWP_TUN_angle.setText(str(pos))
                self.statusBar_HWP_TUN_angle.setText(f"TUN:{pos:.2f}{chr(176)}")

            else:
                self.ui.HWP_TUN_connect.blockSignals(True)
                self.ui.HWP_TUN_connect.setChecked(False)
                self.ui.HWP_TUN_connect.blockSignals(False)
            # if self.arduinoStage1.is_alive():
            # 	pos = self.arduinoStage1.position
            # 	self.ui.arduinoStage1_position.setText(str(pos))
            # else:
            # 	self.ui.arduinoStage1_connect.setChecked(False)

            if self.piezoDev_bus.is_alive():
                pass
            else:
                self.ui.HWP_Polarization_connect.setChecked(False)
                self.ui.filtersPiezoStage_connect.setChecked(False)

            if self.HWP_FIX.is_alive():
                pass
            else:
                self.ui.HWP_FIX_connect.setChecked(False)

            if self.moco.is_alive():
                pos = self.moco.axes["delay"].position
                self.ui.mocoPosition.setValue(pos)
                self.statusBar_moco.setText("Delay:%.3f" % (pos))

                self.estimate_mocoZeroDelay_position(visible=True)
                self.moco_calcDelay()

            else:
                self.ui.mocoConnect.setChecked(False)

            if self.laserClient.is_alive():

                laserInfo = self.laserClient.getInfo()
                # print(laserInfo)
                status = laserInfo["status"]["value"]  # print(status)
                # print(laserInfo)
                self.ui.laserStatus.setText(status["State"])

                self.ui.actionShutter.setEnabled(True)
                self.ui.actionIRShutter.setEnabled(True)

                self.ui.actionShutter.blockSignals(True)
                self.ui.actionIRShutter.blockSignals(True)
                if not status["Main shutter"] is None:
                    self.ui.actionShutter.setChecked(status["Main shutter"])
                if not status["IR shutter"] is None:
                    self.ui.actionIRShutter.setChecked(status["IR shutter"])
                self.ui.actionShutter.blockSignals(False)
                self.ui.actionIRShutter.blockSignals(False)

                wavelength = laserInfo["wavelength"]["value"]
                self.statusBar_ExWavelength.setText(f"Ex:{wavelength}nm")
                self.ui.laserWavelength.setValue(wavelength)

                precompensation = laserInfo["precompensation"]["value"]
                self.ui.laserPrecompensation.setValue(precompensation)

                self.estimate_mocoZeroDelay_position(visible=True)

                power = laserInfo["power"]["value"]
                self.ui.laserPower_internal.setValue(power)

                # self.laserGetWavelength()
                # print('Try get wavelength:DONE', time.time()- self.lastUpdateTime)

                # if status['ON_OFF']:
                # 	self.ui.laserOnOff.setStyleSheet('background-color:green;')
                # else:
                # 	self.ui.laserOnOff.setStyleSheet('background-color:blue;')
                self.lastUpdateTime = time.time()

            else:
                self.ui.laserClient_connect.blockSignals(True)
                self.ui.laserClient_connect.setChecked(False)
                self.ui.laserClient_connect.blockSignals(False)

                self.ui.actionShutter.setEnabled(False)
                self.ui.actionIRShutter.setEnabled(False)

            if self.scriptThread is None and not self.ThorCamera_img_isSaved:
                self.ThorCamera_imgSave()

        except Pyro4.errors.CommunicationError:
            try:
                if not self.shamrock.is_alive():
                    self.ui.andorCamera_Shamrock_connect.setChecked(False)
                if not self.arduinoStage.is_alive():
                    self._ArduinoStage.initialize()
                    self.arduinoStage.initialize()
            except:
                pass
        except KeyboardInterrupt:
            self.state_monitor_alive = False
        except RuntimeError:
            self.state_monitor_alive = False
        except:
            traceback.print_exc()

    def viewCleanLines(self):

        self.mouse_last_pos_arr = np.zeros((0, 2))

    def syncRectROI(self):
        sender = self.sender()
        pos = list(sender.pos())
        size = list(sender.size())
        sender.blockSignals(True)
        # if sender == self.rectROI['cam']:
        # 	pos = [pos[0]-self.piStage_position_cam_shift[0],pos[1]-self.piStage_position_cam_shift[1],]

        if pos[0] < 0:
            pos[0] = 0
        if pos[0] > 100:
            pos[0] = 100
        if pos[1] < 0:
            pos[1] = 0
        if pos[1] > 100:
            pos[1] = 100

        if pos[0] + size[0] > 100:
            size[0] = 100 - pos[0]
        if pos[1] + size[1] > 100:
            size[1] = 100 - pos[1]

        pos_cam = pos
        # pos_cam = [pos[0]+self.piStage_position_cam_shift[0],pos[1]+self.piStage_position_cam_shift[1],]
        # if sender == self.rectROI['cam']:
        # 	sender.setPos(pos_cam)
        # else:
        # 	sender.setPos(pos)
        sender.setPos(pos)
        sender.setSize(size)
        sender.blockSignals(False)

        if sender == self.rectROI["A"]:
            self.rectROI["B"].blockSignals(True)
            self.rectROI["B"].setPos(pos)
            self.rectROI["B"].setSize(size)
            self.rectROI["B"].blockSignals(False)
            self.rectROI["cam"].blockSignals(True)
            self.rectROI["cam"].setPos(pos_cam)
            self.rectROI["cam"].setSize(size)
            self.rectROI["cam"].blockSignals(False)

            self.rectROI["A"].blockSignals(True)
            self.rectROI["A"].setPos(pos)
            self.rectROI["A"].setSize(size)
            self.rectROI["A"].blockSignals(False)
            self.rectROI["cam"].blockSignals(True)
            self.rectROI["cam"].setPos(pos_cam)
            self.rectROI["cam"].setSize(size)
            self.rectROI["cam"].blockSignals(False)
        else:
            self.rectROI["A"].blockSignals(True)
            self.rectROI["A"].setPos(pos)
            self.rectROI["A"].setSize(size)
            self.rectROI["A"].blockSignals(False)
            self.rectROI["B"].blockSignals(True)
            self.rectROI["B"].setPos(pos)
            self.rectROI["B"].setSize(size)
            self.rectROI["B"].blockSignals(False)

        self.scan3D_config["X"]["start"].setValue(pos[0])
        self.scan3D_config["Y"]["start"].setValue(pos[1])

        self.scan3D_config["X"]["end"].setValue(size[0] + pos[0])
        self.scan3D_config["Y"]["end"].setValue(size[1] + pos[1])

    def syncRectROI_table(self, val):
        pos = [0, 0, 0]
        size = [0, 0, 0]
        step = [0, 0, 0]
        for i, ax in enumerate("XYZ"):
            pos[i] = self.scan3D_config[ax]["start"].value()
            size[i] = self.scan3D_config[ax]["end"].value() - pos[i]
            step[i] = self.scan3D_config[ax]["step"].value()
            if size[i] < 0:
                self.scan3D_config[ax]["step"].setValue(-abs(step[i]))
            else:
                self.scan3D_config[ax]["step"].setValue(abs(step[i]))

        self.rectROI["A"].blockSignals(True)
        self.rectROI["B"].blockSignals(True)
        self.rectROI["cam"].blockSignals(True)

        self.rectROI["A"].setPos(pos)
        self.rectROI["A"].setSize(size)

        self.rectROI["B"].setPos(pos)
        self.rectROI["B"].setSize(size)
        # pos_cam = [pos[0]+self.piStage_position_cam_shift[0],pos[1]+self.piStage_position_cam_shift[1],]
        pos_cam = pos
        self.rectROI["cam"].setPos(pos_cam)
        self.rectROI["cam"].setSize(size)

        self.rectROI["A"].blockSignals(False)
        self.rectROI["B"].blockSignals(False)
        self.rectROI["cam"].blockSignals(False)

    def styleTabs(self, index):
        self.ui.configTabWidget.setStyleSheet(
            """
			QTabBar::tab {{}}
			QTabBar::tab:selected {{
				background-color: {color};
			}}
			""".format(
                color=self.tabColors[index]
            )
        )

    def __del__(self):
        self.state_monitor_alive = False
        self.stopActiveThreads()

    def saveUiValues(self):
        keys2save = [
            "PicoScope_AutoRange",
            "PicoScope_ChA_VRange",
            "PicoScope_ChB_VRange",
            "pulseFreq",
            "PicoScope_sampleInterval",
            "PicoScope_samplingDuration",
            "PicoScope_ChA_active",
            "PicoScope_ChB_active",
            "PicoScope_ChA_threshold",
            "PicoScope_ChB_threshold",
            "PicoScope_ChA_thresholdType",
            "PicoScope_ChB_thresholdType",

            "PicoScope_n_captures",
            "PicoScope_TrigThreshold",
            "PicoScope_TrigSource",
            "PicoScope_pretrig",
            "PicoScope_byFrames",
            "PicoScope_delay",
            "HWP_FIX_angle",
            "HWP_TUN_angle",
            #'HWP_TUN_min_power_angle',
            #'HWP_FIX_min_power_angle',
            #'shamrockWavelengthShift',
            "ThorCamera_imgPath",
            "ThorCamera_scale",
            "ThorCamera_angle",
            "ThorCamera_transpose",
            "ThorCamera_reflectH",
            "ThorCamera_reflectV",
            "commutation_mirror0_positions",
            #'commutation_mirror1_positions',
            "readoutSources",
            "mocoZeroDelay_correct_ex_wl",
            "mocoZeroDelay_correct_pos",
            "scan3D_scanOrder",
            "scan3D_scanPath",
            "laserBeamShift_V_zero_angle",
            "laserBeamShift_H_zero_position",
            "HWP_Polarization_angle_correction",
            "arduinoStage_rewrite_position_value" "Pi_X_angle",
            "Pi_Y_angle",
            "Pi_Z_angle",
            "actionADock",
            "actionBDock",
            "actionSpectraDock",
            "actionOscilloscopeDock",
            "actionCameraDock",
            "actionScanDock",

            "andorCamera_filteringKernel",
            "andorCameraExposure_decreaseOnSaturation",
            "andorCamera_HDR",
            "shamrockWavelength_adaptive_centering_by",
            "shamrockWavelength_adaptive_centering_getBaseline",
            "shamrockWavelength_adaptive_centering",
            "andorCamera_HDR_factor",
            "andorCamera_HDR_max",

            "optimize_chan",


            "laserPowerIntens_linkedOut",
            "laserPowerIntens_fixed",
            "laserPowerIntens_TUN_target",
            "laserControlPower_or_Intens",

        ]

        for ax in "XYZ":
            for k in self.scan3D_config[ax]:
                keys2save.append(self.scan3D_config[ax][k].objectName())
                print(keys2save[-1])

        info, currentIndex = self.MultiScan_getInfo()
        self.settings.setValue("probeCoord", info)
        print("probeCoord:", info)
        savedParams = {}
        for key in keys2save:
            if hasattr(self.ui, key):
                obj = getattr(self.ui, key)
                str_type = str(type(obj))
                if "SpinBox" in str_type:
                    savedParams[key] = obj.value()
                elif "CheckBox" in str_type:
                    savedParams[key] = obj.isChecked()
                elif "Edit" in str_type:
                    savedParams[key] = obj.text()
                elif "QComboBox" in str_type:
                    savedParams[key] = obj.currentIndex()

        self.settings.setValue("savedParams", savedParams)

    def _shutdownHardware(self):
        self.piStageLiveTimer.stop()

        try:
            self.piStage.close()
        except Exception as ex:
            microV_logger.error("_shutdownHardware:ERROR", exc_info=ex)

        if not self.ccs200_spectrometer is None:
            self.ccs200_spectrometer.disable()

        if not self.thorcam is None:
            self.thorcam.close()

        try:
            if self.PicoScope:
                self.PicoScope.close()
        except Exception as ex:
            microV_logger.error("_shutdownHardware:ERROR", exc_info=ex)
        # try:
        # 	self.rotPiezoStage.close()
        # except:
        # 	traceback.print_exc()
        try:
            if self.ui.andorCamera_Shamrock_connect.isChecked():
                # self.andorCCD.ShutDown()
                # self.andorCCD.shutdown()
                # self.shamrock.shutdown()
                pass

        except Exception as ex:
            microV_logger.error("_shutdownHardware:ERROR", exc_info=ex)

    def closeEvent(self, evnt=None):
        if self.demo:
            self.settings.setValue('XYZ', self.piStage.getPosition())
        print("closeEvent")
        self.state_monitor_alive = False

        self.saveUiValues()

        state = self.dockarea.saveState()
        self.settings.setValue("dockareaState", state)

        #self.stopActiveThreads()
        # self.redrawThread.running = False

        self._shutdownHardware()


if __name__ == "__main__":

    __spec__ = None
    # QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
    app = QtWidgets.QApplication(sys.argv)

    ex = microV()

    SYSTEM_signal(SIGINT, lambda x, y: ex.closeEvent())

    # import qdarkstyle

    # app.setStyleSheet(qdarkstyle.load_stylesheet(qt_api=pg.Qt.QT_LIB))
    # ex.ui.setStyleSheet(qdarkstyle.load_stylesheet(qt_api=pg.Qt.QT_LIB))
    # from qt_material import apply_stylesheet
    # apply_stylesheet(app, theme='dark_blue.xml')
    # apply_stylesheet(ex.ui, theme='dark_blue.xml')
    # with open(Path('ui/BreezeDark.qss'),'r') as style:
    # 	ex.ui.setStyleSheet(style.read())
    # 	app.setStyleSheet(style.read())
    app.exec_()
