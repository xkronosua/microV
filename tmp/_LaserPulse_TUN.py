import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad, quad_vec
from lmfit import Model
from lmfit.models import QuadraticModel
from scipy.interpolate import interp1d
from scipy.signal import medfilt, butter, filtfilt, find_peaks
import itertools

sech = lambda x: 1 / np.cosh(x)


def I(t, t_p):
    return sech(1.7627 * t / t_p) ** 2


def func(t, tau, t_p1=100, t_p2=100):
    return I(t - tau, t_p1) * I(t, t_p2)


almost_inf = 2000


def IAC(x, t0, A0, A, t_p1=100, t_p2=100):
    t = x - t0
    # print(t)
    f = lambda x: func(x, t, t_p1, t_p2)
    f_norm = lambda x: func(x, 0, t_p1, t_p2)

    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    # r = np.nan_to_num(r,0, neginf=0, posinf=0)
    res = A0 + A * r
    print(t0, A0, A, t_p1, t_p2)
    return res


df = pd.read_csv(sys.argv[1], index_col=0)

ac_mod = Model(IAC)
params = ac_mod.make_params()
params["t_p1"].set(154, min=100, max=250, vary=False)
params["t_p2"].set(10, min=70, max=200, vary=True)
params["t0"].set(0, min=-200, max=200, vary=True)
params["A"].set(1, min=0.8, max=1.1, vary=False)
params["A0"].set(0, min=-0.1, max=0.1, vary=False)
mod = ac_mod

ex_wls = df.ex_wl.unique()
N = len(ex_wls)

fig, axes = plt.subplots(3, int(np.ceil(N // 3)) + 1, sharex=True, sharey=True)
axes = axes.flatten()
res = []
for i, ex_wl in enumerate(ex_wls[0:]):
    g = df[(df.ex_wl == ex_wl) & (df.centerIndex == 0)]

    delay = g["delay"]
    t = 2 * delay.values / 299792458 * 1e12
    sig = g["sfg"].values
    sig = (sig - sig.min()) / np.ptp(sig)
    ex_wl = g["ex_wl"].mean()
    result = mod.fit(sig, params, x=t)
    axes[i].plot(t, sig, "o", label=f"{ex_wl}")
    pulse_width_TUN = result.params["t_p2"].value
    axes[i].plot(t, result.best_fit, "-r", label=f"{pulse_width_TUN:.1f}")
    axes[i].legend()
    axes[i].grid(1)
    res.append({"ex_wl": ex_wl, "pulse_FWHM": pulse_width_TUN})

df1 = pd.DataFrame(res)

plt.figure()
raw = np.load("data.npy")[:-1]

plt.plot(df1.ex_wl, df1.pulse_FWHM, label="calc, FWHM")
plt.plot(raw["Ex_wl"], raw["pulsewidth"] * 1e15, label="datasheet")
# plt.plot(raw['Ex_wl'],raw['pulsewidth']*1e15/0.84,'--',label='datasheet->FWHM')
plt.xlabel("Ex. wavelength, nm")
plt.ylabel("Pulse width, df")

plt.legend()
plt.show()
