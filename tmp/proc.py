import exdir
import numpy as np
from matplotlib import pyplot as plt
import exdir
import sys
from scipy.signal import medfilt
import pandas as pd

fname = "ZnO90-200_ex1300-700_optim2w2_back_test1_delay.exdir"
store = exdir.File(fname, "r")

dset = store["MultiScan"][max(store["MultiScan"])]["data"]

data = dset[dset["time"] != 0]

ex_wls = np.unique(data["position"]["LaserWavelength"])
centers = [2, 3, 4]  # np.unique(data['position']['CenterIndex'])
colors = "rgbcmk"
N = len(ex_wls)
fig, axes = plt.subplots(len(ex_wls[:N]), 1, sharex=True)
for i, ex_wl in enumerate(ex_wls[::-1][:N]):
    mask = data["position"]["LaserWavelength"] == ex_wl
    for center in centers:
        mask1 = data["position"]["CenterIndex"] == center
        d = data[mask & mask1]
        wl = d["data"]["AndorCamera"]["raw"]["wavelength"][0]
        s0 = d["data"]["AndorCamera"]["raw"]["intensity"][:3].mean(axis=0)
        # axes[i].plot(wl,s0/s0.max(),'--',label=f'{ex_wl}: NP{center}',color=colors[center])
        sfg = d["data"]["AndorCamera"]["data"]["w1+w2"]
        max_index = np.where(sfg == sfg.max())[0][0]
        s1 = d["data"]["AndorCamera"]["raw"]["intensity"][max_index]
        axes[i].plot(wl, s1 / s0.max(), label=f"NP{center}", color=colors[center])

    axes[i].grid()

    axes[i].text(300, 0.5, f"Ex:{ex_wl}nm")

axes[0].legend()

plt.show()
