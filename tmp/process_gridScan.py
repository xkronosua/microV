# IPython log file
import numpy as np
import exdir
from scipy.interpolate import interp1d, griddata
import matplotlib.pyplot as plt
from matplotlib import cm

def interp_grid(x,y,t0,t1,dt):
	t_new = np.arange(t0,t1,dt)
	t = np.linspace(t0,t1,len(x))
	x_interp = interp1d(t,x,bounds_error=False,fill_value=0)
	y_interp = interp1d(t,y,bounds_error=False,fill_value=0)
	x_new = x_interp(t_new)
	y_new = y_interp(t_new)
	#plot(t,x,'.b',t_new,x_new,'.r')
	return x_new,y_new

def process_gridScanXY(filepath='gridScanXY1604311927.exdir/',plot=False):
	s=exdir.File(filepath,'r')
	path=s['path']
	raw=s['raw']
	signals = s['signals']

	w1 = (signals[:,0]>=0) & (signals[:,0]<(raw.attrs['end']-raw.attrs['start'])*1e9)
	values = np.copy(signals[w1,2])
	values = values-values.min()
	values = values/values.max()
	dt = np.diff(signals[:,0]).mean()*1e-9
	x_new,y_new = interp_grid(raw[:,0],raw[:,1],raw.attrs['start'],raw.attrs['end'],dt)
	w2 = x_new!=0

	x = np.linspace(raw[:,0].min(),raw[:,0].max(),1000)
	y =  np.linspace(raw[:,1].min(),raw[:,1].max(),1000)
	X, Y = np.meshgrid(x,y)
	Z = griddata((x_new[w2], y_new[w2]),values[w2], (X, Y), method='linear')

	plt.contourf(X, Y, Z,100)

	#colors = 'rgbcm'[::-1]
	#h = (values.max()-values.min())/len(colors)
	if plot:
		plt.scatter(x_new[::100],y_new[::100], c=cm.jet(values[::100]),s=5, edgecolor='none')
		plt.show()
	return X, Y, Z, x_new[w2], y_new[w2]),values[w2]

if __name__=='__main__':
	X,Y,Z = process_gridScanXY(plot=True)
