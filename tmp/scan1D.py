import os
import sys
import logging
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
import numpy as np
import exdir


class scan1DThread(QtCore.QThread):
    progressSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(object)
    dataReadySignal = QtCore.Signal(object)
    running = False
    store = None
    fname = "tmp"
    header = ["index", "axis", "A", "B", "C"]

    def __init__(self, parent=None):
        super(scan1DThread, self).__init__(parent)

        self.fname = Path(self.parent().expData_filePath.text())
        if ".exdir" in str(self.fname):
            pass
        else:
            self.fname = str(self.fname).split(".exdir")[0] + ".exdir"
        self.store = exdir.File(self.fname)

    def __isRunning(f):
        def wrapper(*args, **kw):
            r = None
            try:
                self = args[0]
                self.running = True
                r = f(*args, **kw)
                self.running = False
                self.finishedSignal.emit(time.time())
            except Exception as ex:
                logging.error("Scan1D:ERR", exc_info=ex)
                self.finishedSignal.emit(time.time())
            return r

        return wrapper

    @__isRunning
    def run(self):
        self.parent().ui.actionStop.setChecked(False)
        # self.parent().calibrTimer.stop()
        self.parent().live_x = np.array([])
        self.parent().live_pmtA = np.array([])
        self.parent().live_pmtB = np.array([])

        # fname = self.parent().expData_filePath.text()+"_"+str(round(time.time()))+".txt"
        # with open(fname,'a') as f:
        # 	f.write("#X\tY\tZ\tHWP_power\tHWP_stepper\tpmtA_signal\tpmtB_signal\ttime\n")
        axis = self.parent().ui.scan1D_axis.currentText()
        start_x = self.parent().ui.scan1D_start.value()
        end_x = self.parent().ui.scan1D_end.value()
        step_x = self.parent().ui.scan1D_step.value()
        if start_x > end_x:
            step_x *= -1
        if start_x == end_x:
            end_x += step_x
        steps_range_default = np.arange(start_x, end_x, step_x)
        if len(steps_range_default) == 0:
            self.parent().statusBar_message.setText("Error: invalid start|end|step")
            microV_logger.error("invalid start|end|step")
            return
        move_function = lambda target: self.parent().abstractMoveTo(target, axis=axis)

        # with pd.HDFStore(self.parent().expData_filePath.text()+"_"+str(round(time.time()))+".h5") as store:
        # self.header = ['axis','A','B','C']
        suffix = self.parent().ui.scan1D_name_suffix.text()
        group_name = "scan1D_axis_" + axis
        if suffix != "":
            group_name += "_" + suffix

        if not group_name in self.store:
            try:
                scan1d_group = self.store.create_group(group_name)
            except:
                traceback.print_exc()
                microV_logger.error("scan1D:wrongName. use_default")
                group_name = group_name.replace("_" + suffix, "")
                scan1d_group = self.store.create_group(group_name)
        else:
            scan1d_group = self.store[group_name]

        self.storeActiveGroup = scan1d_group.create_group("data" + str(time.time()))

        def _scan1D(ex_wl=None, steps_range=steps_range_default):
            if not ex_wl is None:
                _storeActiveGroup = self.storeActiveGroup.create_group(
                    "scan1D_ex%d" % int(ex_wl)
                )
            else:
                _storeActiveGroup = self.storeActiveGroup
            previewData_temp = np.empty((len(steps_range), len(self.header)))
            if "previewData" in _storeActiveGroup:
                previewDataset = _storeActiveGroup["previewData"]
            else:
                previewDataset = _storeActiveGroup.create_dataset(
                    "previewData", data=previewData_temp
                )

            previewDataset.attrs["header"] = self.header
            previewDataset.attrs["axis"] = axis

            # import pdb; pdb.set_trace()

            if (
                self.parent().readoutSources.currentText() == "AndorCamera"
                or self.parent().readoutSources.currentText() == "spectraSIM"
            ):
                if len(self.parent().andorCCDBaseline) <= 1:
                    self.parent().andorCameraGetBaseline()
                baseline = np.array(
                    [self.parent().andorCCD_wavelength, self.parent().andorCCDBaseline]
                )
                # import pdb; pdb.set_trace()
                baseline = np.hstack(
                    (np.array([[np.nan, np.nan], [np.nan, np.nan]]), baseline)
                )
                if len(self.parent().andorCCDBaseline) == 0:
                    spectra_len = 1024
                else:
                    spectra_len = len(self.parent().andorCCDBaseline)
                spectra_temp = np.empty(((len(steps_range) + 2, spectra_len + 2)))
                spectra_temp[:2] = baseline

                spectraDataset = _storeActiveGroup.create_dataset(
                    "spectra", data=spectra_temp
                )
                spectraDataset.attrs["header"] = [
                    ["index", "axisPos", "wavelengths..."],
                    ["index", "axisPos", "baseline..."],
                    ["index", "axisPos", "spectra..."],
                    ["...", "...", "..."],
                ]

            # store.put('/data_table', df)

            # axis_df = pd.DataFrame([np.nan],columns=['axis'])
            # store.put('/metadata_', axis_df)

            previewDataset.attrs["metadata"] = {}
            real_position = self.parent().piStage.getPosition()
            previewDataset.attrs["startTime"] = time.time()
            progress = np.arange(len(steps_range))
            metadata = {}
            metadata_list = []
            lastUpdate = time.time()

            metadata = {}
            metadata["axisPos"] = float(steps_range[0])
            metadata["ex_wl"] = self.parent().ui.laserWavelength.value()
            metadata["source"] = self.parent().readoutSources.currentText()
            real_position = self.parent().piStage.getPosition()
            metadata["centerXYZ"] = tuple(real_position.tolist())
            metadata["exposure"] = self.parent().ui.andorCameraExposure.value()
            metadata["laserBultInPower"] = self.parent().ui.laserPower_internal.value()
            metadata["HWP_power"] = float(self.parent().ui.HWP_angle.text())
            metadata["HWP_stepper"] = float(self.parent().ui.HWP_stepper_angle.text())
            metadata["time"] = time.time()
            # metadata['mocoPort'] = self.parent().mocoSection.currentText()
            metadata_rec = generate_recarray_from_defaultdict(metadata)
            # import pdb; pdb.set_trace()
            if "previewMetaData" in _storeActiveGroup:
                previewMetaData = _storeActiveGroup["previewMetaData"]
            else:
                previewMetaData = _storeActiveGroup.create_dataset(
                    "previewMetaData",
                    shape=(len(steps_range),),
                    dtype=metadata_rec.dtype,
                )

            for j, new_pos in enumerate(steps_range):
                t0_ = time.time()
                if self.parent().ui.actionStop.isChecked() or not self.running:
                    break
                if self.parent().ui.actionPause.isChecked():
                    while self.parent().ui.actionPause.isChecked():
                        time.sleep(0.1)
                pmt_valA, pmt_valB, pmt_valC = self.parent().getABData(
                    update=True
                )  # self.parent().readDAQmx(print_dt=True)

                real_position = [
                    round(p, 4) for p in self.parent().piStage.getPosition()
                ]
                HWP_angle = float(self.parent().ui.HWP_angle.text())
                HWP_stepper_angle = float(self.parent().ui.HWP_stepper_angle.text())

                if axis == "X":
                    x = real_position[0]
                    # self.parent().setUiPiPos(pos=real_position)
                elif axis == "Y":
                    x = real_position[1]
                    # self.parent().setUiPiPos(pos=real_position)
                elif axis == "Z":
                    x = real_position[2]
                    # self.parent().setUiPiPos(pos=real_position)
                elif axis == "HWP_Power":
                    x = HWP_angle
                elif axis == "HWP_Polarization":
                    x = HWP_stepper_angle
                elif axis == "LaserWavelength":
                    x = self.parent().ui.laserWavelength.value()
                elif axis == "Delay":
                    x = self.parent().ui.mocoPosition.value()
                else:
                    x = 0
                t0_1 = time.time()
                self.parent().live_pmtA = np.hstack((self.parent().live_pmtA, pmt_valA))
                self.parent().live_pmtB = np.hstack((self.parent().live_pmtB, pmt_valB))
                self.parent().live_x = np.hstack((self.parent().live_x, x))
                t0_2 = time.time()

                # print(self.parent().live_x,self.parent().live_pmtA,self.parent().live_pmtB)
                if time.time() - lastUpdate > 0.3:

                    self.dataReadySignal.emit(
                        {
                            "name": "line_pmtA",
                            "time": time.time(),
                            "data": [self.parent().live_x, self.parent().live_pmtA],
                        }
                    )
                    self.dataReadySignal.emit(
                        {
                            "name": "line_pmtB",
                            "time": time.time(),
                            "data": [self.parent().live_x, self.parent().live_pmtB],
                        }
                    )
                    lastUpdate = time.time()
                t0_3 = time.time()

                # app.processEvents()
                # dataSet = real_position +[HWP_angle, HWP_stepper_angle, pmt_valA, pmt_valB, time.time()]# + spectra[spectra_range[0]:spectra_range[1]]
                # print(dataSet[-1])
                # with open(fname,'a') as f:
                # 	f.write("\t".join([str(round(i,10)) for i in dataSet])+"\n")
                move_function(new_pos)
                data_out = np.array([j, x, pmt_valA, pmt_valB, pmt_valC])

                # store['/data_table'] = df
                # import pdb; pdb.set_trace()
                # if 'previewData' in self.storeActiveGroup:
                previewDataset[j] = data_out
                # else:
                # 	previewData_temp[j] = data_out
                # 	previewDataset = self.storeActiveGroup.require_dataset('previewData', data = data_out)

                # axis_df = pd.DataFrame([x],columns=[axis])

                metadata = {}
                metadata["axisPos"] = float(x)

                metadata["ex_wl"] = self.parent().ui.laserWavelength.value()
                metadata["source"] = self.parent().readoutSources.currentText()

                # metadata['spectra'] = self.parent().andorCCDIntens.tolist()
                # metadata['spectra_wl'] = self.parent().andorCCD_wavelength.tolist()

                real_position = self.parent().piStage.getPosition()
                metadata["centerXYZ"] = tuple(real_position.tolist())
                metadata["exposure"] = self.parent().ui.andorCameraExposure.value()
                metadata[
                    "laserBultInPower"
                ] = self.parent().ui.laserPower_internal.value()
                metadata["HWP_power"] = float(self.parent().ui.HWP_angle.text())
                metadata["HWP_stepper"] = float(
                    self.parent().ui.HWP_stepper_angle.text()
                )
                metadata["time"] = time.time()
                # metadata['mocoPort'] = self.parent().mocoSection.currentText()
                for k in metadata:
                    previewMetaData[k][j] = metadata[k]
                # import pdb; pdb.set_trace()

                # previewDataset.attrs['metadata'][j] = metadata
                metadata_list.append(metadata)

                # import pdb; pdb.set_trace()
                if self.parent().readoutSources.currentText() == "AndorCamera":
                    spectra = np.hstack(
                        [j, x, self.parent().andorCCDIntens]
                    )  # .reshape(1,-1)

                    spectraDataset[j + 2] = spectra
                    if self.parent().ui.scan1D_bg_at_Z.isChecked():
                        pos = self.parent().ui.scan1D_bg_at_Z_pos.value()
                        spectraDataset.attrs["scan1D_bg_at_Z"] = pos

                    # else:
                    # 	spectra_temp = np.empty(((len(steps_range)+2,len(self.parent().andorCCDBaseline)+2)))
                    # 	spectra_temp[:2] = baseline
                    # 	spectra_temp[j] = spectra
                    # 	self.storeActiveGroup.require_dataset('spectra', data = spectra)

                # store.flush()
                # self.parent().ui.scan1D_Scan.setChecked(False)
                # import pdb; pdb.set_trace()
                self.progressSignal.emit(int(progress[j]))
                t1_ = time.time()
                print("dt", t1_ - t0_, t1_ - t0_3, t0_3 - t0_2, t0_2 - t0_1, t0_1 - t0_)
            previewDataset.attrs["metadata"] = metadata_list

        if self.parent().ui.scan1D_varyLaserWavelength.isChecked():
            start_wavelength = self.parent().ui.scan1D_varyLaserWavelength_start.value()
            end_wavelength = self.parent().ui.scan1D_varyLaserWavelength_end.value()
            step_wavelength = self.parent().ui.scan1D_varyLaserWavelength_step.value()
            if start_wavelength > end_wavelength:
                step_wavelength = -step_wavelength

            # wl_range = np.arange(start_wavelength,end_wavelength+step_wavelength,step_wavelength)
            if (
                start_wavelength != (end_wavelength + step_wavelength)
                and step_wavelength != 0
            ):
                wl_range = np.arange(
                    start_wavelength, end_wavelength + step_wavelength, step_wavelength
                )
                for wl in wl_range:
                    if self.parent().ui.actionPause.isChecked():
                        while self.parent().ui.actionPause.isChecked() and self.running:
                            time.sleep(0.02)
                    if not self.running:
                        return

                    self.parent().laserSetWavelength(wl, closeShutter=True)
                    # wl_center = self.parent().shamrockWavelength_center()
                    # self.parent().shamrockSetWavelength(wl=wl_center)

                    # microV_logger.info('shamrockSetWavelength:%f'%wl_center)

                    if self.parent().readoutSources.currentText() == "AndorCamera":
                        wl_center = self.parent().shamrockWavelength_center()
                        self.parent().shamrockSetWavelength(wl=wl_center)
                        if self.parent().ui.scan1D_bg_at_Z.isChecked():
                            pos = self.parent().ui.scan1D_bg_at_Z_pos.value()
                            self.parent().piStage.move(2, [pos], wait=True)
                            self.parent().andorCameraGetBaseline()
                    start_x = self.parent().ui.scan1D_start.value()
                    end_x = self.parent().ui.scan1D_end.value()
                    step_x = self.parent().ui.scan1D_step.value()
                    if start_x > end_x:
                        step_x *= -1
                    if start_x == end_x:
                        end_x += step_x
                    steps_range = np.arange(start_x, end_x, step_x)
                    if len(steps_range) == 0:
                        self.parent().statusBar_message.setText(
                            "Error: invalid start|end|step"
                        )
                        microV_logger.error("invalid start|end|step, using default")
                        steps_range = steps_range_default
                    _scan1D(wl, steps_range=steps_range)
            else:
                if self.parent().readoutSources.currentText() == "AndorCamera":
                    wl_center = self.parent().shamrockWavelength_center()
                    self.parent().shamrockSetWavelength(wl=wl_center)
                wl_range = [start_wavelength]
                _scan1D()
        else:
            _scan1D()

        self.running = False
