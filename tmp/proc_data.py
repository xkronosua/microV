import os
import sys
import time

from pathlib import Path
from glob import glob

import exdir
import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import re
from lmfit.models import ConstantModel, GaussianModel
from scipy.signal import medfilt
from scipy.interpolate import interp1d


# plt.style.use('dark_background')
# set background color
# bg_color = (0.9,0.9,0.9)


def fit(x, y, ex_wl):

    bg = ConstantModel(prefix="bg_")
    pars = bg.guess(y, x=x)
    if ex_wl < 700:
        pars["bg_c"].set(value=y.min(), vary=False)
    else:
        pars["bg_c"].set(value=y[:10].mean(), vary=False)

    tunable_sh = GaussianModel(prefix="tun_")
    pars.update(tunable_sh.make_params())

    pars["tun_center"].set(value=ex_wl / 2, vary=False)
    pars["tun_sigma"].set(value=4.5, min=4, max=5)
    pars["tun_amplitude"].set(value=1, min=0.1 + y.min())

    fix_sh = GaussianModel(prefix="fix_")
    pars.update(fix_sh.make_params())

    pars["fix_center"].set(value=1045 / 2, vary=False)
    pars["fix_sigma"].set(value=4.5, min=4, max=5)
    pars["fix_amplitude"].set(value=1, min=0.1 + y.min())

    sum_freq = GaussianModel(prefix="sum_")
    pars.update(sum_freq.make_params())

    pars["sum_center"].set(value=1 / (1 / ex_wl + 1 / 1045), vary=False)
    pars["sum_sigma"].set(value=4.5, min=4, max=5)
    pars["sum_amplitude"].set(value=0.01, min=0)

    mod = bg + tunable_sh + fix_sh + sum_freq

    init = mod.eval(pars, x=x)
    out = mod.fit(y, pars, x=x)
    # print(x,y,out.best_fit)
    return out, init, mod


pars_to_save = [
    "bg_c",
    "fix_center",
    "fix_amplitude",
    "fix_sigma",
    "tun_center",
    "tun_amplitude",
    "tun_sigma",
    "sum_center",
    "sum_amplitude",
    "sum_sigma",
]

c = 299792458  # m/s

res_data = []

path = sys.argv[1]

print(path)
store = exdir.File(path, "r")

keys = [k for k in store.keys() if k[0] != "_"]
keys.sort()
keys = keys[::-1]

data_ = []
dtype = store[keys[0]]["scan"].dtype


for k in keys:
    try:
        dd = store[k]["scan"]
        if dd.dtype != dtype:
            break
        w = dd["time"] != 0
        if sum(w) == 0:
            break
        data_.append(dd[w])
    except:
        traceback.print_exc()
        break
data = np.hstack(data_)

spectra_len_ = len(data["AndorCamera"]["wavelength"][0])
# out_data = np.zeros(len(data),dtype=[('Ex_wavelength','f8'),('spectra',('wavelength','f8',spectra_len_),('intens','f8',spectra_len_),('wavelength','f8',spectra_len_)), ('power','f8'), ('power_IR','f8')])
res = []
N_graphs = len(np.unique(data["LaserWavelength"]))
N_rows = int(np.ceil(N_graphs / 2))

fig, axes = plt.subplots(
    N_rows,
    2,
    sharex=True,
)
fig1, axes1 = plt.subplots(
    N_rows,
    2,
    sharex=True,
)
axes_ = np.hstack(axes.T)
axes1_ = np.hstack(axes1.T)
# axes.flatten()
res = []

filter_transm_ = np.load("FESH0700/data.npy")
filter_transm = interp1d(filter_transm_["wavelength"], filter_transm_["transmittance"])
for i, ex_wl in enumerate(np.unique(data["LaserWavelength"])[::-1]):
    print("Ex_wl", ex_wl)
    w = data["LaserWavelength"] == ex_wl
    d = data[w]
    max_SFG = 0
    max_SFG_ = 0.5
    I = []

    for index, row in enumerate(d):
        x = row["Delay_line_position"]
        # print(row)
        wl = row["AndorCamera"]["wavelength"]
        intens = row["AndorCamera"]["intensity"] / filter_transm(wl)
        iu = interp1d(wl, intens)
        # if iu(1/(1/ex_wl+1/1045))< iu(1045/2)/20: continue
        # intens = intens/iu(1045/2)

        # maxX = np.arange(len(d['Delay_line_position']))[d['AndorCamera']['w1+w2']==d['AndorCamera']['w1+w2'].max()][0]
        # plt.plot(wl,intens)

        I.append([iu(1 / (1 / ex_wl + 1 / 1045)), iu(1045 / 2), iu(ex_wl / 2)])
    I = np.array(I)
    d_p = d["Delay_line_position"]
    x0 = d_p[
        I[:, 0] >= (I[:, 0].max() * 0.7)
    ].mean()  # +np.ptp(d['Delay_line_position'])/2
    print(x0)
    axes_[i].plot(d["Delay_line_position"] - x0, I[:, 0], "-g", label=f"_sum")
    axes_[i].plot(d["Delay_line_position"] - x0, I[:, 1], "-r", label=f"_fix")
    axes_[i].plot(d["Delay_line_position"] - x0, I[:, 2], "-b", label=f"{ex_wl}nm")
    axes_[i].legend()
    w = I[:, 0] == I[:, 0].max()

    axes1_[i].plot(
        d["AndorCamera"]["wavelength"][w][0],
        d["AndorCamera"]["intensity"][w][0],
        "-m",
        label=f"zero delay",
    )
    axes1_[i].plot(
        d["AndorCamera"]["wavelength"][0],
        d["AndorCamera"]["intensity"][0],
        "-b",
        label=f"{ex_wl}nm",
    )
    axes1_[i].grid(1)
    axes1_[i].legend()

    res.append(
        {
            "ex_wl": ex_wl,
            "TUN": I[w, 1].mean(),
            "FIX": I[w, 2].mean(),
            "TUN_init": I[-5:, 1].mean(),
            "FIX_init": I[-5:, 2].mean(),
            "SUM": I[:, 0].max(),
        }
    )
    # axes_[i].grid(1)

axes_[i].set_xlim((-0.1, 0.1))
# axes_[-2].plot(1,1,2,2,3,3)
axes_[-3].legend(["SFG", "SHG FIX", "SHG TUN"])


plt.figure()
df = pd.DataFrame(res)
# df=df.set_index('ex_wl')
plt.plot(df.ex_wl, df.FIX, ".-r", label="FIX")
plt.plot(df.ex_wl, df.TUN, ".-b", label="TUN")
plt.plot(df.ex_wl, df.FIX_init, "--r", label="FIX_delay")
plt.plot(df.ex_wl, df.TUN_init, "--b", label="TUN_delay")
plt.plot(df.ex_wl, df.SUM, ".-g", label="SUM")
plt.legend()
plt.xlabel("Ex. wavelength, nm")
plt.ylabel("Intens, arb.un")
plt.grid(1)
plt.show()
