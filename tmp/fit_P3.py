import numpy as np
import exdir
from lmfit.models import GaussianModel, SkewedGaussianModel, Model
import matplotlib.pyplot as plt
from scipy.signal import medfilt, filtfilt, butter
from scipy.interpolate import interp1d
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
from pyqtgraph.parametertree import (
    Parameter,
    ParameterTree,
    ParameterItem,
    registerParameterType,
)
from skimage import transform


def unique(array, return_index=False):
    uniq, index = np.unique(array, return_index=True)
    if return_index:
        return uniq[index.argsort()], index[index.argsort()]
    else:
        return uniq[index.argsort()]


s = exdir.File("P3_ex1064_HWP90deg.exdir")
ts = max(s["scan3D"])
print(ts)
dset = s["scan3D"][ts]["data"]

data = dset["data"]["AndorCamera"]["raw"]["intensity"][0, :, :, 0, :]
x = dset["data"]["AndorCamera"]["raw"]["wavelength"][0, 0, 0, 0, :].astype(float)
x = x + 2
shape = data[:, :, 0].shape
if "img" in s["scan3D"][ts]:
    img = s["scan3D"][ts]["img"]
else:
    s["scan3D"][ts].create_dataset(
        "img",
        shape=(1,),
        dtype=[
            ("wavelength", "f4", len(x)),
            ("2w1", "f4", shape),
            ("2w2", "f4", shape),
            ("3w2", "f4", shape),
            ("w1+w2", "f4", shape),
            ("bg", "f4", shape),
        ],
    )

img = s["scan3D"][ts]["img"]

Chan = ["2w1", "2w2", "w1+w2", "3w2", "bg"]
_w1 = 1045
_w2 = 1064
centers = np.array([_w2 / 2])

bg_grid = np.linspace(x.min(), x.max(), len(x))

# print(bg_grid)
w = np.ones(len(bg_grid)) == 1
for c in centers:
    w = w & ~((bg_grid > (c - 30)) & (bg_grid < (c + 30)))

bg_grid = bg_grid[w]
# print(bg_grid)

for i in list(range(data.shape[0]))[::1]:
    for j in list(range(data.shape[1]))[::1]:
        y_ = medfilt(data[i, j].astype(float), 5)

        bg_interp = interp1d(x, y_)
        b, a = butter(3, 0.2)
        bg = bg_interp(bg_grid)
        # bg = filtfilt(b,a,bg)
        # bg_grid = filtfilt(b,a,bg_grid)
        bg = medfilt(bg, 51)
        bg_grid = medfilt(bg_grid, 51)

        bg_interp = interp1d(bg_grid, bg, bounds_error=False, fill_value=0)
        bg = bg_interp(x)
        y = y_ - bg
        interp = interp1d(x, y)

        img["bg"][0, i, j] = bg.sum()
        img["2w2"][0, i, j] = y.sum() / len(y)


X = np.linspace(
    dset["metadata"]["X_target"].min(), dset["metadata"]["X_target"].max(), shape[0]
)
Y = np.linspace(
    dset["metadata"]["Y_target"].min(), dset["metadata"]["Y_target"].max(), shape[1]
)
# scale = np.array([0.15,0.15])#
scale = np.array([np.diff(X).mean(), np.diff(Y).mean()])
X, Y = np.meshgrid(Y, X)


# QtGui.QApplication.setGraphicsSystem('raster')
app = pg.mkQApp()
mw = QtGui.QMainWindow()

cw = QtGui.QWidget()
mw.setCentralWidget(cw)
l = QtGui.QGridLayout()
cw.setLayout(l)

mw.show()

imv = {}
index = 0
for i in range(2):
    for j in range(3):
        if i == 1 and j == 2:
            break
        im = pg.ImageView()
        grid = pg.GridItem()
        grid.setTextPen("y")
        im.addItem(grid)
        txt = pg.TextItem(Chan[index], color="y", border="w", fill=(0, 0, 255, 200))
        im.addItem(txt)
        l.addWidget(im, i, j, 1, 1)
        imv[Chan[index]] = im
        im.getView().setXRange(0, np.ptp(X))
        im.getView().setYRange(0, np.ptp(Y))
        im.ui.roiBtn.hide()
        im.ui.menuBtn.hide()
        index += 1

ang = 0.5
period = np.array([2.4, 2.66])
a = 1
origin = np.array([-0.2, 0.4])
beams_shift = np.array([0.2, -0.4])
roi = []
shearX = 0.08
shearY = -0.01


def rotate(v, deg):
    """Use numpy to build a rotation matrix and take the dot product."""
    radians = np.deg2rad(deg)
    c, s = np.cos(radians), np.sin(radians)
    j = np.matrix([[c, s], [-s, c]])
    m = np.dot(j, v)
    return np.array(m)


def shear(v, shearX=0, shearY=0):
    """Use numpy to build a rotation matrix and take the dot product."""
    j = np.matrix([[1, shearX], [shearY, 1]])
    m = np.dot(j, v)
    return np.array(m)


# scale = np.array([0.3, 0.3])

roi = []
roi_N = 9
for ch in imv:
    for i in range(roi_N):
        row = []
        for j in range(roi_N):
            r = pg.ROI(pos=origin, pen="g")
            roi.append(r)
            imv[ch].addItem(r)

for i, ch in enumerate(Chan):
    im = pg.gaussianFilter(img[ch][0], (1, 1))
    imv[ch].setImage(
        im,
        pos=(0, 0),
        scale=scale,
        autoHistogramRange=True,
        autoRange=False,
        autoLevels=True,
    )


params = [
    {"name": "angle", "type": "float", "value": ang, "step": 0.1},
    {"name": "periodX", "type": "float", "value": period[0], "step": 0.1},
    {"name": "periodY", "type": "float", "value": period[1], "step": 0.1},
    {"name": "a", "type": "float", "value": a, "step": 0.1},
    {"name": "w1_originX", "type": "float", "value": origin[0], "step": 0.1},
    {"name": "w1_originY", "type": "float", "value": origin[1], "step": 0.1},
    {"name": "w2_beamShiftX", "type": "float", "value": beams_shift[0], "step": 0.1},
    {"name": "w2_beamShiftY", "type": "float", "value": beams_shift[1], "step": 0.1},
    {"name": "shearX", "type": "float", "value": shearX, "step": 0.01},
    {"name": "shearY", "type": "float", "value": shearY, "step": 0.01},
]

## Create tree of Parameter objects

Params = Parameter.create(name="params", type="group", children=params)


def update():
    ang = Params.child("angle").value()
    period = np.array(
        [Params.child("periodX").value(), Params.child("periodY").value()]
    )
    a = Params.child("a").value()
    origin = np.array(
        [Params.child("w1_originX").value(), Params.child("w1_originY").value()]
    )
    beams_shift = np.array(
        [Params.child("w2_beamShiftX").value(), Params.child("w2_beamShiftY").value()]
    )
    shearX = Params.child("shearX").value()
    shearY = Params.child("shearY").value()

    index = 0
    for ch in imv:
        for i in range(roi_N):
            for j in range(roi_N):
                pos = origin + np.array([i, j]) * period
                pos = rotate(pos, -ang)[0]
                pos = shear(pos, shearX, shearY)[0]

                print(pos)
                if "w2" in ch:
                    if "+" in ch:
                        pos += beams_shift / 2
                    else:
                        pos += beams_shift
                r = roi[index]
                r.setPos(pos)
                r.setSize((a, a))
                r.setAngle(ang + 45)

                index += 1


update()

## If anything changes in the tree, print a message
def change(param, changes):
    print("tree changes:")
    update()


def valueChanging(param, value):
    print("Value changing (not finalized): %s %s" % (param, value))
    update()


# Too lazy for recursion:
for child in Params.children():
    child.sigValueChanging.connect(valueChanging)
    for ch2 in child.children():
        ch2.sigValueChanging.connect(valueChanging)


Params.sigTreeStateChanged.connect(change)

## Create two ParameterTree widgets, both accessing the same data
t = ParameterTree()
t.setParameters(Params)
l.addWidget(t, 1, 2, 1, 1)
sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
sizePolicy.setHorizontalStretch(0)
sizePolicy.setVerticalStretch(0)
t.setSizePolicy(sizePolicy)
print(scale)
# plt.show()

# w = pg.HistogramLUTWidget()


# d = np.array([img['2w1'][0],img['2w2'][0],img['w1+w2'][0]]).transpose(1,2,0)
# print(d.shape)
# im = pg.ImageItem(d)
# imv['2w2+w1'].addItem(im)
# w.setImageItem(im)
# w.setLevelMode('rgba')

# l.addWidget(w,2,0,1,1)
plt.show()
## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == "__main__":
    import sys

    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        QtGui.QApplication.instance().exec_()
