import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter, medfilt
import pandas as pd
from lmfit.models import ConstantModel, GaussianModel, SkewedGaussianModel


def unique_dummy(array, return_index=False):
    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


def unique_(a, return_index=False):
    index = np.arange(a.size)[abs(np.diff(a + 0.1, prepend=a[0] * 2)) > 0]
    if return_index:
        return a[index], index
    else:
        return a[index]


def fitData(x, y, ex_wl, peaks):

    bg = ConstantModel(prefix="bg_")
    pars = bg.guess(y, x=x)
    pars["bg_c"].set(value=0, min=-1, max=1, vary=False)
    mod = bg
    interp = interp1d(x, y, fill_value=0, bounds_error=False)
    for p in peaks:
        if p["type"] == "gaussian":
            if p["center"](ex_wl) >= x.max() or p["center"](ex_wl) <= x.min():
                continue
            prefix = p["name"] + "_"
            mod_tmp = GaussianModel(prefix=prefix)
            pars.update(mod_tmp.make_params())
            pars[prefix + "center"].set(
                value=p["center"](ex_wl),
                min=p["center"](ex_wl) - 5,
                max=p["center"](ex_wl) + 5,
                vary=True,
            )
            pars[prefix + "sigma"].set(
                value=p["sigma"], min=1, max=p["sigma"] + 5, vary=False
            )
            w = (x > p["center"](ex_wl) - 10) & (x < p["center"](ex_wl) + 10)
            height = interp(x[w]).max()
            pars[prefix + "amplitude"].set(value=height / 0.4 * p["sigma"], min=0.0001)

        elif p["type"] == "skewedgaussian":
            prefix = p["name"] + "_"
            mod_tmp = SkewedGaussianModel(prefix=prefix)
            pars.update(mod_tmp.make_params())
            pars[prefix + "center"].set(
                value=p["center"], min=p["center"] - 5, max=p["center"] + 5, vary=True
            )
            pars[prefix + "sigma"].set(value=p["sigma"], min=1, max=50, vary=True)
            pars[prefix + "amplitude"].set(
                value=interp(p["center"]) / 0.4 * p["sigma"], min=0.001
            )
            pars[prefix + "gamma"].set(value=10, min=0.1, max=100, vary=True)

        mod += mod_tmp

    init = mod.eval(pars, x=x)
    out = mod.fit(y, pars, x=x)
    print(out.fit_report())
    return out, init, mod


path = sys.argv[1]

print(path)
store = exdir.File(path, "r")

dataType = "MultiScan"

keys = [k for k in store[dataType] if k[0] != "_"]
sizes = [len(store[dataType][k]["data"]) for k in keys]

dtype = store[dataType][keys[-1]]["data"].dtype

data = np.zeros(np.sum(sizes), dtype=dtype)
last_row = 0
for i, k in enumerate(keys):
    if store[dataType][k]["data"].dtype == dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        data[last_row : last_row + w.sum()] = d[w]
        last_row += w.sum()
        print(i, k)
data = data[data["time"] != 0]


# unique_ex_wls, unique_ex_wls_index = unique(data['position']['LaserWavelength'], return_index=True)

unique_ex_wls, unique_ex_wls_index = unique_(
    data["position"]["LaserWavelength"], return_index=True
)

groups = np.split(data, unique_ex_wls_index)[1:]
#
# if len(groups)>10:
# 	fig, axes0 = plt.subplots(int(np.ceil(len(groups)/3)),3,sharex=True)
# 	axes0 = axes0.T.flatten()
# else:
# 	fig, axes0 = plt.subplots(len(groups),1,sharex=True, )
# if len(groups)==1:
# 	axes0 = [axes0]

# spectral_correction_df = pd.read_csv('Microscope_Forward_grating1.csv')
# spectral_correction = interp1d(
# 	spectral_correction_df['wavelength'],
# 	spectral_correction_df['FESH0700'],
# 	fill_value=np.nan, bounds_error=False)

centers = [0]
colors = "rgbcm"
res = []

sigma = 2.5
peaks = [
    {
        "type": "gaussian",
        "style": "r",
        "name": "G2w2",
        "sigma": sigma,
        "center": lambda ex_wl: ex_wl / 2,
    },
    {
        "type": "gaussian",
        "style": "g",
        "name": "G3w2",
        "sigma": sigma,
        "center": lambda ex_wl: ex_wl / 3,
    },
    {
        "type": "gaussian",
        "style": "b",
        "name": "G2w1",
        "sigma": sigma,
        "center": lambda ex_wl: 1045 / 2,
    },
    {
        "type": "gaussian",
        "style": "c",
        "name": "G3w1",
        "sigma": sigma,
        "center": lambda ex_wl: 1045 / 3,
    },
    {
        "type": "gaussian",
        "style": "--m",
        "name": "Gw1pw2",
        "sigma": sigma,
        "center": lambda ex_wl: 1 / (1 / 1045 + 1 / ex_wl),
    },
    {
        "type": "gaussian",
        "style": "--y",
        "name": "Gw1p2w2",
        "sigma": sigma,
        "center": lambda ex_wl: 1 / (1 / 1045 + 2 / ex_wl),
    },
    {
        "type": "gaussian",
        "style": "--k",
        "name": "G2w1pw2",
        "sigma": sigma,
        "center": lambda ex_wl: 1 / (2 / 1045 + 1 / ex_wl),
    },
    {
        "type": "gaussian",
        "style": "--r",
        "name": "G2w1mw2",
        "sigma": sigma,
        "center": lambda ex_wl: 1 / (2 / 1045 - 1 / ex_wl),
    },
    {
        "type": "gaussian",
        "style": "--b",
        "name": "G2w2mw1",
        "sigma": sigma,
        "center": lambda ex_wl: 1 / (-1 / 1045 + 2 / ex_wl),
    },
    # {'type':'skewedgaussian','name':'pl400','sigma':10,'center':400},
]

filter_data = pd.read_csv("Microscope_Forward_grating1.csv", index_col=0)
spectral_correction = interp1d(
    filter_data.wavelength, filter_data["FGB37x2"], fill_value=0, bounds_error=False
)

for i, d in enumerate(groups[:-1]):
    ex_wl = d["position"]["LaserWavelength"].mean()
    # print(i,d['position']['LaserWavelength'])
    # continue
    exposure = d["data"]["AndorCamera"]["raw"]["exposure"].astype(float)
    intens = d["data"]["AndorCamera"]["raw"]["intensity"].astype(float)
    intens_norm = (intens.T / exposure).T

    bg_mask = d["position"]["CenterIndex"] != 9
    delay_mask = d["position"]["Delay_line_position_zero_relative"] <= 0.05

    masks = [
        bg_mask & delay_mask,
        (~bg_mask) & delay_mask,
        bg_mask & (~delay_mask),
        (~bg_mask) & (~delay_mask),
    ]

    fig, ax = plt.subplots(2, 2, sharex=True, sharey=True)
    ax = ax.flatten()
    y_min = 1
    for mi, mask in enumerate(masks):
        if mi > 1:
            peaks_ = [p for p in peaks if len(p["name"]) <= 4]
            ax[mi - 2].set_title(f"{ex_wl}nm")
        else:
            peaks_ = peaks.copy()

        intens_norm1 = intens_norm[mask]
        if len(intens_norm1) == 0:
            continue
        # s = intens_norm1.sum(axis=1)
        # s = d['data']['AndorCamera']['data']['w1+w2']
        intens_ = intens_norm1[0] * 0
        wl_tmp = d["data"]["AndorCamera"]["raw"]["wavelength"][mask]
        wl = wl_tmp[0].astype(float)

        for j in range(len(intens_norm1)):
            interp = interp1d(
                wl_tmp[j], intens_norm1[j], fill_value=0, bounds_error=False
            )
            intens_ += interp(wl)
        intens_ /= len(intens_norm1)
        intens_ = intens_ / spectral_correction(wl)
        x = medfilt(wl, 9)  # [mask1]
        y = medfilt(intens_, 9)  # [mask1]#/scorr
        # y = medfilt(intens_bg,5)#[mask1]#/scorr
        y_max = y.max()
        y = y / y_max
        if np.isnan(y).sum() > 0:
            continue
        out, init, mod = fitData(x, y, ex_wl, peaks=peaks_)
        y_min = min(y[y > 1e-8].min(), y_min)

        ax[mi].plot(x, y, ".")
        ax[mi].plot(x, out.best_fit, "-")
        # plt.plot(x,init,'--',alpha=0.5)
        tmp = {"ex_wl": ex_wl, "y_max": y_max}
        for p in peaks_:

            if p["name"] + "_amplitude" in out.params:
                style = p["style"][:-1]
                if style == "":
                    style = "solid"
                ax[mi].vlines(
                    [p["center"](ex_wl)],
                    0,
                    1,
                    color=p["style"][-1],
                    linestyle=style,
                    label=p["name"][1:].replace("m", "-").replace("p", "+"),
                )
                comps = out.eval_components(x=x)
                ax[mi].plot(
                    x,
                    comps[p["name"] + "_"],
                    color=p["style"][-1],
                    linestyle=style,
                    label="_",
                )
                tmp[p["name"]] = out.params[p["name"] + "_amplitude"].value * y_max
        res.append(tmp)
        ax[mi].grid(1)

    ax[1].legend()

    ax[0].set_ylim((y_min, 2))
    ax[0].set_yscale("log")


# axes[0].legend()
df = pd.DataFrame(res)
plt.figure()
plt.plot(df.ex_wl, df["G2w2"], "--c", label="2w2")
# plt.plot(df.ex_wl,df['2w2_0delay'],'-c',label='2w2_0delay')
plt.plot(df.ex_wl, df["G2w1"], "--b", label="2w1")
# plt.plot(df.ex_wl,df['2w1_0delay'],'-b',label='2w1_0delay')
if "Gw1pw2" in df:
    plt.plot(df.ex_wl, df["Gw1pw2"], "-g", label="w1+w2")

plt.legend()


plt.show()
