from scipy.stats import multivariate_normal
from scipy.interpolate import LinearNDInterpolator
from skopt import gp_minimize
from skopt import callbacks
from skopt.callbacks import CheckpointSaver

import numpy as np
import matplotlib.pyplot as plt
import time


# f = create_objective_function()
bounds = np.array([[-8.5, 9], [-10, 12]])
x0 = bounds.mean(axis=1) + 5
mu = np.array([1, -2])
cov = np.array([[5, 4], [4, 12]])

x = np.linspace(-10, 10, 100)
y = np.linspace(-20, 20, 100)
X, Y = np.meshgrid(x, y)
pos = np.empty(X.shape + (2,))
pos[:, :, 0] = X
pos[:, :, 1] = Y
rv = multivariate_normal(mu, cov)
Z = rv.pdf(pos)

interp = LinearNDInterpolator((X.flatten(), Y.flatten()), Z.flatten())
N = 0
pos = []


def func(x, evals=3):
    global N  # , pos
    pos.append(x)
    Z = np.array([])
    for i in range(evals):
        z = interp(*x) + np.random.normal(0, 1 / 500)
        Z = np.append(Z, z)
        print(N)
        N += 1
        time.sleep(0.1)
    return -np.nanmean(Z)


solvers = "EI,LCB,PI".split(",")
solvers = ["LCB"]
logs = {}
checkpoint_saver = CheckpointSaver(
    "./checkpoint.pkl", compress=9
)  # keyword arguments will be passed to `skopt.dump`

for solver in solvers:
    print(solver)
    pos = []
    logs[solver] = gp_minimize(
        func,  # the function to minimize
        bounds,  # the bounds on each dimension of x
        x0=x0.tolist(),  # the starting point
        acq_func=solver,  # the acquisition function (optional)
        n_calls=15,  # number of evaluations of f including at x0
        n_random_starts=2,  # the number of random initial points
        # callback=[checkpoint_saver],
        # a list of callbacks including the checkpoint saver
        # verbose=True,
        # random_state=777,
        noise="gaussian",
    )
    # logs[solver] = minimize(func, x0=x0,args=(pos,1,),
    # 	constraints=[LinearConstraint(np.eye(x0.shape[0]),lb=bounds[:,0],ub=bounds[:,1])],
    # 	bounds=bounds,
    # 	method=solver,
    # 	options={'maxiter':20,'maxls':2, 'maxfun':10,'maxev':10, 'maxfev':10,'xtol':0.1,
    # 	})
    logs[solver].xl = np.array(logs[solver].x_iters)
    N = 0

# make sure different traces are somewhat visually separable
colors = ["r", "g", "b", "y", "m", "y", "r", "g"]
markers = ["x", "+", "o", "s", "p", "x", "+", "o"]


CS = plt.contourf(X, Y, Z)
# plt.clabel(CS, inline=1, fontsize=10)
for i, solver in enumerate(solvers):
    plt.scatter(
        logs[solver].xl[:, 0],
        logs[solver].xl[:, 1],
        c=colors[i],
        marker=markers[i],
        alpha=0.80,
    )
    plt.plot(
        logs[solver].xl[:, 0],
        logs[solver].xl[:, 1],
        "-",
        c=colors[i],
        alpha=0.80,
        label=solver,
    )
    plt.scatter(
        logs[solver].xl[0, 0],
        logs[solver].xl[0, 1],
        s=100,
        c=colors[i],
        marker="o",
        edgecolor="k",
        alpha=0.80,
    )
    plt.scatter(
        logs[solver].x[0],
        logs[solver].x[1],
        s=200,
        c=colors[i],
        marker="s",
        edgecolor="m",
    )

plt.legend()
fix, ax = plt.subplots(1, 2)
for i, solver in enumerate(solvers):
    ax[0].plot(
        logs[solver].xl[:, 0], logs[solver].func_vals, "-", c=colors[i], alpha=0.80
    )
    ax[0].plot(
        logs[solver].x[0], logs[solver].func_vals.min(), "o" + colors[i], alpha=0.80
    )
    ax[1].plot(
        logs[solver].xl[:, 1], logs[solver].func_vals, "-", c=colors[i], alpha=0.80
    )
    ax[1].vlines(
        logs[solver].x[1], logs[solver].func_vals.min(), "o" + colors[i], alpha=0.80
    )

plt.show()
