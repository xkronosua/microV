from PyQt5 import QtCore, QtGui, QtWidgets
#from PyQt5.QtCore import Qt
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
import numpy as np
from collections import defaultdict
import time

from collections.abc import MutableMapping

def _flatten_dict_gen(d, parent_key, sep):
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, MutableMapping):
            yield from flatten_dict(v, new_key, sep=sep).items()
        else:
            yield new_key, v



def flatten_dict(d: MutableMapping, parent_key: str = '', sep: str = '.'):
    return dict(_flatten_dict_gen(d, parent_key, sep))

def group_dict(d, return_info=False):
    groups = defaultdict(list)
    for Class,info in d:
        if return_info:
            groups[Class].append((info, d[Class, info]))
        else:
            groups[Class].append(d[Class, info])
    return groups

def pen2color(pen):
    if type(pen) == str:
        color = pg.mkPen(color=pen).color()
    elif type(pen) == tuple:
        color = QtGui.QColor(*pen)
    elif type(pen) == QtGui.QColor:
        color = pen
    elif type(pen) == int:
        color = pg.intColor(pen)
    else:
        color = pen.color()
    return color


class TableOfLines(QtWidgets.QWidget):
    """
    """
    max_lines_N = 100
    LINES = {}
    LINE_CLASSES_backup = {}
    TABLES = {}
    PLOT_WIDGETS = {}

    def __init__(self, max_lines_N=100):
        super(TableOfLines, self).__init__()
        self.max_lines_N = max_lines_N
        layout = QtWidgets.QVBoxLayout()
        self.toolbox = QtWidgets.QToolBox()
        layout.addWidget(self.toolbox)



        self.setLayout(layout)

    def addPage(self, port_name, plotWidget):
        widget = QtGui.QWidget()
        layout = QtWidgets.QGridLayout()

        table = QtGui.QTableWidget()
        layout.addWidget(table, 0, 0, 1, 2)

        self.TABLES[port_name] = table
        self.LINES[port_name] = {}
        self.LINE_CLASSES_backup[port_name] = {}

        self.PLOT_WIDGETS[port_name] = plotWidget
        table.setObjectName(f"TableOfLines_{port_name}")

        table.setRowCount(self.max_lines_N)
        table.setColumnCount(3)
        for i in range(self.max_lines_N):
            header = table.horizontalHeader()
            header.setSectionResizeMode(0, QtGui.QHeaderView.ResizeToContents)
            header.setSectionResizeMode(1, QtGui.QHeaderView.Stretch)
            header.setSectionResizeMode(2, QtGui.QHeaderView.ResizeToContents)

            item_class = QtGui.QTableWidgetItem(str(i))
            item_class.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            item_class.setCheckState(QtCore.Qt.Unchecked)
            table.setItem(i, 0, item_class)

            item_name = QtGui.QTableWidgetItem(str(i))
            table.setItem(i, 1, item_name)


            colorButton = QtGui.QTableWidgetItem("   ")
            colorButton.setFlags(
                colorButton.flags()
                ^ QtCore.Qt.ItemIsEditable
                ^ QtCore.Qt.ItemIsSelectable
            )
            table.setItem(i, 2, colorButton)

            table.item(i, 2).setBackground(QtGui.QColor(164, 155, 150, 255))

        table.itemClicked.connect(self.on_itemClicked)

        groupByClass = QtWidgets.QCheckBox('Group')
        groupByClass.setObjectName(f"TableOfLines_{port_name}_group")
        groupByClass.toggled[bool].connect(self.refresh_lines)
        layout.addWidget(groupByClass, 1,0)


        show_hide = QtWidgets.QPushButton('Show/Hide')
        show_hide.setObjectName(f"TableOfLines_{port_name}_ShowHide")
        show_hide.toggled[bool].connect(self.refresh_lines)
        layout.addWidget(show_hide, 1, 1)
        show_hide.setCheckable(True)

        widget.setLayout(layout)
        self.toolbox.addItem(widget, port_name)

    def refresh_lines(self, state=None, port_name=None):
        action = None
        if not state is None:
            port_name, action = self.sender().objectName().split("_")[1:]
        if port_name is None:
            for port_name in self.LINES:
                self.refresh_lines(port_name=port_name)
            return
        print(port_name, action)
        groupByClass = self.findChild(QtWidgets.QCheckBox, f"TableOfLines_{port_name}_group")
        self.TABLES[port_name].blockSignals(True)

        if action == "ShowHide":
            for line in self.LINES[port_name].values():
                line.setVisible(state)
            for i in range(self.TABLES[port_name].rowCount()):
                self.TABLES[port_name].item(i,0).setCheckState(
                    QtCore.Qt.Checked if state else QtCore.Qt.Unchecked
                )
        else:

            if groupByClass.isChecked():
                lines_ = group_dict(self.LINES[port_name])
                for row, Class in enumerate(lines_):
                    pen     = lines_[Class][-1].opts["pen"]
                    checked = lines_[Class][-1].isVisible()
                    color = pen2color(pen)

                    self.TABLES[port_name].item(row,2).setBackground(color)
                    self.TABLES[port_name].item(row, 0).setCheckState(
                        QtCore.Qt.Checked if checked else QtCore.Qt.Unchecked
                    )
                    self.TABLES[port_name].item(row, 0).setText(Class)
                    self.TABLES[port_name].item(row, 1).setText("--")
                    for line in lines_[Class]:
                        line.setVisible(checked)
                        line.setPen(pen)
            else:
                lines_ = self.LINES[port_name]

                for row, Class_info in enumerate(lines_):
                    pen = lines_[Class_info].opts["pen"]
                    color = pen2color(pen)
                    checked = lines_[Class_info].isVisible()
                    self.TABLES[port_name].item(row,2).setBackground(color)
                    self.TABLES[port_name].item(row, 0).setCheckState(
                        QtCore.Qt.Checked if checked else QtCore.Qt.Unchecked
                    )
                    self.TABLES[port_name].item(row, 0).setText(Class_info[0])
                    self.TABLES[port_name].item(row, 1).setText(Class_info[1])


            N = len(lines_)

            for i in range(self.TABLES[port_name].rowCount()):
                self.TABLES[port_name].setRowHidden(i, i >= N)
        self.TABLES[port_name].blockSignals(False)


    def checkIfExists(self, port_name, Class, info):
        if (Class,info) in self.LINES[port_name]:
            return True
        return False

    def getLine(self, port_name, Class, info):
        if self.checkIfExists(port_name, Class, info):
            return self.LINES[port_name][Class, name]
        return

    def addLine(self, port_name, Class, info, line=None, x=None, y=None):
        groupByClass = self.findChild(QtWidgets.QCheckBox, f"TableOfLines_{port_name}_group")

        if not (Class,info) in self.LINES[port_name]:
            if len(self.LINES[port_name])>=self.max_lines_N:
                self.removeLine(port_name,*list(self.LINES[port_name].keys())[0])
            if line is None:
                line = pg.PlotCurveItem(x, y,
                    name=f"{Class}[{info}]",
                    pen=pg.intColor(len(self.LINES[port_name]), maxHue=200),
                )
            self.LINES[port_name][Class,info] = line
            self.PLOT_WIDGETS[port_name].addItem(line)

        else:
            self.LINES[port_name][Class,info].setData(*line.getData())

        if groupByClass.isChecked():
            if Class in self.LINE_CLASSES_backup[port_name]:
                line.setVisible(self.LINE_CLASSES_backup[port_name][Class]['visible'])
                line.setPen(self.LINE_CLASSES_backup[port_name][Class]['pen'])
            else:
                pen     = line.opts["pen"]
                visible = line.isVisible()

                self.LINE_CLASSES_backup[port_name][Class] = {"pen": pen, "visible":visible}
        self.refresh_lines(port_name=port_name)


    def removeLine(self, port_name, Class, info):
        if (Class,info) in self.LINES[port_name]:
            if not [c for c, info in self.LINES[port_name]].count(Class):
                pen     = self.LINES[port_name][Class, info].opts["pen"]
                visible = self.LINES[port_name][Class, info].isVisible()

                self.LINE_CLASSES_backup[port_name][Class] = {"pen":pen, "visible":visible}

            self.PLOT_WIDGETS[port_name].removeItem(self.LINES[port_name][Class, info])
            del self.LINES[port_name][Class, info]

    def removeAll(self):
        for port_name in self.LINES:
            Class_info = list(self.LINES[port_name].keys())
            for Class_info_ in Class_info:
                self.removeLine(port_name, *Class_info_)
            self.LINE_CLASSES_backup[port_name] = {}
        self.refresh_lines()

    def on_itemClicked(self, item):
        Class = self.sender().item(item.row(), 0).text()
        info = self.sender().item(item.row(), 1).text()

        table = self.sender()
        port_name = self.sender().objectName().split("_")[-1]
        row = item.row()

        groupByClass = self.findChild(QtWidgets.QCheckBox, f"TableOfLines_{port_name}_group")

        if groupByClass.isChecked():
            lines = group_dict(self.LINES[port_name])[Class]
        else:
            lines = [self.LINES[port_name][Class,info]]

        if item.column() == 0:
            checked = item.checkState() == QtCore.Qt.Checked
            for line in lines:
                line.setVisible(checked)

        elif item.column() == 2:
            color = QtGui.QColorDialog.getColor()
            if color.isValid():
                item.setBackground(color)
                for line in lines:
                    line.setPen(color)

        #print(port_name,lines.keys(), Class, info)



# if __name__ == "__main__":
#
#     app = QtWidgets.QApplication([])
#     table = TableOfLines()
#     table.show()
#     app.exec_()

# Subclass QMainWindow to customize your application's main window
class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.cw = QtGui.QWidget()
        self.setCentralWidget(self.cw)
        self.l = QtGui.QGridLayout()
        self.cw.setLayout(self.l)


        self.tree = {"Scan": 10, "Spectra":3}
        self.PW = {}
        self.table = TableOfLines(max_lines_N=100)
        #self.table.toolbox.setMinHeight(400)
        self.table.toolbox.setFixedWidth(300)
        self.l.addWidget(self.table,0,0,1,2)
        self.addLineBtn = QtGui.QPushButton('Add')
        self.l.addWidget(self.addLineBtn,1,0,1,1)
        self.cleanBtn = QtGui.QPushButton('Clean')
        self.l.addWidget(self.cleanBtn,1,1,1,1)

        self.cleanBtn.clicked.connect(self.cleanAll)
        self.addLineBtn.clicked.connect(self.addLine)

        for i, name in enumerate(self.tree):
            self.PW[name] = pg.PlotWidget(name=name)  ## giving the plots names allows us to link their axes together
            self.l.addWidget(self.PW[name],0,i+2,1,2)
            self.table.addPage(name, self.PW[name])
            n = 0
            for k in [1,2,2,3]:
                for j in range(3):
                    line = pg.PlotCurveItem(np.arange(100)+n*2, np.random.rand(100)+j+n, pen=j)
                    self.table.addLine(name, f"name{k}", f"{k}{j}", line)
                    n+=2

        self.show()

    def cleanAll(self):
        self.table.removeAll()

    def addLine(self):
        t = time.time()/10
        Class = round(t)
        info = round(t-Class,1)
        port_name = self.table.toolbox.itemText(self.table.toolbox.currentIndex())
        line = pg.PlotCurveItem(np.arange(100), np.random.rand(100)+np.random.rand(1)*10, pen=np.random.randint(0,15))

        self.table.addLine(port_name, f"{Class}", f"{info}", line)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()

    app.exec()
