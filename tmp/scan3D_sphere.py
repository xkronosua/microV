# -*- coding: utf-8 -*-
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl
import numpy as np
import time
from hardware.stages.PI_NanoCube_E727 import generate_random_sphere, E727


piStage = E727()
piStage.connect()
piStage.getAxesNames()
piStage.setServoControl()
# piStage.autoZero()
piStage.setVelocity(values=[10000] * 3)
pos = piStage.getPosition()

app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.opts["distance"] = 20
w.show()
w.setWindowTitle("pyqtgraph example: GLScatterPlotItem")

g = gl.GLGridItem()
w.addItem(g)


##
##  First example is a set of points with pxMode=False
##  These demonstrate the ability to have points with real size down to a very small scale
##
N = 1000
Pos = np.empty((N, 3))
origin = np.array([10, 10, 10])

x, y, z = generate_random_sphere(5, N)
Pos[:, 0] = x
Pos[:, 1] = y
Pos[:, 2] = z
Pos += origin

length = (np.diff(Pos, axis=0) ** 2).sum() ** 0.5
vel = piStage.getVelocity().mean()
total_time = length.sum() / vel


def scan_random(self, pos, radius=1, N=1000, stop_event=None):
    if N == 1:
        Pos = [pos]
    else:
        x, y, z = generate_random_sphere(radius, N)
    Pos = np.array([x, y, z]).T + pos
    all_pos = np.zeros((0, 3))
    for i in range(N):
        self.move([0, 1, 2], Pos[i], wait=True)
        all_pos = np.vstack((all_pos, self.getPosition()))
    return all_pos.mean(axis=0)


size = 1
color = "y"

sp1 = gl.GLScatterPlotItem(pos=Pos, color=(0, 1, 0, 1), size=0.1, pxMode=False)

w.addItem(sp1)


##
##  Third example shows a grid of points with rapidly updating position
##  and pxMode = False
##

pos3 = np.zeros(3)

sp3 = gl.GLScatterPlotItem(pos=pos3, color=(1, 0, 0, 1), size=0.1, pxMode=False)

w.addItem(sp3)

n = 0
piStage.move([0, 1, 2], origin)
pos = piStage.getPosition()
print(pos)
pos_new = origin.copy()
errors = np.zeros(1)
t0 = time.time()


def update():

    ## update surface positions and colors
    global sp3, piStage, n, t, Pos, N, pos_new, errors, t0

    piStage.move([0, 1, 2], Pos[n], wait=True)
    pos = piStage.getPosition()
    pos = piStage.getPosition()
    pos = piStage.getPosition()
    pos_new = np.vstack((pos_new, pos))
    sp3.setData(pos=pos_new)
    err = ((Pos[n] - pos) ** 2).sum() ** 0.5
    print(n, err)
    errors = np.append(errors, err)
    n += 1
    if n >= len(Pos[:, 0]) - 1:
        t.stop()
        print(errors.mean(), time.time() - t0)


# piStage.initGridScanXYZ(Pos[:,0],Pos[:,1],Pos[:,2])
# piStage.startGridScanXYZ()
t = QtCore.QTimer()
t.timeout.connect(update)
t.start(1)


## Start Qt event loop unless running in interactive mode.
if __name__ == "__main__":
    import sys

    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        QtGui.QApplication.instance().exec_()
