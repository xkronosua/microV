import numpy as np
import matplotlib.pyplot as plt
import exdir
import sys

from scipy.integrate import quad
from scipy.interpolate import interp1d, Akima1DInterpolator
from scipy.signal import medfilt
from scipy.optimize import minimize_scalar
import pandas as pd
import time
import traceback
from lmfit import Model
from lmfit.models import GaussianModel, ConstantModel, Model


def sech2Model(x, center, amplitude, fwhm):
    return amplitude / np.cosh(1.7627 * (x - center) / fwhm) ** 2


# from pathlib import Path
# sys.path.append(str(Path('~/work/microV/scripts/')))
# from calibrTools import CalibrToolkit


def cross_correlation(
    x, zero_delay_pos, amplitude_FIX, amplitude_TUN, FWHM_FIX, FWHM_TUN, efficiency
):
    c = 299792458  # m/s
    tau = x * 2e-3 / c * 1e15  # fs
    delay_zero_fs = zero_delay_pos * 2e-3 / c * 1e15
    print(
        delay_zero_fs,
        zero_delay_pos,
        amplitude_FIX,
        amplitude_TUN,
        FWHM_FIX,
        FWHM_TUN,
        efficiency,
    )

    I = lambda t: sech2Model(t, 0, amplitude_TUN, FWHM_TUN)
    Ig = lambda t: sech2Model(t, 0, amplitude_FIX, FWHM_FIX)
    func = lambda t, tau_: I(t) * Ig(t - tau_)

    def C_(tau_):
        integr = quad(func, -10000, 10000, args=(tau_,))[0] * efficiency
        return integr

    C = np.vectorize(C_)
    return C(tau + delay_zero_fs)


def gaussian(x, amp, cen, wid):
    """1-d gaussian: gaussian(x, amp, cen, wid)"""
    return (amp / (np.sqrt(2 * np.pi) * wid)) * np.exp(
        -((x - cen) ** 2) / (2 * wid ** 2)
    )


def unique_(a, return_index=False):
    index = np.arange(a.size)[abs(np.diff(a + 0.001, prepend=a[0] * 2)) > 0]
    if return_index:
        return a[index], index
    else:
        return a[index]


def unique_dummy(array, return_index=False, repeats=True):
    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            if not repeats and v in res:
                continue
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


def calcShiftDelay(T, ang, n):
    theta = np.deg2rad(ang)
    theta_ = np.arcsin(np.sin(theta) / n)
    T_ = T / np.cos(theta_)
    in_out_distance = T_ * np.cos(theta - theta_)
    # print(in_out_distance)
    delay_delta = in_out_distance - T_ * n
    d = T * np.sin(theta) * (1 - np.cos(theta) / np.sqrt(n ** 2 - np.sin(theta) ** 2))
    return np.array([delay_delta, T_, d])


def n_UVFS(wavelength):
    wl = wavelength / 1000  # nm -> um
    n = np.sqrt(
        1
        + 0.6961663 * wl ** 2 / (wl ** 2 - 0.0684043 ** 2)
        + 0.4079426 * wl ** 2 / (wl ** 2 - 0.1162414 ** 2)
        + 0.8974794 * wl ** 2 / (wl ** 2 - 9.896161 ** 2)
    )
    return n


# store = exdir.File('BeamShift_V_Compensation_test.exdir','r')
store = exdir.File(sys.argv[1], "r")
dataType = "scanND"

keys = [k for k in store[dataType] if k[0] != "_"]
sizes = [len(store[dataType][k]["data"]) for k in keys]

dtype = store[dataType][keys[-1]]["data"].dtype

data = np.zeros(np.sum(sizes), dtype=dtype)
last_row = 0
for i, k in enumerate(keys):
    if store[dataType][k]["data"].dtype == dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        data[last_row : last_row + w.sum()] = d[w]
        last_row += w.sum()
        print(i, k)
data = data[data["time"] != 0][1:]

unique_ex_wls, unique_ex_wls_index = unique_dummy(
    data["position"]["LaserWavelength"][::-1], return_index=True, repeats=False
)
groups = np.split(data[::-1], unique_ex_wls_index)[1:]


zero_pos = np.load(
    "D://microV/configStore.exdir/Microscope/DelayLine/ZeroDelayPosition/20210329-151232/data.npy"
)
delay_zero_pos = interp1d(zero_pos["ex_wl"], zero_pos["delay_0_pos"])

cmap = plt.get_cmap("jet")
color_norm = plt.Normalize(-50, 50)

res = []
res_delay = []
frame = {}

# gmodel = Model(gaussian)
bgmodel = ConstantModel()

# gmodel_ = GaussianModel()
# model = gmodel_ + bgmodel
sech2model_ = Model(sech2Model)
model = sech2model_ + bgmodel


for i, d in enumerate(groups):
    if len(d) == 0:
        continue
    # plt.figure()
    # plt.matshow()
    # d=d[::-1]
    unique_pos, unique_index = unique_dummy(
        d["position"]["laserBeamShift_V_angle"], return_index=True, repeats=False
    )
    g = np.split(d, unique_index)[1:]
    tmp = []
    tmp1 = []
    fig, ax = plt.subplots(1, 2)
    for gg in g:
        ang_ = gg["position"]["laserBeamShift_V_angle"].mean()
        ang = ang_ + 1
        # if ang<-20 or ang>=50: continue
        ex_wl = d["position"]["LaserWavelength"].mean()
        zero = float(delay_zero_pos(ex_wl))  # -1.215
        delay_delta, T_, D = calcShiftDelay(5, ang, n_UVFS(ex_wl))
        # print(delay_delta, T_, D)
        delay_delta0, T_, D = calcShiftDelay(5, 0, n_UVFS(1300))
        delay_delta01, T_, D = calcShiftDelay(5, 0, n_UVFS(ex_wl))

        shift = delay_delta - delay_delta01
        # print(ex_wl,shift,delay_delta0-delay_delta01)
        delay_pos = gg["metadata"]["Delay_line_position"] - zero - shift / 2
        delay_pos = gg["position"]["Delay_line_position_zero_relative"]
        delay_pos_abs = gg["metadata"]["Delay_line_position"]
        zeroDelay_real = abs(delay_pos - delay_pos_abs).mean()

        if sys.argv[2] == "HRS":
            ylabel = "w1+w2"
        else:
            if len(delay_pos) < 10:
                continue
            if ex_wl < 850:
                ylabel = "2w2-w1"
            elif ex_wl > 825 and ex_wl < 900:
                ylabel = "All"
                # ylabel='w1+w2'
            else:
                ylabel = "2w2+w1"

        sig = gg["data"]["AndorCamera"]["data"][ylabel]
        if sys.argv[2] == "HRS":

            sig -= np.hstack([sig[:3], sig[-3:]]).mean()
            if ex_wl != 1050:
                Iw1 = gg["data"]["AndorCamera"]["data"]["2w1"]
                Iw2 = gg["data"]["AndorCamera"]["data"]["2w2"]
                # Iw1Iw2 = np.sqrt(Iw1*Iw2+1e-5)
                # sig/=Iw1Iw2

                sig /= Iw1 * 4
            elif ex_wl == 700:
                pass
            else:
                Iw1 = gg["data"]["AndorCamera"]["data"]["2w1"]
                sig /= Iw1[:3].mean() * 4

            # sig-=np.hstack([sig[:3], sig[-3:]]).mean()
            # sig/=d['data']['AndorCamera']['data'][ylabel].mean()
        else:
            sig -= np.hstack([sig[:3], sig[-3:]]).mean()
            # sig/=d['data']['AndorCamera']['data'][ylabel].mean()
        sig = sig[::-1]
        delay_pos = delay_pos[::-1]
        delay_pos_abs = delay_pos_abs[::-1]

        # w = (delay_pos>-0.075)&(delay_pos<0.075)
        # sig = sig[w]
        # delay_pos = delay_pos[w]
        if len(delay_pos) < 3:
            continue
        delay_pos_i = np.linspace(delay_pos.min(), delay_pos.max(), 1000)
        delay_pos_abs_i = np.linspace(delay_pos_abs.min(), delay_pos_abs.max(), 1000)

        color = cmap(color_norm(ang))
        ax[0].plot(delay_pos, sig, ".", color=color, label=f"_{ang_:.0f}deg")

        # try:
        # pars = gmodel_.guess(sig, x=delay_pos)
        pars = sech2model_.make_params()
        pars += bgmodel.guess(sig, x=delay_pos)
        pars["center"].set(value=0, min=-0.06, max=0.06)
        # pars['sigma'].set(value=0.012,min=0.01,max=0.015)
        pars["fwhm"].set(value=0.023, min=0.015, max=0.04)
        pars["amplitude"].set(value=sig.max(), min=0)
        pars["c"].set(value=0, vary=False, min=-0.02, max=0.01)
        result = model.fit(sig, pars, x=delay_pos)
        sig_i = model.eval(result.params, x=delay_pos_i)
        print(
            f"fwhm={result.params['fwhm'].value},\tcenter={result.params['center'].value}"
        )
        # sig_i = Akima1DInterpolator(delay_pos,sig)(delay_pos_i)
        # if (ex_wl>900) or (ex_wl<850):
        tmp.append([ang, sig_i.max(), delay_pos_i[sig_i == sig_i.max()].mean(), zero])
        tmp1.append(
            {
                "ang": ang_,
                "peak": sig_i.max(),
                "delay_pos_rel": result.params["center"].value,
                "delay_pos_abs": result.params["center"].value + zeroDelay_real,
                "shift": shift,
                "zero": zero,
                "ex_wl": ex_wl,
            }
        )
        res_delay += tmp1
        # if ex_wl == 1250 or ex_wl == 750:

        # 	w = np.where(sig==sig.max())[0][0]
        # 	wl=gg['data']['AndorCamera']['raw']['wavelength'][w]
        # 	intens=gg['data']['AndorCamera']['raw']['intensity'][w]
        # 	res_spectral.append({'wavelength':wl, 'intensity':intens,'ex_wl':ex_wl})
        ax[0].plot(delay_pos_i, sig_i, color=color, label=f"{ang_:.0f}deg")
        # except:
        # if abs(ang)<3:
        # 	res_delay.append({'delay_0_pos':gg['metadata']['Delay_line_position'][10],'ex_wl':ex_wl})

        # 	pass
        # print(ang_)

    try:
        X = np.vstack(
            [
                gg["position"]["laserBeamShift_V_angle"]
                for gg in g
                if len(gg) == len(g[0])
            ]
        )
        Y = np.vstack(
            [
                gg["position"]["Delay_line_position_zero_relative"]
                for gg in g
                if len(gg) == len(g[0])
            ]
        )
        if sys.argv[2] == "HRS":
            Z = np.vstack(
                [
                    gg["data"]["AndorCamera"]["data"]["w1+w2"]
                    / gg["data"]["AndorCamera"]["data"]["2w1"]
                    for gg in g
                    if len(gg) == len(g[0])
                ]
            )
        else:
            Z = np.vstack(
                [
                    gg["data"]["AndorCamera"]["data"][ylabel]
                    for gg in g
                    if len(gg) == len(g[0])
                ]
            )
        ax[1].contourf(X, Y, Z, 100)
    except:
        traceback.print_exc()

    if len(tmp) < 4:
        continue
    tmp = np.array(tmp)

    ang_tmp = np.linspace(-50, 60, 1000)
    sig_tmp = interp1d(
        tmp[:, 0],
        tmp[:, 1],
        kind="cubic",
        bounds_error=False,
        fill_value=0,
        assume_sorted=False,
    )(ang_tmp)

    res.append({"ang": ang_tmp[sig_tmp == sig_tmp.max()].mean(), "ex_wl": ex_wl})

    frame[int(ex_wl)] = pd.DataFrame(tmp1)

    ax[0].set_ylabel(f"Intensity {ylabel}")
    ax[0].set_xlabel("Delay line position, mm")
    ax[0].grid()
    ax[0].set_title(f"FIX:1045nm, TUN:{ex_wl:.0f}nm")
    ax[0].set_xlim((-0.1, 0.1))
    ax[0].legend()
    # plt.figure()
    # plt.plot(tmp[:,0],tmp[:,2])
    # eq = np.poly1d(np.polyfit(tmp[:,0],tmp[:,2])


df = pd.DataFrame(res)
df_delay = pd.DataFrame(res_delay)


def predict_shift_delay(ang, ex_wl, ang0=0):
    delay_delta, T_, D = calcShiftDelay(5, ang - ang0, n_UVFS(ex_wl))
    delay_delta0, T_, D = calcShiftDelay(5, ang0, n_UVFS(ex_wl))
    zero = delay_zero_pos(ex_wl)
    shift = delay_delta - delay_delta0
    return zero + shift / 2


def f(x, ex_wl=1300):
    return abs(
        (
            (
                predict_shift_delay(frame[ex_wl].ang, ex_wl, x)
                - frame[ex_wl].delay_pos_abs
            )
            ** 2
        ).sum()
    )


df["ang0"] = [
    minimize_scalar(f, -3, args=wl, method="bounded", bounds=(-20, 20)).x
    for wl in frame.keys()
]


plt.figure()
plt.plot(df.ex_wl, df.ang, ".-", label="ang")
plt.plot(df.ex_wl, df.ang0, "o-", label="ang0")
plt.xlabel("Ex. wavelength, nm")
plt.ylabel("Optimal angle, deg")
plt.grid()
plt.legend()

plt.figure()
plt.plot(df.ex_wl, calcShiftDelay(5, df.ang, n_UVFS(df.ex_wl))[2], ".-", label="ang")
plt.plot(df.ex_wl, calcShiftDelay(5, df.ang0, n_UVFS(df.ex_wl))[2], "o-", label="ang0")
plt.legend()
plt.xlabel("Ex. wavelength, nm")
plt.ylabel("Optimal vertical beam shift, mm")
plt.grid()

plt.figure()
plt.plot(df.ex_wl, calcShiftDelay(5, df.ang, n_UVFS(df.ex_wl))[0], ".-", label="ang")
plt.plot(df.ex_wl, calcShiftDelay(5, df.ang0, n_UVFS(df.ex_wl))[0], "o-", label="ang0")
plt.xlabel("Ex. wavelength, nm")
plt.ylabel("Induced delay, mm")
plt.legend()

plt.grid()


out_data = np.zeros(
    len(df), dtype=[("ex_wl", "f4"), ("shift_V", "f8"), ("shift_H", "f8")]
)
out_data["ex_wl"] = df["ex_wl"]
out_data["shift_V"] = calcShiftDelay(5, df.ang, n_UVFS(df.ex_wl))[2]


with exdir.File("processed.exdir") as store_out:
    if "BeamShift" in store_out:
        del store_out["BeamShift"]

    timestamp = time.strftime("%Y%m%d-%H%M%S")
    store_out.require_group(f"BeamShift")
    store_out[f"BeamShift"].require_dataset(timestamp, data=out_data)

    store_out[f"BeamShift"][timestamp].data[:] = out_data[:]

    if "ZeroDelayPosition" in store_out:
        del store_out["ZeroDelayPosition"]

    timestamp = time.strftime("%Y%m%d-%H%M%S")
    store_out.require_group(f"ZeroDelayPosition")
    store_out[f"ZeroDelayPosition"].require_dataset(
        timestamp, data=df_delay.to_records(index=False)
    )

    # store_out[f'ZeroDelayPosition'][timestamp].data[:] = out_data[:]


plt.show()
