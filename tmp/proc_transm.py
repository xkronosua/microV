import numpy as np
import pandas as pd
import exdir
from matplotlib import pyplot as plt
from scipy.interpolate import LinearNDInterpolator

s_f = exdir.File("filters.exdir", "r")
s_b = exdir.File("bg.exdir", "r")

d_f_ = s_f[max(s_f)]["scan"]
d_b_ = s_b[max(s_b)]["scan"]

for grating in [0, 1]:

    plt.figure(grating)

    d_f = d_f_[(d_f_["shamrockGrating"] == grating) & (d_f_["time"] != 0)]
    d_b = d_b_[(d_b_["shamrockGrating"] == grating) & (d_b_["time"] != 0)]

    d0_f = [d_f[d_f["filtersPiezoStage"] == i] for i in range(4)]
    d0_b = [d_b[d_b["filtersPiezoStage"] == i] for i in range(4)]
    if grating == 1:
        d0_f[0] = d0_f[0][1:]

    d0_f[3] = d0_f[3][:17]
    d0_b[3] = d0_b[3][:17]

    d = d0_f.copy()

    colors = "krgbc"
    for i in range(3, -1, -1):
        d[i]["AndorCamera"]["intensity"] = (
            d0_f[i]["AndorCamera"]["intensity"] - d0_b[i]["AndorCamera"]["intensity"]
        )
        d[i]["AndorCamera"]["intensity"] /= d[0]["AndorCamera"]["intensity"]
    names = ["FGB37x2", "FESH0700", "84-712"]

    res = {}
    for i in range(1, 4):
        y = []
        x = d[i]["AndorCamera"]["wavelength"]
        y = np.tile(d[i]["shamrockWavelength"], (x.shape[1], 1)).T
        iu = LinearNDInterpolator(
            list(zip(x.flatten(), y.flatten())),
            d[i]["AndorCamera"]["intensity"].flatten(),
        )
        res[names[i - 1]] = iu
        for j in range(len(d[i]["AndorCamera"]["wavelength"])):
            plt.plot(
                d[i]["AndorCamera"]["wavelength"][j],
                d[i]["AndorCamera"]["intensity"][j],
                ".",
            )  # +colors[i])
            plt.plot(
                d[i]["AndorCamera"]["wavelength"][j],
                iu(d[i]["AndorCamera"]["wavelength"][j], d[i]["shamrockWavelength"][j]),
                "--" + colors[i],
            )

    r = np.core.records.fromrecords([list(res.values())], names=list(res.keys()))
    np.save("filtersTransmInterp_grating%d.npy" % grating, r)

plt.show()
