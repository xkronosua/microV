import numpy as np
from noisyopt import minimizeCompass
import matplotlib.pyplot as plt

import numpy as np
from scipy.stats import multivariate_normal
from scipy.interpolate import LinearNDInterpolator
import time

x, y = np.mgrid[-1.0:1.0:300j, -1.0:1.0:300j]
# Need an (N, 2) array of (x, y) pairs.
xy = np.column_stack([x.flat, y.flat])

mu = np.array([0.0, 0.1])

sigma = np.array([0.2, 0.09])
covariance = np.diag(sigma ** 2)

z = multivariate_normal.pdf(xy, mean=mu, cov=covariance)

# Reshape back to a (30, 30) grid.
z = z.reshape(x.shape)

z *= np.random.rand(*z.shape) ** 3 + np.sin(x) * 2 + np.cos(y)

interp = LinearNDInterpolator(list(zip(x.flatten(), y.flatten())), z.flatten())


def obj(x, r=[]):
    time.sleep(0.1)
    r.append(x)
    print(len(r) - 1, x)
    return 1 / interp(x[0], x[1])


r = []
pos = [0.35, 0.12]
radiuses = [0.5, 0.3]
bounds = [[p - r, p + r] for p, r in zip(pos, radiuses)]
res = minimizeCompass(
    obj, args=[r], x0=pos, bounds=bounds, deltatol=0.01, paired=False, funcNinit=5
)

plt.contourf(x, y, z)
r = np.array(r).T
plt.plot(r[0], r[1], "-", alpha=0.5, marker=None)
plt.plot(r[0][0], r[1][0], "go")
plt.plot(r[0][0], r[1][0], "gx")

plt.scatter(r[0], r[1], c=np.arange(len(r.T)) / len(r.T), cmap="jet", s=3)

plt.show()
