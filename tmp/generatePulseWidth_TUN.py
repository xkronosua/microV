import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback

import time
import numpy_indexed as npi
import argparse
import pandas as pd
from lmfit.models import GaussianModel, ConstantModel, ExponentialModel
from pathlib import Path

from scipy.integrate import quad, quad_vec
from lmfit import Model
from lmfit.models import QuadraticModel
from scipy.signal import argrelextrema, medfilt, butter, filtfilt, find_peaks
from scipy.interpolate import interp1d, splrep, splev, BSpline
from tqdm import tqdm
import itertools
import traceback


sech = lambda x: 1 / np.cosh(x)


def I(t, t_p):
    return sech(1.7627 * t / t_p) ** 2


def func(t, tau, t_p1=100, t_p2=100):
    return I(t - tau, t_p1) * I(t, t_p2)


almost_inf = 2000


def IAC(x, t0, A0, A, t_p1=100, t_p2=100):
    t = x - t0
    # print(t)
    f = lambda x: func(x, t, t_p1, t_p2)
    f_norm = lambda x: func(x, 0, t_p1, t_p2)

    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    # r = np.nan_to_num(r,0, neginf=0, posinf=0)
    res = A0 + A * r
    # print(t0, A0, A, t_p1, t_p2)
    return res


parser = argparse.ArgumentParser(
    description="Process data for multiscan measurements.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument("-f", dest="file", help="Exdir to process")

parser.add_argument(
    "-t",
    dest="timestamps",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in "MultiScan" folder.
					Integer index to process by index in sorted list of timestamps.
					Last measured "-1" by default.
					""",
    default=["-1"],
)

parser.add_argument(
    "--FIX",
    dest="FIX_pulse_width",
    type=float,
    help="Pulse duration for FIX output in fs",
    default=154,
)

parser.add_argument(
    "-s",
    dest="setup",
    help="Experimental setup. [Microscope, HRS]",
    default="Microscope",
)

parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)

parser.add_argument(
    "-D",
    dest="datasheet",
    help='Path to "data.npy" binary in "configStore.exdir" with datasheet for corresponding setup',
    default=None,
)
parser.add_argument(
    "-c",
    dest="CenterIndex",
    help="Index of nanoparticle for Microscope setup",
    default=0,
)
parser.add_argument(
    "--smooth",
    dest="smooth",
    type=int,
    help="Smooth factor. If defined data will be smoothed by b-spline and extrapolated if needed. Optimal value: 2",
    default=None,
)

args = parser.parse_args()
print(args)

if args.setup == "Microscope":
    dataType = "MultiScan"
else:
    dataType = "scanND"



df = pd.read_csv(args.file)

for band in ['3w1','3w2']:
    if band in df:
        df.loc[df['FILTER']=='FESH0700',band] = np.nan
        df.loc[df['FILTER']=='FESH0850',band] = np.nan

# for band in ['3w2', '2w2', '2w1', '3w1', '2w2+w1', '2w1+w2','w1+w2']:
#     if not band in df:
#         continue
#     df[band][abs(df['Delay_line_position_zero_relative'])>0.5] = np.nan
if "LaserShutter" in df:
    df.loc[df['LaserShutter']!=3] = np.nan



df.loc[(df['FILTER']=='FGB37x2_EO84-712') & (df['LaserWavelength']>=1200), '2w2'] = np.nan
PL_bands = {}
for k in df.keys():
    if k[:2].split('_')[0]=="PL":
        PL_bands[k.split('_')[0]] = int(k.split('_')[0][2:])


for k in PL_bands:
    df.loc[(df['FILTER']=='FESH0850'), k] = np.nan
    df.loc[(df['FILTER']=='FESH0700'), k] = np.nan

nonBG_mask = df.CenterType!='BG'
index2type = dict(zip(df.CenterIndex,df.CenterType))
group_by = []
for axis in ['LaserWavelength','filtersPiezoStage','LaserShutter','Delay_line_position_zero_relative','CenterIndex']:
    if axis in df:
        group_by.append(axis)
df1 = df[nonBG_mask].groupby(group_by, as_index=False).mean()
df1['CenterType'] = [index2type[index] for index in df1.CenterIndex]

for i, data_row in df[df['CenterType']=='BG'].groupby(group_by,as_index=False).mean().iterrows():
    pos = {ax:data_row[ax] for ax in group_by}
    del pos['CenterIndex']
    for band in ['3w2', '2w2', '2w1', '3w1', '2w2+w1', '2w1+w2','w1+w2']+list(PL_bands):
        if not band in df1: continue
        mask = 1
        for ax in pos:
            mask = mask & (df1[ax]==pos[ax])
        df1.loc[mask,band]-=data_row[band]



res = []

ac_mod = Model(IAC)
params = ac_mod.make_params()
params["t_p1"].set(args.FIX_pulse_width, min=100, max=250, vary=False)
params["t_p2"].set(190, min=70, max=400, vary=True)
params["t0"].set(0, min=-200, max=200, vary=True)
params["A"].set(1, min=0.8, max=2, vary=True)
params["A0"].set(0.00, min=-0.1, max=0.6, vary=False)
mod = ac_mod

for i,g in df1.groupby(['LaserWavelength','CenterIndex'], as_index=False):
    ex_wl = g.LaserWavelength.mean()
    if g.CenterIndex.mean()!=0: continue
    print(g[group_by].mean())
    delay = (
            g["Delay_line_position_zero_relative"]
            * 2
            / 299792458
            * 1e12
        )
    sig = g["w1+w2"]
    fig, axes = plt.subplots(1, 1)
    mask = np.isnan(sig)
    t = delay[~mask].astype(float).values
    sig = sig[~mask].astype(float).values

    #y = (sig -sig.min()) / np.ptp(sig)
    y = sig/sig.max()

    #y = y[t.argsort()]
    #t = t[t.argsort()]
    # if y[abs(t)<20].min()<y.mean():
    #     y = y[abs(t)>20]
    #     t = t[abs(t)>20]
    #if len(t) < 10:
    #    continue

    #if abs(t[y == y.max()]) > 100:
    #    continue
    # if t.min() > 500 or t.max() < 500:
    #     continue

    axes.plot(t, y, "o")
    result = mod.fit(y, params, x=t)
    print(ex_wl)
    print(result.fit_report(min_correl=1))

    xi = np.linspace(t.min(),t.max(),1000)
    #axes.plot(t, result.best_fit, "-r")
    axes.plot(xi, mod.eval(x=xi,params=result.params), "-r")
    #axes.set_title(f"{timestamp}: @{ex_wl}")
    res.append(
        {
            "Ex_wl": ex_wl,
            "pulsewidth": result.params["t_p2"].value,
            #"timestamp": timestamp,
            "chisqr": result.chisqr,
        }
    )


def weighted_average(df, data_col, weight_col, by_col):
    """Now data_col can be a list of variables"""
    df_data = df[data_col].multiply(df[weight_col], axis="index")
    df_weight = pd.notnull(df[data_col]).multiply(df[weight_col], axis="index")
    df_data[by_col] = df[by_col]
    df_weight[by_col] = df[by_col]
    result = df_data.groupby(by_col).sum() / df_weight.groupby(by_col).sum()
    return result


df = pd.DataFrame(res)
df = df[df.pulsewidth < 290]
df = df[df.pulsewidth > 80]
df = df[abs(df["Ex_wl"]-1045)>10]

df.sort_values(by="Ex_wl", inplace=True)
plt.figure()
df1 = df.groupby("Ex_wl", as_index=False).mean()
df1["pulsewidth"] = weighted_average(df, "pulsewidth", "chisqr", "Ex_wl").values

plt.plot(df.Ex_wl, df.pulsewidth, "o-",label='init')
plt.plot(df1.Ex_wl, df1.pulsewidth, "o-",label='groupByEx_wl_mean')
#for i, d in df.iterrows():
#    plt.text(d.Ex_wl, d.pulsewidth, d.timestamp, rotation=45)

if args.datasheet is None:
    pass
else:
    datasheet_data = np.load(args.datasheet)
    datasheet = interp1d(
        datasheet_data["Ex_wl"][1:-1],
        datasheet_data["pulsewidth"][1:-1] * 1e15,
        bounds_error=False,
        fill_value="extrapolate",
    )
    plt.plot(
        datasheet_data["Ex_wl"][1:-1], datasheet_data["pulsewidth"][1:-1] * 1e15, "-", label='datasheet'
    )

# df1['pulsewidth_correction'] = df1.pulsewidth/datasheet(df1.Ex_wl)
# df1.to_csv('pulsewidth_correction.csv', index=False)

df2 = df1[["Ex_wl", "pulsewidth"]]

if not args.smooth is None:
    # eq = np.poly1d(np.polyfit(df['LaserWavelength'], df['sigma'], 3, w=weights))
    t, c, k = splrep(
        df2["Ex_wl"],
        df2["pulsewidth"],
        s=args.smooth*1000,
        k=3,
    )  # w=weights)
    eq = BSpline(t, c, k, extrapolate=True)
    df2["pulsewidth"] = eq(df2["Ex_wl"])
    plt.plot(
        df2["Ex_wl"], df2["pulsewidth"], "o-", label="smoothed"
    )
df2["pulsewidth"] *= 1e-15
out_data = df2.to_records(index=False)



with exdir.File(args.output) as store_out:

    timestamp = time.strftime("%Y%m%d-%H%M%S")
    path = args.setup + "/Laser/pulsewidth/"
    store_out.require_group(path)
    store_out[path].require_dataset(timestamp, data=out_data)
    store_out[path][timestamp].attrs["pulsewidth_IR"] = args.FIX_pulse_width * 1e-15


plt.show()
