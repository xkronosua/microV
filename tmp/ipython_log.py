# IPython log file

345/3
1045/3
1045/3 -348.9572857500329
1045/3 - 348.9572857500329
1045/3 - 348.9572857500329
373.3*3
1064/1238
1064/1240
1238/1064
1240/1064
1240/373.3/3
1240/373.3
def E_3w2(ex_wl): return 1240/ex_wl*3
ex_wl = linspace(700,1300,1000)
plot(ex_wl,E_3w2(ex_wl))
plot([700,1300],1240/373.3)
plot([700,1300],[1240/373.3]*2)
plot([700,1300],[1240/373.3]*2,'--')
plot([700,1300],[1240/373.3]*2,'--')
plot(ex_wl,E_3w2(ex_wl))
get_ipython().run_line_magic('pwd', '')
get_ipython().run_line_magic('cd', '../data')
get_ipython().run_line_magic('ls', '')
get_ipython().run_line_magic('logstart', '')
def E_3w1(ex_wl): return 1240/1045*3
def E_2w2(ex_wl): return 1240/ex_wl*2
get_ipython().run_line_magic('colors', 'Linux')
def E_3w1(ex_wl): return np.array([1240/1045*3]*len(ex_wl))
def E_w1pw2(ex_wl): return 1240/(1/ex_wl+1/1045)*2
def E_2w1pw2(ex_wl): return 1240/ex_wl + 1240*2/1045
def E_w1pw2(ex_wl): return 1240/ex_wl+1240/1045
def E_2w2pw1(ex_wl): return 1240*2/ex_wl + 1240/1045
plot(ex_wl,E_3w2(ex_wl),label='3w2')
plot(ex_wl,E_3w1(ex_wl),label='3w1')
plot(ex_wl,E_w1pw2(ex_wl),label='w1+w2')
plot([700,1300],[1240/373.3]*2,'--',label='E_g')
plot(ex_wl,E_2w1pw2(ex_wl),label='2w1+w2')
plot(ex_wl,E_2w2pw1(ex_wl),label='2w2+w1')
legend()
grid(1)
plot(ex_wl,E_2w2(ex_wl),label='2w2')
legend()
def E_2w1(ex_wl): return np.array([1240/1045*2]*len(ex_wl))
plot(ex_wl,E_2w1(ex_wl),label='2w1')
legend()
plot([700,1300],[1240/373.3]*2,'--',label='E_g')
plot(ex_wl,E_2w1(ex_wl),label='2w1')
plot(ex_wl,E_2w2(ex_wl),label='2w2')
plot(ex_wl,E_3w1(ex_wl),label='3w1')
plot(ex_wl,E_3w2(ex_wl),label='3w2')
plot(ex_wl,E_2w1pw2(ex_wl),label='2w1+w2')
plot(ex_wl,E_2w2pw1(ex_wl),label='2w2+w1')
plot(ex_wl,E_w1pw2(ex_wl),label='w1+w2')
grid(1)
legend()
10**(1/3)
0.2**0.5
get_ipython().run_line_magic('hist', '')
get_ipython().run_line_magic('paste', '')
get_ipython().run_line_magic('hist', '')
get_ipython().run_line_magic('paste', '')
get_ipython().run_line_magic('paste', '')
plot(ex_wl,E_2w1(ex_wl),label='2w1')
plot(ex_wl,E_2w2(ex_wl),label='2w2')
plot(ex_wl,E_3w1(ex_wl),label='3w1')
plot(ex_wl,E_3w2(ex_wl),label='3w2')
plot(ex_wl,E_2w1pw2(ex_wl),label='2w1+w2')
plot(ex_wl,E_2w2pw1(ex_wl),label='2w2+w1')
plot(ex_wl,E_w1pw2(ex_wl),label='w1+w2')
grid(1)
legend()
get_ipython().run_line_magic('hist', '')
plot([700,1300],[1240/373.3]*2,'--',label='E_g')
