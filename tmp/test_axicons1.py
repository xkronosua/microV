from matplotlib import pyplot as plt
import numpy as np
from raytracing import *
import pandas as pd
from scipy.special import erf
from axicon import Axicon

# Demo #8: Virtual image
path = ImagingPath()
path.fanNumber = 10
path.fanAngle = 10
path.objectHeight = 2.8 * 2
WAVELENGTH = 1.045
WAVELENGTH = 1.300
# WAVELENGTH = 0.7
Ax0 = Axicon(alpha=-np.deg2rad(2), n=materials.FusedSilica.n(WAVELENGTH), diameter=25.4)
Ax1 = Axicon(alpha=np.deg2rad(2), n=materials.FusedSilica.n(WAVELENGTH), diameter=25.4)


path.append(Space(d=800 + 600))
Zoom = 4
f_L3 = 120
f_L4 = f_L3 / Zoom


path.append(
    DielectricSlab(n=materials.FusedSilica.n(WAVELENGTH), thickness=5, diameter=25.4)
)
path.append(Ax0)
path.append(Space(d=120))
# path.append(Lens(f=f_L3,diameter=25.4))
# path.append(Space(d=f_L4+f_L3))
# path.append(Lens(f=f_L4,diameter=25.4))
# path.append(Space(d=100))

path.append(Ax1)
path.append(
    DielectricSlab(n=materials.FusedSilica.n(WAVELENGTH), thickness=5, diameter=25.4)
)
# path.append(Space(d=150))

# path.append(Lens(f=1200,diameter=25.4))
path.append(Space(d=200))


# obj = Objective(f=5, NA=0.5, focusToFocusLength=30, backAperture=5.1, workingDistance=7.8, label="LMM-40X-UVV")
# path.append(obj)
# path.append(Space(d=0))
# path.append(DielectricSlab(n=materials.FusedSilica.n(WAVELENGTH),thickness=0.17,diameter=10))
# #path.append(Space(d=50))
#
# #obj1 = Objective(f=4.5, NA=0.75, focusToFocusLength=-45.06, backAperture=6.8, workingDistance=0.51, label="RMS40X-PF")
# #path.append(obj1)
# path.append(Space(d=4.35))
# path.append(Lens(f=4.5, diameter=6.8))
#
# path.append(Space(d=1000))


# # define a list of rays with uniform distribution
# inputRays = UniformRays(yMin = minHeight,
# 				yMax = maxHeight,
# 				maxCount = nRays,
# 				thetaMax = maxTheta,
# 				thetaMin = minTheta)
#
divergence = 1.1e-3  # rad


def GaussBeam(N, w, divergence, wavelength, isBlocked=False, y0=0, z=0):
    rays = []
    for i in range(3):
        Y = np.random.normal(scale=w[i] / 2, size=N // 3)
        div = np.random.normal(
            scale=divergence[i] / 2 / 2, size=N // 2
        )  # np.random.uniform(-divergence/2,divergence/2,N)
        wl = wavelength[i]

        for j, y in enumerate(Y):
            ray = Ray(
                y=y + y0,
                theta=div[j],
                z=z,
                isBlocked=isBlocked,
                wavelength=wl,
            )
            rays.append(ray)
    # for i,y in enumerate(Y):
    # 	ray = Ray(
    # 	y=y0+y,
    # 	theta=div[i],
    # 	z=z,
    # 	isBlocked=isBlocked,
    # 	wavelength=wavelength,
    # 	)
    # 	rays.append(ray)
    return Rays(rays)


inputRays = GaussBeam(240, [1.5, 1.2, 1], [1.1e-3, 1.1e-3, 1.1e-3], [0.7, 1.045, 1.3])
path.display(rays=inputRays)

# inputRays = GaussBeam(10000,2.8,1.5e-3,WAVELENGTH)

r = path.traceManyThroughInParallel(inputRays)
r.display()
