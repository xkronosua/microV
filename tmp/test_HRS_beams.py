from matplotlib import pyplot as plt
import numpy as np
from raytracing import *
import pandas as pd

beams = []

k = 2
df_FIX = pd.DataFrame([{"ex_wl": 1045, "W0": 1.39}])
beams += [
    GaussianBeam(
        w=df_FIX.iloc[0]["W0"] * 1.18 / 0.85, wavelength=df_FIX.iloc[0]["ex_wl"] * 1e-6
    ),
    GaussianBeam(
        w=df_FIX.iloc[0]["W0"] * 1.18 / 0.85 * k,
        wavelength=df_FIX.iloc[0]["ex_wl"] * 1e-6,
    ),
]

# beams+=[
# 	GaussianBeam(w=df_FIX.iloc[0]['W0']*1.18*0.85*k*r,wavelength=df_FIX.iloc[0]['ex_wl']*1e-6) for r in np.arange(0.01,1,0.1)
# ]


df_TUN = pd.DataFrame(
    [
        {"ex_wl": 700, "W0": 1.45},
        {"ex_wl": 800, "W0": 1.4},
        {"ex_wl": 1200, "W0": 1.3},
        {"ex_wl": 1300, "W0": 1.25},
    ]
)

beams_TUN = [
    GaussianBeam(
        w=df_TUN.iloc[i]["W0"] * 1.18 / 0.85 * k,
        wavelength=df_TUN.iloc[0]["ex_wl"] * 1e-6,
    )
    for i in range(len(df_TUN))
]

beams += beams_TUN

# focalLength1 = 30
# lensDiameter1 = 12
# L1 = Lens(f=focalLength1,diameter=lensDiameter1)
L1 = thorlabs.AchromatDoubletLens(
    fa=30,
    fb=25.83,
    R1=17.77,
    R2=-16.46,
    R3=-136.80,
    tc1=4.50,
    tc2=2.50,
    te=5.82,
    diameter=12,
    # n1=None,
    # n2=None,
    mat1=materials.N_LAK22,
    mat2=materials.N_SF6HT,
    wavelengthRef=0.8,
    url=None,
    label="L1",
    wavelength=1.045,
)

delta = -8

Z_position = 15 - 14
Z_position += delta
lightpath = 10
wallThickness = 2
# lightpath = 2
# wallThickness = 5


path = LaserPath()
path.append(Space(d=10))
path.append(L1)
path.append(Space(d=L1.fb - lightpath / 2 - wallThickness + Z_position))
# path.append(DielectricSlab(n=1.449, thickness=wallThickness/2))
# path.append(DielectricSlab(n=1.449, thickness=wallThickness/2))
# path.append(DielectricSlab(n=1.3534, thickness=lightpath/4))
# path.append(DielectricSlab(n=1.3534, thickness=lightpath/4))
# path.append(DielectricSlab(n=1.3534, thickness=lightpath/4))
# path.append(DielectricSlab(n=1.3534, thickness=lightpath/4))
# path.append(DielectricSlab(n=1.449, thickness=wallThickness/2))
# path.append(DielectricSlab(n=1.449, thickness=wallThickness/2))
path.append(DielectricSlab(n=1.449, thickness=wallThickness, diameter=12))
path.append(DielectricSlab(n=1.3534, thickness=lightpath, diameter=12))
path.append(DielectricSlab(n=1.449, thickness=wallThickness, diameter=12))


path.append(Space(d=20 - lightpath / 2 - wallThickness - Z_position))


path.display(beams=beams)
