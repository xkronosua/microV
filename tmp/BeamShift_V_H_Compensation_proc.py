from pylab import *
import sys
import os
from glob import glob
import pandas as pd
from lmfit.models import GaussianModel, ConstantModel, VoigtModel, Model
from scipy.special import erf
from scipy.signal import medfilt
from scipy.interpolate import interp1d
import exdir
from matplotlib.patches import Ellipse, Rectangle
import numpy.lib.recfunctions as rfn


def unique_dummy(array, return_index=False):
    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


path = sys.argv[1]

s = exdir.File(path, "r")

timestamp = max(s["scanND"])
data = s["scanND"][timestamp]["data"]
data = data[data["time"] != 0]

unique_ex_wls, unique_ex_wls_index = unique_dummy(
    data["position"]["LaserWavelength"], return_index=True
)

groups = np.split(data, unique_ex_wls_index)[1:]
ex_wls = np.unique(data["position"]["LaserWavelength"])

result = []

for i, d in enumerate(groups):

    ex_wl = ex_wls[i]
    print(i, ex_wl)
    # axes[i].set_title(f'Knife-edge scan. Ex:{ex_wl}nm')

    x = d["position"]["laserBeamShift_H"]
    y = d["position"]["laserBeamShift_V"]
    z = d["position"]["Delay_line_position_zero_relative"]
    sig = d["data"]["AndorCamera"]["data"]["w1+w2"]
    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")
    ax.set_title(f"{ex_wl}")
    ax.scatter(x, y, z, c=sig, s=sig / sig.max() * 100)
    ax.set_xlabel("H")
    ax.set_ylabel("V")
    ax.set_zlabel("delay")

plt.show()
