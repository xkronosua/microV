import numpy as np
import exdir
import sys
import os
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter, medfilt, detrend
import time
import numpy_indexed as npi
import argparse
import pandas as pd
from lmfit.models import GaussianModel, PseudoVoigtModel, LorentzianModel, ExponentialGaussianModel, ConstantModel, ExponentialModel, ExpressionModel
from lmfit import Model

from pathlib import Path
from tqdm import tqdm
from matplotlib.backends.backend_pdf import PdfPages
from shutil import copyfile
from multiprocessing import Pool

parser = argparse.ArgumentParser(description="General data fitting row by row.")
parser.add_argument("-f", dest="file", help="Exdir to process")

parser.add_argument(
    "-D",
    dest="dataType",
    type=str,
    help="""Type of measurements [scanND, MultiScan]""",
    default="MultiScan",
)

parser.add_argument(
    "-t",
    dest="timestamps",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in folder.
					Integer index to process by index in sorted list of timestamps.
					Last measured "-1" by default.
					""",
    default=["-1"],
)

parser.add_argument(
    "--range",
    dest="range",
    nargs="+",
    type=int,
    help='''
            Range of rows in binary dataset for processing:
                --range end
                --range start end
                --range start end step
            ''',
    default=None,
)

parser.add_argument(
    "--bands",
    dest="bands",
    nargs="+",
    type=str,
    help="""
					Bands to fit ['2w1', '2w2', '3w1', '3w2', 'w1+w2', '2w2-w1', '2w1+w2', '2w2+w1'].
					""",
    default=["ALL"],
)

parser.add_argument(
    "--bands_PL",
    dest="bands_PL",
    type=str,
    help="""
					Bands to fit PL:
                        --bands_PL center0,sigma0,gamma0;center1,sigma1,gamma1 ...
					""",
    default=None,
)

parser.add_argument(
    "--sync", dest="beams_sync", help="Measurements with delay.", action="store_true"
)

parser.add_argument(
    "--sensitivity", dest="sensitivity", help="Path to csv file with sensitivity curve.",
    default="AndorCameraCalibr_Microscope_forward_grating1.csv"
)

parser.add_argument(
    "--metadata", dest="get_metadata", help="Extract all metadata to final dataset.", action="store_true"
)

parser.add_argument(
    "--delay", dest="delay_positions", help="Process data only for delay specific positions.", type=float, nargs="+",
    default=[]
)


parser.add_argument(
    "--show", dest="show_plot", help="Show all graphs.", action="store_true"
)

parser.add_argument(
    "--indexing",
    dest="indexing",
    help="Defalt indexing of laser outputs [for processing data older than ~Sep1012 should be \"0213\"]. 0 - both closed, 1 - FIX, 2 - TUN, 3 - both opened",
    default="0123",
)

parser.add_argument(
    "-O",
    dest="output",
    help="Path for output files",
    default="processed",
)


args = parser.parse_args()
print(args)

LaserShutterIndexes = {key:int(index) for key,index in zip(["CLOSED","FIX", "TUN", "OPENED"], args.indexing)}

def none2nan(val):
    if val is None:
        return np.nan
    else:
        return val
def extractTimestamp(store, timestamps, dataType):
    timestamps_ = []
    keys = list(store[dataType])
    keys.sort()
    print(timestamps)

    if "ALL" in timestamps:
        timestamps_ = [k for k in store[dataType] if k[0] != "_"]
    else:
        for ts in timestamps:
            try:
                timestamps_.append(keys[int(ts)])
            except:
                traceback.print_exc()
                timestamps_.append(ts)
    print(timestamps_)
    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    dtype = store[dataType][timestamps_[-1]]["data"].dtype

    data = np.zeros(np.sum(sizes), dtype=dtype)
    last_row = 0
    data_dict = {}

    for i, k in enumerate(timestamps_):
        # if store[dataType][k]['data'].dtype==dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        if w.sum() > 0:
            data_dict[k] = {}
            data_dict[k]['data'] = d
            data_dict[k]['attrs'] = store[dataType][k]["data"].attrs.to_dict()
    # data = data[data['time']!=0]

    return data_dict


def signaltonoise_dB(a, axis=0, ddof=0):
    a = np.asanyarray(a)
    m = a.mean(axis)
    sd = a.std(axis=axis, ddof=ddof)
    return 20*np.log10(abs(np.where(sd == 0, 0, m/sd)))


def bands_generator( bands="ALL"):

    bands_ = lambda ex_wl: {
        "2w1": 1045 / 2,
        "2w2": ex_wl / 2,
        "3w1": 1045 / 3,
        "3w2": ex_wl / 3,
        "w1+w2": 1 / (1 / 1045 + 1 / ex_wl),
        # '2w1-w2': 1/(2/1045-1/ex_wl),
        "2w2-w1": 1 / (-1 / 1045 + 2 / ex_wl),
        "2w1+w2": 1 / (2 / 1045 + 1 / ex_wl),
        "2w2+w1": 1 / (1 / 1045 + 2 / ex_wl),
    }
    if bands == "ALL" or bands == ["ALL"]:
        return bands_
    else:
        return lambda ex_wl: {key:bands_(ex_wl)[key] for key in bands}

def PL_bands_generator(input_str):
    if input_str is None: return {}
    if len(input_str)==0: return {}
    bands = {}
    list_str = input_str.split(";")
    for params in list_str:
        tmp = tuple([float(val.replace(" ","")) for val in params.split(',')])
        bands[f'PL{int(tmp[0])}'] = tmp
    return bands


def bg_model(x, BG, BGA1, BGA2, BGX01, BGX02, BGDECAY1, BGDECAY2):
    return BG + BGA1 * np.exp(-(x-BGX01) / BGDECAY1) + BGA2 * np.exp((x-BGX02) / BGDECAY2)

def fit_data(
        data_row,
        bands_generator, sensitivity_corrector, filters_translator,
        PL_bands=None, center_info=None, beams_sync=True, get_metadata=False, plot=False):

    if data_row["time"]==0: return
    if "type" in data_row["data"]["AndorCamera"]["raw"].dtype.names:
        Type = data_row["data"]["AndorCamera"]["raw"]["type"]
        if Type == -1: return # baseline
    exposure = data_row["data"]["AndorCamera"]["raw"]["exposure"]
    intensity = data_row["data"]["AndorCamera"]["raw"]["intensity"].astype(float)

    wavelength = data_row["data"]["AndorCamera"]["raw"]["wavelength"].astype(float)




    if "filtersPiezoStage" in data_row["position"].dtype.names:
        filterIndex = data_row["position"]["filtersPiezoStage"]
    elif "filtersPiezoStage" in data_row["metadata"].dtype.names:
        filterIndex = data_row["metadata"]["filtersPiezoStage"]

    filterLabel = filters_translator[filterIndex]
    intensity_corrected = intensity/sensitivity_corrector[filterLabel](wavelength)/exposure
    intensity_corrected[intensity_corrected<-1e-4] = np.nan

    if "LaserWavelength" in data_row["position"].dtype.names:
        ex_wl = data_row["position"]["LaserWavelength"]
    elif "LaserWavelength" in data_row["metadata"].dtype.names:
        ex_wl = data_row["metadata"]["LaserWavelength"]

    if "LaserShutter" in data_row["position"].dtype.names:
        shutter = data_row["position"]["LaserShutter"]
    elif "LaserShutter" in data_row["metadata"].dtype.names:
        shutter = data_row["metadata"]["LaserShutter"]

    if "Delay_line_position_zero_relative" in data_row["position"].dtype.names:
        delay = data_row["position"]["Delay_line_position_zero_relative"]
    elif "Delay_line_position_zero_relative" in data_row["metadata"].dtype.names:
        delay = data_row["metadata"]["Delay_line_position_zero_relative"]

    CenterIndex = None
    if "CenterIndex" in data_row['position'].dtype.names:
        CenterIndex = data_row['position']['CenterIndex']
    elif "CenterIndex" in data_row['metadata'].dtype.names:
        CenterIndex = data_row['metadata']['CenterIndex']
    if CenterIndex is None:
        CenterIndex = -1 # ?
        CenterType = "BG"
    else:
        CenterType = center_info['Type'][CenterIndex].decode()

    #print("\n")
    if filterLabel == "FESH0850":
        wavelength[wavelength<440] = np.nan
        wavelength[wavelength>850] = np.nan
    if filterLabel == "FESH0700":
        wavelength[wavelength<390] = np.nan
        wavelength[wavelength>700] = np.nan
    #print(f'{filterLabel=}')
    true_data_mask = (~np.isnan(intensity_corrected)) & (~np.isnan(wavelength))


    y = medfilt(intensity_corrected[true_data_mask], 5)
    #y = intensity_corrected[true_data_mask]



    x = wavelength[true_data_mask]

    weights = np.ones(intensity.shape)
    # if filterIndex == 1:
    #     weights[~((wavelength>330) & (wavelength<640))] = 0.1
    weights[exposure<0.03]=1e-10
    for i in range(1,10):
       weights[intensity > (i*55000)] *= 0.5


    weights = weights[true_data_mask]

    nan_vals = ~(np.isnan(y) | np.isnan(x) | (y==0) | (x==0))
    x = x[nan_vals][3:-3]
    y = y[nan_vals][3:-3]
    weights = weights[nan_vals][3:-3]
    y = y[x.argsort()]
    weights = weights[x.argsort()]
    x = x[x.argsort()]



    if len(x) <= 151:
        return

    bands = bands_generator(ex_wl)


    centers = []
    for band in bands:
        centers.append( bands[band])
    centers = np.array(centers)


    #if filterIndex == 1:
    #    y = detrend(y,bp=[abs(x-320).argmin(),abs(x-640).argmin(),])


    interpolated_sig_init = interp1d(x, y, bounds_error=False, fill_value=0)

    # if ex_wl>900:
    #     clean_minimum = medfilt(y,151).min()
    # else:
    #     clean_minimum = y.min()
    clean_minimum = 0
    if CenterType == "BG":
        intens_ptp = abs(np.ptp(y))
    elif shutter == LaserShutterIndexes["OPENED"] and ex_wl>=710:
        intens_ptp = abs(interpolated_sig_init(bands['2w1'])-clean_minimum)
    elif shutter == LaserShutterIndexes["OPENED"]:
        if abs(delay) < 0.05:
            intens_ptp = abs(interpolated_sig_init(bands['w1+w2'])/2-clean_minimum)
        else:
            intens_ptp = abs((interpolated_sig_init([bands['2w2'],bands['2w1']])).mean()/2-clean_minimum)
    elif shutter == LaserShutterIndexes["FIX"]:
        intens_ptp = abs(interpolated_sig_init(bands['2w1'])-clean_minimum)
    elif shutter == LaserShutterIndexes["TUN"]:
        intens_ptp = abs(interpolated_sig_init(bands['2w2'])-clean_minimum)
    else:
        intens_ptp = y.max()-clean_minimum
    #y = (y-clean_minimum)/np.ptp(y)

    y = y/intens_ptp

    interpolated_sig = interp1d(x, y, bounds_error=False, fill_value=0)

    #weights[y>5]*=0.01
    weights[y<3]*=1.5


    title = ""
    for name in data_row['position'].dtype.names:
        pos = data_row['position'][name].round(3)
        title+=f"{name}={pos:G},"
        if len(title)>150:
            title+="\n"
    #print(title)
    log_info = [title]

    # model = ConstantModel(prefix="bg_")
    # params = model.make_params()
    # params["bg_c"].set(0.01, min=0, max=+0.01, vary=True)
    model = Model(bg_model, prefix="bg_")
    params = model.make_params()
    params['bg_BG'].set(0.00, min=-0.001, max=+0.005, vary=False)

    if filterIndex == 1 :

        params['bg_BGA1'].set(y[0], min=0, max=30, vary=True)
        params['bg_BGDECAY1'].set(10,min=5, max=30, vary=True)
        params['bg_BGX01'].set(x.min()-1,min=x.min()-100, max=x.min(), vary=False)

        params['bg_BGA2'].set(y[-1], min=0, max=30, vary=True)
        params['bg_BGDECAY2'].set(10,min=5, max=30,vary=True)
        params['bg_BGX02'].set(x.max()+1,min=x.max(), max=x.max()+100, vary=False)
    else:
        params['bg_BGA1'].set(0, min=0, max=25, vary=False)
        params['bg_BGDECAY1'].set(9.5,min=2, max=50,vary=False)
        params['bg_BGX01'].set(x[0]-20,min=x[0]-50, max=x[0]-10, vary=False)

        params['bg_BGA2'].set(0, min=0, max=5, vary=False)
        params['bg_BGDECAY2'].set(9.5,min=2, max=40,vary=False)
        params['bg_BGX02'].set(x[-1],min=x[-1]-20, max=x[-1]+20, vary=False)



    for band in bands:
        center = bands[band]

        # if center<x.min()*0.9 or center>x.max()*1.1:
        #     log_info.append(f'SKIP(out of range) {band} {center, (x.min()*0.9, x.max()*1.1)}')
        #     continue

        y_tmp=interpolated_sig(np.linspace(center - 2, center + 2, 100))
        y_init = abs(y_tmp).max()
        #SN = signaltonoise_dB(y_tmp)
        # if band == "3w2" and filterIndex == 1 and ex_wl<1030:
        #     SN = 0
        #     y_init=y[]
        pref = f"g{band}_".replace("+", "p").replace("-", "m")
        use_PseudoVoigt = True#( "3w" in band or "+" in band or "-" in band) and (not band == "w1+w2")
        if use_PseudoVoigt:
            g_model = PseudoVoigtModel(prefix=pref)
        else:
            g_model = GaussianModel(prefix=pref)

        vary = True


        if shutter != LaserShutterIndexes["OPENED"]: # BOTH OPENED
            beams_sync = False
            if shutter == LaserShutterIndexes["TUN"] and "w1" in band:
                log_info.append(f'SKIP (shutter) {band}')
                continue
            if shutter == LaserShutterIndexes["FIX"] and "w2" in band:
                log_info.append(f'SKIP (shutter) {band}')
                continue



        if abs(delay) > 0.1: # BOTH OPENED
            beams_sync = False



        if (band in ["w1+w2", "2w2+w1", "2w1+w2", "2w2-w1"]) and not beams_sync:
            log_info.append(f'SKIP (not sync) {band}')
            continue
        # elif  SN<-20:
        #     log_info.append(f'SKIP (SN) {band}, {SN}')
        #     continue
        elif y_init <= 1e-12 and len(bands)>=2:
            log_info.append(f'SKIP (<1e-12) {band}, {y_init}' )
            continue
        else:
            model += g_model
            params += g_model.make_params()

            params[pref + "center"].set(
                center, min=center - 2, max=center + 2, vary=vary
            )
            params[pref + "sigma"].set(1.7, min=1.4, max=4, vary=vary)
            params[pref + "amplitude"].set(
                max(y_init*4, 0.1), min=0, max=max(y_init*20,10), vary=vary
            )




            if band == "2w1":
                params[pref + "sigma"].set(1.75, min=1.7, max=1.9, vary=True)
                # params[pref + "center"].set(
                #     523, min=center - 5, max=center + 5, vary=True
                # )
            elif band == "3w1":
                params[pref + "sigma"].set(1.6, min=1.4, max=1.7, vary=True)

                if shutter == LaserShutterIndexes['FIX'] or shutter == LaserShutterIndexes['OPENED'] and filterIndex == 1:
                    params[pref + "amplitude"].set(
                        max(y_init*4, 0.1), min=y_init, max=max(y_init*20,200), vary=vary
                    )
            elif band == "2w2":
                params[pref + "sigma"].set(2.5, min=1.5, max=3.5, vary=vary)

            elif band == "3w2":
                params[pref + "sigma"].set(2., min=1.6, max=3, vary=vary)

            elif band == "2w2-w1":
                #params[pref + "sigma"].set(2., min=1.6, max=3, vary=vary)
                params[pref + "center"].set(
                    center, min=center - 10, max=center + 10, vary=True
                )
            else:
                pass

            if use_PseudoVoigt:
                if "-" in band:
                    params[pref+"fraction"].set(0.15,min=0.08, max=0.3,vary=True)
                if band in [ "2w1+w2"]:
                    params[pref+"fraction"].set(0.18,min=0.08, max=0.3,vary=False)
                else:
                    params[pref+"fraction"].set(0.14,min=0.13,max=0.2,vary=False)




    for PL_prefix, band_params in PL_bands.items():
        center, sigma, gamma = band_params
        if center<x.min() or center>x.max(): continue
        y_tmp = interpolated_sig(np.linspace(center - 2, center + 2, 100))
        y_init = abs(y_tmp).max()
        # SN = signaltonoise_dB(y_tmp)

        pref = PL_prefix+"_"


        sg_model = ExponentialGaussianModel(prefix=pref)

        vary = True

        if y_init <= 1e-12:
            log_info.append(f'SKIP (<1e-12) {PL_prefix}')
        # elif  SN<0:
        #     log_info.append(f'SKIP (SN) {PL_prefix}')
        else:
            model += sg_model
            params += sg_model.make_params()
            params[pref + "center"].set(
                center-1, min=center - 3, max=center + 1, vary=True
            )
            params[pref + "sigma"].set(sigma, min=abs(sigma-1), max=sigma+1, vary=True)

            if CenterType == "BG":
                params[pref + "amplitude"].set(
                    0, min=0, max=y_init*100+0.001, vary=False
                )
            else:
                if shutter in [LaserShutterIndexes["OPENED"], LaserShutterIndexes["FIX"]]:
                    min_y = interpolated_sig(np.linspace(center-20,center+20,100)).min()
                    params[pref + "amplitude"].set(
                        y_init, min=min_y*5, max=1000, vary=True
                    )
                else:
                    params[pref + "amplitude"].set(
                        y_init, min=0, max=1000, vary=True
                    )


            params[pref + "gamma"].set(gamma*0.9, min=gamma*0.5, max=gamma*2, vary=True)
        # if abs(delay)<0.05 and ex_wl >:
        #     sg_model_SYNC = ExponentialGaussianModel(prefix="SYNC" + pref)
        #
        #     vary = True
        #
        #     if y_init <= 1e-12:
        #         log_info.append(f'SKIP (<1e-12) {PL_prefix}')
        #     # elif  SN<0:
        #     #     log_info.append(f'SKIP (SN) {PL_prefix}')
        #     else:
        #         model += sg_model_SYNC
        #         params += sg_model_SYNC.make_params()
        #         params[pref + "center"].set(
        #             center-10, min=center - 15, max=center -5, vary=True
        #         )
        #         params[pref + "sigma"].set(sigma, min=abs(sigma-0.2), max=sigma+0.2, vary=True)
        #         params[pref + "amplitude"].set(
        #             y_init * 10, min=0, max=y_init*100+0.001, vary=True
        #         )
        #         params[pref + "gamma"].set(gamma, min=gamma*0.85, max=gamma*1.15, vary=True)



    title+=f"|{beams_sync=}"
    log_info.append(f"{beams_sync=}")

    #print(params.keys())

    if beams_sync and "g2w1pw2_center" in params and "g2w2pw1_center" in params :
        # if ex_wl>=1045 and ex_wl<1100:
        #     #"3w1  2w1+w2   2w2+w1  3w2"
        #     params.add('delta2w1pw2_3w1', value=bands['2w1+w2']-bands['3w1'], min=0.01, max=bands['2w1+w2']-bands['3w1']+1, vary=True)
        #     params['g2w1pw2_center'].set(expr='g3w1_center+delta2w1pw2_3w1')
        #
        #     params.add('delta2w2pw1_2w1pw2', value=bands['2w2+w1']-bands['2w1+w2'], min=0.01, max=bands['2w2+w1']-bands['2w1+w2']+1, vary=True)
        #     params['g2w2pw1_center'].set(expr='g2w1pw2_center+delta2w2pw1_2w1pw2')
        #
        #     params.add('delta3w2_2w2pw1', value=bands['3w2']-bands['2w2+w1'], min=0.01, max=bands['3w2']-bands['2w2+w1']+1, vary=True)
        #     params['g3w2_center'].set(expr='g2w2pw1_center+delta3w2_2w2pw1')




        # if ex_wl<1045 and ex_wl>1000:
        #
        #     #"3w1  2w1+w2   2w2+w1  3w2"
        #     params.add('delta2w1pw2_3w1', value=bands['2w1+w2']-bands['3w1'], max=-0.01, min=bands['2w1+w2']-bands['3w1']-1, vary=True)
        #     params['g2w1pw2_center'].set(expr='g3w1_center+delta2w1pw2_3w1')
        #
        #     params.add('delta2w2pw1_2w1pw2', value=bands['2w2+w1']-bands['2w1+w2'], max=-0.01, min=bands['2w2+w1']-bands['2w1+w2']-1, vary=True)
        #     params['g2w2pw1_center'].set(expr='g2w1pw2_center+delta2w2pw1_2w1pw2')
        #
        #     params.add('delta3w2_2w2pw1', value=bands['3w2']-bands['2w2+w1'], max=-0.01, min=bands['3w2']-bands['2w2+w1']-1, vary=True)
        #     params['g3w2_center'].set(expr='g2w2pw1_center+delta3w2_2w2pw1')
        if ex_wl>=1045 and ex_wl<1080:
            params.add('ratio2w2pw1_3w2', value=2, min=1, max=20, vary=True)
            params['g2w2pw1_amplitude'].set(expr='g3w2_amplitude*ratio2w2pw1_3w2')

            params.add('ratio2w1pw2_3w1', value=2, min=1, max=20, vary=True)
            params['g2w1pw2_amplitude'].set(expr='g3w1_amplitude*ratio2w1pw2_3w1')

            params.add('ratio3w2_3w1', value=1, min=0.5, max=2, vary=True)
            params['g3w2_amplitude'].set(expr='g3w1_amplitude*ratio3w2_3w1')

            params.add('ratiow1pw2_2w1p2w2', value=4, min=1.5, max=5, vary=True)
            params['gw1pw2_amplitude'].set(expr='(g2w1_amplitude+g2w2_amplitude)*ratiow1pw2_2w1p2w2')


    if shutter == LaserShutterIndexes["OPENED"] and ex_wl<710:

        params.add('ratio3w1_2w2', value=5, min=1, max=2000, vary=True)
        params['g3w1_amplitude'].set(expr='g2w2_amplitude*ratio3w1_2w2')


    init_eval = model.eval(x=x,params=params)

    try:
        out = model.fit(y, x=x, params=params, weights=weights)
    except KeyboardInterrupt:
        return
    except ValueError:
        traceback.print_exc()
        return
    print('='*100)
    print("\n".join(log_info))
    print(out.fit_report(show_correl=False,sort_pars=True).split("[[Fit Statistics]]")[-1])
    fig = None
    #try:
    if len(x)!=len(out.best_fit): return
    #except:
    #    return
    if plot:
        intens_ptp_ = 1
        fig, axes = plt.subplots(2,1,gridspec_kw={'height_ratios': [1, 1]},sharex=True)
        fig.suptitle(title,fontsize=6)
        axes[0].plot(x, init_eval*intens_ptp_, "k-.",alpha=0.5)
        axes[1].plot(x, init_eval*intens_ptp_, "k-.",alpha=0.5)
        axes[0].plot(x, (y)*intens_ptp_, ".",label='raw')
        axes[1].plot(x, (y)*intens_ptp_, ".",label='raw')
        #plt.plot(x, model.eval(x=x,params=params)*intens_ptp, "--k",label='init')

        axes[0].plot(x, out.best_fit*intens_ptp_, "-")
        axes[1].plot(x, out.best_fit*intens_ptp_, "-")
        components = out.eval_components(x=x)
        for model_name, model_value in components.items():
            try:
                axes[0].plot(x, model_value*intens_ptp_,'--', label=model_name)
                axes[1].plot(x, model_value*intens_ptp_,'--', label=model_name)
            except KeyboardInterrupt:
                return
            except:
                pass
        axes[0].legend(fontsize=8)
        #axes[0].set_ylim((-1e-3,max(out.best_fit*intens_ptp)))
        # if "g2w1_height" in out.params and "g2w2_height" in out.params:
        #     axes[1].set_ylim((-1e-5,np.mean([out.params['g2w1_height'].value,out.params['g2w2_height'].value])*intens_ptp*1.5))
        # elif "g2w1_height" in out.params:
        #     axes[1].set_ylim((-1e-5,out.params['g2w1_height'].value*intens_ptp*1.5))
        # elif "g2w2_height" in out.params:
        #     axes[1].set_ylim((-1e-5,max(0.01, out.params['g2w2_height'].value*intens_ptp*1.5)))
        # else:
        #     axes[1].set_ylim((-1e-5,max(out.best_fit*intens_ptp)/10))
        axes[1].set_ylim((-1e-5,intens_ptp_*1.5))
        axes[0].grid(True, which="both", ls="-")
        axes[1].grid(True, which="both", ls="-")

        axes[0].set_yscale('log')
        axes[0].set_ylim((max(y.min(),1e-4),y.max()*2))

        fig.tight_layout(h_pad=0)
    res = {}
    res['bg'] = none2nan(out.params['bg_BG'].value) * intens_ptp
    res['bg_stderr'] = none2nan(out.params['bg_BG'].stderr) * intens_ptp


    for band in bands:
        prefix = "g" + band.replace("+", "p").replace("-", "m") + "_"
        if prefix + "amplitude" in  out.params:
            res[band] = (
                none2nan(out.params[prefix + "amplitude"].value)
                * intens_ptp
            )

            res[band+"_center"] = (
                out.params[prefix + "center"].value
            )
            res[band+"_sigma"] = (
                out.params[prefix + "sigma"].value
            )

            res[band+"_stderr"] = (
                none2nan(out.params[prefix + "amplitude"].stderr)
                * intens_ptp
            )

            res[band+"_center_stderr"] = (
                out.params[prefix + "center"].stderr
            )
            res[band+"_sigma_stderr"] = (
                out.params[prefix + "sigma"].stderr
            )

    for band in PL_bands:
        if band + "_amplitude" in out.params:
            res[band] = (
                out.params[band + "_amplitude"].value
                * intens_ptp
            )
            res[band+"_center"] = (
                out.params[band + "_center"].value)

            res[band+"_sigma"] = (
                out.params[band + "_sigma"].value)

            res[band+"_gamma"] = (
                out.params[band + "_gamma"].value)



            res[band+"_stderr"] = (
                none2nan(out.params[band + "_amplitude"].stderr)
                * intens_ptp
            )
            res[band+"_center_stderr"] = (
                out.params[band + "_center"].stderr)

            res[band+"_sigma_stderr"] = (
            out.params[band + "_sigma"].stderr)

            res[band+"_gamma_stderr"] = (
                out.params[band + "_gamma"].stderr)

    res['chisqr'] = out.chisqr
    for name in data_row['position'].dtype.names:
        res[name] = data_row['position'][name]
    if get_metadata:
        for name in data_row['metadata'].dtype.names:
            res[name] = data_row['metadata'][name]
    res['time'] = data_row['time']

    if not CenterIndex is None:
        try:
            res['CenterType'] = center_info['Type'][CenterIndex].decode()

        except:
            return
    else:
        res['CenterType'] = ""

    res['FILTER'] = filterLabel

    return {"ex_wl":ex_wl,'params':res, "x":x, "y":out.best_fit * intens_ptp, "model":model, "result":out, "figure":fig}




def process_data_row(arg):
    data, kwargs = arg
    return process_data_row_(data, **kwargs)

def process_data_row_(data_row, args=None, bands=bands_generator(args.bands),
        lamp_correction=None, filters_index2Label=None, timestamp="",
        PL_bands=PL_bands_generator(args.bands_PL), center_info=None,
        pbar=None,output_path="processed"):

    #for i, data_row in enumerate(data[RANGE]):




    if len(args.delay_positions)!=0:
        skip=True
        for delay_pos in args.delay_positions:
            if abs(data_row['position']['Delay_line_position_zero_relative']-delay_pos)<0.01:
                skip = False
                break


        if skip:
            return {}

    res = fit_data(
        data_row,
        bands_generator=bands,
        sensitivity_corrector=lamp_correction,
        filters_translator=filters_index2Label,
        PL_bands=PL_bands,
        get_metadata=args.get_metadata,
        beams_sync=args.beams_sync, center_info=center_info,
        plot=True)

    #pbar.update(1)
    #global_i+=1
    if res is None:
        return {}
    # collector.append(res['params'])
    if not res['figure'] is None:
        #if not res['params']['CenterType'] == 'BG':
        try:
            #pdfSaver.savefig(res['figure'])
            ex_wl = res['ex_wl']
            centerIndex = data_row['position']['CenterIndex']
            time_ = data_row['time']

            name = f"Ex{ex_wl}_NP{centerIndex}_{time_}s"
            res['figure'].savefig(Path(output_path)/f"img/{name}.pdf", dpi=300)
        except KeyboardInterrupt:
            return
        except:
            traceback.print_exc()

    params = res['params']
    params["timestamp"] = timestamp
    plt.close()
    return params#, res['figure']

################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################

if __name__ == "__main__":

    store = exdir.File(args.file, "r")

    dataType = args.dataType



    data_dict = extractTimestamp(store, args.timestamps, dataType)


    last_timestamp = list(data_dict.keys())[-1]
    filters_index2Label = data_dict[last_timestamp]['attrs']["filters"]

    info = data_dict[last_timestamp]['attrs']["centerIndex_info"]

    lamp_data = pd.read_csv(args.sensitivity)
    # dichroicMirror_data = pd.read_csv('DMSP650R.csv')

    lamp_correction = {}
    for f in filters_index2Label.values():
        # tmp = interp1d(dichroicMirror_data.wavelength, dichroicMirror_data['T'],
        # 		bounds_error=False, fill_value=np.nan)
        wl = lamp_data.wavelength
        correction = lamp_data[f]  # *tmp(wl)
        w = correction > 1000

        lamp_correction[f] = interp1d(
            wl[w], correction[w], bounds_error=False, fill_value=np.nan
        )

    res = []
    # pulsewidth_corr_data = pd.read_csv('pulsewidth_correction.csv')
    # pulsewidth_corr = interp1d(pulsewidth_corr_data.Ex_wl, pulsewidth_corr_data.pulsewidth_correction,
    # 	bounds_error=False, fill_value=False)

    n = 0
    collector = []
    if args.range is None:
        RANGE = slice(None)
    else:
        RANGE = slice(*args.range)

    # path_pdf = args.file.split('.exdir')[0]+'.pdf'
    # if os.path.exists(path_pdf):
    #     copyfile(path_pdf, path_pdf.replace(".pdf","_backup.pdf"))
    #
    totalN = np.sum([len(data['data']) for data in data_dict.values()])


    path_csv = Path(args.output)/"fit_data.csv"#args.file.split('.exdir')[0]+'.csv'
    if os.path.exists(path_csv):
        copyfile(path_csv, str(path_csv).replace(".csv","_backup.csv"))

    global_i = 0

    if not Path(args.output).exists():
        Path(args.output).mkdir(parents=True)
        if not (Path(args.output)/"img").exists():
            (Path(args.output)/"img").mkdir(parents=True)

    with tqdm(total=totalN) as pbar:
        try:
            for timestamp, data_ in data_dict.items():
                data = data_['data']
                print(timestamp)

                params = dict(
                    args=args,
                    center_info=data_['attrs']['centerIndex_info'],
                    #pbar=pbar,
                    #pdfSaver=pdfSaver,
                    output_path=args.output,
                    filters_index2Label=filters_index2Label,
                    lamp_correction=lamp_correction,
                    timestamp=timestamp,

                )
                with Pool(processes=os.cpu_count()) as pool:
                    #print(list(zip(data[RANGE], [params]*len(data[RANGE]))))
                    collector += list(tqdm(pool.imap(process_data_row, list(zip(data[RANGE], [params]*len(data[RANGE])))),total=totalN))
        except KeyboardInterrupt:

            plt.close('all')

        except:
            traceback.print_exc()
            df = pd.DataFrame(collector)
            df.to_csv(path_csv)
    if args.show_plot:
        plt.show()
    else:
        plt.close('all')
    df = pd.DataFrame(collector)
    print(df)

    df.to_csv(path_csv)
