# -*- coding: utf-8 -*-
"""
This example demonstrates the use of pyqtgraph's parametertree system. This provides
a simple way to generate user interfaces that control sets of parameters. The example
demonstrates a variety of different parameter types (int, float, list, etc.)
as well as some customized parameter types

"""
import traceback
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

QtCore.pyqtRemoveInputHook()

import sys
from pathlib import Path

app = QtGui.QApplication([])
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import (
    Parameter,
    ParameterTree,
    ParameterItem,
    registerParameterType,
)

print(Path(".").absolute())
sys.path.append("utils")
import pandas as pd


from units_dtypes import AXES_UNITS_DTYPES, READOUT_SOURCES_UNITS_DTYPES


def gen_default_axis(name, axis="X"):
    ax = {
        "name": name,
        "type": "group",
        "children": [
            {
                "name": "Axis",
                "type": "list",
                "values": list(AXES_UNITS_DTYPES.keys()),
                "value": axis,
            },
            {"name": "Active", "type": "bool", "value": True},
            {"name": "Start", "type": "float", "value": 0, "decimals": 4},
            {"name": "End", "type": "float", "value": 100, "decimals": 4},
            {"name": "Step", "type": "float", "value": 1, "decimals": 4},
            {
                "name": "ScanByGrid",
                "type": "bool",
                "value": False,
                "children": [
                    {
                        "name": "Grid",
                        "type": "str",
                        "value": "0,50,100",
                        "visible": False,
                    }
                ],
            },
        ],
    }
    return ax


class ScriptTree(QtGui.QWidget):
    depth = 0

    def __init__(
        self,
        config={"name": "WaitBeforeReadout", "type": "float", "value": 0},
    ):
        super().__init__()
        layout = QtGui.QGridLayout()
        self.setLayout(layout)

        params = [
            {"name": "Config", "type": "group", "children": [config]},
            {
                "name": "Script",
                "type": "group",
                "children": [
                    # 	gen_default_axis('Axis#', 'X'),
                ],
            },
        ]

        ## Create tree of Parameter objects
        self.params = Parameter.create(name="params", type="group", children=params)

        self.params.sigTreeStateChanged.connect(self.on_change)

        ## Create ParameterTree widget
        self.tree = ParameterTree()
        self.tree.setParameters(self.params, showTop=False)
        self.addSubAxis(name="Axis#0", parent=self.params.child("Script"), axis="Z")
        self.addSubAxis(
            name="Axis#0.0", parent=self.params.child("Script", "Axis#0.0"), axis="X"
        )
        # self.addSubAxis(name='Axis#0.0.0', parent=self.params.child('Script','Axis#0.0','Axis#0.0.0'), axis='Y')

        layout.addWidget(self.tree, 0, 0, 1, 4)

        addButton = QtGui.QPushButton("Add")
        addButton.clicked.connect(self.addAxis)
        layout.addWidget(addButton, 1, 0, 1, 1)

        addSubButton = QtGui.QPushButton("Sublevel")
        addSubButton.clicked.connect(self.addSubAxis)
        layout.addWidget(addSubButton, 1, 1, 1, 1)

        removeButton = QtGui.QPushButton("Remove")
        removeButton.clicked.connect(self.removeAxis)
        layout.addWidget(removeButton, 1, 2, 1, 1)

        generateButton = QtGui.QPushButton("Generate")
        generateButton.clicked.connect(self.generateScript)
        layout.addWidget(generateButton, 1, 3, 1, 1)
        # self.show()

    def on_change(self, param, changes):
        print("tree changes:")
        # self.get_depth()
        for param, change, data in changes:
            path = self.params.childPath(param)
            print(path)
            if path is not None:
                childName = ".".join(path)
            else:
                childName = param.name()

            print("  parameter: %s" % childName)
            print("  change:	%s" % change)
            print("  data:	  %s" % str(data))
            print("  ----------")
            if path[-1] == "ScanByGrid" and change == "value":
                parent = self.params.child(*path[:-1])
                checkbox = self.params.child(*path)
                if checkbox.value():
                    parent.child("Start").hide()
                    parent.child("End").hide()
                    parent.child("Step").hide()
                    checkbox.child("Grid").show()
                else:
                    parent.child("Start").show()
                    parent.child("End").show()
                    parent.child("Step").show()
                    checkbox.child("Grid").hide()
            elif path[-1] == "Active" and change == "value":
                pass
                """
				parent = self.params.child(*path[:-1])
				checkbox = self.params.child(*path)
				for child in parent.childs[2:]:
					print(path, child.name())
					if checkbox.value():
						child.show()
					else:
						child.hide()
				dir(child)

				"""

    def removeAxis(self):
        if len(self.tree.selectedIndexes()) == 0:
            return
        child = self.tree.itemFromIndex(self.tree.currentIndex())[0].param
        if "Axis#" in child.name():
            child.parent().removeChild(child)

    def addAxis(self):
        # print(self.tree.itemFromIndex(self.tree.currentIndex())[0])
        # print(dir(self.tree.itemFromIndex(self.tree.currentIndex())[0]))

        child = self.tree.itemFromIndex(self.tree.currentIndex())[0].param
        indexOfChild = 0
        if "Axis#" in child.name():

            for i, child_ in enumerate(child.parent().childs):
                if child_ == child:
                    indexOfChild = i
                    break
            child.parent().insertChild(
                indexOfChild + 1, gen_default_axis(child.name()), autoIncrementName=True
            )
        elif child.name() == "Script":
            child.insertChild(0, gen_default_axis("Axis#"), autoIncrementName=True)

    def addSubAxis(self, status=None, parent=None, name=None, axis="X"):
        # print(self.tree.itemFromIndex(self.tree.currentIndex())[0])
        # print(dir(self.tree.itemFromIndex(self.tree.currentIndex())[0]))
        if parent is None:
            parent = self.tree.itemFromIndex(self.tree.currentIndex())[0].param
            name = parent.name()
        if "Axis#" in name:
            parent.addChild(gen_default_axis(name + ".0", axis), autoIncrementName=True)

    def generateScript(self):
        script = []

        def recGenScript(level, iter_list, parent, script):
            sub_levels = 0
            sub_lev = 0
            for child in parent:
                if "Axis#" in child.name():
                    # print(child.name(),'-'*100)
                    if child.child("ScanByGrid").value():
                        targets = np.array(
                            [
                                float(s)
                                for s in child.child("ScanByGrid")
                                .child("Grid")
                                .value()
                                .split(",")
                            ]
                        )
                    else:
                        targets = np.arange(
                            child.child("Start").value(),
                            child.child("End").value(),
                            child.child("Step").value(),
                        )
                        if not child.child("End").value() in targets:
                            targets = np.append(targets, child.child("End").value())

                    for i, target in enumerate(targets):
                        if child.child("Active").value():
                            script.append(
                                {
                                    "iteration": iter_list.copy(),
                                    "level": level,
                                    "axis": child.child("Axis").value(),
                                    "target": target,
                                    "type": "move",
                                }
                            )
                            sub_lev = recGenScript(
                                level + 1, iter_list + [0], child, script
                            )
                            if sub_lev == 0:
                                script.append(
                                    {
                                        "iteration": iter_list.copy(),
                                        "level": level,
                                        "type": "read",
                                    }
                                )

                        iter_list[-1] += 1

                    if sub_lev == 0:
                        script.append(
                            {
                                "iteration": iter_list.copy(),
                                "level": level,
                                "type": "afterLoop",
                            }
                        )
                    sub_levels += 1
            # print(sub_levels)
            return sub_levels

        recGenScript(
            level=0, iter_list=[0], parent=self.params.child("Script"), script=script
        )
        script.append(
            {
                "type": "finish",
            }
        )
        df = pd.DataFrame(script)

        # def get_max_depth(arr, depth=[0]):
        # 	print(arr)
        # 	depth[0] = max(len(arr),depth[0])
        # 	return depth[0]

        # res=df.iteration.apply(get_max_depth)
        print(df)
        for i, line in enumerate(script):
            if line["type"] == "move":
                print(f"Move: ax[{line['axis']}]->{line['target']}")
            elif line["type"] == "read":
                print(f"Read")
            elif line["type"] == "afterLoop":
                print("ActionAfterLast")
            elif line["type"] == "finish":
                print("FINISH")

        # print(res)

        return df


#
# ## test save/restore
# s = p.saveState()
# p.restoreState(s)
win = QtGui.QMainWindow()
params = ScriptTree()
win.setCentralWidget(params)
win.show()

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == "__main__":
    import sys

    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        QtGui.QApplication.instance().exec_()
