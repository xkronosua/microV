#!/usr/bin/python

###############
# PM100USB.py
#
# Copyright David Baddeley, 2012
# d.baddeley@auckland.ac.nz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
################
import logging
import pyvisa as visa

# logging.getLogger('pyvisa').setLevel(logging.INFO)


class PowerMeter:
    instr = None

    def __init__(self, ID="USB0::0x1313::0x8078::P0011470::INSTR", dispScale=14):
        # self.instr = visa.instrument(ID)
        self._rm = visa.ResourceManager()
        self.ID = ID
        self.dispScale = dispScale

    def initialize(self):
        self.resource = self._rm.open_resource(self.ID)
        self.instr = self.resource

    def shutdown(self):
        if not self.instr is None:
            self.instr.close()

    @property
    def power(self):
        return float(self.instr.query("MEAS:POW?"))

    @property
    def correction_wavelength(self):
        return float(self.instr.query("SENS:CORR:WAV?"))

    @correction_wavelength.setter
    def correction_wavelength(self, wavelength):
        self.instr.write("SENS:CORR:WAV %d" % wavelength)

    @property
    def average_count(self):
        return int(self.instr.query("SENSe:AVERage?"))

    @average_count.setter
    def average_count(self, n):
        self.instr.write("SENSe:AVERage %d" % n)

    # def GetStatusText(self):
    # 	try:
    # 		pow = self.GetPower()
    # 		return 'Laser power: %3.3f mW' % (pow*1e3*self.dispScale)
    # 	except:
    # 		return 'Laser power: ERR'
