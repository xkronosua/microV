import pyqtgraph as pg
from pyqtgraph.examples.optics import *
from pyoptic import Axicon
import numpy as np
from pyqtgraph import Point

app = pg.QtGui.QApplication([])

w = pg.GraphicsLayoutWidget(show=True, border=0.5)
w.resize(1000, 900)
w.show()


### Dispersion demo

optics = []

view = w.addViewBox()

view.setAspectLocked()
# grid = pg.GridItem()
# view.addItem(grid)
view.setRange(pg.QtCore.QRectF(-10, -50, 90, 60))

optics = []
rays = []
l1 = Axicon(pos=(0, 0), r1=3.8, d=7, angle=0, glass="Corning7980")
optics.append(l1)

l2 = Lens(pos=(80, 0), r1=23, r2=23, d=5, angle=0, glass="Corning7980")
optics.append(l2)

l3 = Lens(pos=(100, 0), r1=2, r2=2, d=2, angle=0, glass="Corning7980")
optics.append(l3)

allRays = []
for wl in np.linspace(700, 1300, 25):
    for y in [0.5]:
        r = Ray(start=Point(-100, y), wl=wl)
        view.addItem(r)
        allRays.append(r)

        r = Ray(start=Point(-100, 0.2), wl=1045)
        view.addItem(r)
        allRays.append(r)

        r = Ray(start=Point(-100, -y), wl=wl)
        view.addItem(r)
        allRays.append(r)

        r = Ray(start=Point(-100, -0.2), wl=1045)
        view.addItem(r)
        allRays.append(r)

r = Ray(start=Point(-100, 0), wl=1045)
view.addItem(r)
allRays.append(r)

for o in optics:
    view.addItem(o)

t2 = Tracer(allRays, optics)


## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == "__main__":
    import sys

    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        QtGui.QApplication.instance().exec_()
