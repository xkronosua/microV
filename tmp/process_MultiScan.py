import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter
import time
import numpy_indexed as npi
import argparse
import pandas as pd
from lmfit.models import GaussianModel, ConstantModel, ExponentialModel
from pathlib import Path


parser = argparse.ArgumentParser(description="Process data for multiscan measurements.")
parser.add_argument("-f", dest="file", help="Exdir to process")


parser.add_argument(
    "-t",
    dest="timestamps",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in "MultiScan" folder.
					Integer index to process by index in sorted list of timestamps.
					Last measured "-1" by default.
					""",
    default=["-1"],
)
parser.add_argument(
    "-O", dest="output", help="Output exdir folder.", default="processed.exdir"
)


args = parser.parse_args()
print(args)

dataType = "MultiScan"
store = exdir.File(args.file, "r")


def extractTimestamp(store, timestamps, dataType):
    timestamps_ = []
    keys = list(store[dataType])
    keys.sort()
    print(timestamps)

    if "ALL" in timestamps:
        timestamps_ = [k for k in store[dataType] if k[0] != "_"]
    else:
        for ts in timestamps:
            try:
                timestamps_.append(keys[int(ts)])
            except:
                traceback.print_exc()
                timestamps_.append(ts)
    print(timestamps_)
    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    dtype = store[dataType][timestamps_[-1]]["data"].dtype

    data = np.zeros(np.sum(sizes), dtype=dtype)
    last_row = 0
    data_dict = {}
    for i, k in enumerate(timestamps_):
        # if store[dataType][k]['data'].dtype==dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        if w.sum() > 0:
            data_dict[k] = d
        # data[last_row:last_row+w.sum()] = d[w]
        # last_row+=w.sum()
        # print(i,k)
    data_attrs = store[dataType][timestamps_[-1]]["data"].attrs.to_dict()
    # data = data[data['time']!=0]

    return data_dict, data_attrs


centers = lambda ex_wl: {
    "2w1": 1045 / 2,
    "2w2": ex_wl / 2,
    "3w1": 1045 / 3,
    "3w2": ex_wl / 3,
    "w1+w2": 1 / (1 / 1045 + 1 / ex_wl),
    # '2w1-w2': 1/(2/1045-1/ex_wl),
    "2w2-w1": 1 / (-1 / 1045 + 2 / ex_wl),
    "2w1+w2": 1 / (2 / 1045 + 1 / ex_wl),
    "2w2+w1": 1 / (1 / 1045 + 2 / ex_wl),
}


def fit_data(wl, intens, ex_wl, centers, filter_index, sync=False, plot=False):
    w = (~np.isnan(intens)) & (~np.isnan(wl))
    y = intens[w]
    x = wl[w].astype(float)
    intens_max = np.ptp(intens[w])
    y = (intens[w] / intens_max).astype(float)
    w = (x > 350) & (x < 650)
    x = x[w]
    y = y[w]

    if len(x) == 0:
        raise Exception("aaaaa")

    iu = interp1d(x, y, bounds_error=False, fill_value=0)
    model = ConstantModel(prefix="bg_")
    params = model.make_params()
    # if ex_wl<=725:
    # 	params['bg_c'].set(y.min(), min=-0.1, max=0.1, vary=False)
    # else:
    params["bg_c"].set(0, min=-0.01, max=0.01, vary=False)

    for k in centers:
        y_init = abs(iu(np.linspace(centers[k] - 10, centers[k] + 10, 100)).max())
        # if centers[k]>x.min() and centers[k]<x.max():
        pref = f"g{k}_".replace("+", "p").replace("-", "m")
        g_model = GaussianModel(prefix=pref)
        model += g_model
        params += g_model.make_params()
        vary = True
        if y_init <= 1e-8:
            vary = False
            params[pref + "center"].set(
                centers[k], min=centers[k] - 3, max=centers[k] + 3, vary=vary
            )
            params[pref + "sigma"].set(1.6, min=1, max=3.5, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=y_init / 20, max=10, vary=vary
            )

        elif (k in ["w1+w2", "2w2+w1", "2w1+w2", "2w2-w1"]) and not sync:
            vary = False
            y_init = 0
            params[pref + "center"].set(
                centers[k], min=centers[k] - 3, max=centers[k] + 3, vary=vary
            )
            params[pref + "sigma"].set(1.6, min=1, max=3.5, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=y_init / 20, max=10, vary=vary
            )

        elif k in ["3w1", "3w2", "2w2-w1"]:
            vary = True
            params[pref + "center"].set(
                centers[k] - 3, min=centers[k] - 5, max=centers[k] + 2, vary=vary
            )
            params[pref + "sigma"].set(3.6, min=1, max=5, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=y_init / 20, max=y.max() * 10, vary=vary
            )

        else:
            params[pref + "center"].set(
                centers[k], min=centers[k] - 3, max=centers[k] + 3, vary=vary
            )
            params[pref + "sigma"].set(1.6, min=1, max=3.5, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=y_init / 20, max=10, vary=vary
            )

    out = model.fit(y, x=x, params=params)
    print(out.fit_report(min_correl=0.99))
    if plot:
        plt.plot(x, y, ".")
        plt.plot(x, out.best_fit, "-")
    res = {}
    for k in centers:
        res[k] = (
            out.params["g" + k.replace("+", "p").replace("-", "m") + "_amplitude"].value
            * intens_max
        )
    return res, (x, out.best_fit * intens_max, model, out)


data_dict, data_attrs = extractTimestamp(store, args.timestamps, dataType)


filters = data_attrs["filters"]
currentFilter = filters[data_attrs["filtersPiezoStage"]]
currentFilter_index = 0
info = data_attrs["centerIndex_info"]

lamp_data = pd.read_csv("AndorCameraCalibr_Microscope_forward.csv")
# dichroicMirror_data = pd.read_csv('DMSP650R.csv')

lamp_correction = {}
for f in filters.values():
    # tmp = interp1d(dichroicMirror_data.wavelength, dichroicMirror_data['T'],
    # 		bounds_error=False, fill_value=np.nan)
    wl = lamp_data.wavelength
    correction = lamp_data[f]  # *tmp(wl)
    w = correction > 1000

    lamp_correction[f] = interp1d(
        wl[w], correction[w], bounds_error=False, fill_value=np.nan
    )

res = []
pulsewidth_corr_data = pd.read_csv("pulsewidth_correction.csv")
pulsewidth_corr = interp1d(
    pulsewidth_corr_data.Ex_wl,
    pulsewidth_corr_data.pulsewidth_correction,
    bounds_error=False,
    fill_value=False,
)

n = 0
for timestamp, data in data_dict.items():
    for g in npi.group_by(data["position"]["LaserWavelength"]).split(data):
        ex_wl = g["position"]["LaserWavelength"].mean()
        if ex_wl == 0:
            continue

        fig, axes = plt.subplots(1, 2, sharex=True, sharey=True, num=0)
        fig.suptitle(f"@{ex_wl}")
        group_by_filter = [g]
        if "filtersPiezoStage" in g["position"].dtype.names:
            group_by_filter = npi.group_by(g["position"]["filtersPiezoStage"]).split(g)
        elif "filtersPiezoStage" in g["metadata"].dtype.names:
            group_by_filter = npi.group_by(g["metadata"]["filtersPiezoStage"]).split(g)

        for g1 in group_by_filter:

            if "filtersPiezoStage" in g1["position"].dtype.names:
                currentFilter = filters[g1["position"]["filtersPiezoStage"][0]]
                currentFilter_index = g1["position"]["filtersPiezoStage"][0]
            elif "filtersPiezoStage" in g1["metadata"].dtype.names:
                currentFilter = filters[g1["metadata"]["filtersPiezoStage"][0]]
                currentFilter_index = g1["metadata"]["filtersPiezoStage"][0]

            centerIndex_bg = info["Type"].index(b"BG")
            max_index = max_index = 0

            tmp = {}
            for centerIndex in range(len(info["Type"])):
                d = g1[g1["position"]["CenterIndex"] == centerIndex]
                wl = d["data"]["AndorCamera"]["raw"]["wavelength"].copy()
                if len(wl) == 0:
                    break
                intens_ = d["data"]["AndorCamera"]["raw"]["intensity"]
                intens_sfg = d["data"]["AndorCamera"]["data"]["w1+w2"]

                exposure = d["data"]["AndorCamera"]["raw"]["exposure"]
                intens = (intens_.T / exposure).T

                wl[~((wl > 355) & (wl < 650))] = np.nan
                intens[~((wl > 355) & (wl < 650))] = np.nan
                intens[abs(intens) == np.inf] = np.nan
                intens[abs(intens) > 1e8] = np.nan
                intens[np.isnan(lamp_correction[currentFilter](wl))] = np.nan

                if np.nansum(intens) == 0:
                    break
                max_index = np.where(intens_sfg == intens_sfg.max())[0][0]
                min_index = np.where(intens_sfg == intens_sfg.min())[0][0]
                try:
                    res_min, (x_min, y_min, model, out) = fit_data(
                        wl[min_index],
                        intens[min_index],
                        ex_wl,
                        centers(ex_wl),
                        currentFilter_index,
                        sync=False,
                        plot=False,
                    )
                    res_max, (x_max, y_max, model, out) = fit_data(
                        wl[max_index],
                        intens[max_index],
                        ex_wl,
                        centers(ex_wl),
                        currentFilter_index,
                        sync=True,
                        plot=False,
                    )
                except:
                    traceback.print_exc()
                axes[0].plot(x_min, y_min / lamp_correction[currentFilter](x_min), "-")
                axes[1].plot(x_max, y_max / lamp_correction[currentFilter](x_max), "-")

                axes[0].plot(
                    wl[min_index],
                    intens[min_index] / lamp_correction[currentFilter](wl[min_index]),
                    ".",
                )
                axes[1].plot(
                    wl[max_index],
                    intens[max_index] / lamp_correction[currentFilter](wl[max_index]),
                    ".",
                )

                tmp[info["Type"][centerIndex].decode() + str(centerIndex)] = {
                    "ex_wl": ex_wl,
                    "filter": currentFilter,
                    "centerIndex": centerIndex,
                    "timestamp": timestamp,
                    "data": {"sync": res_max, "delay": res_min},
                }
            name_bg = f"BG{centerIndex_bg}"
            if not name_bg in tmp:
                continue
            for name in tmp:
                if "BG" in name:
                    continue
                t = {
                    "ex_wl": tmp[name]["ex_wl"],
                    "centerIndex": tmp[name]["centerIndex"],
                    "timestamp": tmp[name]["timestamp"],
                    "filter": tmp[name]["filter"],
                }
                for k in tmp[name]["data"]["sync"]:
                    t[k + "_sync"] = (
                        tmp[name]["data"]["sync"][k] - tmp[name_bg]["data"]["sync"][k]
                    ) / lamp_correction[currentFilter](centers(ex_wl)[k])
                    if k == "2w2":
                        t[k + "_sync"] *= pulsewidth_corr(ex_wl) ** 2
                    elif k == "w1+w2":
                        t[k + "_sync"] *= pulsewidth_corr(ex_wl)
                for k in tmp[name]["data"]["delay"]:
                    t[k] = (
                        tmp[name]["data"]["delay"][k] - tmp[name_bg]["data"]["delay"][k]
                    ) / lamp_correction[currentFilter](centers(ex_wl)[k])
                    if k == "2w2":
                        t[k] *= pulsewidth_corr(ex_wl) ** 2
                    elif k == "w1+w2":
                        t[k] *= pulsewidth_corr(ex_wl)
                res.append(t)
        fig.savefig(Path(f"img/{timestamp}_{ex_wl}.png"))
        fig.clf()
        n += 1
        # if n>15: break


df = pd.DataFrame(res)

fig, axes = plt.subplots(3, 1, sharex=True)
symbols = "sox+d"
df_tmp = df.copy()
for i, g in df.groupby("centerIndex", as_index=False):
    w = g["ex_wl"] == 1100
    index = g["centerIndex"].values[0]

    # df_tmp = g.copy()
    for ib, band in enumerate(["2w2", "2w1", "w1+w2_sync"]):
        df_tmp[band].loc[g.index] /= g[band][w].mean()

        axes[ib].plot(
            df_tmp.loc[g.index]["ex_wl"],
            df_tmp.loc[g.index][band],
            symbols[i],
            label=f"NP{index}",
        )
        # w1 = df_tmp['filter']=='EO84-712_FGB37x2'
        # df_tmp[band][w1]*=1.1
        # for j,gg in df_tmp.iterrows():
        # 	print(gg['timestamp'])
        # 	axes[ib].text(gg['ex_wl'],gg[band], gg['timestamp'].split('-')[1],rotation=45,alpha=0.5,fontsize='xx-small')
        axes[ib].grid(1)
df_tmp = df_tmp[df_tmp["2w1"] > 0.6]
df_tmp = df_tmp[df_tmp["2w2"] > 0.1]
df_tmp = df_tmp[df_tmp["2w2"] < 20]

df1 = df_tmp.groupby("ex_wl", as_index=False).mean()
axes[0].legend()
axes[0].plot(df1["ex_wl"], medfilt(df1["2w2"], 3), "-", lw=3)
axes[1].plot(df1["ex_wl"], medfilt(df1["2w1"], 3), "-", lw=3)
axes[2].plot(df1["ex_wl"], medfilt(df1["w1+w2_sync"], 3), "-", lw=3)

axes[-1].set_xlabel("Pump wavelength, nm")
axes[0].set_ylabel("2w2")
axes[1].set_ylabel("2w1")
axes[2].set_ylabel("w1+w2")
axes[0].set_ylim((0, 5))
axes[1].set_ylim((0, 5))
axes[2].set_ylim((0, 5))
# plt.show()
