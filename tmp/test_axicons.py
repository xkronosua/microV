from matplotlib import pyplot as plt
import numpy as np
from raytracing import *
import pandas as pd
from scipy.special import erf
from axicon import Axicon
# Demo #8: Virtual image
path = ImagingPath()
path.fanNumber = 10
path.fanAngle = 10
path.objectHeight = 2.8*2
WAVELENGTH = 1.045
WAVELENGTH = 1.300
#WAVELENGTH = 0.7
Ax0 = Axicon(alpha=-np.deg2rad(2),n=materials.FusedSilica.n(WAVELENGTH),diameter=25.4)
Ax1 = Axicon(alpha=np.deg2rad(2),n=materials.FusedSilica.n(WAVELENGTH),diameter=25.4)


<<<<<<< HEAD
path.append(Space(d=1200))
path.append(DielectricSlab(n=materials.FusedSilica.n(WAVELENGTH),thickness=5,diameter=25.4))
path.append(Ax0)
path.append(Space(d=400))
path.append(Ax1)
path.append(DielectricSlab(n=materials.FusedSilica.n(WAVELENGTH),thickness=5,diameter=25.4))

path.append(Space(d=70))
Zoom = 4
f_L3 = 240

f_L4 = f_L3/Zoom
=======
path.append(Space(d=900))
path.append(DielectricSlab(n=materials.FusedSilica.n(WAVELENGTH),thickness=5,diameter=25.4))
path.append(Ax0)
path.append(Space(d=500))
path.append(Ax1)
path.append(DielectricSlab(n=materials.FusedSilica.n(WAVELENGTH),thickness=5,diameter=25.4))
path.append(Space(d=50))
Zoom = 4
f_L3 = 350

f_L4 = -f_L3/Zoom
>>>>>>> 1b551af3e408588adae807df464b95236d4d0872

path.append(Lens(f=f_L3,diameter=25.4))
path.append(Space(d=f_L4+f_L3))
path.append(Lens(f=f_L4,diameter=25.4))
#path.append(Space(d=150))
<<<<<<< HEAD
=======
#BE = Matrix(1/Zoom,280,0,Zoom)
#path.append(Space(d=280))
#path.append(BE)

>>>>>>> 1b551af3e408588adae807df464b95236d4d0872
path.append(Space(d=300))



# # define a list of rays with uniform distribution
# inputRays = UniformRays(yMin = minHeight,
# 				yMax = maxHeight,
# 				maxCount = nRays,
# 				thetaMax = maxTheta,
# 				thetaMin = minTheta)
#
divergence = 1.5e-3 #rad

inputRays = UniformRays(
	yMax=2.8,
	yMin=-2.8,
	thetaMax=divergence/2#np.deg2rad(0.1),
	,
	thetaMin=-divergence/2#-np.deg2rad(0.1),
	,
	M=2,
	N=20,
)
def GaussBeam(N, w, divergence, wavelength,isBlocked=False,y0=0,z=0,divergenceX=None,divergenceY=None):
	#A = np.linspace(0.01,0.99,N)
	#x = np.linspace(0,w,N)

	#div = np.linspace((divergence/2)**(1/6),0,N)**6
	#Y = np.sqrt(np.abs(np.log(A)*w**2/2))
	#Y = (1-erf(2**0.5*x/w))
	#print(Y,A)
	print(divergence,divergenceX,divergenceY)
	Y = np.random.normal(scale=w/2,size=N)
	div = np.random.normal(scale=divergence/2/2,size=N)#np.random.uniform(-divergence/2,divergence/2,N)
	divX = np.random.normal(scale=divergenceX/2/2,size=N)#np.random.uniform(-divergence/2,divergence/2,N)
	divY = np.random.normal(scale=divergenceY/2/2,size=N)#np.random.uniform(-divergence/2,divergence/2,N)

	rays = []
	for i,y in enumerate(Y):
		div_ = div[i]
		if y>0 and not divergenceX is None:
			div_ = divX[i]
		elif y<=0 and not divergenceY is None:
			div_ = divY[i]
		ray = Ray(
		y=y+y0,
		theta=div_,
		z=z,
		isBlocked=isBlocked,
		wavelength=wavelength,
		)
		rays.append(ray)
	# for i,y in enumerate(Y):
	# 	ray = Ray(
	# 	y=y0+y,
	# 	theta=div[i],
	# 	z=z,
	# 	isBlocked=isBlocked,
	# 	wavelength=wavelength,
	# 	)
	# 	rays.append(ray)
	return Rays(rays)

inputRays = GaussBeam(2000,2.8,1.5e-3,WAVELENGTH, divergenceX=1.5e-3,divergenceY=1.4e-3)
path.display(rays=inputRays)

#inputRays = GaussBeam(10000,2.8,1.5e-3,WAVELENGTH)

r=path.traceManyThroughInParallel(inputRays)
r.display()
