import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter, medfilt
import time
import numpy_indexed as npi
import argparse
import pandas as pd
from lmfit.models import GaussianModel, ExponentialGaussianModel, ConstantModel, ExponentialModel
from pathlib import Path
from matplotlib import cm
import matplotlib

from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description="Process data for multiscan measurements.")
parser.add_argument("-f", dest="file", help="CSV file from preprocessing")

parser.add_argument(
    "--sync", dest="beams_sync", help="Measurements with delay.", action="store_true"
)

parser.add_argument(
    "--center", dest="CenterIndex", help="Center indexes to process.", nargs="+",
    type=int, default=None
)

parser.add_argument(
    "-N", dest="norm", help="Normalize at wavelength.", type=int, default=1045
)

parser.add_argument(
    "--normBy", dest="normBy", help="Normalize by reference.", type=str, default=None
)

parser.add_argument(
    "--fit2level", dest="fit2level", help="Fit SHG by two-level model.", action="store_true"
)

args = parser.parse_args()
print(args)

df = pd.read_csv(args.file)

BANDS = ["2w1", "2w2", "3w1", "3w2", "PL374", 'w1+w2', '2w2-w1', '2w1+w2', '2w2+w1']

MODES = ["FIX", 'TUN', 'SYNC', 'not SYNC']
axes = {}
for mode in MODES:
    fig1, ax = plt.subplots(1,1)
    fig1.suptitle(mode)
    axes[mode] = ax



for i,g in df.groupby('CenterIndex'):
    mask_sync = abs(g['Delay_line_position_zero_relative'].round(2)) <= 0.1
    mask_FIX = g['LaserShutter'] == 1
    mask_TUN = g['LaserShutter'] == 2
    mask_OPENED = g['LaserShutter'] == 3
    centerIndex = int(g["CenterIndex"].mean())
    if centerIndex != 5 : continue
    for band_ in BANDS:
        if not band_ in g: continue
        band = band_ + "_rmBG"
        d = g[mask_FIX]
        d = d[~d[band].isna()].groupby("LaserWavelength",as_index=False).mean()


        #print(d.keys())
        fig = plt.figure()
        fig.suptitle(f"NP{centerIndex}:{band}")
        #plt.plot(d['LaserWavelength'], d[band],label=f"FIX")
        plt.errorbar(d['LaserWavelength'], d[band], yerr=d[band]*0.035, label=f"FIX", capsize=2, elinewidth=1,fmt='.-', #ecolor='k',
        lw=0.5)
        axes["FIX"].errorbar(d['LaserWavelength'], d[band], yerr=d[band]*0.035, label=band_, capsize=2, elinewidth=1,fmt='.-', #ecolor='k',
        lw=0.5)

        d = g.loc[mask_TUN]
        d = d[~d[band].isna()].groupby("LaserWavelength",as_index=False).mean()
        #plt.plot(d['LaserWavelength'], d[band],label=f"TUN")
        plt.errorbar(d['LaserWavelength'], d[band], yerr=d[band]*0.035, label=f"TUN", capsize=2, elinewidth=1,fmt='.-', #ecolor='k',
        lw=0.5)
        axes["TUN"].errorbar(d['LaserWavelength'], d[band], yerr=d[band]*0.035, label=band_, capsize=2, elinewidth=1,fmt='.-', #ecolor='k',
        lw=0.5)


        d = g.loc[mask_OPENED & mask_sync]
        d = d[~d[band].isna()].groupby("LaserWavelength",as_index=False).mean()
        #plt.plot(d['LaserWavelength'], d[band],label=f"SYNC")
        plt.errorbar(d['LaserWavelength'], d[band], yerr=d[band]*0.035, label=f"SYNC", capsize=2, elinewidth=1,fmt='.-', #ecolor='k',
        lw=0.5)
        axes["SYNC"].errorbar(d['LaserWavelength'], d[band], yerr=d[band]*0.035, label=band_, capsize=2, elinewidth=1,fmt='.-', #ecolor='k',
        lw=0.5)



        d = g.loc[mask_OPENED & (~mask_sync)]
        d = d[~d[band].isna()].groupby("LaserWavelength",as_index=False).mean()
        #plt.plot(d['LaserWavelength'], d[band],label=f"not SYNC")
        plt.errorbar(d['LaserWavelength'], d[band], yerr=d[band]*0.035, label=f"not SYNC", capsize=2, elinewidth=1,fmt='.-', #ecolor='k',
        lw=0.5)
        axes["not SYNC"].errorbar(d['LaserWavelength'], d[band], yerr=d[band]*0.035, label=band_, capsize=2, elinewidth=1,fmt='.-', #ecolor='k',
        lw=0.5)
        plt.grid(True, which="both", ls="-")
        plt.xlabel('Ex. wavelength, nm')
        plt.ylabel('Intensity, arb. un.')
        plt.legend()

for mode in MODES:
    axes[mode].legend()
    axes[mode].grid(True, which="both", ls="-")
    axes[mode].set_xlabel('Ex. wavelength, nm')
    axes[mode].set_ylabel('Intensity, arb. un.')
    axes[mode].legend()


def E_3w1(ex_wl): return 1240/1045*3
def E_3w2(ex_wl): return 1240/ex_wl*3
def E_2w2(ex_wl): return 1240/ex_wl*2
def E_3w1(ex_wl): return np.array([1240/1045*3]*len(ex_wl))
def E_w1pw2(ex_wl): return 1240/(1/ex_wl+1/1045)*2
def E_2w1pw2(ex_wl): return 1240/ex_wl + 1240*2/1045
def E_w1pw2(ex_wl): return 1240/ex_wl+1240/1045
def E_2w2pw1(ex_wl): return 1240*2/ex_wl + 1240/1045
def E_g(ex_wl,PL3P=373): return 1240/PL3P
ex_wl = np.linspace(700,1300,1000)

fig2, axes2 = plt.subplots(2,2,sharex=True)
cubic_proc = ["3w2", "3w2", '2w1+w2', '2w2+w1']
for i,ax in enumerate(axes2.flatten()):
    ax.set_title(cubic_proc[i]+"_normBG")

fig3, axes3 = plt.subplots(2,2,sharex=True)
cubic_proc = ["3w2", "3w2", '2w1+w2', '2w2+w1']
for i,ax in enumerate(axes3.flatten()):
    ax.set_title(cubic_proc[i]+"_normBG")


resonance_wl = {
    "3w2": ex_wl[abs(E_3w2(ex_wl)-E_g(ex_wl)).argmin()].mean(),
    "2w1+w2": ex_wl[abs(E_2w1pw2(ex_wl)-E_g(ex_wl)).argmin()].mean(),
    "2w2+w1": ex_wl[abs(E_2w2pw1(ex_wl)-E_g(ex_wl)).argmin()].mean(),
}

for i,g in df.groupby('CenterIndex'):
    mask_sync = abs(g['Delay_line_position_zero_relative'].round(2)) <= 0.1
    mask_FIX = g['LaserShutter'] == 1
    mask_TUN = g['LaserShutter'] == 2
    mask_OPENED = g['LaserShutter'] == 3
    centerIndex = int(g["CenterIndex"].mean())
    if centerIndex == 7: continue
    d = g.loc[mask_TUN]
    d = d[~d["3w2_rmnormBG"].isna()].groupby("LaserWavelength",as_index=False).mean()
    #print(d,mask_TUN.sum())
    #w = abs(d["3w2_rmnormBG"])<1e2
    x_tmp = d['LaserWavelength']#[w]
    y_tmp = d["3w2_rmnormBG"]#[w]
    #y_tmp /= y_tmp[(x_tmp>1080) & (x_tmp<1120)].mean()
    axes2.flatten()[0].plot(x_tmp, y_tmp,  '.-', label=f"NP{centerIndex}",  lw=0.5)
    if centerIndex==0:
        axes2.flatten()[0].plot([resonance_wl['3w2']]*2, [y_tmp.min(),y_tmp.max()],  '--', label=f"rezonance",  lw=1.5)
    d = g.loc[mask_OPENED & mask_sync]
    d = d[~d[band].isna()].groupby("LaserWavelength",as_index=False).mean()
    for i,band_ in enumerate(cubic_proc[1:]):
        w = abs(d[band_+"_rmnormBG"])<1e2
        x_tmp = d['LaserWavelength'][w]
        y_tmp = d[band_+"_rmnormBG"][w]
        #y_tmp /= y_tmp[(x_tmp>1080) & (x_tmp<1120)].mean()
        axes2.flatten()[i+1].plot(x_tmp, y_tmp,  '.-', label=f"NP{centerIndex}",  lw=0.5)
        if centerIndex==0:
            axes2.flatten()[i+1].plot([resonance_wl[band_]]*2, [y_tmp.min(),y_tmp.max()],  '--', label=f"rezonance",  lw=1.5)

        axes2.flatten()[i+1].set_title(band_)

for ax in axes2.flatten():
    ax.legend()
    ax.grid(True, which="both", ls="-")
    ax.set_xlabel('Ex. wavelength, nm')
    ax.set_ylabel('Intensity, arb. un.')

    ax.set_ylim((-0.5,3))

    ax.legend()


for i,g in df.groupby('CenterIndex'):
    mask_sync = abs(g['Delay_line_position_zero_relative'].round(2)) <= 0.1
    mask_FIX = g['LaserShutter'] == 1
    mask_TUN = g['LaserShutter'] == 2
    mask_OPENED = g['LaserShutter'] == 3
    centerIndex = int(g["CenterIndex"].mean())
    if centerIndex == 7: continue
    d = g.loc[mask_TUN]
    d = d[~d["3w2_rmnormBG"].isna()].groupby("LaserWavelength",as_index=False).mean()
    #print(d,mask_TUN.sum())
    #w = abs(d["3w2_rmnormBG"])<1e2
    x_tmp = d['LaserWavelength']#[w]
    y_tmp = d["3w2_rmnormBG"]#[w]
    y_tmp /= y_tmp[(x_tmp>(resonance_wl['3w2']-20)) & (x_tmp<(resonance_wl['3w2']+20))].mean()
    axes3.flatten()[0].plot(x_tmp, y_tmp,  '.-', label=f"NP{centerIndex}",  lw=0.5)
    if centerIndex==0:
        axes3.flatten()[0].plot([resonance_wl['3w2']]*2, [y_tmp.min(),y_tmp.max()],  '--', label=f"rezonance",  lw=1.5)
    d = g.loc[mask_OPENED & mask_sync]
    d = d[~d[band].isna()].groupby("LaserWavelength",as_index=False).mean()
    for i,band_ in enumerate(cubic_proc[1:]):
        w = abs(d[band_+"_rmnormBG"])<1e2
        x_tmp = d['LaserWavelength'][w]
        y_tmp = d[band_+"_rmnormBG"][w]
        y_tmp /= y_tmp[(x_tmp>1100) & (x_tmp<1300)].abs().mean()
        axes3.flatten()[i+1].plot(x_tmp, y_tmp,  '.-', label=f"NP{centerIndex}",  lw=0.5)
        if centerIndex==0:
            axes3.flatten()[i+1].plot([resonance_wl[band_]]*2, [y_tmp.min(),y_tmp.max()],  '--', label=f"rezonance",  lw=1.5)

        axes3.flatten()[i+1].set_title(band_)

for ax in axes3.flatten():
    ax.legend()
    ax.grid(True, which="both", ls="-")
    ax.set_xlabel('Ex. wavelength, nm')
    ax.set_ylabel('Intensity, arb. un.')

    ax.set_ylim((-0.5,3))

    ax.legend()


plt.figure()

for band in ["3w2", '2w1+w2', "2w2+w1", "2w2-w1"]:
    k = band+"_rmnormBG"
    w = (df.CenterIndex==0) & (df[k].abs()<1000)
    plt.plot(df.LaserWavelength[w],df[w][k], ".-", label=k)
plt.legend()
plt.show()
