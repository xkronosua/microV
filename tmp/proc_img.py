import exdir
import numpy as np
import matplotlib.pyplot as plt

# import pandas as pd
from scipy.signal import medfilt2d, medfilt
from numpy.lib.stride_tricks import sliding_window_view
import time
from numba import jit

store = exdir.File("LN219_TUN800nm.exdir", "r")

d3d = store["scan3D"][max(store["scan3D"])]["data"]

intens_ = d3d["data"]["AndorCamera"]["raw"]["intensity"][0, :, :, 0, :]


w = np.where(intens_.sum(axis=2) == intens_.sum(axis=2).min())
t0 = time.time()
intens = np.apply_along_axis(medfilt, 2, (intens_[:, :] - intens_[w].mean(axis=0)), 5)
print(time.time() - t0)

wl = d3d["data"]["AndorCamera"]["raw"]["wavelength"][0, :, :, 0, :]
X = d3d["metadata"]["X"][0, :, :, 0] - d3d["metadata"]["X"][0, 0, 0, 0]
Y = d3d["metadata"]["Y"][0, :, :, 0] - d3d["metadata"]["Y"][0, 0, 0, 0]


ex_wl1 = 1045
ex_wl2 = 800
w1 = 1 / ex_wl1
w2 = 1 / ex_wl2

centers = {"2w2": 2 * w2, "525": 1 / 525, "550": 1 / 550}
window = 10
fig, ax_ = plt.subplots(2, 2, sharex=True, sharey=True)
ax = ax_.flatten()
imgs = []
maxmax = 0
for i in range(len(centers)):
    k, c = list(centers.items())[i]
    c = 1 / c
    print(k, c)
    w = wl.all(axis=(0, 2))
    wl1 = wl[:, w, :]
    img_ = intens[:, w, :]
    img = img_
    img[img > 100] = 0

    X1 = X[:, w]
    Y1 = Y[:, w]

    mask = (wl1 > c - window / 2) & (wl1 < c + window / 2)
    mask = np.all(mask, axis=(0, 1))
    img = img_[:, :, mask].sum(axis=2) - img_[:, :, :200].mean(
        axis=2
    )  # np.sum(img[:,:,mask]- img_[:,:,:200].mean(),axis=2)
    imgs.append(img)

    ax[i].contourf(X1, Y1, img, 500, cmap="turbo")
    ax[i].set_title(k)
    ax[i].set_aspect(1)

ax[3].contourf(X1, Y1, (imgs[-2] + imgs[-1]), 500, cmap="turbo")
ax[3].set_title("525+550")
ax[3].set_aspect(1)

fig, ax = plt.subplots(1, 2)
x = imgs[-1].flatten()
x1 = imgs[-2].flatten()
y = imgs[0].flatten()

threshold = 8
w = (x > threshold) & (y > threshold)

ax[0].plot(x, y, "xg", markersize=2, alpha=0.5, label="550")
ax[0].plot(x1, y, "+y", markersize=2, alpha=0.5, label="525")
ax[1].plot(x[w], y[w], "xg", markersize=2, alpha=0.5, label="_")

leq = np.poly1d(np.polyfit(np.log10(x[w]), np.log10(y[w]), 1))  # ,w=(x>8)&(y>7)))
leq0 = np.linalg.lstsq(np.transpose([np.log10(x[w])]), np.log10(y[w]))[0][0]
ax[1].plot(x[w], 10 ** leq(np.log10(x[w])), "-r", label=f"Slope={leq[1]:.2f}")
ax[1].plot(x[w], 10 ** (leq0 * np.log10(x[w])), "-b", label=f"Slope={leq0:.2f}")
ax[1].legend()
ax[1].set_xscale("log")
ax[1].set_yscale("log")
# ax[0].set_aspect(1)
ax[1].set_aspect(1)

ax[1].set_xlabel("PL @550")
ax[1].legend()
ax[0].legend()
ax[1].set_ylabel("SHG @400nm")
ax[0].set_xlabel("PL")
ax[0].set_ylabel("SHG @400nm")

plt.show()
