from LightPipes import *
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

# img=mpimg.imread('bessel_axicon.png')
# plt.imshow(img); plt.axis('off');plt.title('Bessel beam with axicon')
# plt.show()

wavelength = 1045.0 * nm
size = 15 * mm
N = 128

N2 = int(N / 2)
ZoomFactor = 10
NZ = N2 / ZoomFactor

alpha = 2  # deg
phi = np.deg2rad(180 - alpha * 2)
n1 = 1.5
phi1 = np.deg2rad(360 - (180 - alpha * 2))
n1 = 1.5
z_start = 0.001 * cm
z_end = 150 * cm
steps = 5
delta_z = (z_end - z_start) / steps
z = z_start

F = Begin(size, wavelength, N)
F = GaussBeam(F, 2 * mm)
F = Axicon(phi, n1, 0, 0, F)
F = Forward(F, 69 * cm, size, N)
F = Axicon(F, phi1, n1, 0, 0)

delta_z = 70 * cm
for i in [0]:  # range(1,steps):
    print(i)
    F = Fresnel(delta_z, F)
    I = Intensity(0, F)
    plt.subplot(2, 5, i)
    s = "z= %3.1f m" % (z / m)
    plt.title(s)
    plt.imshow(I, cmap="jet")
    plt.axis("off")
    plt.axis([N2 - NZ, N2 + NZ, N2 - NZ, N2 + NZ])
    z = z + delta_z
plt.show()
