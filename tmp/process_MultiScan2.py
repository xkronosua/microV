import exdir
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
import pandas as pd
import sys
import argparse
import numpy_indexed as npi
from scipy.interpolate import interp1d
from lmfit.models import (
    GaussianModel,
    ConstantModel,
    ExponentialModel,
    SkewedGaussianModel,
)
import traceback

from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg


parser = argparse.ArgumentParser(description="Process data for multiscan measurements.")
parser.add_argument("-f", dest="file", help="Exdir to process")
parser.add_argument(
    "-s",
    dest="scan_index",
    help="Select scan by index.",
    nargs="+",
    type=int,
    default=None,
)
parser.add_argument(
    "--PL", dest="PL_bands", help="PL_Bands to fit.", nargs="+", type=int, default=None
)
parser.add_argument("-c", dest="config", help="Fit all or 0delay.", default="sync")

args = parser.parse_args()
print(args)

store = exdir.File(args.file, "r")
keys = list(store["MultiScan"].keys())
keys.sort()
if not args.scan_index is None:
    timestamps = [keys[i] for i in args.scan_index]
    dd = []
    for i in args.scan_index:
        tmp = store["MultiScan"][keys[i]]["data"]
        dd.append(tmp[tmp["time"] != 0])
    data_ = tmp
    data = np.hstack(dd)
else:
    timestamp = keys[-1]
    data_ = store["MultiScan"][max(store["MultiScan"])]["data"]
    data = data_[data_["time"] != 0]

data = data[data["position"]["LaserWavelength"] != 0]

scan_axes = data["position"].dtype.names


N = len(np.unique(data["position"]["LaserWavelength"]))
# fig, axes_ = plt.subplots(3,int(np.ceil(N//3))+1,sharex=True,sharey=True)
# axes = axes_.flatten()


def centers(ex_wl, bands=args.PL_bands):
    bands_ = {
        "2w1": 1045 / 2,
        "2w2": ex_wl / 2,
        "3w1": 1045 / 3,
        "3w2": ex_wl / 3,
        "w1+w2": 1 / (1 / 1045 + 1 / ex_wl),
        #'2w1-w2': 1/(2/1045-1/ex_wl),
        "2w2-w1": 1 / (-1 / 1045 + 2 / ex_wl),
        "2w1+w2": 1 / (2 / 1045 + 1 / ex_wl),
        "2w2+w1": 1 / (1 / 1045 + 2 / ex_wl),
    }
    if not bands is None:
        for b in bands:
            bands_[f"PL{b}"] = b
    return bands_


filters = data_.attrs["filters"].to_dict()
info = data_.attrs["centerIndex_info"].to_dict()


def fit_data(wl, intens, ex_wl, centers, filter_index, sync=False, plot=False):
    x = wl.astype(float)
    intens_max = np.ptp(intens)
    y = (intens / intens_max).astype(float)
    iu = interp1d(x, y, bounds_error=False, fill_value="extrapolate")
    model = ConstantModel(prefix="bg_")
    params = model.make_params()
    if ex_wl <= 725:
        params["bg_c"].set(y.min(), min=-0.1, max=0.1, vary=False)
    else:
        params["bg_c"].set(y[:10].mean(), min=-0.1, max=0.1, vary=False)
    for k in centers:
        y_init = iu(centers[k])
        if "PL" in k:
            pref = k + "_"
            g_model = SkewedGaussianModel(prefix=pref)
        else:
            pref = f"g{k}_".replace("+", "p").replace("-", "m")
            g_model = GaussianModel(prefix=pref)
        model += g_model
        params += g_model.make_params()
        vary = True
        if (k in ["sfg", "2w2+w1", "2w1+w2", "2w2-w1"]) and not sync:
            vary = False
            y_init = 0
        if (k in ["3w2", "2w2+w1", "2w1+w2"]) and (ex_wl < 800):
            vary = False
            y_init = 0
        if ((k in ["3w1", "2w2+w1", "2w1+w2"]) and (ex_wl < 800)) and filter_index == 2:
            vary = False
            y_init = 0
        if y_init <= 1e-10:
            vary = False
        params[pref + "center"].set(
            centers[k], min=centers[k] - 2, max=centers[k] + 2, vary=vary
        )
        params[pref + "sigma"].set(1.6, min=1, max=2.5, vary=vary)
        params[pref + "amplitude"].set(y_init * 4, min=y_init / 2, max=10, vary=vary)
        if "PL" in k:
            params[pref + "sigma"].set(10, min=1, max=100, vary=False)

            params[pref + "gamma"].set(4, min=0, max=100, vary=False)
            params[pref + "center"].set(centers[k], vary=False)

    out = model.fit(y, x=x, params=params)
    print(out.fit_report(min_correl=1))
    if plot:
        plt.plot(x, y, ".")
        plt.plot(x, out.best_fit, "-")
    res = {}
    for k in centers:
        if "PL" in k:
            res[k] = out.params[k + "_amplitude"].value * intens_max
        else:
            res[k] = (
                out.params[
                    "g" + k.replace("+", "p").replace("-", "m") + "_amplitude"
                ].value
                * intens_max
            )
    return res, (x, out.best_fit * intens_max, out, model)


res = []
colors = "rgbcmkyy" * 2

lamp_data = pd.read_csv("AndorCameraLampCalibr_Microscope_forward.csv", index_col=0)
lamp_correction = {
    k: interp1d(
        lamp_data["wl"], lamp_data[k], bounds_error=False, fill_value="extrapolate"
    )
    for k in lamp_data.keys()[1:]
}


################################################################################

# QtGui.QApplication.setGraphicsSystem('raster')
app = pg.mkQApp()
mw = QtGui.QMainWindow()
mw.setWindowTitle("pyqtgraph example: PlotWidget")
mw.resize(800, 800)
cw = QtGui.QWidget()
mw.setCentralWidget(cw)
l = QtGui.QHBoxLayout()
cw.setLayout(l)

pw = pg.PlotWidget(
    name="Plot1"
)  ## giving the plots names allows us to link their axes together
l.addWidget(pw)

mw.show()

wl = np.linspace(330, 700, 1000)
init_y = np.zeros(1000)
lines = {}
pw.addLegend()
lines["data"] = pw.plot(
    wl,
    init_y,
    pen=(200, 200, 200),
    symbolSize=1,
    symbolBrush=(255, 0, 0),
    symbolPen="w",
)
lines["fit"] = pw.plot(wl, init_y, pen="r")
for i, k in enumerate(centers(1300)):
    lines[k] = pw.plot(wl, init_y, pen=i, style=QtCore.Qt.DashLine, name=k)

# plt.set_xlabel('Ex_wl, nm')

sliders = {}
for i, k in enumerate(data["position"].dtype.names):
    uv = np.unique(data["position"][k])
    step = uv.tolist()
    sliders[k] = QtGui.QSlider()
    sliders[k].setMaximum(len(uv))
    sliders[k].setMinimum(0)
    l.addWidget(sliders[k])
    print(k, uv)


fitted_data = {}
# The function to be called anytime a slider's value changes
def update(index):

    print(index)
    try:
        wl = np.linspace(330, 700, 1000)
        mask = (data["time"] != 0) & (data["position"]["LaserWavelength"] != 0)
        d = data[mask]
        mask = mask[mask]
        for i, k in enumerate(sliders):

            uv = np.unique(data["position"][k])
            sliders[k].setMaximum(len(uv))
            v = uv[sliders[k].value()]
            print(k, sliders[k].value(), v)

            diff = abs(d["position"][k] - v)
            mask = diff == diff.min()
            d = d[mask]
            mask = mask[mask]

            sliders[k].setToolTip(str(v))

            if k == "LaserWavelength":
                ex_wl = v
            if k == "filtersPiezoStage":
                filter = v

        d = d[0]
        if len(d) == 0:
            return
        print(d.shape)
        wl = d["data"]["AndorCamera"]["raw"]["wavelength"]
        exposure = d["data"]["AndorCamera"]["raw"]["exposure"]
        intens = (
            d["data"]["AndorCamera"]["raw"]["intensity"]
            / exposure
            / lamp_correction[filters[filter]](wl)
        )
        intens[np.isinf(intens)] = 0
        intens[np.isnan(intens)] = 0

        print(intens)
        args = str([wl, intens, ex_wl, centers(ex_wl), filter])
        if not args in fitted_data.keys():
            res, (x, y, out, model) = fit_data(
                wl, intens, ex_wl, centers(ex_wl), filter_index=filter
            )
            fitted_data[args] = res, (x, y, out, model)
        else:
            res, (x, y, out, model) = fitted_data[args]
        for k in centers(ex_wl):
            lines[k].setData([centers(ex_wl)[k]] * 2, [y.min(), y.max()])
        lines["data"].setData(wl, intens)
        lines["fit"].setData(x, y)
        # ax.set_ylim(intens.min(),intens.max())

    except:
        traceback.print_exc()


for k in sliders:
    # register the update function with each slider
    sliders[k].valueChanged[int].connect(update)

"""
res_scan = []
for i,d in enumerate(npi.group_by(data['position']['LaserWavelength']).split(data)):
	if len(d)<=1: continue
	#print(d['position']['LaserWavelength'])
	ex_wl = d['position']['LaserWavelength'].mean()
	#if ex_wl==700: continue
	for j,d1 in enumerate(npi.group_by(d['position']['filtersPiezoStage']).split(d)):
		if len(d1)<=1: continue
		filter_index = int(d1['metadata']['filtersPiezoStage'].mean())

		d_bg =  d1[d1['position']['CenterIndex']==info['Type'].index(b'BG')]
		wl_bg = d_bg['data']['AndorCamera']['raw']['wavelength']
		exposure_bg = d_bg['data']['AndorCamera']['raw']['exposure']
		intens_bg = (d_bg['data']['AndorCamera']['raw']['intensity'].T/exposure_bg).T

		try:
			fit_out_bg, _ = fit_data(wl_bg[0], intens_bg[0], ex_wl, centers(ex_wl),filter_index=filter_index)
			w = np.where(intens_bg==intens_bg.max())[0][0]
			fit_out_max_bg, _ = fit_data(wl_bg[w], intens_bg[w], ex_wl, centers(ex_wl),filter_index=filter_index,sync=True)
		except:
			continue
		#print(d1['position']['filtersPiezoStage'])
		for kk,d2 in enumerate(npi.group_by(d1['position']['CenterIndex']).split(d1)):
			#print(d2['position']['CenterIndex'])

			centerIndex = int(d2['position']['CenterIndex'].mean())
			print(ex_wl, centerIndex, filter_index)
			if info['Type'][centerIndex] == b'BG': continue
			corr_curve = lamp_correction[filters[filter_index]]

			delay = d2['position']['Delay_line_position_zero_relative'].copy()
			delay[abs(delay)>0.2]=np.nan

			wl = d2['data']['AndorCamera']['raw']['wavelength']

			exposure = d2['data']['AndorCamera']['raw']['exposure']
			intens_ = (d2['data']['AndorCamera']['raw']['intensity'].T/exposure).T

			# if intens_.shape==intens_bg.shape:
			# 	intens = (intens_-intens_bg)/corr_curve(wl)
			# else:
			# 	continue#intens = (intens_-intens_bg[0])/corr_curve(wl)
			intens = intens_
			# if centerIndex == 0:
			# 	Delay = np.repeat(delay.reshape(delay.shape[0],1),wl.shape[1],axis=1)
			# 	im=axes[i].contourf(Delay,wl,np.log10(abs(intens)),100, alpha=0.5)#,label=f'{pos}')
			# 	ims.append(im)
			# 	axes[i].set_title(str(ex_wl))
			# 	vmin_,vmax_ = im.get_clim()
			# 	if vmax_>vmax: vmax = vmax_
			# 	if vmin_<vmin: vmin = vmin_
			tmp = {'ex_wl':ex_wl,'centerIndex':centerIndex, 'filter_index':filter_index}
			tmp1 = {
				'ex_wl':np.repeat(ex_wl, len(delay)),
				'centerIndex':d2['position']['CenterIndex'],
				'delay': delay,
				'filter_index':np.repeat(filter_index, len(delay)),
				'sfg': d2['data']['AndorCamera']['data']['w1+w2']/exposure
				}

			cent = centers(ex_wl)
			# for m,key in enumerate(cent):
			# 	window = 20
			# 	mask = (wl>cent[key]-window/2) & (wl<cent[key]+window/2)
			# 	if mask.sum()<=2: continue
			# 	#wl_tmp = wl[mask].reshape(wl.shape[0],-1).mean(axis=1)
			# 	sig = intens[mask].reshape(wl.shape[0],-1).mean(axis=1)
			# 	mask_bg = (wl_bg>cent[key]-window/2) & (wl_bg<cent[key]+window/2)
			# 	if mask_bg.sum()<=2: continue
			# 	sig_bg = intens_bg[mask_bg].reshape(wl_bg.shape[0],-1).mean(axis=1)
			# 	try:
			# 		sig = sig - sig_bg
			# 	except:
			# 		continue
			# 	tmp1[key] = sig
			# 	label = f'NP{centerIndex}:'+info['Type'][centerIndex].decode()
			# 	axes1[m][i].plot(delay,sig,colors[int(centerIndex)],label=label,linewidth=filter_index)
			# 	axes1[m][i].set_title(f'{ex_wl}')
			# 	axes1[m][i].grid(1)
			#
			# 	if key in ['2w1','2w2','3w2','3w1']:
			# 		tmp[key] = sig[:3].mean()
			# 	else:
			# 		tmp[key] = sig.max() - sig.min()
			print('FIT_DELAY:')
			label = f'NP{centerIndex}:'+info['Type'][centerIndex].decode()
			fit_out,data_plot = fit_data(wl[0], intens[0], ex_wl, centers(ex_wl),filter_index=filter_index,)
			w = np.where(intens==intens.max())[0][0]
			print('FIT_SYNC:')
			fit_out_max,data_plot_max = fit_data(wl[w], intens[w], ex_wl, centers(ex_wl),filter_index=filter_index,sync=True)

			axes1[centerIndex][i].plot(wl[0], intens[0],'.'+colors[0])
			axes1[centerIndex][i].plot(wl[w],intens[w],'.'+colors[1])

			axes1[centerIndex][i].plot(data_plot[0],data_plot[1],colors[0],linewidth=filter_index)
			axes1[centerIndex][i].plot(data_plot_max[0],data_plot_max[1],colors[1],linewidth=filter_index)
			axes1[centerIndex][i].set_title(f'{ex_wl}:NP{centerIndex}')
			axes1[centerIndex][i].grid(1)


			for m, key in enumerate(centers(ex_wl)):
				if key in ['2w1','2w2','3w2','3w1']:
					tmp[key] = fit_out[key] - fit_out_bg[key]
				else:
					tmp[key] = fit_out_max[key] - fit_out_max_bg[key]
			NN = len(wl)
			# for nn in range(len(wl)):
			# 	sync = (nn>(w-5))&(nn<(w+5))
			# 	fit_out,data_plot = fit_data(wl[i], intens[i], ex_wl, centers(ex_wl),filter_index=filter_index,sync=sync)
			# 	for m, key in enumerate(centers(ex_wl)):
			# 		tmp1[key] = fit_out[key] - fit_out_bg[key]
			#
			#
			res_scan.append(tmp1)
			#if info['Type'][centerIndex] == b'BG': continue

			res.append(tmp)

axes[0].set_xlim(-0.1,0.1)
axes[0].set_ylim(data['data']['AndorCamera']['raw']['wavelength'].min(),data['data']['AndorCamera']['raw']['wavelength'].max())

for im in ims:
	im.set_clim(vmin, vmax)





df = pd.DataFrame(res)

df[abs(df)==np.inf] =np.nan
[(a,b),(c,d)]=list(df.groupby('filter_index'))

r = pd.merge(b,d[['ex_wl','centerIndex','2w1','2w2','sfg']],on=['ex_wl','centerIndex'],how='left')
r.loc[r['2w2_y'].isna(),'2w2_y']=r.loc[r['2w2_y'].isna(),'2w2_x']
r.loc[r['2w1_y'].isna(),'2w1_y']=r.loc[r['2w1_y'].isna(),'2w1_x']
r.loc[r['sfg_y'].isna(),'sfg_y']=r.loc[r['sfg_y'].isna(),'sfg_x']
df_merged = r.rename(columns={'2w2_y':'2w2','2w1_y':'2w1','sfg_y':'sfg'})
fig3,axes3 = plt.subplots(4,1,sharex=True)

for i,g in df.groupby(by='centerIndex'):
	label = f'NP{i}_{info["Type"][i]}'
	for j, g1 in g.groupby(by='filter_index'):
		w = (abs(g1.ex_wl-1050)<100).values
		axes3[0].plot(g1['ex_wl'],g1['2w1']/g1[w]['2w1'].mean(),'-'+colors[i],label=label, linewidth=j)
		axes3[1].plot(g1['ex_wl'],g1['2w2']/g1[w]['2w2'].mean(),'--'+colors[i],label=label, linewidth=j)
		axes3[2].plot(g1['ex_wl'],g1['sfg']/g1[w]['sfg'].mean(),'-.'+colors[i],label=label, linewidth=j)
		axes3[3].plot(g1['ex_wl'],g1['sfg']/g1['2w1'],'-'+colors[i],label=label, linewidth=j)
		axes3[3].plot(g1['ex_wl'],g1['sfg']/g1['2w2'],'--'+colors[i], label=label, linewidth=j)
		label = '_'+label


df_merged.to_csv(args.file.replace('.exdir','_merged.csv').replace('/',''))

# TODO: norm for each
g1 = df_merged.groupby(by='ex_wl', as_index=False).mean()
norm = []
for i,g in df_merged.groupby(by='centerIndex', as_index=False):
	w = (abs(g.ex_wl-1050)<100).values
	g_tmp = g.copy()
	g_tmp.loc[:,g.keys()[3:]]/= g[w][g.keys()[3:]].mean()
	norm.append(g_tmp)
g1 = pd.concat(norm)
df_norm_avg = g1.groupby(by='ex_wl',as_index=False).mean()
axes3[0].plot(df_norm_avg['ex_wl'],df_norm_avg['2w1'],'o-',label=label, linewidth=3, alpha=0.5)
axes3[1].plot(df_norm_avg['ex_wl'],df_norm_avg['2w2'],'o-',label=label, linewidth=3, alpha=0.5)
axes3[2].plot(df_norm_avg['ex_wl'],df_norm_avg['sfg'],'o-',label=label, linewidth=3, alpha=0.5)


axes3[0].set_ylabel('FIX:SH')
axes3[0].legend()
axes3[1].set_ylabel('TUN:SH')
axes3[2].set_ylabel('SFG')
axes3[3].set_ylabel('SFG/SH')
axes3[3].set_xlabel('Ex. wavelength, nm')
[axes3[i].grid(1) for i in range(len(axes3))]


fig4,axes4 = plt.subplots(2,2,sharex=True)

for i,g in df_merged.groupby(by='centerIndex'):
	label = f'NP{i}_{info["Type"][i]}'
	for j, g1 in g.groupby(by='filter_index'):
		w = (abs(g1.ex_wl-1050)<100).values
		axes4[0][0].plot(g1['ex_wl'],g1['3w1']/g1[w]['3w1'].mean(),'-'+colors[i], label=label, linewidth=j)
		axes4[0][1].plot(g1['ex_wl'],g1['3w2']/g1[w]['3w2'].mean(),'-'+colors[i], label=label, linewidth=j)
		axes4[1][0].plot(g1['ex_wl'],g1['2w1+w2']/g1[w]['2w1+w2'].mean(),'-'+colors[i], label=label, linewidth=j)
		axes4[1][1].plot(g1['ex_wl'],g1['2w2+w1']/g1[w]['2w2+w1'].mean(),'-'+colors[i], label=label, linewidth=j)
		# axes4[2][0].plot(g1['ex_wl'],g1['2w1-w2']/g1[w]['2w1-w2'].mean(),'-'+colors[i], label=label, linewidth=j)
		# axes4[2][1].plot(g1['ex_wl'],g1['2w2-w1']/g1[w]['2w2-w1'].mean(),'-'+colors[i], label=label, linewidth=j)
		label = '_'+label
axes4[0][0].legend()
axes4[0][0].set_ylabel('FIX:TH')
axes4[0][1].set_ylabel('TUN:TH')
axes4[1][0].set_ylabel('2w1+w2')
axes4[1][1].set_ylabel('2w2+w1')
# axes4[2][0].set_ylabel('2w1-w2')
# axes4[2][1].set_ylabel('2w2-w1')
axes4[-1][0].set_xlabel('Ex. wavelength, nm')
axes4[-1][1].set_xlabel('Ex. wavelength, nm')
[(axes4[i][0].grid(1),axes4[i][1].grid(1)) for i in range(len(axes4))]



axes4[0][0].plot(df_norm_avg['ex_wl'],df_norm_avg['3w1'])
axes4[0][1].plot(df_norm_avg['ex_wl'],df_norm_avg['3w2'])
axes4[1][0].plot(df_norm_avg['ex_wl'],df_norm_avg['2w1+w2'])
axes4[1][1].plot(df_norm_avg['ex_wl'],df_norm_avg['2w2+w1'])
# axes4[2][0].plot(df_norm_avg['ex_wl'],df_norm_avg['2w1-w2'])
# axes4[2][1].plot(df_norm_avg['ex_wl'],df_norm_avg['2w2-w1'])



df1 = pd.concat([pd.DataFrame(r) for r in res_scan])
df1.to_csv(args.file.replace('.exdir','.csv').replace('/',''))

df_norm_avg.to_csv(args.file.replace('.exdir','_norm_avg.csv').replace('/',''))



fig.suptitle(args.file.split('.exdir')[0])
#fig1.suptitle(args.file.split('.exdir')[0])
#fig2.suptitle(args.file.split('.exdir')[0])
fig3.suptitle(args.file.split('.exdir')[0])
fig4.suptitle(args.file.split('.exdir')[0])
#fig5.suptitle(args.file.split('.exdir')[0])
"""
plt.show()
