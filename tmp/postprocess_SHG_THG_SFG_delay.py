import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter, medfilt
import time
import numpy_indexed as npi
import argparse
import pandas as pd
from lmfit.models import GaussianModel, ExponentialGaussianModel, ConstantModel, ExponentialModel
from pathlib import Path
from matplotlib import cm
import matplotlib

from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description="Process data for multiscan measurements.")
parser.add_argument("-f", dest="file", help="CSV file from preprocessing")

parser.add_argument(
    "--sync", dest="beams_sync", help="Measurements with delay.", action="store_true"
)

parser.add_argument(
    "--center", dest="CenterIndex", help="Center indexes to process.", nargs="+",
    type=int, default=None
)

parser.add_argument(
    "-N", dest="norm", help="Normalize at wavelength.", type=int, default=1045
)

parser.add_argument(
    "--normBy", dest="normBy", help="Normalize by reference.", type=str, default=None
)

parser.add_argument(
    "--fit2level", dest="fit2level", help="Fit SHG by two-level model.", action="store_true"
)


def chi2_SHG(ex_wl, Lambda_eg_nm=286, chi2_0=1,bg=0):
    c = 3e8 # m/s
    w = 2*np.pi*c/(ex_wl*1e-9) # Hz
    w_eg = 2*np.pi*c/(Lambda_eg_nm*1e-9) # Hz
    chi2_ = bg+chi2_0*w_eg**4/(w_eg**2-w**2)/(w_eg**2-4*w**2)
    return chi2_

def chi2_SFG(ex_wl, Lambda_eg_nm=286, chi2_0=1, bg=0):
    c = 3e8 # m/s
    w2 = 2*np.pi*c/(ex_wl*1e-9) # Hz
    w1 = 2*np.pi*c/1045e-9 # Hz
    w = (w1+w2)/2
    w_eg = 2*np.pi*c/(Lambda_eg_nm*1e-9) # Hz
    chi2_ = bg+chi2_0*w_eg**4/(w_eg**2-w**2)/(w_eg**2-4*w**2)
    return chi2_


args = parser.parse_args()
print(args)

df = pd.read_csv(args.file)

plt.scatter(df.time, df.LaserShutter,c=df.LaserShutter)
plt.plot(df.time, df.LaserShutter)

for band in ['3w1','3w2']:
    if band in df:
        #df.loc[df['FILTER']=='FESH0700',band] = np.nan
        df.loc[df['FILTER']=='FESH0850',band] = np.nan

# for band in ['3w2', '2w2', '2w1', '3w1', '2w2+w1', '2w1+w2','w1+w2']:
#     if not band in df:
#         continue
#     df.loc[abs(df['Delay_line_position_zero_relative'])>0.03, band] = np.nan

#df.loc[(df['FILTER']=='FGB37x2_EO84-712') & (df['LaserWavelength']>=1200), '2w2'] = np.nan
PL_bands = {}
for k in df.keys():
    if k[:2].split('_')[0]=="PL":
        PL_bands[k.split('_')[0]] = int(k.split('_')[0][2:])


for k in PL_bands:
    df.loc[(df['FILTER']=='FESH0850'),k] = np.nan
    df.loc[(df['FILTER']=='FESH0700'),k] = np.nan



if args.beams_sync:
    df_tmp = df.copy()

    mask = abs(df['Delay_line_position_zero_relative'])<0.05

    for band in PL_bands:
        df_tmp[band+'_delay'] = np.nan
        df_tmp.loc[mask, band+'_delay'] = df.loc[mask,band]
        df_tmp.loc[mask, band] = np.nan#df.loc[mask,band]


    for band in ['2w1', '2w2', '3w1', '3w2']:
        if band in df:

            df_tmp.loc[mask, band] = np.nan
    for band in ['2w2+w1', '2w1+w2','w1+w2','2w2-w1']:
        if band in df:
            df_tmp.loc[~mask, band] = np.nan
    df = df_tmp
    df = df[df['LaserShutter']==3]
    #df_tmp = df[abs(df['Delay_line_position_zero_relative'])<0.01]
else:
    df['Delay_line_position_zero_relative'] = df['Delay_line_position_zero_relative'].round(2)
nonBG_mask = df.CenterType!='BG'
index2type = dict(zip(df.CenterIndex,df.CenterType))

#df1=df.copy()
df1 = df[nonBG_mask].groupby(['LaserWavelength','LaserShutter','filtersPiezoStage','CenterIndex','Delay_line_position_zero_relative'],as_index=False).mean()
df1['CenterType'] = [index2type[index] for index in df1.CenterIndex]
for i, data_row in df[df['CenterType']=='BG'].groupby(['LaserWavelength','LaserShutter','filtersPiezoStage','Delay_line_position_zero_relative'],as_index=False).mean().iterrows():
    ex_wl = data_row.LaserWavelength
    filterIndex = data_row.filtersPiezoStage
    shutter = data_row.LaserShutter
    delay = data_row.Delay_line_position_zero_relative
    for band in ['3w2', '2w2', '2w1', '3w1', '2w2+w1', '2w1+w2','w1+w2','2w2-w1']+list(PL_bands):
        if not band in df1: continue
        bg = data_row[band]
        if np.isnan(bg):
            bg = 0

        mask = (df1.LaserWavelength==ex_wl) & (df1.filtersPiezoStage==filterIndex) & (df1.LaserShutter==shutter) & (df1.Delay_line_position_zero_relative==delay)
        if not (band+'_rmBG') in df1:
            df1[band+'_rmBG'] = df1[band]
        df1.loc[mask, band+'_rmBG'] -= bg
        if not (band+'_rmnormBG') in df1:
            df1[band+'_rmnormBG'] = df1[band+'_rmBG']
        df1.loc[mask, band+'_rmnormBG'] = df1.loc[mask, band+'_rmBG']
        df1.loc[mask, band+'_rmnormBG'] /= bg

        # if band in ['3w2', '3w1', '2w2+w1', '2w1+w2','2w2-w1']:
        #     df1[band+'_normBG'] = df1[band]
        #     #df1.loc[mask, band+"_normBG"] = df1[band][mask]
        #     df1.loc[mask, band+"_normBG"] /= bg
#fig, axes = plt.subplots(1,3, sharex=True)

fig0, ax0 = plt.subplots(3,1, sharex=True)
axes=ax0
# fig1, ax1 = plt.subplots(1,1)
# fig2, ax2 = plt.subplots(1,1)
#
# axes = [ax0,ax1,ax2]

norm_color = matplotlib.colors.Normalize(vmin=0, vmax=df1.CenterIndex.max())

avg_norm = {'ex_wl': np.arange(700,1305,5)}

if not args.normBy is None:
    reference = pd.read_csv(Path(args.normBy))

df1.to_csv(args.file.replace('.csv','_rmBG.csv'))


norm_at = np.linspace(args.norm-25,args.norm+25,5)
N = 0
for j,g in df1.groupby('CenterIndex'):
    centerType = g['CenterType'].iloc[0]
    centerIndex = int(g['CenterIndex'].mean())
    if not args.CenterIndex is None:
        if not centerIndex in args.CenterIndex:
            continue

    #if np.all(g['CenterType']=='BG'): continue
    fig_tmp1,ax_tmp1 = plt.subplots(1,1)
    fig_tmp2,ax_tmp2 = plt.subplots(1,1)


    for i,band_ in enumerate(['3w2', '2w2', '2w1', '3w1', '2w2+w1', '2w1+w2','w1+w2','2w2-w1']+list(PL_bands)):
        #df1 = df1[df1['2w2']<1000]
        band = band_+'_rmBG'
        if not band in g:
            print(N, band, "SKIP")
            continue
        isna = g[band].isna()
        x = g.LaserWavelength[~isna]
        y = medfilt(g[band][~isna],3)
        #y = g[band][~isna]
        if len(x) == 0:
            print(N, band, "SKIP (no data)",g[band])
            continue
        mask = abs(x-1045)<25
        if band_ in ['2w1','2w2','w1+w2','2w2-w1']:
            iu_ = interp1d(x[~mask],y[~mask])
            y[mask] = iu_(x[mask])

        iu = interp1d(x,y,fill_value=np.nan,bounds_error=False)

        norm = 10**(np.log10(iu(norm_at)).mean())#iu(np.linspace(1025,1075,5)).mean()
        rgba_color = cm.turbo(norm_color(centerIndex  ))
        if args.normBy is None:
            if band_ == "2w2":
                axes[0].plot(x, y/norm, '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
            #elif band_ == "3w2":
            #    axes[1].semilogy(x, y, '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
            elif band_ == "w1+w2":
                axes[2].plot(x, y/norm, '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
        else:

            if band_ == "2w2":
                #I2w_model_LN = (chi2_SHG(reference['ex_wl'])/chi2_SHG(norm_at).mean())**2

                ref = interp1d(reference['ex_wl'],
                    reference[band],#/I2w_model_LN,
                    fill_value=np.nan,bounds_error=False)
                axes[0].plot(x, y/norm/ref(x), '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
            elif band_ == "w1+w2":
                #Iw1pw2_model_LN = (chi2_SFG(reference['ex_wl'])/chi2_SFG(norm_at).mean())**2
                ref = interp1d(reference['ex_wl'],
                    reference[band],#/Iw1pw2_model_LN,
                    fill_value=np.nan,bounds_error=False)
                axes[2].plot(x, y/norm/ref(x), '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
        ax_tmp1.semilogy(x, y, '-', marker=i,  label=band)
        ax_tmp1.set_title(f"NP{centerIndex}:{centerType}")

        if band_ in ['3w2', '3w1', '2w2+w1', '2w1+w2','2w2-w1']:
            isna = g[band_+"_rmnormBG"].isna() | (abs(g[band_+"_rmnormBG"])>10)
            x_ = g.LaserWavelength[~isna]
            y_ = g[band_+"_rmnormBG"][~isna]
            print(x_, y_)
            if band_ == "3w2":
                axes[1].plot(x_, y_, '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
            ax_tmp2.plot(x_, y_, '-', marker=i,  label=band)
            ax_tmp2.set_title(f"NP{centerIndex}:{centerType} (norm BG)")

        if not band in avg_norm:
            avg_norm[band] = 0
        avg_norm[band] += iu(avg_norm["ex_wl"])/norm
        print(N,band)
    N+=1


    ax_tmp2.legend()
    ax_tmp2.grid(True, which="both", ls="-")
    ax_tmp2.set_xlabel('Ex. wavelength, nm')
    ax_tmp2.set_ylabel('Intensity, arb. un.')
    ax_tmp1.legend()
    ax_tmp1.grid(True, which="both", ls="-")
    ax_tmp1.set_xlabel('Ex. wavelength, nm')
    ax_tmp1.set_ylabel('Intensity, arb. un.')

avg_norm = pd.DataFrame(avg_norm)
avg_norm[list(avg_norm.keys())[1:]]/=N
print(avg_norm.keys())
# axes[0].plot(avg_norm['ex_wl'], avg_norm['2w2'+"_rmBG"],'--r', lw=3)
if args.fit2level:
    bg = 0
    #axes[0].plot(avg_norm['ex_wl'], (ref(avg_norm['ex_wl'])/ref(norm_at).mean())**2,'--g', lw=3)
    axes[0].plot(avg_norm['ex_wl'], avg_norm['2w2_rmBG'],'--g', lw=3)
    axes[0].plot(avg_norm['ex_wl'], (chi2_SHG(avg_norm['ex_wl'],bg=bg)/chi2_SHG(norm_at,bg=bg).mean())**2,'--r')

    axes[2].plot(avg_norm['ex_wl'], avg_norm['2w2_rmBG']/(chi2_SHG(avg_norm['ex_wl'],bg=bg)/chi2_SHG(norm_at,bg=bg).mean())**2,'.b', lw=3)
    #axes[2].plot(avg_norm['ex_wl'], (ref(avg_norm['ex_wl'])/ref(norm_at).mean())**2,'--g', lw=3)
    #axes[2].plot(avg_norm['ex_wl'], (ref(avg_norm['ex_wl'])/ref(norm_at).mean())**2,'--g', lw=3)


# if not 'w1+w2' in df1:
#     fig0.delaxes(axes[2])
#     axes[0].change_geometry(2,1,1)
#     axes[1].change_geometry(2,1,2)
#     #axes[2].plot(avg_norm['ex_wl'], avg_norm['w1+w2'+"_rmBG"],'--r', lw=3)
# if not '3w2' in df1:
#     fig0.delaxes(axes[1])
#     axes[0].change_geometry(1,1,1)

    #axes[1].plot(avg_norm['ex_wl'], avg_norm['3w2'+"_rmnormBG"],'--')

avg_norm.to_csv(args.file.replace('.csv','_avg.csv'))

axes[0].grid(True, which="both", ls="-")
axes[1].grid(True, which="both", ls="-")
axes[2].grid(True, which="both", ls="-")
axes[2].set_xlabel('Ex. wavelength, nm')
axes[0].set_ylabel('2w2')
axes[1].set_ylabel('3w2')
axes[2].set_ylabel('w1+w2')
axes[0].legend()
axes[1].legend()
axes[2].legend()

plt.show()
