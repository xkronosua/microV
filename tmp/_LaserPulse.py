import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

sech = lambda x: 1 / np.cosh(x)


def I(t, tau):
    return sech(1.7627 * t / tau) ** 2


def func(t, tau, t_p=100):
    return abs((I(t, t_p) ** 0.5 + I(t - tau, t_p) ** 0.5) ** 2) ** 2


def func(t, tau, t_p=100):
    return abs((np.exp(-((t / t_p) ** 2)) + np.exp(-((t - tau) / t_p))) ** 2) ** 2


t = np.linspace(-1000, 1000, 10000)


def IAC(x, wc, tau_p):
    tau_ = x
    res = (
        1
        + (2 + np.cos(2 * wc * tau_))
        * 3
        * ((tau_ / tau_p) * np.cosh(tau_ / tau_p) - np.sinh(tau_ / tau_p))
        / np.sinh(tau_ / tau_p) ** 3
        + 3
        * (np.sinh(2 * tau_ / tau_p) - (2 * tau_ / tau_p))
        * np.cos(wc * tau_)
        / np.sinh(tau_ / tau_p) ** 3
    )
    # print(res)
    if np.isnan(res).any():
        print(A, wc, tau_p)
    # res = np.nan_to_num(res,1)
    return res


from lmfit import Model


data = np.loadtxt("test.dat")[200:-760]
x = (data[:, 0] * 2e-3 / 299792458) * 1e15
x -= x.mean()
y = data[:, 1] / data[:, 1].mean()
y -= y[0]
y /= y.max() / 6
y += 1.8

from scipy.signal import medfilt

y = medfilt(y, 9)
mod = Model(IAC)
params = mod.make_params(wc=100)
params["tau_p"].set(200, min=80, max=300)
# params['tau0'].set(0,min=x.min(),max=x.max())
# params['A'].set(y.max()*10000,min=0)#y.min()/200,max=y.max())
# params['A0'].set(y[0],min=y[0]-2000,max=y[0]+100)


result = mod.fit(y, params, x=x)
plt.plot(x, y, ".")

plt.plot(x, result.best_fit)

print(result.fit_report())
# Ic = IAC(t,1000,100)
# plt.plot(t,Ic)
plt.show()
