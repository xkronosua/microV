import sys

# sys.path.insert(0,'..\\hardware')
from hardware.cameras.atmcd import AndorAtmcd
from hardware.spectrometers.Shamrock import ShamRock_spectrometer
import microscope

shamrock = ShamRock_spectrometer()
shamrock.initialize()
shamrock.enable()
wavelength = shamrock.get_wavelength()
print(wavelength)
a = AndorAtmcd()
print(a.initialize())
print(a.get_id())
print(a.get_exposure_time())
print(a.get_sensor_temperature())
print(a.get_sensor_shape())
print(a.get_pixel_size())

size = a.get_pixel_size()
shape = a.get_sensor_shape()
shamrock.set_pixel_width(size[0])
shamrock.set_number_pixels(shape[0])


andorCCD_wavelength = shamrock.get_calibration()

print(a.enable())


print(a.set_trigger(microscope.TriggerType.SOFTWARE, microscope.TriggerMode.ONCE))
data = a.grab_next_data()
print(data, data[0].shape)
from pylab import *

figure()
imshow(data[0])
data = a.grab_next_data()
figure()
plot(andorCCD_wavelength, data[0].mean(axis=0))
show()
