import sys
from pathlib import Path
import numpy as np
import traceback
from glob import glob

if len(sys.argv)>1:
	path = glob(sys.argv[1])
else:
	path = ['.']
result = []
for p in path:
	result += list(Path(p).rglob("*.npy"))
print(result)

for p in result:
	try:
		print(p,end='')
		d = np.load(p)
		if d is None:
			print('\tSKIP')
			continue
		elif d.dtype is None:
			print('\tSKIP')
			continue
		elif d.dtype.names is None:
			print('\tSKIP')
			continue
		if 'time' in d.dtype.names:
			if (d['time']==0).sum()>0:
				d1 = d[d['time']!=0]
				np.save(p,d1)
				print('\tDONE')
			else:
				print('\tSKIP')
		else:
			print('\tSKIP')

	except KeyboardInterrupt:
		break
	except:
		print('\tERROR')
		traceback.print_exc()
