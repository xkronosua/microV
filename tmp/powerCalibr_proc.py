import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy import signal


path = sys.argv[1]

print(path)
store = exdir.File(path, "r")

keys = [k for k in store.keys() if k[0] != "_"]
keys.sort()
keys = keys[::-1]

data_ = []
dtype = store[keys[0]]["scan"].dtype


for k in keys:
    try:
        dd = store[k]["scan"]
        if dd.dtype != dtype:
            break
        w = dd["time"] != 0
        if sum(w) == 0:
            break
        data_.append(dd[w])
    except:
        traceback.print_exc()
        break
data = np.hstack(data_)


store_out = exdir.File("processed.exdir")

out_data = np.zeros(
    len(data), dtype=[("Ex_wavelength", "f8"), ("HWP_angle", "f8"), ("power", "f8")]
)

if not "HWP_IR_powerCalibr" in store_out:
    store_out.require_dataset(
        "HWP_IR_powerCalibr", dtype=data.dtype, shape=out_data.shape
    )

if not "HWP_powerCalibr" in store_out:
    store_out.require_dataset("HWP_powerCalibr", dtype=data.dtype, shape=out_data.shape)


ang_limits = []
for ex_wl in np.unique(data["LaserWavelength"]):
    w = data["LaserWavelength"] == ex_wl
    d = data[w]

    if "HWP_IR_Power" in d.dtype.names:
        x = d["HWP_IR_Power"]
    else:
        x = d["HWP_Power"]
    y = d["Powermeter"]["Power"]
    b, a = signal.butter(8, 0.125)
    yy = signal.filtfilt(b, a, y)
    out_data["Ex_wavelength"][w] = ex_wl
    out_data["HWP_angle"][w] = x
    out_data["power"][w] = yy
    ang_limits.append([x[yy == yy.min()][0], x[yy == yy.max()][0]])
    plt.plot(x, y, ".", label=f"_{ex_wl}")
    plt.plot(x, yy, "-", label=f"{ex_wl}")

ang_limits = np.array(ang_limits).mean(axis=0)

np.save(path.replace(".exdir", f"[{ang_limits[0]}_{ang_limits[1]}].npy"), out_data)

if "_fix" in path:
    # store_out['HWP_IR_powerCalibr'].data[:] = out_data
    store_out["HWP_IR_powerCalibr"].attrs[
        "power2angle_interpLimits"
    ] = ang_limits.tolist()
    np.save("processed.exdir/HWP_IR_powerCalibr/data.npy", out_data)
else:
    # store_out['HWP_powerCalibr'].data[:] = out_data
    store_out["HWP_powerCalibr"].attrs["power2angle_interpLimits"] = ang_limits.tolist()
    np.save("processed.exdir/HWP_powerCalibr/data.npy", out_data)
store_out.close()
del store_out
plt.legend()
plt.show()
