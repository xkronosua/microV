import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter, medfilt
import time
import numpy_indexed as npi
import argparse
import pandas as pd
from lmfit.models import GaussianModel, ExponentialGaussianModel, ConstantModel, ExponentialModel
from pathlib import Path
from tqdm import tqdm
from matplotlib.backends.backend_pdf import PdfPages
from shutil import copyfile

parser = argparse.ArgumentParser(description="Process data for multiscan measurements.")
parser.add_argument("-f", dest="file", help="Exdir to process")


parser.add_argument(
    "-t",
    dest="timestamps",
    nargs="+",
    type=str,
    help="""
					List of timestamps "data1234..." to process.
					"ALL" - to process each timestamp in "MultiScan" folder.
					Integer index to process by index in sorted list of timestamps.
					Last measured "-1" by default.
					""",
    default=["-1"],
)

parser.add_argument(
    "--range",
    dest="range",
    nargs="+",
    type=int,
    help='''
            Range of rows in binary dataset for processing:
                --range end
                --range start end
                --range start end step
            ''',
    default=None,
)

parser.add_argument(
    "--bands",
    dest="bands",
    nargs="+",
    type=str,
    help="""
					Bands to fit ['2w1', '2w2', '3w1', '3w2', 'w1+w2', '2w2-w1', '2w1+w2', '2w2+w1'].
					""",
    default=["ALL"],
)

parser.add_argument(
    "--bands_PL",
    dest="bands_PL",
    type=str,
    help="""
					Bands to fit PL:
                        --bands_PL center0,sigma0,gamma0;center1,sigma1,gamma1 ...
					""",
    default=None,
)

parser.add_argument(
    "--sync", dest="beams_sync", help="Measurements with delay.", action="store_true"
)

parser.add_argument(
    "--sensitivity", dest="sensitivity", help="Path to csv file with sensitivity curve.",
    default="AndorCameraCalibr_Microscope_forward_grating1.csv"
)

parser.add_argument(
    "--metadata", dest="get_metadata", help="Extract all metadata to final dataset.", action="store_true"
)

parser.add_argument(
    "--delay", dest="delay_positions", help="Process data only for delay specific positions.", type=float, nargs="+",
    default=[]
)


parser.add_argument(
    "--show", dest="show_plot", help="Show all graphs.", action="store_true"
)

parser.add_argument(
    "--indexing",
    dest="indexing",
    help="Defalt indexing of laser outputs [for processing data older than ~Sep1012 should be \"0213\"]. 0 - both closed, 1 - FIX, 2 - TUN, 3 - both opened",
    default="0123",
)



args = parser.parse_args()
print(args)

LaserShutterIndexes = {key:int(index) for key,index in zip(["CLOSED","FIX", "TUN", "OPENED"], args.indexing)}

def extractTimestamp(store, timestamps, dataType):
    timestamps_ = []
    keys = list(store[dataType])
    keys.sort()
    print(timestamps)

    if "ALL" in timestamps:
        timestamps_ = [k for k in store[dataType] if k[0] != "_"]
    else:
        for ts in timestamps:
            try:
                timestamps_.append(keys[int(ts)])
            except:
                traceback.print_exc()
                timestamps_.append(ts)
    print(timestamps_)
    sizes = [len(store[dataType][k]["data"]) for k in timestamps_]

    dtype = store[dataType][timestamps_[-1]]["data"].dtype

    data = np.zeros(np.sum(sizes), dtype=dtype)
    last_row = 0
    data_dict = {}

    for i, k in enumerate(timestamps_):
        # if store[dataType][k]['data'].dtype==dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        if w.sum() > 0:
            data_dict[k] = {}
            data_dict[k]['data'] = d
            data_dict[k]['attrs'] = store[dataType][k]["data"].attrs.to_dict()
    # data = data[data['time']!=0]

    return data_dict


def signaltonoise_dB(a, axis=0, ddof=0):
    a = np.asanyarray(a)
    m = a.mean(axis)
    sd = a.std(axis=axis, ddof=ddof)
    return 20*np.log10(abs(np.where(sd == 0, 0, m/sd)))


def bands_generator( bands="ALL"):

    bands_ = lambda ex_wl: {
        "2w1": 1045 / 2,
        "2w2": ex_wl / 2,
        "3w1": 1045 / 3,
        "3w2": ex_wl / 3,
        "w1+w2": 1 / (1 / 1045 + 1 / ex_wl),
        # '2w1-w2': 1/(2/1045-1/ex_wl),
        "2w2-w1": 1 / (-1 / 1045 + 2 / ex_wl),
        "2w1+w2": 1 / (2 / 1045 + 1 / ex_wl),
        "2w2+w1": 1 / (1 / 1045 + 2 / ex_wl),
    }
    if bands == "ALL" or bands == ["ALL"]:
        return bands_
    else:
        return lambda ex_wl: {key:bands_(ex_wl)[key] for key in bands}

def PL_bands_generator(input_str):
    if input_str is None: return {}
    if len(input_str)==0: return {}
    bands = {}
    list_str = input_str.split(";")
    for params in list_str:
        tmp = tuple([float(val.replace(" ","")) for val in params.split(',')])
        bands[f'PL{int(tmp[0])}'] = tmp
    return bands


def fit_data(
        data_row,
        bands_generator, sensitivity_corrector, filters_translator,
        PL_bands=None, center_info=None, beams_sync=True, get_metadata=False, plot=False):

    if data_row["time"]==0: return
    if "type" in data_row["data"]["AndorCamera"]["raw"].dtype.names:
        Type = data_row["data"]["AndorCamera"]["raw"]["type"]
        if Type == -1: return # baseline
    exposure = data_row["data"]["AndorCamera"]["raw"]["exposure"]
    intensity = data_row["data"]["AndorCamera"]["raw"]["intensity"].astype(float)
    wavelength = data_row["data"]["AndorCamera"]["raw"]["wavelength"].astype(float)



    if "filtersPiezoStage" in data_row["position"].dtype.names:
        filterIndex = data_row["position"]["filtersPiezoStage"]
    elif "filtersPiezoStage" in data_row["metadata"].dtype.names:
        filterIndex = data_row["metadata"]["filtersPiezoStage"]

    filterLabel = filters_translator[filterIndex]
    intensity_corrected = intensity/sensitivity_corrector[filterLabel](wavelength)/exposure
    intensity_corrected[intensity_corrected<-1e-4] = np.nan

    if "LaserWavelength" in data_row["position"].dtype.names:
        ex_wl = data_row["position"]["LaserWavelength"]
    elif "LaserWavelength" in data_row["metadata"].dtype.names:
        ex_wl = data_row["metadata"]["LaserWavelength"]
    #print("\n")
    if filterLabel == "FESH0850":
        wavelength[wavelength<440] = np.nan
        wavelength[wavelength>850] = np.nan
    if filterLabel == "FESH0700":
        wavelength[wavelength<390] = np.nan
        wavelength[wavelength>700] = np.nan
    #print(f'{filterLabel=}')
    true_data_mask = (~np.isnan(intensity_corrected)) & (~np.isnan(wavelength))


    y = medfilt(intensity_corrected[true_data_mask], 5)
    y = intensity_corrected[true_data_mask]

    x = wavelength[true_data_mask]
    if len(x) == 0:
        return

    interpolated_sig_init = interp1d(x, y, bounds_error=False, fill_value=0)

    clean_minimum = medfilt(y,151).min()

    intens_ptp = y.max()-clean_minimum
    #y = (y-clean_minimum)/np.ptp(y)
    y = (y)/np.ptp(y)

    interpolated_sig = interp1d(x, y, bounds_error=False, fill_value=0)

    bands = bands_generator(ex_wl)

    title = ""
    for name in data_row['position'].dtype.names:
        pos = data_row['position'][name].round(3)
        title+=f"{name}={pos:G},"
        if len(title)>150:
            title+="\n"
    #print(title)
    log_info = [title]
    model = ConstantModel(prefix="bg_")
    params = model.make_params()


    params["bg_c"].set(0, min=-0.1, max=+0.1, vary=False)


    for band in bands:
        center = bands[band]
        if center<x.min()*0.99 or center>x.max()*1.01:
            log_info.append(f'SKIP(out of range) {band} {center, (x.min()*0.99, x.max()*1.01)}')
            continue

        y_tmp=interpolated_sig(np.linspace(center - 2, center + 2, 100))
        y_init = abs(y_tmp).max()
        SN = signaltonoise_dB(y_tmp)

        pref = f"g{band}_".replace("+", "p").replace("-", "m")
        g_model = GaussianModel(prefix=pref)

        vary = True



        if "LaserShutter" in data_row['position'].dtype.names :
            if data_row['position']["LaserShutter"] != LaserShutterIndexes["OPENED"]: # BOTH OPENED
                beams_sync = False
                if data_row['position']["LaserShutter"] == LaserShutterIndexes["TUN"] and "w1" in band:
                    log_info.append(f'SKIP (shutter) {band}')
                if data_row['position']["LaserShutter"] == LaserShutterIndexes["FIX"] and "w2" in band:
                    log_info.append(f'SKIP (shutter) {band}')
        elif "LaserShutter" in data_row['metadata'].dtype.names:
            if data_row['metadata']["LaserShutter"] != LaserShutterIndexes["OPENED"]: # BOTH OPENED
                beams_sync = False
                if data_row['metadata']["LaserShutter"] == LaserShutterIndexes["TUN"] and "w1" in band:
                    log_info.append(f'SKIP (shutter) {band}')
                if data_row['metadata']["LaserShutter"] == LaserShutterIndexes["FIX"] and "w2" in band:
                    log_info.append(f'SKIP (shutter) {band}')


        if "Delay_line_position_zero_relative" in data_row['position'].dtype.names:
            if abs(data_row['position']["Delay_line_position_zero_relative"]) > 0.1: # BOTH OPENED
                beams_sync = False

        elif "Delay_line_position_zero_relative" in data_row['metadata'].dtype.names:
            if abs(data_row['metadata']["Delay_line_position_zero_relative"]) > 0.1: # BOTH OPENED
                beams_sync = False



        if (band in ["w1+w2", "2w2+w1", "2w1+w2", "2w2-w1"]) and not beams_sync:
            log_info.append(f'SKIP (not sync) {band}')
        elif  SN<-20:
            log_info.append(f'SKIP (SN) {band}, {SN}')
        elif y_init <= 1e-9 and len(bands)>=2:
            log_info.append(f'SKIP (<1e-9) {band}, {y_init}' )

        else:
            model += g_model
            params += g_model.make_params()

            params[pref + "center"].set(
                center, min=center - 5, max=center + 5, vary=vary
            )
            params[pref + "sigma"].set(1.7, min=1.6, max=4, vary=vary)
            params[pref + "amplitude"].set(
                y_init * 4, min=0, max=max(y_init*200,10), vary=vary
            )




            if band == "2w1":
                params[pref + "sigma"].set(1.7, min=1.6, max=4, vary=False)
                params[pref + "center"].set(
                    523, min=center - 5, max=center + 5, vary=False
                )
            elif band == "3w1":
                params[pref + "sigma"].set(1.7, min=1.6, max=4, vary=False)
                params[pref + "center"].set(
                    346, min=center - 5, max=center + 5, vary=False
                )
            elif band == "2w2" or band == "3w2":
                params[pref + "sigma"].set(2.5, min=1.6, max=4, vary=vary)
            else:
                pass





    for PL_prefix, band_params in PL_bands.items():
        center, sigma, gamma = band_params
        if center<x.min() or center>x.max(): continue
        y_tmp = interpolated_sig(np.linspace(center - 2, center + 2, 100))
        y_init = abs(y_tmp).max()
        SN = signaltonoise_dB(y_tmp)

        pref = PL_prefix+"_"

        sg_model = ExponentialGaussianModel(prefix=pref)

        vary = True

        if y_init <= 1e-8:
            log_info.append(f'SKIP (<1e-8) {PL_prefix}')
        elif  SN<0:
            log_info.append(f'SKIP (SN) {PL_prefix}')
        else:
            model += sg_model
            params += sg_model.make_params()
            params[pref + "center"].set(
                center, min=center - 4, max=center + 2, vary=True
            )
            params[pref + "sigma"].set(sigma, min=abs(sigma-0.2), max=sigma+0.2, vary=False)
            params[pref + "amplitude"].set(
                y_init * 10, min=0, max=y_init*100, vary=True
            )
            params[pref + "gamma"].set(gamma, min=gamma*0.9, max=gamma*1.1, vary=False)


    title+=f"|{beams_sync=}"
    log_info.append(f"{beams_sync=}")
    out = model.fit(y, x=x, params=params)
    print('='*100)
    print("\n".join(log_info))
    print(out.fit_report(show_correl=False,sort_pars=True).split("[[Fit Statistics]]")[-1])
    fig = None
    try:
        if len(x)!=len(out.best_fit):return
    except:
        return
    if plot:
        fig, axes = plt.subplots(2,1,gridspec_kw={'height_ratios': [1, 1]},sharex=True)
        fig.suptitle(title,fontsize=6)
        axes[0].plot(x, (y-out.params['bg_c'])*intens_ptp, ".",label='raw')
        axes[1].plot(x, (y-out.params['bg_c'])*intens_ptp, ".",label='raw')
        #plt.plot(x, model.eval(x=x,params=params)*intens_ptp, "--k",label='init')

        axes[0].plot(x, out.best_fit*intens_ptp, "-")
        axes[1].plot(x, out.best_fit*intens_ptp, "-")
        components = out.eval_components(x=x)
        for model_name, model_value in components.items():
            try:
                axes[0].plot(x, model_value*intens_ptp,'--', label=model_name)
                axes[1].plot(x, model_value*intens_ptp,'--', label=model_name)
            except:
                pass
        axes[0].legend(fontsize=8)
        #axes[0].set_ylim((-1e-3,max(out.best_fit*intens_ptp)))
        if "g2w2_height" in out.params:
            axes[1].set_ylim((-1e-5,out.params['g2w2_height'].value*intens_ptp*1.5))
        else:
            axes[1].set_ylim((-1e-5,max(out.best_fit*intens_ptp)/10))
        axes[0].grid(True)
        axes[1].grid(True)

        fig.tight_layout(h_pad=0)
    res = {}
    res['bg'] = out.params['bg_c'].value
    for band in bands:
        prefix = "g" + band.replace("+", "p").replace("-", "m") + "_"
        if prefix + "amplitude" in  out.params:
            res[band] = (
                out.params[prefix + "amplitude"].value
                * intens_ptp
            )

            res[band+"_center"] = (
                out.params[prefix + "center"].value
            )
            res[band+"_sigma"] = (
                out.params[prefix + "sigma"].value
            )

    for band in PL_bands:
        if band + "_amplitude" in out.params:
            res[band] = (
                out.params[band + "_amplitude"].value
                * intens_ptp
            )
            res[band+"_center"] = (
                out.params[band + "_center"].value)

            res[band+"_sigma"] = (
                out.params[band + "_sigma"].value)

            res[band+"_gamma"] = (
                out.params[band + "_gamma"].value)
    res['chisqr'] = out.chisqr
    for name in data_row['position'].dtype.names:
        res[name] = data_row['position'][name]
    if get_metadata:
        for name in data_row['metadata'].dtype.names:
            res[name] = data_row['metadata'][name]


    CenterIndex = None
    if "CenterIndex" in data_row['position'].dtype.names:
        CenterIndex = data_row['position']['CenterIndex']
    elif "CenterIndex" in data_row['metadata'].dtype.names:
        CenterIndex = data_row['metadata']['CenterIndex']
    if not CenterIndex is None:
        res['CenterType'] = center_info['Type'][CenterIndex].decode()

    res['FILTER'] = filterLabel

    return {'params':res, "x":x, "y":out.best_fit * intens_ptp, "model":model, "result":out, "figure":fig}




################################################################################
################################################################################
################################################################################
################################################################################
################################################################################
################################################################################



dataType = "MultiScan"
store = exdir.File(args.file, "r")



data_dict = extractTimestamp(store, args.timestamps, dataType)


last_timestamp = list(data_dict.keys())[-1]
filters_index2Label = data_dict[last_timestamp]['attrs']["filters"]

info = data_dict[last_timestamp]['attrs']["centerIndex_info"]

lamp_data = pd.read_csv(args.sensitivity)
# dichroicMirror_data = pd.read_csv('DMSP650R.csv')

lamp_correction = {}
for f in filters_index2Label.values():
    # tmp = interp1d(dichroicMirror_data.wavelength, dichroicMirror_data['T'],
    # 		bounds_error=False, fill_value=np.nan)
    wl = lamp_data.wavelength
    correction = lamp_data[f]  # *tmp(wl)
    w = correction > 1000

    lamp_correction[f] = interp1d(
        wl[w], correction[w], bounds_error=False, fill_value=np.nan
    )

res = []
# pulsewidth_corr_data = pd.read_csv('pulsewidth_correction.csv')
# pulsewidth_corr = interp1d(pulsewidth_corr_data.Ex_wl, pulsewidth_corr_data.pulsewidth_correction,
# 	bounds_error=False, fill_value=False)

n = 0
collector = []
if args.range is None:
    RANGE = slice(None)
else:
    RANGE = slice(*args.range)

path_pdf = args.file.split('.exdir')[0]+'.pdf'
copyfile(path_pdf, path_pdf.replace(".pdf","_backup.pdf"))
pdfSaver = PdfPages(path_pdf)

totalN = np.sum([len(data['data']) for data in data_dict.values()])


path_csv = args.file.split('.exdir')[0]+'.csv'
copyfile(path_csv, path_csv.replace(".csv","_backup.csv"))

global_i = 0
with tqdm(total=totalN) as pbar:
    try:
        for timestamp, data_ in data_dict.items():
            data = data_['data']
            print(timestamp)
            for i, data_row in enumerate(data[RANGE]):




                if len(args.delay_positions)!=0:
                    skip=True
                    for delay_pos in args.delay_positions:
                        if abs(data_row['position']['Delay_line_position_zero_relative']-delay_pos)<0.01:
                            skip = False
                            break


                    if skip:
                        continue

                res = fit_data(
                    data_row,
                    bands_generator=bands_generator(args.bands),
                    sensitivity_corrector=lamp_correction,
                    filters_translator=filters_index2Label,
                    PL_bands=PL_bands_generator(args.bands_PL),
                    get_metadata=args.get_metadata,
                    beams_sync=args.beams_sync, center_info=data_['attrs']['centerIndex_info'],
                    plot=True)
                pbar.update(1)
                global_i+=1
                if res is None:
                    continue
                collector.append(res['params'])
                if not res['figure'] is None:
                    if not res['params']['CenterType'] == 'BG':
                        try:
                            pdfSaver.savefig(res['figure'])
                        except:
                            pass
    except:
        traceback.print_exc()
        pdfSaver.close()
        df = pd.DataFrame(collector)
        df.to_csv(args.file.split('.exdir')[0]+'.csv')
if args.show_plot:
    plt.show()
else:
    plt.close('all')
df = pd.DataFrame(collector)


df.to_csv(args.file.split('.exdir')[0]+'.csv')
try:
    pdfSaver.close()
except:
    traceback.print_exc()
