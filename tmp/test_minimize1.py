from scipy.stats import multivariate_normal
from scipy.interpolate import LinearNDInterpolator


import optunity
import numpy as np
import matplotlib.pyplot as plt


solvers = [
    "random search",
    #'nelder-mead',
    "particle swarm",
    "sobol",
    #'grid search',
]  # optunity.available_solvers()
print("Available solvers: " + ", ".join(solvers))


# f = create_objective_function()
bounds = np.array([[-8.5, 9], [-10, 12]])

mu = np.array([1, -2])
cov = np.array([[5, 4], [4, 12]])

x = np.linspace(-10, 10, 100)
y = np.linspace(-20, 20, 100)
X, Y = np.meshgrid(x, y)
pos = np.empty(X.shape + (2,))
pos[:, :, 0] = X
pos[:, :, 1] = Y
rv = multivariate_normal(mu, cov)
Z = rv.pdf(pos)

interp = LinearNDInterpolator((X.flatten(), Y.flatten()), Z.flatten())
pos = []
evals = 1
N = 0


def func(x, y):
    global N
    Z = np.array([])
    for i in range(evals):
        z = interp(x, y) + np.random.normal(0, 1 / 500)
        Z = np.append(Z, z)
        print(N)
        N += 1
    return np.nanmean(Z)


f = func

logs = {}
res = {}
Pos = {}
for solver in solvers:
    print(solver)
    i = 0
    pos = []
    pars, details, _ = optunity.maximize(
        f, num_evals=20, x=bounds[0], y=bounds[1], solver_name=solver
    )
    logs[solver] = np.array(
        [
            details.call_log["args"]["x"],
            details.call_log["args"]["y"],
            details.call_log["values"],
        ]
    )
    res[solver] = pars
    N = 0
    print(pars)


# make sure different traces are somewhat visually separable
colors = ["r", "g", "b", "y", "m", "y", "r", "g"]
markers = ["x", "+", "o", "s", "p", "x", "+", "o"]

# compute contours of the objective function
delta = 0.025

# x = np.arange(-10, 10, delta)
# y = np.arange(-10, 10, delta)
# X, Y = np.meshgrid(x, y)
# Z = f(X, Y)

CS = plt.contourf(X, Y, Z)
# plt.clabel(CS, inline=1, fontsize=10)
for i, solver in enumerate(solvers):
    plt.scatter(
        logs[solver][0, :],
        logs[solver][1, :],
        c=colors[i],
        marker=markers[i],
        alpha=0.80,
    )
    plt.plot(
        logs[solver][0, :],
        logs[solver][1, :],
        "-",
        c=colors[i],
        alpha=0.80,
        label=solver,
    )
    plt.scatter(
        logs[solver][0, 0],
        logs[solver][1, 0],
        c=colors[i],
        marker="o",
        edgecolor="k",
        alpha=0.80,
    )
    plt.scatter(
        res[solver]["x"],
        res[solver]["y"],
        s=200,
        c=colors[i],
        marker="s",
        edgecolor="m",
    )

plt.legend()
fix, ax = plt.subplots(1, 2)
for i, solver in enumerate(solvers):
    ax[0].plot(logs[solver][0, :], logs[solver][2, :], "-", c=colors[i], alpha=0.80)
    ax[0].plot(res[solver]["x"], logs[solver][2, :].max(), "o" + colors[i], alpha=0.80)
    ax[1].plot(logs[solver][1, :], logs[solver][2, :], "-", c=colors[i], alpha=0.80)
    ax[1].vlines(
        res[solver]["y"], logs[solver][2, :].max(), "o" + colors[i], alpha=0.80
    )

plt.show()
