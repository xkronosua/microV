import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad, quad_vec
import multiprocessing
from lmfit import Model
from lmfit.models import QuadraticModel
from scipy.interpolate import interp1d
from scipy.signal import medfilt, butter, filtfilt
import itertools

sech = lambda x: 1 / np.cosh(x)


def I(t, t_p):
    return sech(1.7627 * t / t_p) ** 2


def E(t, t_p, wc):
    return I(t, t_p) ** 0.5 * np.exp(1j * wc * t)


def func(t, tau, t_p=100, wc=1):
    return abs((E(t, t_p, wc) + E(t - tau, t_p, wc)) ** 2) ** 2


def func_norm(t, t_p=100, wc=1):
    return abs(E(t, t_p, wc)) ** 4


def integrand_func(t, t_p=100, wc=1):
    return np.array(
        [
            quad(func, -almost_inf, almost_inf, args=(tau, t_p, wc))[0]
            / quad(
                func_norm,
                -almost_inf,
                almost_inf,
                args=(
                    t_p,
                    wc,
                ),
            )[0]
            for tau in t
        ]
    )


t = np.linspace(-1000, 1000, 1000)
almost_inf = 2000


def hl_envelopes_idx(s, dmin=2, dmax=2, split=False):
    """
    Input :
    s: 1d-array, data signal from which to extract high and low envelopes
    dmin, dmax: int, optional, size of chunks, use this if the size of the input signal is too big
    split: bool, optional, if True, split the signal in half along its mean, might help to generate the envelope in some cases
    Output :
    lmin,lmax : high/low envelope idx of input signal s
    """

    # locals min
    lmin = (np.diff(np.sign(np.diff(s))) > 0).nonzero()[0] + 1
    # locals max
    lmax = (np.diff(np.sign(np.diff(s))) < 0).nonzero()[0] + 1

    if split:
        # s_mid is zero if s centered around x-axis or more generally mean of signal
        s_mid = np.mean(s)
        # pre-sorting of locals min based on relative position with respect to s_mid
        lmin = lmin[s[lmin] < s_mid]
        # pre-sorting of local max based on relative position with respect to s_mid
        lmax = lmax[s[lmax] > s_mid]

    # global max of dmax-chunks of locals max
    lmin = lmin[
        [i + np.argmin(s[lmin[i : i + dmin]]) for i in range(0, len(lmin), dmin)]
    ]
    # global min of dmin-chunks of locals min
    lmax = lmax[
        [i + np.argmax(s[lmax[i : i + dmax]]) for i in range(0, len(lmax), dmax)]
    ]

    return lmin, lmax


def IAC(x, t0, A, A0, t_p, wc):

    t = x - t0  # .reshape(x.shape[0],1)
    # f = lambda t:  [quad(func,-almost_inf,almost_inf,args=(tau, t_p, wc))[0]/quad(func_norm,-almost_inf,almost_inf,args=( t_p, wc,))[0] for tau in t]
    f = lambda x: func(x, t, t_p, wc)
    f_norm = lambda x: func_norm(x, t_p, wc)
    r = quad_vec(f, -almost_inf, almost_inf)[
        0
    ]  # /quad_vec(f_norm,-almost_inf,almost_inf)[0]
    # print(r)
    r = np.nan_to_num(r, 0, neginf=0, posinf=0)
    # print(r[0])
    res = A0 + A * r

    print(t_p, wc, A, A0, t0)
    return res


# def IAC(x,tau0,A,A0,wc,tau_p):
# 	tau_ = x-tau0
# 	tau_norm = tau_/tau_p
# 	res = 1+(2+np.cos(2*wc*tau_))*3*(tau_norm*np.cosh(tau_norm)-np.sinh(tau_norm))/np.sinh(tau_norm)**3+3*(np.sinh(2*tau_norm)-(2*tau_norm))*np.cos(wc*tau_)/np.sinh(tau_norm)**3
# 	#print(res)
# 	if np.isnan(res).any(): print(x0,wc,tau_p)
# 	print(res.sum())
# 	#res = np.nan_to_num(res,1)
# 	return res*A-A0


data = np.loadtxt("test.dat")[1:-575]
x = (data[:, 0] * 2e-3 / 299792458) * 1e15
x -= x.mean()
y = data[:, 1] / data[:, 1].mean()
# y-=y[0]
y /= y[:100].mean()  # /6
# y+=1.8

# y = medfilt(y,5)
b, a = butter(5, 0.5)
y = filtfilt(b, a, y)


iu = interp1d(x, y)

dt = 0.001  # sampling interval
Fs = 1 / dt  # sampling frequency
t = np.arange(x.min(), x.max(), dt)
s = iu(t)
plt.figure()
r = plt.magnitude_spectrum(s, Fs=Fs, color="C1")
ws_threshold_n = 10
wc = r[1][ws_threshold_n:][r[0][ws_threshold_n:] == r[0][ws_threshold_n:].max()][0]
plt.figure()

# bg = QuadraticModel(prefix='bg_')
# params = bg.guess(y, x=x)

bg = lambda x: 1.5e-6 * x ** 2 - 2.0e-4 * x + 0.44
y = y - bg(x)
y /= abs(y[:100].mean())  # /6
ac_mod = Model(IAC)
params = ac_mod.make_params()
# params += ac_mod.make_params()


params["t_p"].set(160, min=150, max=250, vary=True)
params["t0"].set(0, min=-5, max=5, vary=True)
params["wc"].set(0.428, min=0.04, max=1, vary=True)
params["A"].set(0.15, min=0.11, vary=True)
params["A0"].set(-22, vary=True)


# params['bg_a'].set(1.5e-6,min=0,max=1,vary=False)
# params['bg_b'].set(-20e-5,min=-1,max=1,vary=False)
# params['bg_c'].set(0.44,min=-2,max=2,vary=False)

# mod = bg
# mod = bg+ac_mod
mod = ac_mod

weights = np.ones(len(y))

lmin, lmax = hl_envelopes_idx(y)
weights[lmin] = 30
weights[lmax] = 30

weights /= weights.max()
b, a = butter(5, 0.3)
weights = filtfilt(b, a, weights)

result = mod.fit(y, params, x=x, weights=weights)
plt.plot(x, y, ".-")

plt.plot(x, result.best_fit)

print(result.fit_report())
# Ic = IAC(t,1000,100)
# plt.plot(t,Ic)
plt.show()
