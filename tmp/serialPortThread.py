# pylint: disable=wrong-import-position
import sys
import time
import traceback
import serial
from serial.threaded import LineReader, ReaderThread
import queue

PORT = "./ttyclient"
# PORT = 'loop://'


class PrintLines(LineReader):
    inWaiting = queue.Queue()
    lastData = None, time.time()

    def connection_made(self, transport):
        super(PrintLines, self).connection_made(transport)
        self.transport = transport
        sys.stdout.write("port opened\n")
        self.write_line("hello world")

    def handle_line(self, data):
        info = (data, time.time())
        sys.stdout.write("line received: {!r}\n".format(data))
        self.inWaiting.put(info)
        self.lastData = info

    def query(self, command):
        self.transport.serial.reset_output_buffer()
        self.transport.serial.reset_input_buffer()

        self.write_line(command)
        t = time.time()
        while self.lastData[1] < t:
            time.sleep(0.01)
        return self.lastData, time.time()

    def connection_lost(self, exc):
        if exc:
            traceback.print_exc(exc)
        sys.stdout.write("port closed\n")


# ser = serial.serial_for_url(PORT, baudrate=115200, timeout=1)
# with ReaderThread(ser, PrintLines) as protocol:
# protocol.write_line('hello')
# time.sleep(2)

# alternative usage
ser = serial.serial_for_url(PORT, baudrate=9600, timeout=1)
t = ReaderThread(ser, PrintLines)
t.start()
transport, protocol = t.connect()
protocol.write_line(str(time.time()))
r = protocol.query(str(time.time()) + "!")
print(f"{r=}")
time.sleep(1)
for i in range(protocol.inWaiting.qsize()):
    print(protocol.inWaiting.get())
t.close()
