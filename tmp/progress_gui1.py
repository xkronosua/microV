from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import QObject, QThread
from PyQt5 import Qt  # +
import time


class WorkThread(Qt.QThread):

    threadSignal_finished = Qt.pyqtSignal(object)
    args = ()
    kwargs = {}

    def __init__(self, func=None, args=(), kwargs={}):
        super().__init__()
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def run(self):
        r = self.func(*self.args, **self.kwargs)
        self.threadSignal_finished.emit({r: time.time()})


class MsgBox(Qt.QDialog):
    def __init__(self):
        super().__init__()

        layout = Qt.QVBoxLayout(self)
        self.label = Qt.QLabel("")
        layout.addWidget(self.label)
        close_btn = Qt.QPushButton("Close")
        layout.addWidget(close_btn)

        close_btn.clicked.connect(self.close)

        self.setGeometry(900, 65, 400, 80)
        self.setWindowTitle("MsgBox from WorkThread")


class GUI(Qt.QWidget):  # (QMainWindow):
    def __init__(self):
        super().__init__()

        layout = Qt.QVBoxLayout(self)
        self.btn = Qt.QPushButton("Start thread.")
        layout.addWidget(self.btn)
        self.btn.clicked.connect(self.startExecuting)

        self.btn1 = Qt.QPushButton("Start thread1")
        layout.addWidget(self.btn1)
        self.btn1.clicked.connect(self.startExecuting1)

        self.msg = MsgBox()
        self.thread = None

        self.label = Qt.QLabel("1")
        layout.addWidget(self.label)
        self.label1 = Qt.QLabel("1")
        layout.addWidget(self.label1)

        self.thread1 = None
        # Lots of irrelevant code here ...

    def func(self, a, b=1):
        print(a, b)
        txt = self.label1.text()
        time.sleep(5)
        print(txt)
        if int(txt) % 2 == 0:
            self.label1.setStyleSheet("color:red;")
        else:
            self.label1.setStyleSheet("color:blue;")
        return a

    # Called when "Start/Stop Executing" button is pressed
    def startExecuting(self, user_script):

        if self.thread is None:
            self.thread = WorkThread(self.func, args=(1,), kwargs={"b": 2})

            self.thread.threadSignal_finished.connect(self.on_threadSignal_finished)
            self.thread.start()

            self.btn.setText("Stop thread")
        else:
            self.thread.terminate()
            self.thread = None
            self.btn.setText("Start thread")

    def startExecuting1(self, user_script):

        if self.thread1 is None:
            self.thread1 = WorkThread(self.func, args=(2,), kwargs={"b": 3})

            self.thread1.threadSignal_finished.connect(self.on_threadSignal_finished)
            self.thread1.start()

            self.btn1.setText("Stop thread")
        else:
            self.thread1.terminate()
            self.thread1 = None
            self.btn1.setText("Start thread")

    def on_threadSignal_finished(self, value):
        self.label.setText(str(value))

    def on_threadSignal_finished1(self, value):
        self.label1.setText(str(value))


if __name__ == "__main__":
    app = Qt.QApplication([])
    mw = GUI()
    mw.show()
    app.exec()
