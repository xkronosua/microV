import sys

sys.path.insert(0, "..")
from hardware.stages.arduinoStage import ArduinoStage
import Pyro5
import Pyro5.api

import Pyro5.server

daemon = Pyro5.server.Daemon()

# expose the class from the library using @expose as wrapper function:
ExposedClass = Pyro5.server.expose(ArduinoStage)

uri = daemon.register(
    ExposedClass, objectId="aaa"
)  # register the exposed class rather than the library class itself

print(uri)
daemon.requestLoop()
