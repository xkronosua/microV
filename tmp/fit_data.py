import os
import sys
import time

from pathlib import Path
from glob import glob

import exdir
import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import re
from lmfit.models import ConstantModel, GaussianModel
from scipy.signal import medfilt
from scipy.interpolate import interp1d


def fit(x, y, ex_wl):

    bg = ConstantModel(prefix="bg_")
    pars = bg.guess(y, x=x)
    if ex_wl < 700:
        pars["bg_c"].set(value=y.min(), vary=False)
    else:
        pars["bg_c"].set(value=y[:10].mean(), vary=False)

    tunable_sh = GaussianModel(prefix="tun_")
    pars.update(tunable_sh.make_params())

    pars["tun_center"].set(value=ex_wl / 2, vary=False)
    pars["tun_sigma"].set(value=4.5, min=4, max=5)
    pars["tun_amplitude"].set(value=1, min=0.1 + y.min())

    fix_sh = GaussianModel(prefix="fix_")
    pars.update(fix_sh.make_params())

    pars["fix_center"].set(value=1045 / 2, vary=False)
    pars["fix_sigma"].set(value=4.5, min=4, max=5)
    pars["fix_amplitude"].set(value=1, min=0.1 + y.min())

    sum_freq = GaussianModel(prefix="sum_")
    pars.update(sum_freq.make_params())

    pars["sum_center"].set(value=1 / (1 / ex_wl + 1 / 1045), vary=False)
    pars["sum_sigma"].set(value=4.5, min=4, max=5)
    pars["sum_amplitude"].set(value=0.01, min=0)

    mod = bg + tunable_sh + fix_sh + sum_freq

    init = mod.eval(pars, x=x)
    out = mod.fit(y, pars, x=x)
    # print(x,y,out.best_fit)
    return out, init, mod


pars_to_save = [
    "bg_c",
    "fix_center",
    "fix_amplitude",
    "fix_sigma",
    "tun_center",
    "tun_amplitude",
    "tun_sigma",
    "sum_center",
    "sum_amplitude",
    "sum_sigma",
]

c = 299792458  # m/s

res_data = []

path = sys.argv[1]

print(path)
store = exdir.File(path, "r")

keys = [k for k in store.keys() if k[0] != "_"]
keys.sort()
keys = keys[::-1]

data_ = []
dtype = store[keys[0]]["scan"].dtype


for k in keys:
    try:
        dd = store[k]["scan"]
        if dd.dtype != dtype:
            break
        w = dd["time"] != 0
        if sum(w) == 0:
            break
        data_.append(dd[w])
    except:
        traceback.print_exc()
        break
data = np.hstack(data_)

spectra_len_ = len(data["AndorCamera"]["wavelength"][0])
# out_data = np.zeros(len(data),dtype=[('Ex_wavelength','f8'),('spectra',('wavelength','f8',spectra_len_),('intens','f8',spectra_len_),('wavelength','f8',spectra_len_)), ('power','f8'), ('power_IR','f8')])
res = []
N_graphs = len(np.unique(data["LaserWavelength"]))
N_rows = int(np.ceil(N_graphs))

fig, axes = plt.subplots(
    N_rows, 1, sharex=True, sharey=True, gridspec_kw={"hspace": 0, "wspace": 0}
)
axes_ = axes  # .reshape((np.multiply(*axes.shape)))
# axes.flatten()
for i, ex_wl in enumerate(np.unique(data["LaserWavelength"])[::-1]):
    print("Ex_wl", ex_wl)
    w = data["LaserWavelength"] == ex_wl
    d = data[w]
    max_SFG = 0
    max_SFG_ = 0.5

    for row in d[10:]:
        x = row["Delay_line_position"]
        # print(row)
        wl = row["AndorCamera"]["wavelength"]
        intens = row["AndorCamera"]["intensity"]
        iu = interp1d(wl, intens)
        if iu(1 / (1 / ex_wl + 1 / 1045)) < iu(1045 / 2) / 20:
            continue
        intens = intens / iu(1045 / 2)

        # plt.plot(wl,intens)

        print(x, max_SFG)
        out, init, mod = fit(wl, intens, ex_wl)
        print(out.chisqr)
        if out.chisqr > 5:
            continue
        if out.params["sum_amplitude"].value > out.params["fix_amplitude"].value / 20:
            if out.params["sum_amplitude"].value > max_SFG:
                max_SFG = out.params["sum_amplitude"].value
                print("+")
            else:
                print("max_detected:", x)
                tmp = {
                    "delay_pos": x,
                    "ex_wl": ex_wl,
                    "chisqr": out.chisqr,
                    "fix_amplitude_max": iu(1045 / 2),
                }
                for p in pars_to_save:
                    tmp[p] = out.params[p].value
                res.append(tmp)
                axes_[i].plot(wl, intens - out.params["bg_c"], ".g", label=f"{ex_wl}nm")
                axes_[i].plot(
                    wl, out.best_fit - out.params["bg_c"], "-r", label=f"_{ex_wl}nm"
                )
                # axes_[i].legend()
                axes_[i].text(350, 1, f"{ex_wl}nm")
                axes_[i].set_xlim((300, 700))
                # axes_[i].set_ylim((0,2))
                break

        else:
            continue


plt.show()
