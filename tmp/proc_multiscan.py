import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter
import pandas as pd
from lmfit.models import ConstantModel, GaussianModel, SkewedGaussianModel


def unique_dummy(array, return_index=False):
    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


def fit(x, y, ex_wl):

    bg = ConstantModel(prefix="bg_")
    pars = bg.guess(y, x=x)
    pars["bg_c"].set(value=0, min=1, max=5, vary=False)

    sigma = 2.5
    tun_sh = GaussianModel(prefix="tun2w_")
    pars.update(tun_sh.make_params())
    centers = [ex_wl / 2, ex_wl / 3, 527, 1 / (1 / ex_wl + 1 / 1045), 395]
    pars["tun2w_center"].set(
        value=centers[0] + 2.5, min=centers[0] - 3, max=centers[0] + 4, vary=False
    )
    pars["tun2w_sigma"].set(value=sigma, min=1, max=5, vary=False)
    pars["tun2w_amplitude"].set(value=10, min=0.1)

    tun_th = GaussianModel(prefix="tun3w_")
    pars.update(tun_th.make_params())

    pars["tun3w_center"].set(
        value=centers[1] + 2, min=centers[1] - 3, max=centers[1] + 10, vary=False
    )
    pars["tun3w_sigma"].set(value=sigma, min=1, max=10, vary=False)
    pars["tun3w_amplitude"].set(value=10, min=0.1)

    fix_sh = GaussianModel(prefix="fix2w_")
    pars.update(fix_sh.make_params())

    pars["fix2w_center"].set(
        value=centers[2], min=centers[2] - 3, max=centers[2] + 10, vary=False
    )
    pars["fix2w_sigma"].set(value=sigma, min=1, max=5, vary=False)
    pars["fix2w_amplitude"].set(value=10, min=0.1)

    sum_freq = GaussianModel(prefix="sum_")
    pars.update(sum_freq.make_params())

    pars["sum_center"].set(
        value=centers[3] + 3, min=centers[3] - 3, max=centers[3] + 10, vary=False
    )
    pars["sum_sigma"].set(value=sigma, min=1, max=5, vary=False)
    pars["sum_amplitude"].set(value=10, min=0)

    pl = SkewedGaussianModel(prefix="pl_")
    pars.update(pl.make_params())

    pars["pl_center"].set(
        value=centers[4], min=centers[4] - 10, max=centers[4] + 10, vary=False
    )
    pars["pl_sigma"].set(value=10, min=5, max=20, vary=False)
    pars["pl_amplitude"].set(value=10, min=0)
    pars["pl_gamma"].set(value=10, min=5, max=100, vary=True)

    mod = tun_sh + tun_th + fix_sh + sum_freq + pl

    init = mod.eval(pars, x=x)
    out = mod.fit(y, pars, x=x)
    print(out.fit_report())
    return out, init, mod


path = sys.argv[1]

print(path)
store = exdir.File(path, "r")

dataType = "MultiScan"

keys = [k for k in store[dataType] if k[0] != "_"]
sizes = [len(store[dataType][k]["data"]) for k in keys]

dtype = store[dataType][keys[-1]]["data"].dtype

data = np.zeros(np.sum(sizes), dtype=dtype)
last_row = 0
for i, k in enumerate(keys):
    if store[dataType][k]["data"].dtype == dtype:
        d = store[dataType][k]["data"]
        w = d["time"] != 0
        data[last_row : last_row + w.sum()] = d[w]
        last_row += w.sum()
        print(i, k)
data = data[data["time"] != 0]


# unique_ex_wls, unique_ex_wls_index = unique(data['position']['LaserWavelength'], return_index=True)

unique_ex_wls, unique_ex_wls_index = unique_dummy(
    data["position"]["LaserWavelength"], return_index=True
)

groups = np.split(data, unique_ex_wls_index)[1:]
ex_wls = np.unique(data["position"]["LaserWavelength"])[::-1]

if len(ex_wls) > 10:
    fig, axes0 = plt.subplots(int(np.ceil(len(ex_wls) / 3)), 3, sharex=True)
    axes0 = axes0.T.flatten()
else:
    fig, axes0 = plt.subplots(
        len(ex_wls),
        1,
        sharex=True,
    )
if len(ex_wls) == 1:
    axes0 = [axes0]

spectral_correction_df = pd.read_csv("Microscope_Backward_grating1.csv")
spectral_correction = interp1d(
    spectral_correction_df["wavelength"], spectral_correction_df["FESH0700"]
)

centers = [0]
colors = "rgbcm"
res = []
for i, ex_wl in enumerate(ex_wls[:-4]):
    print(f"{i}:\t{ex_wl}")
    d = groups[np.where(unique_ex_wls == ex_wl)[0][-1]]
    # fig,axes = plt.subplots(1,len(centers),sharex=True,figsize=(15,8))
    for center in centers:

        mask = d["position"]["CenterIndex"] == center

        sfg = d["data"]["AndorCamera"]["data"]["w1+w2"][mask]
        sh2 = d["data"]["AndorCamera"]["data"]["2w2"][mask]
        sh1 = d["data"]["AndorCamera"]["data"]["2w1"][mask]
        th2 = d["data"]["AndorCamera"]["data"]["3w2"][mask]
        fwm = d["data"]["AndorCamera"]["data"]["2w2-w1"][mask]

        peak_index = np.where(sfg == sfg.max())[0][0]
        wl0 = d["data"]["AndorCamera"]["raw"]["wavelength"][mask][peak_index].astype(
            float
        )
        mask1 = (wl0 > 380) & (wl0 < 680)

        exposure = d["data"]["AndorCamera"]["raw"]["exposure"][mask][peak_index].astype(
            float
        )
        intens0 = (
            d["data"]["AndorCamera"]["raw"]["intensity"][mask][peak_index].astype(float)
            / exposure
        )
        scorr = spectral_correction(wl0[mask1])

        x = wl0[mask1]
        y = intens0[mask1]  # /scorr
        y_max = y.max()
        y = y / y_max
        out, init, mod = fit(x, y, ex_wl)
        plt.figure()
        plt.plot(x, y, "-")
        plt.plot(x, out.best_fit, "-")
        # plt.plot(x,init,'--')
        res.append(
            {
                "ex_wl": ex_wl,
                "2w2": out.params["tun2w_amplitude"].value,
                "3w2": out.params["tun3w_amplitude"].value,
                "2w1": out.params["fix2w_amplitude"].value,
                "w1+w2": out.params["sum_amplitude"].value,
                "pl": out.params["pl_amplitude"].value,
            }
        )


# axes[0].legend()
df = pd.DataFrame(res)
plt.figure()
plt.plot(df.ex_wl, df["2w2"], "--c", label="2w2")
# plt.plot(df.ex_wl,df['2w2_0delay'],'-c',label='2w2_0delay')
plt.plot(df.ex_wl, df["2w1"], "--b", label="2w1")
# plt.plot(df.ex_wl,df['2w1_0delay'],'-b',label='2w1_0delay')
plt.plot(df.ex_wl, df["w1+w2"], "-g", label="w1+w2")

plt.legend()
plt.figure()
plt.plot(df.ex_wl, df["2w2"] / df["2w1"], "--c", label="2w2")


plt.show()
