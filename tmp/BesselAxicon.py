from LightPipes import *
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from raytracing import materials

# img=mpimg.imread('bessel_axicon.png')
# plt.imshow(img); plt.axis('off');plt.title('Bessel beam with axicon')
# plt.show()

wavelength = 1045.0 * nm
size = 25.4 * mm
N = 1000

angle = 2
phi = np.deg2rad((90 - angle) * 2)
n = materials.FusedSilica.n(wavelength * 1e6)
phi1 = np.deg2rad((90 - angle) * 2)
n1 = materials.FusedSilica.n(wavelength * 1e6)

F = Begin(size, wavelength, N)

w0 = 2.8 * mm
divergence = 1.5e-3
f = w0 / np.tan(divergence / 2)
F = GaussBeam(F, w0)
# F = GaussHermite(w0=w0,A=1,m=0,n=0,Fin=F)

# F = Lens(-f,F)
# F = Forvard(80*cm,F)
print("Axicons")
F = Axicon(phi, n, 0, 0, F)
F = Forvard(60 * cm, F)
F = Axicon(F, phi, n1, 0, 0)
print("Axicons:done")

print("BeamSplitter")
R0 = 0.99
R1 = 0.2
t = 1 * mm

F1 = IntAttenuator(R0, F)
print("1")
F1 = Tilt(F1, np.pi / 2, 0)
print("1")
F1 = Fresnel(2 * mm, F1)
# F1 = Forward(1*mm,F1)
print("1")

F2 = IntAttenuator(1 - R0, F)
print("2")
F2 = Fresnel(2 * mm, F2)
# F2 = Forward(1*mm,F2)
print("2")
F2 = IntAttenuator(R1, F2)
F2 = Tilt(np.pi / 2, 0, F2)
print("2")
F2 = Fresnel(2 * mm, F2)
F2 = IntAttenuator(R0, F2)
print("2")

F3 = IntAttenuator(R0, F2)
print("3")
F3 = Fresnel(2 * mm, F3)
# F2 = Forward(1*mm,F2)
print("3")
F3 = IntAttenuator(R1, F3)
F3 = Tilt(np.pi / 2, 0, F3)
print("3")
F3 = Fresnel(2 * mm, F3)
F3 = IntAttenuator(R0, F3)
print("3")

F2 = BeamMix(F2, F3)

F = BeamMix(F1, F2)
print("BeamSplitter:Done")
positions = [0 * cm, 20 * cm, 60 * cm, 1 * m]
z_prev = 0
fig, ax = plt.subplots(2, len(positions) // 2 + 1)
ax = ax.flatten()
for i, z in enumerate(positions):
    print(i, z)
    F = Fresnel(z - z_prev, F)
    I = Intensity(0, F)
    ax[i].set_title(f"{z=}")
    x = F.xvalues * 1000
    y = F.yvalues * 1000
    X, Y = np.meshgrid(x, y)
    ax[i].contourf(X, Y, I, 100, cmap="jet")
    ax[i].set_aspect(1)
    z_prev = z

plt.show()
