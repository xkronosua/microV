import numpy as np
import exdir
import sys
import matplotlib.pyplot as plt
import traceback
from scipy.interpolate import interp1d
from scipy.signal import argrelextrema, filtfilt, butter, medfilt
import time
import numpy_indexed as npi
import argparse
import pandas as pd
from lmfit.models import GaussianModel, ExponentialGaussianModel, ConstantModel, ExponentialModel
from pathlib import Path
from matplotlib import cm
import matplotlib

from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description="Process data for multiscan measurements.")
parser.add_argument("-f", dest="file", help="CSV file from preprocessing")

parser.add_argument(
    "--sync", dest="beams_sync", help="Measurements with delay.", action="store_true"
)

parser.add_argument(
    "-N", dest="norm", help="Normalize at wavelength.", type=int, default=1045
)

args = parser.parse_args()
print(args)

df = pd.read_csv(args.file)

for band in ['3w1','3w2']:
    if band in df:
        df[band][df['FILTER']=='FESH0700'] = np.nan
        df[band][df['FILTER']=='FESH0850'] = np.nan

df['2w2'][(df['FILTER']=='FGB37x2_EO84-712') & (df['LaserWavelength']>=1200)] = np.nan
PL_bands = {}
for k in df.keys():
    if k[:2].split('_')[0]=="PL":
        PL_bands[k.split('_')[0]] = int(k.split('_')[0][2:])

for k in PL_bands:
    df[k][(df['FILTER']=='FESH0850')] = np.nan
    df[k][(df['FILTER']=='FESH0700')] = np.nan

if args.beams_sync:
    df = df[abs(df['Delay_line_position_zero_relative'])<0.01]

nonBG_mask = df.CenterType!='BG'
index2type = dict(zip(df.CenterIndex,df.CenterType))
df1 = df[nonBG_mask].groupby(['LaserWavelength','filtersPiezoStage','CenterIndex'],as_index=False).mean()
df1['CenterType'] = [index2type[index] for index in df1.CenterIndex]
for i, data_row in df[df['CenterType']=='BG'].groupby(['LaserWavelength','filtersPiezoStage'],as_index=False).mean().iterrows():
    ex_wl = data_row.LaserWavelength
    filterIndex = data_row.filtersPiezoStage
    for band in ['3w2', '2w2', '2w1', '3w1', '2w2+w1', '2w1+w2']+list(PL_bands):
        if not band in df1: continue
        df1[(df1.LaserWavelength==ex_wl) & (df1.filtersPiezoStage==filterIndex)][band]-=data_row[band]

#fig, axes = plt.subplots(1,3, sharex=True)
fig0, ax0 = plt.subplots(1,1)
fig1, ax1 = plt.subplots(1,1)
fig2, ax2 = plt.subplots(1,1)
axes = [ax0,ax1,ax2]

fig01, ax01 = plt.subplots(1,1)
#fig11, ax11 = plt.subplots(1,1)
#fig21, ax21 = plt.subplots(1,1)
axes1 = [ax01]


norm_color = matplotlib.colors.Normalize(vmin=0, vmax=df1.CenterIndex.max())
avg_norm = {'ex_wl': np.arange(700,1305,5)}
N = 0
#LN = pd.read_csv(Path("../Sep0821/LN276_1_avg.csv"))

for i,g in df1.groupby('CenterIndex'):
    centerType = g['CenterType'].iloc[0]
    centerIndex = g['CenterIndex'].iloc[0]

    if np.all(g['CenterType']=='BG'): continue
    plt.figure()
    for i,band in enumerate(['3w2', '2w2', '2w1', '3w1', '2w2+w1', '2w1+w2']+list(PL_bands)):
        #df1 = df1[df1['2w2']<1000]
        if not band in g: continue
        isna = g[band].isna()
        x = g.LaserWavelength[~isna]
        #y_PL = medfilt(g['PL374'][~isna],5)
        y = medfilt(g[band][~isna],5)
        if len(x) == 0: continue
        iu = interp1d(x,y,fill_value=np.nan,bounds_error=False)
        norm = 10**(np.log10(np.nanmean(iu(np.linspace(900,950,5)))))#iu(np.linspace(1025,1075,5)).mean()
        rgba_color = cm.turbo(norm_color(centerIndex  ))
        if band == "2w2":
            axes[0].semilogy(x, y/norm, '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
            #LN_IU = interp1d(LN['ex_wl'],LN[band],fill_value=np.nan,bounds_error=False)
            #axes1[0].semilogy(x, y/norm/LN_IU(x), '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
            #axes1[0].semilogy(x, y_PL/y, '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")

        elif band == "3w2":
            axes[1].semilogy(x, y/norm, '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")
        else:
            axes[2].semilogy(x, y/norm, '-', marker='v', color=rgba_color, label=f"NP{centerIndex}")


        plt.semilogy(x, y, '-', marker=i,  label=band)
        if not band in avg_norm:
            avg_norm[band] = 0
        avg_norm[band] += iu(avg_norm["ex_wl"])/norm

    N+=1

    plt.legend()
    plt.grid(True, which="both", ls="-")
    plt.xlabel('Ex. wavelength, nm')
    plt.ylabel('Intensity, arb. un.')

axes[0].grid(True, which="both", ls="-")
axes[1].grid(True, which="both", ls="-")
axes[2].grid(True, which="both", ls="-")
axes[2].set_xlabel('Ex. wavelength, nm')
axes[0].set_ylabel('2w2')
axes[1].set_ylabel('3w2')
axes[2].set_ylabel('PL')

axes1[0].grid(True, which="both", ls="-")


axes[0].legend()
axes[1].legend()
axes[2].legend()

avg_norm = pd.DataFrame(avg_norm)
avg_norm[list(avg_norm.keys())[1:]]/=N
axes[0].semilogy(avg_norm['ex_wl'], avg_norm['2w2'],'--r', lw=3)
#axes[1].semilogy(avg_norm['ex_wl'], avg_norm['3w2'],'--')
#axes[2].semilogy(avg_norm['ex_wl'], avg_norm['w1+w2'],'--r', lw=3)

avg_norm.to_csv(args.file.replace('.csv','_avg.csv'))




plt.show()
