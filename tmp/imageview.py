# -*- coding: utf-8 -*-
"""
Demonstrates use of PlotWidget class. This is little more than a
GraphicsView with a PlotItem placed in its center.
"""


from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg

# QtGui.QApplication.setGraphicsSystem('raster')
app = pg.mkQApp()
mw = QtGui.QMainWindow()
mw.setWindowTitle("pyqtgraph example: PlotWidget")
mw.resize(800, 800)
cw = QtGui.QWidget()
mw.setCentralWidget(cw)
l = QtGui.QVBoxLayout()
cw.setLayout(l)


imv = pg.ImageView()

l.addWidget(imv)
grid = pg.GridItem()
img1 = pg.ImageItem()
img2 = pg.ImageItem()
imv.addItem(img1)
imv.addItem(img2)
imv.addItem(grid)
grid.setTextPen("y")
grid.setPen("y")

mw.show()

img = pg.gaussianFilter(np.random.normal(size=(200, 200)), (5, 5)) * 20 + 100
img = img[np.newaxis, :, :]
decay = np.exp(-np.linspace(0, 0.3, 100))[:, np.newaxis, np.newaxis]
data = np.random.normal(size=(100, 200, 200))
data += img * decay
data += 2
# imv.setImage(data)
print(imv.imageItem)
img1.setImage(data[:, :, 0], alpha=0.5)
img1.setPos(100, 20)
img2.setImage(data[:, 1, :], alpha=0.5)
img2.setPos(10, 200)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == "__main__":
    import sys

    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        QtGui.QApplication.instance().exec_()
