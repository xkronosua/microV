import numpy as np
import exdir
from lmfit.models import GaussianModel, SkewedGaussianModel, Model
import matplotlib.pyplot as plt
from scipy.signal import medfilt, filtfilt, butter
from scipy.interpolate import interp1d
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
from pyqtgraph.parametertree import (
    Parameter,
    ParameterTree,
    ParameterItem,
    registerParameterType,
)

s = exdir.File("P3_F1045_T1300_scan1_copy.exdir/")
ts = max(s["scan3D"])
dset = s["scan3D"][ts]["data"]

data = dset["data"]["AndorCamera"]["raw"]["intensity"][0, :, :, 0, :]
x = dset["data"]["AndorCamera"]["raw"]["wavelength"][0, 0, 0, 0, :].astype(float)

shape = data[:, :, 0].shape
if "img" in s["scan3D"][ts]:
    img = s["scan3D"][ts]["img"]
else:

    s["scan3D"][ts].create_dataset(
        "img",
        shape=(1,),
        dtype=[
            ("wavelength", "f4", len(x)),
            ("2w1", "f4", shape),
            ("2w2", "f4", shape),
            ("2w2+w1", "f4", shape),
            ("w1+w2", "f4", shape),
            ("bg", "f4", shape),
        ],
    )

img = s["scan3D"][ts]["img"]

Chan = ["2w1", "2w2", "w1+w2", "2w2+w1", "bg"]

centers = np.array(
    [1045 / 2, 1300 / 2, 1 / (1 / 1045 + 1 / 1300), 1 / (1 / 1045 + 2 / 1300)]
)

bg_grid = np.linspace(x.min(), x.max(), len(x))

print(bg_grid)
w = np.ones(len(bg_grid)) == 1
for c in centers:
    w = w & ~((bg_grid > (c - 10)) & (bg_grid < (c + 10)))

bg_grid = bg_grid[w]
print(bg_grid)
"""
for i in list(range(data.shape[0]))[::10]:
	for j in list(range(data.shape[1]))[::10]:
		y_ = medfilt(data[i,j].astype(float),5)

		
		bg_interp = interp1d(x,y_)
		b, a = butter(3,0.2)
		bg = bg_interp(bg_grid)
		#bg = filtfilt(b,a,bg)
		#bg_grid = filtfilt(b,a,bg_grid)
		bg = medfilt(bg,51)
		bg_grid = medfilt(bg_grid,51)
		
		bg_interp = interp1d(bg_grid,bg,bounds_error=False,fill_value=0)
		bg = bg_interp(x)
		y = y_ - bg
		interp = interp1d(x,y)

		

		sigma = 3
		mod_2w1 = GaussianModel(prefix='g2w1_')
		pars = mod_2w1 .guess(y, x=x)
		pars.update(mod_2w1.make_params())
		c = 1045/2-0.5
		pars['g2w1_center'].set(value=c,vary=False,min=c-4,max=c+4)
		pars['g2w1_sigma'].set(value=sigma, vary=True,min=2,max=11)
		pars['g2w1_amplitude'].set(value=interp(c)/0.3989423/sigma, min=0.1)


		mod_2w2 = GaussianModel(prefix='g2w2_')
		pars.update(mod_2w2.make_params())
		
		c = 1300/2-3
		pars['g2w2_center'].set(value=c,vary=True,min=c-4,max=c+4)
		pars['g2w2_sigma'].set(value=sigma, min=2, max=11,vary=True)
		pars['g2w2_amplitude'].set(value=interp(c)/0.3989423/sigma, min=0.1)


		mod_w1w2 = GaussianModel(prefix='gw1w2_')
		pars.update(mod_w1w2.make_params())
		c = 1/(1/1045+1/1300)-1
		pars['gw1w2_center'].set(value=c, vary=True, min=c-4,max=c+4)
		pars['gw1w2_sigma'].set(value=sigma, min=2, max=11,vary=True)
		pars['gw1w2_amplitude'].set(value=interp(c)/0.3989423/sigma, min=0.1)

		mod_w12w2 = GaussianModel(prefix='gw12w2_')
		pars.update(mod_w12w2.make_params())
		c = 1/(1/1045+2/(1300-6))
		pars['gw12w2_center'].set(value=c, vary=False, min=c-4,max=c+4)
		pars['gw12w2_sigma'].set(value=sigma, min=2, max=11,vary=True)
		pars['gw12w2_amplitude'].set(value=interp(c)/0.3989423/sigma, min=0.1)


		#mod = mod_2w1 + mod_2w2 + mod_w1w2 + mod_w12w2 + mod_bg
		mod = mod_2w1 + mod_2w2 + mod_w1w2 + mod_w12w2
		
		init = mod.eval(pars, x=x)
		out = mod.fit(y, pars, x=x)

		#print(out.fit_report(min_correl=0.5))
		print(bg.sum(),i,j)
		comps = out.eval_components(x=x)
		
		img['bg'][0,i,j] = bg.sum()
		img['2w1'][0,i,j] = out.params['g2w1_amplitude'].value
		img['2w2'][0,i,j] = out.params['g2w2_amplitude'].value
		img['2w2+w1'][0,i,j] = out.params['gw12w2_amplitude'].value
		img['w1+w2'][0,i,j] = out.params['gw1w2_amplitude'].value

		if i%10==0 and j%10==0:
			plt.figure()
			plt.plot(x,y_,'.k',label='raw')
			#plt.plot(x,init,'--',label='init')
			plt.plot(x,out.best_fit+bg,'r',label='fit')
			plt.plot(x,bg,'--',label='bg')
			
			
			for ind,pref in enumerate(['g2w1_','g2w2_','gw1w2_','gw12w2_']):
				plt.plot(x, comps[pref], label=Chan[ind])
			#plt.plot(x, comps['bg_'], label='bg')
			plt.legend()
"""
X = np.linspace(dset["metadata"]["X"].min(), dset["metadata"]["X"].max(), shape[0])
Y = np.linspace(dset["metadata"]["Y"].min(), dset["metadata"]["Y"].max(), shape[1])
scale = np.array([np.diff(X).mean(), np.diff(Y).mean()])
X, Y = np.meshgrid(Y, X)


# QtGui.QApplication.setGraphicsSystem('raster')
app = pg.mkQApp()
mw = QtGui.QMainWindow()

cw = QtGui.QWidget()
mw.setCentralWidget(cw)
l = QtGui.QGridLayout()
cw.setLayout(l)

mw.show()

imv = {}
index = 0
for i in range(3):
    for j in range(2):
        if i == 2 and j == 1:
            break
        im = pg.ImageView()
        grid = pg.GridItem()
        im.addItem(grid)
        txt = pg.TextItem(Chan[index], color="y", border="w", fill=(0, 0, 255, 100))
        im.addItem(txt)
        l.addWidget(im, i, j, 1, 1)
        imv[Chan[index]] = im
        im.getView().setXRange(0, np.ptp(X))
        im.getView().setYRange(0, np.ptp(Y))
        index += 1

ang = 5.0
period = 2.5
a = 1.2
origin = np.array([-2.4, -3.7])
beams_shift = np.array([0.4, -0.3])
roi = []


def rotate(v, deg):
    """Use numpy to build a rotation matrix and take the dot product."""
    radians = np.deg2rad(deg)
    c, s = np.cos(radians), np.sin(radians)
    j = np.matrix([[c, s], [-s, c]])
    m = np.dot(j, v)
    return np.array(m)


# scale = np.array([0.3, 0.3])

roi = []
roi_N = 10
for ch in imv:
    for i in range(roi_N):
        row = []
        for j in range(roi_N):
            r = pg.ROI(pos=origin, pen="g")
            roi.append(r)
            imv[ch].addItem(r)

for i, ch in enumerate(Chan):
    im = pg.gaussianFilter(img[ch][0], (1, 1))
    imv[ch].setImage(
        im,
        pos=(0, 0),
        scale=scale,
        autoHistogramRange=True,
        autoRange=False,
        autoLevels=True,
    )


params = [
    {"name": "angle", "type": "float", "value": ang, "step": 0.1},
    {"name": "period", "type": "float", "value": period, "step": 0.1},
    {"name": "a", "type": "float", "value": a, "step": 0.1},
    {"name": "x", "type": "float", "value": origin[0], "step": 0.1},
    {"name": "y", "type": "float", "value": origin[1], "step": 0.1},
    {"name": "sx", "type": "float", "value": beams_shift[0], "step": 0.1},
    {"name": "sy", "type": "float", "value": beams_shift[1], "step": 0.1},
]

## Create tree of Parameter objects

Params = Parameter.create(name="params", type="group", children=params)


def update():
    ang = Params.child("angle").value()
    period = Params.child("period").value()
    a = Params.child("a").value()
    origin = np.array([Params.child("x").value(), Params.child("y").value()])
    beams_shift = np.array([Params.child("sx").value(), Params.child("sy").value()])

    index = 0
    for ch in imv:
        for i in range(roi_N):
            for j in range(roi_N):
                pos = origin + np.array([i, j]) * period
                pos = rotate(pos, -ang)[0]
                print(pos)
                if ch == "2w2+w1" or ch == "2w2":

                    pos += beams_shift
                r = roi[index]
                r.setPos(pos)
                r.setSize((a, a))
                r.setAngle(ang + 45)

                index += 1


update()

## If anything changes in the tree, print a message
def change(param, changes):
    print("tree changes:")
    update()


def valueChanging(param, value):
    print("Value changing (not finalized): %s %s" % (param, value))
    update()


# Too lazy for recursion:
for child in Params.children():
    child.sigValueChanging.connect(valueChanging)
    for ch2 in child.children():
        ch2.sigValueChanging.connect(valueChanging)


Params.sigTreeStateChanged.connect(change)

## Create two ParameterTree widgets, both accessing the same data
t = ParameterTree()
t.setParameters(Params)
l.addWidget(t, 2, 1, 1, 1)
sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
sizePolicy.setHorizontalStretch(0)
sizePolicy.setVerticalStretch(0)
t.setSizePolicy(sizePolicy)
print(scale)
# plt.show()

# w = pg.HistogramLUTWidget()


# d = np.array([img['2w1'][0],img['2w2'][0],img['w1+w2'][0]]).transpose(1,2,0)
# print(d.shape)
# im = pg.ImageItem(d)
# imv['2w2+w1'].addItem(im)
# w.setImageItem(im)
# w.setLevelMode('rgba')

# l.addWidget(w,2,0,1,1)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == "__main__":
    import sys

    if (sys.flags.interactive != 1) or not hasattr(QtCore, "PYQT_VERSION"):
        QtGui.QApplication.instance().exec_()
