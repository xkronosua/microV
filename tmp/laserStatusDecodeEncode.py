import time


def encode(status):
    res = 0
    if status["Emission"]:
        res += 0x00000001
    if not status["Pulsing"]:
        res += 0x00000002
    if status["Main shutter"]:
        res += 0x00000004
    if status["IR shutter"]:
        res += 0x00000008
    if status["Servo on"]:
        res += 0x00000020
    if status["User interlock"]:
        res += 0x00000200
    if status["Keyswitch"]:
        res += 0x00000400
    if status["Power supply"]:
        res += 0x00000800
    if status["Internal"]:
        res += 0x00001000
    if status["Warning"]:
        res += 0x00004000

    if status["fault"]:
        res += 0x00008000
    s = 0
    print(status["State"])
    if status["State"] == "Initializing":
        s = 10  # s >= 0 and s <= 24:
    if status["State"] == "READY":
        s = 25

    if status["State"] == "Turning on / optimizing":
        s = 30  # s>= 26 and s <= 49:
    if status["State"] == "RUN":
        s = 50
    if status["State"] == "Moving to Align mode":
        s = 55  # s>= 51 and s <= 59:
    if status["State"] == "ALIGN":
        s = 60
    if status["State"] == "Exiting Align mode":
        s = 65  # s>=61 and s <= 69:
    print(s, s << 16)
    res += s << 16
    return res


def decode(status):
    resp = {}

    resp["RawSTB"] = status.decode()

    try:
        status = int(status)
    except:
        res = {
            "RawSTB": None,  # "i8"),
            "Emission": None,  # "i2"),
            "Pulsing": None,  # "i2"),
            "Main shutter": None,  # "i2"),
            "IR shutter": None,  # "i2"),
            "Servo on": None,  # "i2"),
            "User interlock": None,  # "i2"),
            "Keyswitch": None,  # "i2"),
            "Power supply": None,  # "i2"),
            "Internal": None,  # "i2"),
            "Warning": None,  # "i2"),
            "fault": 1,  # "i2"),
            "State": "Status is wrong",  # "S20"),
            "ON_OFF": None,  # "i2"),
            "time": 0,  #'float64')
        }
        return res
    # 0 0x00000001 Emission The laser diodes are energized. Laser emission is possible if the
    # 		shutter(s) is (are) open.

    resp["Emission"] = (status & 0x00000001) == 1

    # 1 0x00000002 Pulsing 1 = “pulsing”
    # 		The name PULSING is used for compatibility with Mai Tai. In the
    # 		InSight DS+ system, this bit indicates that the laser has either:
    # 		a) Reached the Run state and can be used to take data.
    # 		or
    # 		b) Reached the Align state and can be used to align the optical
    # 		system.
    resp["Pulsing"] = (status & 0x00000002) != 0

    # 2 0x00000004 Main shutter 1 = The main shutter is open (sensed position).
    resp["Main shutter"] = (status & 0x00000004) != 0

    # 3 0x00000008 Dual beam
    # 		shutter 1 = The IR shutter is open (sensed position).
    # 		N OTE : On systems purchased without the optional dual beam
    # 		output, this bit is always set to 1.
    resp["IR shutter"] = (status & 0x00000008) != 0

    # 5 0x00000020 Servo on 1 = Servo is on (see “SERVO” command on page A-6).
    resp["Servo on"] = (status & 0x00000020) != 0

    # 9 0x00000200 User interlock 1 = The user interlock (CDRH interlock) is open; laser is forced off.
    resp["User interlock"] = status & 0x00000200

    # 10 0x00000400 Keyswitch 1 = The safety keyswitch interlock is open; laser is forced off.
    resp["Keyswitch"] = status & 0x00000400

    # 11 0x00000800 Power supply 1 = The power supply interlock is open; laser is forced off.
    resp["Power supply"] = status & 0x00000800

    # 12 0x00001000 Internal 1 = The internal interlock is open; laser is forced off.
    resp["Internal"] = status & 0x00001000

    # 14 0x00004000 Warning 1 = The system is currently detecting a warning condition. The laser
    # 	continues to operate, but it is best to resolve the issue at your earliest
    # 	convenience.
    # 	Use READ:HISTORY? (page A-4) to see what is causing the
    # 	warning.
    resp["Warning"] = status & 0x00004000

    # 15 0x00008000 fault 1 = The system is currently detecting a fault condition. InSight
    # 	immediately turns off the laser diodes. Use READ:HISTORY? to see
    # 	what is causing the fault.
    # 	Note: The fault condition may clear itself when the laser turns itself
    # 	off. If so, the fault bit clears.
    resp["fault"] = status & 0x00008000

    # 16 to 22 0x007F0000	State
    # 	After masking, shift right 16 bits and interpret as a number with a
    # 	value from 0 to 127 (see Table A-2).
    # 	Most state numbers are not specifically assigned, but several ranges
    # 	can be described and three specific values (25, 50, and 60) are
    # 	guaranteed not to change.
    s = (status & 0x007F0000) >> 16
    print((status & 0x007F0000), s)
    if s >= 0 and s <= 24:
        resp["State"] = "Initializing"
    elif s == 25:
        resp["State"] = "READY"
    elif s >= 26 and s <= 49:
        resp["State"] = "Turning on / optimizing"
    elif s == 50:
        resp["State"] = "RUN"
    elif s >= 51 and s <= 59:
        resp["State"] = "Moving to Align mode"
    elif s == 60:
        resp["State"] = "ALIGN"
    elif s >= 61 and s <= 69:
        resp["State"] = "Exiting Align mode"
    else:
        resp["State"] = ""

    resp["ON_OFF"] = not (resp["State"] in ["Initializing", "READY", ""])

    resp["MODE"] = None
    if resp["State"] == "RUN":
        resp["MODE"] = "RUN"
    elif resp["State"] == "ALIGN" or resp["State"] == "Moving to Align mode":
        resp["MODE"] = "ALIGN"

    resp["time"] = time.time()

    return resp


if __name__ == "__main__":
    import pandas as pd

    df_ = pd.read_csv("laser_states.csv")

    print(df_[["Main shutter", "IR shutter", "State", "status_code"]])
    df = df_.copy()
    df["bit"] = ""
    status_bit_old = []
    status_bit_new = []
    for k in df:
        df[k + "_new"] = 0
    # ,'IR shutter','State','status_code']
    for i, row in df.iterrows():
        # print(row)
        res = {
            "Emission": None,  # "i2"),
            "Pulsing": None,  # "i2"),
            "Main shutter": None,  # "i2"),
            "IR shutter": None,  # "i2"),
            "Servo on": None,  # "i2"),
            "User interlock": None,  # "i2"),
            "Keyswitch": None,  # "i2"),
            "Power supply": None,  # "i2"),
            "Internal": None,  # "i2"),
            "Warning": None,  # "i2"),
            "fault": 1,  # "i2"),
            "State": "Status is wrong",  # "S20"),
            "ON_OFF": None,  # "i2"),
            "time": 0,  #'float64')
        }
        for k in row.keys():
            # print(k)
            res[k] = row[k]

        status_bit_old.append(f"{row['status_code']:b}")
        # print(res)

        # status = b'3932219'
        # r=decode(status)
        # print(r)

        rr = encode(res)
        r1 = decode(str(rr).encode())
        for k in r1:
            if k in df:
                df.loc[i, k + "_new"] = r1[k]
        df.loc[i, "bit"] = f"{row['status_code']:b}"
        df.loc[i, "bit_new"] = f"{rr:b}"

    print(
        df[
            [
                "Main shutter",
                "Main shutter_new",
                "IR shutter",
                "IR shutter_new",
                "State",
                "State_new",
                #'bit','bit_new'
            ]
        ]
    )
