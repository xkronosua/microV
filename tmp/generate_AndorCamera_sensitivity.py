import sys
import exdir
import numpy as np
from matplotlib import pyplot as plt
import argparse
from scipy.interpolate import interp1d, LinearNDInterpolator
from scipy.signal import medfilt
import pandas as pd

parser = argparse.ArgumentParser(
    description="Process AndorCamera sensitivity calibration curves."
)
parser.add_argument(
    "-s", dest="setup", help="Setup: HRS or Microscope", default="Microscope"
)
parser.add_argument(
    "-c",
    dest="configuration",
    help="Configuration of measuements [forward, backward, 90deg, ...]",
    default="forward",
)
parser.add_argument("-d", dest="data", help="Exdir folder with data")
parser.add_argument("-b", dest="background", help="Exdir folder with background")
parser.add_argument(
    "-g",
    dest="grating",
    type=int,
    help="Process data for specific grating [0,1]",
    default=None,
)


args = parser.parse_args()
print(args)

store = exdir.File(args.data, "r")
store_bg = exdir.File(args.background, "r")

data = store["scanND"][max(store["scanND"])]["data"]
data_bg = store_bg["scanND"][max(store_bg["scanND"])]["data"]

if not args.grating is None:
    mask = data["position"]["shamrockGrating"] == args.grating & data["time"] != 0
else:
    mask = data["time"] != 0

filters = data.attrs["filters"].to_dict()
colors = "rgbcmy"
fig, axes = plt.subplots(3, 1, sharex=True)
# axes=[axes]


lamp_data = np.loadtxt("USHIO_BRL_12V50W.csv", delimiter=",")
lamp = interp1d(
    lamp_data[:, 0], lamp_data[:, 1], bounds_error=False, fill_value="extrapolate"
)

norm_intens = (
    data["data"]["AndorCamera"]["raw"]["intensity"].max()
    / data["data"]["AndorCamera"]["raw"]["exposure"].mean()
)
axes[0].plot(lamp.x, lamp.y / lamp.y.max() * norm_intens, "--k", label="lamp")

crop = 200
interps = []
lin_interps = []

fgb = np.loadtxt("FGB37.csv", skiprows=1, delimiter="\t")
T = 1 / (2 / fgb[:, 1] - 1) / 1.1
T = fgb[:, 1] ** 2 / 1.06
axes[1].plot(fgb[:, 0], T, "--k", label="FGB37x2!")
for f_index in filters:

    mask1 = data[mask]["position"]["filtersPiezoStage"] == f_index
    d = data[mask][mask1][1:]
    d_bg = data_bg[mask][mask1][1:]
    mask2 = d["data"]["AndorCamera"]["raw"]["wavelength"].mean(axis=1) > 250

    wl = d["data"]["AndorCamera"]["raw"]["wavelength"][mask2, crop:-crop]

    intens = d["data"]["AndorCamera"]["raw"]["intensity"][mask2, crop:-crop]
    intens_bg = d_bg["data"]["AndorCamera"]["raw"]["intensity"][mask2, crop:-crop]

    exposure = d["data"]["AndorCamera"]["raw"]["exposure"][mask2]
    exposure_bg = d_bg["data"]["AndorCamera"]["raw"]["exposure"][mask2]

    center_wl_ = d["position"]["shamrockWavelength"][mask2]
    center_wl = np.repeat(
        center_wl_.reshape(center_wl_.shape[0], 1), wl.shape[1], axis=1
    )

    intens1 = ((intens - intens_bg).T / exposure).T  # - (intens_bg.T/exposure_bg).T
    # intens1 -= intens1.min()
    axes[0].plot(
        wl.flatten(),
        intens1.flatten(),
        colors[f_index] + ".",
        label=filters[f_index],
        alpha=0.1,
    )
    intens1_corr = intens1 / lamp(wl)

    axes[2].plot(
        wl.flatten(),
        intens1_corr.flatten(),
        colors[f_index] + ".",
        label="_" + filters[f_index],
        alpha=0.01,
    )

    x = wl.flatten()
    xc = center_wl.flatten()
    y = intens1_corr.flatten()
    y = y[x.argsort()]
    xc = xc[x.argsort()]
    x = x[x.argsort()]

    x = medfilt(x, 51)
    y = medfilt(y, 51)
    axes[2].plot(x, y, colors[f_index] + "-", label=filters[f_index])
    iu = interp1d(x, y, bounds_error=False, fill_value="extrapolate")
    lin_interps.append(iu)

    if f_index != 0:
        datasheet_data = np.loadtxt(
            filters[f_index] + ".csv", delimiter="\t", skiprows=1
        )
        datasheet = interp1d(
            datasheet_data[:, 0],
            datasheet_data[:, 1],
            bounds_error=False,
            fill_value="extrapolate",
        )
        axes[1].plot(
            x, datasheet(x), colors[f_index] + "--", label="_" + filters[f_index]
        )
        # y = y**2/datasheet(x)/np.max(y)
        print(y)
    axes[2].plot(x, y, colors[f_index] + "--", label=filters[f_index])
    # y1=y
    # interp = LinearNDInterpolator(np.vstack((xc,x)).T, y.flatten())
    # interps.append(interp)


WL = np.linspace(240, 940, 1000)
res = {"wl": WL}
# zero_len = interps[0].values.shape[0]
for f_index in filters:
    print(f_index)
    # c_wl, wl = interps[f_index].points[:zero_len].T
    wl = lin_interps[f_index].x
    intens = lin_interps[f_index].y
    intens_0 = lin_interps[0].y

    # intens = interps[f_index].values[:zero_len]
    # intens_0 = interps[0].values#(c_wl.reshape(center_wl.shape),wl.reshape(center_wl.shape))
    x = wl
    y = (intens / intens_0).flatten()

    axes[1].plot(x, y, colors[f_index] + ".", label="_" + filters[f_index], alpha=0.01)

    y = y[x.argsort()]
    x = x[x.argsort()]

    x = medfilt(x, 15)
    y = medfilt(y, 15)
    axes[1].plot(x, y, colors[f_index] + "-", label=filters[f_index])

    corr_curve = lin_interps[f_index](WL)
    res[filters[f_index]] = corr_curve

df = pd.DataFrame(res)
df[df.keys()[1:]] = df[df.keys()[1:]] / df["None"].iloc[len(df) // 2]
df.to_csv(args.data.replace(".exdir", ".csv"))


axes[0].legend()
axes[1].legend()
axes[2].legend()

axes[1].set_xlim(280, 1000)
axes[1].set_ylim(0, 1)

axes[0].set_title("Raw")
axes[1].set_title("Test norm by air")
axes[2].set_title("Correction curve")
axes[2].set_ylim(0, res["None"][len(res["None"]) // 2] * 1.5)

plt.legend()
plt.show()
