import Pyro4
from microscope.device_server import device

# ...

# Pyro4.config is a singleton, these changes to config will be
# used for all the device servers.  This needs to be done after
# importing microscope.device_server
Pyro4.config.COMPRESSION = True
Pyro4.config.PICKLE_PROTOCOL_VERSION = 2

# from hardware.lasers.Spectra_Physics_Insight_X3 import InSightX3Laser
from hardware.stages.MoCo_linearStage import MoCo_linearStage
from hardware.stages.Thorlabs_piezoStage_ELLx_bus import ELLx_bus
from hardware.stages.Thorlabs_rotationStage_TDC001 import TDC001_rotationStage
from hardware.stages.Thorlabs_motorizedActuator import KDC101_motorizedActuator

from hardware.controllers.AndorShamrock_spectrometer import AndorShamrock_spectrometer

from hardware.stages.arduinoStage import ArduinoStage
#from hardware.stages.daqmxStage import DAQmxStage

from hardware.cameras.thorlabs_ccs import CCS200


import logging
import sys

# demo = False
demo = True


if "sim" in sys.argv:
    demo = True

logging.info(f"Demo mode:{demo}")
# print(logging.handlers)
# logging.getLogger().setLevel(logging.DEBUG)

ellx_bus_axConfig = {
    "filters": {"address": b"2", "limits": (0, 3), "stepsPerUnit": 31},
    "HWP": {"address": b"0", "limits": (0, 359.9), "stepsPerUnit": int(143360 / 360)},
    "mirror0": {
        "address": b"1",
        "limits": (0, 359.9),
        "stepsPerUnit": int(143360 / 360),
    },
}

ellx_bus = device(
    ELLx_bus,
    host="127.0.0.1",
    port=8002,
    conf={"demo": demo, "axConfig": ellx_bus_axConfig, "serial_number": "DT0424NNA"},
)


ell09_axConfig = {
    "HWP": {"address": b"1", "limits": (0, 720), "stepsPerUnit": int(143360 / 360)}
}


ell09 = device(
    ELLx_bus,
    host="127.0.0.1",
    port=8003,
    conf={"demo": demo, "axConfig": ell09_axConfig, "serial_number": "DO02FRYTA"},
)  # DO02FRYTA


ell18_axConfig = {
    "beamShift_V": {
        "address": b"0",
        "limits": (-359.9, 359.9),
        "stepsPerUnit": int(143360 / 360),
    }
}


ell18 = device(
    ELLx_bus,
    host="127.0.0.1",
    port=8008,
    conf={"demo": demo, "axConfig": ell18_axConfig, "serial_number": "DP03WAHOA"},
)  # DO02FRYTA


arduinoStages_axConfig = {
    "lens_stage": {"motor": 0, "position": 0, "limits": (0, 15), "stepsPerUnit": 1600},
    "knife_stage": {
        "motor": 1,
        "position": 0,
        "limits": (0, 360),
        "stepsPerUnit": 1600,
    },
    "beamShift_H": {
        "motor": 2,
        "position": 0,
        "limits": (-30, 30),
        "stepsPerUnit": 800,
    },
}

arduinoStages = device(
    ArduinoStage,
    host="127.0.0.1",
    port=8006,
    conf={
        "demo": demo,
        "axConfig": arduinoStages_axConfig,
    },
)

beamShift_axConfig = {
    "beamShift_H": {"SerialNum": 27260096,  "limits": (0, 25)},
    "beamShift_V": {"SerialNum": 27260097,  "limits": (0, 25)},

}

beamShift = device(
    KDC101_motorizedActuator,
    host="127.0.0.1",
    port=8009,
    conf={
        "demo": demo,
        "axConfig": beamShift_axConfig
    },
)


DEVICES = [
    beamShift,
    # 	device(InSightX3Laser, host="127.0.0.1", port=8000, conf={'demo':demo}),
    device(MoCo_linearStage, host="127.0.0.1", port=8001, conf={"demo": demo}),
    ellx_bus,
    ell09,
    ell18,
    device(
        AndorShamrock_spectrometer, host="127.0.0.1", port=8004, conf={"demo": demo}
    ),
    device(TDC001_rotationStage, host="127.0.0.1", port=8005, conf={"demo": demo}),
    arduinoStages,
    device(CCS200, host="127.0.0.1", port=8007, conf={"demo": demo}),
    # device(DAQmxStage, host="127.0.0.1", port=8007, conf={'demo':demo}),
    # device(E727, host="127.0.0.1", port=8010, conf={'demo':demo})

]
