import os
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets

if pg.Qt.QT_LIB == "PyQt5":
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
    from PyQt5 import uic
else:
    from PySide2.QtUiTools import QUiLoader

import traceback
import sys
import numpy as np
from scipy.signal import medfilt
from scipy.signal import argrelextrema
from scipy.interpolate import interp1d
import pandas as pd
import csv
import time
import logging
import re
from colorama import Back, Fore, Style, init
import exdir


def list_to_recarray(items, forced_dtype=None):
    formats = None

    if len(items[0][0]) == 3:
        formats = ",".join([i[2] for i in items[0]])
    if not forced_dtype is None:
        formats = ",".join([forced_dtype] * len(items[0]))
    names = ",".join([i[0] for i in items[0]])
    rec_list = [tuple([i[1] for i in item]) for item in items]
    return np.core.records.fromrecords(rec_list, names=names, formats=formats)


def loadUi(path, parent=None, Dir=None, module="", package=""):
    if pg.Qt.QT_LIB == "PyQt5":
        ui = uic.loadUi(str(path), parent)

    else:
        loader = QUiLoader()
        if not Dir is None:
            loader.setWorkingDirectory(str(Dir))
        ui_file = QtCore.QFile(str(path))
        ui_file.open(QtCore.QFile.ReadOnly)
        ui = loader.load(ui_file, parentWidget=parent)
        ui_file.close()
    return ui


class qtSignals(QtCore.QObject):
    objSignal = QtCore.Signal(object)


class qtLoggingSignalEmiter(logging.Handler):
    level = 0
    filters = []
    lock = False
    levelno = 0

    def __init__(self):
        super(logging.Handler).__init__()
        self.new_record = qtSignals()

    def emit(self, record):
        msg = self.format(record)
        self.new_record.objSignal.emit(msg)  # <---- emit signal here


class ColorLogsWrapper(object):
    COLOR_MAP = {
        "debug": Fore.CYAN,
        "info": Fore.GREEN,
        "warning": Fore.YELLOW,
        "error": Fore.RED,
        "critical": Back.RED,
    }

    def __init__(self, logger):
        self.logger = logger

    def __getattr__(self, attr_name):
        if attr_name == "warn":
            attr_name = "warning"
        if attr_name not in "debug info warning error critical":
            return getattr(self.logger, attr_name)
        log_level = getattr(logging, attr_name.upper())
        # mimicking logging/__init__.py behaviour
        if not self.logger.isEnabledFor(log_level):
            return

        def wrapped_attr(msg, *args, **kwargs):
            style_prefix = self.COLOR_MAP[attr_name]
            msg = style_prefix + msg + Style.RESET_ALL
            # We call _.log directly to not increase the callstack
            # so that Logger.findCaller extract the corrects filename/lineno
            return self.logger._log(log_level, msg, args, **kwargs)

        return wrapped_attr


class WorkThread(QtCore.QThread):
    finishedSignal = QtCore.Signal(object)
    args = ()
    kwargs = {}

    def __init__(self, func, args=(), kwargs={}):
        super().__init__()
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def run(self):
        t0 = time.time()
        try:
            r = self.func(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
        t1 = time.time()
        self.finishedSignal.emit({"res": r, "start_time": t0, "dt": t1 - t0})


class WorkerSignals(QtCore.QObject):
    """
    Defines the signals available from a running worker thread.
    Supported signals are:
    finished
            No data
    error
            `tuple` (exctype, value, traceback.format_exc() )
    result
            `object` data returned from processing, anything
    progress
            `int` indicating % progress
    """

    finished = QtCore.Signal()
    error = QtCore.Signal(tuple)
    result = QtCore.Signal(object)
    progress = QtCore.Signal(int)
    status = QtCore.Signal(object)
    stateUpdate = QtCore.Signal()

    # status = pyqtSignal(object)


class Worker(QtCore.QRunnable):
    """
    Worker thread
    Inherits from QtCore.QRunnable to handler worker thread setup, signals and wrap-up.
    :param callback: The function callback to run on this worker thread. Supplied args and
                                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    Usage:
            def execute_this_fn(self, progress_callback):
                    for n in range(0, 5):
                            time.sleep(1)
                            progress_callback.emit(n*100/4)
                    return "Done."

            self.threadpool = QtCore.QThreadPool()
            worker = Worker(self.execute_this_fn) # Any other args, kwargs are passed to the run function
            worker.signals.result.connect(self.print_output)
            worker.signals.finished.connect(self.thread_complete)
            worker.signals.progress.connect(self.progress_fn)

            # Execute
            self.threadpool.start(worker)

    """

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        # self.kwargs['progress_callback'] = self.signals.progress
        self.kwargs["status_callback"] = self.signals.status
        # status_callback'] = self.signals.status

    @QtCore.Slot()
    def stop(self):
        # do thread cleanup, stop thread
        return

    @QtCore.Slot()
    def run(self):
        """
        Initialise the runner function with passed args, kwargs.
        """

        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


"""
def exdirAppend(dataset,data,axis=0):
	'''
	Append data to the end of memmap in exdir dataset
	'''
	tmp_store = np.require(dataset[:], requirements=['O'])
	add_len = data.shape[0]
	tmp_store.resize((tmp_store.shape[0]+add_len, tmp_store.shape[1]),refcheck=False)
	tmp_store[-add_len:,:] = data
	return tmp_store
"""


def exdirAppend(dataset, data):
    """
    Append data to the end of memmap in exdir dataset
    """
    # import pdb; pdb.set_trace()
    axis = 0
    tmp_store = np.require(dataset[:], requirements=["O"])
    if len(data.shape) == 1:
        add_len = 1
    else:
        add_len = data.shape[axis]
    new_shape = np.array(tmp_store.shape)
    new_shape[axis] += add_len
    tmp_store.resize(new_shape, refcheck=False)
    tmp_store[-add_len:] = data
    return tmp_store


def setExdirData(dset, data, shape=None, dtype=None, attrs=None):
    path = dset.root_directory
    parent_path = dset.parent_path
    name = dset.name
    obj_name = dset.object_name

    with exdir.File(path) as s:
        # print(list(s))
        if shape is None:
            shape = data.shape
        if dtype is None:
            dtype = data.dtype

        if s[name].shape != shape or s[name].dtype != dtype:
            if attrs is None:
                attrs = s[name].attrs.to_dict()
            del s[name]
            dset_tmp = s[parent_path].create_dataset(obj_name, data=data)
        else:
            dset_tmp = s[parent_path][
                obj_name
            ]  # .require_dataset(obj_name, shape=shape, dtype=dtype)
            dset_tmp.data[: len(data)] = data
        if not attrs is None:
            for k in attrs:
                dset_tmp.attrs[k] = attrs[k]
        del dset_tmp

    return True


def pyqtSaveTableToCSV(table, path):
    header = []
    out = []
    with open(path, "w") as stream:
        writer = csv.writer(stream)
        header = []
        for col in range(table.columnCount()):
            header_item = table.horizontalHeaderItem(col)
            # if header_item is None:
            # 	continue
            header.append(header_item.text())
        writer.writerow(header)

        for row in range(table.rowCount()):
            rowdata = []
            tmp = []
            for column in range(table.columnCount()):
                item = table.item(row, column)
                if item is not None:
                    rowdata.append(item.text())
                else:
                    rowdata.append("")
                # tmp.append(rowdata)
            out.append(rowdata)
            writer.writerow(rowdata)
    return out, header


def pyqtLoadTableFromCSV(table, path):
    header = []
    out = []
    with open(path, "r") as stream:
        table.setRowCount(0)
        # table.setColumnCount(0)

        for j, rowdata in enumerate(csv.reader(stream)):
            if j == 0:
                header = rowdata
                table.setHorizontalHeaderLabels(header)
                print(header)
                continue
            row = table.rowCount()
            table.insertRow(row)
            table.setColumnCount(len(rowdata))
            tmp = []
            for column, data in enumerate(rowdata):
                item = QtGui.QTableWidgetItem(data)
                table.setItem(row, column, item)
                tmp.append(data)
            out.append(rowdata)
    return out, header


def gen_grid_2D(
    rangeX=(0, 10), rangeY=(0, 10), axis=0, N=100, max_points=2 ** 15, step=2
):
    """Generate zig-zag toolpath with N scans along axis(0-X,1-Y) direction"""
    index = np.arange(N)
    t = np.linspace(0, 1, N)

    if axis == 0:
        x = np.linspace(rangeX[0], rangeX[1], N)
        x = np.insert(x, index, x)
        y = np.zeros(len(x))
        y[1::4] = 1
        y[2::4] = 1
        y = y * (rangeY[1] - rangeY[0]) + rangeY[0]
    else:
        y = np.linspace(rangeY[0], rangeY[1], N)
        y = np.insert(y, index, y)
        x = np.zeros(len(y))
        x[1::4] = 1
        x[2::4] = 1
        x = x * (rangeX[1] - rangeX[0]) + rangeX[0]
    # x = np.append(x,x[-1])
    # y = np.append(y,y[-1])

    r = (np.diff(x) ** 2 + np.diff(y) ** 2) ** 0.5
    segment_length = (r / r.sum() * max_points).astype(int)
    if step != 0:
        xi = [x[0]]
        yi = [y[0]]
        for i in range(len(x) - 1):
            sign = -1 if i % 2 == 0 else 1
            if axis == 0:
                if x[i] == x[i + 1]:
                    y_tmp = np.linspace(y[i], y[i + 1], step)
                    yi.append(y_tmp)
                    xi.append(np.ones(len(y_tmp)) * x[i])
            else:
                if y[i] == y[i + 1]:
                    x_tmp = np.linspace(x[i], x[i + 1], step)
                    xi.append(x_tmp)
                    yi.append(np.ones(len(x_tmp)) * y[i])
        x = np.hstack(xi)
        y = np.hstack(yi)
        return x, y, segment_length
    else:
        return x, y, segment_length


def generate2Dtoolpath(Range_x, Range_y, mask=None, skip=2):
    """zigzag toolpath by X axis with higher desity by mask"""
    Y, X = np.meshgrid(Range_y, Range_x)
    print("gen:", X.shape, Y.shape)
    Yi, Xi = np.meshgrid(np.arange(len(Range_y)), np.arange(len(Range_x)))

    if not mask is None and mask.shape == X.shape:
        newGrid = np.full(X.shape, np.nan)
        newGrid[::skip, ::skip] = X[::skip, ::skip]
        newGrid[skip // 2 :: skip, skip // 2 :: skip] = X[
            skip // 2 :: skip, skip // 2 :: skip
        ]
        newGrid[mask] = X[mask]
    else:
        newGrid = X.copy()
    newGrid = newGrid.T
    newGrid[::2] = newGrid[::2].T[::-1].T
    newGrid = newGrid.T
    Xi = Xi.T
    Xi[::2] = Xi[::2].T[::-1].T
    Xi = Xi.T

    n, m = X.shape
    for i in range(m):
        yield 1, i, Range_y[i]
        for j in range(n):
            val = newGrid[j, i]
            xi = Xi[j, i]
            if not np.isnan(val):
                yield 0, xi, val
            else:
                continue


def zCompensation(ex_wl, zShift=0):
    """
    Z position shift compensation
    """
    cal = np.loadtxt("beamWaistZPosition.txt", skiprows=1)[:, 1:]
    cal[:, 1] = medfilt(cal[:, 1], 3)

    cal = cal[np.unique(cal[:, 0], return_index=True)[-1]]
    cal[:, 0] -= cal[:, 0].min()
    fit = interp1d(
        cal[:, 0],
        cal[:, 1],
        kind="slinear",
        bounds_error=False,
        fill_value="extrapolate",
    )
    return fit(ex_wl) - zShift


def photon_count(signal, t, dt_window, threshold):
    n = int(len(signal) // 10)  # int((t.max()-t.min())//dt_window)
    w = signal < threshold
    if sum(w) == 0:
        return np.zeros(len(t)), t, (t.max() - t.min()) / n
    s = abs(signal * w)
    t = t[w]
    # s = signal.cumsum()

    print("N", n, len(signal), (t.max() - t.min()), dt_window)
    s = np.array([i.sum() for i in np.array_split(s, n)])
    print(s.shape)
    t_new = np.linspace(t.min(), t.max(), len(s))
    return s, t_new, (t.max() - t.min()) / n


# redrawQueue = Queue()


class redrawThread(QtCore.QThread):

    newData = QtCore.Signal(object)
    bgRefresh = QtCore.Signal()
    t0 = time.time()
    running = False

    def __init__(self, q, parent=None):
        super(redrawThread, self).__init__(parent)
        self.q = q

    def __del__(self):
        self.exiting = True
        self.wait()

    def run(self):
        self.running = True
        while self.running:
            if not self.q.empty():
                out = []
                for items in range(0, self.q.qsize()):
                    out.append(self.q.get_nowait())
                self.newData.emit(out)
            # self.q.task_done()
            self.bgRefresh.emit()
            QtCore.QThread.msleep(100)


################ functions #####################################################
def gaussian(x, amp, cen, wid, bg=0):
    """1-d gaussian: gaussian(x, amp, cen, wid, bg)"""
    return bg + (amp / (np.sqrt(2 * np.pi) * wid)) * np.exp(
        -((x - cen) ** 2) / (2 * wid ** 2)
    )
