AXES_UNITS_DTYPES = {
    "X": ("um", "f4"),
    "Y": ("um", "f4"),
    "Z": ("um", "f4"),
    # for relative step
    "dX": ("um", "f4"),
    "dY": ("um", "f4"),
    "dZ": ("um", "f4"),

    "XYZ": ("um", ("f4", 3)),

    "X_target": ("um", "f4"),
    "Y_target": ("um", "f4"),
    "Z_target": ("um", "f4"),
    "Delay_line_position": ("mm", "f4"),
    "Delay_line_position_zero_relative": ("mm", "f4"),
    "laserBeamShift_V": ("mm", "f4"),
    "laserBeamShift_V_angle": ("deg", "f4"),
    "laserBeamShift_H": ("mm", "f4"),
    "Lens_position": ("mm", "f4"),
    "Knife_position": ("mm", "f4"),
    "HWP_TUN_angle": ("deg", "f4"),
    "HWP_TUN_power": ("W", "f4"),
    "HWP_TUN_intens": ("W/cm**2", "f4"),
    "HWP_FIX_angle": ("deg", "f4"),
    "HWP_FIX_power": ("W", "f4"),
    "HWP_FIX_intens": ("W/cm**2", "f4"),
    "HWP_Polarization": ("deg", "f4"),
    "LaserWavelength": ("nm", "u2"),
    "Laser_precompensation": ("dimensionless", "f2"),
    "LaserShutter": ("dimensionless", "u1"),
    "filtersPiezoStage": ("dimensionless", "u1"),
    "shamrockWavelength": ("nm", "f4"),
    "shamrockGrating": ("dimensionless", "u1"),
    "shamrockFocusingMirror": ("dimensionless", "u2"),
    "andorCameraExposure": ("s", "f4"),
    "ThorCameraExposure": ("s", "f4"),
    "CenterIndex": ("dimensionless", "u2"),
    "commutation_mirror0": ("deg", "f4"),
    "commutation_mirror1": ("deg", "f4"),
    "commutation_mirror2": ("dimensionless", "f4"),
    "None": ("s", "f8"),
}

PARAMETERS_UNITS_DTYPES = {
    "Confocal parameter": ("m", "f4"),
    "Waist radius 1/e**2": ("m", "f4"),
    "Waist diameter FWHM": ("m", "f4"),
    "Average power": ("W", "f4"),
    "Peak intensity": ("W/cm**2", "f4"),
    "Average power density": ("W/cm**3", "f4"),
    "Peak power density": ("W/cm**3", "f4"),
    "Pulsewidth": ("s", "f4"),
    "Ex_wavelength": ("nm", "u2"),
    "angle": ("deg", "f4"),
    "w0": ("m", "f4"),
}


READOUT_SOURCES_UNITS_DTYPES = {
    "Powermeter": {"Power": ("W", "f4")},
    "AndorCamera": {"exposure": ("s", "f4")},
}
