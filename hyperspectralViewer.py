#!/usr/bin/python3

import sys
from PyQt5.QtWidgets import (
    QApplication,
    QFileSystemModel,
    QTreeView,
    QTabWidget,
    QWidget,
    QVBoxLayout,
    QGridLayout,
    QTextBrowser,
    QPlainTextEdit,
    QStatusBar,
    QMainWindow
)
from PyQt5.QtCore import pyqtSignal as Signal
from PyQt5.QtGui import QIcon
import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
import exdir
import logging
import traceback
import os
import pandas as pd
from pyqtgraph.dockarea import DockArea, Dock
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import (
    Parameter,
    ParameterTree,
    ParameterItem,
    registerParameterType,
)
from pathlib import Path

import syntax
from scipy.ndimage import gaussian_filter
from scipy.spatial import cKDTree
from scipy.signal import medfilt
from scipy.interpolate import interp1d
import time
from numpy.lib import recfunctions as rfn
import numpy_indexed as npi
import yaml

from utils.tableOfLines import TableOfLines

QtCore.pyqtRemoveInputHook()
import resources
import pprint
def unique(array):
    uniq, index = np.unique(array, return_index=True)
    return uniq[index.argsort()]


def unique_dummy(array, return_index=False):

    res = [array[0]]
    index = [0]
    for i, v in enumerate(array):
        if v != res[-1]:
            index.append(i)
            res.append(v)
    if return_index:
        return np.array(res), np.array(index)
    else:
        return np.array(res)


def group_by(array, raw):
    indexes = np.unique(array, return_index=True)[1]
    indexes_ = sorted(indexes)
    indexes_.append(-1)
    return (raw[slice(indexes_[i], indexes_[i + 1])] for i in range(len(indexes_) - 1))


def unique_raw(array):
    indexes = np.unique(array, return_index=True)[1]
    return np.array([array[index] for index in sorted(indexes)])


def unique_(a, return_index=False):
    if len(a)<=1:
        if return_index:
            return a, 0
        else:
            return a
    index = np.arange(a.size)[abs(np.diff(a + 0.1, prepend=a[0] * 2)) > 0]
    if return_index:
        return a[index], index
    else:
        return a[index]


class axis_ComplexParameter(pTypes.GroupParameter):
    value_changed = Signal(object)

    def __init__(self, index, ax_name, dset, axes=None, **opts):
        opts["type"] = "bool"
        opts["value"] = True
        pTypes.GroupParameter.__init__(self, **opts)

        self.dset = dset
        if axes is None:
            axes = self.dset["position"].dtype.names
        self.addChild(
            {
                "name": "Axis",
                "type": "list",
                "values": ["--"] + list(axes),
                "value": ax_name,
            }
        )
        self.addChild(
            {
                "name": "Value",
                "type": "list",
                "values": unique_(self.dset["position"][ax_name]).tolist(),
            }
        )
        self.addChild({"name": "Round", "type": "int", "value": -1})

        self.Axis = self.param("Axis")
        self.Value = self.param("Value")
        self.Axis.sigValueChanged.connect(self.axisChanged)
        self.Value.sigValueChanged.connect(self.valueChanged)

    def setParams(self, params):
        self.params = params

    def axisChanged(self):
        print(self.Axis.value())
        axis = self.Axis.value()
        # self.refreshTreeValues()
        self.value_changed.emit(
            {"axis": self.Axis.value(), "value": self.Value.value()}
        )

    def valueChanged(self):
        # self.refreshTreeValues()

        self.value_changed.emit(
            {"axis": self.Axis.value(), "value": self.Value.value()}
        )

class FileSystemModel(QtGui.QFileSystemModel):
    def columnCount(self, parent = QtCore.QModelIndex()):
        return super(FileSystemModel, self).columnCount()+1

    def data(self, index, role):
        if index.column() == self.columnCount() - 1:
            if role == QtCore.Qt.DisplayRole:
                tmp = Path(self.filePath(index))/"attributes.yaml"
                if tmp.exists():
                    #print(self.filePath(index))
                    res = ""
                    with open(tmp, 'r') as file:
                        info = yaml.safe_load(file)
                        res = info['README']
                    return res
                else:
                    return ""
            if role == QtCore.Qt.TextAlignmentRole:
                return QtCore.Qt.AlignHCenter
        return super(FileSystemModel, self).data(index, role)

def map_polar_resp_on_image(imgView, dset, band="2w1", pw_garbadge=None):
    cmap = pg.ColorMap(pos=[0.0, 1.0], color=[(0,0,255),(255,0,0)])


    img = dset.parent["img"][0]
    #if np.all(imgView.getImageItem().image == img['image_roi']):
    #    return
    imgView.setImage(img['image_roi'],pos=img['roi_pos'],
    scale=img['scale'])

    data = dset
    info = data.attrs['centerIndex_info']
    #scatter = pg.ScatterPlotItem(x=info['X'],y=info['Y'],pen='r')
    #imgView.addItem(scatter)
    #tmp_items.append(scatter)

    bg_index = info['Type'].index(b'BG')
    bg_mask = data['position']['CenterIndex'] == bg_index
    bg_ang = data[bg_mask]['position']['HWP_Polarization']*2
    bg_sig = data[bg_mask]['data']['AndorCamera']['data'][band]
    interp_bg = interp1d(bg_ang, bg_sig, bounds_error=False, fill_value=np.nan)

    for centerIndex in range(len(info['X'])):
        if centerIndex == bg_index:
            continue
        print(centerIndex)
        mask = data['position']['CenterIndex'] == centerIndex
        ang = data[mask]['position']['HWP_Polarization']*2
        sig = data[mask]['data']['AndorCamera']['data'][band]

        view_size = min(img['image_roi'].shape*img['scale'])
        norm_sig = view_size / max(10,len(info['X'])/2)

        sig1 = medfilt(sig-interp_bg(ang), 5)
        ptp_sig = sig1.max()#np.ptp(sig1)
        sig1 = sig1 / ptp_sig * norm_sig

        x = sig1 * np.cos(np.deg2rad(ang)) + info["X"][int(centerIndex)]
        y = sig1 * np.sin(np.deg2rad(ang)) + info["Y"][int(centerIndex)]

        curve = f"NP{centerIndex}_curve"
        if not curve in pw_garbadge:
            curveItem = pg.PlotCurveItem(x=x,y=y, pen=pg.mkPen(color=cmap.map(ptp_sig),width=3))
            pw_garbadge[curve] = curveItem
            imgView.addItem(curveItem)
        else:
            pw_garbadge[curve].setData(x,y)
            pw_garbadge[curve].setPen(pg.mkPen(color=cmap.map(ptp_sig),width=3))


        label = f"NP{centerIndex}_label"
        if not label in pw_garbadge:
            labelItem = pg.TextItem(color=(200,10,10))
            effect = QtGui.QGraphicsDropShadowEffect()
            effect.setOffset(1,1)

            labelItem.setGraphicsEffect(effect)
            pw_garbadge[label] = labelItem
            imgView.addItem(labelItem)
        pw_garbadge[label].setPos(info["X"][int(centerIndex)],info["Y"][int(centerIndex)])
        pw_garbadge[label].setHtml(f"<span style='font-size: 12pt'>{centerIndex}")





class HyperspectralViewer(QMainWindow):
    zIndex = 0
    mouse_last_pos = {}
    _active_axes = []
    prev_params_raw = []
    _map_polar_resp_on_image_last_dset = ""

    def __init__(self):
        super(HyperspectralViewer,self).__init__()
        self.settings = QtCore.QSettings(
            "gui_hyperspecralViewer.ini", QtCore.QSettings.IniFormat
        )
        self.initUI()

    def initUI(self):
        self.setWindowTitle("HyperspectralViewer")
        # self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar = QStatusBar()
        self.setStatusBar(self.statusBar)

        self.model = FileSystemModel()#QFileSystemModel()
        self.model.setRootPath(QtCore.QDir.currentPath())

        self.tree = QTreeView()

        self.tree.setModel(self.model)

        self.tree.header().resizeSection(0, 300)

        if "filePath" in self.settings.allKeys():
            path = self.settings.value("filePath")
        else:
            path = str(Path(QtCore.QDir.currentPath()).parent)

        self.model.index(path)
        self.tree.model().index(path)
        self.tree.model().setRootPath(path)
        self.tree.setRootIndex(self.tree.model().index(path))

        self.tree.setAnimated(False)
        self.tree.setIndentation(20)
        self.tree.setSortingEnabled(True)

        self.tree.setWindowTitle("Dir View")
        #self.model.appendColumn()
        # self.tree.resize(640, 480)

        self.windowLayout = QGridLayout()

        self.dockarea = DockArea()
        self.windowLayout.addWidget(self.dockarea, 0, 0, 1, 3)
        self.docks = {}

        self.pw = {}
        self.pw_garbadge = {}
        self.grids = {}
        for view in ["img0", "img1"]:
            self.docks[view] = Dock(view, closable=True)  # , size=(500,400))

            self.pw[view] = pg.ImageView()
            self.pw[view].view.invertY(True)
            self.grids[view] = pg.GridItem()

            self.grids[view].setPen(pg.mkPen("y"))

            self.pw[view].addItem(self.grids[view])
            self.grids[view].scene().sigMouseClicked.connect(self.onImgMouseClicked)
            self.grids[view].scene().setObjectName(f"grid_scene_{view}")
            self.pw[view].getView().setAspectLocked(False)

            self.docks[view].addWidget(self.pw[view])



        self.lines = {}
        proxy = []
        for view in ["RAW", "DATA"]:

            self.docks[view] = Dock(view)  # , size=(500,400))
            self.pw[view] = pg.PlotWidget(
                name=view.replace("graph","")
            )  ## giving the plots names allows us to link their axes together
            self.pw[view].addLegend()
            self.docks[view].addWidget(self.pw[view])
            self.lines[view] = {}
            self.pw[view].scene().setObjectName(f"scene_{view}")

            # proxy.append(pg.SignalProxy(
            #     self.pw[view].scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved
            # ))
            self.pw[view].scene().sigMouseMoved.connect(self.mouseMoved)

            self.pw[view].scene().sigMouseClicked.connect(self.mouseClicked)

            self.mouse_last_pos[view] = (0,0)

        w = QWidget()
        l = QGridLayout()
        w.setLayout(l)
        self.docks["path"] = Dock("path")  # , size=(500,400))
        self.dockarea.addDock(self.docks["path"], "bottom")
        self.docks["path"].setStretch(x=3, y=3)
        l.addWidget(self.tree, 1, 0, 1, 3)
        self.docks["path"].addWidget(w)
        self.exdirPath = QtGui.QLineEdit(path)
        self.exdirPath_prev = self.exdirPath.text()

        l.addWidget(self.exdirPath, 2, 0, 1, 1)
        self.exdirPath_open = QtGui.QToolButton()
        l.addWidget(self.exdirPath_open, 2, 1, 1, 1)

        self.exdirPath_open.clicked.connect(self.exdirPath_open_dialog)
        self.exdirPath.editingFinished.connect(self.exdirPath_set)

        cw = QWidget()
        cw.setLayout(self.windowLayout)
        self.setCentralWidget(cw)

        self.tree.clicked.connect(self.on_treeView_clicked)


        self.dockarea.addDock(
            self.docks["img0"], "top"
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
        for view in self.docks:
            if "img" in view and view != "img0":
                self.dockarea.addDock(
                    self.docks[view], "above", self.docks["img0"],
                )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)

        self.dockarea.addDock(
            self.docks["RAW"], "right", self.docks["img0"]
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
        self.dockarea.addDock(
            self.docks["DATA"], "below", self.docks["RAW"]
        )  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)

        self.config_tree = ParameterTree()
        # self.windowLayout.addWidget(self.config_tree,0,3,1,3)
        self.docks["config_tree"] = Dock("Config", closable=True)

        self.docks["config_tree"].addWidget(self.config_tree)
        self.docks["config_tree"].setStretch(x=5, y=3)

        self.update_graph_data_btn = QtGui.QPushButton("Update")
        self.docks["config_tree"].addWidget(self.update_graph_data_btn)
        self.update_graph_data_btn.clicked.connect(self.update_graph_data)

        self.clean_graph_btn = QtGui.QPushButton("Clean")
        self.docks["config_tree"].addWidget(self.clean_graph_btn)
        self.clean_graph_btn.clicked.connect(self.clean_graph)

        self.export2csv_btn = QtGui.QPushButton("Export to csv")
        self.docks["config_tree"].addWidget(self.export2csv_btn)
        self.export2csv_btn.clicked.connect(self.export2csv)

        self.dockarea.addDock(self.docks["config_tree"], "right")

        # import pdb; pdb.set_trace()
        #self.toolbox_lines = QtGui.QToolBox()

        #self.table_lines = {"raw": QtGui.QTableWidget(), "data": QtGui.QTableWidget()}
        self.linesManager = TableOfLines(max_lines_N=100)

        # self.docks['config_tree'].addWidget(self.toolbox_lines)
        self.docks["lines"] = Dock("lines")  # , size=(500,400))
        self.docks["lines"].addWidget(self.linesManager)

        self.dockarea.addDock(self.docks["lines"], "bottom", self.docks["config_tree"])
        self.docks["lines"].setStretch(x=3, y=3)

        self.linesManager.addPage("DATA", self.pw["DATA"])
        self.linesManager.addPage("RAW", self.pw["RAW"])



        self.docks["README"] = Dock("README")  # , size=(500,400))
        self.readme = QPlainTextEdit()#QTextBrowser()
        self.highlight = syntax.PythonHighlighter(self.readme.document())
        self.readme.setPlainText("""
        for i in range(5):
            print(i)
        """)
        self.docks["README"].addWidget(self.readme)
        self.dockarea.addDock(self.docks["README"], "below", self.docks["lines"])
        self.docks["README"].setStretch(x=3, y=3)


        for name in self.pw:
            self.pw_garbadge[name] = {}
        self.show()

    def mouseMoved(self, evt, view_name=None):
        pos = evt  # [0]\
        if self.sender() is None:
            return
        if self.sender().parent() is None:
            return
        vb = self.sender().parent().plotItem.vb

        if self.sender().parent().sceneBoundingRect().contains(pos):
            try:
                mousePoint = vb.mapSceneToView(pos)
            except:
                return
            self.statusBar.showMessage(
                f"[{mousePoint.x(), mousePoint.y()}]"
            )


    def init_highlight(self):
        self.highlight = syntax.PythonHighlighter(self.readme.document())

    def onImgMouseClicked(self, event):
        view = self.sender().objectName().split("_")[-1]
        pos = self.grids[view].mapFromScene(event.scenePos())
        w = self.activeDataset[self.zIndex]["data"]["AndorCamera"]["time"] != 0
        raw = self.activeDataset[self.zIndex]["data"]["AndorCamera"]["raw"][w]

        X = self.activeDataset[self.zIndex]["metadata"]["X"][w]
        Y = self.activeDataset[self.zIndex]["metadata"]["Y"][w]
        combined_x_y_arrays = np.dstack([X, Y])[0]

        name = f"{pos.x():.3f},{pos.y():.3f}"
        idx = cKDTree(combined_x_y_arrays).query([pos.x(), pos.y()])[1]

        raw = raw[idx]
        # import pdb; pdb.set_trace()
        wl = raw["wavelength"]
        exposure = raw["exposure"]
        intens = raw["intensity"] / exposure

        self.linesManager.addUpdateLine("RAW", "RAW", "", x=wl, y=intens)

    def clean_graph(self):
        self.linesManager.removeAll()
        self._map_polar_resp_on_image_last_dset = ""
        #self.drop_garbadge()

    def drop_garbadge(self):
        for view in self.pw_garbadge:
            for key in self.pw_garbadge[view]:
                self.pw[view].removeItem(self.pw_garbadge[view][key])

            self.pw_garbadge[view] = {}

    def export2csv(self):
        axes = [child["Axis"] for child in self.params.child("Axes").childs]

        def rec_ask(axes, res_ax=[], res_pos=[]):
            if len(axes) > 0:
                item, ok = QtGui.QInputDialog.getItem(
                    self,
                    "Select axis and data to export",
                    "Axis",
                    ["*"] + axes,
                    0,
                    False,
                )
                if ok:
                    res_ax.append(item)
                    if item == "*":
                        return res_ax, res_pos
                    axes.remove(item)

                    pos = unique(self.activeDataset["position"][item])
                    pos_str = pos.astype("str").tolist()
                    item, ok = QtGui.QInputDialog.getItem(
                        self,
                        "Select axis and data to export",
                        "Axis",
                        ["*"] + pos_str,
                        0,
                        False,
                    )
                    if ok:
                        if item == "*":
                            res_pos.append("*")
                            return res_ax, res_pos
                        p = pos[pos_str.index(item)]
                        res_pos.append(p)
                        rec_ask(axes, res_ax, res_pos)
                    else:
                        return
                else:
                    return
            return res_ax, res_pos

        axes2extr, positions2extr = rec_ask(axes)
        print(axes2extr, positions2extr)
        path = QtGui.QFileDialog.getSaveFileName(
            self, "Save to:", "", "Text Files (*.csv)"
        )[0]

        w = self.activeDataset["time"] != 0
        data = self.activeDataset[w]

        axes_ = list(data["position"].dtype.names)
        print(axes_)
        # import pdb;pdb.set_trace()
        if len(axes_) == 0:
            return
        elif len(axes_) == 1:
            ax = data["position"].dtype.names[0]
            tmp = {ax: data["position"][ax]}
            sources = data["data"].dtype.names
            for src in sources:
                lines_name = list(data["data"][src]["data"].dtype.names)
                for name in lines_name:
                    tmp[name] = data["data"][src]["data"][name]
            df = pd.DataFrame(tmp)

            df.to_csv(path.replace(".csv", ax + ".csv"), index=False)

            source = self.params.child("Data").child("Source").value()
            lines_name = list(self.activeDataset["data"][source]["raw"].dtype.names)
            for d in data:
                positions = (
                    f"{d['position'].tolist()}"[1:-1].replace(",", "_").replace(" ", "")
                )
                tmp = {}
                if source == "PicoScope":
                    for i, name in enumerate(lines_name):
                        tmp[name] = d["data"][source]["raw"][name]

                elif source == "AndorCamera":
                    tmp["wavelength"] = d["data"][source]["raw"]["wavelength"]
                    tmp["intensity"] = (
                        d["data"][source]["raw"]["intensity"]
                        / d["data"][source]["raw"]["exposure"]
                    )
                df = pd.DataFrame(tmp)
                print(positions)

                df.to_csv(path.replace(".csv", positions + ".csv"), index=False)
        else:
            groups = np.split(
                data, unique_dummy(data["position"][axes_[:-1]], return_index=True)[1]
            )[1:]

            for d in groups:
                if len(d) == 0:
                    continue
                positions = (
                    f"{d['position'][axes_[:-1]][0].tolist()}"[1:-1]
                    .replace(",", "_")
                    .replace(" ", "")
                )
                tmp = {}
                sources = d["data"].dtype.names
                tmp[axes_[-1]] = d["position"][axes_[-1]]
                for src in sources:
                    lines_name = list(d["data"][src]["data"].dtype.names)

                    for name in lines_name:
                        tmp[name] = d["data"][src]["data"][name]
                df = pd.DataFrame(tmp)
                print(positions)

                df.to_csv(path.replace(".csv", positions + ".csv"), index=False)
            for i, ax in enumerate(axes2extr):
                if ax == "*":
                    break
                if positions2extr[i] == "*":
                    continue
                w1 = self.activeDataset["position"][ax] == positions2extr[i]
                w = w & w1
            data = self.activeDataset[w]
            # np.split(a[:,1], np.unique(a[:,0], return_index = True)[1])[1:]
            # np.split(a, np.unique(a[...,:-1],axis=0, return_index = True)[1])[1:]
            print(data.shape)
            source = self.params.child("Data").child("Source").value()
            lines_name = list(self.activeDataset["data"][source]["raw"].dtype.names)
            for d in data:
                positions = f"{d['position'].tolist()}"[1:-1].replace(", ", "_")
                tmp = {}
                if source == "PicoScope":
                    for i, name in enumerate(lines_name):
                        tmp[name] = d["data"][source]["raw"][name]

                elif source == "AndorCamera":
                    tmp["wavelength"] = d["data"][source]["raw"]["wavelength"]
                    tmp["intensity"] = (
                        d["data"][source]["raw"]["intensity"]
                        / d["data"][source]["raw"]["exposure"]
                    )
                df = pd.DataFrame(tmp)
                print(positions)

                df.to_csv(path.replace(".csv", positions + ".csv"), index=False)

    def refreshTreeValues(self):
        self._lock_update_graph_data = True
        pos = self.activeDataset["position"].copy()
        index = np.arange(len(pos))
        if "AndorCamera" in self.activeDataset["data"].dtype.names:
            index = index[1:]
        index_prev = index.copy()

        for i, child in enumerate(self.params.child("Axes").childs):
            if child["Axis"] == "--":
                continue
            ax = child["Axis"]
            if not ax in pos.dtype.names: continue
            pos_ = pos[ax][index]
            Round_digits = child["Round"]
            if Round_digits > 0:
                pos_ = pos[ax][index].round(Round_digits)

            unique_vals = unique_raw(pos_)
            prev_value = child["Value"]
            child.child("Value").setLimits(unique_vals.tolist())

            dist_ = abs(unique_vals - prev_value)
            if len(dist_) == 0: continue
            new_value = unique_vals[dist_.argmin()]
            child.child("Value").setValue(new_value)

            index_prev = index.copy()
            index = index[pos_ == new_value]

        data_raw = self.activeDataset[index]
        # data_raw = rfn.apply_along_fields(np.mean, data_raw)
        try:
            data_raw = data_raw[0]
        except:
            pass
        data = self.activeDataset[index_prev]
        self._lock_update_graph_data = False
        return data, data_raw

    def update_graph_data(self, info=None):
        if self._lock_update_graph_data: return
        # import pdb;pdb.set_trace()
        print(info)
        smooth = [
            self.params.child("Plot").child("Smooth" + s).value() for s in ["X", "Y"]
        ]
        print(f"{smooth=}")
        as_log10 = self.params.child("Plot", "log10").value()
        window = self.params.child("Plot", "window").value()
        baseline0 = self.params.child("Plot", "0->Baseline").value()
        baseline_min = self.params.child("Plot", "min->Baseline").value()
        gen_polar_resp = self.params.child("Plot", "Generate polar. resp.").value()
        medfilt_spectrum = self.params.child("Plot", "medfilt").value()

        if self.expType == "MultiScan" or self.expType == "scanND":
            axes = [
                child["Axis"]
                for child in self.params.child("Axes").childs
                if child["Axis"] != "--"
            ]
            values = [
                child["Value"]
                for child in self.params.child("Axes").childs
                if child["Axis"] != "--"
            ]
            data, data_raw = self.refreshTreeValues()
            source = self.params.child("Data", "Source").value()

            lines_name = list(self.activeDataset["data"][source]["data"].dtype.names)
            if "time" in lines_name:
                lines_name.remove("time")
            for i, name in enumerate(lines_name):
                if not axes[-1] in data["position"].dtype.names: continue
                x = data["position"][axes[-1]]
                y = data["data"][source]["data"][name]
                dset_name = self.activeDataset.name

                self.linesManager.addUpdateLine("DATA", name, dset_name, x=x, y=y, symbol='o', symbolSize=5)

            if "img" in self.activeDataset.parent and "HWP_Polarization" in self.activeDataset["position"].dtype.names:
                name_tmp = self.activeDataset.name
                if self._map_polar_resp_on_image_last_dset != name_tmp and gen_polar_resp:
                    self.drop_garbadge()
                    map_polar_resp_on_image(self.pw["img0"], self.activeDataset,band='2w1', pw_garbadge=self.pw_garbadge["img0"])
                    self._map_polar_resp_on_image_last_dset = name_tmp

            elif "raw" in self.activeDataset["data"][source].dtype.names and len(data_raw)>0:

                lines_name = list(self.activeDataset["data"][source]["raw"].dtype.names)
                if source == "PicoScope":
                    if "time" in lines_name:
                        lines_name.remove("time")
                    for i, name in enumerate(lines_name):
                        x = data_raw["data"][source]["raw"]["time"]
                        y = data_raw["data"][source]["raw"][name]

                        self.linesManager.addUpdateLine("RAW", name, "", x=x, y=y, pen=pen)

                elif source == "AndorCamera":
                    setup = self.params.child("Data").child("Setup").value()
                    fix_sensitivity = self.params.child("Data").child("Fix").value()

                    x = data_raw["data"][source]["raw"]["wavelength"]
                    y = (
                        data_raw["data"][source]["raw"]["intensity"]
                        / data_raw["data"][source]["raw"]["exposure"]
                    )
                    w = self.activeDataset["time"] != 0
                    if baseline0:
                        data_raw0 = self.activeDataset[w][0]
                        y0 = (
                            data_raw0["data"][source]["raw"]["intensity"]
                            / data_raw0["data"][source]["raw"]["exposure"]
                        )
                        y -= y0

                    self.linesManager.addUpdateLine("RAW", name, "", x=x, y=y)

                    wl = data["data"]["AndorCamera"]["raw"]["wavelength"][0]
                    position = data["position"][data["position"].dtype.names[-1]]

                    scale = (np.ptp(position) / len(position), np.ptp(wl) / len(wl))
                    print(scale, np.ptp(wl), wl.min(), wl.max())
                    pos = (position[0], wl[0])
                    print(pos)
                    img = data["data"]["AndorCamera"]["raw"]["intensity"]
                    if smooth != [0, 0]:
                        img = gaussian_filter(img, smooth)
                    if as_log10:
                        img = np.log10(img)

                        img[img <= 0] = 0
                        img[np.isinf(img)] = 0

                    #self.drop_garbadge()

                    self.pw["img1"].setImage(
                        img,
                        scale=scale,
                        pos=pos,
                        autoHistogramRange=True,
                        autoLevels=True,
                    )

        elif self.expType == "scan3D":
            # import pdb; pdb.set_trace()
            axes = [child["Axis"] for child in self.params.child("Axes").childs]
            values = [child["Value"] for child in self.params.child("Axes").childs]
            for i, ax in enumerate(axes):
                value = values[i]
                try:
                    index = (
                        self.params.child("Axes")
                        .childs[i]
                        .child("Value")
                        .opts["values"]
                        .index(value)
                    )
                except:
                    continue
                values[i] = unique(self.activeDataset["position"][ax])[index]

            source = self.params.child("Data").child("Source").value()
            # import pdb; pdb.set_trace()
            for d in self.activeDataset:
                if d["time"] == 0:
                    continue
                w = d["data"][source]["time"] != 0
                X = d["metadata"]["X"]
                Y = d["metadata"]["Y"]
                dy = np.diff(Y, axis=1).mean(axis=0)

                dy = dy[dy > 0].mean()
                dx = np.diff(X, axis=0).mean(axis=0)
                dx = dx[dx > 0].mean()
                scale = (dx, dy)

                print("scale:", scale)
                pos = (X.flatten()[0], Y.flatten()[0])
                if np.any(np.isnan(scale)):
                    scale = (1, 1)
                    pos = (0, 0)
                    continue
                if source == "AndorCamera":
                    raw = d["data"]["AndorCamera"]["raw"][:, :, self.zIndex]
                    # exposure = raw['exposure']
                    img = raw["intensity"]
                elif source == "PicoScope":
                    #import pdb; pdb.set_trace()
                    raw = d["data"]["PicoScope"]["data"]
                    img = np.vstack([raw[x] for x in raw.dtype.names])

                if smooth != [0, 0]:
                    img = gaussian_filter(img, smooth + [int(window)])
                if as_log10:
                    img = np.log10(img)
                    img[img <= 0] = 0
                    img[np.isinf(img)] = 0
                if baseline_min:
                    w_min = np.where(img.sum(axis=2) == img.sum(axis=2).min())
                    img = img[:, :] - img[w_min].mean(axis=0)
                if medfilt_spectrum >= 3:
                    if medfilt_spectrum % 2 == 0:
                        medfilt_spectrum += 1
                    img = np.apply_along_axis(medfilt, 2, img, medfilt_spectrum)

                if source == "AndorCamera":
                    z = d["data"]["AndorCamera"]["raw"]["wavelength"][0, 0, 0]
                else:
                    z = np.arange(len(img.T))
                print(z)

                #self.drop_garbadge()

                self.pw["img1"].setImage(
                    img.transpose(2, 0, 1),
                    scale=scale,
                    pos=pos,
                    autoHistogramRange=True,
                    autoLevels=True,
                    xvals=z,
                )




    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def on_treeView_clicked(self, index):
        self._lock_update_graph_data = True
        self.tree.resizeColumnToContents(True)

        indexItem = self.model.index(index.row(), 0, index.parent())

        path = self.model.filePath(indexItem)
        print(path)
        self.dataType = ""
        if not ".exdir" in path:
            return
        try:
            if "attributes.yaml" in path:

                parent_index = 0
                filePath = Path(path)
                for i, p in enumerate(filePath.parts):
                    if "exdir" in p:
                        parent_index = i
                print(filePath, parent_index, len(filePath.parts))

                with exdir.File(str(Path(*filePath.parts[: parent_index + 1])), "r") as store:
                    path_tmp = "/".join(filePath.parts[parent_index+1:-1])
                    print(path_tmp)
                    readme = store[path_tmp].attrs.to_dict()
                    readme = pprint.pformat(readme)
                    self.readme.setPlainText(readme)#str(readme))

                return
            else:
                parent_index = 0
                filePath = Path(path)
                for i, p in enumerate(filePath.parts):
                    if "exdir" in p:
                        parent_index = i
                print(filePath, parent_index, len(filePath.parts))
                try:
                    del self.activeDataset
                    self.store.close()
                except:
                    pass
                self.store = exdir.File(str(Path(*filePath.parts[: parent_index + 1])), "r")

                if len(filePath.parts) == parent_index + 2:
                    expType = filePath.parts[parent_index + 1]
                    timestamp = max(self.store[expType])
                elif len(filePath.parts) == parent_index + 3:
                    expType = filePath.parts[parent_index + 1]
                    timestamp = filePath.parts[parent_index + 2]
                elif len(filePath.parts) == parent_index + 5:
                    expType = filePath.parts[parent_index + 1]
                    timestamp = filePath.parts[parent_index + 2]
                else:
                    return
                self.expType = expType
                if 'README' in self.store[expType][timestamp].attrs:
                    readme = self.store[expType][timestamp].attrs['README']
                    readme = pprint.pformat(readme)
                    self.readme.setPlainText(readme)
                else:
                    self.readme.setPlainText("")
                self.activeDataset = self.store[expType][timestamp][
                    "data"
                ]  # [self.store[expType][timestamp]['data']['time']!=0]

                print(self.store, timestamp, expType, self.activeDataset.dtype)
                axes = self.activeDataset["position"].dtype.names


                metadata = None
                if "metadata" in self.activeDataset:
                    metadata = self.activeDataset["metadata"].dtype.names

                sources = self.activeDataset["data"].dtype.names
                ax_param = []
                #import pdb; pdb.set_trace()
                if len(set(self._active_axes).intersection(axes)) < len(axes):
                    self._active_axes = axes
                    for i, name in enumerate(axes):

                        ax_param.append(
                            axis_ComplexParameter(
                                i,
                                name,
                                self.activeDataset,
                                axes=self._active_axes,
                                name=f"Axis{i}",
                            )
                        )
                        ax_param[-1].value_changed.connect(self.update_graph_data)
                    params = [
                        {
                            "name": "Axes",
                            "type": "group",
                            "children": ax_param,
                        },
                        {
                            "name": "Data",
                            "type": "group",
                            "children": [
                                {
                                    "name": "Source",
                                    "type": "list",
                                    "values": sources,
                                    "value": sources[0],
                                },
                                {
                                    "name": "Setup",
                                    "type": "list",
                                    "values": ["HRS", "Microscope"],
                                    "value": "HRS",
                                },
                                {"name": "Fix", "type": "bool", "value": False},
                            ],
                        },
                        {
                            "name": "Plot",
                            "type": "group",
                            "children": [
                                {"name": "SmoothX", "type": "int", "value": 0},
                                {"name": "SmoothY", "type": "int", "value": 0},
                                {"name": "medfilt", "type": "int", "value": 0},
                                {"name": "Integrate Raw", "type": "bool", "value": False},
                                {"name": "log10", "type": "bool", "value": False},
                                {"name": "window", "type": "float", "value": 1},
                                {"name": "0->Baseline", "type": "bool", "value": False},
                                {"name": "min->Baseline", "type": "bool", "value": True},
                                {"name": "Generate polar. resp.", "type": "bool", "value": False},
                            ],
                        },
                    ]
                else:
                    try:
                        self.refreshTreeValues()
                        self.update_graph_data()
                    except:
                        traceback.print_exc()

                    params = self.prev_params_raw


                print(params)
                self.params = Parameter.create(name="params", type="group", children=params)

                if self.expType == "scan3D":
                    if not "3D" in self.params:
                        self.params.addChild(
                            {
                                "name": "3D",
                                "type": "group",
                                "children": [
                                    {
                                        "name": "Z",
                                        "type": "list",
                                        "values": unique(
                                            self.activeDataset["metadata"]["Z_target"]
                                        ).tolist(),
                                    }
                                ],
                            }
                        )

                    self.params.child("3D").child("Z").sigValueChanging.connect(
                        lambda x: self.setZlevel()
                    )

                for ax in ax_param:
                    ax.setParams(self.params)

                self.config_tree.setParameters(self.params, showTop=False)

                if self.expType == "scan3D":
                    self.zIndex = 0
                self.prev_params_raw = params


            self._lock_update_graph_data = False
            self.update_graph_data()


        except:
            traceback.print_exc()

    def setZlevel(self, z=None):
        if z is None:
            z = self.params.child("3D")["Z"].value()
        Z = np.linspace(
            self.activeDataset[self.zIndex]["metadata"]["Z_target"][0, 0, 0],
            self.activeDataset[self.zIndex]["metadata"]["Z_target"][0, 0, -1],
            self.activeDataset[self.zIndex]["metadata"].shape[2],
        )

        iz = abs(Z - z).argmin()
        self.zIndex = iz
        self.update_graph_data()

    def exdirPath_open_dialog(self):
        fname = QtGui.QFileDialog.getExistingDirectory(
            self, "Open", self.exdirPath.text()
        )
        if type(fname) == tuple:
            fname = fname[0]
        try:
            self.exdirPath.setText(fname)
            self.model.index(fname)
            self.tree.model().index(fname)
            self.tree.model().setRootPath(fname)
            self.tree.setRootIndex(self.tree.model().index(fname))

            logging.info("exdirPath: " + fname)
            self.settings.setValue("filePath", fname)

        except:
            traceback.print_exc()

    def exdirPath_set(self):
        fname = self.exdirPath.text()
        print(fname)
        if os.path.exists(fname):

            self.model.index(fname)
            self.tree.model().index(fname)
            self.tree.model().setRootPath(fname)
            self.tree.setRootIndex(self.tree.model().index(fname))
            self.exdirPath_prev = self.exdirPath.text()
            logging.info("exdirPath: " + fname)
        else:
            self.exdirPath.blockSignals(True)
            self.exdirPath.setText(self.exdirPath_prev)
            self.exdirPath.blockSignals(False)
            logging.info("exdirPath_Err:" + fname)
            return



    def mouseClicked(self, evt, view_name=None):
        view_name = self.sender().objectName().split("_")[-1]
        pos = evt  # [0]
        x, y = self.mouse_last_pos[view_name]
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ControlModifier and view_name == "DATA":
            distance = []
            for key in self.linesManager.LINES["DATA"]:
                X, Y = self.linesManager.LINES["DATA"][key].getData()
                min_dist = (((x - X) ** 2 + (y - Y) ** 2) ** 0.5).min()
                distance.append(min_dist)
            distance = np.array(distance)
            min_index = np.where(distance == distance.min())[0][0]
            print(min_index)
            key = list(self.linesManager.LINES["DATA"].keys())[min_index]
            self.linesManager.LINES["DATA"][key].setVisible(False)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = HyperspectralViewer()
    with open(Path("./ui/Darkeum1.qss"), "r") as style:
        ex.setStyleSheet(style.read())
        app.setStyleSheet(style.read())
    ex.init_highlight()
    sys.exit(app.exec_())
