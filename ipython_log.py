# IPython log file

import pandas as pd

pre = pd.read_csv("calibr/precompensation_old_1.167.csv", names=["wl", "pos"])
pre
get_ipython().run_line_magic("ls", "")
get_ipython().run_line_magic("ls", "calibr/")
d = pd.read_csv("calibr/delay_zero_pos.csv", index_col=0)
d
p = pd.read_csv("calibr/precompensation_old_1.167.csv", names=["precomp"], index_col=0)
p
plot(d)
plot(p - p.iloc[-1])
plot(p - p.iloc[-1], "r")
plot(d)
plot(d.index * 1.16, d)
plot(d.index / 1.16, d)
plot(d.index / 1.1, d)
plot(d * 1.16)
plot(p - p.iloc[-1], "r")
plot(d * 1.1)
plot(d * 1.08)
plot(d * 1.11)
plot(p - p.iloc[-1], "r")
plot(d * 1.11)
plot(d * 1.01)
plot(d * 1.005)
plot(d * 1.02)
plot(d * 1.09)
plot(p - p.iloc[-1], "r")
plot(d * 1.09)
plot(d / 1.16)
100 / 1.16
103 / 1.16
5e-8 / 1.16
plot(p - p.iloc[-1], "r")
plot(p - p.iloc[-1], "o")
plot(p - p.iloc[-1], "-o")
plot(p - p.iloc[-1], "-or")
plot(p, "-or", label='"precompensation"')
legend()
plot(d / 1.16 + p.iloc[-1], "xg", label="Delay_line correction curve")
plot(d / 1.16 + p.iloc[-1].values, "xg", label="Delay_line correction curve")
plot(d / 1.16 + p.iloc[-1].values, "xg", label="none")
legend()
plot(p, "-or", label='"precompensation"')
plot(d / 1.16 + p.iloc[-1].values, "tb", label="Delay_line correction curve")
plot(d / 1.16 + p.iloc[-1].values, "sb", label="Delay_line correction curve")
plot(d / 1.16 + p.iloc[-1].values, "sb", label="Delay_line correction curve")
plot(p, "-or", label='"precompensation"')
legend()
xlabel("Wavelength, nm")
ylabel("Precompensation, arb.un.[0-100]")
get_ipython().run_line_magic("ls", "")
get_ipython().run_line_magic("ls", "tmp")
get_ipython().run_line_magic("ls", "calibr/")
limits = pd.read_csv("calibr/Dataset 1.csv", names=["min", "max"], index_col=0)
limits
get_ipython().system("nano calibr/Dataset\\ 1.csv")
get_ipython().system("nano calibr/Dataset\\ 1.csv")
get_ipython().run_line_magic("ls", "tmp")
get_ipython().run_line_magic("ls", "calibr/")
limits = pd.read_csv(
    "calibr/precompensation_limits.csv", names=["min", "max"], index_col=0
)
get_ipython().system("nano calibr/precompensation_limits.csv")
limits = pd.read_csv("calibr/precompensation_limits.csv")
limits
plot(limits["X"], limits["Y"], "c")
from scipy.interpolate import interp1d

in_min = interp1d(limits["X"], limits["Y"])
get_ipython().set_next_input("in_min = interp1d")
get_ipython().run_line_magic("pinfo", "interp1d")
in_min = interp1d(limits["X"], limits["Y"], bounds_error=False, kind="extrapolate")
in_min = interp1d(limits["X"], limits["Y"], bounds_error=False, fill_value=0)
in_max = interp1d(limits["X.1"], limits["Y.1"], bounds_error=False, fill_value=0)
in_p = interp1d(p.index, p, bounds_error=False, fill_value=0)
p
p.index
in_p = interp1d(p.index.values, p.values, bounds_error=False, fill_value=0)
p
len(p.index.values)
len(p.values)
in_p = interp1d(p.index.values, p.values, bounds_error=False, fill_value=0)
in_p = interp1d(p.index.values, p.values, bounds_error=False, fill_value=0)
p.values
p.index.values
p.values[:]
p.values.flatten()
in_p = interp1d(p.index.values, p.values.flatten(), bounds_error=False, fill_value=0)
x = arange(680, 1300, 1)
x
in_min(x)
plot(x, in_min(x))
in_min = interp1d(limits["X"], limits["Y"], bounds_error=False, fill_value=nan)
in_max = interp1d(limits["X.1"], limits["Y.1"], bounds_error=False, fill_value=nan)
plot(x, in_min(x))
plot(x, in_min(x))
plot(x, in_max(x))
plot(d / 1.16 + p.iloc[-1].values, "sb", label="Delay_line correction curve")
plot(p, "-or", label='"precompensation"')
plot(x, in_min(x), "c")
plot(x, in_max(x), "c")
legend()
xlim((680, 1300))
ylim((0, 100))
xlabel("Wavelength, nm")
ylim((0, 100))
grid(1)
ylabel("Precompensation, arb.un.[0-100]")
get_ipython().run_line_magic("logstart", "")
