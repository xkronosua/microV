import Pyro4
from microscope.device_server import device

# ...

# Pyro4.config is a singleton, these changes to config will be
# used for all the device servers.  This needs to be done after
# importing microscope.device_server
Pyro4.config.COMPRESSION = True
Pyro4.config.PICKLE_PROTOCOL_VERSION = 2

from hardware.lasers.Spectra_Physics_Insight_X3 import InSightX3Laser

import logging
import sys

# demo = False
demo = True


if "sim" in sys.argv:
    demo = True
# print(logging.handlers)
# logging.getLogger().setLevel(logging.DEBUG)

DEVICES = [
    device(InSightX3Laser, host="127.0.0.1", port=8000, conf={"demo": demo}),
]
