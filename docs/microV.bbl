% $ biblatex auxiliary file $
% $ biblatex bbl format version 3.1 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\datalist[entry]{nty/global//global/global}
  \entry{diels_control_1985}{article}{}
    \name{author}{4}{}{%
      {{hash=DJC}{%
         family={Diels},
         familyi={D\bibinitperiod},
         given={Jean-Claude},
         giveni={J\bibinithyphendelim C\bibinitperiod},
      }}%
      {{hash=FJ}{%
         family={Fontaine},
         familyi={F\bibinitperiod},
         given={Joel},
         giveni={J\bibinitperiod},
      }}%
      {{hash=MI}{%
         family={McMichael},
         familyi={M\bibinitperiod},
         given={Ian},
         giveni={I\bibinitperiod},
      }}%
      {{hash=SF}{%
         family={Simoni},
         familyi={S\bibinitperiod},
         given={Francesco},
         giveni={F\bibinitperiod},
      }}%
    }
    \strng{namehash}{DJC+1}
    \strng{fullhash}{DJCFJMISF1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{D}
    \field{sortinithash}{D}
    \field{abstract}{%
    The performances of a tunable femtosecond dye laser are analyzed using
  accurate correlation techniques. The source is a passively mode-locked dye
  laser, of which both the frequency and frequency modulation are controlled by
  one or two intracavity prisms. Interferometric second-order autocorrelations,
  with a peak-to-background ratio of 8 to 1, are used simultaneously with the
  conventional intensity autocorrelation and the pulse spectrum to determine
  the pulse shape. The main advantages of the interferometric autocorrelations
  are that they provide phase information otherwise not available, and they are
  more sensitive to the pulse shape than the intensity autocorrelation. The
  phase sensitivity is demonstrated in an analysis of the Gaussian pulses with
  a linear frequency modulation. Analytical expressions for the envelopes of
  the interferometric autocorrelations of typical pulse shapes are provided for
  quick pulse shape identification. A numerical method is used to analyze the
  more complex pulse shapes and chirps that can be produced by the laser. A
  series of examples demonstrates the control of this laser over various pulse
  shapes and frequency modulations. Pulse broadening or compression by
  propagation through glass is calculated for the pulse shapes determined from
  the fittings. Comparisons of autocorrelations and cross correlations
  calculated for the dispersed pulses, with the actual measurements,
  demonstrate the accuracy of the fitting procedure. The method of pulse shape
  determination demonstrated here requires a train of identical pulses. Indeed,
  it is shown that, for example, a train of unchirped pulses randomly
  distributed in frequency can have the same interfer-ometric autocorrelation
  than a single chirped pulse. In the case of the present source, a comparison
  of the pulse spectrum, with that of the second harmonic, gives an additional
  proof that pulse-to-pulse fluctuations are negligible.%
    }
    \verb{doi}
    \verb 10.1364/AO.24.001270
    \endverb
    \field{pages}{1270}
    \field{title}{Control and measurement of ultrashort pulse shapes with
  femtosecond accuracy}
    \field{volume}{24}
    \verb{file}
    \verb Full Text PDF:files/1775/Diels et al. - 1985 - Control and measuremen
    \verb t of ultrashort pulse shapes.pdf:application/pdf
    \endverb
    \field{journaltitle}{Applied optics}
    \field{month}{06}
    \field{year}{1985}
  \endentry

  \entry{young_pragmatic_2015}{article}{}
    \name{author}{5}{}{%
      {{hash=YMD}{%
         family={Young},
         familyi={Y\bibinitperiod},
         given={Michael\bibnamedelima D.},
         giveni={M\bibinitperiod\bibinitdelim D\bibinitperiod},
      }}%
      {{hash=FJJ}{%
         family={Field},
         familyi={F\bibinitperiod},
         given={Jeffrey\bibnamedelima J.},
         giveni={J\bibinitperiod\bibinitdelim J\bibinitperiod},
      }}%
      {{hash=SKE}{%
         family={Sheetz},
         familyi={S\bibinitperiod},
         given={Kraig\bibnamedelima E.},
         giveni={K\bibinitperiod\bibinitdelim E\bibinitperiod},
      }}%
      {{hash=BRA}{%
         family={Bartels},
         familyi={B\bibinitperiod},
         given={Randy\bibnamedelima A.},
         giveni={R\bibinitperiod\bibinitdelim A\bibinitperiod},
      }}%
      {{hash=SJ}{%
         family={Squier},
         familyi={S\bibinitperiod},
         given={Jeff},
         giveni={J\bibinitperiod},
      }}%
    }
    \list{language}{1}{%
      {en}%
    }
    \strng{namehash}{YMD+1}
    \strng{fullhash}{YMDFJJSKEBRASJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{Y}
    \field{sortinithash}{Y}
    \verb{doi}
    \verb 10.1364/AOP.7.000276
    \endverb
    \field{issn}{1943-8206}
    \field{number}{2}
    \field{pages}{276}
    \field{title}{A pragmatic guide to multiphoton microscope design}
    \verb{url}
    \verb https://www.osapublishing.org/abstract.cfm?URI=aop-7-2-276
    \endverb
    \field{volume}{7}
    \field{journaltitle}{Advances in Optics and Photonics}
    \field{month}{06}
    \field{year}{2015}
    \field{urlday}{03}
    \field{urlmonth}{07}
    \field{urlyear}{2019}
  \endentry
\enddatalist
\endinput
