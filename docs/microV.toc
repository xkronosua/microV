\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {nil}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction and preparation}{2}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}About}{2}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Installing}{3}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}First start}{3}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}device\_server}{4}{section.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}laserController}{4}{section.1.5}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}microV}{6}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Interface}{6}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Fast control}{7}{subsection.2.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Data preview}{7}{subsection.2.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Laser}{8}{subsection.2.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.4}Motion}{10}{subsection.2.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{3D translation stage (PI NanoCube E727). Fig.\ref {fig:motion-3d} }{10}{section*.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Filters piezostage (ELLx). Fig.\ref {fig:motion-filters}}{10}{section*.9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Delay line (MoCo linear stage). Fig.\ref {fig:motion-delay}}{10}{section*.10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Linear translation stages (lens+knife position). Fig.\ref {fig:motion-lens}}{12}{section*.12}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{HWP (FIX/TUN output). Fig.\ref {fig:motion-hwp_power}}{12}{section*.13}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{HWP (Polarization). Fig.\ref {fig:motion-hwp}}{13}{section*.15}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Commutation. Fig.\ref {fig:motion-commutation}}{13}{section*.16}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Beam shift. Fig.\ref {fig:motion-beamshift}}{14}{section*.17}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.5}Readout}{15}{subsection.2.1.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Spectrometer. Fig.\ref {fig:readout-spectrometer}}{15}{section*.20}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Picoscope. Fig.\ref {fig:readout-picoscope}}{16}{section*.21}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Powermeter. Fig.\ref {fig:readout-powermeter}}{17}{section*.22}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Camera. Fig.\ref {fig:readout-camera}}{17}{section*.24}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{CCS200. Fig.\ref {fig:readout-CCS200}}{18}{section*.25}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.6}Plot}{18}{subsection.2.1.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.7}Setup}{18}{subsection.2.1.7}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Finalization protocol}{18}{section*.26}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Optimization}{19}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Measurements}{19}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}ScanND}{19}{subsection.2.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Config.}{22}{section*.29}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Script.}{23}{section*.30}%
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Single axis.}{23}{section*.31}%
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Sequence of single axes.}{23}{section*.32}%
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Sublevel of axis.}{23}{section*.33}%
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{N-dimensional scanning.}{23}{section*.34}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Scan3D}{24}{subsection.2.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}MultiScan}{25}{subsection.2.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Configuration of optimization for 3D tracking}{25}{section*.36}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Configuration of live optimization of beams overlapping and delay}{26}{section*.38}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{How to work with all of this?}{26}{section*.39}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Calibration}{28}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Power}{28}{subsection.2.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}Pulsewidth}{30}{subsection.2.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{FIX}{30}{section*.41}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{TUN}{31}{section*.44}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.3}Beam size}{33}{subsection.2.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Microscope}{33}{section*.46}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{HRS}{36}{section*.49}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.4}Delay and overlap}{37}{subsection.2.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Microscope}{37}{section*.51}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{HRS}{38}{section*.52}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.5}Intensity calculation}{38}{subsection.2.4.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Folders}{39}{section.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}hyperspectralViewer}{41}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Writing scripts}{42}{chapter.4}%
