import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import quad, quad_vec
from pathlib import Path
from scipy import signal

sech = lambda x: 1 / np.cosh(x)


def I(t, t_p):
    return sech(1.7627 * t / t_p) ** 2


def E(t, t_p, wc=1):
    return I(t, t_p) ** 0.5 * np.exp(1j * wc * t)


def func(t, tau, t_p=100, wc=1):
    return abs((E(t, t_p, wc) + E(t - tau, t_p, wc)) ** 2) ** 2


def func_upper(t, tau, t_p=100):
    return (abs(E(t, t_p)) + abs(E(t - tau, t_p))) ** 4


def func_lower(t, tau, t_p=100):
    return (abs(E(t, t_p)) - abs(E(t - tau, t_p))) ** 4


def func_norm(t, t_p=100):
    return 16 * abs(E(t, t_p)) ** 4


almost_inf = 5000


def IAC(x, t0, A, A0, t_p, wc):
    t = x - t0  # .reshape(x.shape[0],1)
    # f = lambda t:  [quad(func,-almost_inf,almost_inf,args=(tau, t_p, wc))[0]/quad(func_norm,-almost_inf,almost_inf,args=( t_p, wc,))[0] for tau in t]
    f = lambda x: func(x, t, t_p, wc)
    f_norm = lambda x: func_norm(x, t_p)
    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    # print(r)
    r = np.nan_to_num(r, 0, neginf=0, posinf=0)
    # print(r[0])
    res = A0 + A * r
    print(t_p, wc, A, A0, t0)
    return res


def IAC_upper(x, t0, A, A0, t_p):
    t = x - t0  # .reshape(x.shape[0],1)
    # f = lambda t:  [quad(func,-almost_inf,almost_inf,args=(tau, t_p, wc))[0]/quad(func_norm,-almost_inf,almost_inf,args=( t_p, wc,))[0] for tau in t]
    f = lambda x: func_upper(x, t, t_p)
    f_norm = lambda x: func_norm(x, t_p)
    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    # print(r)
    r = np.nan_to_num(r, 0, neginf=0, posinf=0)
    # print(r[0])
    res = A0 + A * r

    return res


def IAC_lower(x, t0, A, A0, t_p):
    t = x - t0  # .reshape(x.shape[0],1)
    # f = lambda t:  [quad(func,-almost_inf,almost_inf,args=(tau, t_p, wc))[0]/quad(func_norm,-almost_inf,almost_inf,args=( t_p, wc,))[0] for tau in t]
    f = lambda x: func_lower(x, t, t_p)
    f_norm = lambda x: func_norm(x, t_p)
    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )
    # print(r)
    r = np.nan_to_num(r, 0, neginf=0, posinf=0)
    # print(r[0])
    res = A0 + A * r
    return res


def func2(t, tau, t_p1=100, t_p2=100, A1=1, A2=1):
    return 4 * A1 * A2 * I(t, t_p1) * I(t - tau, t_p2)


def func_norm2(t, t_p1=100, t_p2=100, A1=1, A2=1):
    return A1 * A2 * abs(E(t, t_p1) * E(t, t_p2)) ** 2


def AC2(x, t0, A0, A1, A2, t_p1, t_p2):
    t = x - t0  # .reshape(x.shape[0],1)
    # f = lambda t:  [quad(func,-almost_inf,almost_inf,args=(tau, t_p, wc))[0]/quad(func_norm,-almost_inf,almost_inf,args=( t_p, wc,))[0] for tau in t]
    f = lambda x: func2(x, t, t_p1, t_p2, A1, A2)
    f_norm = lambda x: func_norm2(x, t_p1, t_p2, A1, A2)
    r = (
        quad_vec(f, -almost_inf, almost_inf)[0]
        / quad_vec(f_norm, -almost_inf, almost_inf)[0]
    )

    r = np.nan_to_num(r, 0, neginf=0, posinf=0)
    # print(r[0])
    res = A0 + r

    return res


# these are matplotlib.patch.Patch properties
props = dict(boxstyle="round", facecolor="white", alpha=0.9)


t = np.linspace(-500, 500, 1000)

t_p = 150
t_p2 = 100
delay = 250

wc = 0.2

Intens0 = I(t, t_p)
Intens1 = I(t + delay, t_p2)

plt.figure()
plt.plot(t, Intens0, "b", label="TUN")
plt.plot(t, Intens1, "g", label="FIX")

Intens_autocorr = AC2(t, 0, 0, 1, 1, t_p, t_p2)
plt.plot(t, Intens_autocorr, "r", label="Intens. autocor.")
plt.legend()
plt.grid(1)
plt.xlabel("t, fs")
plt.ylabel("Intens.")
plt.annotate(
    s=r"$\Delta\tau_1^{FWHM}$",
    xy=(0, 0.5),
    xytext=(t_p - 50, 0.8),
    arrowprops=dict(arrowstyle="->"),
    bbox=props,
)
plt.annotate(
    s="", xy=(t_p / 2, 0.5), xytext=(-t_p / 2, 0.5), arrowprops=dict(arrowstyle="<->")
)
plt.annotate(
    s=r"$\Delta\tau_2^{FWHM}$",
    xy=(-delay, 0.5),
    xytext=(-delay - t_p2 * 1.5, 0.8),
    arrowprops=dict(arrowstyle="->"),
    bbox=props,
)
plt.annotate(
    s="",
    xy=(t_p2 / 2 - delay, 0.5),
    xytext=(-t_p2 / 2 - delay, 0.5),
    arrowprops=dict(arrowstyle="<->"),
)

k = 1.54
if t_p2 > t_p:
    k *= 1 + ((t_p2 - t_p) / t_p) ** 2
else:
    k *= 1 - (abs(t_p2 - t_p) / t_p) ** 2

plt.annotate(
    s=r"$\Delta\tau_A^{FWHM}$",
    xy=(0, 2),
    xytext=(t_p, 2.3),
    arrowprops=dict(arrowstyle="->"),
    bbox=props,
)
plt.annotate(
    s="",
    xy=(t_p * k / 2, 2),
    xytext=(-t_p * k / 2, 2),
    arrowprops=dict(arrowstyle="<->"),
)


plt.tight_layout()
plt.savefig(Path("../images/sech2.pdf"))


tau = np.linspace(-700, 700, 1000)
plt.figure()
Intens_interf = IAC(tau, 0, 1, 0, t_p, wc)
plt.plot(tau, Intens_interf)

Intens_upper = IAC_upper(tau, 0, 1, 0, t_p)
plt.plot(tau, Intens_upper, "--")

Intens_lower = IAC_lower(tau, 0, 1, 0, t_p)
plt.plot(tau, Intens_lower, "--")


plt.text(50, 1 - 0.01, r"$16\int E^4(t)dt$", bbox=props)

plt.text(500, 1 / 8 + 0.01, r"$2\int E^4(t)dt$", bbox=props)

plt.annotate(
    xy=(150, IAC_upper(150, 0, 1, 0, t_p)),
    xytext=(250, 0.3),
    arrowprops=dict(arrowstyle="->"),
    s=r"$\int |E(t) + E(t+\tau)|^4dt$",
    bbox=props,
)


plt.annotate(
    xy=(150, IAC_lower(150, 0, 1, 0, t_p)),
    xytext=(250, 0),
    arrowprops=dict(arrowstyle="->"),
    s=r"$\int |E(t) - E(t+\tau)|^4dt$",
    bbox=props,
)


plt.xlim((-700, 700))
plt.xlabel(r"$\tau$, fs")
plt.ylabel("Intens.")

plt.grid(1)
plt.tight_layout()
plt.savefig(Path("../images/interfAutocorr.pdf"))

plt.figure()


f = 80e6  # Hz
T = 1 / f  # s
tau = 2e-9  # T/8#150e-15 # s
N = 3
t = np.arange(0, T * N, tau / 100) - T / 2
duty_cycle = tau / T

plt.plot(t, [1] * len(t), "-.r", label="$P_{peak}$", alpha=0.5)

sq = signal.square(2 * np.pi * f * t, duty=duty_cycle) / 2 + 0.5

plt.plot(t, sq, "g", lw=2, label="square")
plt.plot(t, [duty_cycle] * len(t), "--g", label="$P_{avg}$ (square)")
sech2 = 0
for i in range(N):
    sech2 += I(t - T * i - tau / 2, tau)

factor = sq.sum() / sech2.sum()

plt.plot(t, sech2, "m", lw=2, label="sech$^2$")
plt.plot(t, [duty_cycle * factor] * len(t), "--m", label="$P_{avg}$ (sech$^2$)")
plt.legend(loc="upper left")

plt.xlabel("Time, s")
plt.ylabel("Power")
plt.title(r"$f$=%.1f MHz, $\Delta\tau^{FWHM}$=%.2f ns" % (f / 1e6, tau / 1e-9))

plt.grid(1)
plt.tight_layout()
plt.savefig(Path("../images/power_avg_peak.pdf"))

plt.show()
