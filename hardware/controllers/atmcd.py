from microscope.cameras.atmcd import (
    AndorAtmcd,
    GetPixelSize,
    SetTemperature,
    SetReadMode,
    SetPreAmpGain,
    CoolerON,
    SetFanMode,
    SetPreAmpGain,
    SetVSSpeed,
    SetADChannel,
    SetAcquisitionMode,
    SetShutter,
    GetDetector,
    IsTriggerModeAvailable,
    StartAcquisition,
    SetTriggerMode,
    GetTemperature,
    ReadMode,
    AcquisitionMode,
    AtmcdException,
    DRV_NO_NEW_DATA,
    GetMostRecentImage,
    Initialize,
)
import microscope
import microscope.abc
import time
from ctypes import c_long


class AndorCamera(AndorAtmcd):
    """Implements CameraDevice interface for Andor ATMCD library."""

    amplifiers = []

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def configure(self):
        with self:
            Initialize(b".")
        self.set_trigger(
            microscope.TriggerType.SOFTWARE,
            microscope.TriggerMode.ONCE,
        )
        self.set_setting("Binning", (1, 127))
        self.set_setting("Cooler Enabled", True)
        self.set_setting("Fan mode", 0)
        self.set_setting("TemperatureSetPoint", -85)

    def get_pixel_size(self):
        """Return the pixel size."""
        with self:
            return GetPixelSize()


if __name__ == "__main__":
    c = AndorCamera()
    c.configure()
    print(c.get_all_settings())

    c.enable()

    print(c.grab_next_data())
    c.disable()
