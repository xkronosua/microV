import microscope
import microscope.abc

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).parent.absolute()))

# print(sys.path)

from Shamrock import ShamRockController
from ShamrockDummy import ShamRockController as ShamRockController_demo
from AndorCamera import AndorCamera


import time


import contextlib
import threading
import typing
import logging

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class AndorAtmcd(
    microscope.abc.Device  # microscope.abc.FloatingDeviceMixin, microscope.abc.Camera,
):
    """Implements CameraDevice interface for Andor ATMCD library."""

    trigger_mode = microscope.TriggerMode.ONCE
    trigger_type = microscope.TriggerType.SOFTWARE
    alive = False

    def __init__(self, demo=False, **kwargs):
        super().__init__(**kwargs)
        if demo:
            from AndorCameraDummy import AndorCamera as AndorCamera_demo

            self.andorCCD = AndorCamera_demo()
        else:
            self.andorCCD = AndorCamera()
        self.demo = demo
        # Recursion depth for context manager behaviour.
        self._rdepth = 0
        # The handle used by the DLL to identify this camera.
        self._handle = None
        # The following parameters will be populated after hardware init.
        self._roi = None
        self._binning = None

    def initialize(self):

        return True

    def enable(self):
        try:
            self.andorCCD.Initialize()
            # _logger.info("Preparing for acquisition.")
            # if self._acquiring:
            # 	self.abort()
            # self._create_buffers()
            self._acquiring = True
            self._sent = 0
            # _logger.info("Acquisition enabled.")
            self.alive = True
        except Exception as ex:
            _logger.error("AndorAtmcd initialize...", exc_info=ex)
            self.alive = False
            self._acquiring = False
        return self.alive

    def _on_shutdown(self):
        if self.alive:
            self.andorCCD.ShutDown()
        self.alive = False

    def _do_shutdown(self):
        if self.alive:
            self.andorCCD.ShutDown()
        self.alive = False

    def is_alive(self):
        return self.alive

    def shutdown(self):
        if self.alive:
            self._on_shutdown()
        self.alive = False

    def set_exposure_time(self, exposure):
        if exposure:
            self.andorCCD.SetExposureTime(exposure)
            # self._exposure_time = exposure

    def get_exposure_time(self):
        return self.andorCCD.GetAcquisitionTimings()[0]

    def set_read_mode(self, mode="FVB"):
        self.andorCCD.SetReadMode(mode)

    def get_sensor_temperature(self):
        return self.andorCCD.GetTemperature()

    def set_sensor_temperature(self, temperature):
        return self.andorCCD.SetTemperature(temperature)

    def get_sensor_shape(self):
        return self.andorCCD.GetDetector()

    def get_pixel_size(self):
        return self.andorCCD.GetPixelSize()

    def grab_next_data(self, soft_trigger=True):
        if soft_trigger:
            self._do_trigger()
        #self.andorCCD.StartAcquisition()
        t = time.time()
        # self.andorCCD.WaitForAcquisition()
        # data = self.andorCCD.GetMostRecentImage()
        data = self._fetch_data()
        return (data, t)

    def set_trigger(self, ttype, tmode):
        self.trigger_mode = tmode
        self.trigger_type = ttype

    def _do_trigger(self):
        self.andorCCD.StartAcquisition()

    def soft_trigger(self):
        self._do_trigger()




    def _fetch_data(self):
        #t0 = time.time()
        self.andorCCD.WaitForAcquisition()
        data = self.andorCCD.GetMostRecentImage()
        #print(time.time() - t0)
        return data

    def _get_binning(self):
        return (1, 1)

    def _get_roi(self):
        pass

    def _set_binning(self):
        pass

    def _set_roi(self):
        pass

    def abort(self):
        self.andorCCD.CancelWait()
        self.andorCCD.AbortAcquisition()

    def get_id(self):
        return 0

    def demo_getPosition(self):
        return self.andorCCD.demo_getPosition()


class ShamRock_spectrometer(microscope.abc.Device):
    demo = False
    shamrock = None
    alive = False

    def __init__(self, demo=False, **kwargs):
        super().__init__(**kwargs)
        self.demo = demo
        if demo:
            self.shamrock = ShamRockController_demo()
        else:
            self.shamrock = ShamRockController()

    def initialize(self):
        return True

    def enable(self):
        try:
            self.shamrock.Initialize()
            self.shamrock.Connect()
            self.alive = True
        except Exception as ex:
            _logger.error("ShamRock_spectrometer initialize...", exc_info=ex)
            self.alive = False
        return self.alive

    def is_alive(self):
        return self.alive

    def _on_shutdown(self):
        if self.alive:
            self.shamrock.Close()
        self.alive = False

    def _do_shutdown(self):
        if self.alive:
            self.shamrock.Close()
        self.alive = False

    def get_detectorOffset(self):
        return self.shamrock.shamrock.GetDetectorOffset()

    def set_detectorOffset(self, offset):
        self.shamrock.shamrock.SetDetectorOffset(offset)

    def get_detectorOffsetPort2(self):
        return self.shamrock.shamrock.GetDetectorOffset()

    def set_detectorOffsetPort2(self, offset):
        self.shamrock.shamrock.SetDetectorOffsetPort2(offset)

    def get_gratingOffset(self, grating):
        grating += 1
        return self.shamrock.shamrock.GetGratingOffset(grating)

    def set_gratingOffset(self, grating, offset):
        grating += 1
        self.shamrock.shamrock.SetGratingOffset(grating, offset)

    def get_wavelength(self):
        return self.shamrock.shamrock.GetWavelength()

    def set_wavelength(self, wl):
        if wl:
            self.shamrock.shamrock.SetWavelength(wl)

    def get_port(self):
        return self.shamrock.shamrock.GetPort()

    def set_port(self, port):
        self.shamrock.shamrock.SetPort(port)

    def get_focusingMirrorPosition(self):
        return self.shamrock.shamrock.GetFocusMirror()

    def set_focusingMirrorPosition(self, position=None):
        if position is None:
            self.shamrock.shamrock.FocusMirrorReset()
        else:
            current = self.shamrock.shamrock.GetFocusMirror()
            delta = position - current
            self.shamrock.shamrock.SetFocusMirror(delta)
        return self.shamrock.shamrock.GetFocusMirror()

    def get_grating(self):
        return self.shamrock.shamrock.GetGrating() - 1

    def set_grating(self, grating):
        return self.shamrock.shamrock.SetGrating(grating + 1)

    def set_pixel_width(self, width):
        if width:
            self.shamrock.shamrock.SetPixelWidth(width)

    def set_number_pixels(self, n):
        if n:
            self.shamrock.shamrock.SetNumberPixels(n)

    def get_calibration(self):
        return self.shamrock.shamrock.GetCalibration()

    def get_info(self):
        return self.shamrock.shamrock.GetInfo()


class AndorShamrock_spectrometer(microscope.abc.Controller):
    """Prior ProScanIII controller.

    The controlled devices have the following labels:

    `filter 1`
      Filter wheel connected to connector labelled "FILTER 1".
    `filter 2`
      Filter wheel connected to connector labelled "FILTER 1".
    `filter 3`
      Filter wheel connected to connector labelled "A AXIS".

    .. note::

       The Prior ProScanIII can control up to three filter wheels.
       However, a filter position may have a different number
       dependening on which connector it is.  For example, using an 8
       position filter wheel, what is position 1 on the filter 1 and 2
       connectors, is position 4 when on the A axis (filter 3)
       connector.

    """

    def __init__(self, demo=False, **kwargs) -> None:
        super().__init__(**kwargs)
        # self._conn = _ProScanIIIConnection(port, baudrate, timeout)
        self._devices: typing.Mapping[str, microscope.abc.Device] = {}
        self._devices["shamrock"] = ShamRock_spectrometer(demo=demo)
        self._devices["andor"] = AndorAtmcd(demo=demo)

    @property
    def devices(self) -> typing.Mapping[str, microscope.abc.Device]:
        return self._devices
