import numpy as np
import time


class PS3000a:
    noSamples = -1
    ChA_VRange = 1
    ChB_VRange = 1
    pretrig_s = 0

    def __init__(self, serialNumber=None):
        pass

    def open(self, a=None):
        pass

    def close(self):
        pass

    def getMaxValue(self):
        return 32764

    def setChannel(self, channel="B", coupling="DC", VRange=1, VOffset=0):
        if channel == "B":
            self.ChB_VRange = VRange
        else:
            self.ChA_VRange = VRange
        # print('chrange:',channel,self.ChA_VRange,self.ChB_VRange)

    def setSamplingInterval(self, sampleInterval, samplingDuration):
        self.sampleInterval = sampleInterval
        self.samplingDuration = samplingDuration
        self.noSamples = round(samplingDuration / sampleInterval)
        return sampleInterval, self.noSamples, int(2e9)

    def setSimpleTrigger(
        self,
        trigSrc="A",
        threshold_V=0,
        direction=0,
        timeout_ms=5,
        enabled=True,
        delay=0,
    ):

        self.delay_s = delay * self.sampleInterval

    def memorySegments(self, n_captures):
        self.memory_Segments = n_captures
        return self.memory_Segments

    def setNoOfCaptures(self, n_captures):
        self.n_captures = n_captures

    def runBlock(self, pretrig=None):
        # pass
        self.pretrig_s = self.sampleInterval * pretrig

    def waitReady(self):

        print("sleep", (self.samplingDuration + self.delay_s) * self.n_captures)
        time.sleep((self.samplingDuration + self.delay_s) * self.n_captures)

    def getDataRawBulk(self, channel="B", data=[]):
        tmp = np.random.rand(self.n_captures, self.noSamples)
        data[:] = (tmp * 2 ** 16).astype(data.dtype)

    def getDataV(self, channel="B", dataV=[]):
        dataV[:] = np.random.rand(dataV.shape[0], dataV.shape[1])

    def rawToV(self, channel="B", dataRaw=None):
        k = 1
        dataRaw = dataRaw.astype(np.float)

        if channel == "B":
            k = self.ChB_VRange
        else:
            k = self.ChA_VRange
        # print('chrange:',channel,self.ChA_VRange,self.ChB_VRange)
        return dataRaw  # *k


class ps3000a:
    def __init__(self, serialNumber=None):
        pass

    def PS3000a(connect=False, serialNumber=None):
        return PS3000a()
