import os, subprocess, serial, time
from functools import wraps, reduce

# this script lets you emulate a serial device
# the client program should use the serial port file specifed by client_port

# if the port is a location that the user can't access (ex: /dev/ttyUSB0 often),
# sudo is required
import numpy as np
import re
from binascii import unhexlify, hexlify
import struct


class SerialEmulator(object):
    def __init__(self, device_port="ttydevice", client_port="ttyclient"):
        self.device_port = device_port
        self.client_port = client_port
        if os.name == "posix":
            cmd = [
                "/usr/bin/socat",
                "-d",
                "-d",
                "PTY,link=%s,raw,echo=0" % self.device_port,
                "PTY,link=%s,raw,echo=0" % self.client_port,
            ]
        else:
            cmd = [
                "socat.exe",
                "-d",
                "-d",
                "PTY,link=%s,raw,echo=0" % self.device_port,
                "PTY,link=%s,raw,echo=0" % self.client_port,
            ]
        self.proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        time.sleep(1)
        self.serial = serial.Serial(self.device_port, 9600, rtscts=True, dsrdtr=True)
        self.err = b""
        self.out = b""

    def write(self, out):
        self.serial.write(out)

    def flush(self):
        self.serial.flushInput()
        self.serial.flushOutput()

    def read(self):
        line = b""
        while self.serial.inWaiting() > 0:
            s = self.serial.read(1)
            if s == b"\n":
                break
            line += s
        # print(line)
        return line

    def __del__(self):
        self.stop()

    def stop(self):
        self.proc.kill()
        self.out, self.err = self.proc.communicate()

    def _checksum(self, command):
        return reduce(lambda x, y: x ^ y, map(ord, command))


if __name__ == "__main__":
    import signal

    signal.signal(signal.SIGINT, signal.SIG_DFL)

    emulator = SerialEmulator("ttydevice", "ttyclient_MoCo")

    current_pos = 0
    stepsPerUnit = 1 / 5e-5
    vel = int(10 * stepsPerUnit)
    dx = int(0.1 * stepsPerUnit)
    target = current_pos
    stage_address = b"0"

    status = stage_address + b"S0:ff ff ff ff ff ff ff\r\n"
    while True:
        line = emulator.read().split(b"\r")[0]
        # print('raw: |',parts,'|')
        if len(line) == 0:
            pass
            ##print(line)
            ##print(params)
            # time.sleep(0.01)
            # continue

        if len(line) == 1:
            p = b""
            stage_address = b"0"
            if line == b"'":
                p = stage_address + b"P0:" + str(current_pos).encode() + b"\r\n"
                emulator.write(p)

            elif line == b"%":
                p = status
                emulator.write(p)
            if p:
                print(line, p)
        else:
            command = line[0:2]
            value = line[2:]
            # print(command)
            p = b""
            if command == b"ma":
                target = int(value)

                # p = stage_address +b'P0:' + str(current_pos).encode()+b'\r\n'
                # emulator.write(p)

            elif command == b"mr":
                target = current_pos + int(value)
                # p = stage_address +b'P0:' + str(current_pos).encode()+b'\r\n'
                # emulator.write(p)

            elif b"mc" in line:
                target = 0
                p = stage_address + b"P0:" + str(current_pos).encode() + b"\r\n"
                emulator.write(p)

            else:
                pass
            if p:
                print(line, command, p)

        if abs(current_pos - target) >= dx:

            dt = dx * vel
            direction = (target - current_pos) / abs(target - current_pos)
            current_pos += int(dx * direction)
            status = stage_address + b"S0:AA AA AA AA AA AA AA\r\n"
        else:
            status = stage_address + b"S0:ff ff ff ff ff ff ff\r\n"
            current_pos = target

        time.sleep(0.01)
