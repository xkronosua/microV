import os, subprocess, serial, time
from functools import wraps, reduce

# this script lets you emulate a serial device
# the client program should use the serial port file specifed by client_port

# if the port is a location that the user can't access (ex: /dev/ttyUSB0 often),
# sudo is required
import numpy as np
import re
import pandas as pd


DEFAULT_INFO = {
    "TIMer:WATChdog": 10,  # "f4"),
    "LCD:BRIGhtness": 255,  # "i8"),
    "CONT:DSMPOS": 0,  # "i8"),
    "CONTrol:MTRMOV": 0,  # "i8"),
    "SHUTter": 0,  # "i2"),
    "IRSHUTter": 0,  # "i2"),
    "WAVelength": 1000,  # "i8"),
    "POWer": 0,  # "f4"),
    "MODE": "RUN",  # "S5"),
    "ON_OFF": False,
    "Status": {
        "RawSTB": 0,  # "i8"),
        "Emission": 0,  # "i2"),
        "Pulsing": 0,  # "i2"),
        "Main shutter": 0,  # "i2"),
        "IR shutter": 0,  # "i2"),
        "Servo on": 0,  # "i2"),
        "User interlock": 0,  # "i2"),
        "Keyswitch": 0,  # "i2"),
        "Power supply": 0,  # "i2"),
        "Internal": 0,  # "i2"),
        "Warning": 0,  # "i2"),
        "fault": 0,  # "i2"),
        "State": "READY",  # "S20"),
        "ON_OFF": 0,  # "i2"),
        "time": 0,  #'float64')
    },
}

DEFAULT_INFO_Status_hide = [
    "User interlock",
    "Power supply",
    "Keyswitch",
    "Internal",
    "Servo on",
    "Pulsing",
]


try:
    laser_states = pd.read_csv("laser_states.csv", index_col=0)
except:
    laser_states = pd.read_csv("hardware/sim/laser_states.csv", index_col=0)


def generate_status(
    config={
        "Emission": None,
        "Pulsing": None,
        "Main shutter": None,
        "IR shutter": None,
        "Servo on": None,
        "User interlock": None,
        "Keyswitch": None,
        "Power supply": None,
        "Internal": None,
        "Warning": None,
        "fault": None,
        "State": None,
    },
    states_lib=None,
):
    w = np.ones(len(states_lib)) == 1
    for k in config:
        if not config is None:
            w = w & (states_lib[k] == config[k])
            print(k, w.sum())
    r = states_lib[w]
    if len(r) > 0:
        return r["status_code"].iloc[0]
    else:
        return b""


class SerialEmulator(object):
    def __init__(self, device_port="ttydevice", client_port="ttyclient"):
        self.device_port = device_port
        self.client_port = client_port
        if os.name == "posix":
            cmd = [
                "/usr/bin/socat",
                "-d",
                "-d",
                "PTY,link=%s,raw,echo=0" % self.device_port,
                "PTY,link=%s,raw,echo=0" % self.client_port,
            ]
        else:
            cmd = [
                "socat.exe",
                "-d",
                "-d",
                "PTY,link=%s,raw,echo=0" % self.device_port,
                "PTY,link=%s,raw,echo=0" % self.client_port,
            ]
        self.proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        time.sleep(1)
        self.serial = serial.Serial(self.device_port, 9600, rtscts=True, dsrdtr=True)
        self.err = b""
        self.out = b""

    def write(self, out):
        self.serial.write(out)

    def flush(self):
        self.serial.flushInput()
        self.serial.flushOutput()

    def read(self):
        line = b""
        while self.serial.inWaiting() > 0:
            s = self.serial.read(1)
            if s == b"\n":
                break
            line += s
        # print(line)
        return line

    def __del__(self):
        self.stop()

    def stop(self):
        self.proc.kill()
        self.out, self.err = self.proc.communicate()

    def _checksum(self, command):
        return reduce(lambda x, y: x ^ y, map(ord, command))


if __name__ == "__main__":
    import signal

    signal.signal(signal.SIGINT, signal.SIG_DFL)

    emulator = SerialEmulator("ttydevice", "ttyclient")
    DEFAULT_INFO_tmp = {k.encode(): val for k, val in DEFAULT_INFO.items()}
    DEFAULT_INFO_tmp[b"Status"] = {
        k.encode(): val for k, val in DEFAULT_INFO["Status"].items()
    }

    params = DEFAULT_INFO_tmp

    # count = pos.copy()
    print(params)
    prev_params = params
    status = 3276851  # 8379951
    while True:
        line = emulator.read()
        parts = line[:-1].split(b" ")
        # print('raw: |',parts,'|')
        if line == b"":
            # print(line)
            # print(params)
            time.sleep(0.1)
            continue
        elif b"*IDN?" in line:
            print("*IDN?")
            time.sleep(0.1)
            emulator.write(
                b"Spectra-Physics, InSight DeepSee, xxxx, 0180-0.01.60/175-1.00.15/TN00001003\n"
            )
        elif b"READ:POWer?" in line:
            print("READ:POWer?")
            time.sleep(0.1)
            emulator.write(str(np.random.rand(1)[0]).encode() + b"\n")
        elif b"READ:POWer?" in line:
            print("READ:POWer?")
            time.sleep(0.1)
            emulator.write(str(np.random.rand(1)[0]).encode() + b"\n")
        elif b"LCD:BRIGhtness?" in line:
            print("LCD:BRIGhtness?")
            time.sleep(0.1)
            emulator.write(str(int(np.random.rand(1)[0] * 255)).encode() + b"\n")
        elif b"CONT:DSMPOS?" in line:
            print("CONT:DSMPOS?")
            time.sleep(0.1)
            emulator.write(str(np.random.rand(1)[0] * 100).encode() + b"\n")
        elif b"STB?" in line:
            print("STB?", params[b"SHUTter"], params[b"IRSHUTter"])
            time.sleep(0.1)
            conf = {
                "Emission": True,
                "Pulsing": True,
                "Main shutter": params[b"SHUTter"],
                "IR shutter": params[b"IRSHUTter"],
                "State": params[b"MODE"],
            }

            status = generate_status(conf, laser_states)
            emulator.write(status + b"\n")

        elif len(parts) == 2:
            # try:
            # 	val = int(parts[1])
            # except:
            val = parts[1].replace(b" ", b"").replace(b"\r", b"")
            # else:
            # 	val = float(parts[1])
            print("<", parts[0], val, val != 0)
            params[parts[0]] = parts[1]
            if parts[0] == b"SHUTter":
                params[parts[0]] = parts[1] == b"1"
                print("set SHUTter", params[parts[0]])
                # params[b'Status'][b'Main shutter'] = parts[1]!=0
            elif parts[0] == b"IRSHUTter":
                params[parts[0]] = parts[1] == b"1"
                print("set IRSHUTter", params[parts[0]])
                # params[b'Status'][b'IR shutter'] = parts[1]!=0

            time.sleep(0.1)
        else:
            print(">", parts[0], params)
            if parts[0] in params:
                emulator.write(params[parts[0]] + b"\n")
                print(params[parts[0][:-1]])
            elif len(parts[0].split(b":")) > 1:
                key = parts[0].split(b":")[1].replace(b"?", b"")
                print(key)
                if key in params:
                    if type(params[key]) == bytes:
                        resp = params[key]
                    else:
                        resp = str(params[key]).encode()
                    emulator.write(resp + b"\n")
                # print(params[parts[0][:-1]])
            else:
                emulator.write(b"\r\n")
            time.sleep(0.1)
        # time.sleep(0.1)
        # for k in params:
        # 	if prev_params[k]!=params[k]:
