import os, subprocess, serial, time
from functools import wraps, reduce

# this script lets you emulate a serial device
# the client program should use the serial port file specifed by client_port

# if the port is a location that the user can't access (ex: /dev/ttyUSB0 often),
# sudo is required
import numpy as np
import re
from binascii import unhexlify, hexlify
import struct


class SerialEmulator(object):
    def __init__(self, device_port="ttydevice", client_port="ttyclient_ELLx"):
        self.device_port = device_port
        self.client_port = client_port
        if os.name == "posix":
            cmd = [
                "/usr/bin/socat",
                "-d",
                "-d",
                "PTY,link=%s,raw,echo=0" % self.device_port,
                "PTY,link=%s,raw,echo=0" % self.client_port,
            ]
        else:
            cmd = [
                "socat.exe",
                "-d",
                "-d",
                "PTY,link=%s,raw,echo=0" % self.device_port,
                "PTY,link=%s,raw,echo=0" % self.client_port,
            ]
        self.proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        time.sleep(1)
        self.serial = serial.Serial(self.device_port, 9600, rtscts=True, dsrdtr=True)
        self.err = b""
        self.out = b""

    def write(self, out):
        self.serial.write(out)

    def flush(self):
        self.serial.flushInput()
        self.serial.flushOutput()

    def read(self):
        line = b""
        while self.serial.inWaiting() > 0:
            s = self.serial.read(1)
            if s == b"\n":
                break
            line += s
        # print(line)
        return line

    def __del__(self):
        self.stop()

    def stop(self):
        self.proc.kill()
        self.out, self.err = self.proc.communicate()

    def _checksum(self, command):
        return reduce(lambda x, y: x ^ y, map(ord, command))


if __name__ == "__main__":
    import signal

    signal.signal(signal.SIGINT, signal.SIG_DFL)

    emulator = SerialEmulator("ttydevice", "ttyclient_ELLx")

    current_pos = 0
    while True:
        line = emulator.read().split(b"\r")[0]
        command = line[1:3]

        # print('raw: |',parts,'|')
        if len(line) == 0:
            # print(line)
            # print(params)
            time.sleep(0.1)
            continue
        stage_address = line[:1]
        value = line[3:]
        if command == b"ma":
            current_pos = struct.unpack(">l", unhexlify(value))[0]
            p = (
                stage_address
                + b"PO"
                + hexlify(struct.pack(">l", current_pos))
                + b"\r\n"
            )
            emulator.write(p)

        elif command == b"mr":
            current_pos += struct.unpack(">l", unhexlify(value))[0]
            p = (
                stage_address
                + b"PO"
                + hexlify(struct.pack(">l", current_pos))
                + b"\r\n"
            )
            emulator.write(p)

        elif command == b"gp":
            p = (
                stage_address
                + b"PO"
                + hexlify(struct.pack(">l", current_pos))
                + b"\r\n"
            )
            emulator.write(p)

        elif command == b"gs":
            p = stage_address + b"GS00\r\n"
            emulator.write(p)

        elif command == b"in":
            p = stage_address + b"IN061234567820150181001F00000001\r\n"
            emulator.write(p)

        elif b"ho" in line:
            current_pos = 0
            p = (
                stage_address
                + b"PO"
                + hexlify(struct.pack(">l", current_pos))
                + b"\r\n"
            )
            emulator.write(p)

        else:
            pass
        time.sleep(0.1)
