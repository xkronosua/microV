# -*- coding: utf-8 -*-
import ctypes  # give location of dll
from ctypes import (
    c_int,
    c_bool,
    c_long,
    c_void_p,
    c_float,
    c_double,
    c_char,
    c_char_p,
    c_ulong,
    byref,
    create_string_buffer,
)
import time, os
import numpy as np

from ctypes import cdll


import microscope
import microscope.abc

import logging
import typing

_logger = logging.getLogger(__name__)


TLCCS_STATUS_SCAN_IDLE = 0x0002  # // CCS waits for new scan to execute
TLCCS_STATUS_SCAN_TRIGGERED = 0x0004  # // scan in progress
TLCCS_STATUS_SCAN_START_TRANS = 0x0008  # // scan starting
TLCCS_STATUS_SCAN_TRANSFER = 0x0010  # // scan is done, waiting for data transfer to PC
TLCCS_STATUS_WAIT_FOR_EXT_TRIG = (
    0x0080  # / same as IDLE except that external trigger is armed
)

IDLE = 2
CONT_SCAN = 4
DATA_READY = 16
WAITING_FOR_TRIG = 128
NUM_RAW_PIXELS = 3648
BYTES_PER_DOUBLE = 8
MAX_ATTEMPTS = 10  # maximum number of attempts to take a spectrum


class CCS_lib:
    def __init__(self, demo=False):
        if not demo:
            self.libDLL = cdll.LoadLibrary("TLCCS_64.dll")
            self.tlccs_init = self.libDLL["tlccs_init"]
            self.tlccs_init.argtypes = (c_char_p, c_bool, c_bool, ctypes.POINTER(c_int))
            self.tlccs_init.restype = c_char

            self.tlccs_close = self.libDLL["tlccs_close"]
            self.tlccs_close.argtypes = (c_int,)
            self.tlccs_close.restype = c_char

            self.tlccs_error_message = self.libDLL["tlccs_error_message"]
            self.tlccs_error_message.argtypes = (c_int, c_char, c_char_p)
            self.tlccs_error_message.restype = c_char

            self.tlccs_setIntegrationTime = self.libDLL["tlccs_setIntegrationTime"]
            self.tlccs_setIntegrationTime.argtypes = (c_int, c_double)
            self.tlccs_setIntegrationTime.restype = c_char

            self.tlccs_getIntegrationTime = self.libDLL["tlccs_getIntegrationTime"]
            self.tlccs_getIntegrationTime.argtypes = (c_int, ctypes.POINTER(c_double))
            self.tlccs_getIntegrationTime.restype = c_char

            self.tlccs_startScan = self.libDLL["tlccs_startScan"]
            self.tlccs_startScan.argtypes = (c_int,)
            self.tlccs_startScan.restype = c_char

            self.tlccs_startScanCont = self.libDLL["tlccs_startScanCont"]
            self.tlccs_startScanCont.argtypes = (c_int,)
            self.tlccs_startScanCont.restype = c_char

            self.tlccs_startScanExtTrg = self.libDLL["tlccs_startScanExtTrg"]
            self.tlccs_startScanExtTrg.argtypes = (c_int,)
            self.tlccs_startScanExtTrg.restype = c_char

            self.tlccs_getDeviceStatus = self.libDLL["tlccs_getDeviceStatus"]
            self.tlccs_getDeviceStatus.argtypes = (c_int, ctypes.POINTER(c_int))
            self.tlccs_getDeviceStatus.restype = c_char

            self.tlccs_getScanData = self.libDLL["tlccs_getScanData"]
            self.tlccs_getScanData.argtypes = (c_int, ctypes.POINTER(c_double))
            self.tlccs_getScanData.restype = c_char

            self.tlccs_getWavelengthData = self.libDLL["tlccs_getWavelengthData"]
            self.tlccs_getWavelengthData.argtypes = (
                c_int,
                c_int,
                ctypes.POINTER(c_double),
                ctypes.POINTER(c_double),
                ctypes.POINTER(c_double),
            )
            self.tlccs_getWavelengthData.restype = c_char


class _CCS200:

    instr = c_long(0)

    def init(self, demo=False, resource=b"USB0::0x1313::0x8089::M00310899::RAW"):
        self.ccs = CCS_lib(demo)
        r = self.ccs.tlccs_init(resource, 0, 0, self.instr)
        if not r == b"\x00":
            print("init>", self.instr.value, self.error_message(r), r)
        return r

    def close(self):
        r = self.ccs.tlccs_close(self.instr)
        if not r == b"\x00":
            print("close>", self.instr.value, self.error_message(r), r)
        return r

    def error_message(self, err):
        buf = create_string_buffer(256)
        r = self.ccs.tlccs_error_message(self.instr, err, buf)
        return buf.value

    def setIntegrationTime(self, val=0.1):
        r = self.ccs.tlccs_setIntegrationTime(self.instr, val)
        if not r == b"\x00":
            print("setIntegrationTime>", self.instr.value, self.error_message(r), r)
        return r

    def getIntegrationTime(self, val=0.1):
        val = ctypes.c_double(0)
        r = self.ccs.tlccs_getIntegrationTime(self.instr, val)
        if not r == b"\x00":
            print("getIntegrationTime>", self.instr.value, self.error_message(r), r)
        return val.value

    def startScan(self):
        r = self.ccs.tlccs_startScan(self.instr)
        if not r == b"\x00":
            print("startScan>", self.instr.value, self.error_message(r), r)
        return r

    def startScanCont(self):
        r = self.ccs.tlccs_startScanCont(self.instr)
        if not r == b"\x00":
            print("startScanCont>", self.instr.value, self.error_message(r), r)
        return r

    def startScanExtTrg(self):
        r = self.ccs.tlccs_startScanExtTrg(self.instr)
        if not r == b"\x00":
            print("startScanExtTrg>", self.instr.value, self.error_message(r), r)
        return r

    def getDeviceStatus(self):
        status = c_int(0)
        r = self.ccs.tlccs_getDeviceStatus(self.instr, status)
        if not r == b"\x00":
            print("getDeviceStatus>", self.instr.value, self.error_message(r), r)
        return status.value

    def getScanData(self):

        data = (c_double * NUM_RAW_PIXELS)()
        r = self.ccs.tlccs_getScanData(self.instr, data)
        if not r == b"\x00":
            print("getScanData>", self.instr.value, self.error_message(r), r)
        return np.ctypeslib.as_array(data)

    def getWavelengthData(self):

        data = (c_double * NUM_RAW_PIXELS)()
        MIN = c_double(0)
        MAX = c_double(0)
        r = self.ccs.tlccs_getWavelengthData(self.instr, 0, data, MIN, MAX)
        if not r == b"\x00":
            print(
                "getWavelengthData>",
                0,
                self.instr.value,
                MIN,
                MAX,
                self.error_message(r),
                r,
            )
        return np.ctypeslib.as_array(data)  # , MIN.value, MAX.value

    def getSpectra(self):
        self.startScan()
        intens = self.getScanData()
        wavelength = self.getWavelengthData()
        return (wavelength, intens)


class CCS200(microscope.abc.Camera):
    _fetch_thread = None
    _handle = None
    initialized = False

    def __init__(self, demo=False, **kwargs) -> None:

        # _logger.info(f'CCS200: demo={demo}')
        super().__init__(**kwargs)
        self._acquiring = False
        if demo:
            return
        else:
            self._handle = _CCS200()

        # self._serial_number = serial_number
        self._sensor_shape = (1, 3648)
        self._roi = microscope.ROI(0, 0, NUM_RAW_PIXELS, 1)
        self._binning = microscope.Binning(1, 1)
        self.set_trigger(microscope.TriggerType.SOFTWARE, microscope.TriggerMode.ONCE)

        # self.set_trigger( microscope.TriggerType.RISING_EDGE , microscope.TriggerMode.BULB )
        self.initialize()
        # _logger.info(f'CCS200: demo={demo}')
        self.initialized = True

    def initialize(self):
        if not self.initialized:
            self._handle.init()

            # self._handle.close()
            # self._handle.init()
        self.initialized = True
        return self.initialized

    def is_alive(self):
        return self.initialized

    def get_exposure_time(self):
        return self._handle.getIntegrationTime()

    def _do_shutdown(self):
        if not self._handle is None and self.initialized:
            self._handle.close()
        self.initialized = False

    def _do_trigger(self):
        if not self._acquiring:
            if self._trigger_type == microscope.TriggerType.SOFTWARE:
                self._handle.startScan()
            else:
                self._handle.startScanExtTrg()
            self._acquiring = True

    def _fetch_data(self):
        data = None
        if self._acquiring:
            data = self._handle.getScanData()
            data = data.reshape(1, len(data))
            self._acquiring = False
        return data

    def getAllData(self):
        self._handle.startScan()
        data = self._handle.getScanData()
        wavelength = self.get_wavelength()
        exposure = self.get_exposure_time()
        return wavelength, data, exposure

    def get_wavelength(self):
        return self._handle.getWavelengthData()

    def _get_binning(self):
        return (1, 1)

    def _get_roi(self):
        return self._roi

    def _get_sensor_shape(self):
        return self._sensor_shape

    def _set_binning(self, bin=(1, 1)):
        pass

    def _set_roi(self, roi):
        self.roi = roi

    def abort(self):
        pass

    def set_exposure_time(self, val):
        self._handle.setIntegrationTime(val)

    def set_trigger(self, ttype: microscope.TriggerType, tmode: microscope.TriggerMode):
        self._trigger_mode = tmode
        self._trigger_type = ttype

    @property
    def trigger_mode(self) -> microscope.TriggerMode:
        return self._trigger_mode

    @property
    def trigger_type(self) -> microscope.TriggerType:
        return self._trigger_type


if __name__ == "__main__":

    # c = CCS200()
    # print(c.init())
    # print(c.setIntegrationTime(0.01))
    # data = []
    # for i in range(10):
    # 	print(c.startScanExtTrg())
    # 	print(c.getDeviceStatus())
    # 	d = c.getScanData()
    # 	data.append(d)
    # print(c.close())
    # from pylab import *
    # for i in data:
    # 	#plot(np.linspace(1.927816925e+002,1.023623779e+003,len(i))[::-1],i)
    # 	plot(i)
    # show(0)

    # c=_CCS200()
    # c.init()
    # c.startScan()
    # print(c.getIntegrationTime())
    # print(c.getScanData())

    # print(c.getWavelengthData())
    # c.close()

    # c = CCS200()
    # c.enable()
    print(0)
