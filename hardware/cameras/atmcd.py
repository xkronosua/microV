from microscope.cameras.atmcd import (
    AndorAtmcd,
    GetPixelSize,
    SetTemperature,
    SetReadMode,
    SetPreAmpGain,
    CoolerON,
    SetFanMode,
    SetPreAmpGain,
    SetVSSpeed,
    SetADChannel,
    SetAcquisitionMode,
    SetShutter,
    GetDetector,
    IsTriggerModeAvailable,
    StartAcquisition,
    SetTriggerMode,
    GetTemperature,
    ReadMode,
    AcquisitionMode,
    AtmcdException,
    DRV_NO_NEW_DATA,
    GetMostRecentImage,
)
import microscope
import microscope.abc
import time
from ctypes import c_long

TRIGGER_MODE_CODES = {"internal": 0, "external": 1, "softwareTrigger": 10}
FAN_MODES = {"full": 0, "low": 1, "off": 2}


class AndorCamera(AndorAtmcd):
    """Implements CameraDevice interface for Andor ATMCD library."""

    amplifiers = []

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_pixel_size(self):
        """Return the pixel size."""
        with self:
            return GetPixelSize()

    def set_sensor_temperature(self, temperature):
        with self:
            SetTemperature(temperature)

    def get_sensor_temperature(self):
        with self:
            return GetTemperature()


if __name__ == "__main__":
    c = AndorCamera()
    # c.configure()
    c.set_trigger(
        microscope.TriggerType.SOFTWARE,
        microscope.TriggerMode.ONCE,
    )
    print(c.get_all_settings())
    c.set_setting("Binning", (1, 127))
    c.set_setting("Cooler Enabled", True)
    c.set_setting("Fan mode", 0)
    c.set_setting("TemperatureSetPoint", -85)

    c.enable()

    # print(c.grab_next_image())
    # c.disable()
