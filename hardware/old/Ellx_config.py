# Serve two test cameras, each on their own process.
from microscope.device_server import device
from Spectra_Physics_Insight_X3 import InSightX3Laser
import logging

print(logging.handlers)
logging.getLogger().setLevel(logging.DEBUG)
DEVICES = [
    device(InSightX3Laser, host="127.0.0.1", port=8000, conf={"demo": False}),
]
