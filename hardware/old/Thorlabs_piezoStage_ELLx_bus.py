from binascii import unhexlify, hexlify
import struct
import serial
from serial.tools import list_ports

from serialManager import ResourceManager

from hardwareUtils import TimeoutLock
import threading
from queue import Queue
import time
from collections import deque
import re
import logging

root = logging.getLogger()
root.setLevel(logging.DEBUG)

STATUS_ERROR_CODES = {
    0: "OK, no error",
    1: "Communication time out",
    2: "Mechanical time out",
    3: "Command error or not supported",
    4: "Value out of range",
    5: "Module isolated",
    6: "Module out of isolation",
    7: "Initializing error",
    8: "Thermal error",
    9: "Busy",
    10: "Sensor Error(May appear during self test. If code persists there is an error)",
    11: "Motor Error(May appear during self test. If code persists there is an error)",
    12: "Out of Range(e.g. stage has been instructed to move beyond its travel range).",
    13: "Over Current error",
}


class ELLx(object):
    Stage = None
    activeStageAddress = b"0"
    lock = TimeoutLock.lock()
    last_readout = {"time": time.time(), "val": b""}
    readout_log = deque()
    position = {b"0": 0, b"1": 0, b"2": 0, b"3": 0}

    status = {b"0": 0, b"1": 0, b"2": 0, b"3": 0}
    status_code = {b"0": 0, b"1": 0, b"2": 0, b"3": 0}
    info = {}
    demo = False
    addressDict = {}
    filters_min_position = 0
    filters_max_position = 93
    filters_PositionIndexDict = {0: 0, 31: 1, 62: 2, 93: 3}
    stepsPerUnit = {
        "filters": 31,
        "polarizer": 143360 / 360,
        "polarizer1": 143360 / 360,
        "polarizer2": 143360 / 360,
    }  # steps per deg

    def __init__(
        self,
        address=b"0",
        addressDict={"filters": b"0", "polarizer1": b"1", "polarizer2": b"2"},
        demo=False,
    ):
        if demo:
            self.Stage = ResourceManager_sim()
        else:
            self.Stage = ResourceManager()
        self.demo = demo
        # self.PositionIndexDict = PositionIndexDict
        self.activeStageAddress = address

        self.addressDict = addressDict
        self.namesDict = {val: key for key, val in addressDict.items()}

        # self.Stage.addHandler(self._test_handler)
        self.Stage.addHandler(self._status_handler)
        self.Stage.addHandler(self._position_handler)
        self.Stage.addHandler(self._info_handler)
        # self.min_position = min_position
        # self.max_position = max_position

    def __getitem__(self, key):
        if key in self.addressDict:
            self.activeStageAddress = self.addressDict[key]
        return self

    def __del__(self):
        if not self.Stage is None:
            self.Stage.close()

    def connect(self, port=None, baudrate=9600):

        if port is None:
            ports = list(list_ports.grep("0403:6015"))
            for p in ports:
                if p.serial_number == "DO02FRYTA":
                    port = p.device
                    break
            if port is None and not self.demo:
                logging.error("No ELLx device found.")
                return
        try:
            self.Stage.open_resource(port, baud_rate=baudrate, newlineChar=b"\r\n")
            return True
        except Exception as ex:
            logging.error("ELLx open_resource error", exc_info=ex)
            return False

    def disconnect(self):
        self.Stage.close()

    def _test_handler(self, msg):
        # import pdb; pdb.set_trace()
        print("_test_handler", msg)
        return False

    def _position_handler(self, msg):
        m = re.search(b"([0-9A-F])PO(.*)", msg)
        if m:
            address = m.groups()[0]
            r = m.groups()[1].split(b"\r")[0]
            # r = val.split(b'\r')[0]
            # print(f"[{msg}],[{r}]")
            self.position[address] = (
                struct.unpack(">l", unhexlify(r))[0]
                / self.stepsPerUnit[self.namesDict[address]]
            )
            logging.debug(f"_position_handler:{self.position}")
            return True
        else:
            return False

    def _status_handler(self, msg):

        m = re.search(b"([0-9A-F])GS(.*)", msg)
        if m:
            # import pdb; pdb.set_trace()
            address = m.groups()[0]
            r = m.groups()[1].split(b"\r")[0]

            # r = val.split(b'\r')[0]
            self.status_code[address] = struct.unpack(">b", unhexlify(r))[0]

            if self.status_code[address] in STATUS_ERROR_CODES:
                self.status[address] = STATUS_ERROR_CODES[self.status_code[address]]
            logging.debug(f"_status_handler:{self.status}, {msg}")
            return True
        else:
            return False

    def _info_handler(self, msg):
        m = re.search(b"([0-9A-F])IN(.*)", msg)
        if m:
            address = m.groups()[0]
            r = address + b"IN" + m.groups()[1].split(b"\r")[0]
            # r = val.split(b'\r')[0]
            # import pdb; pdb.set_trace()
            if len(r) == 33:
                # r = unhexlify(resp)
                out = {
                    "ELL": r[3:5],
                    "SN": r[5:13],
                    "Year": r[13:17],
                    "FW rel": r[17:19],
                    "HW rel": r[19:21],
                }
                out["TRAVEL mm/deg"] = struct.unpack(">h", unhexlify(r[21:25]))[0]
                out["PULSES/M.U."] = struct.unpack(">l", unhexlify(r[25:33]))[0]
                self.info[address] = out
                logging.debug(f"_info_handler:{self.info}")
                return True
            else:
                return False
        else:
            return False

    def getInfo(self):
        """
        Instruct hardware unit to identify itself giving information
        about model, serial number, firmware and hardware releases, travel,
        encoder pulses per measurement unit.
        """
        self.info[self.activeStageAddress] = None
        r = self.Stage.write(self.activeStageAddress + b"in" + b"\r\n")

        # rr = self.Stage.readline()
        # import pdb; pdb.set_trace()
        if r is None:
            return
        t0 = time.time()
        while self.info[self.activeStageAddress] is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.info[self.activeStageAddress]

    def getStatus(self, wait=True):
        """
        Instruct hardware unit to get module status or error value.
        Once read the error value is cleared
        """
        self.status[self.activeStageAddress] = None
        r = self.Stage.write(self.activeStageAddress + b"gs" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while (
            self.status[self.activeStageAddress] is None and self.Stage.online and wait
        ):
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.status[self.activeStageAddress]

    def getPosition(self):
        """
        Instruct hardware unit to get module position
        """
        self.position[self.activeStageAddress] = None
        r = self.Stage.write(self.activeStageAddress + b"gp" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.position[self.activeStageAddress] is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position[self.activeStageAddress]

    def forward(self):
        if self.position[self.activeStageAddress] is None:
            self.getPosition()
        if self.position[self.activeStageAddress] is None:
            return
        if self.namesDict[self.activeStageAddress] == "filters":
            if self.position[self.activeStageAddress] >= self.filters_max_position:
                return self.position[self.activeStageAddress]

        self.position[self.activeStageAddress] = None
        r = self.Stage.write(self.activeStageAddress + b"fw" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.position[self.activeStageAddress] is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position[self.activeStageAddress]

    def backward(self):
        if self.position[self.activeStageAddress] is None:
            self.getPosition()
        if self.position[self.activeStageAddress] is None:
            return
        if self.namesDict[self.activeStageAddress] == "filters":
            if self.position[self.activeStageAddress] <= self.filters_min_position:
                return self.position[self.activeStageAddress]
        self.position[self.activeStageAddress] = None
        r = self.Stage.write(self.activeStageAddress + b"bw" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.position[self.activeStageAddress] is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position[self.activeStageAddress]

    def moveHome(self, direction=0):
        direction = hexlify(struct.pack(">b", direction))
        self.position[self.activeStageAddress] = None
        r = self.Stage.write(self.activeStageAddress + b"ho" + direction + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.position[self.activeStageAddress] is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position[self.activeStageAddress]

    def moveAbs(self, position):
        position = int(
            position * self.stepsPerUnit[self.namesDict[self.activeStageAddress]]
        )

        position_b = hexlify(struct.pack(">l", position)).upper()

        self.position[self.activeStageAddress] = None
        r = self.Stage.write(self.activeStageAddress + b"ma" + position_b + b"\r\n")
        # self.status_code[self.activeStageAddress]=-1
        # self.Stage.write(self.activeStageAddress+b"gs"+b'\r\n')
        if r is None:
            return
        t0 = time.time()
        # self.getStatus()
        while self.position[self.activeStageAddress] is None and self.Stage.online:
            time.sleep(0.01)
            # self.getStatus()
            # self.Stage.write(self.activeStageAddress+b"gs"+b'\r\n')
            if time.time() - t0 > 100:
                break
        return self.position[self.activeStageAddress]

    def moveRel(self, step):
        step = int(step * self.stepsPerUnit[self.namesDict[self.activeStageAddress]])

        step_b = hexlify(struct.pack(">l", step)).upper()

        self.position[self.activeStageAddress] = None
        r = self.Stage.write(self.activeStageAddress + b"mr" + step_b + b"\r\n")
        # self.status_code[self.activeStageAddress]=-1
        # self.Stage.write(self.activeStageAddress+b"gs"+b'\r\n')
        if r is None:
            return
        t0 = time.time()
        # self.getStatus()
        while self.position[self.activeStageAddress] is None and self.Stage.online:
            time.sleep(0.01)
            # self.getStatus()
            # self.Stage.write(self.activeStageAddress+b"gs"+b'\r\n')
            if time.time() - t0 > 100:
                break
        return self.position[self.activeStageAddress]

    def getIndex(self):
        pos = self.getPosition()
        if pos in self.filters_PositionIndexDict:
            return self.filters_PositionIndexDict[pos]
        else:
            return

    def setIndex(self, index):

        if index in self.filters_PositionIndexDict.values():
            pos = list(self.filters_PositionIndexDict.keys())[
                list(self.filters_PositionIndexDict.values()).index(index)
            ]
            r = self.moveAbs(pos)
            return r
        else:
            return

    def SAVE_USER_DATA(self):
        """
        Instruct hardware unit to save motor parameters (such as forward or
        backward frequencies) and other reserved or user parameters
        """
        r = self.Stage.write(self.activeStageAddress + b"us" + b"\r\n")
        if r is None:
            return

    def CHANGEADDRESS(self, new_address=b""):
        """
        Instruct hardware unit to change to the address of another device.
        The address is entered in the range 0-F. The default value is a 0.
        """
        r = self.Stage.write(self.activeStageAddress + b"ca" + new_address + b"\r\n")
        if r is None:
            return

    def ELLx_HOSTREQ_MOTOR1INFO(self):
        "i1"

    def ELLx_DEVGET_MOTOR1INFO(self):
        "I1"

    def ELLx_HOSTSET_FWP_MOTOR1(self):
        "f1"

    def ELLx_HOSTSET_BWP_MOTOR1(self):
        "b1"

    def ELLx_HOSTREQ_SEARCHFREQ_MOTOR1(self):
        "s1"

    def ELLx_HOSTREQ_SCANCURRENTCURVE_MOTOR1(self):
        "c1"

    def ELLx_DEVGET_CURRENTCURVEMEASURE_MOTOR1(self):
        "C1"

    def ELLx_HOST_ISOLATEMINUTES(self):
        "is"

    def ELLx_HOSTREQ_MOTOR2INFO(self):
        "i2"

    def ELLx_DEVGET_MOTOR2INFO(self):
        "I2"

    def ELLx_HOSTSET_FWP_MOTOR2(self):
        "f2"

    def ELLx_HOSTSET_BWP_MOTOR2(self):
        "b2"

    def ELLx_HOSTREQ_SEARCHFREQ_MOTOR2(self):
        "s2"

    def ELLx_HOSTREQ_SCANCURRENTCURVE_MOTOR2(self):
        "c2"

    def ELLx_DEVGET_CURRENTCURVEMEASURE_MOTOR2(self):
        "C2"

    def ELLx_HOSTREQ_MOTOR3INFO(self):
        "i3"

    def ELLx_DEVGET_MOTOR3INFO(self):
        "I3"

    def ELLx_HOSTSET_FWP_MOTOR3(self):
        "f3"

    def ELLx_HOSTSET_BWP_MOTOR3(self):
        "b3"

    def ELLx_HOSTREQ_SEARCHFREQ_MOTOR3(self):
        "s3"

    def ELLx_HOSTREQ_SCANCURRENTCURVE_MOTOR3(self):
        "c3"

    def ELLx_DEVGET_CURRENTCURVEMEASURE_MOTOR3(self):
        "C3"

    def ELLx_HOSTREQ_HOME(self):
        "ho"

    def ELLx_HOSTREQ_MOVEABSOLUTE(self):
        "ma"

    def ELLx_HOSTREQ_MOVERELATIVE(self):
        "mr"

    def ELLx_HOSTREQ_HOMEOFFSET(self):
        "go"

    def ELLx_DEVGET_HOMEOFFSET(self):
        "HO"

    def ELLx_HOSTSET_HOMEOFFSET(self):
        "so"

    def ELLx_HOSTREQ_JOGSTEPSIZE(self):
        "gj"

    def ELLx_DEVGET_JOGSTEPSIZE(self):
        "GJ"

    def ELLx_HOSTSET_JOGSTEPSIZE(self):
        "sj"

    def ELLx_HOST_MOTIONSTOP(self):
        "ms"

    def ELLx_HOST_GETPOSITION(self):
        "gp"

    def ELLx_DEV_GETPOSITION(self):
        "PO"

    def ELLx_HOSTREQ_VELOCITY(self):
        "gv"

    def ELLx_DEVGET_VELOCITY(self):
        "GV"

    def ELLx_HOSTSET_VELOCITY(self):
        "sv"

    def ELLx_DEVGET_BUTTONSTATUS(self):
        "BS"

    def ELLx_DEVGET_BUTTONPOSITION(self):
        "BO"

    def ELLx_HOST_GROUPADDRESS(self):
        "ga"

    def ELLx_HOST_DRIVETIME(self):
        "t1,t2,t3"

    def ELLx_DEV_DRIVETIME(self):
        "P1,P2,P3"

    def ELLx_HOST_PADDLEMOVEABSOLUTE(self):
        "a1,a2,a3"

    def ELLx_DEV_PADDLEMOVEABSOLUTE(self):
        "P1,P2,P3"

    def ELLx_HOST_PADDLEMOVERELATIVE(self):
        "r1,r2,r3"

    def ELLx_DEV_PADDLEMOVERELATIVE(self):
        "P1,P2,P3"

    def ELLx_HOST_OPTIMIZE_MOTORS(self):
        "om"

    def ELLx_DEV_STATUS(self):
        "GS"

    def ELLx_HOST_CLEAN_MECHANICS(self):
        "cm"

    def ELLx_DEV_STATUS(self):
        "GS"

    def ELLx_HOST_STOP(self):
        "st"

    def ELLx_DEV_STATUS(self):
        "GS"

    def ELLx_HOST_ENERGIZE_MOTOR(self):
        "e1"

    def ELLx_DEV_STATUS(self):
        "GS"

    def ELLx_HOST_HALT_MOTOR(self):
        "h1"

    def ELLx_DEV_STATUS(self):
        "GS"


class ResourceManager_sim(ResourceManager):
    last_line = ""
    pos = 0
    resp_line = ""
    handlers = []
    stage_address = b"0"
    step = 31
    min_position = 0
    max_position = 93

    def __init__(self, parent=None):
        pass  # super(ResourceManager_sim,self).__init__()

    def open_resource(self, port, baud_rate=115200, timeout=1, readout_mode=0):
        self.online = True

    def close(self):
        self.online = False

    def query(self, line):
        time.sleep(1)
        return b"000000000\n"

    def write(self, line):
        line = line.rstrip(b"\n").rstrip(b"\r")
        if line == b"":
            pass  # time.sleep(0.1)
        elif b"gs" in line:
            p = self.stage_address + b"GS00\r"
            for h in self.handlers:
                r = h(p)
                print(r, p)
        elif b"fw" in line:
            self.pos += self.step
            if self.pos > self.max_position:
                self.pos = self.max_position

            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)

        elif b"bw" in line:
            self.pos -= self.step
            if self.pos < self.min_position:
                self.pos = self.min_position
            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)

        elif b"ma" in line:
            self.pos = struct.unpack(">l", unhexlify(line[3:]))[0]

            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)

        elif b"gp" in line:
            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)
                # print(r,p)
        elif b"ho" in line:
            self.pos = 0
            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)
        else:
            return True
        return True

    def addHandler(self, handler):
        self.handlers.append(handler)


if __name__ == "__main__":
    import sys

    demo = False
    if "sim" in sys.argv:
        demo = True
    ellx = ELLx(demo=demo)
    ellx.connect()
    print("Info:", ellx["filters"].getInfo())
    print("Info:", ellx["polarizer1"].getInfo())
    # time.sleep(100)
    # ellx.disconnect()
    # print(ellx['filters'].moveHome())

    # print('Status:',ellx['filters'].getStatus())
    # print('pos:',ellx['filters'].getPosition())
    # print('index:',ellx['filters'].getIndex())
    # print(ellx['filters'].moveHome())
    # print(ellx['filters'].forward())
    # print('index:',ellx['filters'].getIndex())
    # print(ellx['filters'].forward())
    # print('index:',ellx['filters'].getIndex())
    # print(ellx['filters'].forward())
    # print('index:',ellx['filters'].getIndex())
    # print(ellx['filters'].backward())
    # print(ellx['filters'].setIndex(3))
    # print(ellx['filters'].moveHome())
    # print('done')
    # ellx.disconnect()

    # ellx = ELLx(address=b'1',demo=demo)
    # ellx.connect()

    print("Status:", ellx["polarizer1"].getStatus())
    print("pos:", ellx["polarizer1"].getPosition())
    time.sleep(3)
    print("Home:", ellx["polarizer1"].moveHome())
    time.sleep(3)
    print(f"moveRel {90}:", ellx["polarizer1"].moveAbs(90))
    print(f"moveRel {0}:", ellx["polarizer1"].moveAbs(0))
    print("Status:", ellx["polarizer1"].getStatus())
    print("pos:", ellx["polarizer1"].getPosition())
    for i in range(7):
        s = 1
        if i > 5:
            s = -1
        print(f"moveRel {s*(i+1)*45}:", ellx["polarizer1"].moveRel(s * 45))
        time.sleep(1)
    print("Status:", ellx["polarizer1"].getStatus())
    # time.sleep(3)
    # print('Home:', ellx['polarizer1'].moveHome())
    # time.sleep(3)
    # print('pos:',ellx['polarizer1'].getPosition())
    # for i in range(7):
    # 	print(f'moveAbs {i*45}:', ellx['polarizer1'].moveAbs(i*10))
    #
    # 	time.sleep(5)
    # 	print('Status:',ellx['polarizer1'].getStatus())
    # 	time.sleep(2)
    # print('pos:',ellx['polarizer1'].getPosition())
    # print(ellx['polarizer1'].moveAbs(2))
    # print('pos:',ellx['polarizer1'].getPosition())
    # print(ellx['polarizer1'].moveRel(-1))
    # print(ellx['polarizer1'].moveHome())

    print("done")
    # ellx.disconnect()
