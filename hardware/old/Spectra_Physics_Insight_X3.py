#!/usr/bin/env python3

import logging
import serial
import microscope.abc
from serial.tools import list_ports
import time
from threading import Thread

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class InSightX3Laser(microscope.abc.SerialDeviceMixin, microscope.abc.LightSource):
    _monitor_thread = None

    def __init__(self, com=None, baud=115200, timeout=0.1, demo=False, **kwargs):
        super().__init__(**kwargs)

        if com is None:
            ports = list(list_ports.grep("10c4:ea60"))
            if len(ports) > 0:
                com = ports[0].device
        if demo:
            com = "./ttyclient"
            baud = 9600
        self.demo = demo
        self.connection = serial.Serial(
            port=com,
            baudrate=baud,
            timeout=timeout,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            parity=serial.PARITY_NONE,
        )
        # Start a logger.
        response = self.send(b"*IDN?")
        _logger.info("InSightX3 laser serial number: [%s]", response.decode())
        self._max_power_mw = 3000  # ???

    def send(self, command):
        """Send command and retrieve response."""
        success = False
        while not success:
            self._write(command)
            response = self._readline()
            # Catch zero-length responses to queries and retry.
            if not command.endswith(b"?"):
                success = True
            elif len(response) > 0:
                success = True
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def send_locked(self, command):
        response = self.send(command)
        return response

    def _write(self, command):
        """Send a command."""
        response = self.connection.write(command + b"\r\n")
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _write_locked(self, command):
        """Send a command."""
        response = self._write(command)
        return response

    # def _readline(self):
    # 	"""Read a line from connection without leading and trailing whitespace.
    # 	We override from SerialDeviceMixin
    # 	"""
    # 	response = self.connection.readline().strip()
    # 	if self.connection.readline().strip() != b"OK":
    # 		raise microscope.DeviceError(
    # 			"Did not get a proper answer from the laser serial comm."
    # 		)
    # 	return response

    def _flush_handshake(self):
        self.connection.readline()

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_status(self):
        status = self.send(b"STB?")
        resp = {}

        resp["RawSTB"] = status.decode()
        try:
            status = int(status)
        except:
            res = {
                "RawSTB": None,  # "i8"),
                "Emission": None,  # "i2"),
                "Pulsing": None,  # "i2"),
                "Main shutter": None,  # "i2"),
                "IR shutter": None,  # "i2"),
                "Servo on": None,  # "i2"),
                "User interlock": None,  # "i2"),
                "Keyswitch": None,  # "i2"),
                "Power supply": None,  # "i2"),
                "Internal": None,  # "i2"),
                "Warning": None,  # "i2"),
                "fault": 1,  # "i2"),
                "State": "Status is wrong",  # "S20"),
                "ON_OFF": None,  # "i2"),
                "time": 0,  #'float64')
            }
            return res
        # 0 0x00000001 Emission The laser diodes are energized. Laser emission is possible if the
        # 		shutter(s) is (are) open.

        resp["Emission"] = (status & 0x00000001) == 1

        # 1 0x00000002 Pulsing 1 = “pulsing”
        # 		The name PULSING is used for compatibility with Mai Tai. In the
        # 		InSight DS+ system, this bit indicates that the laser has either:
        # 		a) Reached the Run state and can be used to take data.
        # 		or
        # 		b) Reached the Align state and can be used to align the optical
        # 		system.
        resp["Pulsing"] = status & 0x00000002

        # 2 0x00000004 Main shutter 1 = The main shutter is open (sensed position).
        resp["Main shutter"] = (status & 0x00000004) != 0

        # 3 0x00000008 Dual beam
        # 		shutter 1 = The IR shutter is open (sensed position).
        # 		N OTE : On systems purchased without the optional dual beam
        # 		output, this bit is always set to 1.
        resp["IR shutter"] = (status & 0x00000008) != 0

        # 5 0x00000020 Servo on 1 = Servo is on (see “SERVO” command on page A-6).
        resp["Servo on"] = status & 0x00000020

        # 9 0x00000200 User interlock 1 = The user interlock (CDRH interlock) is open; laser is forced off.
        resp["User interlock"] = status & 0x00000200

        # 10 0x00000400 Keyswitch 1 = The safety keyswitch interlock is open; laser is forced off.
        resp["Keyswitch"] = status & 0x00000400

        # 11 0x00000800 Power supply 1 = The power supply interlock is open; laser is forced off.
        resp["Power supply"] = status & 0x00000800

        # 12 0x00001000 Internal 1 = The internal interlock is open; laser is forced off.
        resp["Internal"] = status & 0x00001000

        # 14 0x00004000 Warning 1 = The system is currently detecting a warning condition. The laser
        # 	continues to operate, but it is best to resolve the issue at your earliest
        # 	convenience.
        # 	Use READ:HISTORY? (page A-4) to see what is causing the
        # 	warning.
        resp["Warning"] = status & 0x00004000

        # 15 0x00008000 fault 1 = The system is currently detecting a fault condition. InSight
        # 	immediately turns off the laser diodes. Use READ:HISTORY? to see
        # 	what is causing the fault.
        # 	Note: The fault condition may clear itself when the laser turns itself
        # 	off. If so, the fault bit clears.
        resp["fault"] = status & 0x00008000

        # 16 to 22 0x007F0000	State
        # 	After masking, shift right 16 bits and interpret as a number with a
        # 	value from 0 to 127 (see Table A-2).
        # 	Most state numbers are not specifically assigned, but several ranges
        # 	can be described and three specific values (25, 50, and 60) are
        # 	guaranteed not to change.
        s = (status & 0x007F0000) >> 16
        if s >= 0 and s <= 24:
            resp["State"] = "Initializing"
        elif s == 25:
            resp["State"] = "READY"
        elif s >= 26 and s <= 49:
            resp["State"] = "Turning on / optimizing"
        elif s == 50:
            resp["State"] = "RUN"
        elif s >= 51 and s <= 59:
            resp["State"] = "Moving to Align mode"
        elif s == 60:
            resp["State"] = "ALIGN"
        elif s >= 61 and s <= 69:
            resp["State"] = "Exiting Align mode"
        else:
            resp["State"] = ""
        resp["time"] = time.time()
        return resp

    @microscope.abc.SerialDeviceMixin.lock_comms
    def enable(self):
        return self.laser_enable()

    def enable_nonblocking(self):
        """Turn the laser ON. Return True if we succeeded, False otherwise."""
        _logger.info("Turning laser ON.")
        status = self.get_status()

        if status["State"] != "READY":
            _logger.error("Laser not ready!")
            return False

        _logger.info("Laser: ON...")
        self._write_locked(b"ON")
        self._write_locked(b"TIMer:WATChdog 10")
        t0 = time.time()
        warmedup = b"0"

        while warmedup < b"100" and time.time() - t0 < 130:
            time.sleep(1)
            warmedup = self.send_locked(b"READ:PCTWarmedup?")
            _logger.info(f"PCTWarmedup:{warmedup.decode()}")
        status = self.get_status()
        t0 = time.time()
        while not status["State"] in ["RUN", "ALIGN"] and time.time() - t0 < 400:
            time.sleep(1)
            status = self.get_status()
            # print(r)
        if not status["State"] in ["RUN", "ALIGN"]:
            _logger.error("laserOn: FAIL!")
            return False

        if not self.get_is_on():
            # Something went wrong.
            _logger.error("Failed to turn ON. Current status:\r\n")
            _logger.error(self.get_status())
            return False

        self._monitor_thread = Thread(target=self._monitor_loop)
        self._monitor_thread.daemon = True
        self._monitor_thread.start()
        return True

    def _monitor_loop(self):
        status = self.get_status()
        while status["State"] != "READY":
            time.sleep(3)
            status = self.get_status()

    def _on_shutdown(self):
        self.disable()
        # self._flush_handshake()

    def initialize(self):
        pass  # self.flush_buffer()

    @microscope.abc.SerialDeviceMixin.lock_comms
    def disable(self):
        return self.disable_nonblocking()

    def disable_nonblocking(self):
        """Turn the laser OFF. Return True if we succeeded, False otherwise."""
        _logger.info("Turning laser OFF.")
        # Turning LASER OFF
        self._write_locked(b"OFF")
        # self._flush_handshake()

        if self.get_is_on():
            _logger.error("Failed to turn OFF. Current status:\r\n")
            _logger.error(self.get_status())
            return False
        return True

    @microscope.abc.SerialDeviceMixin.lock_comms
    def is_alive(self):
        if self.demo:
            self.connection.reset_output_buffer()
        reply = self.send(b"*IDN?")
        # 'Spectra-Physics, InSight DeepSee, xxxx, 0180-0.01.60/175-1.00.15/TN00001003'
        return reply.startswith(b"Spectra-Physics")

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_is_on(self):
        """Return True if the laser is currently able to produce light."""
        status = self.get_status()
        is_on = not (status["State"] in ["Initializing", "READY", ""])

        _logger.info("Are we on? [%s]", status["State"])
        return is_on

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _do_get_power(self) -> float:
        """Returns the laser output power (in Watts)."""
        if not self.get_is_on():
            return 0.0
        response = self.send(b"READ:POWer?")
        power = float(response.decode())  # W
        return power

    def _do_set_power(self, val):
        print("_do_set_power", val)

    @property
    @microscope.abc.SerialDeviceMixin.lock_comms
    def watchdog(self) -> int:
        """Get the number of seconds for the software watchdog timer."""
        response = self.send(b"TIMer:WATChdog")
        return int(response.decode())

    @microscope.abc.SerialDeviceMixin.lock_comms
    @watchdog.setter
    def watchdog(self, timeout: int) -> None:
        """Set the number of seconds for the software watchdog timer."""
        self._write(b"TIMer:WATChdog %d" % timeout)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _do_get_wavelength(self) -> int:
        """Internal function that actually returns the laser wavelength."""
        response = self.send(b"READ:WAVelength?")
        return int(response.decode())

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _do_set_wavelength(self, wl: int) -> None:
        """Internal function that actually sets the laser wavelength."""
        self._write(b"WAVelength %d" % wl)

    def get_set_wavelength(self) -> float:
        """Return the wavelength set point."""
        return self._set_point

    @property
    def wavelength(self) -> int:
        """Read the wavelength to between 680 and 1300 nm."""
        return self._do_get_wavelength()

    @wavelength.setter
    def wavelength(self, wl: int) -> None:
        """Set the wavelength to between 680 and 1300 nm."""
        self._do_set_wavelength(wl)
        self._set_point = wl

    @microscope.abc.SerialDeviceMixin.lock_comms
    def lcd_brightness(self) -> int:
        """Read the power supply LCD screen back illumination brightness level [0 - 255]."""
        response = self.send(b"LCD:BRIGhtness?")
        return int(response.decode())

    @microscope.abc.SerialDeviceMixin.lock_comms
    def set_lcd_brightness(self, val: int) -> None:
        """Set the power supply LCD screen back illumination brightness level [0 - 255]."""
        self._write(b"LCD:BRIGhtness %d" % val)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def MODE(self) -> str:
        """Read the system mode: value=[RUN, ALIGN]"""
        response = self.send(b"MODE?")
        return response.decode()

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setMODE(self, mode: str) -> None:
        """Set the system mode: value=[RUN, ALIGN]"""
        if mode in ["RUN", "ALIGN"]:
            self._write(b"MODE %s" % mode.encode())

    @microscope.abc.SerialDeviceMixin.lock_comms
    def shutter(self) -> bool:
        """Read the main shutter state."""
        # response = self.send(b"SHUTter?")[0] == b'1'
        status = self.get_status()
        return status["Main shutter"]

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setShutter(self, state: bool) -> None:
        """Set the main shutter state."""
        if state:
            state = b"1"
        else:
            state = b"0"
        self._write(b"SHUTter " + state)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def IRshutter(self) -> bool:
        """Read the IR shutter state."""
        # response = self.send(b"SHUTter?")[0] == b'1'
        status = self.get_status()
        return status["IR shutter"]

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setIRshutter(self, state: bool) -> None:
        """Set the IR shutter state."""
        if state:
            state = b"1"
        else:
            state = b"0"
        self._write(b"IRSHUTter " + state)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def precompensation(self) -> float:
        """Read the actual DeepSee precompensation motor position."""
        response = self.send(b"CONT:DSMPOS?")
        return float(response.decode())

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setPrecompensation(self, target: float) -> None:
        """Set precompensation motor position."""
        self._write(b"CONTrol:MTRMOV %2.2f" % target)
