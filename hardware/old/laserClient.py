import numpy as np
import time
import threading
import subprocess
import os
import psutil
import sys
import multiprocessing
from laserServer import LaserServer
import exdir
from hardwareUtils import TimeoutLock
import socket
import json
import traceback

import logging


class LaserClient(object):
    client = None
    timeout = 10
    online = False
    demo = False

    def __init__(self, host="localhost", port=None, timeout=10, demo=False):
        self.timeout = timeout
        self.host = host
        self.port = port
        self.demo = demo

    def connect(self):
        pid_list = []
        with open("laserServer.pid") as f:
            pid_list = f.readlines()
        print(pid_list)
        pid = None
        for pid in pid_list:
            pid, port = pid.split("\t")
            pid = int(pid)
            port = int(port)
            if psutil.pid_exists(pid):
                print("a process with pid %d exists" % pid)
                self.port = port
                break
            else:
                pid = None

        if pid is None:
            logging.error("Can't find laserServer process")
            # logging.info("Starting new laserServer process")

            # ret = QtGui.QMessageBox.question(self, 'LaserDaemon', "Run LaserDaemon before!", QtGui.QMessageBox.Ok)
            self.online = False
            return False
            # if os.name == 'posix':
            # 	subprocess.Popen(['python','laserDaemon.py',mode], shell=True)
            # else:
            # 	subprocess.Popen(['laser.bat',mode], shell=True)

        else:
            logging.info(f"laserServer process exists: pid={pid}, port={self.port}")
        success = False
        for i in range(10):
            try:
                self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                success = True
                break
            except socket.error:
                logging.error("Could not create a socket")
                time.sleep(1)
                success = False
        if not success:
            self.online = False
            return False
        self.client.settimeout(self.timeout)

        try:
            self.client.connect((self.host, self.port))
        except socket.error:
            logging.error("Could not connect to server")
            self.online = False
            return False
        self.online = True
        return True

    def send(self, command, value=None, timeout=10):
        msg = {command: value}
        try:
            self.client.sendall(json.dumps(msg).encode())
        except:

            return
        t0 = time.time()
        message = False
        while time.time() - t0 < timeout and self.online:
            try:
                message = self.client.recv(4096)
            except socket.timeout:
                # traceback.print_exc()
                continue
            if message:
                # print(message)
                break
        if message:
            message = json.loads(message)
            # print(message)
            if command in message:
                return message[command]
            else:
                return None
        else:
            return None

    def wavelength(self, value=None, timeout=10):
        r = self.send("WAVelength", value, timeout=timeout)
        return r

    def status(self, timeout=10):
        r = self.send("Status", None, timeout=timeout)
        return r

    def watchdog(self, value=None, timeout=10):
        r = self.send("TIMer:WATChdog", value, timeout=timeout)
        return r

    def lcd_brightness(self, value=None, timeout=10):
        r = self.send("LCD:BRIGhtness", value, timeout=timeout)
        return r

    def dsm_position(self, timeout=10):
        r = self.send("CONT:DSMPOS", None, timeout=timeout)
        return r

    def mtr_move(self, value=None, timeout=10):
        r = self.send("CONTrol:MTRMOV", value, timeout=timeout)
        return r

    def shutter(self, value=None, timeout=10):
        if value is None:
            pass
        elif value:
            value = "1"
        else:
            value = "0"
        r = self.send("SHUTter", value, timeout=timeout)
        return r

    def IRshutter(self, value=None, timeout=10):
        if value is None:
            pass
        elif value:
            value = "1"
        else:
            value = "0"
        r = self.send("IRSHUTter", value, timeout=timeout)
        return r

    def power(self, timeout=10):
        r = self.send("POWer", timeout=timeout)
        return r

    def hist(self, timeout=10):
        r = self.send("AHIS", timeout=timeout)
        return r

    def mode(self, value="RUN", timeout=10):
        if not value in ["RUN", "ALIGN", None]:
            return None
        r = self.send("MODE", value=value, timeout=timeout)
        return r

    def laserOn(self, timeout=200):
        r = self.send("ON_OFF", 1, timeout=timeout)
        return r

    def laserOff(self, timeout=100):
        r = self.send("ON_OFF", 0, timeout=timeout)
        return r

    def getAllInfo(self, timeout=10):
        r = self.send("ALL_INFO", timeout=timeout)
        return r

    def __del__(self):
        self.disconnect()

    def disconnect(self):
        if not self.client is None:
            self.client.close()
        self.online = False


if __name__ == "__main__":
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    c = LaserClient()
    c.connect()
