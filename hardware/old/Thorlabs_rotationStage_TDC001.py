# -*- coding: utf-8 -*-
"""
APT Motor Controller for Thorlabs
Adopted from
https://github.com/HaeffnerLab/Haeffner-Lab-LabRAD-Tools/blob/master/cdllservers/APTMotor/APTMotorServer.py
With thanks to SeanTanner@ThorLabs for providing APT.dll and APT.lib


V1.1
20141125 V1.0    First working version
20141201 V1.0a   Use short notation for moving (movRelative -> mRel)
20150417 V1.1    Implementation of simple QT GUI

Michael Leung
mcleung@stanford.edu
"""
import ctypes
from ctypes import c_long, c_buffer, c_float, pointer
import logging
import os
import time

"""
HWTYPE_BSC001		11	// 1 Ch benchtop stepper driver
HWTYPE_BSC101		12	// 1 Ch benchtop stepper driver
HWTYPE_BSC002		13	// 2 Ch benchtop stepper driver
HWTYPE_BDC101		14	// 1 Ch benchtop DC servo driver
HWTYPE_SCC001		21	// 1 Ch stepper driver card (used within BSC102,103 units)
HWTYPE_DCC001		22	// 1 Ch DC servo driver card (used within BDC102,103 units)
HWTYPE_ODC001		24	// 1 Ch DC servo driver cube
HWTYPE_OST001		25	// 1 Ch stepper driver cube
HWTYPE_MST601		26	// 2 Ch modular stepper driver module
HWTYPE_TST001		29	// 1 Ch Stepper driver T-Cube
HWTYPE_TDC001		31	// 1 Ch DC servo driver T-Cube
HWTYPE_LTSXXX		42	// LTS300/LTS150 Long Travel Integrated Driver/Stages
HWTYPE_L490MZ		43	// L490MZ Integrated Driver/Labjack
HWTYPE_BBD10X		44	// 1/2/3 Ch benchtop brushless DC servo driver
"""


class APTMotor:
    aptdll = None

    def __init__(self, SerialNum=None, HWTYPE=31):

        self.online = False
        # logging.debug(f"APTMotor: APT initialized")
        self.HWType = c_long(HWTYPE)
        self.blCorr = 0.10  # 100um backlash correction
        self.SerialNum = SerialNum

    def connect(self):
        try:
            self.aptdll = ctypes.windll.LoadLibrary("APT.dll")
            self.aptdll.EnableEventDlg(True)

            self.aptdll.APTInit()

            if self.SerialNum is not None:
                logging.info(f"APTmotor: Serial is {self.SerialNum}")
                self.SerialNum = c_long(self.SerialNum)
                self.initializeHardwareDevice()
            else:
                logging.error("APTMotor: No serial, please setSerialNumber")
            self.online = True
        except Exception as ex:
            logging.error("APTMotor", exc_info=ex)
            self.online = True
        return self.online

    def disconnect(self):
        if not self.aptdll is None:
            self.aptdll.APTCleanUp()
        self.online = False
        return True

    def getNumberOfHardwareUnits(self):
        """
        Returns the number of HW units connected that are available to be interfaced
        """
        numUnits = c_long()
        self.aptdll.GetNumHWUnitsEx(self.HWType, pointer(numUnits))
        return numUnits.value

    def getSerialNumberByIdx(self, index):
        """
        Returns the Serial Number of the specified index
        """
        HWSerialNum = c_long()
        hardwareIndex = c_long(index)
        self.aptdll.GetHWSerialNumEx(self.HWType, hardwareIndex, pointer(HWSerialNum))
        return HWSerialNum

    def setSerialNumber(self, SerialNum):
        """
        Sets the Serial Number of the specified index
        """
        logging.info(f"APTMotor: Serial is {SerialNum}")
        self.SerialNum = c_long(SerialNum)
        return self.SerialNum.value

    def initializeHardwareDevice(self):
        """
        Initialises the motor.
        You can only get the position of the motor and move the motor after it has been initialised.
        Once initiallised, it will not respond to other objects trying to control it, until released.
        """

        logging.info(f"APTMotor: initializeHardwareDevice serial {self.SerialNum}")
        result = self.aptdll.InitHWDevice(self.SerialNum)
        if result == 0:
            self.online = True
            logging.info("APTMotor: initializeHardwareDevice connection SUCESS")
        # need some kind of error reporting here
        else:
            raise Exception("Connection Failed. Check Serial Number!")
        return True

    ### Interfacing with the motor settings
    def getHardwareInformation(self):
        model = c_buffer(255)
        softwareVersion = c_buffer(255)
        hardwareNotes = c_buffer(255)
        self.aptdll.GetHWInfo(
            self.SerialNum, model, 255, softwareVersion, 255, hardwareNotes, 255
        )
        hwinfo = [model.value, softwareVersion.value, hardwareNotes.value]
        return hwinfo

    def getStageAxisInformation(self):
        minimumPosition = c_float()
        maximumPosition = c_float()
        units = c_long()
        pitch = c_float()
        self.aptdll.MOT_GetStageAxisInfo(
            self.SerialNum,
            pointer(minimumPosition),
            pointer(maximumPosition),
            pointer(units),
            pointer(pitch),
        )
        stageAxisInformation = [
            minimumPosition.value,
            maximumPosition.value,
            units.value,
            pitch.value,
        ]
        return stageAxisInformation

    def setStageAxisInformation(self, stageAxisInformation):
        minimumPosition = c_float(stageAxisInformation[0])
        maximumPosition = c_float(stageAxisInformation[1])
        units = c_long(stageAxisInformation[2])  # units of mm
        # Get different pitches of lead screw for moving stages for different stages.
        pitch = c_float(stageAxisInformation[3])
        self.aptdll.MOT_SetStageAxisInfo(
            self.SerialNum, minimumPosition, maximumPosition, units, pitch
        )
        return True

    def getHardwareLimitSwitches(self):
        reverseLimitSwitch = c_long()
        forwardLimitSwitch = c_long()
        self.aptdll.MOT_GetHWLimSwitches(
            self.SerialNum, pointer(reverseLimitSwitch), pointer(forwardLimitSwitch)
        )
        hardwareLimitSwitches = [reverseLimitSwitch.value, forwardLimitSwitch.value]
        return hardwareLimitSwitches

    def getVelocityParameters(self):
        minimumVelocity = c_float()
        acceleration = c_float()
        maximumVelocity = c_float()
        self.aptdll.MOT_GetVelParams(
            self.SerialNum,
            pointer(minimumVelocity),
            pointer(acceleration),
            pointer(maximumVelocity),
        )
        velocityParameters = [
            minimumVelocity.value,
            acceleration.value,
            maximumVelocity.value,
        ]
        return velocityParameters

    def getVel(self):
        logging.debug("APTMotor: getVel probing...")
        minVel, acc, maxVel = self.getVelocityParameters()
        logging.debug("APTMotor: getVel maxVel")
        return maxVel

    def setVelocityParameters(self, minVel, acc, maxVel):
        minimumVelocity = c_float(minVel)
        acceleration = c_float(acc)
        maximumVelocity = c_float(maxVel)
        self.aptdll.MOT_SetVelParams(
            self.SerialNum, minimumVelocity, acceleration, maximumVelocity
        )
        return True

    def setVel(self, maxVel):
        logging.debug(f"APTMotor: setVel {maxVel}")
        minVel, acc, oldVel = self.getVelocityParameters()
        self.setVelocityParameters(minVel, acc, maxVel)
        return True

    def getVelocityParameterLimits(self):
        maximumAcceleration = c_float()
        maximumVelocity = c_float()
        self.aptdll.MOT_GetVelParamLimits(
            self.SerialNum, pointer(maximumAcceleration), pointer(maximumVelocity)
        )
        velocityParameterLimits = [maximumAcceleration.value, maximumVelocity.value]
        return velocityParameterLimits

        """
		Controlling the motors
		m = move
		c = controlled velocity
		b = backlash correction

		Rel = relative distance from current position.
		Abs = absolute position
		"""

    def getPos(self):
        """
        Obtain the current absolute position of the stage
        """
        logging.debug(f"APTMotor: getPos probing...")
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")

        position = c_float()
        self.aptdll.MOT_GetPosition(self.SerialNum, pointer(position))
        logging.debug(f"APTMotor: getPos  {position.value}")
        return position.value

    def mRel(self, relDistance):
        """
        Moves the motor a relative distance specified
        relDistance    float     Relative position desired
        """
        logging.debug(f"APTMotor: mRel  {relDistance, c_float(relDistance)}")
        if not self.online:
            logging.debug(
                f"APTMotor: Please connect first! Use initializeHardwareDevice"
            )
            # raise Exception('Please connect first! Use initializeHardwareDevice')
        relativeDistance = c_float(relDistance)
        self.aptdll.MOT_MoveRelativeEx(self.SerialNum, relativeDistance, True)
        logging.debug(f"APTMotor: mRel SUCESS")
        return True

    def mAbs(self, absPosition):
        """
        Moves the motor to the Absolute position specified
        absPosition    float     Position desired
        """
        logging.debug(f"APTMotor: mAbs  {absPosition, c_float(absPosition)}")
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        absolutePosition = c_float(absPosition)
        self.aptdll.MOT_MoveAbsoluteEx(self.SerialNum, absolutePosition, True)
        logging.debug(f"APTMotor: mAbs SUCESS")
        return True

    def mcRel(self, relDistance, moveVel=0.5):
        """
        Moves the motor a relative distance specified at a controlled velocity
        relDistance    float     Relative position desired
        moveVel        float     Motor velocity, mm/sec
        """
        logging.debug(
            f"APTMotor: mcRel ', relDistance, c_float(relDistance), 'mVel {moveVel}"
        )
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        # Save velocities to reset after move
        maxVel = self.getVelocityParameterLimits()[1]
        # Set new desired max velocity
        self.setVel(moveVel)
        self.mRel(relDistance)
        self.setVel(maxVel)
        logging.debug(f"APTMotor: mcRel SUCESS")
        return True

    def mcAbs(self, absPosition, moveVel=0.5):
        """
        Moves the motor to the Absolute position specified at a controlled velocity
        absPosition    float     Position desired
        moveVel        float     Motor velocity, mm/sec
        """
        logging.debug(
            f"APTMotor: mcAbs ', absPosition, c_float(absPosition), 'mVel {moveVel}"
        )
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        # Save velocities to reset after move
        minVel, acc, maxVel = self.getVelocityParameters()
        # Set new desired max velocity
        self.setVel(moveVel)
        self.mAbs(absPosition)
        self.setVel(maxVel)
        logging.debug(f"APTMotor: mcAbs SUCESS")
        return True

    def mbRel(self, relDistance):
        """
        Moves the motor a relative distance specified
        relDistance    float     Relative position desired
        """
        logging.debug(f"APTMotor: mbRel  {relDistance, c_float(relDistance)}")
        if not self.online:
            logging.debug(
                f"APTMotor: Please connect first! Use initializeHardwareDevice"
            )
            # raise Exception('Please connect first! Use initializeHardwareDevice')
        self.mRel(relDistance - self.blCorr)
        self.mRel(self.blCorr)
        logging.debug(f"APTMotor: mbRel SUCESS")
        return True

    def mbAbs(self, absPosition):
        """
        Moves the motor to the Absolute position specified
        absPosition    float     Position desired
        """
        logging.debug(f"APTMotor: mbAbs  {absPosition, c_float(absPosition)}")
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        if absPosition < self.getPos():
            logging.debug(f"APTMotor: backlash mAbs {absPosition - self.blCorr}")
            self.mAbs(absPosition - self.blCorr)
        self.mAbs(absPosition)
        logging.debug(f"APTMotor: mbAbs SUCESS")
        return True

    def go_home(self):
        """
        Move the stage to home position and reset position entry
        """
        logging.debug(f"APTMotor: Going home")
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        logging.debug(f"APTMotor: go_home SUCESS")
        self.aptdll.MOT_MoveHome(self.SerialNum)
        return True

        """ Miscelaneous """

    def identify(self):
        """
        Causes the motor to blink the Active LED
        """
        self.aptdll.MOT_Identify(self.SerialNum)
        return True

    def cleanUpAPT(self):
        """
        Releases the APT object
        Use when exiting the program
        """
        self.aptdll.APTCleanUp()
        logging.debug(f"APTMotor: APT cleaned up")
        self.online = False


class TDC001(object):
    demo = False
    online = False
    driver = None

    def __new__(cls, SerialNum=None, HWTYPE=31, demo=False):
        if demo:
            return APTMotor_sim()
        else:
            return APTMotor(SerialNum=SerialNum, HWTYPE=HWTYPE)


class APTMotor_sim:
    online = False
    pos = 0
    vel = 10

    def __init__(self):
        print("sim")
        pass

    def connect(self):
        self.online = True
        return True

    def disconnect(self):
        self.online = False
        return True

    def getPos(self):
        return self.pos

    def mAbs(self, pos):
        t = abs(self.pos - pos) / self.vel
        time.sleep(t)
        self.pos = pos
        return True

    def mRel(self, step):
        t = abs(step) / self.vel
        time.sleep(t)
        self.pos += step
        return True

    def mcRel(self, step, vel):
        t = abs(step) / vel
        time.sleep(t)
        self.pos += step
        return True

    def go_home(self):
        t = abs(self.pos) / self.vel
        time.sleep(t)
        self.pos = 0
        return True

    def cleanUpAPT(self):
        self.online = False


if __name__ == "__main__":
    # -*- coding: utf-8 -*-
    import sys

    demo = False
    if "sim" in sys.argv:
        demo = True
    Motor1 = TDC001(
        83854487, HWTYPE=31, demo=demo
    )  # The number should correspond to the serial number.
    # Use help APTMotor to obtain full list of hardware (HW) supported.

    # Note: You can control multiple motors by creating more APTMotor Objects

    # Obtain current position of motor
    print(Motor1.getPos())
    Motor1.go_home()
    print(Motor1.getPos())
    # You can control multiple motors by creating more APTMotor Objects
    # Serial numbers can be added later by using setSerialNumber and initializeHardwareDevice
    # This functionality is particularly useful in the GUI setup.

    # Move motor forward by 1mm, wait half a second, and return to original position.
    # mRel is move relative. mAbs is move absolute (go to position xxx)
    Motor1.mRel(10)  # advance 1mm
    time.sleep(0.5)
    print(Motor1.getPos())
    Motor1.mRel(-1)  # retract 1mm
    print(Motor1.getPos())
    time.sleep(1)

    # Move motor forward by 1mm, wait half a second, and return to original position, at a velocity of 0.5mm/sec
    motVel = 0.5  # motor velocity, in mm/sec
    Motor1.mcRel(1, motVel)  # advance 1mm
    print(Motor1.getPos())
    time.sleep(0.5)
    Motor1.mcRel(-1, motVel)  # retract 1mm
    print(Motor1.getPos())

    # Clean up APT object, free up memory
    Motor1.cleanUpAPT()
