#
# Copyright (C) 2018 Pico Technology Ltd. See LICENSE file for terms.
#
# PS3000A RAPID BLOCK MODE EXAMPLE
# This example opens a 3000a driver device, sets up one channel and a trigger then collects 10 block of data in rapid succession.
# This data is then plotted as mV against time in ns.

import ctypes
from picosdk.ps3000a import ps3000a as ps
import numpy as np
import matplotlib.pyplot as plt
from picosdk.functions import adc2mV, assert_pico_ok, mV2adc
import time


class Pico3k(object):
    # Create chandle and status ready for use
    status = {}
    chandle = ctypes.c_int16()
    channels = {"A": {"enabled": False}, "B": {"enabled": False}}

    def __init__(self):
        pass

    def connect(self):
        # Opens the device/s
        self.status["openunit"] = ps.ps3000aOpenUnit(ctypes.byref(self.chandle), None)

        try:
            assert_pico_ok(self.status["openunit"])
        except:

            # powerstate becomes the status number of openunit
            powerstate = self.status["openunit"]

            # If powerstate is the same as 282 then it will run this if statement
            if powerstate == 282:
                # Changes the power input to "PICO_POWER_SUPPLY_NOT_CONNECTED"
                self.status["ChangePowerSource"] = ps.ps3000aChangePowerSource(
                    self.chandle, 282
                )
                # If the powerstate is the same as 286 then it will run this if statement
            elif powerstate == 286:
                # Changes the power input to "PICO_USB3_0_DEVICE_NON_USB3_0_PORT"
                self.status["ChangePowerSource"] = ps.ps3000aChangePowerSource(
                    self.chandle, 286
                )
            else:
                raise

            assert_pico_ok(status["ChangePowerSource"])
        return self.status["openunit"]

    def setChannel(
        self, chan="A", coupling="DC", VRange="1V", VOffset=0.0, enabled=True
    ):
        # Set up channel A
        # handle = chandle
        # channel = PS3000A_CHANNEL_A = 0
        # enabled = 1
        # coupling type = PS3000A_DC = 1
        # range = PS3000A_1V = 8
        # analogue offset = 0 V
        self.channels[chan]["enabled"] = enabled
        VRange = VRange.replace("mV", "MV")
        self.channels[chan]["range"] = ps.PS3000A_RANGE["PS3000A_" + VRange]

        self.status["setCh" + chan] = ps.ps3000aSetChannel(
            self.chandle,
            ps.PS3000A_CHANNEL["PS3000A_CHANNEL_" + chan],
            enabled,
            ps.PS3000A_COUPLING["PS3000A_" + coupling],
            ps.PS3000A_RANGE["PS3000A_" + VRange],
            VOffset,
        )
        assert_pico_ok(self.status["setCh" + chan])
        return self.status["setCh" + chan]

    def setTrigger(
        self,
        source="A",
        enable=1,
        preTriggerSamples=40000,
        postTriggerSamples=40000,
        autoTrigger_ms=1000,
        delay=0,
        direction=3,
        threshold=1024,
        timebase=1,
        nSegments=10,
    ):
        # Sets up single trigger
        # Handle = Chandle
        # Enable = 1
        # Source = ps3000A_channel_A = 0
        # Threshold = 1024 ADC counts
        # Direction = ps3000A_Falling = 3
        # Delay = 0
        # autoTrigger_ms = 1000
        self.status["trigger"] = ps.ps3000aSetSimpleTrigger(
            self.chandle,
            enable,
            ps.PS3000A_CHANNEL["PS3000A_CHANNEL_" + source],
            threshold,
            direction,
            delay,
            autoTrigger_ms,
        )
        assert_pico_ok(self.status["trigger"])

        # Setting the number of sample to be collected
        self.maxsamples = preTriggerSamples + postTriggerSamples

        # Gets timebase innfomation
        # Handle = chandle
        # Timebase = 2 = timebase
        # Nosample = maxsamples
        # TimeIntervalNanoseconds = ctypes.byref(timeIntervalns)
        # MaxSamples = ctypes.byref(returnedMaxSamples)
        # Segement index = 0
        # timebase = 1
        timeIntervalns = ctypes.c_float()
        returnedMaxSamples = ctypes.c_int16()
        self.status["GetTimebase"] = ps.ps3000aGetTimebase2(
            self.chandle,
            timebase,
            self.maxsamples,
            ctypes.byref(timeIntervalns),
            1,
            ctypes.byref(returnedMaxSamples),
            0,
        )
        assert_pico_ok(self.status["GetTimebase"])
        self.timeIntervalns = timeIntervalns
        self.timebase = timebase
        self.postTriggerSamples = postTriggerSamples
        self.preTriggerSamples = preTriggerSamples
        self.returnedMaxSamples = returnedMaxSamples
        # Creates a overlow location for data
        overflow = ctypes.c_int16()
        # Creates converted types maxsamples
        cmaxSamples = ctypes.c_int32(self.maxsamples)

        # Handle = Chandle
        # nSegments = 10
        # nMaxSamples = ctypes.byref(cmaxSamples)
        self.nSegments = nSegments
        self.status["MemorySegments"] = ps.ps3000aMemorySegments(
            self.chandle, nSegments, ctypes.byref(cmaxSamples)
        )
        assert_pico_ok(self.status["MemorySegments"])

        # sets number of captures
        self.status["SetNoOfCaptures"] = ps.ps3000aSetNoOfCaptures(
            self.chandle, nSegments
        )
        assert_pico_ok(self.status["SetNoOfCaptures"])
        self.dataBuffer = {}

        for i in range(nSegments):
            # Create buffers ready for assigning pointers for data collection
            # bufferAMax = (ctypes.c_int16 * maxsamples)()
            # bufferAMin = (ctypes.c_int16 * maxsamples)() # used for downsampling which isn't in the scope of this example
            if self.channels["A"]["enabled"]:
                self.dataBuffer["AMax%d" % i] = np.empty(
                    self.maxsamples, dtype=np.dtype("int16")
                )
                self.dataBuffer["AMin%d" % i] = np.empty(
                    self.maxsamples, dtype=np.dtype("int16")
                )  # used for downsampling which isn't in the scope of this example

                # Setting the data buffer location for data collection from channel A
                # Handle = Chandle
                # source = ps3000A_channel_A = 0
                # Buffer max = ctypes.byref(bufferAMax)
                # Buffer min = ctypes.byref(bufferAMin)
                # Buffer length = maxsamples
                # Segment index = 0
                # Ratio mode = ps3000A_Ratio_Mode_None = 0
                self.status["SetDataBuffers"] = ps.ps3000aSetDataBuffers(
                    self.chandle,
                    0,
                    self.dataBuffer["AMax%d" % i].ctypes.data,
                    self.dataBuffer["AMin%d" % i].ctypes.data,
                    self.maxsamples,
                    i,
                    0,
                )
                assert_pico_ok(self.status["SetDataBuffers"])
            if self.channels["B"]["enabled"]:
                self.dataBuffer["BMax%d" % i] = np.empty(
                    self.maxsamples, dtype=np.dtype("int16")
                )
                self.dataBuffer["BMin%d" % i] = np.empty(
                    self.maxsamples, dtype=np.dtype("int16")
                )  # used for downsampling which isn't in the scope of this example

                # Setting the data buffer location for data collection from channel A
                # Handle = Chandle
                # source = ps3000A_channel_A = 0
                # Buffer max = ctypes.byref(bufferAMax)
                # Buffer min = ctypes.byref(bufferAMin)
                # Buffer length = maxsamples
                # Segment index = 0
                # Ratio mode = ps3000A_Ratio_Mode_None = 0
                self.status["SetDataBuffers"] = ps.ps3000aSetDataBuffers(
                    self.chandle,
                    1,
                    self.dataBuffer["BMax%d" % i].ctypes.data,
                    self.dataBuffer["BMin%d" % i].ctypes.data,
                    self.maxsamples,
                    i,
                    0,
                )
                assert_pico_ok(self.status["SetDataBuffers"])

        # Creates a overlow location for data
        self.overflow = (ctypes.c_int16 * self.nSegments)()

    def runBlock(self):
        # import pdb; pdb.set_trace()
        # Starts the block capture
        # Handle = chandle
        # Number of prTriggerSamples
        # Number of postTriggerSamples
        # Timebase = 2 = 4ns (see Programmer's guide for more information on timebases)
        # time indisposed ms = None (This is not needed within the example)
        # Segment index = 0
        # LpRead = None
        # pParameter = None
        t0 = time.time()

        # Creates converted types maxsamples
        cmaxSamples = ctypes.c_int32(self.maxsamples)

        # Checks data collection to finish the capture
        ready = ctypes.c_int16(0)
        check = ctypes.c_int16(0)

        self.status["runblock"] = ps.ps3000aRunBlock(
            self.chandle,
            self.preTriggerSamples,
            self.postTriggerSamples,
            self.timebase,
            1,
            None,
            0,
            None,
            None,
        )
        assert_pico_ok(self.status["runblock"])

        while ready.value == check.value:
            self.status["isReady"] = ps.ps3000aIsReady(
                self.chandle, ctypes.byref(ready)
            )
        t1 = time.time()
        print("dt", t1 - t0)
        # Handle = chandle
        # noOfSamples = ctypes.byref(cmaxSamples)
        # fromSegmentIndex = 0
        # ToSegmentIndex = 9
        # DownSampleRatio = 0
        # DownSampleRatioMode = 0
        # Overflow = ctypes.byref(overflow)

        self.status["GetValuesBulk"] = ps.ps3000aGetValuesBulk(
            self.chandle,
            ctypes.byref(cmaxSamples),
            0,
            self.nSegments - 1,
            1,
            0,
            ctypes.byref(self.overflow),
        )
        assert_pico_ok(self.status["GetValuesBulk"])

        # Handle = chandle
        # Times = Times = (ctypes.c_int16*10)() = ctypes.byref(Times)
        # Timeunits = TimeUnits = ctypes.c_char() = ctypes.byref(TimeUnits)
        # Fromsegmentindex = 0
        # Tosegementindex = 9
        Times = (ctypes.c_int16 * self.nSegments)()
        TimeUnits = ctypes.c_char()
        self.status[
            "GetValuesTriggerTimeOffsetBulk"
        ] = ps.ps3000aGetValuesTriggerTimeOffsetBulk64(
            self.chandle,
            ctypes.byref(Times),
            ctypes.byref(TimeUnits),
            0,
            self.nSegments - 1,
        )
        assert_pico_ok(self.status["GetValuesTriggerTimeOffsetBulk"])

        # Finds the max ADC count
        # Handle = chandle
        # Value = ctype.byref(maxADC)
        maxADC = ctypes.c_int16()
        self.status["maximumValue"] = ps.ps3000aMaximumValue(
            self.chandle, ctypes.byref(maxADC)
        )
        assert_pico_ok(self.status["maximumValue"])

        # Converts ADC from channel A to mV
        t = np.linspace(
            0, (cmaxSamples.value) * self.timeIntervalns.value, cmaxSamples.value
        )
        res = [t]
        print("looop...", t)
        # import pdb; pdb.set_trace()
        if self.channels["A"]["enabled"]:
            dataA = np.array(
                [self.dataBuffer["AMax%d" % i] for i in range(self.nSegments)]
            )
            dataA = np.array(adc2mV(dataA, self.channels["A"]["range"], maxADC))
            res.append(dataA)
        if self.channels["B"]["enabled"]:
            dataB = np.array(
                [self.dataBuffer["BMax%d" % i] for i in range(self.nSegments)]
            )
            dataB = np.array(adc2mV(dataB, self.channels["B"]["range"], maxADC))
            res.append(dataB)
        print("done")
        # Creates the time data
        # res = np.array([resA,resB])
        return res

    def stop(self):
        # Stops the scope
        # Handle = chandle
        self.status["stop"] = ps.ps3000aStop(self.chandle)
        assert_pico_ok(self.status["stop"])

        # Closes the unit
        # Handle = chandle
        self.status["close"] = ps.ps3000aCloseUnit(self.chandle)
        assert_pico_ok(self.status["close"])

        # Displays the staus returns
        return self.status

    def __del__(self):
        self.status["close"] = ps.ps3000aCloseUnit(self.chandle)
        assert_pico_ok(self.status["close"])


if __name__ == "__main__":
    p = Pico3k()
    r = p.connect()
    print(r)
    r = p.setChannel(chan="A", coupling="DC", VRange="1V", VOffset=0.0, enabled=True)
    print(r)
    r = p.setChannel(chan="B", coupling="DC", VRange="1V", VOffset=0.0, enabled=True)
    print(r)
    p.setTrigger(
        source="A",
        enable=1,
        preTriggerSamples=40000,
        postTriggerSamples=40000,
        autoTrigger_ms=1000,
        delay=0,
        direction=3,
        threshold=1024,
        timebase=1,
        nSegments=11,
    )
    t0 = time.time()

    res = p.runBlock()
    t, dataA, dataB = res
    t1 = time.time()
    print("dt1", t1 - t0)
    # r = p.stop()
    print(dataA.shape, dataA.max(axis=1) - dataA.min(axis=1), dataA.dtype)
    time.sleep(10)
    print("done")
