import matplotlib.pyplot as plt
import numpy as np
import time

from picoscope import ps3000a

SERIAL_NUM = b"CW694/010\x00"
ps = ps3000a.PS3000a(SERIAL_NUM)

# now = time.strftime("%Y%m%d_%H%M%S")
# filename = "sweep_" + now + ".swp"
# output_file = open(filename, "wb")

c = 3e8

# rapid block mode

ps.setChannel(channel="A", coupling="DC", VRange=1)
ps.setChannel(channel="B", coupling="DC", VRange=1)

n_captures = 35000  # int(600 * 1.4)
sample_interval = 2e-9
sample_duration = sample_interval * 7 * 10  # 1/80e6

(sampleInterval, noSamples, maxSamples) = ps.setSamplingInterval(
    sample_interval, sample_duration
)
print(ps.timebase)
print(sampleInterval)
# ps.setSimpleTrigger("A", threshold_V=0.1,direction='Rising')
maxSegments = ps.getMaxMemorySegments()
samples_per_segment = ps.memorySegments(n_captures)
print(
    sampleInterval,
    sample_duration,
    samples_per_segment,
    maxSegments,
    sample_duration * n_captures,
)
ps.setNoOfCaptures(n_captures)

# data = np.zeros((n_captures, samples_per_segment), dtype=np.int16)
import exdir

s = exdir.File("/tmp/test.exdir")

# if not 'dataA' in s:
# 	s.create_dataset('dataA',shape=n_captures*ps.noSamples, dtype=np.int16)
# if not 'dataB' in s:
# 	s.create_dataset('dataB',shape=n_captures*ps.noSamples, dtype=np.int16)

# if not 'dataA' in s:
# 	s.create_dataset('dataA',shape=(n_captures, ps.noSamples), dtype=np.int16)
# if not 'dataB' in s:
# 	s.create_dataset('dataB',shape=(n_captures, ps.noSamples), dtype=np.int16)


# dataA = s['dataA']
# dataB = s['dataB']
dataA = np.zeros((n_captures, noSamples), dtype=np.int16)
dataB = np.zeros((n_captures, noSamples), dtype=np.int16)

print(dataA)
for i in range(1):
    t1 = time.time()
    ps.runBlock()

    ps.waitReady()

    t2 = time.time()
    print("Time to get sweep: " + str(t2 - t1))
    ps.getDataRawBulk(channel="A", data=dataA)
    ps.getDataRawBulk(channel="B", data=dataB)
    # for i in range(n_captures):
    # 	ps.getDataRaw(channel='A',data=dataA[noSamples*i:noSamples*(i+1)],segmentIndex=i)
    # for i in range(n_captures):
    # 	ps.getDataRaw(channel='B',data=dataB[noSamples*i:noSamples*(i+1)],segmentIndex=i)
    t3 = time.time()
    print("Time to read data: " + str(t3 - t2))
s.attrs["noSamples"] = ps.noSamples
print(dataA)
dataA_ = ps.rawToV("A", dataA)
dataB_ = ps.rawToV("B", dataB)

print(sample_duration * n_captures)
# output_file.write(data)
# t4 = time.time()
# print "Time to write data to disk: ", str(t4 - t3)
# output_file.close()
ps.close()
# plt.imshow(A[:, 0:ps.noSamples], aspect='auto', interpolation='none',
#           cmap=plt.cm.hot)
plt.plot(np.hstack(dataA_[:10000]))
# plt.plot(dataA[:])
# plt.colorbar()
plt.show()

# ps.close()
