# -*- coding: utf-8 -*-
import os
import sys
import time
import traceback
import numpy as np
from collections import OrderedDict
import re

from pipython import GCSDevice, GCSError, gcserror
from pipython.fastaligntools import ResultID
from pipython.datarectools import RecordOptions, TriggerSources
from pipython.pitools import itemstostr, savegcsarray, waitonwalk

import logging


import inspect

funcName = lambda: inspect.stack()[1][3]


class E727:

    serial_number = "0117047326"
    pi_device = GCSDevice("E-727")

    axes_transform_dict = {
        0: "1",
        1: "3",
        2: "2",
    }
    axes = ["1", "2", "3"]  # real axes names

    def __init__(self):
        pass

    def set_axes_transform(self, axes_dict):
        """Convert input axes names to real axes names understandable for 3D stage
        ( in order to redefine axes due to the stage mounting orientation)
        """
        self.axes_transform_dict = axes_dict

    def transform_axes(self, axes):
        if axes is None:
            axes = list(self.axes_transform_dict.keys())
        if type(axes) == int:
            axes = [axes]
        axes = [self.axes_transform_dict[ax] for ax in axes]
        return axes

    def state(self):
        """Return last error state from buffer and reset buffer to "no error"."""
        state = self._state
        self._state = GCSError(gcserror.PI_CNTR_NO_ERROR_0)
        return state

    def connect(self):
        # /////////////////////////////////////////
        # // connect to the controller over USB. //
        # /////////////////////////////////////////
        res = False
        try:
            self.pi_device.ConnectUSB(self.serial_number)
            res = True
        except GCSError as exc:
            logging.error(
                f"Can't connect to device with SN={self.serial_number}", exc_info=exc
            )
            logging.info("Trying to find SN")
            ids = pi_device.EnumerateUSB()

            for id_ in ids:
                if "E-727" in id_:
                    pattern = re.search(".*(\d{10}).*", id_)
                    if pattern:
                        self.serial_number = pattern.groups()[0]
                        try:
                            self.pi_device.ConnectUSB(self.serial_number)
                            res = True
                        except GCSError as exc:
                            logging.error(
                                f"Can't connect to device with SN={self.serial_number}",
                                exc_info=exc,
                            )
                            return False
                        break
        return res

    def close(self):
        self.pi_device.close()

    def __del__(self):
        self.close()

    def handle_exceptions(f):
        def wrapper(*args, **kw):
            try:
                return f(*args, **kw)
            except GCSError as exc:
                logging.error(f"Error in {funcName()}", exc_info=exc)
                return None

        return wrapper

    @handle_exceptions
    def getAxesNames(self):
        # /////////////////////////////////////////
        # // Get the name of the connected axis. //
        # /////////////////////////////////////////
        self.axes = self.pi_device.qSAI()
        return self.axes

    @handle_exceptions
    def setServoControl(self, axes=None, values=(True, True, True)):
        """Set servo-control "on" or "off" (closed-loop/open-loop mode)."""
        axes = self.transform_axes(axes)
        self.pi_device.SVO(axes, values=values)
        return True

    @handle_exceptions
    def getServoControl(self, axes=None):
        """Set servo-control "on" or "off" (closed-loop/open-loop mode)."""
        axes = self.transform_axes(axes)
        res = self.pi_device.qSVO(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def autoZero(self, axes=None, wait=False, timeout=10):
        """Automatic zero-point calibration"""
        axes_ = axes
        axes = self.transform_axes(axes)

        self.pi_device.ATZ()
        res = True
        if wait:
            t0 = time.time()
            while time.time() - t0 < timeout:
                res = self.getAutoZero(axes_)
                if not np.all(res):
                    logging.info("AutoZero: Done.")
                    return True
            res = False
        return res

    @handle_exceptions
    def getAutoZero(self, axes=None):
        """Reports if AutoZero procedure was successful"""
        axes = self.transform_axes(axes)
        res = self.pi_device.qATZ(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def move(self, axes, values=None, wait=False, timeout=10):
        """Move szAxes  to  specified  absolute  positions."""
        axes_ = axes
        if values is None:
            return False
        axes = self.transform_axes(axes)
        self.pi_device.MOV(axes, values=values)
        res = True
        if wait:
            t0 = time.time()
            while time.time() - t0 < timeout:
                res = self.IsMoving(axes_)
                if not np.all(res):
                    # logging.info('AutoZero: Done.')
                    return True
            res = False
        return res

    @handle_exceptions
    def IsMoving(self, axes=None):
        """Check if axes are moving"""
        axes = self.transform_axes(axes)
        res = self.pi_device.IsMoving(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def getPosition(self, axes=None):
        """Get Real Position"""
        axes = self.transform_axes(axes)
        res = self.pi_device.qPOS(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def getOnTarget(self, axes=None):
        """Get On Target State"""
        axes = self.transform_axes(axes)
        res = self.pi_device.qONT(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def setVelocity(self, axes=None, values=(1000, 1000, 1000)):
        """Set Closed-Loop Velocity"""
        axes = self.transform_axes(axes)
        res = self.pi_device.VEL(axes, values=values)
        return True

    @handle_exceptions
    def getVelocity(self, axes=None):
        """Set Closed-Loop Velocity"""
        axes = self.transform_axes(axes)
        res = self.pi_device.qVEL(axes)
        return np.array(list(res.values()))


from scipy import signal


def gen_grid_2D(rangeX=(0, 10), rangeY=(0, 10), axis=0, N=100):

    index = np.arange(N)
    t = np.linspace(0, 1, N)

    if axis == 0:
        x = np.linspace(rangeX[0], rangeX[1], N)
        x = np.insert(x, index, x)
        y = np.zeros(len(x))
        y[1::4] = 1
        y[2::4] = 1
        y = y * (rangeY[1] - rangeY[0]) + rangeY[0]
    else:
        y = np.linspace(rangeY[0], rangeY[1], N)
        y = np.insert(y, index, y)
        x = np.zeros(len(y))
        x[1::4] = 1
        x[2::4] = 1
        x = x * (rangeX[1] - rangeX[0]) + rangeX[0]
    x = np.append(x, x[-1])
    y = np.append(y, y[-1])

    return x, y


if __name__ == "__main__":
    p = E727()
    p.connect()

    p.setVelocity([0, 1, 2], (5000, 5000, 5000))
    x, y = gen_grid_2D(rangeX=(0, 50), rangeY=(0, 50), axis=0, N=100)
    z = np.zeros(len(x))
    pos = np.array([x, y, z]).T

    iWaveGeneratorIDs = [1, 2, 3]
    #% id of wave generator
    iWaveTableIDs = [1, 2, 3]
    #% id of wave table
    iStartMode = (
        1  #% start mode = 1 (start wave generator output synchronized by servo cycle)
    )
    iNumCycles = 1
    #% number of wave generator cycles
    ifrequencyOfWave = len(pos)
    #% frequency of wave

    iInterpolationType = 1
    #% interpolation between points, used if piTableRate > 1. 1 = linear interpolation
    iOffsetOfFirstPointInWaveTable = 0
    #% index of starting point of curve in segment
    iAddAppendWave_reset = "X"
    #% 0=clear wave table (1=add wavetable values, 2=append to existing wave table contents)
    iAddAppendWave_append = "&"
    #% 0=clear wave table (1=add wavetable values, 2=append to existing wave table contents)

    axes = ["1", "2", "3"]
    iTableRate = 2
    iOptions = [RecordOptions.ACTUAL_POSITION_2] * len(axes)
    iTriggerSources = [TriggerSources.DEFAULT_0] * len(axes)
    sTriggerOptions = ["0", "0", "0"]

    print("switch on servo mode")
    p.pi_device.SVO(axes, [1] * len(axes))

    p.pi_device.WGO(axes, [0] * len(axes))

    PIdevice = p.pi_device

    #% query servo cycle time
    PARAM_ServoUpdateTime = 234881536
    servoCycleTime = PIdevice.qSPA(axes[0], PARAM_ServoUpdateTime)[axes[0]][
        PARAM_ServoUpdateTime
    ]

    #% calculate number of point in wavetable from given frequency
    iNumberOfPoints = int(1.7 / (servoCycleTime * ifrequencyOfWave))

    #% wavetable contains one segment
    iSegmentLength = iNumberOfPoints

    # pos = np.array([
    # 	[0,  10,1],
    # 	[0,   0,5],
    # 	[1,   0,3],
    # 	[1,   5,3],
    # 	[1.1, 5,3],
    # 	[1.1, 0,3],
    # 	[2,   0,3],
    # 	[2,  10,3],
    # 	[2.1,10,3],
    # 	[2.1, 0,3],
    # 	[0,   0,3],
    # 	])

    for i in range(len(pos) - 1):
        if i == 0:
            append = iAddAppendWave_reset
        else:
            append = iAddAppendWave_append
        for j in range(3):
            #% send wave table to controller
            PIdevice.WAV_LIN(
                iWaveTableIDs[j],
                iOffsetOfFirstPointInWaveTable,
                iNumberOfPoints,
                append,
                int(iNumberOfPoints / 5),
                pos[i + 1, j] - pos[i, j],
                pos[i, j],
                iNumberOfPoints,
            )

    #% link wave table to wave generator
    PIdevice.WSL(iWaveGeneratorIDs, iWaveTableIDs)

    #% set wave table rate
    PIdevice.WTR(0, iTableRate, iInterpolationType)
    #% for this command the WaveGenerator ID of the E727 Controller has to be 0

    #% set wave generator output cycles
    PIdevice.WGC(iWaveGeneratorIDs, [iNumCycles] * len(axes))

    print("Configuring data recorder...")
    PIdevice.RTR(iTableRate)

    #% configure which data is recorded by the data recorder
    PIdevice.DRC(iWaveTableIDs, axes, iOptions)

    #% configure which event triggers the data recorder (starts the data acquisition):
    PIdevice.DRT(iWaveTableIDs, iTriggerSources, sTriggerOptions)

    #%% Perform move

    print("Move to start position")
    #% move to start position of sine-waveform
    #% !! Adjust start-position when changing waveform or assignment !!
    PIdevice.MOV(axes, pos[0].tolist())
    #% wait for motion to stop
    while np.all(list(PIdevice.IsMoving().values())):
        time.sleep(0.1)

    #% restart recording as soon as wave generator output starts running
    PIdevice.WGR()

    #% start wave generator output, data recorder starts simultaneously
    print("Start wavegenerator")
    t0 = time.time()
    PIdevice.WGO(iWaveGeneratorIDs, [iStartMode] * len(axes))

    #% wait for Wave Generator to finish
    ret = True
    while ret == True:
        ret = PIdevice.IsGeneratorRunning(iWaveGeneratorIDs)
        # print(list(ret.values()),p.getPosition())
        ret = np.all(list(ret.values()))

        time.sleep(0.1)

    print("done")

    p.pi_device.WGO(axes, [0] * len(axes))
    print("dt", time.time() - t0)

    #%% Read data from controller

    numberOfPoints = PIdevice.qDRL(iWaveTableIDs)[iWaveTableIDs[0]]

    #% % Set a predefined number of points you want to retrieve from controller.
    #% % Wait until controller has recorded this number of points. WARNING: By
    #% % setting numberOfPoints to a greater value than the controllers data
    #% % recorder length (see controller manual) you will create an infinite loop.
    #% numberOfPoints = 1000;
    #% while (PIdevice.qDRL(1) < numberOfPoints)
    #%	 pause(0.02);
    #%
    #% end
    # numberOfPoints=3001

    startPoint = 1
    print("Retrieving data from controller...")
    header = PIdevice.qDRR(iWaveTableIDs, startPoint, numberOfPoints)

    print("Retrieving data finished.")

    while PIdevice.bufstate is not True:
        time.sleep(0.1)
    p.pi_device.SVO(axes, [0] * len(axes))

    print(header)
    d = np.array(PIdevice.bufdata)
    print(d.shape)
    from matplotlib import pyplot as plt

    plt.figure(1)
    plt.plot(d[0], ".", label="0")
    plt.plot(d[1], ".", label="1")
    plt.plot(d[2], ".", label="2")
    plt.plot(x, "-", label="-0")
    plt.plot(y, "-", label="-1")
    plt.plot(z, "-", label="-2")

    plt.figure(2)
    plt.plot(d[0], d[1], ".", label="0")
    plt.plot(x, y, "-r", label="t")
    plt.legend()
    plt.show()
