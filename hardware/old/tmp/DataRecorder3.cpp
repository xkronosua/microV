// DataRecorder.cpp : Defines the entry point for the console application.
//


#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include "PI_GCS2_DLL.h"

int main()
{ 
	int ID;
	char szErrorMesage[1024];
	char szUsbController[1024];
	int	iError;
	BOOL bOK = TRUE;




	/////////////////////////////////////////
	// connect to the controller over USB. //
	/////////////////////////////////////////
	PI_EnumerateUSB(szUsbController, 1024, "PI E-727");
	ID = PI_ConnectUSB(szUsbController);
	if (ID<0)
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("ConnectUSB: ERROR %d: %s\n", iError, szErrorMesage);
		return(1);
	}
   


	/////////////////////////////////////////
	// Get the name of the connected axis. //
	/////////////////////////////////////////
	char szAxes[17];
	if (!PI_qSAI(ID, szAxes, 16))
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("SAI?: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	printf(">SAI?:\n%s\n", szAxes);

	// Use only the first axis.
	//strcpy(szAxes, "1");



	/////////////////////////////////////////
	// close the servo loop (closed-loop). //
	/////////////////////////////////////////
	BOOL bFlags[3];

	// Switch on the Servo for all axes
	bFlags[0] = TRUE; // servo on for the axis in the string 'axes'.
	bFlags[1] = TRUE;
	bFlags[2] = TRUE;
	// call the SerVO mode command.
	if(!PI_SVO(ID, szAxes, bFlags))
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("SVO: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	printf(">SVO 1 1\n\n");

	BOOL bFlags1[3];
	double pdLowVoltageArray[3];
	if (!PI_ATZ(ID, szAxes,pdLowVoltageArray, bFlags))
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("ATZ: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	
	int piAtz[3];
	for(int i=0;i<100;i++){
		bool ok = PI_qATZ(ID, szAxes,piAtz);
		if (piAtz[0] && piAtz[1] && piAtz[2]){
			printf(">ATZ:%d %d %d\n", piAtz[0],piAtz[1],piAtz[2]);
			break;
		}
		printf("%d\n", i);
		Sleep(100);
	}
	

	// // call the SerVO mode command.
	// if(!PI_CMO(ID, szAxes, bFlags))
	// {
	// 	iError = PI_GetError(ID);
	// 	PI_TranslateError(iError, szErrorMesage, 1024);
	// 	printf("CMO: ERROR %d: %s\n", iError, szErrorMesage);
	// 	PI_CloseConnection(ID);
	// 	return(1);
	// }
	// printf(">CMO 1 1\n\n");

	// select the desired wave generator.
	int iWaveGenerator[] = {1,2,3};
	// select the start mode for the corresponding wave generator.
	int iStatMode[3];

	iStatMode[0] = 0;
	iStatMode[1] = 0;
	iStatMode[2] = 0;

	if((bOK = PI_WGO(ID, iWaveGenerator, iStatMode, 3)) == FALSE)
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("WGO: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}

	if((bOK = PI_WGR(ID)) == FALSE)
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("WGR: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	printf(">WGR\n\n");


	double vel[] = {10,10,10};
	
	if(!PI_VEL(ID, szAxes, vel))
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("VEL: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}

    ///////////////////////////////////////////
    // Write a sin wave to the wave table 1. //
    ///////////////////////////////////////////
	int iWaveTableIds[3];
	iWaveTableIds[0] = 1;
	iWaveTableIds[1] = 2;
	iWaveTableIds[2] = 3;
	int N = 0;
	if((bOK = PI_WAV_SIN_P(ID, iWaveTableIds[0], 1, 100, 0, 50, 10, 0, 100)) == FALSE)
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("WAV_SIN_P: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	printf(">WAV 1 X SIN_P 1000 10.0 0.0 1000 1 500\n\n");
	// double X_positions[] = {0, 40, 30.5, 45.2, 5, 0};
	// int iAppendWave = 0;
	// for(int i=0; i<6; i++){
	// 	if(i!=0){iAppendWave=2;}
	// 	bOK = PI_WAV_LIN (ID, iWaveTableIds[0],
	// 		0,//int iOffsetOfFirstPointInWaveTable,
	// 		10,//int iNumberOfWavePoints,
	// 		iAppendWave,//int iAppendWave,
	// 		0, //int iNumberOfSpeedUpDownPointsOfWave,
	// 		X_positions[i+1]-X_positions[i],//double dAmplitudeOfWave,
	// 		X_positions[i],//double dOffsetOfWave,
	// 		10);//int iSegmentLength);
	// 		N+=10;
	// }

	
	// double Y_positions[] = {1, 41, 31.5, 46.2, 6, 1};
	// iAppendWave = 0;
	// for(int i=0; i<6; i++){
	// 	if(i!=0){iAppendWave=2;}
	// 	bOK = PI_WAV_LIN (ID, iWaveTableIds[1],
	// 		0,//int iOffsetOfFirstPointInWaveTable,
	// 		10,//int iNumberOfWavePoints,
	// 		iAppendWave,//int iAppendWave,
	// 		0, //int iNumberOfSpeedUpDownPointsOfWave,
	// 		Y_positions[i+1]-Y_positions[i],//double dAmplitudeOfWave,
	// 		Y_positions[i],//double dOffsetOfWave,
	// 		10);//int iSegmentLength);
	
	// }

	

	// double Z_positions[] = {2, 42, 32.5, 47.2, 7, 2};
	// iAppendWave = 0;
	// for(int i=0; i<6; i++){
	// 	if(i!=0){iAppendWave=2;}
	// 	bOK = PI_WAV_LIN (ID, iWaveTableIds[2],
	// 		0,//int iOffsetOfFirstPointInWaveTable,
	// 		10,//int iNumberOfWavePoints,
	// 		iAppendWave,//int iAppendWave,
	// 		0, //int iNumberOfSpeedUpDownPointsOfWave,
	// 		Z_positions[i+1]-Z_positions[i],//double dAmplitudeOfWave,
	// 		Z_positions[i],//double dOffsetOfWave,
	// 		10);//int iSegmentLength);

	// }
	
	
	if(bOK == FALSE)
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("WAV_SIN_P: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	
	printf(">WAV 1 X SIN_P 1000 10.0 0.0 1000 1 500\n\n");



    ////////////////////////////////////////
    // define the data recorder channels. //
    ////////////////////////////////////////
	
	// select the desired record channels to change.
	int iDataRecorderChannelIds[3];
	iDataRecorderChannelIds[0] = 1;
	iDataRecorderChannelIds[1] = 2;
	iDataRecorderChannelIds[2] = 3;

	


	// select the corresponding record mode.
	int iDataRecorderOptions[] = {2,2,2};

	// Call the data recorder configuration command
	if((bOK = PI_DRC(ID, iDataRecorderChannelIds, szAxes, iDataRecorderOptions)) == FALSE)
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("DRC: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	printf(">DRC 1 1 2\n\n");


	
	if (PI_qDRC(ID, iWaveTableIds,szAxes, iDataRecorderOptions, 16,3) == 0)
    {
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("qDRC: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
    }

	printf("%d, %d, %d\n", iDataRecorderOptions[0], iDataRecorderOptions[1], iDataRecorderOptions[2]);


    ////////////////////////////
    // Select the wave table. //
    ////////////////////////////





    // Call the wave selection command
    if (PI_WSL(ID, iWaveGenerator, iWaveTableIds, 3) == 0)
    {
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("WSL: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
    }
	printf(">WSL 1 1\n\n");



	int piNumberOfCyclesArray[] = {1,1,1};
	// Call the wave selection command
    if (PI_WGC(ID, iWaveGenerator, piNumberOfCyclesArray, 3) == 0)
    {
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("WSL: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
    }
	printf(">WSL 1 1\n\n");

    /////////////////////////////////
    // start the wave generator 1. //
    /////////////////////////////////

	PI_GcsCommandset(ID, "WGO 1 1");

	iStatMode[0]=1;
	iStatMode[1]=1;
	iStatMode[2]=1;
    

	// if((bOK = PI_WGO(ID, iWaveGenerator, iStatMode, 3)) == FALSE)
	// {
	// 	iError = PI_GetError(ID);
	// 	PI_TranslateError(iError, szErrorMesage, 1024);
	// 	printf("WGO: ERROR %d: %s\n", iError, szErrorMesage);
	// 	PI_CloseConnection(ID);
	// 	return(1);
	// }
	// printf(">WGO 1 1\n\n");
	
	
	BOOL pbValueArray[3];
	bool tmp = false;
	double pos[3];
	int i=0;
	double pdValueArray[150];
	do// wait until the read pointer does not increase any more
	{

		

		PI_IsMoving(ID, szAxes, pbValueArray);
		tmp = (pbValueArray[0] || pbValueArray[1] || pbValueArray[2]);
		
		//PI_qPOS(ID, szAxes, pos);
		//printf("%d [%d %d %d] %.4f %.4f %.4f\n",tmp, pbValueArray[0],pbValueArray[1],pbValueArray[2], pos[0],pos[1],pos[2]);
		PI_qDRR_SYNC(ID,1,i,1,pdValueArray);
		
		printf("%d [%d %d %d] %.4f\n",tmp, pbValueArray[0],pbValueArray[1],pbValueArray[2], pdValueArray[0]);
		
		Sleep(0.1);
		i++;
		tmp=i<10000;
	}while(tmp);
	
    ///////////////////////////////////
    // start reading asynchronously. //
    ///////////////////////////////////

    // select the desired record channels to change.
	iDataRecorderChannelIds[0] = 1;
	iDataRecorderChannelIds[1] = 2;
	iDataRecorderChannelIds[2] = 3;


	double* dDataTable;
	char szHeader[301];
	int iNReadChannels = 3;
	int iNReadValues = 151;
	if((bOK = PI_qDRR(ID, iDataRecorderChannelIds, iNReadChannels, 1, iNReadValues, &dDataTable, szHeader, 301)) == FALSE)
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("DRR?: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	printf(">DRR? 100 1 1 2 3:\n%s\n", szHeader);



    /////////////////////////////////////////////////////////////
    // wait until the read pointer does not increase any more. //
    /////////////////////////////////////////////////////////////

	int iIndex = -1;
	int iOldIndex;

	do// wait until the read pointer does not increase any more
	{

		iOldIndex = iIndex;
		Sleep(100);
		iIndex = PI_GetAsyncBufferIndex(ID);
	}while(iOldIndex < iIndex);



    /////////////////////////////////////
    // read the values from the array. //
    /////////////////////////////////////

	int k;
	for(iIndex = 0; iIndex < (iOldIndex / iNReadChannels); iIndex++)
	{// print read data
	// the data columns 
	// c1_1 c2_1 c3_1 c4_1
	// c1_2 c2_2 c3_2 c4_2
	// ...
	// c1_n c2_n c3_n c4_n
	// are aligned as follows:
	// dDataTable:
	// {c1_1,c2_1,c3_1,c4_1,c1_2,c2_2,...,c4_n}
		//if (iIndex>100) break;
		printf("%03d", iIndex);

		for(k = 0; k < iNReadChannels; k++)
		  printf("\t%05.05f", dDataTable[(iIndex * iNReadChannels) + k]);

		printf("\n");  
	}
	

    /////////////////////////////////
    // stop the wave generator 1. //
    /////////////////////////////////



    // select the start mode for the corresponding wave generator.
	iStatMode[0] = 0;
	iStatMode[1] = 0;
	iStatMode[2] = 0;

	if((bOK = PI_WGO(ID, iWaveGenerator, iStatMode, 3)) == FALSE)
	{
		iError = PI_GetError(ID);
		PI_TranslateError(iError, szErrorMesage, 1024);
		printf("WGO: ERROR %d: %s\n", iError, szErrorMesage);
		PI_CloseConnection(ID);
		return(1);
	}
	printf(">WGO 1 0\n\n");


	PI_CloseConnection(ID);

	if(bOK)
		printf("No Errors");
	
	getch();

	return 0;
}

