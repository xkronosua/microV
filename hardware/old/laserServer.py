from serialManager import ResourceManager
import numpy as np
import time
import threading
from serial.tools import list_ports
import os
import psutil
from collections import OrderedDict
import numpy.lib.recfunctions as rfn

# from filelock import FileLock
import exdir
from hardwareUtils import TimeoutLock
import socket
import json

# from multiprocessing import shared_memory
import sys

import logging

# logger = logging.getLogger(__name__)
# logging.setLevel(logging.DEBUG)


def int_None(val):
    if val is None or val == "" or val == " ":
        return None
    else:
        try:
            val = int(val)
        except:
            val = None
        return val


def float_None(val):
    if val is None or val == "" or val == " ":
        return None
    else:
        try:
            val = float(val)
        except:
            val = None
        return val


DEFAULT_INFO = {
    "TIMer:WATChdog": 10,  # "f4"),
    "LCD:BRIGhtness": 255,  # "i8"),
    "CONT:DSMPOS": 0,  # "i8"),
    "CONTrol:MTRMOV": 0,  # "i8"),
    "SHUTter": 0,  # "i2"),
    "IRSHUTter": 0,  # "i2"),
    "WAVelength": 1000,  # "i8"),
    "POWer": 0,  # "f4"),
    "MODE": "RUN",  # "S5"),
    "ON_OFF": False,
    "Status": {
        "RawSTB": 0,  # "i8"),
        "Emission": 0,  # "i2"),
        "Pulsing": 0,  # "i2"),
        "Main shutter": 0,  # "i2"),
        "IR shutter": 0,  # "i2"),
        "Servo on": 0,  # "i2"),
        "User interlock": 0,  # "i2"),
        "Keyswitch": 0,  # "i2"),
        "Power supply": 0,  # "i2"),
        "Internal": 0,  # "i2"),
        "Warning": 0,  # "i2"),
        "fault": 0,  # "i2"),
        "State": "READY",  # "S20"),
        "ON_OFF": 0,  # "i2"),
        "time": 0,  #'float64')
    },
}

DEFAULT_INFO_Status_hide = [
    "User interlock",
    "Power supply",
    "Keyswitch",
    "Internal",
    "Servo on",
    "Pulsing",
]


class LaserServer(object):

    status = {}
    lock = TimeoutLock.lock()
    state_lock = TimeoutLock.lock()
    monitor_run = False
    LaserInfo = {}
    demo = False

    def __init__(self, host="localhost", port=None, demo=False):
        if demo:
            self.Laser = ResourceManager_sim()
        else:
            self.Laser = ResourceManager()

        logging.info(f"Demo Mode:{demo}")

        pid_list = []
        if os.path.exists("laserServer.pid"):
            with open("laserServer.pid") as f:
                pid_list = f.readlines()
            open("laserServer.pid", "w").close()
            print(pid_list)
            for pid in pid_list:
                pid = int(pid.split("\t")[0])
                if psutil.pid_exists(pid):
                    print("a process with pid %d exists" % pid)
                    p = psutil.Process(pid)
                    p.terminate()  # or p.kill()
                    print("a process with pid %d killed" % pid)

                else:
                    pass  # print("a process with pid %d does not exist" % pid)

        self.LaserInfo = DEFAULT_INFO

        self.setInfo("TIMer:WATChdog", 10)
        self.setInfo("State", None)
        self.pid = os.getpid()

        self.host = host
        if port is None:
            self.port = 0
        # socket.setdefaulttimeout(10)

        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((self.host, self.port))
        self.server.listen(5)
        name = self.server.getsockname()
        self.port = name[1]

        with open("laserServer.pid", "a") as f:
            f.write(f"{self.pid}\t{name[1]}\n")
        logging.info(f"Starting laser server with pid:{self.pid}")

    def __del__(self):
        self.Laser.close()
        self.alive = False

    def listen_for_clients(self):
        print("Listening...")
        while self.alive and self.Laser.online:
            try:
                client, addr = self.server.accept()
            except:
                print("Server accept failed")

            print("Accepted Connection from: " + str(addr[0]) + ":" + str(addr[1]))
            t = threading.Thread(target=self.handle_client, args=(client, addr))
            t.daemon = True
            t.start()
            time.sleep(0.1)
        self.alive = False
        logging.info(f"listen_for_clients thread done.")

    def setInfo(self, key="", value=None):
        with TimeoutLock(
            "LaserInfo", self.state_lock, timeout=10, raise_on_timeout=True
        ):
            self.LaserInfo[key] = value

    def getInfo(self, key=""):
        if key in self.LaserInfo:
            with TimeoutLock(
                "LaserInfo", self.state_lock, timeout=10, raise_on_timeout=True
            ):
                return self.LaserInfo[key]
        else:
            return None

    def process_commands(self, commandsDict):
        res = {}
        print(commandsDict)
        for key in commandsDict:
            # if not commandsDict[key]:
            # 	commandsDict[key] = None
            if key == "ON_OFF":
                if commandsDict[key]:
                    r = self.laserOn()
                else:
                    r = self.laserOff()
                res["ON_OFF"] = self.getInfo("ON_OFF")
            elif key == "ALL_INFO":
                res["ALL_INFO"] = self.LaserInfo
            elif key == "MODE":
                self.MODE(commandsDict[key])
                self.MODE()
                res["MODE"] = self.getInfo("MODE")
            elif key == "WAVelength":
                self.WAVelength(commandsDict[key])
                self.WAVelength()
                res["WAVelength"] = self.getInfo("WAVelength")
            elif key == "IRSHUTter":
                self.IRSHUTter(commandsDict[key])
                self.IRSHUTter()
                res["IRSHUTter"] = self.getInfo("IRSHUTter")
            elif key == "SHUTter":
                self.SHUTter(commandsDict[key])
                self.SHUTter()
                res["SHUTter"] = self.getInfo("SHUTter")
            elif key == "CONTrol:MTRMOV":
                self.CONTrol_MTRMOV(commandsDict[key])
                self.CONTrol_MTRMOV()
                res["CONTrol:MTRMOV"] = self.getInfo("CONTrol:MTRMOV")
            elif key == "CONT:DSMPOS":
                self.CONT_DSMPOS()
                res["CONT:DSMPOS"] = self.getInfo("CONT:DSMPOS")
            elif key == "LCD:BRIGhtness":
                self.LCD_BRIGhtness(commandsDict[key])
                self.LCD_BRIGhtness()
                res["LCD:BRIGhtness"] = self.getInfo("LCD:BRIGhtness")
            elif key == "TIMer:WATChdog":
                self.TIMer_WATChdog(commandsDict[key])
                self.TIMer_WATChdog()
                res["TIMer:WATChdog"] = self.getInfo("TIMer:WATChdog")
            elif key == "Status":
                self.checkState()
                res["Status"] = self.getInfo("Status")
            elif key == "POWer":
                self.READ_POWer()
                res["POWer"] = self.getInfo("POWer")
            elif key == "AHIS":
                self.READ_AHIS()
                res["AHIS"] = self.getInfo("AHIS")
        return res

    def handle_client(self, client_socket, address):
        size = 1024
        while self.alive and self.Laser.online:
            try:
                data = client_socket.recv(size)
                if not data:
                    time.sleep(0.1)
                    continue
                if "q^" in data.decode():
                    print(
                        "Received request for exit from: "
                        + str(address[0])
                        + ":"
                        + str(address[1])
                    )
                    break

                else:
                    try:
                        commandsDict = json.loads(data)

                    except:
                        client_socket.sendall(b"")
                        continue
                    if type(commandsDict) == dict:
                        res = self.process_commands(commandsDict)
                        client_socket.sendall(json.dumps(res).encode())
                    else:
                        break

            except socket.error:
                time.sleep(0.1)
                # print('error')
                # self.alive = False
                # client_socket.close()
                # return False
            # time.sleep(0.1)

        client_socket.close()
        logging.info(f"client_socket thread done.")

    def connect(self, port=None, baudrate=115200):
        if port is None:
            ports = list(list_ports.grep("10c4:ea60"))
            if len(ports) == 0:
                logging.error("Cant find real laser!!! working with simulation")
                port = "./ttyclient"
                baudrate = 9600
            else:
                port = ports[0].device
                baudrate = 115200

        logging.info("Connecting...")
        self.Laser.open_resource(port, baud_rate=baudrate, readout_mode=1)
        with TimeoutLock("*IDN?", self.lock, timeout=10, raise_on_timeout=True):
            idn = self.Laser.query("*IDN?")
            if not idn is None:
                idn = idn.decode()
            logging.info(idn)

        self.alive = True
        self.listener_thread = threading.Thread(target=self.listen_for_clients)
        self.listener_thread.daemon = True
        self.listener_thread.start()
        self.monitor_sleep = 5
        self.monitor_run = True
        self.monitor = threading.Thread(target=self.state_monitor)
        self.monitor.daemon = True
        self.monitor.start()
        logging.info(f"Starting listener thread")

    def disconnect(self):
        self.Laser.close()

    def laserOn(self):
        self.monitor_run = True
        self.checkState()

        if self.getInfo("Status")["State"] != "READY":
            logging.error("laserOn: not ready!")
            return None
        self.setInfo("ON_OFF", None)
        with TimeoutLock("ON", self.lock, timeout=10, raise_on_timeout=True):
            self.Laser.query("ON", timeout=3)
            logging.info("Laser: ON...")
        t0 = time.time()
        warmedup = "0"
        self.TIMer_WATChdog(self.getInfo("TIMer:WATChdog"))
        self.TIMer_WATChdog()
        while warmedup < "100" and time.time() - t0 < 130:
            time.sleep(1)
            with TimeoutLock(
                "READ:PCTWarmedup?", self.lock, timeout=10, raise_on_timeout=True
            ):
                warmedup = self.Laser.query("READ:PCTWarmedup?")
                if not warmedup is None:
                    warmedup = warmedup.decode()
            logging.info(f"PCTWarmedup:{warmedup}")
        self.checkState()
        t0 = time.time()
        while (
            not self.getInfo("Status")["State"] in ["RUN", "ALIGN"]
            and time.time() - t0 < 400
        ):
            time.sleep(1)
            r = self.checkState()
            print(r)
        if not self.getInfo("Status")["State"] in ["RUN", "ALIGN"]:
            logging.error("laserOn: FAIL!")
            self.setInfo("ON_OFF", 0)
            return None
        else:

            self.monitor_sleep = 1
            self.LCD_BRIGhtness()
            self.CONT_DSMPOS()

            self.setInfo("ON_OFF", 1)
            return True

    def laserOff(self):

        with TimeoutLock("OFF", self.lock, timeout=10, raise_on_timeout=True):
            self.setInfo("ON_OFF", None)
            self.Laser.query("OFF", timeout=10)
            self.monitor_run = False
            self.setInfo("ON_OFF", 0)
        logging.info("Laser: OFF...")

    def _standard_req_resp(self, msg, value=None, timeout=10, raise_on_timeout=True):
        """Standart function to write/read messages to/from the laser.
        msg - command for laser
        value  - if None - read, else - write (should be str ready to write)
        timeout - max wait time
        """
        # if type(msg)==str: msg = msg.encode()
        # if type(value)==str: value = value.encode()
        if value is None or value == "None":
            msg_ = msg + "?"
        else:
            msg_ = msg + " " + value
            timeout = 0

        with TimeoutLock(
            msg_, self.lock, timeout=timeout + 0.5, raise_on_timeout=raise_on_timeout
        ):
            logging.debug(msg_)
            # print('>',msg_)
            res = self.Laser.query(msg_, timeout=timeout)
            if not res is None:
                res = res.rstrip(b"\r").rstrip(b"\n").decode()
            logging.info(f"{msg}: {res}")

            return res

    def TIMer_WATChdog(self, value=None):
        """Sets the number of seconds for the software watchdog timer."""
        msg = "TIMer:WATChdog"
        self.setInfo(msg, None)
        res = self._standard_req_resp(msg, value=str(value))
        res = float_None(res)
        self.setInfo(msg, res)
        logging.info(f"")
        return res

    def LCD_BRIGhtness(self, value=None):
        """Sets the power supply LCD screen back illumination brightness level."""
        msg = "LCD:BRIGhtness"
        self.setInfo(msg, None)
        res = self._standard_req_resp(msg, value=str(value))
        res = int_None(res)
        self.setInfo(msg, res)
        return res

    def CONT_DSMPOS(self):
        """This query reads and returns the actual DeepSee motor position."""
        msg = "CONT:DSMPOS"
        self.setInfo(msg, None)
        res = self._standard_req_resp(msg, value=None)
        res = float_None(res)
        self.setInfo(msg, res)
        return res

    def CONTrol_MTRMOV(self, value=None):
        """This query reads and returns motor position."""
        msg = "CONTrol:MTRMOV"
        self.setInfo(msg, None)
        res = self._standard_req_resp(msg, value=str(value))
        res = float_None(res)
        self.setInfo(msg, res)
        return res

    def SHUTter(self, value=None):
        """Opens or closes the main shutter."""
        msg = "SHUTter"
        if not value is None:
            if type(value) == str:
                pass
            elif value:
                value = "1"
            else:
                value = "0"

            res = self._standard_req_resp(msg, value=value)
            res = res == "1"
        self.checkState()
        try:
            res = self.getInfo("Status")["Main shutter"]
        except:
            res = None
        self.setInfo(msg, res)
        return res

    def IRSHUTter(self, value=None):
        """Opens or closes the IR shutter."""
        msg = "IRSHUTter"
        if not value is None:
            if type(value) == str:
                pass
            elif value:
                value = "1"
            else:
                value = "0"

            res = self._standard_req_resp(msg, value=value)
            res = res == "1"
        self.checkState()
        try:
            res = self.getInfo("Status")["IR shutter"]
        except:
            res = None
        self.setInfo(msg, res)
        return res

    def WAVelength(self, value=None):
        """Sets/read the wavelength to between 680 and 1300 nm."""
        msg = "WAVelength"
        self.setInfo(msg, None)
        if value is None:
            msg_ = "READ:" + msg
        else:
            msg_ = msg
        res = self._standard_req_resp(msg_, value=str(value))
        res = int_None(res)
        self.setInfo(msg, res)
        return res

    def READ_POWer(self):
        """Returns the laser output power (in Watts)."""
        msg = "POWer"

        msg_ = "READ:" + msg
        res = self._standard_req_resp(msg_, value=None, raise_on_timeout=False)

        res = float_None(res)
        self.setInfo(msg, res)
        return res

    # TODO:
    def READ_AHIS(self):
        """The READ:AHIS? command reports the most recent 16 codes from the system, with the
        most recent code listed first."""
        msg = "AHIS"
        msg_ = "READ:" + msg
        res = self._standard_req_resp(msg_, value=None)
        # res = float_None(res)
        self.setInfo(msg, res)
        return res

    def MODE(self, value=None):
        """Sets the system mode: value=[RUN, ALIGN]"""
        msg = "MODE"
        self.setInfo(msg, None)
        if value is None:
            pass
        elif not value in ["RUN", "ALIGN"]:
            return
        res = self._standard_req_resp(msg, value=value)
        if not res is None:
            res = res.rstrip("\n")
        self.setInfo(msg, res)
        return res

    def state_monitor(self):
        prev_resp = ""
        t0 = time.time()
        while self.Laser.online and self.monitor_run:
            if time.time() - t0 > 5:
                self.READ_POWer()
                t0 = time.time()
            resp = self.checkState()
            if resp is None:
                logging.error("state_monitor ERROR!!")
                # break
            else:
                if resp != prev_resp:
                    prev_resp = resp
                    resp_ = {}
                    for k in resp:
                        if not k in DEFAULT_INFO_Status_hide:
                            resp_[k] = resp[k]
                    logging.info(f"Laser status:{resp_}")
                time.sleep(self.monitor_sleep)
        logging.info(f"State_monitor thread done.")

    def checkState(self):
        with TimeoutLock("STB?", self.lock, timeout=0.1, raise_on_timeout=False):
            status = self.Laser.query("STB?")
            if not status is None:
                status = status.rstrip(b"\n").replace(b"\r", b"")
        if status is None or status == b"":
            return
        else:
            resp = {}
            resp["RawSTB"] = status.decode()
            try:
                status = int(status)
            except:
                return
            # 0 0x00000001 Emission The laser diodes are energized. Laser emission is possible if the
            # 		shutter(s) is (are) open.

            resp["Emission"] = (status & 0x00000001) == 1

            # 1 0x00000002 Pulsing 1 = “pulsing”
            # 		The name PULSING is used for compatibility with Mai Tai. In the
            # 		InSight DS+ system, this bit indicates that the laser has either:
            # 		a) Reached the Run state and can be used to take data.
            # 		or
            # 		b) Reached the Align state and can be used to align the optical
            # 		system.
            resp["Pulsing"] = status & 0x00000002

            # 2 0x00000004 Main shutter 1 = The main shutter is open (sensed position).
            resp["Main shutter"] = (status & 0x00000004) != 0

            # 3 0x00000008 Dual beam
            # 		shutter 1 = The IR shutter is open (sensed position).
            # 		N OTE : On systems purchased without the optional dual beam
            # 		output, this bit is always set to 1.
            resp["IR shutter"] = (status & 0x00000008) != 0

            # 5 0x00000020 Servo on 1 = Servo is on (see “SERVO” command on page A-6).
            resp["Servo on"] = status & 0x00000020

            # 9 0x00000200 User interlock 1 = The user interlock (CDRH interlock) is open; laser is forced off.
            resp["User interlock"] = status & 0x00000200

            # 10 0x00000400 Keyswitch 1 = The safety keyswitch interlock is open; laser is forced off.
            resp["Keyswitch"] = status & 0x00000400

            # 11 0x00000800 Power supply 1 = The power supply interlock is open; laser is forced off.
            resp["Power supply"] = status & 0x00000800

            # 12 0x00001000 Internal 1 = The internal interlock is open; laser is forced off.
            resp["Internal"] = status & 0x00001000

            # 14 0x00004000 Warning 1 = The system is currently detecting a warning condition. The laser
            # 	continues to operate, but it is best to resolve the issue at your earliest
            # 	convenience.
            # 	Use READ:HISTORY? (page A-4) to see what is causing the
            # 	warning.
            resp["Warning"] = status & 0x00004000

            # 15 0x00008000 fault 1 = The system is currently detecting a fault condition. InSight
            # 	immediately turns off the laser diodes. Use READ:HISTORY? to see
            # 	what is causing the fault.
            # 	Note: The fault condition may clear itself when the laser turns itself
            # 	off. If so, the fault bit clears.
            resp["fault"] = status & 0x00008000

            # 16 to 22 0x007F0000	State
            # 	After masking, shift right 16 bits and interpret as a number with a
            # 	value from 0 to 127 (see Table A-2).
            # 	Most state numbers are not specifically assigned, but several ranges
            # 	can be described and three specific values (25, 50, and 60) are
            # 	guaranteed not to change.
            s = (status & 0x007F0000) >> 16
            if s >= 0 and s <= 24:
                resp["State"] = "Initializing"
            elif s == 25:
                resp["State"] = "READY"
            elif s >= 26 and s <= 49:
                resp["State"] = "Turning on / optimizing"
            elif s == 50:
                resp["State"] = "RUN"
            elif s >= 51 and s <= 59:
                resp["State"] = "Moving to Align mode"
            elif s == 60:
                resp["State"] = "ALIGN"
            elif s >= 61 and s <= 69:
                resp["State"] = "Exiting Align mode"
            else:
                resp["State"] = ""
            resp["time"] = time.time()
            # for key in resp:
            # 	self.statusMng(key, resp[key])
            self.setInfo("Status", resp)
            return resp


class ResourceManager_sim(ResourceManager):
    last_line = ""
    DEFAULT_INFO_tmp = {k: val for k, val in DEFAULT_INFO.items()}
    DEFAULT_INFO_tmp["Status"] = {k: val for k, val in DEFAULT_INFO["Status"].items()}

    params = DEFAULT_INFO_tmp
    status = 8379951
    resp_line = ""

    def __init__(self, parent=None):
        pass  # super(ResourceManager_sim,self).__init__()

    def open_resource(self, port, baud_rate=115200, timeout=1, readout_mode=0):
        self.online = True

    def close(self):
        self.online = False

    def query(self, msg="", ending=b"\r\n", timeout=10):
        line = msg
        self.last_line = line
        parts = line.rstrip("\n").rstrip("\r").split(" ")
        if line == "":
            return
        elif "*IDN?" in line:
            print(">*IDN?")
            time.sleep(0.1)
            self.resp_line = b"0000000000000\n"
        elif "READ:POWer?" in line:
            print("READ:POWer?")
            time.sleep(0.1)
            self.resp_line = str(np.random.rand(1)[0] * 2.5).encode() + b"\n"
        elif "READ:POWer?" in line:
            print("READ:POWer?")
            time.sleep(0.1)
            self.resp_line = str(np.random.rand(1)[0]).encode() + b"\n"
        elif "READ:PCTWarmedup?" in line:
            print("READ:POWer?")
            time.sleep(0.1)
            self.resp_line = str(100).encode() + b"\n"
        elif "STB?" in line:
            print("STB?", self.params["SHUTter"], self.params["IRSHUTter"])
            time.sleep(0.1)
            if self.params["SHUTter"] and self.params["IRSHUTter"]:
                status = 268435468
            elif self.params["SHUTter"]:
                status = 268435463
            elif self.params["IRSHUTter"]:
                status = 268435467
            else:
                status = 268435459
            self.resp_line = str(status).encode() + b"\n"
        elif "MODE" in line:
            if "?" in line:
                self.resp_line = str(self.params["MODE"]).encode() + b"\n"
            else:
                self.params["MODE"] = line.rstrip("\n").rstrip("\r").split(" ")[1]
                self.resp_line = self.params["MODE"].encode() + b"\n"
        elif len(parts) == 2:
            # try:
            # 	val = int(parts[1])
            # except:
            val = parts[1].replace(" ", "").replace("\r", "")
            # else:
            # 	val = float(parts[1])
            print("<", parts[0], val, val != 0)
            self.params[parts[0]] = parts[1]
            if parts[0] == "SHUTter":
                self.params[parts[0]] = parts[1] == "1"
                print("set SHUTter", self.params[parts[0]])
                # self.params[b'Status'][b'Main shutter'] = parts[1]!=0
            elif parts[0] == "IRSHUTter":
                self.params[parts[0]] = parts[1] == "1"
                print("set IRSHUTter", self.params[parts[0]])
                # self.params[b'Status'][b'IR shutter'] = parts[1]!=0

            time.sleep(0.1)
        else:
            print(">", parts[0], self.params)
            if parts[0] in self.params:
                self.resp_line = self.params[parts[0]].encode() + b"\n"
                print(self.params[parts[0][:-1]])
            elif len(parts[0].split(":")) > 1:
                key = parts[0].split(":")[1].replace("?", "")
                print(key)
                if key in self.params:
                    if type(self.params[key]) == str:
                        resp = self.params[key].encode()
                    else:
                        resp = str(self.params[key]).encode()
                    self.resp_line = resp + b"\n"
                # print(self.params[parts[0][:-1]])
            else:
                self.resp_line = b"\r\n"
            time.sleep(0.1)
        return self.resp_line


if __name__ == "__main__":

    print(__name__)
    if "sim" in sys.argv:
        demo = True
    else:
        demo = False
    print(mode)
    s = LaserServer(demo=demo)
    s.connect()
    if "inf" in sys.argv:
        while s.Laser.online:
            try:
                time.sleep(1)

            except KeyboardInterrupt:
                break
        s.disconnect()
