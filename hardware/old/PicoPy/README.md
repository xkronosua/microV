PicoPy
======

Pythonic interface for picoscopes 3000, 4000, and 5000 and the PT104 temperature logger.
Install:
pacaur -S picoscope libps3000a libps4000 libps5000a
or 
yaourt -S picoscope libps3000a libps4000 libps5000a

Create links:
sudo ln -s /opt/picoscope/lib/libps3000a.so /usr/lib/libPS3000a.so
sudo ln -s /opt/picoscope/lib/libps4000.so /usr/lib/libPS4000.so
sudo ln -s /opt/picoscope/lib/libps5000a.so /usr/lib/libPS5000a.so

Build:
sudo python setup.py install

