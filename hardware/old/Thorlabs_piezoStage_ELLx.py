from binascii import unhexlify, hexlify
import serial
from serial.tools import list_ports

from serialManager import ResourceManager
import struct
from hardwareUtils import TimeoutLock
import threading
from queue import Queue
import time
from collections import deque

import logging

root = logging.getLogger()
root.setLevel(logging.DEBUG)


STATUS_ERROR_CODES = {
    0: "OK, no error",
    1: "Communication time out",
    2: "Mechanical time out",
    3: "Command error or not supported",
    4: "Value out of range",
    5: "Module isolated",
    6: "Module out of isolation",
    7: "Initializing error",
    8: "Thermal error",
    9: "Busy",
    10: "Sensor Error(May appear during self test. If code persists there is an error)",
    11: "Motor Error(May appear during self test. If code persists there is an error)",
    12: "Out of Range(e.g. stage has been instructed to move beyond its travel range).",
    13: "Over Current error",
}


class ELLx(object):
    Stage = None
    stage_address = b"0"
    lock = TimeoutLock.lock()
    last_readout = {"time": time.time(), "val": b""}
    readout_log = deque()
    position = 0
    PositionIndexDict = {}
    status = 0
    status_code = 0
    info = {}
    demo = False
    stepsPerUnit = 143360 / 360  # steps per deg

    def __init__(
        self,
        address=b"0",
        min_position=0,
        max_position=93,
        demo=False,
        PositionIndexDict={0: 0, 31: 1, 62: 2, 93: 3},
        index=0,
    ):
        if demo:
            self.Stage = ResourceManager_sim()
        else:
            self.Stage = ResourceManager()
        self.demo = demo
        self.PositionIndexDict = PositionIndexDict
        self.stage_address = address
        self.Stage.addHandler(self._position_handler)
        self.Stage.addHandler(self._status_handler)
        self.Stage.addHandler(self._info_handler)
        self.min_position = min_position
        self.max_position = max_position

    def __del__(self):
        if not self.Stage is None:
            self.Stage.close()

    def initialize(self, port=None, baudrate=9600):
        return self.connect(port=port, baudrate=baudrate)

    def shutdown(self):
        self.disconnect()

    def connect(self, port=None, baudrate=9600):

        if port is None:
            ports = list(list_ports.grep("0403:6015"))
            for p in ports:
                if p.serial_number == "DO02FRYTA":
                    port = p.device
                    break
            if port is None and not self.demo:
                logging.error("No ELLx device found.")
                return
        try:
            self.Stage.open_resource(port, baud_rate=baudrate, newlineChar=b"\r\n")
            return True
        except Exception as ex:
            logging.error("ELLx open_resource error", exc_info=ex)
            return False

    def disconnect(self):
        self.Stage.close()

    def _position_handler(self, msg):

        if self.stage_address + b"PO" in msg:
            r = msg.replace(b"\r", b"").replace(b"\n", b"")[3:]
            print(f"[{msg}],[{r}]")
            self.position = struct.unpack(">l", unhexlify(r))[0] / self.stepsPerUnit

            logging.debug(f"_position_handler:{self.position}")
            return True
        else:
            return False

    def _status_handler(self, msg):
        if msg[1:3] == b"GS":
            self.stage_address = msg[:1]
            r = msg.replace(b"\r", b"")[3:]
            self.status_code = struct.unpack(">b", unhexlify(r))[0]
            if self.status_code in STATUS_ERROR_CODES:
                self.status = STATUS_ERROR_CODES[self.status_code]
            logging.debug(f"_status_handler:{self.status}, {msg}")
            return True
        else:
            return False

    def _info_handler(self, msg):
        if self.stage_address + b"IN" in msg:
            r = msg.replace(b"\r", b"")
            if len(r) == 33:
                # r = unhexlify(resp)
                out = {
                    "ELL": r[3:5],
                    "SN": r[5:13],
                    "Year": r[13:17],
                    "FW rel": r[17:19],
                    "HW rel": r[19:21],
                    "PULSES/M.U.": r[25:33],
                }
                out["TRAVEL mm/deg"] = struct.unpack(">h", unhexlify(r[21:25]))[0]
                out["PULSES/M.U."] = struct.unpack(">l", unhexlify(r[25:33]))[0]
                self.info = out
                logging.debug(f"_info_handler:{self.info}")
                return True
            else:
                return False
        else:
            return False

    def getInfo(self):
        """
        Instruct hardware unit to identify itself giving information
        about model, serial number, firmware and hardware releases, travel,
        encoder pulses per measurement unit.
        """
        self.info = None
        r = self.Stage.write(self.stage_address + b"in" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.info is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.info

    def getStatus(self):
        """
        Instruct hardware unit to get module status or error value.
        Once read the error value is cleared
        """
        self.status = None
        r = self.Stage.write(self.stage_address + b"gs" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.status is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.status

    def getPosition(self):
        """
        Instruct hardware unit to get module position
        """
        self.position = None
        r = self.Stage.write(self.stage_address + b"gp" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.position is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position

    def forward(self):
        if self.position is None:
            self.getPosition()
        if self.position is None:
            return
        if self.position >= self.max_position:
            return self.position

        self.position = None
        r = self.Stage.write(self.stage_address + b"fw" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.position is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position

    def backward(self):
        if self.position is None:
            self.getPosition()
        if self.position is None:
            return
        if self.position <= self.min_position:
            return self.position

        r = self.Stage.write(self.stage_address + b"bw" + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.position is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position

    def moveHome(self, direction=0):
        direction = hexlify(struct.pack(">b", direction))
        r = self.Stage.write(self.stage_address + b"ho" + direction + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        self.position = None
        while self.position is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position

    def moveAbs(self, position):
        position = int(position * self.stepsPerUnit)
        position = hexlify(struct.pack(">l", position)).upper()
        r = self.Stage.write(self.stage_address + b"ma" + position + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        while self.position is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position

    def moveRel(self, step):
        step = int(step * self.stepsPerUnit)
        step = hexlify(struct.pack(">l", step)).upper()
        r = self.Stage.write(self.stage_address + b"mr" + step + b"\r\n")
        if r is None:
            return
        t0 = time.time()
        self.position = None
        while self.position is None and self.Stage.online:
            time.sleep(0.1)
            if time.time() - t0 > 10:
                break
        return self.position

    def getIndex(self):
        pos = self.getPosition()

        if pos in self.PositionIndexDict:
            return self.PositionIndexDict[pos]
        else:
            return

    def setIndex(self, index):

        if index in self.PositionIndexDict.values():
            pos = list(self.PositionIndexDict.keys())[
                list(self.PositionIndexDict.values()).index(index)
            ]
            r = self.moveAbs(pos)
            return r
        else:
            return

    def SAVE_USER_DATA(self):
        """
        Instruct hardware unit to save motor parameters (such as forward or
        backward frequencies) and other reserved or user parameters
        """
        r = self.Stage.write(self.stage_address + b"us" + b"\r\n")
        if r is None:
            return

    def CHANGEADDRESS(self, new_address=b""):
        """
        Instruct hardware unit to change to the address of another device.
        The address is entered in the range 0-F. The default value is a 0.
        """
        r = self.Stage.write(self.stage_address + b"ca" + new_address + b"\r\n")
        if r is None:
            return

    def ELLx_HOSTREQ_MOTOR1INFO(self):
        "i1"

    def ELLx_DEVGET_MOTOR1INFO(self):
        "I1"

    def ELLx_HOSTSET_FWP_MOTOR1(self):
        "f1"

    def ELLx_HOSTSET_BWP_MOTOR1(self):
        "b1"

    def ELLx_HOSTREQ_SEARCHFREQ_MOTOR1(self):
        "s1"

    def ELLx_HOSTREQ_SCANCURRENTCURVE_MOTOR1(self):
        "c1"

    def ELLx_DEVGET_CURRENTCURVEMEASURE_MOTOR1(self):
        "C1"

    def ELLx_HOST_ISOLATEMINUTES(self):
        "is"

    def ELLx_HOSTREQ_MOTOR2INFO(self):
        "i2"

    def ELLx_DEVGET_MOTOR2INFO(self):
        "I2"

    def ELLx_HOSTSET_FWP_MOTOR2(self):
        "f2"

    def ELLx_HOSTSET_BWP_MOTOR2(self):
        "b2"

    def ELLx_HOSTREQ_SEARCHFREQ_MOTOR2(self):
        "s2"

    def ELLx_HOSTREQ_SCANCURRENTCURVE_MOTOR2(self):
        "c2"

    def ELLx_DEVGET_CURRENTCURVEMEASURE_MOTOR2(self):
        "C2"

    def ELLx_HOSTREQ_MOTOR3INFO(self):
        "i3"

    def ELLx_DEVGET_MOTOR3INFO(self):
        "I3"

    def ELLx_HOSTSET_FWP_MOTOR3(self):
        "f3"

    def ELLx_HOSTSET_BWP_MOTOR3(self):
        "b3"

    def ELLx_HOSTREQ_SEARCHFREQ_MOTOR3(self):
        "s3"

    def ELLx_HOSTREQ_SCANCURRENTCURVE_MOTOR3(self):
        "c3"

    def ELLx_DEVGET_CURRENTCURVEMEASURE_MOTOR3(self):
        "C3"

    def ELLx_HOSTREQ_HOME(self):
        "ho"

    def ELLx_HOSTREQ_MOVEABSOLUTE(self):
        "ma"

    def ELLx_HOSTREQ_MOVERELATIVE(self):
        "mr"

    def ELLx_HOSTREQ_HOMEOFFSET(self):
        "go"

    def ELLx_DEVGET_HOMEOFFSET(self):
        "HO"

    def ELLx_HOSTSET_HOMEOFFSET(self):
        "so"

    def ELLx_HOSTREQ_JOGSTEPSIZE(self):
        "gj"

    def ELLx_DEVGET_JOGSTEPSIZE(self):
        "GJ"

    def ELLx_HOSTSET_JOGSTEPSIZE(self):
        "sj"

    def ELLx_HOST_MOTIONSTOP(self):
        "ms"

    def ELLx_HOST_GETPOSITION(self):
        "gp"

    def ELLx_DEV_GETPOSITION(self):
        "PO"

    def ELLx_HOSTREQ_VELOCITY(self):
        "gv"

    def ELLx_DEVGET_VELOCITY(self):
        "GV"

    def ELLx_HOSTSET_VELOCITY(self):
        "sv"

    def ELLx_DEVGET_BUTTONSTATUS(self):
        "BS"

    def ELLx_DEVGET_BUTTONPOSITION(self):
        "BO"

    def ELLx_HOST_GROUPADDRESS(self):
        "ga"

    def ELLx_HOST_DRIVETIME(self):
        "t1,t2,t3"

    def ELLx_DEV_DRIVETIME(self):
        "P1,P2,P3"

    def ELLx_HOST_PADDLEMOVEABSOLUTE(self):
        "a1,a2,a3"

    def ELLx_DEV_PADDLEMOVEABSOLUTE(self):
        "P1,P2,P3"

    def ELLx_HOST_PADDLEMOVERELATIVE(self):
        "r1,r2,r3"

    def ELLx_DEV_PADDLEMOVERELATIVE(self):
        "P1,P2,P3"

    def ELLx_HOST_OPTIMIZE_MOTORS(self):
        "om"

    def ELLx_DEV_STATUS(self):
        "GS"

    def ELLx_HOST_CLEAN_MECHANICS(self):
        "cm"

    def ELLx_DEV_STATUS(self):
        "GS"

    def ELLx_HOST_STOP(self):
        "st"

    def ELLx_DEV_STATUS(self):
        "GS"

    def ELLx_HOST_ENERGIZE_MOTOR(self):
        "e1"

    def ELLx_DEV_STATUS(self):
        "GS"

    def ELLx_HOST_HALT_MOTOR(self):
        "h1"

    def ELLx_DEV_STATUS(self):
        "GS"


class ResourceManager_sim(ResourceManager):
    last_line = ""
    pos = 0
    resp_line = ""
    handlers = []
    stage_address = b"0"
    step = 31
    min_position = 0
    max_position = 93

    def __init__(self, parent=None):
        pass  # super(ResourceManager_sim,self).__init__()

    def open_resource(
        self, port, baud_rate=115200, timeout=1, readout_mode=0, newlineChar=b"\n"
    ):
        self.online = True

    def close(self):
        self.online = False

    def query(self, line):
        time.sleep(1)
        return b"000000000\n"

    def write(self, line):
        line = line.rstrip(b"\n").rstrip(b"\r")
        if line == b"":
            pass  # time.sleep(0.1)
        elif b"gs" in line:
            p = self.stage_address + b"GS00\r"
            for h in self.handlers:
                r = h(p)
                print(r, p)
        elif b"fw" in line:
            self.pos += self.step
            if self.pos > self.max_position:
                self.pos = self.max_position

            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)

        elif b"bw" in line:
            self.pos -= self.step
            if self.pos < self.min_position:
                self.pos = self.min_position
            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)

        elif b"ma" in line:
            self.pos = struct.unpack(">l", unhexlify(line[3:]))[0]

            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)

        elif b"gp" in line:
            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)
                # print(r,p)
        elif b"ho" in line:
            self.pos = 0
            p = (
                self.stage_address
                + b"PO"
                + hexlify(struct.pack(">l", self.pos))
                + b"\r"
            )
            for h in self.handlers:
                r = h(p)
        else:
            return True
        return True

    def addHandler(self, handler):
        self.handlers.append(handler)


if __name__ == "__main__":
    import sys

    demo = False
    if "sim" in sys.argv:
        demo = True
    ellx = ELLx(address=b"0", demo=demo)
    ellx.connect()
    print("Status:", ellx.getStatus())
    print("pos:", ellx.getPosition())
    print("index:", ellx.getIndex())
    print(ellx.moveHome())
    print(ellx.forward())
    print("index:", ellx.getIndex())
    print(ellx.forward())
    print("index:", ellx.getIndex())
    print(ellx.forward())
    print("index:", ellx.getIndex())
    print(ellx.backward())
    print(ellx.moveHome())
    print("done")
    ellx.disconnect()

    ellx = ELLx(address=b"1", demo=demo)
    ellx.connect()
    print("Status:", ellx.getStatus())
    print("pos:", ellx.getPosition())
    print("index:", ellx.getIndex())
    print(ellx.moveHome())
    print(ellx.forward())
    print("index:", ellx.getIndex())
    print(ellx.forward())
    print("index:", ellx.getIndex())
    print(ellx.forward())
    print("index:", ellx.getIndex())
    print(ellx.backward())
    print(ellx.moveHome())
    print("done")
    ellx.disconnect()
