import serial
import time
import logging
import traceback
import threading
import numpy as np
from collections import deque
from hardwareUtils import TimeoutLock


class ResourceManager(object):
    device = None
    listener = None
    lastRequestTime = time.time()
    readout_mode = 0
    online = False
    lock = TimeoutLock.lock()
    log = deque([(time.time(), b"")])
    handlers = []
    monitors = []
    newlineChar = "\n"
    monitorPeriod = 1

    def __init__(self):
        pass

    def ifOnline(function):
        def decorator(*args, **kwargs):
            self = args[0]
            if not self.device is None:
                if self.device.isOpen():
                    res = function(*args, **kwargs)
                    self.lastRequestTime = time.time()
                    # self.online = True
                else:
                    self.online = False
                    logging.error("Device is not connected")
                    return
            else:

                self.online = False
                logging.error("Device is offline")
                return
            return res

        return decorator

    def open_resource(
        self, port, baud_rate=115200, timeout=1, readout_mode=0, newlineChar=b"\n"
    ):
        self.newlineChar = newlineChar
        self.readout_mode = readout_mode
        try:
            self.device = serial.Serial(port, baudrate=baud_rate, timeout=timeout)
        except serial.serialutil.SerialException as ex:
            logging.error("Error:open_resource", exc_info=ex)
            self.online = False
            return
        self.online = True

        self.listener_thread = threading.Thread(target=self.listener)
        self.listener_thread.daemon = True
        logging.info(f"Starting listener thread...")
        self.listener_thread.start()

    def addHandler(self, handler):
        self.handlers.append(handler)

    def addMonitor(self, monitor):
        self.monitors.append(monitor)

    def listener(self):
        r = b""
        errors = 0
        t0 = time.time()
        lastMonitorTime = time.time()
        while self.online:
            with TimeoutLock("listener", self.lock, timeout=10):
                if time.time() - lastMonitorTime > self.monitorPeriod:
                    for monitor in self.monitors:
                        monitor()
                    lastMonitorTime = time.time()

                for i in range(10):
                    if not self.online:
                        break
                    try:
                        if self.readout_mode == 0:

                            r += self.device.read(1)
                            if r:
                                inWaiting = self.device.inWaiting()
                                r += self.device.read(inWaiting)
                                lines = r.split(self.newlineChar)
                                # print(r,'|',lines)
                                if lines[-1] != b"":
                                    # not finished readout. try again
                                    r = lines[-1]
                                else:

                                    for l in lines[:-1]:
                                        preprocessed = False
                                        for h in self.handlers:
                                            preprocessed = h(l)
                                        if not preprocessed:
                                            self.log.append((time.time(), l))
                                    r = b""
                                    break
                        else:

                            r = self.device.readline().rstrip(b"\r")
                            # print(r)
                            if r:
                                preprocessed = False
                                for h in self.handlers:
                                    preprocessed = h(r)
                                if not preprocessed:
                                    self.log.append((time.time(), r))
                                    break
                            else:
                                break
                    except (
                        AttributeError,
                        serial.serialutil.SerialException,
                        TypeError,
                    ) as ex:
                        logging.error("serialMonitor:", exc_info=ex)
                        errors += 1

                        break
                if errors > 10:
                    if time.time() - t0 < 5:
                        break
                    else:
                        t0 = time.time()

            time.sleep(0.05)
        logging.info(f"Listener thread done.")

    @ifOnline
    def query(self, msg=b"", ending=b"\r\n", timeout=10):
        if type(msg) == str:
            msg = msg.encode()
        if type(ending) == str:
            ending = ending.encode()

        if msg[-2:] != ending:
            msg = msg + ending

        try:
            t_bef = time.time()
            print(f"write: {msg}")
            self.device.write(msg)
            t0 = time.time()
            if timeout == 0:
                return
            resp = None

            while time.time() - t0 < timeout:
                # time.sleep(sleep)
                if len(self.log) == 1:
                    time.sleep(0.1)
                elif self.log[-1][0] >= t_bef:
                    resp = self.log.pop()[1]
                    break
                else:
                    time.sleep(0.1)
                    # self.device.write(msg)

            return resp

        except serial.serialutil.SerialException as e:
            logging.error("query error", exc_info=e)
            self.online = False
            return

    @ifOnline
    def write(self, msg=b""):
        if type(msg) == str:
            msg = msg.encode()
        r = self.device.write(msg)
        return True

    @ifOnline
    def read(self, n):
        try:
            resp = self.device.read(n)
            return resp
        except serial.SerialException as e:
            logging.error("readout error")
            return

    @ifOnline
    def readline(self):
        try:
            resp = self.device.readline()
            return resp
        except serial.SerialException as e:
            logging.error()
            return

    def close(self):
        if not self.device is None:
            if self.device.isOpen():
                self.device.close()
                self.online = False

    def __del__(self):
        self.close()


if __name__ == "__main__":

    dev = ResourceManager()
    dev.open_resource("./ttyclient", baudrate=9600)
