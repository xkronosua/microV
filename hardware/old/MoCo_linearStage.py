from serialManager import ResourceManager
import time
import logging
from serial.tools import list_ports
from binascii import unhexlify, hexlify
import struct
import numpy as np
import threading
import traceback


root = logging.getLogger()
root.setLevel(logging.DEBUG)


class MOCO:
    inst = None
    position = 0

    status_default = {
        "Move Status": None,
        "Moving": None,
        "Pos-err": None,
        "Motor-Off": None,
        "Limit Switch": None,
        "Limit reverse": None,
        "Limit Forward": None,
        "Error Number": None,
    }
    status = None

    position = None
    lock = threading.Lock()
    demo = False
    calibrationCoef = 5e-8  # 1 step -> m
    Stage = None
    positionChanged = None
    positionMonitorThread = None
    free = True

    def __init__(self, demo=False, positionChangedSignal=None):
        self.positionChanged = positionChangedSignal
        if demo:
            self.Stage = ResourceManager_sim()
        else:
            self.Stage = ResourceManager()
        self.demo = demo

    def connect(self, port=None, baudrate=9600, readout_mode=1):

        if port is None:
            ports = list(list_ports.grep("0403:6015"))
            for p in ports:
                if p.serial_number == "FTZ9WDMZA":
                    port = p.device
                    break
            if port is None and not self.demo:
                logging.error("No MoCo device found.")
                return False

        self.Stage.open_resource(port, baud_rate=baudrate, readout_mode=readout_mode)
        self.Stage.addHandler(self._position_handler)
        self.Stage.addHandler(self._status_handler)
        self.Stage.addMonitor(self._monitor)

        p = self.Stage.query(b"*IDN?")
        print(p)
        # self.positionMonitorThread = threading.Thread(target=self.monitor)
        # self.positionMonitorThread.start()

        return True

    def _monitor(self):
        # while self.Stage.online:
        print("_monitor")
        if not self.lock.locked():
            self.Stage.write(b"'")
            self.Stage.write(b"%")
        # time.sleep(0.3)

    def disconnect(self):
        self.Stage.close()
        self.Stage.online = False

    def __del__(self):
        self.Stage.close()
        self.Stage.online = False

    def _position_handler(self, msg):
        # print(msg)
        if b"0P0:" in msg:
            try:
                # print(msg,msg.rstrip(b'\r\n'))
                self.position = int(msg.rstrip(b"\r\n").split(b"0P0:")[1])
                if not self.positionChanged is None:
                    self.positionChanged.objSignal.emit(
                        self.position * 1000 * self.calibrationCoef
                    )
                return True
            except Exception as ex:
                logging.error("Can't read position from stream", exc_info=ex)
                return False

        else:
            return False

    def _status_handler(self, msg):
        if b"0S0:" in msg:
            try:
                r = msg.rstrip(b"\r\n").split(b"0S0:")[1].split(b" ")
                r = [int(rr, 16) for rr in r]
                """
				Move Status(LM629)
				Report Operational Flags1
				Report Operational Flags2
				Report Motion Flags
				report Limit Switch Flags
				Live Status Limit Switch
				Error Number
				"""
                status = self.status_default
                status["Move Status"] = r[0]
                status["Moving"] = "{:08b}".format(r[0])[5] != "1"
                status["Pos-err"] = "{:08b}".format(r[0])[2] == "1"
                status["Motor-Off"] = "{:08b}".format(r[0])[0] == "1"

                status["Limit Switch"] = r[5]
                status["Limit reverse"] = "{:08b}".format(r[5])[7] == "1"
                status["Limit Forward"] = "{:08b}".format(r[5])[5] == "1"
                status["Error Number"] = r[6]
                self.status = status
                return True

            except Exception as ex:
                logging.error("Can't read status from stream", exc_info=ex)
                return False
        else:
            return False

    def getStatus(self):

        self.status = None
        with self.lock:
            r = self.Stage.write(b"%")
        if r is None:
            return
        t0 = time.time()
        while self.status is None and self.Stage.online:
            time.sleep(0.1)
            print("getStatus", self.status)
            if time.time() - t0 > 10:
                break
        return self.status

    def getPosition(self):
        self.position = None
        with self.lock:
            r = self.Stage.write(b"'")
        if r is None:
            return -1
        t0 = time.time()
        while self.position is None and self.Stage.online:
            time.sleep(0.1)
            print("getPosition", self.position)
            if time.time() - t0 > 20:
                break

        return self.position

    def getPosition_mm(self):
        pos = self.getPosition()
        res = pos * 1000 * self.calibrationCoef
        return res

    def moveRel(self, incr, wait=False):
        with self.lock:
            r = self.Stage.write(b"mr" + str(int(incr)).encode() + b"\r\n")
        if r is None:
            return
        if wait:
            while self.isMoving():
                time.sleep(0.1)
            pos = self.getPosition()
        else:
            pos = self.position
        return pos

    def moveAbs(self, pos, wait=False):
        with self.lock:
            r = self.Stage.write(b"ma" + str(int(pos)).encode() + b"\r\n")
        if r is None:
            return
        print(pos)
        if wait:
            while self.isMoving():
                time.sleep(0.1)
            pos = self.getPosition()
        else:
            pos = self.position
        return pos

    def moveRel_mm(self, pos, wait=False):
        pos = int(pos / 1000 / self.calibrationCoef)
        pos = self.moveRel(pos, wait=wait)
        pos = pos * 1000 * self.calibrationCoef
        return pos

    def moveAbs_mm(self, pos, wait=False):
        pos = int(pos / 1000 / self.calibrationCoef)
        pos = self.moveAbs(pos, wait=wait)
        pos = pos * 1000 * self.calibrationCoef

        return pos

    def stop(self):
        self.Stage.write(b"ab1\r\n")
        pos = self.getPosition()
        return pos

    def isMoving(self):
        return self.getStatus()["Moving"]

    def calibr(self, wait=False):
        with self.lock:
            r = self.Stage.write(b"mc2\r\n")
        if r is None:
            return
        if wait:
            while self.isMoving() or abs(self.getPosition()) > 50:
                print(abs(self.getPosition()))
                time.sleep(1)
        pos = self.getPosition()
        return pos


class ResourceManager_sim(ResourceManager):
    last_line = ""
    pos = 0
    resp_line = ""
    handlers = []
    monitors = []
    vel = 100 / 20 / 5e-5  # steps/s

    def __init__(self, parent=None):
        pass  # super(ResourceManager_sim,self).__init__()

    def open_resource(self, port, baud_rate=115200, timeout=1, readout_mode=0):
        self.online = True
        t = threading.Thread(target=self.loop)
        t.daemon = True
        t.start()

    def loop(self):
        lastMonitorTime = time.time()
        while self.online:
            if time.time() - lastMonitorTime > 0.3:
                for monitor in self.monitors:
                    monitor()
                lastMonitorTime = time.time()
            time.sleep(0.05)

    def close(self):
        self.online = False

    def query(self, line):
        time.sleep(1)
        return b"000000000\n"

    def write(self, line):
        line = line.rstrip(b"\n").rstrip(b"\r")
        if line == b"":
            pass  # time.sleep(0.1)
        elif b"ma" in line:
            pos = int(line.split(b"ma")[1].decode())
            print("ma")

            def f(self, pos):
                print("aaaaaaaaaaaaaaaa")
                dist = pos - self.pos
                dt = abs(dist) / self.vel
                print("::", dt, self.pos, pos, self.vel, dist / 10, dt / 10)
                p = b"0S0:AA AA AA AA AA AA AA\r\n"
                for h in self.handlers:
                    r = h(p)
                for i in range(10):
                    time.sleep(dt / 10)
                    self.pos += dist / 10
                    self.pos = int(self.pos)
                    print("::", dt, self.pos, self.vel)
                    p = b"0S0:AA AA AA AA AA AA AA\r\n"
                    p1 = (
                        b"0P0:"
                        + str(int(self.pos + np.random.rand(1)[0] * 10)).encode()
                        + b"\r\n"
                    )

                    for h in self.handlers:
                        r = h(p)
                        r = h(p1)
                p = b"0S0:ff ff ff ff ff ff ff\r\n"
                p1 = (
                    b"0P0:"
                    + str(int(self.pos + np.random.rand(1)[0] * 10)).encode()
                    + b"\r\n"
                )

                for h in self.handlers:
                    r = h(p)
                    r = h(p1)

            t = threading.Thread(target=f, args=(self, pos))
            t.daemon = True
            t.start()
        elif b"mr" in line:
            self.pos += int(line.split(b"mr")[1].decode())
        elif b"mc" in line:
            self.pos = 0
        elif b"'" in line:
            p = (
                b"0P0:"
                + str(int(self.pos + np.random.rand(1)[0] * 10)).encode()
                + b"\r\n"
            )
            for h in self.handlers:
                r = h(p)
                # print(r,p)
        elif b"%" in line:
            p = b"0S0:ff ff ff ff ff ff ff\r\n"
            for h in self.handlers:
                r = h(p)
                # print(r,p)
        else:
            return True
        return True

    def addHandler(self, handler):
        self.handlers.append(handler)

    def addMonitor(self, monitor):
        self.monitors.append(monitor)


if __name__ == "__main__":
    demo = False
    import sys

    if "sim" in sys.argv:
        demo = True

    m = MOCO(demo=demo)
    m.connect()

    print("Position:", m.getPosition())
    print("Status:", m.getStatus())
    print(m.calibr(True))
    print("abs")
    # time.sleep(2)
    t0 = time.time()
    print(m.moveAbs_mm(100, True), time.time() - t0)
    print("abs")
    #
    # print(m.moveAbs(100000,True))
    # print('rel')
    # print(m.moveRel(100000,True))
    # print('calibr')
    # print(m.calibr(True))
    # print('done')
    # m.disconnect()
