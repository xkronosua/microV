import serial
import time
import logging
import traceback
import threading
import numpy as np


class ResourceManager(object):
    device = None
    listener = None
    lastRequestTime = time.time()
    offline = threading.Event()
    online = False

    def __init__(self):
        pass

    def ifOnline(function):
        def decorator(*args, **kwargs):
            self = args[0]
            if not self.device is None:
                if self.device.isOpen():
                    res = function(*args, **kwargs)
                    self.lastRequestTime = time.time()
                    self.online = True
                else:
                    self.online = False
                    logging.error("Laser is not connected")
                    return
            else:
                self.offline.set()
                self.online = False
                logging.error("Laser is offline")
                return
            return res

        return decorator

    def open_resource(self, port, baud_rate=115200, timeout=1):
        self.device = serial.Serial(port, baudrate=baud_rate, timeout=timeout)
        self.online = True

    @ifOnline
    def query(self, msg=b"", sleep=0.1, ending=b"\r\n", timeout=10):
        if type(msg) == str:
            msg = msg.encode()
        if msg[-2:] != ending:
            msg = msg + ending

        try:
            self.device.write(msg)
            t0 = time.time()
            if timeout == 0:
                return
            while time.time() - t0 < timeout:
                time.sleep(sleep)
                resp = self.device.readline()
                print("resp", resp)
                if resp:
                    break
                else:
                    logging.debug("no response. resending")
                    self.device.write(msg)

            return resp[:-1].decode()

        except serial.serialutil.SerialException as e:
            logging.error("query error", exc_info=e)
            self.online = False
            return

    @ifOnline
    def write(self, msg=b""):
        if type(msg) == str:
            msg = msg.encode()
        self.device.write(msg)

    @ifOnline
    def read(self, n):
        try:
            resp = self.device.read(n)
            return resp.decode()
        except serial.SerialException as e:
            logging.error()
            return

    @ifOnline
    def readline(self):
        try:
            resp = self.device.readline()
            return resp.decode()
        except serial.SerialException as e:
            logging.error()
            return

    @ifOnline
    def close(self):
        if not self.device is None:
            if self.device.isOpen():
                self.device.close()

    def __del__(self):
        if not self.device is None:
            if self.device.isOpen():
                self.device.close()


if __name__ == "__main__":

    dev = ResourceManager()
    dev.open_resource("./ttyclient", baudrate=9600)
