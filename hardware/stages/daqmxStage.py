import numpy as np
import time

from threading import Timer, Thread
import traceback
from threading import RLock
import threading
import microscope
import microscope.abc
import logging
from scipy.signal import square

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)

import pathlib
import sys

PATH = pathlib.Path(__file__).parent.absolute()
sys.path.append(str(PATH))


try:
    from PyDAQmx.DAQmxFunctions import *
    from PyDAQmx.DAQmxConstants import *
except Exception as ex:
    _logger.error("PyDAQmx load error:", exc_info=ex)


class Stepper:
    """Class to create a continuous pulse train on a counter

    Usage:  pulse = ContinuousTrainGeneration(period [s],
                            duty_cycle (default = 0.5), counter (default = "dev1/ctr0"),
                            reset = True/False)
                    pulse.start()
                    pulse.stop()
                    pulse.clear()
    """

    Direction = False
    Enable = False
    reset = False
    taskHandle = None
    taskHandleDirEnable = None
    steps_deg = 1.8
    calibr = 400
    currentPosition = 0
    lock = RLock()
    online = False

    def __init__(
        self,
        freq=800,
        counter="Dev1/ctr1",
        reset=True,
        enableTimeout=10,
        currentPosition=0,
    ):
        self.reset = reset
        dt = 2.5e-6
        T = 1 / freq
        duty_cycle = 1 - dt / T
        print("duty_cycle=", duty_cycle)
        if reset:
            self.currentAngle = 0
            DAQmxResetDevice(counter.split("/")[0])
        taskHandle = TaskHandle(0)
        DAQmxCreateTask("", byref(taskHandle))
        DAQmxCreateCOPulseChanFreq(
            taskHandle, counter, "", DAQmx_Val_Hz, DAQmx_Val_Low, 0.0, freq, duty_cycle
        )

        self.taskHandle = taskHandle

        taskHandle1 = TaskHandle(1)
        DAQmxCreateTask("", byref(taskHandle1))
        DAQmxCreateDOChan(
            taskHandle1, "/Dev1/port0/line6", "", DAQmx_Val_ChanForAllLines
        )
        self.taskHandleDir = taskHandle1

        taskHandle2 = TaskHandle(2)
        DAQmxCreateTask("", byref(taskHandle2))
        DAQmxCreateDOChan(
            taskHandle2, "/Dev1/port0/line7", "", DAQmx_Val_ChanForAllLines
        )

        # DAQmxCfgSampClkTiming(taskHandle2,'/Dev1/Ctr0InternalOutput',10000.0,DAQmx_Val_Rising,DAQmx_Val_ContSamps,1000)

        self.taskHandleEnable = taskHandle2

        self.taskHandleDirEnable = taskHandle1
        self.enableTimeout = enableTimeout
        self.currentPosition = currentPosition
        self.online = True

        self.enableTimeout_thread = Thread(target=self.onEnableTimeout, daemon=True)
        self.enableTimeStart = time.time()
        self.enableTimeout_thread.start()

    def onEnableTimeout(self):

        while self.online:
            if self.Enable:
                if (
                    time.time() - self.enableTimeStart
                ) > self.enableTimeout and not self._moving:
                    print("daqmxStepper_enableTimeout")
                    self.enable(0)
            time.sleep(1)

    def enable(self, state):
        if state:
            state = 1
        else:
            state = 0
        if self.Enable and not state:
            DAQmxStopTask(self.taskHandleEnable)

        if self.Enable != state:
            self.Enable = state
            data = np.array([self.Enable], dtype=np.uint8)
            # N = 1000
            # data = (square(np.arange(N),duty=0.999)>0)
            # data = data.astype(np.uint8)
            # data[-1]=1
            with self.lock:

                DAQmxStartTask(self.taskHandleEnable)
                DAQmxWriteDigitalLines(
                    self.taskHandleEnable,
                    len(data),
                    1,
                    10.0,
                    DAQmx_Val_GroupByChannel,
                    data,
                    None,
                    None,
                )
                # DAQmxWriteDigitalU8(self.taskHandleEnable,100,1,100,DAQmx_Val_GroupByChannel,data,None,None)

                self.enableTimeStart = time.time()

        if not self.Enable:
            DAQmxStopTask(self.taskHandleEnable)

    def direction(self, state):
        if state:
            state = 0
        else:
            state = 1
        self.Direction = state
        data = np.array([self.Direction], dtype=np.uint8)
        with self.lock:
            DAQmxStartTask(self.taskHandleDir)
            DAQmxWriteDigitalLines(
                self.taskHandleDir,
                1,
                1,
                10.0,
                DAQmx_Val_GroupByChannel,
                data,
                None,
                None,
            )
            DAQmxStopTask(self.taskHandleDir)

    def start(self, steps, timeout=100):
        self.enable(1)
        if steps == 0:
            return
        with self.lock:
            self._moving = True
            DAQmxCfgImplicitTiming(self.taskHandle, DAQmx_Val_FiniteSamps, int(steps))
            DAQmxStartTask(self.taskHandle)
            DAQmxWaitUntilTaskDone(self.taskHandle, timeout)
            self.stop()
            self._moving = False

    def moveTo(self, target):
        delta = target - self.currentPosition
        self.moveBy(delta)

    def moveBy(self, delta):
        sign = delta / abs(delta)
        delta_steps = int(abs(delta * self.calibr))
        delta = sign * delta_steps / self.calibr
        self.direction(delta > 0)
        self.start(steps=delta_steps)
        self.currentPosition += delta

    def stop(self):
        with self.lock:
            DAQmxStopTask(self.taskHandle)

    def setPosition(self, val):
        self.currentPosition = val

    def getPosition(self):
        return self.currentPosition

    def resetPosition(self):
        self.currentPosition = 0

    def clear(self):
        with self.lock:
            DAQmxClearTask(self.taskHandle)
            DAQmxClearTask(self.taskHandleDir)
            DAQmxClearTask(self.taskHandleEnable)

    def close(self):
        self.enable(0)
        self.online = False
        self.clear()


class StepperDummy:

    Direction = False
    Enable = False
    reset = False
    taskHandle = None
    taskHandleDirEnable = None
    steps_deg = 1.8
    calibr = 1600
    currentPosition = 0
    lock = RLock()
    online = False
    velocity = 1

    def __init__(
        self,
        freq=1000,
        counter="Dev1/ctr1",
        reset=True,
        enableTimeout=20,
        currentPosition=0,
    ):
        self.currentPosition = currentPosition
        self.online = True

    def moveTo(self, target):
        delta = target - self.currentPosition
        self.moveBy(delta)

    def moveBy(self, delta):
        self.currentPosition += delta
        time.sleep(abs(delta / self.velocity))

    def enable(self, state):
        pass

    def stop(self):
        pass

    def setPosition(self, val):
        self.currentPosition = val

    def getPosition(self):
        return self.currentPosition

    def resetPosition(self):
        self.currentPosition = 0

    def clear(self):
        pass

    def close(self):
        self.online = False


class _DAQmxStage(microscope.abc.StageAxis):

    demo = False
    stage = None
    # stepsPerUnit = 1
    initialized = False

    def __init__(
        self, limits=microscope.AxisLimits(0, 25), freq=1000, demo=False, **kwargs
    ):

        super().__init__(**kwargs)
        self._limits = limits
        # self.stepsPerUnit = stepsPerUnit
        # self.address = address
        # self.connection = connection
        self.demo = demo
        if demo:
            self.stage = StepperDummy()
        else:
            self.stage = Stepper(freq=freq)

    def initialize(self):
        # if not self.initialized:
        # 	self.stage.enable(1)
        if (PATH / ".knife_stage_pos").exists():
            with open(PATH / ".knife_stage_pos", "r") as f:
                pos = float(f.read())
                self.set_position(pos)
        self.initialized = self.stage.online

    def shutdown(self):
        self.stage.online = False
        self.stage.enable(0)
        self.stage.close()
        self.initialized = False

    def is_alive(self):
        self.initialized = self.stage.online
        return self.stage.online

    def get_status(self):
        # info = self.stage.getStageAxisInformation()
        return 1

    def get_position(self):
        """Return the stage's position."""
        position = self.stage.getPosition()
        with open(PATH / ".knife_stage_pos", "w") as f:
            f.write(str(position))
        return position

    def set_position(self, pos):
        """Set stage position."""
        self.stage.setPosition(pos)
        with open(PATH / ".knife_stage_pos", "w") as f:
            f.write(str(pos))
        return pos

    @property
    def position(self):
        return self.get_position()

    @property
    def limits(self) -> microscope.AxisLimits:
        return self._limits

    def move_to(self, target=None):
        """Move to target"""
        # The default position set points are zero. If the motors are started without
        # writing to a target, that motor will move to zero, so only start motors
        # if a target has been provided.
        if target is None:
            self.stage.moveTo(0)
        else:
            self.stage.moveTo(target)
        return True

    def move_by(self, delta):
        self.stage.moveBy(delta)
        return True


class DAQmxStage(microscope.abc.Stage):
    """TDC001 controller for multiple devices."""

    def __init__(self, axConfig=None, demo=False, **kwargs) -> None:
        super().__init__(**kwargs)

        self.demo = demo

        if axConfig is None:
            axConfig = {"X": {"limits": (0, 25), "freq": 250}}
        _axes = {}
        for name in axConfig:
            _axes[name] = _DAQmxStage(
                limits=microscope.AxisLimits(*axConfig[name]["limits"]),
                freq=axConfig[name]["freq"],
                demo=demo,
            )
        self._axes = _axes

    def initialize(self) -> bool:
        pass

    def _on_shutdown(self) -> None:
        for name in self._axes:
            self._axes[name].shutdown()

    def _do_shutdown(self) -> None:
        for name in self._axes:
            self._axes[name].shutdown()

    @property
    def axes(self):
        return self._axes

    def move_by(self, delta) -> float:
        for name, rpos in delta.items():
            self.axes[name].move_by(rpos)

    def move_to(self, position) -> float:
        for name, pos in position.items():
            self.axes[name].move_to(pos)

    def is_alive(self):
        res = []
        for name in self._axes:
            res.append(self._axes[name].is_alive())
        return any(res)


if __name__ == "__main__":
    demo = False
    if "sim" in sys.argv:
        demo = True

    time.sleep(5)
    _stage = DAQmxStage(demo=demo, axConfig={"X": {"limits": (0, 25), "freq": 500}})
    _stage.initialize()
    stage = _stage.axes["X"]
    stage.initialize()
    # stage.set_position(0)
    print(stage.position)
    t0 = time.time()
    print(stage.move_to(0))
    print("position", stage.position)
    print(stage.move_to(10))
    print("position", stage.position)

    for i in range(20):
        print(stage.move_by(0.02))
        print("position", stage.position)
    for i in range(20):
        print(stage.move_by(-0.02))
        print("position", stage.position)
        time.sleep(0.2)
    # print(stage.move_to(0))
    # print('position',stage.position)
    # print(stage.move_to(5))
    # print('position',stage.position)

    # print(15/(time.time()-t0))
    # print(_stage.shutdown())

    time.sleep(12)
