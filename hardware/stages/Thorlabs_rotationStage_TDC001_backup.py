# -*- coding: utf-8 -*-
"""
APT Motor Controller for Thorlabs
Adopted from
https://github.com/HaeffnerLab/Haeffner-Lab-LabRAD-Tools/blob/master/cdllservers/APTMotor/APTMotorServer.py
With thanks to SeanTanner@ThorLabs for providing APT.dll and APT.lib


V1.1
20141125 V1.0    First working version
20141201 V1.0a   Use short notation for moving (movRelative -> mRel)
20150417 V1.1    Implementation of simple QT GUI

Michael Leung
mcleung@stanford.edu
"""
import ctypes
from ctypes import c_long, c_buffer, c_float, pointer
import logging
import os
import time
import microscope
import microscope.abc

"""
HWTYPE_BSC001		11	// 1 Ch benchtop stepper driver
HWTYPE_BSC101		12	// 1 Ch benchtop stepper driver
HWTYPE_BSC002		13	// 2 Ch benchtop stepper driver
HWTYPE_BDC101		14	// 1 Ch benchtop DC servo driver
HWTYPE_SCC001		21	// 1 Ch stepper driver card (used within BSC102,103 units)
HWTYPE_DCC001		22	// 1 Ch DC servo driver card (used within BDC102,103 units)
HWTYPE_ODC001		24	// 1 Ch DC servo driver cube
HWTYPE_OST001		25	// 1 Ch stepper driver cube
HWTYPE_MST601		26	// 2 Ch modular stepper driver module
HWTYPE_TST001		29	// 1 Ch Stepper driver T-Cube
HWTYPE_TDC001		31	// 1 Ch DC servo driver T-Cube
HWTYPE_LTSXXX		42	// LTS300/LTS150 Long Travel Integrated Driver/Stages
HWTYPE_L490MZ		43	// L490MZ Integrated Driver/Labjack
HWTYPE_BBD10X		44	// 1/2/3 Ch benchtop brushless DC servo driver
"""


class APTMotor:
    aptdll = None

    def __init__(self, SerialNum=None, HWTYPE=31):

        self.online = False
        # logging.debug(f"APTMotor: APT initialized")
        self.HWType = c_long(HWTYPE)
        self.blCorr = 0.10  # 100um backlash correction
        self.SerialNum = SerialNum

    def connect(self):
        try:
            self.aptdll = ctypes.windll.LoadLibrary("APT.dll")
            self.aptdll.EnableEventDlg(True)

            self.aptdll.APTInit()

            if self.SerialNum is not None:
                logging.info(f"APTmotor: Serial is {self.SerialNum}")
                self.SerialNum = c_long(self.SerialNum)
                self.initializeHardwareDevice()
            else:
                logging.error("APTMotor: No serial, please setSerialNumber")
            self.online = True
        except Exception as ex:
            logging.error("APTMotor", exc_info=ex)
            self.online = True
        return self.online

    def disconnect(self):
        if not self.aptdll is None:
            self.aptdll.APTCleanUp()
        self.online = False
        return True

    def getNumberOfHardwareUnits(self):
        """
        Returns the number of HW units connected that are available to be interfaced
        """
        numUnits = c_long()
        self.aptdll.GetNumHWUnitsEx(self.HWType, pointer(numUnits))
        return numUnits.value

    def getSerialNumberByIdx(self, index):
        """
        Returns the Serial Number of the specified index
        """
        HWSerialNum = c_long()
        hardwareIndex = c_long(index)
        self.aptdll.GetHWSerialNumEx(self.HWType, hardwareIndex, pointer(HWSerialNum))
        return HWSerialNum

    def setSerialNumber(self, SerialNum):
        """
        Sets the Serial Number of the specified index
        """
        logging.info(f"APTMotor: Serial is {SerialNum}")
        self.SerialNum = c_long(SerialNum)
        return self.SerialNum.value

    def initializeHardwareDevice(self):
        """
        Initialises the motor.
        You can only get the position of the motor and move the motor after it has been initialised.
        Once initiallised, it will not respond to other objects trying to control it, until released.
        """

        logging.info(f"APTMotor: initializeHardwareDevice serial {self.SerialNum}")
        result = self.aptdll.InitHWDevice(self.SerialNum)
        if result == 0:
            self.online = True
            logging.info("APTMotor: initializeHardwareDevice connection SUCESS")
        # need some kind of error reporting here
        else:
            raise Exception("Connection Failed. Check Serial Number!")
        return True

    ### Interfacing with the motor settings
    def getHardwareInformation(self):
        model = c_buffer(255)
        softwareVersion = c_buffer(255)
        hardwareNotes = c_buffer(255)
        self.aptdll.GetHWInfo(
            self.SerialNum, model, 255, softwareVersion, 255, hardwareNotes, 255
        )
        hwinfo = [model.value, softwareVersion.value, hardwareNotes.value]
        return hwinfo

    def getStageAxisInformation(self):
        minimumPosition = c_float()
        maximumPosition = c_float()
        units = c_long()
        pitch = c_float()
        self.aptdll.MOT_GetStageAxisInfo(
            self.SerialNum,
            pointer(minimumPosition),
            pointer(maximumPosition),
            pointer(units),
            pointer(pitch),
        )
        stageAxisInformation = [
            minimumPosition.value,
            maximumPosition.value,
            units.value,
            pitch.value,
        ]
        return stageAxisInformation

    def setStageAxisInformation(self, stageAxisInformation):
        minimumPosition = c_float(stageAxisInformation[0])
        maximumPosition = c_float(stageAxisInformation[1])
        units = c_long(stageAxisInformation[2])  # units of mm
        # Get different pitches of lead screw for moving stages for different stages.
        pitch = c_float(stageAxisInformation[3])
        self.aptdll.MOT_SetStageAxisInfo(
            self.SerialNum, minimumPosition, maximumPosition, units, pitch
        )
        return True

    def getHardwareLimitSwitches(self):
        reverseLimitSwitch = c_long()
        forwardLimitSwitch = c_long()
        self.aptdll.MOT_GetHWLimSwitches(
            self.SerialNum, pointer(reverseLimitSwitch), pointer(forwardLimitSwitch)
        )
        hardwareLimitSwitches = [reverseLimitSwitch.value, forwardLimitSwitch.value]
        return hardwareLimitSwitches

    def getVelocityParameters(self):
        minimumVelocity = c_float()
        acceleration = c_float()
        maximumVelocity = c_float()
        self.aptdll.MOT_GetVelParams(
            self.SerialNum,
            pointer(minimumVelocity),
            pointer(acceleration),
            pointer(maximumVelocity),
        )
        velocityParameters = [
            minimumVelocity.value,
            acceleration.value,
            maximumVelocity.value,
        ]
        return velocityParameters

    def getVel(self):
        logging.debug("APTMotor: getVel probing...")
        minVel, acc, maxVel = self.getVelocityParameters()
        logging.debug("APTMotor: getVel maxVel")
        return maxVel

    def setVelocityParameters(self, minVel, acc, maxVel):
        minimumVelocity = c_float(minVel)
        acceleration = c_float(acc)
        maximumVelocity = c_float(maxVel)
        self.aptdll.MOT_SetVelParams(
            self.SerialNum, minimumVelocity, acceleration, maximumVelocity
        )
        return True

    def setVel(self, maxVel):
        logging.debug(f"APTMotor: setVel {maxVel}")
        minVel, acc, oldVel = self.getVelocityParameters()
        self.setVelocityParameters(minVel, acc, maxVel)
        return True

    def getVelocityParameterLimits(self):
        maximumAcceleration = c_float()
        maximumVelocity = c_float()
        self.aptdll.MOT_GetVelParamLimits(
            self.SerialNum, pointer(maximumAcceleration), pointer(maximumVelocity)
        )
        velocityParameterLimits = [maximumAcceleration.value, maximumVelocity.value]
        return velocityParameterLimits

        """
		Controlling the motors
		m = move
		c = controlled velocity
		b = backlash correction

		Rel = relative distance from current position.
		Abs = absolute position
		"""

    def getPos(self):
        """
        Obtain the current absolute position of the stage
        """
        logging.debug(f"APTMotor: getPos probing...")
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")

        position = c_float()
        self.aptdll.MOT_GetPosition(self.SerialNum, pointer(position))
        logging.debug(f"APTMotor: getPos  {position.value}")
        return position.value

    def mRel(self, relDistance):
        """
        Moves the motor a relative distance specified
        relDistance    float     Relative position desired
        """
        logging.debug(f"APTMotor: mRel  {relDistance, c_float(relDistance)}")
        if not self.online:
            logging.debug(
                f"APTMotor: Please connect first! Use initializeHardwareDevice"
            )
            # raise Exception('Please connect first! Use initializeHardwareDevice')
        relativeDistance = c_float(relDistance)
        self.aptdll.MOT_MoveRelativeEx(self.SerialNum, relativeDistance, True)
        logging.debug(f"APTMotor: mRel SUCESS")
        return True

    def mAbs(self, absPosition):
        """
        Moves the motor to the Absolute position specified
        absPosition    float     Position desired
        """
        logging.debug(f"APTMotor: mAbs  {absPosition, c_float(absPosition)}")
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        absolutePosition = c_float(absPosition)
        self.aptdll.MOT_MoveAbsoluteEx(self.SerialNum, absolutePosition, True)
        logging.debug(f"APTMotor: mAbs SUCESS")
        return True

    def mcRel(self, relDistance, moveVel=0.5):
        """
        Moves the motor a relative distance specified at a controlled velocity
        relDistance    float     Relative position desired
        moveVel        float     Motor velocity, mm/sec
        """
        logging.debug(
            f"APTMotor: mcRel ', relDistance, c_float(relDistance), 'mVel {moveVel}"
        )
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        # Save velocities to reset after move
        maxVel = self.getVelocityParameterLimits()[1]
        # Set new desired max velocity
        self.setVel(moveVel)
        self.mRel(relDistance)
        self.setVel(maxVel)
        logging.debug(f"APTMotor: mcRel SUCESS")
        return True

    def mcAbs(self, absPosition, moveVel=0.5):
        """
        Moves the motor to the Absolute position specified at a controlled velocity
        absPosition    float     Position desired
        moveVel        float     Motor velocity, mm/sec
        """
        logging.debug(
            f"APTMotor: mcAbs ', absPosition, c_float(absPosition), 'mVel {moveVel}"
        )
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        # Save velocities to reset after move
        minVel, acc, maxVel = self.getVelocityParameters()
        # Set new desired max velocity
        self.setVel(moveVel)
        self.mAbs(absPosition)
        self.setVel(maxVel)
        logging.debug(f"APTMotor: mcAbs SUCESS")
        return True

    def mbRel(self, relDistance):
        """
        Moves the motor a relative distance specified
        relDistance    float     Relative position desired
        """
        logging.debug(f"APTMotor: mbRel  {relDistance, c_float(relDistance)}")
        if not self.online:
            logging.debug(
                f"APTMotor: Please connect first! Use initializeHardwareDevice"
            )
            # raise Exception('Please connect first! Use initializeHardwareDevice')
        self.mRel(relDistance - self.blCorr)
        self.mRel(self.blCorr)
        logging.debug(f"APTMotor: mbRel SUCESS")
        return True

    def mbAbs(self, absPosition):
        """
        Moves the motor to the Absolute position specified
        absPosition    float     Position desired
        """
        logging.debug(f"APTMotor: mbAbs  {absPosition, c_float(absPosition)}")
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        if absPosition < self.getPos():
            logging.debug(f"APTMotor: backlash mAbs {absPosition - self.blCorr}")
            self.mAbs(absPosition - self.blCorr)
        self.mAbs(absPosition)
        logging.debug(f"APTMotor: mbAbs SUCESS")
        return True

    def go_home(self):
        """
        Move the stage to home position and reset position entry
        """
        logging.debug(f"APTMotor: Going home")
        if not self.online:
            raise Exception("Please connect first! Use initializeHardwareDevice")
        logging.debug(f"APTMotor: go_home SUCESS")
        self.aptdll.MOT_MoveHome(self.SerialNum)
        return True

        """ Miscelaneous """

    def identify(self):
        """
        Causes the motor to blink the Active LED
        """
        self.aptdll.MOT_Identify(self.SerialNum)
        return True

    def cleanUpAPT(self):
        """
        Releases the APT object
        Use when exiting the program
        """
        self.aptdll.APTCleanUp()
        logging.debug(f"APTMotor: APT cleaned up")
        self.online = False


class TDC001(object):
    demo = False
    online = False
    driver = None

    def __new__(cls, SerialNum=None, HWTYPE=31, demo=False):
        if demo:
            return APTMotor_sim()
        else:
            return APTMotor(SerialNum=SerialNum, HWTYPE=HWTYPE)


class APTMotor_sim:
    online = False
    pos = 0
    vel = 10

    def __init__(self):
        print("sim")
        pass

    def connect(self):
        self.online = True
        return True

    def disconnect(self):
        self.online = False
        return True

    def getPos(self):
        return self.pos

    def mbAbs(self, pos):
        t = abs(self.pos - pos) / self.vel
        time.sleep(t)
        self.pos = pos
        return True

    def mbRel(self, step):
        t = abs(step) / self.vel
        time.sleep(t)
        self.pos += step
        return True

    def mcRel(self, step, vel):
        t = abs(step) / vel
        time.sleep(t)
        self.pos += step
        return True

    def go_home(self):
        t = abs(self.pos) / self.vel
        time.sleep(t)
        self.pos = 0
        return True

    def cleanUpAPT(self):
        self.online = False


class _TDC001(microscope.abc.StageAxis):

    demo = False
    stage = None
    # stepsPerUnit = 1
    initialized = False

    def __init__(
        self,
        SerialNum=None,
        HWTYPE=None,
        limits=microscope.AxisLimits(0, 360),
        # stepsPerUnit=1,
        demo=False,
        **kwargs,
    ):

        super().__init__(**kwargs)
        self._limits = limits
        # self.stepsPerUnit = stepsPerUnit
        # self.address = address
        # self.connection = connection
        self.demo = demo
        if demo:
            self.stage = APTMotor_sim()
        else:
            self.stage = APTMotor(SerialNum, HWTYPE)

    def initialize(self):
        if not self.initialized:
            self.stage.connect()
        self.initialized = self.stage.online

    def shutdown(self):

        self.stage.disconnect()
        self.initialized = False

    def is_alive(self):
        self.initialized = self.stage.online
        return self.stage.online

    def get_status(self):
        info = self.stage.getStageAxisInformation()
        return info

    def get_position(self):
        """Return the stage's position."""
        position = self.stage.getPos()

        return position

    @property
    def position(self):
        return self.get_position()

    @property
    def limits(self) -> microscope.AxisLimits:
        return self._limits

    def move_to(self, target=None):
        """Move to target"""
        # The default position set points are zero. If the motors are started without
        # writing to a target, that motor will move to zero, so only start motors
        # if a target has been provided.
        if target is None:
            self.stage.go_home()
        else:
            self.stage.mbAbs(target)
        return True

    def move_by(self, delta):
        self.stage.mbRel(delta)
        return True


class TDC001_rotationStage(microscope.abc.Stage):
    """TDC001 controller for multiple devices."""

    def __init__(self, axConfig=None, demo=False, **kwargs) -> None:
        super().__init__(**kwargs)

        self.demo = demo

        if axConfig is None:
            axConfig = {
                "HWP": {"SerialNum": 83854487, "HWTYPE": 31, "limits": (0, 360)}
            }
        _axes = {}
        for name in axConfig:
            _axes[name] = _TDC001(
                SerialNum=axConfig[name]["SerialNum"],
                HWTYPE=axConfig[name]["HWTYPE"],
                limits=microscope.AxisLimits(*axConfig[name]["limits"]),
                demo=demo,
            )
        self._axes = _axes

    def initialize(self) -> bool:
        pass

    def _on_shutdown(self) -> None:
        for name in self._axes:
            self._axes[name].shutdown()

    def _do_shutdown(self) -> None:
        for name in self._axes:
            self._axes[name].shutdown()

    @property
    def axes(self):
        return self._axes

    def move_by(self, delta) -> float:
        for name, rpos in delta.items():
            self.axes[name].move_by(rpos)

    def move_to(self, position) -> float:
        for name, pos in position.items():
            self.axes[name].move_to(pos)

    def is_alive(self):
        res = []
        for name in self._axes:
            res.append(self._axes[name].is_alive())
        return any(res)


if __name__ == "__main__":
    # -*- coding: utf-8 -*-
    import sys

    demo = False
    if "sim" in sys.argv:
        demo = True

    stage = TDC001_rotationStage()

    stage.axes["HWP"].initialize()
    print("pos:", stage.axes["HWP"].position)
    print("move_to 10:", stage.axes["HWP"].move_to(10))
    print("move_to :", stage.axes["HWP"].move_to())
    print("move_by 10:", stage.axes["HWP"].move_by(10))
    print("move_by -10:", stage.axes["HWP"].move_by(-10))

    print("HWP is alive:", stage.axes["HWP"].is_alive())
    print("stage is alive:", stage.is_alive())
    print("HWP status:", stage.axes["HWP"].get_status())
    print("stage shutdown:", stage.shutdown())
    print("HWP is alive:", stage.axes["HWP"].is_alive())
    print("stage is alive:", stage.is_alive())
