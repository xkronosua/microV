# -*- coding: utf-8 -*-

import logging
import os
import time
import microscope
import microscope.abc
import threading


class Motor_sim(object):
    position = 0
    motion_error = False
    is_in_motion = False

    def __init__(self):
        pass

    def enable(self):
        self.alive = True
        # t = threading.Thread(target=self.process,daemon=True)
        # t.start()
        return True

    def disable(self):
        self.alive = False

    def get_stage_axis_info(self):
        return ("KDC101", "31")

    def move_to(self, target, wait=False):
        print("moving:start")
        # self.is_in_motion = True
        self.target = target

        self.position = target
        self.is_in_motion = False
        print("moving:done")

    def move_by(self, delta, wait=False):
        # self.is_in_motion = True
        self.target = self.position + delta

        self.position = self.target
        self.is_in_motion = False

    def move_home(self):
        # self.is_in_motion = True
        self.target = 0
        self.position = 0

        self.is_in_motion = False

    def process(self):
        current_pos = 0

        dx = 0.1
        target = 0

        while self.alive:
            print(self.position, self.target)

            if abs(current_pos - self.target) >= dx:

                direction = (self.target - current_pos) / abs(self.target - current_pos)
                current_pos += dx * direction
                self.is_in_motion = True
            else:
                current_pos = self.target
                self.position = current_pos
                self.is_in_motion = False

            time.sleep(0.1)
            print("moving")


class _KDC101(microscope.abc.StageAxis):

    demo = False
    stage = None
    # stepsPerUnit = 1
    initialized = False

    def __init__(
        self,
        SerialNum=None,
        limits=microscope.AxisLimits(0, 360),
        # stepsPerUnit=1,
        demo=False,
        **kwargs
    ):

        super().__init__(**kwargs)
        self._limits = limits
        # self.stepsPerUnit = stepsPerUnit
        # self.address = address
        # self.connection = connection
        self.demo = demo
        if demo:
            self.stage = Motor_sim()
        else:
            import thorlabs_apt

            # print(thorlabs_apt.list_available_devices())

            self.stage = thorlabs_apt.Motor(serial_number=SerialNum)

    def initialize(self):
        if not self.initialized:
            try:
                self.stage.enable()
            except Exception as ex:
                print("initialization error")
                logging.error("initialization error:", exc_info=ex)
                self.initialized = False

        self.initialized = True

    def shutdown(self):

        self.stage.disable()
        self.initialized = False

    def is_alive(self):
        return self.initialized and not self.stage.motion_error

    def get_status(self):
        info = self.stage.get_stage_axis_info()
        return info

    def get_position(self):
        """Return the stage's position."""
        position = self.stage.position

        return position

    @property
    def position(self):
        return self.get_position()

    @property
    def limits(self) -> microscope.AxisLimits:
        return self._limits

    def move_to(self, target=None, wait=False):
        """Move to target"""
        # The default position set points are zero. If the motors are started without
        # writing to a target, that motor will move to zero, so only start motors
        # if a target has been provided.
        if target is None:
            self.stage.move_home()
        else:
            self.stage.move_to(target)
            if wait:
                for i in range(500):
                    time.sleep(0.1)
                    if not self.is_moving():
                        break
        return True

    def move_by(self, delta, wait=False):
        self.stage.move_by(delta)
        if wait:
            for i in range(50):
                time.sleep(0.05)
                if not self.is_moving():
                    break
        return True

    def is_moving(self):
        return self.stage.is_in_motion


class KDC101_motorizedActuator(microscope.abc.Stage):
    """KDC101 controller for multiple devices."""
    _axes = {}
    def __init__(self, axConfig=None, demo=False, **kwargs) -> None:
        super().__init__(**kwargs)

        self.demo = demo

        if axConfig is None:
            axConfig = {
                "beamShift_H": {"SerialNum": 27260096,  "limits": (0, 25)},
                "beamShift_V": {"SerialNum": 27260097,  "limits": (0, 12)},
            }

        _axes = {}
        for name in axConfig:

            _axes[name] = _KDC101(SerialNum=axConfig[name]["SerialNum"], demo=demo)
            print(_axes)
        self._axes = _axes

    def initialize(self) -> bool:
        for name in self._axes:
            self._axes[name].initialize()
            print("KDC100:",name)

    def _on_shutdown(self) -> None:
        for name in self._axes:
            self._axes[name].shutdown()

    def _do_shutdown(self) -> None:
        for name in self._axes:
            self._axes[name].shutdown()

    @property
    def axes(self):
        return self._axes

    def move_by(self, delta) -> float:
        for name, rpos in delta.items():
            self.axes[name].move_by(rpos)

    def move_to(self, position) -> float:
        for name, pos in position.items():
            self.axes[name].move_to(pos)

    def is_alive(self):
        res = []
        for name in self._axes:
            res.append(self._axes[name].is_alive())
        return any(res)


if __name__ == "__main__":
    # -*- coding: utf-8 -*-
    import sys

    demo = False
    if "sim" in sys.argv:
        demo = True

    stage = KDC101_motorizedActuator()
    print("Initialization:")
    stage.initialize()
    print("pos:", stage.axes["beamShift_H"].position)
    print("move_to 10:", stage.axes["beamShift_H"].move_to(10))
    print("move_to :", stage.axes["beamShift_H"].move_to())
    print("move_by 10:", stage.axes["beamShift_H"].move_by(10))
    print("move_by -10:", stage.axes["beamShift_H"].move_by(-10))

    print("HWP is alive:", stage.axes["beamShift_V"].is_alive())
    print("stage is alive:", stage.is_alive())
    print("HWP status:", stage.axes["beamShift_V"].get_status())
    print("stage shutdown:", stage.shutdown())
    print("HWP is alive:", stage.axes["beamShift_V"].is_alive())
    print("stage is alive:", stage.is_alive())
