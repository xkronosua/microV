# from serialManager import ResourceManager
import time
import logging
import serial
from serial.tools import list_ports
from binascii import unhexlify, hexlify
import struct
import numpy as np
import threading
import traceback

import microscope
import microscope.abc

import re

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)
import pathlib
import sys

PATH = pathlib.Path(__file__).parent.absolute()
sys.path.append(str(PATH))


class _Stageh(microscope.abc.SerialDeviceMixin, microscope.abc.StageAxis):

    demo = False
    connection = None
    stepsPerUnit = 1600
    stage = None
    pos = [0]
    initialized = False
    motor = 0
    _motor = b"0"

    def __init__(
        self,
        connection=None,
        motor=0,
        limits=microscope.AxisLimits(0, 10),
        stepsPerUnit=1600,
        position=0,
        demo=False,
        **kwargs,
    ):

        super().__init__(**kwargs)
        self.motor = motor
        self._motor = str(motor).encode()
        self._limits = limits
        self.stepsPerUnit = stepsPerUnit
        self.connection = connection

        self.pos = position

    def initialize(self):
        try:
            if (PATH / (".arduino_stage_pos%d" % self.motor)).exists():
                print(f"last position motor[{self.motor}]:")
                with open(PATH / (".arduino_stage_pos%d" % self.motor), "r") as f:
                    pos = float(f.read())
                    print(pos)
                    self.set_position(pos)
            self.initialized = True
        except:
            traceback.print_exc()
            self.initialized = False

    @microscope.abc.SerialDeviceMixin.lock_comms
    def send(self, command: bytes, waitResponse=True) -> bytes:
        """Send command and retrieve response."""
        success = False
        while not success:
            # print(command)
            self._write(command)
            if waitResponse:
                response = self._readline()
                # Catch zero-length responses to queries and retry.
                if len(response) > 0:
                    success = True
            else:
                response = None
        return response

    def _write(self, command: bytes, ending=b"\r\n") -> int:
        """Write a command."""
        response = self.connection.write(command + ending)
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _readline(self) -> bytes:
        """Read line with lock."""
        response = self.connection.readline()
        return response

    def is_alive(self):
        if not self.initialized:
            return False
        try:

            self.connection.inWaiting()
            return True
        except:
            return False

    def lock(self, state):
        """enable/disable motor"""

        if state:
            enable = b"1"
        else:
            enable = b"0"
        self.send(self._motor + b"e" + enable)

    def get_position(self):
        """Return the stage's position."""
        pos = None
        for i in range(50):
            resp = self.send(self._motor + b"p")
            try:
                pos = int(resp) / self.stepsPerUnit
                break
            except:
                pass
            time.sleep(0.1)
        if not pos is None:
            self.pos = pos

            with open(PATH / (".arduino_stage_pos%d" % self.motor), "w") as f:
                f.write(str(pos))

        return self.pos

    def set_position(self, pos):
        """Set stage position."""
        nstep = int(pos * self.stepsPerUnit)
        try:
            resp = int(self.send(self._motor + b"s" + str(nstep).encode()))
        except:
            resp = int(self.send(self._motor + b"s" + str(nstep).encode()))
        self.pos = pos

        with open(PATH / (".arduino_stage_pos%d" % self.motor), "w") as f:
            f.write(str(pos))

        return self.pos

    def is_moving(self):
        print("is_moving")
        resp = self.send(self._motor + b"m")
        print(resp)
        return b"1" in resp

    def get_status(self):
        return self.is_alive()

    @property
    def position(self):
        return self.get_position()

    @property
    def limits(self) -> microscope.AxisLimits:
        return self._limits

    @microscope.abc.SerialDeviceMixin.lock_comms
    def flush_buffer_(self):

        self.connection.reset_output_buffer()
        self.connection.reset_input_buffer()
        while self.connection.inWaiting() > 1:
            response = self._readline()

    @microscope.abc.SerialDeviceMixin.lock_comms
    def move_to(self, target=None, wait=False):
        """Move to target"""
        # The default position set points are zero. If the motors are started without
        # writing to a target, that motor will move to zero, so only start motors
        # if a target has been proved.
        # _logger.info(f'TARGET:{target}')
        if target is None:
            command = self._motor + b"H"
            pos = self.send(command).replace(b"\r", b"").replace(b"\n", b"")
            # self.flush_buffer_()
            self.pos = self.get_position()
            return self.pos

        if target < self._limits.lower:
            target = self._limits.lower
        elif target > self._limits.upper:
            target = self._limits.upper

        steps = int(self.stepsPerUnit * target)
        expected_position = str(steps).encode()
        command_char = b"A" if wait else b"a"
        command = self._motor + command_char + str(steps).encode()
        if target >= self._limits.lower and target <= self._limits.upper:
            pos = self.send(command).replace(b"\r", b"").replace(b"\n", b"")
            # pos = self.send(command).replace(b'\r',b'').replace(b'\n',b'')
            if wait:

                if pos != expected_position:
                    _logger.error(
                        f"Motor {self._motor} position mismatch: {expected_position}!={pos}"
                    )
                self.flush_buffer_()
            else:
                pass
                # def update_pos():
                # 	while self.is_moving():
                # 		time.sleep(0.1)
                # 		#self.pos = self.get_position()
                # 	self.pos = self.get_position()

                # t = threading.Thread(target=update_pos,daemon=True)
                # t.start()
        self.pos = self.get_position()
        return self.pos

    def move_by(self, delta, wait=False):
        if int(self.stepsPerUnit * delta) == 0:
            self.pos = self.get_position()
            return self.pos

        target = self.pos + delta
        steps = int(self.stepsPerUnit * delta)
        expected_position = str(int(self.stepsPerUnit * target)).encode()
        command_char = b"R" if wait else b"r"
        command = self._motor + command_char + str(steps).encode()
        print(command)

        if target >= self._limits.lower and target <= self._limits.upper:
            pos = self.send(command).replace(b"\r", b"").replace(b"\n", b"")
            if wait:

                if pos != expected_position:
                    _logger.error(
                        f"Motor {self._motor} position mismatch: {expected_position}!={pos}"
                    )
                    self.flush_buffer_()
            else:
                pass
                # def update_pos():
                # 	while self.is_moving():
                # 		time.sleep(0.1)
                # 		#self.pos = self.get_position()
                # 	self.pos = self.get_position()
                # t = threading.Thread(target=update_pos,daemon=True)
                # t.start()
        self.pos = self.get_position()
        return self.pos

    def shutdown(self):
        if not self.connection is None:
            self.connection.close()
            self.initialized = False


class ArduinoStage(microscope.abc.Stage):
    """MoCo dc controller for PI linear stage. Units - mm"""

    connection = serial.Serial()
    serial_number = None

    def __init__(
        self,
        port=None,
        baudrate=19200,
        serial_number="5573532393535190A1B0",
        timeout=0.1,
        axConfig=None,
        demo=False,
        **kwargs,
    ) -> None:
        super().__init__(**kwargs)
        self.port = port
        self.serial_number = serial_number
        self.baudrate = baudrate

        self.demo = demo
        if self.demo:
            # port = './ttyclient_MoCo'
            # baudrate = 9600
            self.connection = SerialDummy()
        else:
            self.connection = serial.Serial()
            self.connection.port = port
            self.connection.baudrate = baudrate
            self.connection.timeout = timeout
            self.connection.stopbits = serial.STOPBITS_ONE
            self.connection.bytesize = serial.EIGHTBITS
            self.connection.parity = serial.PARITY_NONE
            self.connection.set_buffer_size(rx_size=8192, tx_size=None)
            self.connection.xonxoff = True

        if axConfig is None:
            axConfig = {
                "lens_stage": {
                    "motor": 0,
                    "position": 0,
                    "limits": (0, 15),
                    "stepsPerUnit": 1600,
                },
                "knife_stage": {
                    "motor": 1,
                    "position": 0,
                    "limits": (0, 25),
                    "stepsPerUnit": 1600,
                },
            }

        _axes = {}

        for i, name in enumerate(axConfig):

            _axes[name] = _Stageh(
                connection=self.connection,
                motor=axConfig[name]["motor"],
                position=axConfig[name]["position"],
                limits=microscope.AxisLimits(*axConfig[name]["limits"]),
                stepsPerUnit=axConfig[name]["stepsPerUnit"],
                demo=demo,
            )
            # if not axConfig[name]['position'] is None:

        names = list(_axes.keys())
        for name in names[1:]:
            _axes[name]._comms_lock = _axes[names[0]]._comms_lock

        self._axes = _axes

    def initialize(self) -> bool:

        if self.port is None:
            ports = list(list_ports.grep("2341:0043"))
            for p in ports:
                if p.serial_number == self.serial_number:
                    self.connection.port = p.device
                    break
        # print(self.connection.port)
        if self.demo:
            self.connection.port = "./ttyclient_MoCo"
            baudrate = 9600
            self.connection.open()
        try:
            if self.is_alive():
                return True
            else:
                self.connection.open()
                time.sleep(1)
                self.connection.read(self.connection.inWaiting())
                return True

        except Exception as ex:
            _logger.error(f"Can't connect to arduino", exc_info=ex)
            return False

    def _on_shutdown(self) -> None:
        if not self.connection is None:
            self.connection.close()

    def _do_shutdown(self) -> None:
        if not self.connection is None:
            self.connection.close()

    @property
    def axes(self):
        return self._axes

    def move_by(self, delta) -> float:
        for name, rpos in delta.items():
            self.axes[name].move_by(rpos)

    def move_to(self, position) -> float:
        for name, pos in position.items():
            self.axes[name].move_to(pos)
            # print('TARGET0:',pos)

    def setLight(self, value):
        if self.is_alive():
            ax = list(self.axes.keys())[0]
            self.axes[ax].send(f"0L{value}\r\n".encode())

    def is_alive(self):
        try:
            self.connection.inWaiting()
            return True
        except:
            return False


class SerialDummy(object):

    alive = False
    line = b""
    response = b""

    def __init__(self):
        pass

    def open(self):
        self.alive = True
        t = threading.Thread(target=self.process, daemon=True)
        t.start()
        return True

    def flushInput(self):
        pass  # self.line = b''

    def flushOutput(self):
        pass  # self.response = b''

    def reset_input_buffer(self):
        self.response = b""
        self.line = b""

    def reset_output_buffer(self):
        self.response = b""
        self.line = b""

    def is_alive(self):
        return self.alive

    def isOpen(self):
        return self.alive

    def inWaiting(self):
        return True

    def close(self):
        self.alive = False

    def process(self):
        current_pos = 0
        current_pos1 = 0

        stepsPerUnit = np.array([1600, 800, 91.67, 800])
        dx = 0.1 * stepsPerUnit
        target = [0, 0, 0, 0]

        is_moving = [0, 0, 0, 0]

        wait_onTarget = [0, 0, 0, 0]

        current_pos = [0, 0, 0, 0]

        while self.alive:
            # print(self.line)
            if self.line != b"":
                # print(self.line)
                if self.line[-1] == b"\n":
                    lines = self.line.split(b"\n")
                    self.line = b""

                else:
                    lines = self.line.split(b"\n")[:-1]
                    self.line = self.line.split(b"\n")[-1]

                for line in lines:
                    # self.line = b''
                    line = line.replace(b"\r", b"")
                    print("line:", line)
                    # print('raw: |',parts,'|')
                    if len(line) < 2:
                        continue
                    ID = int(line[:1])
                    command = line[1:2]
                    if len(line) > 2:
                        value = int(line[2:])
                    else:
                        value = None

                    if command == b"a":
                        target[ID] = value
                        is_moving[ID] = True
                        self.response += str(current_pos[ID]).encode() + b"\r\n"
                    if command == b"r":
                        target[ID] = current_pos[ID] + value
                        is_moving[ID] = True
                        self.response += str(current_pos[ID]).encode() + b"\r\n"
                    elif command == b"A":
                        target[ID] = value
                        is_moving[ID] = True
                        wait_onTarget[ID] = True
                    elif command == b"R":
                        target[ID] = target[ID] + value
                        is_moving[ID] = True
                        wait_onTarget[ID] = True
                    elif command == b"s":
                        current_pos[ID] = value
                        target[ID] = value
                        self.response += str(current_pos[ID]).encode() + b"\r\n"

                    elif command == b"p":
                        self.response += str(current_pos[ID]).encode() + b"\r\n"

                    elif command == b"m":
                        self.response += str(is_moving[ID]).encode() + b"\r\n"

                    elif command == b"e":
                        self.response += str(value).encode() + b"\r\n"
                    elif command == b"L":
                        self.response += str(value).encode() + b"\r\n"

                    elif command == b"H":
                        target[ID] = 0
                        is_moving[ID] = True
                        wait_onTarget[ID] = True

                    else:
                        pass

            for i in range(len(target)):
                wait = True
                while wait:
                    if abs(current_pos[i] - target[i]) >= dx[i]:

                        # dt = dx*vel
                        # print('dt',dt)
                        # time.sleep(dt)
                        print(current_pos[i])
                        direction = (target[i] - current_pos[i]) / abs(
                            target[i] - current_pos[i]
                        )
                        current_pos[i] += int(dx[i] * direction)
                        is_moving[i] = True

                    else:
                        current_pos[i] = int(target[i])
                        is_moving[i] = False
                        if wait_onTarget[i]:
                            self.response += str(current_pos[i]).encode() + b"\r\n"
                        wait_onTarget[i] = False

                    wait = wait_onTarget[i]

                    time.sleep(0.03)

    def write(self, line):
        self.line += line
        # print(line,self.line)
        # print(self.line)

    def readline(self):
        for i in range(10):
            if self.response == b"":
                time.sleep(0.02)
            else:
                break

        resp = self.response

        self.response = b"\n".join(self.response.split(b"\n")[1:])
        return resp

    def read(self, n):
        r = self.response
        self.response = self.response[n:]

        return r[:n]


if __name__ == "__main__":
    demo = False
    if "sim" in sys.argv:
        demo = True
    print("init")
    _stage = ArduinoStage(demo=demo)
    _stage.initialize()

    stage = _stage.axes["lens_stage"]
    stage1 = _stage.axes["knife_stage"]
    print("init")
    stage.initialize()
    stage1.initialize()

    # stage.enable(1)
    print("position:", stage.position)
    t0 = time.time()
    print(stage.move_to(0, wait=True))
    print("position:", stage.position)
    print(stage.move_to(2000, wait=False))

    while stage.is_moving():
        print("position", stage.position)
        time.sleep(0.01)
    time.sleep(1)
    print("position", stage.position)

    t0 = time.time()
    """
	#stage1.enable(1)
	print('position1:', stage1.position)
	t0 = time.time()
	print(stage1.move_to(0,wait=True))
	print('position1:', stage1.position)
	print(stage1.move_to(5,wait=False))
	print('position1:', stage1.position)
	while(stage1.is_moving()):
		print('position1',stage1.position)
		time.sleep(0.01)
	print('position1',stage1.position)
	time.sleep(1)
	#stage1.enable(0)
	#print(15/(time.time()-t0))
	"""
    print(_stage.shutdown())
