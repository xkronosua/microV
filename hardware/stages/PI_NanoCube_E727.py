# -*- coding: utf-8 -*-
import os
import sys
import time
import traceback
import numpy as np
from collections import OrderedDict
import re
import threading
import exdir
from pathlib import Path
from collections.abc import Iterable

from multiprocessing import shared_memory

from pipython import GCSDevice, GCSError, gcserror
from pipython.fastaligntools import ResultID
from pipython.datarectools import RecordOptions, TriggerSources
from pipython.pitools import itemstostr, savegcsarray  # ,waitonwalk

from scipy.spatial.transform import Rotation

import logging

import inspect

funcName = lambda: inspect.stack()[1][3]


def gen_grid_2D(rangeX=(0, 10), rangeY=(0, 10), axis=0, N=100, max_points=2 ** 15):
    """Generate zig-zag toolpath with N scans along axis(0-X,1-Y) direction"""
    index = np.arange(N)
    t = np.linspace(0, 1, N)

    if axis == 0:
        x = np.linspace(rangeX[0], rangeX[1], N)
        x = np.insert(x, index, x)
        y = np.zeros(len(x))
        y[1::4] = 1
        y[2::4] = 1
        y = y * (rangeY[1] - rangeY[0]) + rangeY[0]
    else:
        y = np.linspace(rangeY[0], rangeY[1], N)
        y = np.insert(y, index, y)
        x = np.zeros(len(y))
        x[1::4] = 1
        x[2::4] = 1
        x = x * (rangeX[1] - rangeX[0]) + rangeX[0]
    # x = np.append(x,x[-1])
    # y = np.append(y,y[-1])

    r = (np.diff(x) ** 2 + np.diff(y) ** 2) ** 0.5
    segment_length = (r / r.sum() * max_points).astype(int)
    return x, y, segment_length


class E727:
    serial_number = "0117047326"
    pi_device = GCSDevice("E-727")
    # axes_transform_dict = {0:'1',1:'2',2:'3', }
    axes_transform_dict = {
        0: "1",
        1: "3",
        2: "2",
    }
    axes_transform_dict_inv = {v: k for k, v in axes_transform_dict.items()}
    axes = ["1", "2", "3"]  # real axes names
    softStartEvent = threading.Event()
    scanParam = {}
    demo = False
    target = np.array([0, 0, 0])

    # tiltCompensation = Rotation.from_euler('y', 2.45, degrees=True)
    tiltCompensation = Rotation.from_euler("y", 0, degrees=True)

    tiltCompensation_inv = tiltCompensation.inv()

    def __init__(self, demo=False):
        if demo:
            self.pi_device = GCSDevice_sim("E-727")

    def set_tiltCompensation(self, X_angle=0, Y_angle=0, Z_angle=0):
        print(self.tiltCompensation.as_euler("xyz", degrees=True))
        self.tiltCompensation = Rotation.from_euler(
            "xyz", [X_angle, Y_angle, Z_angle], degrees=True
        )
        self.tiltCompensation_inv = self.tiltCompensation.inv()
        print(self.tiltCompensation.as_euler("xyz", degrees=True))

    def set_axes_transform(self, axes_dict):
        """Convert input axes names to real axes names understandable for 3D stage
        ( in order to redefine axes due to the stage mounting orientation)
        """
        self.axes_transform_dict = axes_dict

    def transform_axes(self, axes):
        if axes is None:
            axes = list(self.axes_transform_dict.keys())
        if type(axes) == int:
            axes = [axes]
        axes = [self.axes_transform_dict[ax] for ax in axes]
        return axes

    def state(self):
        """Return last error state from buffer and reset buffer to "no error"."""
        state = self._state
        self._state = GCSError(gcserror.PI_CNTR_NO_ERROR_0)
        return state

    def connect(self):
        # /////////////////////////////////////////
        # // connect to the controller over USB. //
        # /////////////////////////////////////////
        res = False
        try:
            self.pi_device.ConnectUSB(self.serial_number)
            res = True
        except GCSError as exc:
            logging.error(
                f"Can't connect to device with SN={self.serial_number}", exc_info=exc
            )
            logging.info("Trying to find SN")
            ids = self.pi_device.EnumerateUSB()

            for id_ in ids:
                if "E-727" in id_:
                    pattern = re.search(".*(\d{10}).*", id_)
                    if pattern:
                        self.serial_number = pattern.groups()[0]
                        try:
                            self.pi_device.ConnectUSB(self.serial_number)
                            res = True
                        except GCSError as exc:
                            logging.error(
                                f"Can't connect to device with SN={self.serial_number}",
                                exc_info=exc,
                            )
                            return False

                        break

        return res

    def close(self):
        self.pi_device.close()

    def __del__(self):
        self.close()

    def handle_exceptions(f):
        def wrapper(*args, **kw):
            try:
                return f(*args, **kw)
            except GCSError as exc:
                logging.error(f"Error in {funcName()}", exc_info=exc)
                return None

        return wrapper

    @handle_exceptions
    def getAxesNames(self):
        """Get the name of the connected axis."""
        self.axes = self.pi_device.qSAI()
        return self.axes

    @handle_exceptions
    def setServoControl(self, axes=None, values=(True, True, True)):
        """Set servo-control "on" or "off" (closed-loop/open-loop mode)."""
        axes = self.transform_axes(axes)
        self.pi_device.SVO(axes, values=values)
        return True

    @handle_exceptions
    def getServoControl(self, axes=None):
        """Set servo-control "on" or "off" (closed-loop/open-loop mode)."""
        axes = self.transform_axes(axes)
        res = self.pi_device.qSVO(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def autoZero(self, axes=None, wait=False, timeout=10):
        """Automatic zero-point calibration"""
        axes_ = axes
        axes = self.transform_axes(axes)

        self.pi_device.ATZ()
        res = True
        if wait:
            t0 = time.time()
            while time.time() - t0 < timeout:
                res = self.getAutoZero(axes_)
                if np.all(res):
                    logging.info("AutoZero: Done.")
                    return True
            res = False
        return res

    @handle_exceptions
    def getAutoZero(self, axes=None):
        """Reports if AutoZero procedure was successful"""
        axes = self.transform_axes(axes)
        res = self.pi_device.qATZ(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def move(self, axes, values=None, wait=False, timeout=2):
        """Move szAxes  to  specified  absolute  positions."""
        # TODO: write universal transformation
        # import pdb; pdb.set_trace()
        axes_ = axes
        if values is None:
            return False
        axes = self.transform_axes(axes)

        pos = self.getPosition()

        for i in range(len(axes)):
            index = self.axes_transform_dict_inv[axes[i]]
            if isinstance(values, Iterable):
                if len(values) > 0:
                    pos[index] = values[i]
            else:
                pos[index] = values
        axes1 = self.transform_axes([0, 1, 2])
        self.target = pos
        pos = self.tiltCompensation.apply(pos)
        pos[pos < 0] = 0
        pos[pos > 100] = 100
        # print(axes1,pos,)
        self.pi_device.MOV(axes1, values=pos.tolist())
        res = True
        if wait:
            t0 = time.time()
            while time.time() - t0 < timeout:
                res = self.IsMoving(axes_)
                res1 = self.getOnTarget()
                if not np.any(res) and np.all(res1):
                    return True
            res = False
        return res

    @handle_exceptions
    def move_by(self, axes, values=None, wait=False, timeout=2):
        """Move szAxes  to  specified  absolute  positions."""
        # TODO: write universal transformation
        # import pdb; pdb.set_trace()
        axes_ = axes
        if values is None:
            return False
        axes = self.transform_axes(axes)

        pos = self.getPosition()
        pos[:] = 0
        for i in range(len(axes)):
            index = self.axes_transform_dict_inv[axes[i]]
            if isinstance(values, Iterable):
                if len(values) > 0:
                    pos[index] = values[i]
            else:
                pos[index] = values
        axes1 = self.transform_axes([0, 1, 2])
        self.target = pos
        #pos = self.tiltCompensation.apply(pos)
        #pos[pos < 0] = 0
        #pos[pos > 100] = 100
        # print(axes1,pos,)
        print(axes, axes1,pos)
        self.pi_device.MVR(axes1, values=pos.tolist())
        res = True
        if wait:
            t0 = time.time()
            while time.time() - t0 < timeout:
                res = self.IsMoving(axes_)
                res1 = self.getOnTarget()
                if not np.any(res) and np.all(res1):
                    return True
            res = False
        return res


    @handle_exceptions
    def IsMoving(self, axes=None):
        """Check if axes are moving"""
        axes = self.transform_axes(axes)
        res = self.pi_device.IsMoving(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def getPosition(self, axes=None):
        """Get Real Position"""
        if not self.isAlive():
            return np.array([np.nan] * 3)
        axes = self.transform_axes(axes)
        res = self.pi_device.qPOS(axes)
        res = self.tiltCompensation_inv.apply(np.array(list(res.values())))
        return res

    @handle_exceptions
    def getTargetPosition(self, axes=None):
        """Get Real Position"""
        axes = self.transform_axes(axes)
        # res = self.pi_device.qCTV(axes)
        # res = self.tiltCompensation_inv.apply(np.array(list(res.values())))
        return self.target

    @handle_exceptions
    def getOnTarget(self, axes=None):
        """Get On Target State"""
        axes = self.transform_axes(axes)
        res = self.pi_device.qONT(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def setVelocity(self, axes=None, values=(1000, 1000, 1000)):
        """Set Closed-Loop Velocity"""
        axes = self.transform_axes(axes)
        res = self.pi_device.VEL(axes, values=values)
        return True

    @handle_exceptions
    def getVelocity(self, axes=None):
        """Set Closed-Loop Velocity"""
        axes = self.transform_axes(axes)
        res = self.pi_device.qVEL(axes)
        return np.array(list(res.values()))

    @handle_exceptions
    def getCycleTime(self):
        """Gets the current cycle time for running a defined motion profile."""
        res = self.pi_device.qSCT()
        return res

    def isAlive(self):
        return self.pi_device.connected

    @handle_exceptions
    def scan_random(self, pos, radius=1, N=1, stop_event=None):
        if N == 1:
            Pos = [pos]
        else:
            x, y, z = generate_random_sphere(radius, N)
            Pos = np.array([x, y, z]).T + pos
        all_pos = np.zeros((0, 3))
        for i in range(N):
            self.move([0, 1, 2], Pos[i], wait=True)
            all_pos = np.vstack((all_pos, self.getPosition()))
            if not stop_event is None:
                if stop_event.is_set():
                    break
        return all_pos.mean(axis=0)

    @handle_exceptions
    def initGridScanXY(
        self,
        rangeX=(0, 50),
        rangeY=(0, 50),
        scan_axis=0,
        N=100,
        rate=2,
        triggered=False,
    ):
        self.scanParam = {}

        self.scanParam["gridScanXY_done"] = False
        axes = self.transform_axes([0, 1])
        self.scanParam["axes"] = axes
        self.scanParam["triggered"] = triggered

        x, y, segment_length = gen_grid_2D(
            rangeX=rangeX, rangeY=rangeY, axis=scan_axis, N=N
        )
        # z = np.zeros(len(x))
        pos = np.array([x, y]).T
        self.scanParam["path"] = pos
        self.scanParam["segment_lengtht"] = segment_length

        vel = self.getVelocity(axes=[0, 1]).mean()
        self.scanParam["estimatedScanTime"] = segment_length.sum() / vel

        iWaveGeneratorIDs = [int(i) for i in axes]  # [1,3];		#% id of wave generator
        iWaveTableIDs = [1, 2]
        #% id of wave table
        if triggered:
            iStartMode = 2  #% start mode = 1 (start wave generator output synchronized by servo cycle)
        else:
            iStartMode = 1
        iNumCycles = 1
        #% number of wave generator cycles
        # ifrequencyOfWave				= len(pos);		#% frequency of wave

        iInterpolationType = 1
        #% interpolation between points, used if piTableRate > 1. 1 = linear interpolation
        iOffsetOfFirstPointInWaveTable = 0
        #% index of starting point of curve in segment
        iAddAppendWave_reset = "X"
        #% 0=clear wave table (1=add wavetable values, 2=append to existing wave table contents)
        iAddAppendWave_append = "&"
        #% 0=clear wave table (1=add wavetable values, 2=append to existing wave table contents)
        iTableRate = rate

        iOptions = [RecordOptions.ACTUAL_POSITION_2] * len(axes)
        iTriggerSources = [TriggerSources.DEFAULT_0] * len(axes)
        sTriggerOptions = ["0"] * len(axes)

        self.scanParam["iWaveGeneratorIDs"] = iWaveGeneratorIDs
        self.scanParam["iWaveTableIDs"] = iWaveTableIDs
        self.scanParam["iStartMode"] = iStartMode
        self.scanParam["iNumCycles"] = iNumCycles
        self.scanParam["iInterpolationType"] = iInterpolationType
        self.scanParam[
            "iOffsetOfFirstPointInWaveTable"
        ] = iOffsetOfFirstPointInWaveTable
        self.scanParam["iTableRate"] = iTableRate
        self.scanParam["iOptions"] = iOptions
        self.scanParam["iTriggerSources"] = iTriggerSources
        self.scanParam["sTriggerOptions"] = sTriggerOptions

        print("switch on servo mode")
        self.pi_device.SVO(axes, [1] * len(axes))

        self.pi_device.WGO(axes, [0] * len(axes))

        #% query servo cycle time
        PARAM_ServoUpdateTime = 234881536
        servoCycleTime = self.pi_device.qSPA(axes[0], PARAM_ServoUpdateTime)[axes[0]][
            PARAM_ServoUpdateTime
        ]
        self.scanParam["servoCycleTime"] = servoCycleTime
        self.scanParam["waveTable_dt"] = (
            self.scanParam["servoCycleTime"] * self.scanParam["iTableRate"]
        )
        #% calculate number of point in wavetable from given frequency
        # iNumberOfPoints = int (1.7/(servoCycleTime * ifrequencyOfWave));

        #% wavetable contains one segment
        # iSegmentLength = iNumberOfPoints;

        for i in range(len(pos) - 1):
            if i == 0:
                append = iAddAppendWave_reset
            else:
                append = iAddAppendWave_append
            for j in range(len(axes)):
                #% send wave table to controller
                self.pi_device.WAV_LIN(
                    iWaveTableIDs[j],
                    iOffsetOfFirstPointInWaveTable,
                    segment_length[i],
                    append,
                    int(segment_length[i] / 5),
                    pos[i + 1, j] - pos[i, j],
                    pos[i, j],
                    segment_length[i],
                )

        #% link wave table to wave generator
        self.pi_device.WSL(iWaveGeneratorIDs, iWaveTableIDs)
        #% set wave table rate
        self.pi_device.WTR(0, iTableRate, iInterpolationType)
        #% for this command the WaveGenerator ID of the E727 Controller has to be 0

        #% set wave generator output cycles
        self.pi_device.WGC(iWaveGeneratorIDs, [iNumCycles] * len(axes))
        print("Configuring data recorder...")
        self.pi_device.RTR(iTableRate)

        #% configure which data is recorded by the data recorder
        self.pi_device.DRC(iWaveTableIDs, axes, iOptions)

        #% configure which event triggers the data recorder (starts the data acquisition):
        self.pi_device.DRT(iWaveTableIDs, iTriggerSources, sTriggerOptions)

        #%% Perform move

        print("Move to start position")
        #% move to start position of sine-waveform
        #% !! Adjust start-position when changing waveform or assignment !!
        self.pi_device.MOV(axes, pos[0].tolist())

        #% wait for motion to stop
        while np.all(list(self.pi_device.IsMoving().values())):
            time.sleep(0.1)

        #% restart recording as soon as wave generator output starts running
        self.pi_device.WGR()

        #% start wave generator output, data recorder starts simultaneously
        print("Ready to start wavegenerator")
        return self.scanParam.copy()

    @handle_exceptions
    def startGridScanXY(self, timeout=10, stop_callback=None):
        self.softStartEvent.wait(timeout=timeout)
        t0 = time.time()
        self.pi_device.WGO(
            self.scanParam["iWaveGeneratorIDs"],
            [self.scanParam["iStartMode"]] * len(self.scanParam["axes"]),
        )

        #% wait for Wave Generator to finish
        ret = True
        while ret == True:
            ret = self.pi_device.IsGeneratorRunning(self.scanParam["iWaveGeneratorIDs"])
            # print(list(ret.values()),p.getPosition())
            ret = np.all(list(ret.values()))
            time.sleep(0.1)

        # stop generator
        self.pi_device.WGO(self.scanParam["axes"], [0] * len(self.scanParam["axes"]))
        if not stop_callback is None:
            stop_callback()
        t1 = time.time()
        print("dt", t1 - t0)

        #%% Read data from controller

        numberOfPoints = self.pi_device.qDRL(self.scanParam["iWaveTableIDs"])[
            self.scanParam["iWaveTableIDs"][0]
        ]

        #% % Set a predefined number of points you want to retrieve from controller.
        #% % Wait until controller has recorded this number of points. WARNING: By
        #% % setting numberOfPoints to a greater value than the controllers data
        #% % recorder length (see controller manual) you will create an infinite loop.
        #% numberOfPoints = 1000;
        #% while (PIdevice.qDRL(1) < numberOfPoints)
        #%	 pause(0.02);
        #%
        #% end
        # numberOfPoints=3001

        startPoint = 1
        print("Retrieving data from controller...")
        header = self.pi_device.qDRR(
            self.scanParam["iWaveTableIDs"], startPoint, numberOfPoints
        )

        print("Retrieving data finished.")

        while self.pi_device.bufstate is not True:
            time.sleep(0.1)
            print("readBuffer")
        # self.pi_device.SVO(axes, [0]*len(axes));

        print(header)

        data = np.array(self.pi_device.bufdata)
        numberOfPoints = len(data.T)
        t = np.linspace(
            0, numberOfPoints * self.scanParam["waveTable_dt"], numberOfPoints
        )
        data = np.vstack([t, data]).T
        print(data.shape)
        # servoCycleTime1 = self.getCycleTime()
        # print('servoCycleTime',self.scanParam['servoCycleTime'],servoCycleTime1)

        # import pdb; pdb.set_trace()

        self.scanParam["last_gridScanXY"] = {
            "path": self.scanParam["path"],
            "header": header,
            "data": data,
            "start": t0,
            "stop": t1,
            "dt": self.scanParam["waveTable_dt"],
        }
        self.scanParam["gridScanXY_done"] = True
        return self.scanParam["last_gridScanXY"]

    @handle_exceptions
    def gridScanXY(self, stop_callback=None, timeout=10):

        # self.initGridScanXY( rangeX=rangeX,  rangeY=rangeY, scan_axis=scan_axis, N=N, rate=rate)
        self.scan_thread = threading.Thread(
            target=self.startGridScanXY, args=(timeout, stop_callback)
        )
        self.scan_thread.start()

    @handle_exceptions
    def initGridScanXYZ(self, X, Y, Z, rate=2, triggered=False):
        self.scanParam = {}

        self.scanParam["gridScanXY_done"] = False
        axes = self.transform_axes([0, 1, 2])
        self.scanParam["axes"] = axes
        self.scanParam["triggered"] = triggered

        r = (np.diff(Z) ** 2 + np.diff(Y) ** 2 + np.diff(Z) ** 2) ** 0.5
        max_points = 2 ** 15
        segment_length = (r / r.sum() * max_points).astype(int)
        # z = np.zeros(len(x))
        pos = np.array([X, Y, Z]).T
        self.scanParam["path"] = pos
        self.scanParam["segment_lengtht"] = segment_length

        vel = self.getVelocity(axes=[0, 1, 2]).mean()
        self.scanParam["estimatedScanTime"] = segment_length.sum() / vel

        iWaveGeneratorIDs = [int(i) for i in axes]  # [1,3];		#% id of wave generator
        iWaveTableIDs = [1, 2, 3]
        #% id of wave table
        if triggered:
            iStartMode = 2  #% start mode = 1 (start wave generator output synchronized by servo cycle)
        else:
            iStartMode = 1
        iNumCycles = 1
        #% number of wave generator cycles
        # ifrequencyOfWave				= len(pos);		#% frequency of wave

        iInterpolationType = 1
        #% interpolation between points, used if piTableRate > 1. 1 = linear interpolation
        iOffsetOfFirstPointInWaveTable = 0
        #% index of starting point of curve in segment
        iAddAppendWave_reset = "X"
        #% 0=clear wave table (1=add wavetable values, 2=append to existing wave table contents)
        iAddAppendWave_append = "&"
        #% 0=clear wave table (1=add wavetable values, 2=append to existing wave table contents)
        iTableRate = rate

        iOptions = [RecordOptions.ACTUAL_POSITION_2] * len(axes)
        iTriggerSources = [TriggerSources.DEFAULT_0] * len(axes)
        sTriggerOptions = ["0"] * len(axes)

        self.scanParam["iWaveGeneratorIDs"] = iWaveGeneratorIDs
        self.scanParam["iWaveTableIDs"] = iWaveTableIDs
        self.scanParam["iStartMode"] = iStartMode
        self.scanParam["iNumCycles"] = iNumCycles
        self.scanParam["iInterpolationType"] = iInterpolationType
        self.scanParam[
            "iOffsetOfFirstPointInWaveTable"
        ] = iOffsetOfFirstPointInWaveTable
        self.scanParam["iTableRate"] = iTableRate
        self.scanParam["iOptions"] = iOptions
        self.scanParam["iTriggerSources"] = iTriggerSources
        self.scanParam["sTriggerOptions"] = sTriggerOptions

        print("switch on servo mode")
        self.pi_device.SVO(axes, [1] * len(axes))

        self.pi_device.WGO(axes, [0] * len(axes))

        #% query servo cycle time
        PARAM_ServoUpdateTime = 234881536
        servoCycleTime = self.pi_device.qSPA(axes[0], PARAM_ServoUpdateTime)[axes[0]][
            PARAM_ServoUpdateTime
        ]
        self.scanParam["servoCycleTime"] = servoCycleTime
        self.scanParam["waveTable_dt"] = (
            self.scanParam["servoCycleTime"] * self.scanParam["iTableRate"]
        )
        #% calculate number of point in wavetable from given frequency
        # iNumberOfPoints = int (1.7/(servoCycleTime * ifrequencyOfWave));

        #% wavetable contains one segment
        # iSegmentLength = iNumberOfPoints;

        for i in range(len(pos) - 1):
            if i == 0:
                append = iAddAppendWave_reset
            else:
                append = iAddAppendWave_append
            for j in range(len(axes)):
                #% send wave table to controller
                self.pi_device.WAV_LIN(
                    iWaveTableIDs[j],
                    iOffsetOfFirstPointInWaveTable,
                    segment_length[i],
                    append,
                    int(segment_length[i] / 5),
                    pos[i + 1, j] - pos[i, j],
                    pos[i, j],
                    segment_length[i],
                )

        #% link wave table to wave generator
        self.pi_device.WSL(iWaveGeneratorIDs, iWaveTableIDs)
        #% set wave table rate
        self.pi_device.WTR(0, iTableRate, iInterpolationType)
        #% for this command the WaveGenerator ID of the E727 Controller has to be 0

        #% set wave generator output cycles
        self.pi_device.WGC(iWaveGeneratorIDs, [iNumCycles] * len(axes))
        print("Configuring data recorder...")
        self.pi_device.RTR(iTableRate)

        #% configure which data is recorded by the data recorder
        self.pi_device.DRC(iWaveTableIDs, axes, iOptions)

        #% configure which event triggers the data recorder (starts the data acquisition):
        self.pi_device.DRT(iWaveTableIDs, iTriggerSources, sTriggerOptions)

        #%% Perform move

        print("Move to start position")
        #% move to start position of sine-waveform
        #% !! Adjust start-position when changing waveform or assignment !!
        self.pi_device.MOV(axes, pos[0].tolist())

        #% wait for motion to stop
        while np.all(list(self.pi_device.IsMoving().values())):
            time.sleep(0.1)

        #% restart recording as soon as wave generator output starts running
        self.pi_device.WGR()

        #% start wave generator output, data recorder starts simultaneously
        print("Ready to start wavegenerator")
        return self.scanParam.copy()

    @handle_exceptions
    def startGridScanXYZ(self, timeout=10, stop_callback=None):
        self.softStartEvent.wait(timeout=timeout)
        t0 = time.time()
        self.pi_device.WGO(
            self.scanParam["iWaveGeneratorIDs"],
            [self.scanParam["iStartMode"]] * len(self.scanParam["axes"]),
        )

        #% wait for Wave Generator to finish
        ret = True
        while ret == True:
            ret = self.pi_device.IsGeneratorRunning(self.scanParam["iWaveGeneratorIDs"])
            # print(list(ret.values()),p.getPosition())
            ret = np.all(list(ret.values()))
            time.sleep(0.1)

        # stop generator
        self.pi_device.WGO(self.scanParam["axes"], [0] * len(self.scanParam["axes"]))
        if not stop_callback is None:
            stop_callback()
        t1 = time.time()
        print("dt", t1 - t0)

        #%% Read data from controller

        numberOfPoints = self.pi_device.qDRL(self.scanParam["iWaveTableIDs"])[
            self.scanParam["iWaveTableIDs"][0]
        ]

        #% % Set a predefined number of points you want to retrieve from controller.
        #% % Wait until controller has recorded this number of points. WARNING: By
        #% % setting numberOfPoints to a greater value than the controllers data
        #% % recorder length (see controller manual) you will create an infinite loop.
        #% numberOfPoints = 1000;
        #% while (PIdevice.qDRL(1) < numberOfPoints)
        #%	 pause(0.02);
        #%
        #% end
        # numberOfPoints=3001

        startPoint = 1
        print("Retrieving data from controller...")
        header = self.pi_device.qDRR(
            self.scanParam["iWaveTableIDs"], startPoint, numberOfPoints
        )

        print("Retrieving data finished.")

        while self.pi_device.bufstate is not True:
            time.sleep(0.1)
            print("readBuffer")
        # self.pi_device.SVO(axes, [0]*len(axes));

        print(header)

        data = np.array(self.pi_device.bufdata)
        numberOfPoints = len(data.T)
        t = np.linspace(
            0, numberOfPoints * self.scanParam["waveTable_dt"], numberOfPoints
        )
        data = np.vstack([t, data]).T
        print(data.shape)
        # servoCycleTime1 = self.getCycleTime()
        # print('servoCycleTime',self.scanParam['servoCycleTime'],servoCycleTime1)

        # import pdb; pdb.set_trace()

        self.scanParam["last_gridScanXY"] = {
            "path": self.scanParam["path"],
            "header": header,
            "data": data,
            "start": t0,
            "stop": t1,
            "dt": self.scanParam["waveTable_dt"],
        }
        self.scanParam["gridScanXY_done"] = True
        return self.scanParam["last_gridScanXY"]

    @handle_exceptions
    def gridScanXYZ(self, stop_callback=None, timeout=10):

        # self.initGridScanXY( rangeX=rangeX,  rangeY=rangeY, scan_axis=scan_axis, N=N, rate=rate)
        self.scan_thread = threading.Thread(
            target=self.startGridScanXY, args=(timeout, stop_callback)
        )
        self.scan_thread.start()


def generate_random_sphere(r, N):
    phi = np.random.uniform(0, np.pi * 2, N)
    theta = np.random.uniform(0, np.pi, N)
    r = np.random.uniform(0, r, N)
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)
    return x, y, z


class GCSDevice_sim:
    connected = False
    pos = {"1": 0, "2": 0, "3": 0}
    vel = {"1": 100, "2": 100, "3": 100}
    axes = ["1", "2", "3"]
    path = {"1": 0, "2": 0}
    bufstate = True
    bufdata = None
    pos_ = np.array([0, 0, 0], dtype=np.float64)

    # store = exdir.File('demo_state.exdir','a')
    def __init__(self, name):
        print("sim")
        try:
            self.shm = shared_memory.SharedMemory(
                create=True, size=self.pos_.nbytes, name="GCSDevice_sim_pos"
            )
        except FileExistsError:
            self.shm = shared_memory.SharedMemory(
                create=False, size=self.pos_.nbytes, name="GCSDevice_sim_pos"
            )

    def ConnectUSB(self, sn):
        self.serial_number = sn
        self.connected = True

    def qSAI(self):
        return self.axes

    def SVO(self, axes, values):
        return

    def VEL(self, axes, values):
        for i, ax in enumerate(axes):
            self.vel[ax] = values[i]
        return True

    def qVEL(self, axes):
        res = {ax: self.vel[ax] for ax in axes}
        return res

    def MOV(self, axes, values):

        pos = self.pos

        for i, ax in enumerate(axes):
            # print(axes,values,i)
            pos[ax] = values[i]
        # print(pos, self.pos)
        d = np.array([abs(pos[ax] - self.pos[ax]) for ax in self.axes])
        v = np.array([self.vel[ax] for ax in self.axes])
        t = d / v
        # print('sleep:',t.sum())
        time.sleep(t.sum())
        return True

    def IsMoving(self, axes=("1", "2", "3")):
        res = {ax: False for ax in axes}
        return res

    def qONT(self, axes=("1", "2", "3")):
        res = {ax: True for ax in axes}
        return res

    def qPOS(self, axes):
        res = {
            ax: self.pos[ax] + np.random.normal(loc=0, scale=0.01, size=1)[0]
            for ax in axes
        }
        # np.save('piStagePos.npy',np.array(list(res.values())))
        self.pos_ = np.array(list(res.values()))
        # print(self.pos_,self.pos_.shape)
        b = np.ndarray(self.pos_.shape, dtype=self.pos_.dtype, buffer=self.shm.buf)
        b[:] = self.pos_
        # np.save(Path(self.storePath)/'position/piStage/data.npy',np.array(list(self.pos.values())))
        return res

    def qCTV(self, axes):
        res = {ax: self.pos[ax] for ax in axes}
        return res

    def WGO(self, axes, val):
        # res = {ax:self.pos[ax] for ax in axes}
        if np.sum(val) != 0:
            self.running = True
            self.running_counter = 0
        return

    def IsGeneratorRunning(self, genid):
        self.running_counter += 1000
        self.running = self.running_counter <= len(self.path["1"])
        res = {ax: self.running for ax in self.axes}
        return res

    def qDRL(self, iWaveTableIDs):
        res = {int(ax): 2 ** 15 for ax in self.axes}
        return res

    def qSCT(self):
        return 5e-5

    def qDRR(self, iWaveTableIDs, startPoint, numberOfPoints):
        import exdir
        from pathlib import Path

        s = exdir.File(Path("tmp/gridScanXY1604311927.exdir/"))
        x0, y0 = self.bufdata
        x, y = s["raw"].data.T
        x = x / x.max() * np.ptp(x0) + x0.min()
        y = y / y.max() * np.ptp(y0) + y0.min()
        self.bufdata = [x, y]
        return {}

    def qSPA(self, axes, param=234881536):
        # PARAM_ServoUpdateTime = 234881536;
        r = {ax: {param: 5e-5} for ax in self.axes}
        # servoCycleTime = PIdevice.qSPA(axes[0],PARAM_ServoUpdateTime)[axes[0]][PARAM_ServoUpdateTime];

        # res = {ax:self.pos[ax] for ax in axes}
        return r

    def WSL(self, iWaveGeneratorIDs, iWaveTableIDs):
        pass

    def WTR(
        self, zero, iTableRate, iInterpolationType
    ):  #% for this command the WaveGenerator ID of the E727 Controller has to be 0
        pass

    def WGC(self, iWaveGeneratorIDs, cycles):
        pass

    def RTR(self, tableid):
        pass

    def DRC(self, iWaveTableIDs, axes, iOptions):
        pass

    def DRT(self, iWaveTableIDs, iTriggerSources, sTriggerOptions):
        pass

    def WGR(self):
        pass

    def WAV_LIN(
        self,
        iWaveTableID,
        iOffsetOfFirstPointInWaveTable,
        segment_length1,
        append,
        slowdown,
        dx,
        x0,
        segment_length,
    ):
        # print(iWaveTableID, iOffsetOfFirstPointInWaveTable,
        # 	segment_length1,append, slowdown,dx,x0,segment_length,self.path.keys())
        if append == "X":
            self.path[str(iWaveTableID)] = np.linspace(x0, x0 + dx, segment_length)
        else:
            self.path[str(iWaveTableID)] = np.hstack(
                [self.path[str(iWaveTableID)], np.linspace(x0, x0 + dx, segment_length)]
            )
        self.bufdata = [self.path[ax] for ax in ["1", "2"]]

    def close(self):
        self.connected = False
        return True


if __name__ == "__main__":
    from matplotlib import pyplot as plt

    p = E727()
    p.connect()
    p.autoZero(wait=True)
    time.sleep(2)
    p.setVelocity([0, 1, 2], (5000, 5000, 5000))
    """
	x, y = gen_grid_2D(rangeX=(0,50),rangeY=(0,50), axis=0, N=100)
	z = np.zeros(len(x))
	pos = np.array([x,y,z]).T



	iWaveGeneratorIDs = [1,2,3];		#% id of wave generator
	iWaveTableIDs = [1,2,3];		#% id of wave table
	iStartMode = 1		#% start mode = 1 (start wave generator output synchronized by servo cycle)
	iNumCycles					  = 1;		#% number of wave generator cycles
	ifrequencyOfWave				= len(pos);		#% frequency of wave

	iInterpolationType			  = 1;		#% interpolation between points, used if piTableRate > 1. 1 = linear interpolation
	iOffsetOfFirstPointInWaveTable	 = 0;		#% index of starting point of curve in segment
	iAddAppendWave_reset					 = 'X';		#% 0=clear wave table (1=add wavetable values, 2=append to existing wave table contents)
	iAddAppendWave_append					 = '&';		#% 0=clear wave table (1=add wavetable values, 2=append to existing wave table contents)

	axes = ['1','2','3'];
	iTableRate  = 2;
	iOptions = [RecordOptions.ACTUAL_POSITION_2]*len(axes)
	iTriggerSources = [TriggerSources.DEFAULT_0]*len(axes)
	sTriggerOptions = ['0','0','0']


	print('switch on servo mode');
	p.pi_device.SVO(axes, [1]*len(axes));

	p.pi_device.WGO(axes, [0]*len(axes));

	PIdevice = p.pi_device



	#% query servo cycle time
	PARAM_ServoUpdateTime = 234881536;
	servoCycleTime = PIdevice.qSPA(axes[0],PARAM_ServoUpdateTime)[axes[0]][PARAM_ServoUpdateTime];

	#% calculate number of point in wavetable from given frequency
	iNumberOfPoints = int (1.7/(servoCycleTime * ifrequencyOfWave));

	#% wavetable contains one segment
	iSegmentLength = iNumberOfPoints;



	# pos = np.array([
	# 	[0,  10,1],
	# 	[0,	0,5],
	# 	[1,	0,3],
	# 	[1,	5,3],
	# 	[1.1, 5,3],
	# 	[1.1, 0,3],
	# 	[2,	0,3],
	# 	[2,  10,3],
	# 	[2.1,10,3],
	# 	[2.1, 0,3],
	# 	[0,	0,3],
	# 	])


	for i in range(len(pos)-1):
		if i == 0:
			append = iAddAppendWave_reset
		else:
			append = iAddAppendWave_append
		for j in range(3):
			#% send wave table to controller
			PIdevice.WAV_LIN(iWaveTableIDs[j],
								iOffsetOfFirstPointInWaveTable,
								iNumberOfPoints,
								append,
								int(iNumberOfPoints/5),
								pos[i+1,j] - pos[i,j],
								pos[i,j],
								iNumberOfPoints);

	#% link wave table to wave generator
	PIdevice.WSL(iWaveGeneratorIDs, iWaveTableIDs);


	#% set wave table rate
	PIdevice.WTR (0, iTableRate, iInterpolationType);	 #% for this command the WaveGenerator ID of the E727 Controller has to be 0

	#% set wave generator output cycles
	PIdevice.WGC(iWaveGeneratorIDs, [iNumCycles]*len(axes));




	print('Configuring data recorder...');
	PIdevice.RTR( iTableRate );

	#% configure which data is recorded by the data recorder
	PIdevice.DRC ( iWaveTableIDs, axes, iOptions );

	#% configure which event triggers the data recorder (starts the data acquisition):
	PIdevice.DRT ( iWaveTableIDs, iTriggerSources, sTriggerOptions );

	#%% Perform move

	print('Move to start position');
	#% move to start position of sine-waveform
	#% !! Adjust start-position when changing waveform or assignment !!
	PIdevice.MOV(axes, pos[0].tolist());
	#% wait for motion to stop
	while np.all(list(PIdevice.IsMoving().values())):
		time.sleep(0.1);


	#% restart recording as soon as wave generator output starts running
	PIdevice.WGR ();

	#% start wave generator output, data recorder starts simultaneously
	print('Start wavegenerator');
	t0=time.time()
	PIdevice.WGO(iWaveGeneratorIDs, [iStartMode]*len(axes));

	#% wait for Wave Generator to finish
	ret = True;
	while ( (ret == True)):
		ret = PIdevice.IsGeneratorRunning(iWaveGeneratorIDs);
		#print(list(ret.values()),p.getPosition())
		ret = np.all(list(ret.values()))

		time.sleep(0.1);


	print('done')






	p.pi_device.WGO(axes, [0]*len(axes));
	print('dt',time.time()-t0)





	#%% Read data from controller


	numberOfPoints  = PIdevice.qDRL ( iWaveTableIDs )[iWaveTableIDs[0]];


	#% % Set a predefined number of points you want to retrieve from controller.
	#% % Wait until controller has recorded this number of points. WARNING: By
	#% % setting numberOfPoints to a greater value than the controllers data
	#% % recorder length (see controller manual) you will create an infinite loop.
	#% numberOfPoints = 1000;
	#% while (PIdevice.qDRL(1) < numberOfPoints)
	#%	 pause(0.02);
	#%
	#% end
	#numberOfPoints=3001

	startPoint	  = 1;
	print('Retrieving data from controller...');
	header = PIdevice.qDRR ( iWaveTableIDs, startPoint, numberOfPoints );

	print('Retrieving data finished.');

	while PIdevice.bufstate is not True:
		time.sleep(0.1)
	p.pi_device.SVO(axes, [0]*len(axes));

	print(header)
	d = np.array(PIdevice.bufdata)
	print(d.shape)
	"""

    def stop_callback():
        print("External readout stop_callback")

    p.gridScanXY(
        rangeX=(0, 50), rangeY=(0, 50), scan_axis=0, N=50, stop_callback=stop_callback
    )
    p.softStartEvent.set()
    while not p.gridScanXY_done:
        time.sleep(0.1)
    t, x, y, header, data, t0, t1 = p.last_gridScanXY
    from matplotlib import pyplot as plt

    plt.figure(1)
    plt.plot(t, data[0], ".", label="0")
    plt.plot(t, data[1], ".", label="1")
    # plt.plot(d[2],'.',label='2')
    plt.plot(x, "-", label="-0")
    plt.plot(y, "-", label="-1")
    # plt.plot(z,'-',label='-2')

    plt.figure(2)
    plt.plot(data[0], data[1], ".", label="0")
    plt.plot(x, y, "-r", label="t")
    plt.legend()
    plt.show()
    import exdir
    from pathlib import Path

    s = exdir.File(Path("tmp/gridScanXY" + str(int(time.time())) + ".exdir"))
    path = s.create_dataset("path", data=np.transpose([x, y]))
    d = s.create_dataset("raw", data=data.T)
    d.attrs["header"] = header
    d.attrs["start"] = t0
    d.attrs["end"] = t1
