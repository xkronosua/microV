# from serialManager import ResourceManager
import time
import logging
import serial
from serial.tools import list_ports
from binascii import unhexlify, hexlify
import struct
import numpy as np
import threading
import traceback
import re

# import typing
import microscope
import microscope.abc


_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)

STATUS_ERROR_CODES = {
    0: "OK, no error",
    1: "Communication time out",
    2: "Mechanical time out",
    3: "Command error or not supported",
    4: "Value out of range",
    5: "Module isolated",
    6: "Module out of isolation",
    7: "Initializing error",
    8: "Thermal error",
    9: "Busy",
    10: "Sensor Error(May appear during self test. If code persists there is an error)",
    11: "Motor Error(May appear during self test. If code persists there is an error)",
    12: "Out of Range(e.g. stage has been instructed to move beyond its travel range).",
    13: "Over Current error",
}


class _ELLx(microscope.abc.SerialDeviceMixin, microscope.abc.StageAxis):

    demo = False
    connection = None
    stepsPerUnit = 1

    def __init__(
        self,
        connection=None,
        address=b"0",
        limits=microscope.AxisLimits(0, 10),
        stepsPerUnit=1,
        demo=False,
        **kwargs,
    ):

        super().__init__(**kwargs)
        self._limits = limits
        self.stepsPerUnit = stepsPerUnit
        self.address = address
        self.connection = connection

    def send(self, command: bytes) -> bytes:
        """Send command and retrieve response."""
        success = False
        for i in range(2000):
            if success: break
            #while not success:
            self._write(command)
            response = self._readline()
            # Catch zero-length responses to queries and retry.
            if len(response) > 0:
                success = True
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def send_locked(self, command: bytes) -> bytes:
        response = self.send(command)
        return response

    def _write(self, command: bytes, ending=b"\r\n") -> int:
        """Write a command."""
        response = self.connection.write(command + ending)
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _write_locked(self, command: bytes, ending=b"\r\n") -> int:
        """Write a command with lock."""
        response = self._write(command, ending=ending)
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _readline(self) -> bytes:
        """Read line with lock."""
        response = self.connection.readline()
        return response

    def initialize(self):
        pass
        # try:
        # self.connection.open()
        # self.connection.isOpen()
        # except Exception as ex:
        # _logger.error("Can't connect to ELLx stage", exc_info=ex)
        # return False

    def is_alive(self):
        try:
            self.connection.inWaiting()
            return True
        except:
            return False

    def _flush_handshake(self):
        self.connection.readline()

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_info(self):
        self.connection.reset_output_buffer()
        self._write(self.address + b"in")
        success = False
        info = None
        for i in range(1000):
            if success: break
            self._write(self.address + b"in")
            response = self._readline()
            # Catch zero-length responses to queries and retry.
            if len(response) > 0:
                m = re.search(self.address + b"IN(.*)\r", response)
                if m:
                    r = self.address + b"IN" + m.groups()[0]
                    # r = val.split(b'\r')[0]
                    if len(r) == 33:
                        # r = unhexlify(resp)
                        info = {}
                        info["ELL"] = struct.unpack(">b", unhexlify(r[3:5]))[0]
                        info["SN"] = r[5:13].decode()
                        info["Year"] = r[13:17].decode()
                        info["FW rel"] = r[17:19].decode()
                        info["HW rel"] = r[19:21].decode()
                        info["TRAVEL mm/deg"] = struct.unpack(
                            ">h", unhexlify(r[21:25])
                        )[0]
                        info["PULSES/M.U."] = struct.unpack(">l", unhexlify(r[25:33]))[
                            0
                        ]

                        success = True
        return info

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_status(self):
        self.connection.reset_output_buffer()
        # self._write(self.address+b"gs")
        success = False
        status_code = -1

        for i in range(1000):
            if success: break
            self._write(self.address + b"gs")
            response = self._readline()
            # Catch zero-length responses to queries and retry.
            if len(response) > 0:
                m = re.search(self.address + b"GS(.*)\r", response)
                if m:
                    r = m.groups()[0]
                    status_code = struct.unpack(">b", unhexlify(r))[0]
                    success = True
        return status_code

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_position(self):
        """Return the stage's position."""
        # self.connection.reset_output_buffer()
        # self._write(self.address+b"gp")
        success = False
        position = None

        for i in range(1000):
            if success: break
            self._write(self.address + b"gp")
            response = self._readline()
            # Catch zero-length responses to queries and retry.
            if len(response) > 0:
                m = re.search(self.address + b"PO(.*)\r", response)
                if m:
                    r = m.groups()[0]
                    position = struct.unpack(">l", unhexlify(r))[0]
                    success = True
        return position

    def is_moving(self) -> bool:
        status_code = self.get_status()
        return status_code == 9  # Busy

    @property
    def position(self):
        return self.get_position() / self.stepsPerUnit

    @property
    def limits(self) -> microscope.AxisLimits:
        return self._limits

    def move_to(self, target=None, wait=False, direction=0):
        """Move to target"""
        # The default position set points are zero. If the motors are started without
        # writing to a target, that motor will move to zero, so only start motors
        # if a target has been provided.

        if target is None:
            self._write_locked(self.address + b"ho" + str(direction).encode())
        else:
            if target < self._limits.lower:
                target = self._limits.lower
            elif target > self._limits.upper:
                target = self._limits.upper

            target = int(target * self.stepsPerUnit)
            target_b = hexlify(struct.pack(">l", target)).upper()
            self._write_locked(self.address + b"ma" + target_b)

        if wait:
            for i in range(100):
                if not self.is_moving():
                    break
                time.sleep(0.1)
        status = self.get_status()
        return status

    def move_by(self, delta, wait=False):
        delta = int(delta * self.stepsPerUnit)
        delta_b = hexlify(struct.pack(">l", delta)).upper()
        self._write_locked(self.address + b"mr" + delta_b)

        if wait:
            for i in range(100):
                if not self.is_moving():
                    break
                time.sleep(0.1)
        status = self.get_status()
        return status


class ELLx_bus(microscope.abc.Stage):
    """ELLx bus controller for multiple devices."""

    connection = serial.Serial()
    serial_number = None
    initialized = False

    def __init__(
        self,
        port=None,
        baudrate=9600,
        serial_number="DT0424NN",  # DO02FRYTA
        timeout=0.1,
        axConfig=None,
        demo=False,
        **kwargs,
    ) -> None:
        super().__init__(**kwargs)
        self.port = port
        self.serial_number = serial_number
        self.baudrate = baudrate
        if port is None:
            ports = list(list_ports.grep("0403:6015"))
            for p in ports:
                if p.serial_number == serial_number:
                    port = p.device
                    break

        self.demo = demo
        if self.demo:
            # port = './ttyclient_ELLx'
            # baudrate = 9600
            self.connection = SerialDummy()
        else:
            self.connection = serial.Serial()
            self.connection.port = port
            self.connection.baudrate = baudrate
            self.connection.timeout = timeout
            self.connection.stopbits = serial.STOPBITS_ONE
            self.connection.bytesize = serial.EIGHTBITS
            self.connection.parity = serial.PARITY_NONE

        if axConfig is None:
            axConfig = {
                "filters": {"address": b"0", "limits": (0, 3), "stepsPerUnit": 31},
                "HWP": {
                    "address": b"1",
                    "limits": (0, 720),
                    "stepsPerUnit": int(143360 / 360),
                },
            }
        _axes = {}
        for name in axConfig:
            _axes[name] = _ELLx(
                connection=self.connection,
                address=axConfig[name]["address"],
                limits=microscope.AxisLimits(*axConfig[name]["limits"]),
                stepsPerUnit=axConfig[name]["stepsPerUnit"],
                demo=demo,
            )
        self._axes = _axes

    def initialize(self) -> bool:
        if not self.initialized:
            res = self._initialize()
            if res:
                self.initialized = True

    def _initialize(self):

        if self.port is None:
            ports = list(list_ports.grep("0403:6015"))
            for p in ports:
                if p.serial_number == self.serial_number:
                    self.connection.port = p.device
                    break
        try:
            self.connection.open()
            return True
        except Exception as ex:
            _logger.error(f"Can't connect to ELLx bus{ex}")
            return False

    def _on_shutdown(self) -> None:
        if not self.connection is None:
            self.connection.close()
        self.initialized = False

    def _do_shutdown(self) -> None:
        if not self.connection is None:
            self.connection.close()
        self.initialized = False

    @property
    def axes(self):
        return self._axes

    def move_by(self, delta) -> float:
        for name, rpos in delta.items():
            self.axes[name].move_by(rpos)

    def move_to(self, position) -> float:
        for name, pos in position.items():
            self.axes[name].move_to(pos)

    def is_alive(self):
        try:
            self.connection.inWaiting()
            return True
        except:
            self.initialized = False
            return False


class SerialDummy(object):

    alive = False
    line = b""
    response = b""

    def __init__(self):
        pass

    def open(self):
        self.alive = True
        t = threading.Thread(target=self.process, daemon=True)
        t.start()
        return True

    def flushInput(self):
        pass  # self.line = b''

    def flushOutput(self):
        pass  # self.response = b''

    def reset_output_buffer(self):
        pass  # self.response = b''

    def is_alive(self):
        return self.alive

    def isOpen(self):
        return self.alive

    def inWaiting(self):
        return True

    def close(self):
        self.alive = False

    def process(self):
        current_pos = 0
        stepsPerUnit = 1 / 5e-5
        vel = int(10 * stepsPerUnit)
        dx = int(0.1 * stepsPerUnit)
        target = 0
        stage_address = b"0"
        status = stage_address + b"S0:ff ff ff ff ff ff ff\r\n"

        while self.alive:
            # print(self.line)
            if self.line != b"":
                print(self.line)
                line = self.line.split(b"\n")[0]
                self.line = b"\n".join(self.line.split(b"\n")[1:])

                line = line.split(b"\r")[0]
                self.line = b""
                command = line[1:3]

                # print('raw: |',parts,'|')
                if len(line) == 0:
                    # print(line)
                    # print(params)
                    time.sleep(0.1)
                    continue
                stage_address = line[:1]
                value = line[3:]
                if command == b"ma":
                    current_pos = struct.unpack(">l", unhexlify(value))[0]
                    p = (
                        stage_address
                        + b"PO"
                        + hexlify(struct.pack(">l", current_pos))
                        + b"\r\n"
                    )
                    self.response += p

                elif command == b"mr":
                    current_pos += struct.unpack(">l", unhexlify(value))[0]
                    p = (
                        stage_address
                        + b"PO"
                        + hexlify(struct.pack(">l", current_pos))
                        + b"\r\n"
                    )
                    self.response += p

                elif command == b"gp":
                    p = (
                        stage_address
                        + b"PO"
                        + hexlify(struct.pack(">l", current_pos))
                        + b"\r\n"
                    )
                    self.response += p

                elif command == b"gs":
                    p = stage_address + b"GS00\r\n"
                    self.response += p

                elif command == b"in":
                    p = stage_address + b"IN061234567820150181001F00000001\r\n"
                    self.response += p

                elif b"ho" in line:
                    current_pos = 0
                    p = (
                        stage_address
                        + b"PO"
                        + hexlify(struct.pack(">l", current_pos))
                        + b"\r\n"
                    )
                    self.response += p

                else:
                    pass
                time.sleep(0.1)
            time.sleep(0.01)

    def write(self, line):
        self.line += line

        # print(self.line)

    def readline(self):
        for i in range(10):
            if self.response == b"":
                time.sleep(0.02)
            else:
                break

        resp = self.response

        self.response = b"\n".join(self.response.split(b"\n")[1:])
        return resp


if __name__ == "__main__":

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    demo = False
    import sys

    if "sim" in sys.argv:
        demo = True

    m = MOCO(demo=demo)
    m.connect()

    print("Position:", m.getPosition())
    print("Status:", m.getStatus())
    print(m.calibr(True))
    print("abs")
    # time.sleep(2)
    t0 = time.time()
    print(m.moveAbs_mm(100, True), time.time() - t0)
    print("abs")
    #
    # print(m.moveAbs(100000,True))
    # print('rel')
    # print(m.moveRel(100000,True))
    # print('calibr')
    # print(m.calibr(True))
    # print('done')
    # m.disconnect()
