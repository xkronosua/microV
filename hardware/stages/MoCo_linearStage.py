# from serialManager import ResourceManager
import time
import logging
import serial
from serial.tools import list_ports
from binascii import unhexlify, hexlify
import struct
import numpy as np
import threading
import traceback

import microscope
import microscope.abc

import re

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class _MoCo(microscope.abc.SerialDeviceMixin, microscope.abc.StageAxis):
    status_default = {
        "Move Status": None,
        "Moving": None,
        "Pos-err": None,
        "Motor-Off": None,
        "Limit Switch": None,
        "Limit reverse": None,
        "Limit Forward": None,
        "Error Number": None,
    }

    demo = False
    connection = None
    stepsPerUnit = 1

    def __init__(
        self,
        connection=None,
        address=b"0",
        limits=microscope.AxisLimits(0, 10),
        stepsPerUnit=1,
        demo=False,
        **kwargs,
    ):

        super().__init__(**kwargs)
        self._limits = limits
        self.stepsPerUnit = stepsPerUnit
        self.address = address
        self.connection = connection

    def send(self, command: bytes) -> bytes:
        """Send command and retrieve response."""
        success = False
        while not success:
            self._write(command)
            response = self._readline()
            # Catch zero-length responses to queries and retry.
            if len(response) > 0:
                success = True
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def send_locked(self, command: bytes) -> bytes:
        response = self.send(command)
        return response

    def _write(self, command: bytes, ending=b"\r\n") -> int:
        """Write a command."""
        response = self.connection.write(command + ending)
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _write_locked(self, command: bytes, ending=b"\r\n") -> int:
        """Write a command with lock."""
        response = self._write(command, ending=ending)
        return response

    @microscope.abc.SerialDeviceMixin.lock_comms
    def _readline(self) -> bytes:
        """Read line with lock."""
        response = self.connection.readline()
        return response

    def _flush_handshake(self):
        self.connection.readline()

    def is_moving(self) -> bool:
        status = self.get_status()
        return status["Moving"]

    def initialize(self):

        r = self.send_locked(b"*IDN?")

    def is_alive(self):
        try:
            self.connection.inWaiting()
            return True
        except:
            return False

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_position(self):
        """Return the stage's position."""
        self.connection.reset_output_buffer()
        success = False
        position = None
        while not success:
            self._write(b"'", ending=b"")
            response = self._readline()
            # print(response)
            # Catch zero-length responses to queries and retry.
            if len(response) > 0:
                m = re.search(self.address + b"P0:(.*)\r", response)
                if m:
                    r = m.groups()[0]
                    position = int(r)
                    success = True
        return position

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_status(self):
        self.connection.reset_output_buffer()
        success = False
        status = self.status_default

        while not success:
            self._write(b"%", ending=b"")
            response = self._readline()
            # Catch zero-length responses to queries and retry.
            # Catch zero-length responses to queries and retry.
            if len(response) > 0:
                m = re.search(self.address + b"S0:(.*)\r", response)
                if m:
                    success = True
                    r = m.groups()[0].split(b" ")
                    r = [int(rr, 16) for rr in r]
                    """
					Move Status(LM629)
					Report Operational Flags1
					Report Operational Flags2
					Report Motion Flags
					report Limit Switch Flags
					Live Status Limit Switch
					Error Number
					"""
                    status["Move Status"] = r[0]
                    status["Moving"] = "{:08b}".format(r[0])[5] != "1"
                    status["Pos-err"] = "{:08b}".format(r[0])[2] == "1"
                    status["Motor-Off"] = "{:08b}".format(r[0])[0] == "1"

                    status["Limit Switch"] = r[5]
                    status["Limit reverse"] = "{:08b}".format(r[5])[7] == "1"
                    status["Limit Forward"] = "{:08b}".format(r[5])[5] == "1"
                    status["Error Number"] = r[6]
        return status

    @property
    def position(self):
        return self.get_position() / self.stepsPerUnit

    @property
    def limits(self) -> microscope.AxisLimits:
        return self._limits

    def move_to(self, target=None, wait=False):
        """Move to target"""
        # The default position set points are zero. If the motors are started without
        # writing to a target, that motor will move to zero, so only start motors
        # if a target has been provided.
        if target is None:
            self._write_locked(b"mc2")
        else:
            if target < self._limits.lower:
                target = self._limits.lower
            elif target > self._limits.upper:
                target = self._limits.upper

            target = int(target * self.stepsPerUnit)
            self._write_locked(b"ma" + str(target).encode())

        if wait:
            for i in range(200):
                if not self.is_moving():
                    break
                time.sleep(0.1)
        status = self.get_status()
        return status

    def move_by(self, delta, wait=False):
        delta = int(delta * self.stepsPerUnit)
        self._write_locked(b"mr" + str(delta).encode())

        if wait:
            for i in range(200):
                if not self.is_moving():
                    break
                time.sleep(0.1)
        status = self.get_status()
        return status

    def stop(self):
        self._write_locked(b"ab1")
        status = self.get_status()
        return status


class MoCo_linearStage(microscope.abc.Stage):
    """MoCo dc controller for PI linear stage. Units - mm"""

    connection = serial.Serial()
    serial_number = None
    initialized = False

    def __init__(
        self,
        port=None,
        baudrate=9600,
        serial_number="FTZ9WDMZA",
        timeout=0.1,
        axConfig=None,
        demo=False,
        **kwargs,
    ) -> None:
        super().__init__(**kwargs)
        self.port = port
        self.serial_number = serial_number
        self.baudrate = baudrate
        if port is None:
            ports = list(list_ports.grep("0403:6015"))
            for p in ports:
                if p.serial_number == serial_number:
                    port = p.device
                    break

        self.demo = demo
        if self.demo:
            # port = './ttyclient_MoCo'
            # baudrate = 9600
            self.connection = SerialDummy()
        else:
            self.connection = serial.Serial()
            self.connection.port = port
            self.connection.baudrate = baudrate
            self.connection.timeout = timeout
            self.connection.stopbits = serial.STOPBITS_ONE
            self.connection.bytesize = serial.EIGHTBITS
            self.connection.parity = serial.PARITY_NONE
            self.connection.set_buffer_size(rx_size=8192, tx_size=None)
            self.connection.xonxoff = True

        if axConfig is None:
            axConfig = {
                "delay": {"address": b"0", "limits": (0, 100), "stepsPerUnit": 1 / 5e-5}
            }
        _axes = {}
        for name in axConfig:
            _axes[name] = _MoCo(
                connection=self.connection,
                address=axConfig[name]["address"],
                limits=microscope.AxisLimits(*axConfig[name]["limits"]),
                stepsPerUnit=axConfig[name]["stepsPerUnit"],
                demo=demo,
            )
        self._axes = _axes

    def initialize(self) -> bool:
        if not self.initialized:
            res = self._initialize()
            if res:
                self.initialized = True

    def _initialize(self):

        if self.port is None:
            ports = list(list_ports.grep("0403:6015"))
            for p in ports:
                if p.serial_number == self.serial_number:
                    self.connection.port = p.device
                    break

        if self.demo:
            self.connection.port = "./ttyclient_MoCo"
            baudrate = 9600
        try:
            self.connection.open()
            self.connection.isOpen()
            return True

        except Exception as ex:
            _logger.error(f"Can't connect to MoCo")  # , exc_info=ex)
            return False

    def _on_shutdown(self) -> None:
        if not self.connection is None:
            self.connection.close()
        self.initialized = False

    def _do_shutdown(self) -> None:
        if not self.connection is None:
            self.connection.close()
        self.initialized = False

    @property
    def axes(self):
        return self._axes

    def move_by(self, delta) -> float:
        for name, rpos in delta.items():
            self.axes[name].move_by(rpos)

    def move_to(self, position) -> float:
        for name, pos in position.items():
            self.axes[name].move_to(pos)

    def is_alive(self):
        try:
            self.connection.inWaiting()
            return True
        except:
            self.initialized = False

            return False


class SerialDummy(object):

    alive = False
    line = b""
    response = b""

    def __init__(self):
        pass

    def open(self):
        self.alive = True
        t = threading.Thread(target=self.process, daemon=True)
        t.start()
        return True

    def flushInput(self):
        pass  # self.line = b''

    def flushOutput(self):
        pass  # self.response = b''

    def reset_output_buffer(self):
        pass  # self.response = b''

    def is_alive(self):
        return self.alive

    def isOpen(self):
        return self.alive

    def inWaiting(self):
        return True

    def close(self):
        self.alive = False

    def process(self):
        current_pos = 0
        stepsPerUnit = 1 / 5e-5
        vel = int(10 * stepsPerUnit)
        dx = int(0.1 * stepsPerUnit)
        target = 0
        stage_address = b"0"
        status = stage_address + b"S0:ff ff ff ff ff ff ff\r\n"

        while self.alive:
            # print(self.line)
            if self.line != b"":
                print(self.line)

                line = self.line.split(b"\n")[0]
                self.line = (
                    b"\n".join(self.line.split(b"\n")[1:])
                    .replace(b"%", b"")
                    .replace(b"'", b"")
                )
                line = line.split(b"\r")[0]
                # print('line:',line)
                if len(line) == 0:
                    pass

                if len(line) == 1:
                    p = b""

                    if line == b"'":
                        p = stage_address + b"P0:" + str(current_pos).encode() + b"\r\n"

                    elif line == b"%":
                        p = status

                    if p:
                        print(line, p)
                        self.response += p
                else:
                    command = line[0:2]
                    value = line[2:]
                    print(command)

                    if command == b"ma":
                        target = int(value)

                    elif command == b"mr":
                        target = current_pos + int(value)

                    elif b"mc" in line:
                        target = 0
                        p = stage_address + b"P0:" + str(current_pos).encode() + b"\r\n"
                        self.response += p
                    else:
                        pass

            if abs(current_pos - target) >= dx:

                dt = dx * vel
                direction = (target - current_pos) / abs(target - current_pos)
                current_pos += int(dx * direction)
                status = stage_address + b"S0:AA AA AA AA AA AA AA\r\n"
            else:
                status = stage_address + b"S0:ff ff ff ff ff ff ff\r\n"
                current_pos = int(target)
            # print(current_pos,target)
            time.sleep(0.01)

    def write(self, line):
        self.line += line

        # print(self.line)

    def readline(self):
        for i in range(10):
            if self.response == b"":
                time.sleep(0.02)
            else:
                break

        resp = self.response

        self.response = b"\n".join(self.response.split(b"\n")[1:])
        return resp


if __name__ == "__main__":
    import sys

    demo = False
    if "sim" in sys.argv:
        demo = True
    stage = MoCo_linearStage(demo=demo)
    stage.initialize()
    print(stage.axes["delay"].position)
    target = 100
    stage.axes["delay"].move_to(target)
    print(stage.axes["delay"].position)
    for i in range(100):
        time.sleep(0.5)
        pos = stage.axes["delay"].position
        print(pos)
        if pos == target:
            break

    target = 0
    stage.axes["delay"].move_to(target)
    print(stage.axes["delay"].position)
    for i in range(100):
        time.sleep(0.5)
        pos = stage.axes["delay"].position
        print(pos)
        if pos == target:
            break
