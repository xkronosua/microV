//#define DEBUG true

#define stepPin  11
#define dirPin  12
#define enPin  10
#define homePin A0


#define stepPin1  9//3//6
#define dirPin1  5
#define enPin1  4
#define homePin1  -1


#define stepPin2  6
#define dirPin2  7
#define enPin2  8
#define homePin2   -1

//#define stepPin3  3
//#define dirPin3  2
//#define enPin3  1
//#define homePin3  -1

#define lampPin 3

#include "AsyncStream.h"

AsyncStream<100> serial(&Serial, '\n');

#define SMOOTH_ALGORITHM
#define DRIVER_STEP_TIME 10
#include <GyverStepper.h>

//#include "GyverTimers.h"

//GStepper< STEPPER2WIRE> stepper(800, stepPin, dirPin, enPin);
//GStepper< STEPPER2WIRE> stepper1(800, stepPin1, dirPin1, enPin1);
//GStepper< STEPPER2WIRE> stepper2(800, stepPin2, dirPin2, enPin2);
//GStepper< STEPPER2WIRE> stepper3(800, stepPin3, dirPin3, enPin3);

#define N_steppers 3
GStepper< STEPPER2WIRE> stepper[] = {
  {1600, stepPin, dirPin, enPin},
  {1600, stepPin1, dirPin1, enPin1},
  {1600, stepPin2, dirPin2, enPin2},
  //{1600, stepPin3, dirPin3, enPin3}
  };

int8_t HOME_PIN[] = {
  homePin,
  homePin1,
  homePin2//, homePin3
};

//String inputString_tmp = "";         // a String to hold incoming data
//String inputString = "";         // a String to hold incoming data

//bool stringComplete = false;  // whether the string is complete


long target[N_steppers];
bool wait_end[N_steppers];
bool _wait_onTarget[N_steppers];
bool wait_home[N_steppers];




int i=0;
void setup() {

// Пины D3 и D11 - 31.4 кГц
TCCR2B = 0b00000001;  // x1
TCCR2A = 0b00000001;  // phase correct


for (int i=0; i<N_steppers; i++){
    target[i] = 0;
    wait_end[i] = false;
    _wait_onTarget[i] = false;
    wait_home[i] = false;
  }

  Serial.begin(19200);
  pinMode(13,OUTPUT);

  pinMode(lampPin,OUTPUT);
  digitalWrite(lampPin,HIGH);
  
  digitalWrite(13,HIGH);
  //Serial.print("  ");
  // initialize serial:
  // reserve 200 bytes for the inputString:
  //inputString.reserve(200);
  for (int i=0;i<N_steppers;i++){
    stepper[i].setRunMode(FOLLOW_POS);
    stepper[i].setMaxSpeed(3200);
    stepper[i].setAcceleration(0);
    stepper[i].invertEn(true);
    if(HOME_PIN[i]>=0){
      pinMode(HOME_PIN[i], INPUT_PULLUP);
      }
  }
  
//  stepper[1].setMaxSpeed(3200);
//  stepper[1].setAcceleration(0);
//  // stepper[1].setAcceleration(0);
//  
  stepper[2].setMaxSpeed(1000);
  stepper[2].setAcceleration(0);
  
  //stepper[1].autoPower(true);
  //stepper[2].autoPower(true);
  
  stepper[0].enable();
  stepper[1].enable();
  stepper[2].enable();
  
  //stepper[1].disable();
  //stepper[2].disable();
  
  // настраиваем прерывания с периодом, при котором 
  // система сможет обеспечить максимальную скорость мотора.
  // Для большей плавности лучше лучше взять период чуть меньше, например в два раза
//  Timer2.setPeriod(stepper[2].getMinPeriod() / 2);
//  // взводим прерывание
//  Timer2.enableISR();
}


//// обработчик
//ISR(TIMER2_A) {
//  //for (int i=0; i<N_steppers; i++){
//    //stepper[0].tick(); // тикаем тут
//    //stepper[2].tick(); // тикаем тут
//  //}
//}

long value = 0;
int motor = 0;

//
//boolean isNumeric(String str) {
//    unsigned int stringLength = str.length();
// 
//    if (stringLength == 0) {
//        return false;
//    }
//    int start;
//    str.charAt(0)=='-' ? start = 1: start = 0;
//    for(unsigned int i = start; i < stringLength; ++i) {
//        if (isDigit(str.charAt(i))) {
//            continue;
//        } 
//        return false;
//    }
//    return true;
//}



void parseCommands(){
  //if (stringComplete) {
    char string_for_atoi[2] = { serial.buf[0], '\0' };
    motor = atoi(string_for_atoi); //inputString.substring(0,1);
    //if (motor<0 || motor>N_steppers) return;
    value = atol(serial.buf+2);

    //if(sscanf(serial.buf+2, "%d", &value) != 1)  return;
       
    #ifdef DEBUG
      Serial.print(serial.buf);
      Serial.print("|");
      Serial.print(strlen(serial.buf));
      Serial.print("|");
      Serial.print(serial.buf[0]);
      Serial.print(">");
      Serial.print(motor);
      Serial.print("|");
      Serial.print(serial.buf+2);
      Serial.print(">");
      Serial.print(value);
      Serial.print("|");
      //Serial.print((value)? "num": "err");

      Serial.print("|H1:");
      Serial.print(digitalRead(HOME_PIN[1]));
      
      
      Serial.println("|");
    #endif

    
    if(motor>N_steppers) return;
    
    switch (serial.buf[1]){
    case 'p':  {
        // get position
        Serial.println(stepper[motor].getCurrent());
        break;    
      }
    case 'm':{
      // get is_moving
      Serial.println(stepper[motor].tick());
      break;
    }
    case 'a':{
      // absolute move to
      target[motor] = value;
      stepper[motor].setTarget(target[motor]);
      Serial.println(stepper[motor].getCurrent());  
      break;
    }

    case 'A': {
      // absolute move to (wait)
      target[motor] = value;
      _wait_onTarget[motor] = true;
      stepper[motor].setTarget(target[motor]);
      break;
    }
    case 'H':{
      // move to endstop [if pin not -1]
      if (HOME_PIN[motor]>=0){
        target[motor] = -99999;
        //stepper[i].setRunMode(KEEP_SPEED);
        //stepper[i].setSpeedDeg(-10);   // медленно крутимся НАЗАД
        stepper[motor].setTarget(target[motor], RELATIVE);

      }
      else{
        target[motor] = 0;
        stepper[motor].setTarget(target[motor]);
        }
      _wait_onTarget[motor] = true;
      wait_home[motor] = true;
     break;
    }
    
    case 'r':{
      // relative move  
      target[motor] = target[motor] + value;
      stepper[motor].setTarget(target[motor]);
      Serial.println(stepper[motor].getCurrent());
      break;
    }

    case 'R':{
      // relative move (wait)
      if(!stepper[motor].tick())
      {
        target[motor] = target[motor] + value;
        _wait_onTarget[motor] = true;
        stepper[motor].setTarget(target[motor]);
        //Serial.println(stepper[motor].getCurrent());
      }
    break;
    }

    case 's':{
      // rewrite position
      //isNumeric(value) ? 
      target[motor] = value; //.toInt(): target[_motor];
      stepper[motor].setCurrent(target[motor]);
      Serial.println(stepper[motor].getCurrent());
      break;
    }
    case 'e':{
      // enable/disable
      value==1 ?  stepper[motor].enable() : stepper[motor].disable();
      Serial.println(value==1);
      break;
    }

    case 'L':{
      // enable/disable Lamp
      analogWrite(lampPin,value);
      Serial.println(value);
      break;
    }
       
    ////////////////////////////////////////////////////// 
    //--------------------------------------------------
//    Serial.println(inputString);
//    Serial.println(inputString.length());
//    
//    Serial.println(inputString.substring(1).toInt());
//    Serial.println(inputString.substring(2).toInt());
//    Serial.println(inputString.substring(3).toInt());
    
    //inputString = "";
    //stringComplete = false;
  //}
    }
}

void loop() {
  
//  if(Serial.available()){
//  
//    char inChar = (char)Serial.read();
//    if (inChar == '\r'){}
//    else if (inChar == '\n') {
//      stringComplete = true;
//      if(inputString_tmp.length()<2){
//        inputString_tmp = "";
//        stringComplete = false;
//        }
//      else{
//        inputString = inputString_tmp;
//        inputString_tmp = "";
//        }
//    }
//    else{
//      inputString_tmp += inChar;
//      }
//  
//  }
  if (serial.available()) {     // если данные получены
    //Serial.println(serial.buf); // выводим их (как char*)    
    parseCommands();
    serial.buf[0]=NULL;
  }
  

  for(i=0;i<N_steppers;i++){
    if(stepper[i].tick() && wait_home[i]){
      if(HOME_PIN[i]>=0){
        //Serial.println(stepper[i].getCurrent());
        if(digitalRead(HOME_PIN[i])){
          stepper[i].stop();
          stepper[i].reset();
          stepper[i].brake();
          Serial.println(stepper[i].getCurrent());
          wait_home[i] = false;
          _wait_onTarget[i] = false;
          target[i]=0;
          stepper[i].setTarget(target[i]);
          //stepper[i].setRunMode(FOLLOW_POS);
          }
        }
      }
      if (!stepper[i].tick() && _wait_onTarget[i]){
         Serial.println(stepper[i].getCurrent());
         _wait_onTarget[i] = false;
         wait_home[i] = false;
        }
      //stepper[i].tick();
      //if (!stepper[i].tick()) {
      //   stepper[i].setTarget(target[i]);
      //}
  
    }

}
