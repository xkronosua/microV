# -*- coding: utf-8 -*-
"""
APT Motor Controller for Thorlabs
Adopted from
https://github.com/HaeffnerLab/Haeffner-Lab-LabRAD-Tools/blob/master/cdllservers/APTMotor/APTMotorServer.py
With thanks to SeanTanner@ThorLabs for providing APT.dll and APT.lib


V1.1
20141125 V1.0    First working version
20141201 V1.0a   Use short notation for moving (movRelative -> mRel)
20150417 V1.1    Implementation of simple QT GUI

Michael Leung
mcleung@stanford.edu
"""
import logging
import os
import time
import microscope
import microscope.abc
import threading


class Motor_sim(object):
    position = 0
    motion_error = False
    is_in_motion = False

    def __init__(self):
        pass

    def enable(self):
        self.alive = True
        # t = threading.Thread(target=self.process,daemon=True)
        # t.start()
        return True

    def disable(self):
        self.alive = False

    def get_stage_axis_info(self):
        return ("TDC001", "31")

    def move_to(self, target, wait=False):
        print("moving:start")
        # self.is_in_motion = True
        self.target = target

        self.position = target
        self.is_in_motion = False
        print("moving:done")

    def move_by(self, delta, wait=False):
        # self.is_in_motion = True
        self.target = self.position + delta

        self.position = self.target
        self.is_in_motion = False

    def move_home(self):
        # self.is_in_motion = True
        self.target = 0
        self.position = 0

        self.is_in_motion = False

    def process(self):
        current_pos = 0

        dx = 0.1
        target = 0

        while self.alive:
            print(self.position, self.target)

            if abs(current_pos - self.target) >= dx:

                direction = (self.target - current_pos) / abs(self.target - current_pos)
                current_pos += dx * direction
                self.is_in_motion = True
            else:
                current_pos = self.target
                self.position = current_pos
                self.is_in_motion = False

            time.sleep(0.5)
            print("moving")


class _TDC001(microscope.abc.StageAxis):

    demo = False
    stage = None
    # stepsPerUnit = 1
    initialized = False

    def __init__(
        self,
        SerialNum=None,
        HWTYPE=None,
        limits=microscope.AxisLimits(0, 360),
        # stepsPerUnit=1,
        demo=False,
        **kwargs
    ):

        super().__init__(**kwargs)
        self._limits = limits
        # self.stepsPerUnit = stepsPerUnit
        # self.address = address
        # self.connection = connection
        self.demo = demo
        if demo:
            self.stage = Motor_sim()
        else:
            import thorlabs_apt

            # print(thorlabs_apt.list_available_devices())

            self.stage = thorlabs_apt.Motor(serial_number=SerialNum)

    def initialize(self):
        if not self.initialized:
            self.stage.enable()
        self.initialized = True

    def shutdown(self):

        self.stage.disable()
        self.initialized = False

    def is_alive(self):
        return self.initialized and not self.stage.motion_error

    def get_status(self):
        info = self.stage.get_stage_axis_info()
        return info

    def get_position(self):
        """Return the stage's position."""
        position = self.stage.position

        return position

    @property
    def position(self):
        return self.get_position()

    @property
    def limits(self) -> microscope.AxisLimits:
        return self._limits

    def move_to(self, target=None, wait=False):
        """Move to target"""
        # The default position set points are zero. If the motors are started without
        # writing to a target, that motor will move to zero, so only start motors
        # if a target has been provided.
        if target is None:
            self.stage.move_home()
        else:
            self.stage.move_to(target)
            if wait:
                for i in range(500):
                    time.sleep(0.1)
                    if not self.is_moving():
                        break

        return True

    def move_by(self, delta, wait=False):
        self.stage.move_by(delta)
        if wait:
            for i in range(50):
                time.sleep(0.05)
                if not self.is_moving():
                    break
        return True

    def is_moving(self):
        return self.stage.is_in_motion


class TDC001_rotationStage(microscope.abc.Stage):
    """TDC001 controller for multiple devices."""

    def __init__(self, axConfig=None, demo=False, **kwargs) -> None:
        super().__init__(**kwargs)

        self.demo = demo

        if axConfig is None:
            axConfig = {
                "HWP": {"SerialNum": 83854487, "HWTYPE": 31, "limits": (0, 360)}
            }

        _axes = {}
        for name in axConfig:

            _axes[name] = _TDC001(SerialNum=axConfig[name]["SerialNum"], demo=demo)
            print(_axes)
        self._axes = _axes

    def initialize(self) -> bool:
        pass

    def _on_shutdown(self) -> None:
        for name in self._axes:
            self._axes[name].shutdown()

    def _do_shutdown(self) -> None:
        for name in self._axes:
            self._axes[name].shutdown()

    @property
    def axes(self):
        return self._axes

    def move_by(self, delta) -> float:
        for name, rpos in delta.items():
            self.axes[name].move_by(rpos)

    def move_to(self, position) -> float:
        for name, pos in position.items():
            self.axes[name].move_to(pos)

    def is_alive(self):
        res = []
        for name in self._axes:
            res.append(self._axes[name].is_alive())
        return any(res)


if __name__ == "__main__":
    # -*- coding: utf-8 -*-
    import sys

    demo = False
    if "sim" in sys.argv:
        demo = True

    stage = TDC001_rotationStage()

    stage.axes["HWP"].initialize()
    print("pos:", stage.axes["HWP"].position)
    print("move_to 10:", stage.axes["HWP"].move_to(10))
    print("move_to :", stage.axes["HWP"].move_to())
    print("move_by 10:", stage.axes["HWP"].move_by(10))
    print("move_by -10:", stage.axes["HWP"].move_by(-10))

    print("HWP is alive:", stage.axes["HWP"].is_alive())
    print("stage is alive:", stage.is_alive())
    print("HWP status:", stage.axes["HWP"].get_status())
    print("stage shutdown:", stage.shutdown())
    print("HWP is alive:", stage.axes["HWP"].is_alive())
    print("stage is alive:", stage.is_alive())
