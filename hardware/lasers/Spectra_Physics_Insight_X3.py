#!/usr/bin/env python3

import logging
import serial
import microscope.abc
from serial.tools import list_ports
import time
from threading import Thread
import threading

# import exdir

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class InSightX3Laser(microscope.abc.SerialDeviceMixin, microscope.abc.LightSource):
    trigger_mode = microscope.TriggerMode.ONCE
    trigger_type = microscope.TriggerType.SOFTWARE

    _monitor_thread = None
    monitor_enabled = False
    demo = False

    lastRequestTime = time.time()
    laserInfo = {}
    laserInfoUpdateTimeout = 1
    watchdogTime = time.time()
    watchdogTimeout = 3600 # s

    def __init__(self, com=None, baud=115200, timeout=0.3, demo=False, **kwargs):
        super().__init__(**kwargs)
        info_keys = [
            "power",
            "wavelength",
            "precompensation",
            "status",
            "enabled",
            "ON_OFF",
        ]
        for key in info_keys:
            self.laserInfo[key] = {"value": 0, "time": time.time()}
        state = {
            "RawSTB": None,  # "i8"),
            "Emission": None,  # "i2"),
            "Pulsing": None,  # "i2"),
            "Main shutter": None,  # "i2"),
            "IR shutter": None,  # "i2"),
            "Servo on": None,  # "i2"),
            "User interlock": None,  # "i2"),
            "Keyswitch": None,  # "i2"),
            "Power supply": None,  # "i2"),
            "Internal": None,  # "i2"),
            "Warning": None,  # "i2"),
            "fault": 1,  # "i2"),
            "MODE": "RUN",
            "State": "Status is wrong",  # "S20"),
            "ON_OFF": False,  # "i2"),
            "time": 0,  #'float64')
        }
        self.laserInfo["status"] = {"value": state, "time": time.time()}

        if com is None:
            ports = list(list_ports.grep("10c4:ea60"))
            if len(ports) > 0:
                com = ports[0].device
        self.demo = demo
        if self.demo:
            # com = './ttyclient'
            # baud = 9600
            _logger.info("DUMMMMMMMY")
            self.connection = SerialDummy()
        else:
            self.connection = serial.Serial()
            self.connection.port = com
            self.connection.baudrate = baud
            self.connection.timeout = timeout
            self.connection.stopbits = serial.STOPBITS_ONE
            self.connection.bytesize = serial.EIGHTBITS
            self.connection.parity = serial.PARITY_NONE
            self.connection.set_buffer_size(rx_size=8192, tx_size=None)
            self.connection.xonxoff = True

    @microscope.abc.SerialDeviceMixin.lock_comms
    def send(self, command: bytes, timeout=5) -> bytes:
        """Send command and retrieve response."""
        success = False
        t0 = time.time()
        response = b""

        while not success:
            if time.time() - t0 > timeout:
                self._flush_handshake()
                break
            self._write(command)
            # time.sleep(0.1)
            response = self._readline()
            # Catch zero-length responses to queries and retry.
            if not command.endswith(b"?"):
                success = True
            elif len(response) > 0:
                success = True
        return response

    # @microscope.abc.SerialDeviceMixin.lock_comms
    # def flush_buffer_(self):
    # 	maxN = 10
    # 	n = 0
    # 	while self.connection.inWaiting()>1:
    # 		response = self._readline()
    # 		n+=1
    # 		if n>= maxN: break

    def _flush_handshake(self):
        self.connection.reset_output_buffer()
        self.connection.reset_input_buffer()

    def getInfo(self):
        return self.laserInfo

    # @microscope.abc.SerialDeviceMixin.lock_comms
    def enable(self):
        """Turn the laser ON. Return True if we succeeded, False otherwise."""
        _logger.info("Turning laser ON.")
        status = self.get_status()

        if status["State"] != "READY":
            _logger.error("Laser not ready!")
            return False

        if not self.monitor_enabled:
            self.monitor_enabled = True

            self._monitor_thread = Thread(target=self._monitor_loop)
            self._monitor_thread.daemon = True
            self._monitor_thread.start()

        _logger.info("Laser: ON...")
        with self._comms_lock:
            self._write(b"ON")
        self.setWatchdog(11)
        watchdog = self.watchdog()
        _logger.info(f"Watchdog timeout: {watchdog}")

        t0 = time.time()
        warmedup = b"0"

        while warmedup < b"100" and time.time() - t0 < 130:
            time.sleep(1)
            warmedup = self.send(b"READ:PCTWarmedup?")
            _logger.info(f"PCTWarmedup:{warmedup.decode()}")

        status = self.get_status()
        t0 = time.time()
        while not status["State"] in ["RUN", "ALIGN"] and time.time() - t0 < 400:
            time.sleep(1)
            status = self.get_status()
            # print(r)
        if not status["State"] in ["RUN", "ALIGN"]:
            _logger.error("laserOn: FAIL!")
            return False

        if not self.get_is_on():
            # Something went wrong.
            _logger.error(f"Failed to turn ON. Current status:{self.get_status()}")
            return False

        self.kickWatchdog()

        return True

    def _monitor_loop(self):
        _logger.info("_monitor_loop: started")
        while self.monitor_enabled:
            # self.flush_buffer_()
            if (
                time.time() - self.lastRequestTime > 2
                or time.time() - self.laserInfo["status"]["time"]
                > self.laserInfoUpdateTimeout
            ):
                self.get_status()
                self.lastRequestTime = time.time()

            if (
                time.time() - self.laserInfo["wavelength"]["time"]
                > self.laserInfoUpdateTimeout
            ):
                self.wavelength()

            if (
                time.time() - self.laserInfo["power"]["time"]
                > self.laserInfoUpdateTimeout
            ):
                self.power()

            if (
                time.time() - self.laserInfo["precompensation"]["time"]
                > self.laserInfoUpdateTimeout
            ):
                self.precompensation()
            if (time.time() - self.watchdogTime) > self.watchdogTimeout:
                if self.get_is_on():
                    self.disable()
                    _logger.warning("watchdogTimeout")

            time.sleep(0.01)


        _logger.info("_monitor_loop: finished")

    def _on_shutdown(self):
        if self.is_alive():
            self.disable()
        self.monitor_enabled = False
        # self._flush_handshake()

    def _do_shutdown(self):
        if self.is_alive():
            self.disable()
        self.monitor_enabled = False
        # self._flush_handshake()

    def initialize(self):
        try:
            self.connection.open()
            # self.connection.isOpen()
        except Exception as ex:
            _logger.error("Can't connect to laser.")  # ,exc_info=ex)
            return False
        response = self.send(b"*IDN?")
        _logger.info("InSightX3 laser serial number: [%s]", response.decode())
        self._max_power_mw = 3000  # ???

        if not self.monitor_enabled:
            self.monitor_enabled = True

            self._monitor_thread = Thread(target=self._monitor_loop)
            self._monitor_thread.daemon = True
            self._monitor_thread.start()

        # self.flush_buffer()
        return True

    @microscope.abc.SerialDeviceMixin.lock_comms
    def disable(self):
        """Turn the laser OFF. Return True if we succeeded, False otherwise."""
        _logger.info("Turning laser OFF.")
        # Turning LASER OFF
        self._write(b"OFF")
        # self._flush_handshake()
        self.monitor_enabled = False
        if self.get_is_on():
            _logger.error("Failed to turn OFF. Current status:\r\n")
            _logger.error(self.get_status())
            return False
        return True

    # @microscope.abc.SerialDeviceMixin.lock_comms
    def is_alive(self):
        # if self.connection.isOpen():
        if self.demo:
            return True
        try:
            self.connection.inWaiting()
            return True
        except:
            self.monitor_enabled = False
            return False

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_is_on(self):
        """Return True if the laser is currently able to produce light."""
        status = self.get_status()
        is_on = not (status["State"] in ["Initializing", "READY", ""])
        _logger.info("Are we on? [%s:%s]", status["State"], status["RawSTB"])
        return is_on

    @microscope.abc.SerialDeviceMixin.lock_comms
    def get_status(self):
        resp = {}

        status = self.send(b"STB?")
        resp["RawSTB"] = status.decode()

        try:
            status = int(status)
        except:
            res = {
                "RawSTB": None,  # "i8"),
                "Emission": None,  # "i2"),
                "Pulsing": None,  # "i2"),
                "Main shutter": None,  # "i2"),
                "IR shutter": None,  # "i2"),
                "Servo on": None,  # "i2"),
                "User interlock": None,  # "i2"),
                "Keyswitch": None,  # "i2"),
                "Power supply": None,  # "i2"),
                "Internal": None,  # "i2"),
                "Warning": None,  # "i2"),
                "fault": 1,  # "i2"),
                "State": "Status is wrong",  # "S20"),
                "ON_OFF": False,  # "i2"),
                "time": 0,  #'float64')
            }
            return res
        # 0 0x00000001 Emission The laser diodes are energized. Laser emission is possible if the
        # 		shutter(s) is (are) open.

        resp["Emission"] = (status & 0x00000001) == 1

        # 1 0x00000002 Pulsing 1 = “pulsing”
        # 		The name PULSING is used for compatibility with Mai Tai. In the
        # 		InSight DS+ system, this bit indicates that the laser has either:
        # 		a) Reached the Run state and can be used to take data.
        # 		or
        # 		b) Reached the Align state and can be used to align the optical
        # 		system.
        resp["Pulsing"] = (status & 0x00000002) != 0

        # 2 0x00000004 Main shutter 1 = The main shutter is open (sensed position).
        resp["Main shutter"] = (status & 0x00000004) != 0

        # 3 0x00000008 Dual beam
        # 		shutter 1 = The IR shutter is open (sensed position).
        # 		N OTE : On systems purchased without the optional dual beam
        # 		output, this bit is always set to 1.
        resp["IR shutter"] = (status & 0x00000008) != 0

        # 5 0x00000020 Servo on 1 = Servo is on (see “SERVO” command on page A-6).
        resp["Servo on"] = (status & 0x00000020) != 0

        # 9 0x00000200 User interlock 1 = The user interlock (CDRH interlock) is open; laser is forced off.
        resp["User interlock"] = status & 0x00000200

        # 10 0x00000400 Keyswitch 1 = The safety keyswitch interlock is open; laser is forced off.
        resp["Keyswitch"] = status & 0x00000400

        # 11 0x00000800 Power supply 1 = The power supply interlock is open; laser is forced off.
        resp["Power supply"] = status & 0x00000800

        # 12 0x00001000 Internal 1 = The internal interlock is open; laser is forced off.
        resp["Internal"] = status & 0x00001000

        # 14 0x00004000 Warning 1 = The system is currently detecting a warning condition. The laser
        # 	continues to operate, but it is best to resolve the issue at your earliest
        # 	convenience.
        # 	Use READ:HISTORY? (page A-4) to see what is causing the
        # 	warning.
        resp["Warning"] = status & 0x00004000

        # 15 0x00008000 fault 1 = The system is currently detecting a fault condition. InSight
        # 	immediately turns off the laser diodes. Use READ:HISTORY? to see
        # 	what is causing the fault.
        # 	Note: The fault condition may clear itself when the laser turns itself
        # 	off. If so, the fault bit clears.
        resp["fault"] = status & 0x00008000

        # 16 to 22 0x007F0000	State
        # 	After masking, shift right 16 bits and interpret as a number with a
        # 	value from 0 to 127 (see Table A-2).
        # 	Most state numbers are not specifically assigned, but several ranges
        # 	can be described and three specific values (25, 50, and 60) are
        # 	guaranteed not to change.
        s = (status & 0x007F0000) >> 16
        if s >= 0 and s <= 24:
            resp["State"] = "Initializing"
        elif s == 25:
            resp["State"] = "READY"
        elif s >= 26 and s <= 49:
            resp["State"] = "Turning on / optimizing"
        elif s == 50:
            resp["State"] = "RUN"
        elif s >= 51 and s <= 59:
            resp["State"] = "Moving to Align mode"
        elif s == 60:
            resp["State"] = "ALIGN"
        elif s >= 61 and s <= 69:
            resp["State"] = "Exiting Align mode"
        else:
            resp["State"] = ""

        resp["ON_OFF"] = not (resp["State"] in ["Initializing", "READY", ""])

        resp["MODE"] = None
        if resp["State"] == "RUN":
            resp["MODE"] = "RUN"
        elif resp["State"] == "ALIGN" or resp["State"] == "Moving to Align mode":
            resp["MODE"] = "ALIGN"

        resp["time"] = time.time()

        self.laserInfo["status"]["value"] = resp
        self.laserInfo["status"]["time"] = time.time()
        return resp

    def _do_get_power(self) -> float:
        pass

    def _do_set_power(self, val):
        pass

    @microscope.abc.SerialDeviceMixin.lock_comms
    def power(self):
        """Returns the laser output power (in Watts)."""
        response = self.send(b"READ:POWer?")
        try:
            result = float(response.decode())
            self.laserInfo["power"]["value"] = result
            self.laserInfo["power"]["time"] = time.time()
            return result
        except Exception as ex:
            _logger.error(f"power", exc_info=ex)
            return

    @microscope.abc.SerialDeviceMixin.lock_comms
    def watchdog(self) -> float:
        """Get the number of seconds for the software watchdog timer."""
        response = self.send(b"TIMer:WATChdog?")
        try:
            result = float(response.decode())
            return result
        except Exception as ex:
            _logger.error(f"watchdog", exc_info=ex)
            return

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setWatchdog(self, timeout: int) -> None:
        """Set the number of seconds for the software watchdog timer."""
        self.kickWatchdog()
        self._write(b"TIMer:WATChdog %d" % timeout)

    def setWatchdogForServer(self, timeout: int) -> None:
        """Set the number of minutes from last request for the watchdog timer."""
        self.kickWatchdog()
        self.watchdogTimeout = timeout*60

    def kickWatchdog(self) -> None:
        """Forse reset watchog timer(for measurements)."""
        self.watchdogTime = time.time()

    def getWatchdogTimeLeft_sec(self) -> None:
        """Get time left (in seconds) until laser OFF by watchdog."""
        time_left_s = self.watchdogTimeout - (time.time() - self.watchdogTime)
        if time_left_s < 0:
            time_left_s = 0
        return int(time_left_s)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def wavelength(self) -> int:
        """Get the laser wavelength."""
        response = self.send(b"READ:WAVelength?")
        try:
            result = int(response.decode())
            self.laserInfo["wavelength"]["value"] = result
            self.laserInfo["wavelength"]["time"] = time.time()
            return result
        except Exception as ex:
            _logger.error(f"wavelength", exc_info=ex)
            return

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setWavelength(self, wl: int, wait: bool = False, timeout: float = 10) -> None:
        """Set the laser wavelength."""
        self.kickWatchdog()
        self._write(b"WAVelength %d" % wl)
        if wait:
            t0 = time.time()
            while (time.time() - t0) < timeout:
                val = self.wavelength()
                if val == wl:
                    t0 = time.time()
                    status = self.get_status()
                    while (
                        not status["State"] in ["RUN", "ALIGN"]
                        and (time.time() - t0) < timeout
                    ):
                        status = self.get_status()
                    break
                else:
                    time.sleep(0.2)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def lcd_brightness(self) -> int:
        """Read the power supply LCD screen back illumination brightness level [0 - 255]."""
        response = self.send(b"LCD:BRIGhtness?")
        try:
            result = int(response.decode())
            return result
        except Exception as ex:
            _logger.error(f"lcd_brightness", exc_info=ex)
            return

    @microscope.abc.SerialDeviceMixin.lock_comms
    def set_lcd_brightness(self, val: int) -> None:
        """Set the power supply LCD screen back illumination brightness level [0 - 255]."""
        self.kickWatchdog()
        self._write(b"LCD:BRIGhtness %d" % val)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def MODE(self) -> str:
        """Read the system mode: value=[RUN, ALIGN]"""
        response = self.send(b"MODE?")
        if response == b"":
            return

        return response.decode()

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setMODE(self, mode: str) -> None:
        """Set the system mode: value=[RUN, ALIGN]"""
        self.kickWatchdog()
        try:
            self._write(b"MODE %s" % mode.encode())
        except:
            _logger.error(f"Wrong MODE [{mode}]")

    @microscope.abc.SerialDeviceMixin.lock_comms
    def shutter(self) -> bool:
        """Read the main shutter state."""
        # response = self.send(b"SHUTter?")[0] == b'1'
        status = self.get_status()
        return status["Main shutter"]

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setShutter(self, state: bool) -> None:
        """Set the main shutter state."""
        self.kickWatchdog()
        if state:
            state = b"1"
        else:
            state = b"0"
        self._write(b"SHUTter " + state)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def IRshutter(self) -> bool:
        """Read the IR shutter state."""
        # response = self.send(b"SHUTter?")[0] == b'1'
        status = self.get_status()
        return status["IR shutter"]

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setIRshutter(self, state: bool) -> None:
        """Set the IR shutter state."""
        self.kickWatchdog()
        if state:
            state = b"1"
        else:
            state = b"0"
        self._write(b"IRSHUTter " + state)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def precompensationLimits(self) -> float:
        """Read the actual DeepSee precompensation motor position."""
        try:
            MIN = float(self.send(b"CONTrol:SLMIN?").decode())
            MAX = float(self.send(b"CONTrol:SLMAX?").decode())

            return (MIN, MAX)
        except Exception as ex:
            _logger.error(f"precompensationLimits", exc_info=ex)
            return (None, None)

    @microscope.abc.SerialDeviceMixin.lock_comms
    def precompensation(self) -> float:
        """Read the actual DeepSee precompensation motor position."""
        response = self.send(b"CONT:DSMPOS?")
        try:
            result = float(response.decode())
            self.laserInfo["precompensation"]["value"] = result
            self.laserInfo["precompensation"]["time"] = time.time()
            return result
        except Exception as ex:
            _logger.error(f"precompensation", exc_info=ex)
            return

    @microscope.abc.SerialDeviceMixin.lock_comms
    def setPrecompensation(self, target: float) -> None:
        """Set precompensation motor position."""
        self.kickWatchdog()
        self._write(b"CONTrol:MTRMOV %2.2f" % target)

    def _do_trigger(self):
        pass

    def set_trigger(self, ttype, tmode):
        self.trigger_mode = tmode
        self.trigger_type = ttype


import numpy as np
import re
import pandas as pd


DEFAULT_INFO_Status_hide = [
    "User interlock",
    "Power supply",
    "Keyswitch",
    "Internal",
    "Servo on",
    "Pulsing",
]


class SerialDummy(object):

    alive = False
    line = b""
    response = b""
    lock = threading.RLock()
    # store = exdir.File('demo_state.exdir','a')
    def __init__(self):
        _logger.info("DUMMMMMMMY")
        self.wavelength = 1045
        self.lcd = 155
        self.watchdog = 3
        self.precompensation = 1
        self.status = {
            "Emission": False,
            "Pulsing": False,
            "Main shutter": False,
            "IR shutter": False,
            "Servo on": False,
            "User interlock": False,
            "Keyswitch": False,
            "Power supply": False,
            "Internal": False,
            "Warning": False,
            "fault": False,
            "State": "READY",
        }

    def open(self):
        self.alive = True
        # t = threading.Thread(target=self.process,daemon=True)
        # t.start()
        return True

    def flushInput(self):
        pass  # self.line = b''

    def flushOutput(self):
        pass  # self.response = b''

    def reset_output_buffer(self):
        self.response = b""

    def is_alive(self):
        return self.alive

    def isOpen(self):
        return self.alive

    def inWaiting(self):
        return True

    def close(self):
        self.alive = False

    def write(self, line):
        with self.lock:
            self.line = line

        # print(self.line)

    def status_encode(self, status):
        res = 0
        if status["Emission"]:
            res += 0x00000001
        if not status["Pulsing"]:
            res += 0x00000002
        if status["Main shutter"]:
            res += 0x00000004
        if status["IR shutter"]:
            res += 0x00000008
        if status["Servo on"]:
            res += 0x00000020
        if status["User interlock"]:
            res += 0x00000200
        if status["Keyswitch"]:
            res += 0x00000400
        if status["Power supply"]:
            res += 0x00000800
        if status["Internal"]:
            res += 0x00001000
        if status["Warning"]:
            res += 0x00004000

        if status["fault"]:
            res += 0x00008000
        s = 0
        # print(status['State'])
        if status["State"] == "Initializing":
            s = 24  # s >= 0 and s <= 24:
        if status["State"] == "READY":
            s = 25

        if status["State"] == "Turning on / optimizing":
            s = 49  # s>= 26 and s <= 49:
        if status["State"] == "RUN":
            s = 50
        if status["State"] == "Moving to Align mode":
            s = 59  # s>= 51 and s <= 59:
        if status["State"] == "ALIGN":
            s = 60
        if status["State"] == "Exiting Align mode":
            s = 69  # s>=61 and s <= 69:
        # print(s,s<<16)
        res += s << 16
        return res

    def readline(self):
        for i in range(100):
            if self.response != b"":
                break
            time.sleep(0.01)
        resp = self.response
        self.response = b""
        return resp

    def write(self, line):
        self.response = b""
        time.sleep(0.01)
        print(f">>{line}")
        with self.lock:
            line = line.replace(b"\r", b"").split(b"\n")[0]
            if b"*IDN?" in line:
                self.response += b"Spectra-Physics, InSight DeepSee, xxxx, 0180-0.01.60/175-1.00.15/TN00001003\n"

            elif b"SHUTter" in line:
                if b"IR" in line:
                    if b"?" in line:
                        self.response += (
                            str(int(self.state["IR shutter"])).encode() + b"\n"
                        )
                    else:
                        self.status["IR shutter"] = b"1" in line
                else:
                    if b"?" in line:
                        self.response += (
                            str(int(self.state["Main shutter"])).encode() + b"\n"
                        )
                    else:
                        self.status["Main shutter"] = b"1" in line
            elif b"READ:POWer?" in line:
                self.response += str(np.random.rand(1)[0]).encode() + b"\n"
            elif b"LCD:BRIGhtness" in line:
                if b"?" in line:
                    self.response += str(self.lcd).encode() + b"\n"
                else:
                    self.lcd = int(line.split(b" ")[1])
            elif b"TIMer:WATChdog" in line:

                if b"?" in line:
                    self.response += str(self.watchdog).encode() + b"\n"
                else:
                    self.watchdog = int(line.split(b" ")[1].decode())

            elif b"CONT:DSMPOS?" in line:
                self.response += str(self.precompensation).encode() + b"\n"
            elif b"CONTrol:MTRMOV" in line:
                self.precompensation = float(line.split(b" ")[1].decode())
            elif b"MODE" in line:
                if b"?" in line:
                    if self.status["State"] == "ALIGN":
                        self.response += b"ALIGN\n"
                    else:
                        self.response += b"RUN\n"
                else:
                    self.status["State"] = line.split(b" ")[1].decode()
            elif b"STB?" in line:
                s = self.status_encode(self.status)
                self.response += str(s).encode() + b"\n"

            elif b"WAVelength" in line:
                if b"?" in line:
                    self.response += str(self.wavelength).encode() + b"\n"
                else:
                    wl = int(line.split(b" ")[1])
                    time.sleep(abs(self.wavelength - wl) / 100)
                    self.wavelength = wl

            elif b"READ:PCTWarmedup?" in line or b"ON" in line:
                self.response += b"100\n"
                self.status["Emission"] = True
                self.status["State"] = "RUN"

            elif b"OFF" in line:
                self.status["State"] = "READY"
                self.status["Emission"] = False

            else:
                pass
            print(self.status, self.response)
